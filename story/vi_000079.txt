Những năm sống êm đềm với vợ, Vỹ luôn luôn tưởng tượng ra một tình cảnh chết người: Tỉ dụ hôm nào đó, anh bắt gặp Hoa - vợ anh - đang ngây ngất bên một gã lạ mặt, không hiểu anh sẽ ứng xử ra sao? Đấy là dấu hiệu của người rất yêu vợ. Tưởng thế thì từ phía anh sẽ chẳng có chuyện gì.

Nhưng rồi mọi việc cứ như tự nhiên phải thế. Một mặt, Vỹ “quản” vợ cả trong giấc ngủ, mặt khác, không biết từ lúc nào anh cứ tơ tưởng tới Huệ - cô nhân viên đánh máy của cơ quan. Huệ có ông chồng càng ngày càng béo ị. Khi chiếc bụng chảy ra, người đàn ông, trước mặt vợ có gì đó rất thảm hại. Huệ cứ chán đời dần... để cuối cùng “đầu mày, cuối mắt” với Vỹ.

Hoa nhận ra vài nét thay đổi ở chồng: Hay thảng thốt, lười ăn, biếng ngủ, việc gì cũng bất như ý và nhân đấy kiếm cớ... thở dài. Vốn tin chồng, Hoa chỉ nghĩ chắc công việc cơ quan làm Vỹ mệt mỏi, tịnh không nghi ngờ tí ti Vỹ mắc bệnh ngoại tình.

Chính vì thế Hoa ra sức chăm sóc Vỹ. Nhà cửa sạch sẽ hơn, quần áo thơm tho hơn, cơm canh sốt dẻo hơn và lời ăn tiếng nói cứ dịu dàng đến mức Vỹ không chịu được.

Một vài lần hẹn hò đưa Huệ đi phố, Vỹ thấy Huệ cũng xăm xắn mua sắm cho chồng. Càng bít rít với Vỹ, Huệ càng tỏ vẻ quan tâm đến anh chồng sắp bị cắm sừng, như một kiểu chuộc lỗi trước. Điều đó không khỏi có lúc khiến Vỹ khinh bỉ: “Mẹ cái bọn đàn bà! Thấy nó tự dưng chiều chuộng đừng vội tưởng bở”.

Sự trùng hợp ngẫu nhiên với những săn sóc của Hoa dành cho mình làm Vỹ không yên tâm. Nhưng cơ thể nóng rực của Huệ với cặp mắt lúng la lúng liếng hút mất hồn anh.

Một hôm, Hoa đem về bộ vét rất nhã, bắt Vỹ ướm thử. Ngắm chồng một cách hồ hởi, Hoa nói đùa:

- Trông anh thích lắm!

Vỹ tái mặt vì điều đó gợi anh nhớ tới lần Huệ cũng sắm cho chồng một bộ vét, nói bô bô trước cơ quan: “Ông ấy nhà em chỉ thích những thứ vợ sắm”. Thế rồi ngay chiều ấy, Huệ nhận lời đi chơi Hồ Tây với Vỹ. Hai người ngồi sát bên nhau, toàn dùng những ngôn ngữ mèo chuột.

 
 Minh họa: Lê Huy Quang


Tuy thế chưa có chuyện gì nghiêm trọng. Phải đến hôm hai người, kẻ dối chồng, người gạt vợ nói là cơ quan có việc đột xuất phải làm đêm, kỳ thực là hẹn nhau đi chơi. Cả hai cùng hiểu ngầm, đêm nay mọi chuyện sẽ được dọn dẹp để không còn bất cứ ngăn cách nhỏ nào giữa họ. Từ lúc cơm chiều xong Vỹ đã thấp thỏm nhìn ra cổng. Như bổn phận truyền kiếp của người vợ, Hoa vẫn tất tưởi dọn dẹp. Lúc Vỹ sắp đi, rất vô tình, Hoa đem đồ ra là lượt, định gấp cất đi chờ có dịp mới mặc. Trong khi Hoa lúi húi với chiếc bàn là, Vỹ vừa sốt ruột sợ lỡ hẹn với Huệ, vừa vô cùng bất an trước việc làm không mấy logic (theo suy đoán của anh) của Hoa. “Hay cô ta cũng hẹn ai?”. Vỹ có cảm giác chờ anh ra khỏi nhà là Hoa cũng biến. Như một kẻ bất lực, anh lén mở chiếc xắc của vợ. Ngoài lọ nước hoa, vài thứ lặt vặt, có thêm bao thuốc ba số. Vỹ nóng ran cả mặt nhưng sợ Hoa biết hành vi “khám xét” của anh nên không dám hỏi. “Được, nếu ta về không thấy bao thuốc thì mụ biết tay”. Thấy Vỹ ngồi thừ ở ghế, Hoa nói vọng ra:

- Anh còn chưa đi à? Rồi lại muộn cho mà xem. Đi đường, bọn trẻ hay đua xe, anh phải cẩn thận.

Vỹ đành ra khỏi nhà. Ngồi trên chiếc Dream mà anh tưởng ngồi trên chiếc xe bò. Mặt đường sao bỗng dưng lồi lõm thế. Từ xa Vỹ đã nhận ra Huệ với chiếc váy mi ni đang đứng như một mệnh phụ đoan chính sắp qua đường. “Chà, đàn bà, nguy hiểm thật. Liệu lão chồng béo ị của cô ta có tưởng tượng nổi đêm nay vợ lão tan nát dưới tay ta?”. Phanh kít xe trước mặt Huệ, chưa vội đón cái nhìn hiến thân của cô ta, Vỹ hất hàm:

- Cái gì ở trong chiếc xắc kia?

- Súng lục - Huệ cười ngặt nghẽo. Làm không ra hồn tôi bắn cho nát đầu. Đi thôi chứ.

Vỹ vẫn cộc cằn:

- Tôi muốn biết cô mang theo cái gì?

Huệ thì thào:

- Ngốc ạ, toàn cho anh cả...

- Xem trước?

- Vớ vẩn, thì đây.

Huệ hé miệng túi ra đủ để Vỹ nhìn thấy “những thứ dành cho anh” trong đó có bao Dunhil đỏ rực. Mắt anh tối sầm lại. Vỹ nói hổn hển:

- Anh có việc hệ trọng, đành lỡ hẹn với em.

Huệ đang cười tình, sầm mặt:

- Việc gì?

- Chưa thể nói được.

- Hừ - Huệ khinh bỉ quay đi - Tôi không phải là con điếm. Goodbye!

Vỹ quay xe trong khi Huệ cũng bỏ đi. Anh phóng như điên như dại. Dọc đường, cứ thấy xe nào có đèo phụ nữ anh đều tưởng đấy là vợ mình. Đến lối rẽ vào khu tập thể, do ngoái lại nhìn một thiếu phụ đang đứng chờ ai, xe Dream của anh tông phải chiếc công nông đỗ lại lấy hàng. May chỉ vỡ chiếc yếm và trẹo đầu gối. Lê lết đẩy chiếc xe về nhà, anh có cảm giác sắp phải chọc tiết đồ gian phu, dâm phụ đang quấn vào nhau. Cửa mở ra, Vỹ thấy vợ đang ngồi chải chiếc áo vét lễ hội của anh, tâm trạng vô cùng mãn nguyện. Không ngẩng lên, chị hỏi:

- Anh quên gì à? May quá, con Minh đi Pháp về biếu anh bao thuốc mà em quên khuấy mất. Anh cầm đi mà hút đêm.

Đáp lại vợ, Vỹ rên hừ hừ:

- Tìm cho anh lọ dầu xoa.
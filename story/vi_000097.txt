Bây giờ là tháng năm. Ở đó, Đà Nẵng biển xanh và nắng vàng. Và hiển nhiên là nóng. Còn ở đây, bọn mình không có mùa hè. Sáng ra 9h trời đất mới tan sương. Buổi tối đi gác mình phải mặc cái áo ba ký và mang theo một lon lửa. Ngồi gác mà có thấy gì đâu ngoài một lớp sương mờ giống như ngồi trong mây. Và ánh điện hắt lên từ thị trấn giáp biên của Thái Lan cách chốt chừng 12km...

Tháng 5 năm 1979. 31 năm rồi. Lá thư đã cũ. Những cái tên lạ nghe mãi thành quen. 
Cương vẫn giữ lá thư này. Cùng những cảm giác đã có từ nó. Chị vẫn sống thật giản dị và nghiêm khắc. Học trò chị thì thào nhận xét: Khô cứng. Như những con số. Cương nghe được. Có lần cười với tôi: Em biết không? Những con số cũng đầy ắp tâm trạng. Tôi im lặng muốn ôm ghì lấy chị. Tôi biết chị vẫn luôn nhớ đến Khang.

Bao giờ cũng vậy. Nhìn Cương. Tôi thấy thương Cương đến ngộp thở. 

*
*      *
Trúc rủ rê tôi: Đêm nay Quyên đừng về nhà. Qua biển với Trúc đi! Trúc nói thêm, vội vàng như thể sợ tôi từ chối: Trưa mai Trúc bay rồi...

Ừ thì qua biển với Trúc. Ngủ lại trong ngôi nhà đó. Trong căn phòng đó.

Căn phòng có bức tường và những cánh cửa bằng kính rộng thênh nhìn ra phía biển. Ban ngày chói chang nắng. Những bức rèm màu tối dầy nặng căng dài. Ban đêm. Tấm rèm được cuốn gọn lại. Khung cửa kính được mở ra. Gió biển nhẹ lùa vào. Căn phòng bỗng trở thành một nơi chốn đáng ao ước. Cho tất cả những ai muốn đắm mình trong bí ẩn. Và trong những giấc mộng hoang đường. 

Trúc đã xây ngôi nhà này bằng tiền cô kiếm được sau rất nhiều năm ra đi. Để nuôi giữ những ký ức đẹp? Để có một nơi chốn đi về? Một người thân của Trúc đã giúp cô trông nom nó. Khi Trúc ở bên kia. Trong một căn phòng chật hẹp thiếu tiện nghi và nhiều bóng tối. Người thân của Trúc được phép sử dụng ngôi nhà cho cuộc sống của mình. Trừ căn phòng đó. Căn phòng có bức tường bằng kính trông ra phía biển. Căn phòng chỉ được dọn dẹp tinh tươm mỗi dịp Trúc về thăm. 

Đêm đã khuya. Tôi vẫn mang cảm giác xa xôi khi đọc lại lá thư cũ kỹ đặt trên bàn viết của Cương. Một cảm giác thật lạ kỳ. Cứ như không phải lá thư đó Khang viết cho Cương mà một ai đó viết cho chính tôi: 

Bây giờ là tháng năm...

Tháng năm. Đà Nẵng. Tôi và Trúc. Trong căn phòng có khung cửa kính rộng thênh nhìn ra phía biển. 
Những con sóng không ngưng nghỉ. Đêm tối. Không nhìn thấy gì. Phía biển thẫm đen. Trời âm ỉ nóng. Áp thấp đang đâu đó ngoài xa kia. Tôi bảo Trúc tắt hết đèn. Hai đứa nằm trong bóng tối. Tiếng sóng rì rào khe khẽ. Tôi nói: Như thể đang trên một con tàu. Giữa đại dương bao la. 

Im lặng hồi lâu. Rồi Trúc hỏi: Những câu thơ đó là của ai? Giọng Trúc trĩu nặng: Những câu thơ Quyên đã đọc hồi đó, trước Tòa thị chính? Tôi mụ mị. Hoàng hôn vừa tự tử/ Máu đào loang trên sông?.. Chịu, không nhớ. Im lặng một lát nữa. Tôi hỏi Trúc: Trúc vẫn nhớ Tùng sao? Ừ. Đã quá lâu rồi mà. Ừ, nhưng vẫn nhớ. 

Tùng. Sông Hàn. Những trái kiền kiền khô nở bung như những hoa sen nâu. Tùng. Bãi biển này. Khi đó là một làng chài. Những đốm lân tinh trên cát một đêm mùa hạ.

Mùi biển mặn theo khung cửa len vào phòng. Có điều gì đang xảy ra ngoài khơi xa kia? Tâm trí tôi mông lung. Giờ này, chắc chị Cương đang đau lắm. Những thay đổi của thời tiết. Lá thư cũ mèm kia. Và cảm giác xa xôi da diết buồn.
*
*    *

Đó là những ngày cuối cùng của tháng chạp năm 1985. Tiếng pháo tất niên ì ầm khắp nơi. Các con đường trong phố đỏ rực. Pháo Nam ô Đà Nẵng nổ giòn tan như những chuỗi cười ngày thơ trẻ. Tôi và Trúc ngồi trước Tòa thị chính. Chỗ bãi cỏ xanh dẫn xuống sát mé sông. Tôi vừa về thăm nhà. Hai đứa có bao chuyện muốn kể cho nhau nghe. 

 
 Minh họa: Lê Huy Quang


Chiều xuống muộn. Mặt sông loang loáng đỏ. Tùng lang thang đâu đó dọc bờ sông. Dừng lại. Rồi ngồi xuống bên Trúc. Tôi nhìn Tùng dò xét. Mái tóc Tùng cắt rất cao. Chân tóc cạo trắng chỉn chu nghiêm túc. Giọng nói trầm quyến rũ. Trước Tùng, Trúc choáng ngợp và ngay lập tức quên mất sự hiện hữu của tôi. 

Những ngày cuối năm của Trúc bỗng chìm trong một không gian mê đắm kỳ lạ. Không gian của tình yêu đột ngột, gấp gáp, tràn ngập âu lo giữa Trúc và Tùng. 

Sau này rất nhiều lần tôi băn khoăn: Tại sao dây đàn nhà Trúc lại đứt? Đúng vào hôm mùng một Tết? Tôi tự dằn vặt, như thể chính tôi chứ không ai khác làm nên tất cả những chia xa. 
Khi đó tôi đang hát: Kiếp nào có yêu nhau, thì xin hẹn đến mai sau. Hoa xanh khi chưa nở. Tình xanh khi chưa lo sợ. Bao giờ có yêu nhau. Thì xin gạt hết thương đau... Tùng nói: Những ca từ ma mị. Tôi làm đứt dây đàn nhà Trúc. Dây sol. Có phải đó là điềm ly biệt? Buổi gặp mặt đầu năm bỗng dưng lặng ngắt. 

Cũng chính dịp Tết năm đó, Cương nhận được thư của Khang. Những lá thư viết từ tháng 5 năm 1979, tháng 10 năm 1980... đã loanh quanh đâu đó để đầu năm 1986 mới đến được tay người nhận. Lúc đó Khang đã không còn. 

Cương nói: Tại sao Khang phải đi xa đến thế để chết?

Thư Khang viết: Trên chốt có cái hồ bằng đá. Mùa mưa đựng đầy nước. Có mấy con ếch kêu âm u. Mấy hôm trời lạnh trùn ở đâu bò ra đầy đường rồi chết thúi. Mỗi con trùn to bằng ngón tay, dài cả hai gang tay. Mấy thằng ở trung đội vận tải có thời chuyên đi trầm nói: Thứ này đói mà gặp là đỡ lắm. Ăn rất bổ.  
...

Tình hình rất căng. Mình không biết bao giờ mới được về. Dường như chưa ai có được kỳ nghỉ phép!
...
Ước gì được như thời còn học phổ thông tụi mình có được một đêm tụ tập lại, học thì ít mà chơi thì nhiều. Nhớ lúc đó Cương cứ sốt ruột: Không học gì à? Không học gì à?

Cương nói với tôi: Trong cuộc đời, duy nhất chỉ một người có thể mở cánh cửa bí ẩn để khơi gợi những cảm xúc và cả những ký ức nào đó trong một con người. Và Cương đã thật sự tin như thế. Mất Khang, Cương cũng đã gặp gỡ nhiều người. Nhưng những lần gặp gỡ và những câu chuyện cứ trôi tuột đi. 
Trôi qua Cương. Trôi xa mãi. Cánh cửa nào đó trong Cương vẫn đóng. 

Khang và những lá thư của Khang thì khác. Những câu chữ, những hình ảnh, những lời nhắn nhủ bình thường với tất cả mọi người lại có ma lực thay đổi cảm xúc Cương, nhấc bổng Cương, mang Cương đi, ném vứt Cương đến những chân trời lạ lẫm. Phố chợt khác. Bài hát chợt khác. Không khí Cương thở cũng khác. Và người đối diện Cương cũng khác đi. Cương nói: Một ánh sáng mờ lạnh tỏa lan. Và những khát khao thức dậy. 

Nhiều lúc tôi đoán chắc Cương nhầm lẫn. Nhưng tôi không khuyên bảo được chị. Vả lại với Cương, tôi bao giờ cũng chỉ là đứa em vụng dại mà chị đã phải thay mẹ chăm sóc từ khi tôi còn nhỏ.  

*
*    *
Hơi thở của Trúc đã đều đặn. Tôi không ngủ được. Tôi nằm buồn bã. Một thời khắc nào đó trong đêm tôi thấy mình mê đi. Một vài giấc mơ đến với màu đen và tiếng khóc than. Một đồ vật của một ai đó đã chết cứ tìm cách xuất hiện và bám theo tôi. Rồi những đứa trẻ trần truồng. Là đứa trẻ sơ sinh con của Trúc và Tùng hồi đó. Hay đứa trẻ bụ bẫm trong bức ảnh treo ở phòng Cương – một mong ước không bao giờ đạt đến. Tôi không nhận biết được rõ ràng. Nhưng cảm giác lạnh toát và rủn mềm thì rõ rệt trên cơ thể tôi khi tôi thức tỉnh. Tâm trí tôi lại mông lung. Tôi nghĩ về những con người sau những tổn thương đã dựng lên quanh họ những bức tường. Hay đã đan dệt quanh họ những tấm kén. Để thu mình vào bên trong. Để trốn chạy? Để tự nuôi dưỡng chăm sóc bản thân? Đó là điều cần thiết. Tôi biết thế. Nhưng cần thiết hơn, quan trọng hơn, tôi nghĩ là những con người đó phải có đủ sức mạnh và sự tự tin để đến một ngày. Phá đổ bức tường đó. Xé rách cái kén đó. Để bước ra. 

Những bức tường cao ngất? Đêm nay tôi thấy tôi như đang lút chìm vào gạch đá? 
*
*    *

Những cái móng tay của Trúc luôn trụi lủi vì bã cà phê, nước chanh và xà bông rửa ly tách. Quán cà phê nhỏ xíu nhà Trúc ở đường Lê Đình Dương, gần Cổ Viện Chàm. Chủ yếu bán cho những người lao động nghèo. Không nhạc. Không cây cảnh. Không tranh treo tường. Chỉ có Trúc và mẹ. Và những bàn ghế gỗ. Và các thức uống bình thường. Trúc pha chế cà phê, nước chanh, sữa đá đập theo cách mà cô nghĩ là ngon. Và những người khách bằng lòng với những thức uống cô mang ra cùng với nụ cười thơ dại rất đỗi nao lòng. 

Đêm mùa xuân. Biển động. Tùng nói: Anh sẽ từ bỏ tất cả. Lý tưởng. Gia đình. Công việc. Chỉ để có được em. Trúc đã tin nhưng cô biết cô sẽ không để Tùng làm điều đó. Trúc không đành lòng để Tùng vì cô mà đánh mất những gì anh đang có. Và sẽ có. Ngày một nhiều hơn. Cô bảo anh hãy đi đi. Hãy trở về. Hãy quên Trúc. 

Chính ở đây. Trên bãi biển này. Khi đó còn là một làng chài. Trúc đã viết trên cát những cái tên. Trúc và Tùng và tình yêu. Trúc tan chảy trong hạnh phúc khi Tùng tràn ngập cô như sóng biển man dại tràn ngập bờ cát. 

Rồi Tùng đột ngột vắng bặt. Trúc đau đớn. Nhưng cô không một lần tìm gặp Tùng. Không một lần đi tìm sự thật. Cô chấp nhận sự chọn lựa đó của Tùng. Chấp nhận ý nghĩ là anh đang bình yên ở một nơi xa đó. Anh có gia đình. Có vị trí cao trong xã hội. Cô nuôi dưỡng ý nghĩ trong tim anh luôn có cô. Đêm hôm đó. Bờ biển. Những cái tên viết vội. Ánh lân tinh òa vỡ trên cát ướt. Chỉ vậy là đủ để cô yêu anh suốt một đời. 

Trúc không biết rằng Tùng không ở đâu xa. Gia đình anh ta lúc đó không ở miền Bắc xa xôi. Và bây giờ cũng vậy. Tùng đang công tác ngay ở đây. Trong thành phố này. Anh ta có vợ. Không chỉ một vợ. Và chân tóc vẫn cạo trắng chỉn chu nghiêm túc thế. Giọng nói vẫn trầm ấm thế. Ánh mắt khi giao tiếp với mọi người vẫn tạo nên một cảm giác tin cậy yêu thương thế.

Số phận phải chăng đã ưu đãi Trúc trong chuyện này. Khi chọn cho Trúc một nơi chốn xa xôi để sống. Để Trúc gần như không có cơ hội gặp lại người của ngày xưa. 

Trúc trở mình vươn tay chạm vào tôi. Quờ tìm những ngón tay tôi nắn nhẹ: Không ngủ à? Quyên xem này, đến làn da cũng đã già nua rồi! Đừng nghĩ ngợi nhiều nữa. 

Tôi bỗng muốn nói: Hay là Trúc đừng đi nữa! 
*
*    *

Đã bao lần tôi muốn đốt những bức thư cũ kỹ trên bàn viết của chị Cương. Đã bao lần tôi muốn kể cho Trúc nghe về Tùng. Tôi muốn chị Cương bước ra khỏi không gian xa xôi mê mị đó. Tôi muốn Trúc ở lại đây. Mỗi buổi sáng mở tung những tấm rèm dầy nặng này. Để ban mai tràn ngập căn phòng. Tôi muốn tất cả có đủ can đảm để nhìn thẳng vào thế giới này một lần. Đầy rách nát đau đớn. Sau đó ảo vọng không còn. Nhưng biết đâu yên bình sẽ đến. 

Nhưng tôi đã không làm được điều tôi muốn. Cũng như tôi đã không bao giờ có thể sắp xếp được một cuộc đi. Chỉ hơn tiếng đồng hồ ngồi trên máy bay. Chạm đất là tôi có thể chạm được vào giấc mơ của tôi. Gương mặt người tôi yêu dấu. Do dự là bản chất của những con người luôn sợ hãi đau đớn như tôi. Tôi chỉ xứng làm một con ốc mượn hồn nấp trốn trong chiếc vỏ tìm được. Nghe tiếng động lại rụt sâu vào. 

Tôi quay nhìn khung cửa. Tấm rèm mở rộng nhưng không một chút gió. Bên ngoài là bóng đêm và những dồn nén u ẩn. Tôi thấy mềm lòng. Đây là thiên đường Trúc nuôi giữ và không muốn mất. Là nơi Trúc sống trong giấc mơ. Có gì giống những lá thư trên bàn viết của chị Cương không? 

Giọng Trúc bỗng dịu nhẹ như hơi thở: Quyên đừng cố chấp nữa. Được không? Hãy để cho họ được gặp nhau. Tôi giật mình: Ai? Những nhân vật. Không phải con người chỉ còn nơi đó để gặp nhau thôi sao? Họ đã xa cách nhau mãi trong cuộc đời rồi. Quyên đừng để họ cách biệt nhau cả trong những trang viết của mình.

Không dưng tôi bật cười. Trúc cũng cười. 

Tiếng cười nhẹ nhõm của Trúc dẫn dắt tôi về những ngày cuối tháng chạp xưa. Các con đường trong phố đỏ rực. Pháo Nam ô Đà Nẵng nổ giòn tan. Hồi đó tôi đã mơ sẽ có một ngày chụp những tấm ảnh thật đẹp về những con phố yên lặng vắng vẻ thơm trầm nhang và tràn ngập xác pháo trong buổi sáng sớm tinh sương ngày đầu tiên năm mới. Tôi đã không kịp và sẽ không bao giờ làm được điều đó. 
Trúc đã đúng chăng? Có thật nhiều điều đã trôi xa rồi. Tùng, Trúc, chị Cương, Khang và cả tôi.... Như thể tất cả đã bước lên những con tàu và những con tàu đó đã ra đi. Đi thật xa. Không cách gì quay lại được. Những bức tường. Những tấm kén. Những gạch đá nát tan. Tất cả có thể đã không còn tồn tại từ lâu. 

Tôi nhắm mắt nghe tiếng tôi thì thầm trong bóng tối: Nỗi buồn da diết hơn/ Không vì thiếu hụt một người khác/ Mà vì những lãng quên của chính mình. Vì những ngày dịu dàng khôn tả/ Đã từng hiện hữu... 
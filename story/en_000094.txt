Rabindranath Tagore (1861-1941) là nhà thơ cận đại nổi tiếng của Ấn Độ. Ông là nhà văn, nhà hoạt động xã hội và là một triết gia Bà La Môn. Với 50 tập thơ, 12 tiểu thuyết dài và vừa, 42 vở kịch, hơn 100 truyện ngắn, trong đó có truyện “Người Kabul” rất nổi tiếng, Tagore là người châu Á đầu tiên nhận giải thưởng Nôben văn chương năm 1913.
 
Đứa con gái năm tuổi Sakula của tôi suốt ngày líu lo như khiếu. Tôi tin là con bé sẽ chẳng khi nào trầm lặng, buồn bã được. Mẹ cháu thường cáu vì điều đó, nhưng tôi thì không. Tôi coi việc Sakula trầm lặng, suy tư là rất không bình thường và không sao chịu nổi khi nó im lặng, ỉu xìu chẳng nói năng gì. Vì vậy, hai cha con tôi thường trò chuyện với nhau rất rôm rả.

Chẳng hạn như, sáng hôm đó, trong khi tôi đang viết chương 17 của một tiểu thuyết mới, sakula chạy vào phòng, đặt bàn tay nhỏ xíu của cháu vào tay tôi, nói:

- Ba ơi, Unôsahin bảo, trong đám mây có con voi, khi nó phun nước ra mũi thì trời mưa đấy, đúng không ba?

Tôi còn đang nghĩ cách trả lời thì con bé lại liến thoắng chuyển sang chủ đề khác:

- Ba ơi, ba với má quan hệ với nhau thế nào hả ba?

Tôi bất giác lẩm bẩm: “Về mặt pháp lý, má con là người em gái thân yêu nhất của ba!”, nhưng không nói rõ ra mà ngẩng lên bảo “Con đi chơi với Unôsahin đi! Ba đang bận mà!”

Cửa sổ phòng tôi mở ra phố.Thằng bé Unôsahin đang chơi bên ngoài cửa sổ. Tôi bảo Sakula ra đó chơi. Đang suy nghĩ để viết tiếp, con bé bỗng chạy đến bên cửa sổ bảo : “Ba ơi, có một người Kabul! Có một người Kabul đấy!” Tôi nhìn ra cửa sổ, quả nhiên có một người Kabul đang đi tới. Anh ta mặc bộ trang phục đặc trưng cửa người Apganistan bẩn thỉu, rộng thùng thình, quấn chiếc khăn to sụ trên đầu, vai đeo túi xách, tay cầm mấy hộp nho khô.

Tôi không biết con gái tôi có cảm tưởng gì khi nhìn thấy con người đó, nhưng con bé bắt đầu gọi anh ta. “Ôi chào, tôi nghĩ, anh ta sắp đến kìa, chuyện viết lách, hãy dừng lại đã”. Ngay lúc đó, người Kabul ngước mắt nhìn bọn trẻ khiến con bé sợ hãi bỏ chạy vào nhà với mẹ cháu. Con bé lơ mơ nghĩ rằng, trong chiếc túi mang trên vai của người đàn ông kia thế nào cũng có vài ba đứa trẻ như nó. Lúc đó gã tiểu thương kia đã bước vào nhà, mỉm cười chào tôi.

Dù đang nóng lòng hoàn thành chương 17, nhưng tôi nghĩ, anh ta đã tới, mình cũng phải dừng công việc lại, mua vài thứ đồ cho anh ta vậy. Tôi mua vài thứ và bắt đầu nói chuyện với anh ta.

Khi sắp đi ra, người Kabul hỏi:

-Thưa ngài, cô bé vừa rồi đâu ạ?

Tôi nghĩ, con bé Sakula  không nên sợ sệt một cách vô cớ như vậy, nên cho gọi cháu ra. Con bé đứng sát bên tôi nhìn người Kabul và chiếc túi của anh ta. Anh ta cho con bé mấy quả nho khô, nhưng cháu không nhận, chỉ càng nép sát vào người tôi, nỗi băn khoăn e ngại của nó càng tăng thêm.

Đó là lần đầu tiên Sakula và người Kabul gặp nhau.

Nhưng chỉ mấy ngày sau, vào một buổi sáng nọ, khi tôi sắp đi ra ngoài thì tình cờ phát hiện con bé Sakula đang ngồi trên chiếc ghế băng trước cửa, còn người đàn ông Kabul kia lại ngồi bên chân con bé nói cười rất vui vẻ. Con bé Sakula xưa nay ngoài tôi ra, chưa bao giờ cháu gặp một ai lại nhiệt tình, chăm chú nghe cháu nói chuyện như vậy. Trên vạt áo của cháu là những quả hạnh nhân và nho khô, quà tặng của người Kabul cho con bé.

- Sao anh lại cho con bé những thứ ấy? Tôi hỏi và đưa cho anh ta đồng bạc. Anh ta có vẻ không bằng lòng, cầm lấy đồng bạc bỏ vào túi.

Một lát sau khi trở về, tôi nhận ra chính đồng bạc đó đã gây phiến hà cho con bé. Bởi vì, anh chàng tiểu thương người Kabul kia đã cho Sakula đồng bạc đó. Mẹ cháu nhìn thấy, truy hỏi:

- Con lấy đồng bạc đó ở đâu, Sakula?

- Người Kabul cho con đấy mẹ ạ! Sakula vui vẻ nói.

- Người Kabul cho con ư? Mẹ con bé sợ hãi kêu lên – Sakula! Sao con lại nhận tiền của ông ấy hả?

Đúng lúc đó tôi bước vào cửa và cứu con bé một “bàn thua” trông thấy.

Tôi phát hiện ra hai chú cháu không chỉ gặp nhau một hai lần. Người Kabul dùng trái cây và nho khô, những của “hối lộ” có sức hấp dẫn lớn đối với bọn trẻ, làm cho con bé từ chỗ sợ hãi lúc đầu, bây giờ  lại trở thành bạn thân của nhau.

Hai chú cháu thường trò chuyện với nhau rất vui. Con bé tỏ ra rất thích thú khi gặp người Kabul. Sakula thường tươi cười vui vẻ ngồi trước mặt người Kabul, nhìn thẳng vào anh ta hỏi:

- Người Kabul! Trong túi của chú có những gì thế?

Anh ta thường dùng giọng mũi của người miền núi, đáp:

- Một con voi to tướng!

Con bé cười tít mắt. Người Kabul cũng không muốn bỏ lỡ cơ hội chọc cười Sakula, bèn hỏi lại:

- Sakula này, bao giờ thì cháu về “công công gia” đấy?

Đối với các cô gái Bănglađet, chuyện về “công công gia” (nhà chồng) họ đã biết từ nhỏ, nhưng gia đình tôi chưa bao giờ cho cháu biết chuyện này nên cháu rất mù mờ, song nó không hề để lộ điều đó, lém lỉnh hỏi: “Thế chú đã đến đó chưa?”

Mọi người ai cũng biết mấy từ “công công gia” ấy còn có một  nghĩa khác. Đó là cách nói khéo của từ “nhà tù”, một nơi không cần tiêu tốn tiền bạc vẫn được “chăm lo” chu đáo. Gã tiểu thương thô lỗ này cho rằng con gái tôi ám chỉ nơi đó. Gã “hừm” một tiếng và tưởng tượng ra cảnh những viên cảnh sát thường vung nắm đấm với mọi người, nói to:  “Chú sẽ đập nát cái “công công gia” nhếch nhác ấy!” Nghe gã tiểu thương nói thế, Sakula cười lên đắc chí. Con bé không hiểu là người bạn lớn của nó cũng muốn chọc cười nó cho vui.

Những ngày đầu mùa thu là những ngày mà các bậc đế vương thời cổ đại thường chinh đông phạt bắc; sống ở một góc nhỏ Calcutta này, chẳng hề đi đâu nhưng đầu óc tôi cũng phiêu diêu khắp thế giới. Chỉ cần nghe tên một đất nước nào đó là tôi muốn bay tới đó ngay để ngắm cảnh, xem người. Khi nhìn thấy người Kabul kia tôi như lạc vào cõi thần tiên với những đỉnh núi trọc, vách núi dựng đứng cao chót vót, những con đường nhỏ hẹp người chỉ có thể bò qua. Dường như tôi cũng nhìn thấy từng đoàn lạc đà chở hàng, từng toán thương nhân quấn khăn trên đầu, có người còn mang theo bên mình những thứ vũ khí cổ quái từ trên núi đi xuống đồng bằng. Tôi hầu như nhìn thấy…nhưng ngay lúc đó mẹ Sukala bước đến bảo tôi “Hãy lưu tâm tới con người đó.”

Mẹ Sakula là một người đàn bà cực kỳ nhát gan. Chỉ cần nghe thấy một âm thanh nào đó trên phố hoặc nhìn thấy có người đi về phía mình là cô ấy lập tức đoán định rằng họ không thể là ai khác ngoài bọn ăn cướp, bọn say rượu, rắn độc, hổ báo, sâu bọ hoặc giả là bọn thủy thủ người Anh. Qua nhiều năm kinh nghiệm cô ấy vẫn không sao xóa được nỗi sợ hãi cố hữu của mình. Vì vậy, đối với người Kabul này cô rất nghi ngờ, thường nhắc tôi phải chú ý tới hành động của anh ta. Tôi chỉ cười nhưng cô ấy thì rất nghiêm túc nêu lên những rủi ro rất có thể xảy ra với con bé khi để nó tiếp xúc với người Kabul!

Trung tuần tháng giêng hàng năm, Ôsaman, người Kabul đó thường về nước một lần. Khi sắp lên đường anh ta thường rất bận vì phải đến từng nhà thu nợ. Nhưng năm nay anh ta vẫn cố dành thời gian đến thăm Sakula mấy lần trước khi về quê.  Hình như hai chú cháu có hẹn với nhau, anh ta chỉ đến thăm con bé vào buổi tối. Lúc đầu tôi cũng thấy ngài ngại nhưng về sau cảm giác ấy không còn nữa khi mà bao giờ con gái tôi cũng rất vui khi gặp người tiểu thương đó.

Một buổi sáng nọ khí trời rất lạnh, tôi đang ngồi trong phòng xem sách, những tia nắng nhẹ buổi sớm chiếu vào bàn viết lan tỏa lên người tôi khiến tôi cảm thấy thư thái dễ chịu. Lúc đó khoảng 8 giờ. Đột nhiên tôi nghe có tiếng cãi vã trên phố, nhìn ra thì thấy Ôsaman bị hai viên cảnh sát áp giải, đằng sau cả một đám trẻ con bám theo. Trên áo của Ôsaman có mấy vết máu. Trong tay viên cảnh sát có một con dao nhọn. Tôi chạy ra cửa ngăn họ lại hỏi xem có chuyện gì xảy ra. Qua sự bàn tán của mọi người tôi biết Ôsaman cãi nhau với một khách hàng của mình và làm ông ta bị thương. Lúc này người Kabul đó đang rất bực tức chửi mắng kẻ thù của anh ta. Bất ngờ, từ ban công nhà tôi Sakula xuất hiện, gọi to “Người Kabul! Người Kabul!” Khi quay lại nhìn con gái tôi, trên mặt Ôsaman chợt nở một nụ cười. Hôm nay anh ta không mang theo chiếc túi xách nên hai chú cháu không nói với nhau về đề tài “con voi” nữa, Sakula hỏi ngay đến chuyện thứ hai: “Chú đến công công gia chứ?” Ôsaman cười bảo: “Chú đang đến đó đây, cô bạn nhỏ ạ!”

Vì bị kết tội mưu sát, Osaman bị kết án 8 năm tù giam.

Thời gian trôi đi, Ôsaman bị mọi người quên lãng. Chúng tôi vẫn sống ở đó và làm công việc cũ, rất ít, thậm chí chẳng khi nào nhắc đến người Kabul đó nữa. Ngay như Sakula cũng dần quên người bạn vong niên của nó.

Năm tháng qua đi, vào một ngày mùa thu rực rỡ, chúng tôi chuẩn bị đám cưới cho Sakula.

Sáng tinh mơ hôm đó, mọi người đang tíu tít chuẩn bị cho lễ cưới thì có một người tiến đến đứng trước mặt tôi, cung kính thi lễ. Thì ra đó là Ôsaman, người Kabul dạo nào. Lúc đầu tôi không nhận ra anh ta. Anh ta không mang túi cũng không quấn khăn cao trên đầu và chẳng còn gì là chút thần khí như trước nữa, nhưng khi anh ta cười thì tôi nhận ra ngay.

- Anh về lúc nào thế Ôsaman? Tôi hỏi

- Tối hôm qua - Anh ta đáp - Tôi từ nhà tù đến đây.

Những lời này nghe rất chối tai. Tôi dự cảm đây chẳng phải là điềm lành, tìm cách từ chối…

- Ở đây đang có chuyện vui. Tôi rất bận. Mấy hôm nữa anh hãy đến được không?

Ôsaman lập tức quay người bước đi, nhưng anh ta chợt dừng lại trước cửa, chần chừ một lúc rồi nói:

- Tôi có thể gặp người bạn nhỏ của tôi một lát, được không thưa ngài? Chỉ một lát thôi mà!

Ôsaman cứ nghĩ Sakula vẫn như xưa nên đã chuẩn bị sẵn một ít nho khô, đó là những thứ mà một người đồng hương của anh cho anh chứ bản thân anh thì chẳng còn đồng xu nào để mua sắm nó!

Tôi nói:

- Gia đình chúng tôi đang có chuyện vui. Hôm nay anh không thể gặp ai ở đây được đâu!

Trên mặt Ôsaman lộ rõ vẻ thất vọng. Anh ta bất mãn nhìn tôi và nói “Tạm biệt” rồi quay gót.

Tôi cảm thấy mình hơi bất nhẫn, định gọi anh ta lại, không ngờ tự anh đã quay lại. Anh đến trước mặt tôi, đưa túm nho khô ra, nói: “Thưa ngài! Tôi mang thứ này đến cho cô bạn bé bỏng của tôi. Ngài có thể thay tôi trao món quà này cho cháu được không, thưa ngài?”

Tôi nhận lấy túm nho khô, đang định trả tiền thì anh ta nắm lấy tay tôi, nói:

- Ngài thật nhân từ, thưa ngài! Tôi sẽ nhớ ngài mãi mãi. Ngài không cần trả tiền cho tôi đâu. Ngài có cô con gái thật đáng yêu, ở quê tôi cũng có một đứa con gái như vậy. Tôi nhớ cháu quá  nên tặng con gái ngài chút quà này chứ đâu phải cần tiền ạ. Nói tới đây anh ta thò tay vào túi áo lấy ra một tờ giấy nhỏ, nhàu nhò, bẩn thỉu, trải ra trên bàn. Trên tờ giấy là một dấu lăn tay nhỏ xíu. Không phải là ảnh cũng không phải là bức họa, nó chỉ là một dấu lăn tay bằng mực đen đã phai mầu. Anh ta giữ mãi cái dấu lăn tay bé nhỏ của con gái trong 8 năm dằng dặc ngồi tù!

Nước mắt tôi lặng lẽ trào ra. Tôi quên hẳn anh ta chỉ là một tiểu thương   nhập cư nghèo khổ, còn tôi là…mà không, không phải…Liệu tôi có gì đáng tự hào hơn anh ta? Anh ta cũng là một người cha.

Chính dấu lăn tay của con gái người Kabul đó đã khiến tôi nghĩ tới Sakula. Tôi liền gọi cháu ra. Mọi người cố ngăn lại nhưng tôi không nghe. Sakula bước ra phòng khách. Cháu mặc bộ váy áo cưới màu hồng, giữa trán điểm một nốt son nhựa đàn hương, trang điểm thành cô dâu, e lệ đứng trước mặt người khách mới tới.

Nhìn cảnh tượng ấy người Kabul lộ rõ vẻ kinh ngạc. anh ta cảm thấy không còn tự nhiên như ngày nào với cô bé Sakula bé bỏng nữa. Nhìn Sakula một hồi lâu, Osaman  mỉm cười hỏi: “Cô bé ơi, cháu sắp đến công công gia của cháu phải không nào?”

Hiện tại Sakula đã biết công công gia là thế nào rồi, con bé đỏ mặt, e thẹn cúi đầu.

Tôi nhớ lại lần gặp nhau đầu tiên giữa con gái tôi và người tiểu thương Kabul này, cảm thấy buồn buồn. Sau khi Sakula vào nhà trong, Ôsaman thở dài khe khẽ và ngồi bệt xuống đất. Anh ta chợt nghĩ, suốt 8 năm đằng đẵng ở trong tù, con gái anh nhất định đã lớn lên, khác trước nhiều lắm. mà trong ngần ấy thời gian lẽ nào bản thân nó không phát sinh ra biến cố gì?

Tiếng nhạc của buổi hôn lễ vang lên, những tia nắng vàng ấm áp lan tỏa khắp nơi, Ôsaman ngồi đây miên man nhớ tới những dãy núi điệp trùng ở quê nhà…

Tôi đưa cho Ôsaman một tờ ngân phiếu, nói:

- Hãy về quê đi Ôsaman, về với đứa con gái yêu quý của anh đi! Chúc cho cuộc gặp gỡ giữa hai cha con vui vẻ! Đó chính là hạnh vận mà anh đã mang lại cho con gái tôi đấy Ôsaman ạ!

Do trích bớt một phần kinh phí cho Ôsaman, hình thức buổi hôn lễ của Sakula có giảm đi đôi chút. Không còn nhiều đèn điện trang trí nữa, tôi cũng  không thuê đội quân nhạc giúp vui. Cánh phụ nữ trong nhà thất vọng lắm nhưng tôi lại cảm thấy rất hài lòng: Buổi tiệc thật tuyệt vời, và vinh hạnh nữa, bởi lẽ, tôi đang nghĩ tới, ở nơi xa xôi kia sẽ có cuộc trùng phùng cảm động giữa một người cha hết mực yêu thương con với đứa con gái độc nhất của mình sau tám năm trời xa cách…
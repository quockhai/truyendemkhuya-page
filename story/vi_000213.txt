Vậy là một mùa xuân nữa lại về. Hoa đào đã lấm tấm đỏ. Mấy cây lê, cây mận quanh nhà như những cây tuyết trắng muốt. Rừng sa mộc trên đồi đội triệu triệu hạt sương, đứng im lìm thành đội quân trùng điệp. Mưa xuân bay bay ngoài mái hiên. Từng đợt gió âm ấm tràn về… Sáng nay chỉ còn mình Mây ở nhà. Mây phải trông vạc rượu Tết cho bố.

Năm nào nhà Mây cũng nấu hai vạc rượu. Rượu ngô ủ men lá thơm lừng, chảy suốt một ngày một đêm mới cạn. Lũ em đi học. Bố mẹ dắt ngựa sang bên ngoại từ sớm. Bên ấy, nhà ông bà đã thịt lợn Tết. Ở đây, người ta ăn Tết sớm. Đầu tháng Chạp đã có nhà thịt lợn. Mỗi nhà một con, không nhà ai chung nhau. Như thế năm mới sẽ được no đủ, may mắn. Chỉ có ngày thịt lợn thì gọi anh em, con cháu đến ăn một bữa. Nhiều nhà thịt treo chật trên giàn bếp, mỡ đầy hai chum ăn quanh năm, đến khi thịt con lợn của năm sau vẫn chưa hết…

 
 Minh họa: Đào Quốc Huy
Mây ngắm những đám mây bay ra từ khe núi. Đám mây mỏng mảnh như chiếc khăn trắng, uốn lượn, rồi tan theo gió, mất hút. Mây ước mình biến thành đám mây ấy được tự do giữa núi rừng này. Đã hai mùa xuân, Mây không còn cái cảm giác nôn nao mong ngóng Tết đến nữa. Mỗi cái Tết qua đi là ngày Mây phải về nhà Ban càng gần. Mây sẽ không được ở cùng với bố mẹ, với lũ em. Mây phải về nhà người ta, làm con trâu nhà người ta. Đời làm dâu, lại là dâu gán nợ thì tủi cực lắm. Mây thấy sợ nên Mây không mong mùa xuân đến, không mong cái Tết về. Nghĩ đến những ngày tháng phía trước của mình, nước mắt Mây cứ trào ra.

- Con Chẳm…Con Chẳm…

Mây giật mình bởi tiếng kêu dưới chân cầu thang. Sao Răm lại hốt hoảng thế này? Không lẽ có chuyện gì sao? Nó nhắc đến con Chẳm. Thì vừa hôm trước cả bọn đưa nó về nhà thằng Sính bên Pờ Ly Ngài mà. Chắc chưa quen chồng, nhớ nhà rồi trốn về với bố mẹ đẻ thôi!? Răm vẫn trân trân nhìn Mây. Rồi nó lao lên trên sàn ôm ghì lấy Mây thổn thức:

- Con Chẳm…

- Nó có chuyện gì? - Giọng Mây run run.

- Nó …chết rồi!

Mây chết lặng. Trời ơi, sao có thể thế được!? Mới hôm trước nó còn đẹp như một bông hoa. Chiếc áo chàm nó mặc bó sát người, hàng cúc bạc hình hoa mua lấp lánh, kiềng bạc sáng choang. Nó ngồi trên lưng con ngựa bạch. Thằng Sính dắt ngựa tít mắt cười. Sao nó lại chết?! Chẳm ơi, sao mày lại bỏ đi? Hay con ma rừng bắt mày, con ma núi bắt mày?

Mây ôm Răm, hai đứa cùng nức nở. Hai đôi vai thon rung lên từng nhịp. Vậy là con bạn thân, đẹp như con chim quý trên rừng đã chết…

Lũ con gái ở bản Tà Chải này cái tuổi con gái qua nhanh, cái phận đàn bà mau tới. Cha mẹ nhận đồ sêu của người ta khi mới chín mười tuổi. Đến mười lăm mười sáu ăn chưa biết no, nghĩ chưa biết khôn đã về làm dâu nhà người. Rồi suốt một đời ru rú trong xó buồng nhà chồng, không dám bước qua ngưỡng cửa nhà chồng. Đẻ cho nhà chồng một lũ con như gà như vịt. Bầu vú chưa căng chín tròn đã nhàu nhĩ bởi tay chồng. Hai cái đầu vú thì nhằng nhằng sứt sẹo vì răng của cả đống con nghiến rứt. Suốt ngày cắm mặt trong nhà, trong bếp nấu rượu cho chồng uống, lùi lũi trong chuồng băm cỏ cho trâu bò dê ngựa nhà chồng ăn…

Bao đời nay cái phận đàn bà như thế rồi. Không khác được. Có đau, có buồn cứ lấy cái gương mẹ chồng, chị dâu ra mà nhìn. Rồi thành quen. Chưa người đàn bà nào dám bước qua ngưỡng cửa nhà chồng để về nhà mẹ đẻ. Khổ quá thì cứ cố cắn răng mà chịu. Nhục quá thì làm nắm lá ngón. Chết cũng phải ở nhà chồng, làm ma nhà chồng.

Sao con Chẳm phải chết cơ chứ? Nó là đứa con gái đẹp và ngoan nhất nhì bản này. Người trong vùng bảo Tà Chải có hai bông hoa đẹp, hai con chim quý. Trai Tà Chải suốt ngày uống rượu ngô, không chịu làm ăn gì, chắc không giữ nổi hoa đẹp, chim quý. Trai bản khác sẽ đến hái hoa, bắt chim đi thôi. Không có bàn tay lạ hái hoa thì hoa đẹp cũng tự héo. Không có bàn tay lạ bắt chim thì tự chim quý nó cũng bay đi. Hai bông hoa đẹp, hai con chim quý ấy là Mây và Chẳm. Vậy mà Chẳm chết. Mây thấy tim mình như có lũ ong đất hàng nghìn con đang bu vào. Những giọt nước mắt cứ tràn ra, giàn giụa.

Bố mẹ Chẳm nhận bạc nhà thằng Sính từ năm Chẳm mười tuổi. Pờ Ly Ngài có nghề chạm bạc nổi tiếng ở vùng này. Những chiếc vòng dày, sáng lấp lánh mê hoặc bố mẹ lũ con gái trong vùng. Khuyên tai bạc làm loá mắt con gái đẹp. Thằng Sính hơn Chẳm một tuổi, nhưng nó bé, đen, thấp hơn Chẳm một cái đầu. Có lần nó đến nhà Chẳm cùng bố mẹ nó, Chẳm trốn biệt không thèm gặp. Mẹ đi tìm Chẳm, bảo: “Con gái ngoan, mày phải nghe bố mẹ chứ. Lấy chồng Pờ Ly Ngài chỉ đẻ và nuôi con. Sướng lắm con gái à. Bố mẹ thương mày mới nhận bạc của người ta…”.


 Ảnh: Phạm Dụy Tuấn
Chẳm không thích thằng Sính. Đến năm nó mười tám tuổi rồi, có lớn người mà tính vẫn như thằng trẻ con. Một lần, Chẳm cùng lũ con gái bản kéo nhau xuống chợ Ngàm Bảng, vừa đến đầu chợ thấy đám người xúm xít lại. Cả lũ kéo nhau vào xem. Một đám thanh niên đang dùng chân tay, gậy gộc đánh liên tiếp vào hai thanh niên khác. Chẳm không tin vào mắt mình. Một trong hai kẻ bị đánh là Sính.

Sính cố vùng vẫy nhưng không thoát thân, đành chịu trận, tơi tả không khác gì mớ giẻ. Tự dưng Chẳm thấy thương nó. Chẳm xông vào ngăn lũ người lại. Chúng tản ra, cười hô hố. Trông man rợ và nham hiểm. “A, thì ra là chồng của Chẳm đấy à?”. “Người đẹp như mày mà lấy nó cho phí cái đẹp đi”. “Thôi đưa nó về mà chăm không thì mang lên rừng mà chôn”.

Mấy thằng thanh niên ấy ở bản Nhùng. Cái bản giàu nhất trong vùng. Có lần chúng đã đến bản Chẳm rủ lũ con gái đi chơi. Chúng lại hô hố cười bỏ lại Chẳm bệt xuống bên Sính. Mấy đứa bạn hãi quá dạt vào quán thắng cố của lão Xèng chột. Sính nằm dài ra đất nắm tay Chẳm, trán nó rớm máu. Mấy người đàn bà đi qua liếc nhìn, ngoáy cái mông làm cho chiếc váy xúng xính: “Cho cái thằng xấu bụng chết”. Họ ném lại câu nói rồi bỏ đi.

Thì ra, Sính và thằng bạn tức bọn thanh niên bản Nhùng thường hay đến Pờ Ly Ngài ve vãn lũ gái bản. Xuống chợ, thấy ngựa của bọn nó hai thằng lấy dây dù buộc đuôi hai con ngựa, lại lấy cứt ngựa nhét vào quẩy tấu trên lưng ngựa. Bọn thanh niên bản Nhùng uống rượu trong quán nhìn thấy. Cơn sôi máu cùng với cái nóng của rượu làm Sính và thằng bạn ăn trận đòn tả tơi.

“Cái bụng xấu thế, nó đánh cho là phải thôi”. Chẳm vùng vằng bỏ đi vì xấu hổ. Chẳm không thấy thương thằng Sính như lúc nhìn thấy nó bị đánh nữa, mà thấy ghét đứa xấu cái bụng… Một ngày Chẳm ôm lấy Mây nức nở: “Mây ơi, tao sắp phải về nhà Sính rồi. Bố tao đã nhận con lợn đen, can rượu nếp nhà Sính mang đến. Nhận cả bạc nữa”. “Cứ về với nó đi. Nó cũng yêu mày mà!”. “Nhưng nó trẻ con quá, có sửa được không?”. “Sửa được mà. Nó thành đàn ông, thành bố trẻ con là nó khắc người lớn ra thôi. Có vợ nó mới không lêu lổng cùng lũ trai bản”. “Nhưng tao sợ nó lắm. Hôm trước nó nói nó phải nhanh cưới tao, không để cho bọn con trai bản mình, bản khác nhòm ngó”… Mây nhìn thấy mắt Chẳm như mắt con nai trên rừng đang bị lũ người săn đuổi. Chẳm bồn chồn lo lắng là đúng lắm. Lần Mây cùng Chẳm đi hái rau lợn bên suối Huổm, Sính từ đâu đi tới giật lấy tay Chẳm, nói: “Mày là vợ tao rồi, không được đi chơi với thằng nào, tao cấm!”. Thì ra nó ghen. Biết Chẳm, Mây đẹp bọn con trai các bản ngày đêm đến Tà Chải ve vãn. Chẳm vùng tay bỏ về, mắt thằng Sính ngầu đỏ. Mây nhìn cũng thấy sợ.

- Con Chẳm nó chết thật rồi. Nó ăn lá ngón tự tử. Tao sợ lắm! - Răm lại nấc lên.

Vừa chiều hai hôm trước, Mây, Răm cùng lũ bạn trong bản và mấy người nhà đưa Chẳm về nhà chồng. Ai cũng thấy vui vì Sính trông có vẻ hiền, tỏ ra quan tâm, chăm sóc vợ. Nó yêu Chẳm quá nên hay ghen đấy thôi. Nó lấy được Chẳm nó sẽ yêu, sẽ tốt với Chẳm. Từ Tà Chải sang Pờ Ly Ngài đi hai lần con ngựa dừng chân. Giờ chân Mây vẫn còn mỏi. Vậy mà Chẳm chết rồi…

Người nhà con Chẳm bảo thằng Sính đã giết vợ. Đêm trước, khi mọi người nhà Chẳm về hết, họ nhà nó còn đang ăn uống tất trên sàn, Sính mò vào buồng đòi Chẳm cởi váy áo cho nó ngủ. Từ trước đến nay, ở vùng này chưa người đàn ông nào đòi ngủ với vợ ngay đêm đầu tiên. Có người còn cho cả bạn gái của vợ ở lại với vợ hai ba ngày.

Chẳm chưa hết lạ lẫm, chưa hết buồn vậy mà nó đã đòi với Chẳm ngay lúc đấy. Chẳm co rúm người lại, ép sát hai đùi vào nhau, lùi sâu vào trong góc giường. Thằng Sính chồm lên, xé váy áo, cắn chảy máu môi Chẳm. Đau quá, tức quá, Chẳm co chân đạp vào bụng Sính. Nó hộc lên, tát bôm bốp vào mặt, vào đầu Chẳm. 

 
 Ảnh: Phạm Dụy Tuấn
Ngoài nhà thấy vậy, mẹ chồng chạy vào: “Con dâu ngoan à, mày phải chiều chồng mày chứ. Lấy chồng ai cũng phải như thế thôi. Rồi sẽ quen mà!”. Thằng Sính vùng vằng bỏ ra ngoài uống rượu tiếp. Đêm ấy Sính không mò vào nữa. Nó ngủ lăn ngủ lóc ra sàn trong cơn say. Chẳm không chợp mắt được. Những giọt nước mắt cứ tràn trên gối. Chẳm thấy mình như sống trong một thế giới xa lạ. Không có tiếng cười của bọn bạn. Không có tiếng bước chân của bố mẹ. Không có tiếng cạc cạc của lũ vịt bầu…

Đêm đầu tiên ở nhà người yên tĩnh đến rợn người. Yên tĩnh như trong khu rừng ma. Chẳm vục dậy nhìn đêm qua cửa sổ. Vầng trăng non cong cong treo đầu núi, lơ lửng, mỏng manh như sắp rụng xuống cánh rừng u tối. Trăng đầu tháng sáng leo lét, lạnh lẽo. Làn sương mỏng mảnh bao phủ trên đỉnh núi nhọn hoắt - dãy núi cao nhất quê Chẳm - tựa làn khói từ cõi âm ti. Chẳm rùng mình.

Sáng. Mẹ chồng kéo Chẳm vào góc buồng, đưa cho nắm lá gì đó, bảo nhai. Chẳm làm theo. Đắng đến thụt lưỡi, Chẳm định nhè ra. Mẹ chồng lấy tay bịt mồm Chẳm. “Mày cố mà nhai, ngày xưa mẹ chồng cũng cho tao ăn cái này, bảo không thấy sợ, sẽ yêu chồng hơn”. Chẳm nhắm mắt nuốt, miếng bã lá trôi hết vào bụng, Chẳm thấy ngòn ngọt ở đầu lưỡi. Người Chẳm nóng ran, cứ ngất nga ngất ngư như say rượu. Hai bầu vú thì căng cứng lên… Cả ngày như thế. Chẳm vật vã trong buồng. Chẳm chờ mà không thấy Sính vào…

Đêm đổ ập xuống. Sính vào nhìn Chẳm như con hổ nhìn con nai rừng. Trải tấm vải trắng xuống giường, vật Chẳm ra. Chẳm cuốn theo từng cử động, từng nhịp sóng của Sính. Chẳm thấy như bồng bềnh trên đám mây buổi sớm. Rồi nhói buốt, buốt lên đến tận óc. Sính nằm vật ra thở. Một lúc sau, nó vùng lên, giật tung tấm vải. Nó soi lên mặt. Tấm vải vẫn trắng tinh, nhàu nát. Nó vứt tấm vải vào mặt Chẳm, hộc lên: “Con chó cái, mày đã cho thằng nào?...”.

Chẳm thấy nhục quá, đau quá. Vậy là nó nghi ngờ Chẳm vì không thấy dấu vết. Cái dấu vết mà đàn ông vùng này luôn được bố chỉ bảo từ khi mới lên mười, mười một. Nó còn rít lên bao tiếng chửi rủa. Chẳm biết làm gì, biết nói gì bây giờ? Nhưng Chẳm đã cùng ai bao giờ đâu. Sao nó cứ muốn Chẳm, bắt Chẳm phải có cái ấy. Ngày mai nó sẽ đem trả Chẳm, rồi bắt đền! Thế thì nhục lắm, con gái ở đây mà bị thế thì chỉ có nhảy xuống vực hay ăn lá ngón mà chết. Chẳm lao ra cánh rừng thâm u. Vầng trăng non như một nét mày sơn nữ chiếu sáng mờ ảo. Tiếng dế kêu ri ri lẫn trong tiếng thì thào của cỏ nghe thành tiếng nấc uất nghẹn.

Người đi làm nương sớm thấy Chẳm cứng đờ trên vạt cỏ. Hai má sưng vù, tím màu quả sim chín, miệng còn ngậm một cái lá…

Kể xong, con Răm lại nấc lên từng hồi.

Mây thấy lạnh toát cả sống lưng. Mây sợ. Rồi Mây cũng phải về nhà chồng thôi. Bố mẹ Mây đã ăn nhiều gà béo, uống nhiều rượu thơm của người ta rồi. Nhà Ban ở trong bản này. Mây không ưng cái bụng mỗi khi nghĩ sẽ phải lấy Ban. Ban không đen, không xấu nhưng nó kém Mây ba tuổi, chỉ bé bằng thằng em Mây. Vậy mà Mây phải làm vợ nó sao? Sao bố mẹ lại nhận gà, nhận rượu nhà Ban chứ?

Có lần Mây khóc hỏi mẹ, mẹ chỉ gạt nước mắt. Không hỏi được mẹ, Mây vùng vằng bảo sẽ trốn đi, không ở bản này nữa. Mây không lấy thằng Ban đâu! Mẹ hốt hoảng nhìn Mây, rồi quỳ thụp xuống chân Mây. Bố mẹ Mây đã nợ nhà Ban ba con trâu mộng từ ngày bố mẹ lấy nhau và ngày ông nội Mây chết. Nhưng đã nhận gả con cho nhau thì vẫn được nhận gà, nhận rượu vào ngày Tết và rằm tháng Bảy. Mây không lấy Ban thì nhà Mây phải trả sáu con.

Lệ bản này thế rồi, ai nuốt lời nói là bị phạt. Rồi bao nhiêu gà, rượu của người ta bố mẹ Mây đã nhận từ lâu, tính làm sao mà trả được. Mây phải lấy Ban thôi. Đàn bà bản này bao nhiêu người vợ già hơn chồng có sao đâu. Mây cũng sẽ như họ. Về nhà người ta làm từ sáng đến đêm năm này qua năm khác; đẻ con, nuôi con, nuôi cháu mấy chục năm rồi sẽ quen hết, quên hết thôi…

Đã qua Tết. Cái Tết năm nay qua nhanh thật. Mây chỉ quẩn quanh trong nhà, không đi chơi chợ, chơi hội lồng tồng. Xác hoa đào rụng tả tơi quanh vườn, khắp các lối đi trong bản. Cứ mỗi một làn gió đi qua là trăm ngàn cánh hoa hồng tươi trên cành chao liệng xuống đất, rồi trắng bệch, nhàu nhĩ, hoà vào trong bùn đất. Mây hay ngắm những cánh hoa ấy, rồi lại nghĩ đến Chẳm, đến mình.

Đã mấy lần buồn, nhớ Chẳm, Mây lén uống rượu một mình. Rồi ngủ thiếp đi. Mây hay mơ trong giấc men say. Mây thấy mình biến thành những dải mây trắng bồng bềnh. Những dải mây trắng muốt theo gió đi khắp núi đồi và đến một nơi thật xa lạ. Chưa bao giờ Mây thấy hay hình dung ra. Ở đấy có cánh đồng bằng phẳng chạy tới chân trời, những con đường thênh thang, mềm mại và những ngôi nhà mái đỏ au…

Nhưng có lần Mây thấy mình bị vứt xuống một cái hang sâu đen kịt, thăm thẳm. Mây rơi mãi, rơi mãi… Mây mụ mị không còn biết gì. Và rồi Mây thấy tim mình nhói buốt bởi những bàn tay đầy móng vuốt đang cấu xé, đầu óc quay cuồng trước những cái mặt ghớm ghiếc nhe răng ra cười… Mây bừng tỉnh, mồ hôi đầm đìa. Mây ngập lún trong cái cảm giác lạnh toát, run rẩy.


 Ảnh: Phạm Dụy Tuấn
Gần hết tháng Giêng, các cuộc vui chơi của người già, thanh niên, con trẻ giờ mới thực sự chấm dứt. Gió xuân ấm áp. Những sợi nắng vàng rải xuống núi đồi, nương ruộng ngày càng nhiều hơn. Cỏ mơn mởn cong lên chờ mưa. Lá cây rừng mỡ màng, căng tràn sức xuân. Nhà nhà chuẩn bị vào mùa đốt nương tra hạt. Nhưng cũng mấy ngày nữa nhà Mây mới bắt đầu vào mùa nên hàng ngày Mây đi cắt rau lợn và chăn đàn dê.

Đàn dê nhà Mây hơn chục con. Mây ghét tiếng lũ dê vào buổi sáng. Ghét nhất là tiếng be be của con dê đực già. Sáng sớm khi mọi người con đang ngủ, nó đã kêu be be, lồng lộn trong chuồng để dồn đuổi lũ dê cái và húc những con dê đực non. Có lần với cặp sừng nhọn, sắc như mũi dao nó đã húc lòi ruột một con đực non khi thấy con dê đó đang ve vãn một con cái trong đàn. Bố Mây đã phải dùng dao gọt sừng nó.

Mây thấy ngại ngùng, pha chút bực tức mỗi lần ra mở cửa chuồng dê. Con đực già luôn đứng chặn ngang giữa cửa. Nó giương cặp mắt hau háu nhìn khắp chuồng. Nó hếch chiếc mõm lên lộ ra mấy chiếc răng trắng, to, bộ râu dài dưới cằm rung rung thật đáng ghét. Không một con cái nào có thể qua cửa nếu không cho nó cưỡi lên lưng.

Mây nóng bừng mặt trước hành động của con đực già. Mây vung chiếc roi quất tới tấp vào đầu, vào lưng nó như cố xua đi cái hình ảnh ấy khỏi đầu. Vậy mà suốt cả buổi cái hình ảnh ấy nó lại không chịu ra khỏi đầu Mây. Mây thấy một cảm giác lạ, rồi thấy xấu hổ. Mây càng ghét tiếng be be của con đực già mỗi buổi sáng.

Ngôi trường bên bản Nậm Dế người ta sắp xây xong. Từ nay lũ em Mây không phải đi học xa nữa. Từ Tà Chải sang Nậm Dế chỉ phải qua một quả đồi. Giá ngày xưa Mây và lũ bạn có trường gần như thế để đi học thì chắc Mây cũng sẽ học giỏi, học được lên cao, rồi được xuống thị xã, thành phố dưới xuôi như lũ con gái thị trấn. Nhưng ở bản Mây chả đứa nào đi học hết cấp hai. Cô giáo đến bảo bố mẹ cho đi học.

Bố mẹ nói: “Nó như cái cây trong rừng, không thể có chân mà đi được. Nó như hòn đá trên núi, không có cánh mà bay đi xa được. Có học cũng thế thôi. Ở nhà giúp bố mẹ, rồi dệt váy áo và gối cho nhà chồng là vừa”. Cô giáo đến nhà nào bố mẹ cũng nói thế. Vậy là bọn Mây phải nghỉ học. Dáng đi mỗi ngày thêm chúi về trước vì cái quẩy tấu sau lưng mỗi ngày thêm cao lên.

Chiều tắt nắng. Màn đêm lân la mò về. Mây mang váy áo ra suối giặt. Con suối cuối bản Mây đến lạ, quanh năm nước ấm nóng như có người đun. Mây giật mình khi nhìn thấy một thân thể đàn ông. Người đó trắng trẻo, vâm váp quá. Những bắp thịt cuồn cuộn lên theo mỗi nhịp chân tay. Mây chưa nhìn thấy một người đàn ông nào có thân hình đẹp như thế.

Con trai, đàn ông trong bản Mây và mấy bản bên quanh năm chỉ vùi mình trong rượu ngô, rượu sắn, có người vẫn lén lút hút thuốc phiện nên chân tay như cành củi khô, đầu tóc rối xù, người thì hôi như cú. Có mấy khi họ tắm giặt. Đột nhiên Mây lại như nghe thấy tiếng be be và cái cảnh con dê đực già đứng trước của chuồng mỗi sáng hiện lên. Mây nóng ran cả người, toan chạy về.

Nhưng không hiểu sao Mây cứ đứng chôn chân không nhích đi được. Người đàn ông đã mặc chiếc áo lót vào. Anh ta ngoảnh lại. Hai tia mắt chạm vào nhau. Anh ta tiến đến hỏi Mây ở bản nào. Mây đỏ bừng mặt, chẳng nói được câu gì. Anh ta trông thật đẹp trai. Không giống con trai ở bản này. Anh ta nói đang làm công trình trường học bên Nậm Dế…

Chiếc áo trôi đi một quãng xa mà Mây mới biết. Hình ảnh khi Mây mới ra suối cứ choán hết tâm trí Mây. Dòng nước chảy qua các mỏm đá phát ra những tiếng như đang cười Mây. Mây tỉnh ra thì trăng đã lên cao. Vầng trăng non nằm nghiêng nghiêng trên đỉnh núi. Những ngôi sao như hàng ngàn mảnh bạc li ti, sáng lấp lánh mà người thợ bạc Pờ Ly Ngài nào đó vãi lên trời.

Đêm ấy Mây mơ. Giấc mơ làm sáng ra Mây đỏ mặt. Cả ngày Mây không làm gì được. Đầu óc cứ để đi đâu. Mây làm vỡ mấy cái bát khi mang đi rửa. Mẹ mắng: “Con gái vụng rồi về nhà người ta bố mẹ lại phải nghe chửi. Phải cẩn thận chứ!”.

Chiều về. Mây lại thấy nao nao. Có ngày không có váy áo để giặt Mây cũng đem một cái áo ra suối. Mây đã thấy trong người như có men rượu khi nhìn người ấy. Đàn bà trong bản thường nói với nhau: “Con gái mà cứ thích nhìn đàn ông tắm là con gái hư”. Thế là Mây hư thật rồi. Mây đã nấp sau tảng đá to ngắm người ấy nhiều lần. Nhiều đêm Mây lại mơ. Những giấc mơ từ ngày bé Mây chưa gặp.

- Mây đẹp thật! Con trai bản này chắc nhiều người mê lắm đây.

Mây giật mình. Thì ra là anh ta. Sao anh ta lại biết tên Mây. Nắm cỏ rơi khỏi tay Mây. Mây lúng túng. Nhưng rồi ánh mắt thật hiền của anh ta làm Mây trấn tĩnh lại. Anh ta tên là Hùng, người mãi tận vùng xuôi. Hùng bảo làm xong công trình này sẽ sang các bản quanh đây làm công trình khác. Hùng hỏi Mây nhiều thứ lắm, Mây chẳng nhớ hết, cũng không nhớ Mây đã nói những gì. Hùng nói thật nhiều. Những lời mà cái tai Mây thích nghe. Mây chưa thấy con trai ở đây nói với Mây như thế bao giờ.


Ảnh: Phạm Dụy Tuấn
- Mai tôi đến nhà Mây nhé!

- Không được đâu. Mây có chồng rồi, người trong bản nhìn thấy cười cho.

- Thế thì tôi đến thăm bố mẹ Mây.

- Tùy anh Hùng thôi. Đừng để người bản cười Mây.

Mây sợ Hùng đến nhưng Hùng đã đến thật. Hùng đi xe máy, phi thẳng vào chân cầu thang. Con chó đen nhà Mây lao ra nhe hàm răng trắng ởn sủa ông ổng. Lũ vịt bầu chạy toán loại lao cả vào cột dưới sàn kêu cạc cạc. Lũ dê cái hiền thế tự dưng kêu be be… “Không phải đến thăm Mây, tôi đến mua dê cho công nhân. Tôi thích món tiết canh dê và rượu tiết dê”. – Hùng nói vừa chỉ tay vào con dê đực non.

Bố mẹ Mây nhìn Hùng từ đầu đến chân. Đàn ông ở đây, nhất là con trai không ai dám ăn tiết canh và uống rượu tiết dê, mà lại là dê đực. Cái tục kiêng ấy nó gắn với một câu chuyện đau lòng từ xa xưa, Mây đã loáng thoáng nghe người già nói với lũ con trai mới lớn. Mây không tin vì thấy có lần lũ trai bản lấy trộm tiết dê pha rượu uống rồi có sao đâu! Mây lại đỏ bừng mặt, quay đi khi Hùng nhìn.

Bố mẹ Mây vui cả buổi tối khi bán được con dê gầy với giá cao. Còn Mây thì ngất ngây trong cái bụng. Hình như có lúc Mây hát. Trái tim Mây đang hát thật. Hát rất khẽ, rất êm.

- Mây đẹp lắm, con trai bản này chắc nhiều người khổ vì Mây.

- Không, anh Hùng nói sai rồi. Mây xấu lắm, con gái vùng xuôi mới đẹp.

- Mây đẹp thật mà. Tôi cũng khổ vì Mây đây này. Tôi muốn ở lại đây, Mây có đồng ý không?

- Ở đây khổ, nghèo, người xuôi có bao giờ ở lại đây đâu.

- Nhưng tôi muốn ở lại thật mà. Từ khi gặp Mây là tôi muốn ở lại. Thật đấy, nói sai tôi bị thú rừng ăn thịt, đá núi đè chết…

Mây ngỡ ngàng trước chiếc vòng bạc Hùng đưa lên trước mặt. Chiếc vòng bạc đẹp quá, đẹp hơn tất cả những chiếc vòng Mây đã nhìn thấy. Hùng bảo hôm qua  phải sang tận Pờ Ly Ngài để mua. Chiếc vòng vừa xinh, cứ như làm để cho riêng Mây. Ánh mắt Mây chứa cả bầu trời sao lung linh, đựng cả một biển rượu men lá sóng sánh sóng sánh. Mây như bay lên…

Hùng bế Mây lên gốc thông bên tảng đá. Cả hai đê mê, lăn theo tiếng thông reo. Hùng sẽ trả hết nợ cho bố mẹ Mây. Hùng sẽ ở đây. Rồi Mây sinh cho Hùng những đứa con xinh đẹp, bụ bẫm. Hàng tháng mẹ con Mây chờ Hùng đi làm các công trình trở về…Những ngày phía trước như thế đấy.

Gốc thông đã nghe những thanh âm tột cùng sung sướng trong đớn đau của Mây. Nhiều đêm, Mây muốn được Hùng cứ ôm mãi mà ngắm vành trăng non manh mảnh treo trên đỉnh núi, rồi ngủ thiếp đi trong cánh tay rắn chắc ấy cho đến sáng.

Hơn hai tuần rồi Hùng không sang. Mây cồn cào gan ruột. Cả ngày chả muốn làm gì.

Buổi sáng Mây thấy muốn nôn. Mây nôn thốc nôn tháo. Mẹ hốt hoảng nhìn Mây. Mẹ chạy ra ôm Mây. Bất ngờ, mẹ lật áo Mây lên. Mẹ ngã vật ra. Thôi thế là con này hỏng rồi! Mẹ khóc như trong nhà có người chết. Mây lạy mẹ, van mẹ mà bà vẫn chưa thôi khóc.

Chiều, Mây vượt dốc sang bên Nậm Dế. Mấy phòng học đã xong. Không một bóng người. Vài đứa trẻ từ đâu chạy ùa ra nghịch những cát sỏi, bê tông vương vãi. “Đi lâu rồi”. Mây nghe không hết câu trả lời của lũ trẻ. Cái bản Hùng sẽ đi xây tiếp ở đâu, là bản nào, bản nào? Sao lúc ấy Mây không hỏi rõ?

Mây lao nhanh xuống dốc.

Trăng non đã lên. Những bầu ngực núi phía tây đang ưỡn lên gọi trăng.


Hà Giang, tháng 9 năm 2008
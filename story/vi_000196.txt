Chị có mái tóc dài, suôn mềm và óng mượt. Đó cũng nhờ một tay má chăm sóc, vuốt ve hàng ngày nên tóc chị giống y chang tóc của má. Mỗi sáng thức dậy, má kêu chị ngồi ở mép giường để má chải tóc. Má nhẹ nhàng, cẩn thận gỡ từng sợi tóc rối cho thẳng thớm, cột lại đàng hoàng rồi mới cho chị đi rửa mặt. 

Mỗi chiều, sau khi đi ruộng về, nấu cơm xong, bao giờ má cũng nấu một nồi nước lá sả để gội tóc cho chị. Và cho má. Sợ không đủ lá sả, cha khoanh vuông đất sau nhà bằng bốn cái đệm bàng trồng toàn sả. Má cười: “Nhiều vậy bán chớ nấu gội sao hết?”. 

 
 Cha dừng cuốc, quệt mồ hôi: “Kệ, thà dư chớ đừng để thiếu. Thiếu khó ngủ lắm!”. Có bữa má hái nắm lá chanh còn xanh lè nấu nước gội đầu. Đêm, cha thủ thỉ: “Mới thì có mới thiệt nhưng mùi hắc quá!”. Tiếng của má: “Ừ, vậy thì đi kiếm mùi sả cho nó ngọt ngào!”. Giọng cha nhừa nhựa: “Tóc dài dễ giận quá ta!”. Sau đó im lặng, chỉ có tiếng vạc tre giường lúc khoan lúc nhặt lẫn trong tiếng ếch nhái kêu râm ran…

Cha về nhà ngoại chiết mấy nhánh bưởi “Năm roi” về trồng ngay trước cửa. Mấy tháng sau, nó bén rễ xanh mướt, đâm chồi nảy lộc không cần bón phân. Tới mùa, bông nở trắng tươi, thơm ngào ngạt, lan tỏa vào tận trong nhà. Má hái vài lá sắp tàn, thêm vài cái bông bưởi vừa mới hé nở bỏ vô nồi nấu nước gội tóc. 

Cha kể: “Hồi con gái, tóc má dài tới đầu gối, rất nhiều trai tráng xóm trên xóm dưới ước mơ được một lần hôn mái tóc thơm mùi bông bưởi của má nhưng có được đâu, bị cha cho “rớt đài” hết. Bởi vì, đâu có ai… đẹp trai bằng cha!”. Má đang xối nước lên đầu dừng lại, nguýt dài: “Dữ hôn! Đẹp quá nên sòn sòn mỗi năm một đứa. Nửa chục rồi đó. Riết rồi thân tôi quéo lại luôn nè!”.

Năm sau má đẻ con Bé Bảy, trắng ngời ngời như bông bưởi. Bà con hàng xóm tới thăm lúc má đang kỳ ở cữ cứ xuýt xoa: “Coi thằng Đậm đen đúa vậy mà đẻ con trắng bóc, hay thiệt nghen!”. Cha bưng mâm cơm vô buồng cho má rồi buông một câu: “Biết phải con tui hôn?!”. Nói vậy nhưng trên mặt cha tràn đầy hạnh phúc. Má liếc cha, thách thức: “Vậy thì đừng nuôi!”. Cha cười khì khì: “Kệ, thấy nó dễ thương, nuôi đại mà”.

Năm chị mười tuổi tóc đã dài đến giữa lưng. Trước giờ đi học, má tết tóc chị thành hai bím rồi buộc cái nơ bằng vải đỏ.

 
 Ảnh: st

Năm chị mười sáu tuổi tóc dài đến hông, dầy hơn hai nắm tay. Má không tết tóc cho chị nữa mà để chị tự chăm sóc. Song, má vẫn thường khuyên chị, làm gì thì làm nhưng nhớ phải giữ gìn mái tóc, đó là góc con người, để mai này lấy chồng được nhiều hạnh phúc, giống như má vậy. Nhiều khi má định sai chị làm việc nọ việc kia, nhưng thấy chiều xuống, má kêu: “Bé Ba, Bé Tư đâu…”. 

Trên sàn nước, chị cúi người hất mái tóc ra phía trước, ngọn tóc nổi trên mặt sông bồng bềnh. Một tay chị múc nước lá bông bưởi đã pha sẵn trong thau đổ từng chút một lên đầu, một tay vuốt từng lọn tóc cho ướt đều, lấy mặt sông làm gương soi xem sợi tóc nào còn khô. Gội xong, chị đứng dưới tán cây bưởi vừa chải vừa hong tóc trong nắng hoàng hôn. Cha ngồi uống trà bên hiên nhà, choàng tay qua vai má, nói: “Nhìn giống y má tụi nhỏ ngày xưa”. Má hất tay cha, nghiêm giọng: “Già rồi nghen. Tụi nhỏ nghe thấy được nó cười đó”. Má mua cho chị cây kẹp hình chiếc lá kẹp sau gáy. Nhìn chị với những nét đầu tiên biểu hiện dáng vóc thiếu nữ đã làm cho nhiều chàng trai thèm muốn.

Rồi chị đi lấy chồng. Không phải người cùng xã mà ở thành phố. Anh chị quen nhau từ năm đầu đại học, ra trường cưới ngay. Ngày cưới, chị không đi ra tiệm làm tóc, chỉ ngồi yên lặng để má chải tóc lần cuối. Sau đó má lấy nước bông bưởi đã nấu đặc quánh, rắc lên tóc chị như một loại nước hoa, thơm dìu dịu. Chị mặc áo dài màu đỏ, khoác thêm chiếc áo ren trắng làm tôn mái tóc dài đen huyền phủ gần hết lưng. Trên chiếc đò dọc đưa chị ra thị trấn để lên xe về thành phố, tóc của chị bay bay, óng ánh trong nắng sớm làm những người tiễn dâu cứ tiếc chị đi lấy chồng xa, sẽ ít có dịp nhìn ngắm nữa!

Những buổi trưa hè nóng bức, cha mắc võng nằm dưới tán cây vú sữa, má ngồi cạnh chải tóc cho Bé Bảy. Tóc Bé Bảy cũng đã chấm vai rồi. Má lại nói những lời mà rất nhiều lần đã nói với chị: “Trời cho con gái con lứa là phải đẹp, cả người lẫn nết. Mái tóc cũng đẹp, càng dài càng đẹp. Phải ráng giữ nghen con!”. Bé Bảy “dạ” chứ nào hiểu hết lời má. 

“Con thấy tóc chị Hai con đẹp không?”. “Dạ, đẹp lắm!”. Dứt lời, nó chạy đi chơi với bạn bè trong xóm. Còn má ngồi lại, vén hết mớ tóc dài ra trước ngực, nâng lên bày tay nhẹ nhàng chải. Đã có những sợi tóc rụng mắc vào răng lược, má gỡ từng sợi một, vuốt thẳng, cột lại thành lọn nhỏ. Rồi đem vắt trên vách ngay đầu giường ngủ.

 
  Ảnh: st
Việc làm, việc chồng con đã cuốn chị theo thời gian, thỉnh thoảng mới tranh thủ về thăm gia đình một hai ngày. Những lần đó, chị hay mang về cho má mấy loại dầu gội đầu rất khó nhớ tên, mỗi lần nói méo cả miệng vẫn chưa đúng. Chị nói: “Má lớn tuổi rồi, xài loại này cho tiện, khỏi mắc công nấu nước, má à!”. Má nhìn chị rất lâu, thở dài: “Má xài không quen. Với lại, tóc má đâu còn nhiều nữa. Nấu nồi nhỏ là đủ gội rồi”.

Một buổi trưa, trời nắng chang chang, chị bồng thằng con hơn một tuổi về tới cửa, cha ngồi trong nhà nheo nheo mắt hỏi: “Ủa, đứa nào vậy bây?”. “Con nè cha!”. “Bây có lộn nhà không vậy? Con Bé Hai tóc dài mà?”. Má đang nấu cơm sau bếp bước vội lên nhà trên. Má chợt sững người khi nhìn chị. Đôi đũa xới cơm trên tay má rớt xuống đất. Tự dưng nước mắt má ứa ra. 

Con Hai đây sao? Mái tóc dài ngày xưa của chị mà má đã chăm chút, vuốt ve, nâng niu giờ cụt ngủn như tóc thằng Ba, thằng Tư, không che hết cái cổ trắng ngần của chị. Gì vậy con? Chẳng lẽ khổ đến nỗi phải bán tóc mà ăn sao? “Không phải đâu má. Tại công việc bộn bề quá mà con không có thời gian nên cắt ngắn vầy cho tiện. Kiểu này cũng là mốt bây giờ đó má”. Cha nghẹn giọng: “Chồng bây nó không cản sao?”. “Dạ… ảnh cũng tiếc lắm!”. Chị nói mà nghe như giọng của ai chứ không phải của mình.

Buổi chiều, chị ngồi ngay bậu cửa chải tóc cho má. Khi tháo búi, xổ tóc của má chị bỗng giật mình khi trong đó rớt ra một lọn tóc bằng ngón chân cái. Thì ra mỗi lần chải tóc, những sợi tóc rụng bám vào răng lược, má đã cầm từng sợi vuốt thẳng rồi cột lại thành lọn nhỏ để làm tóc mượn. Có thêm lọn tóc mượn này, tóc má sẽ đầy búi, nếu không, khi búi lên nó chỉ bằng trái chanh thôi. “Bữa con cắt tóc còn giữ lại không Bé Hai? Nè, chừng nào về đem cho má nghen!”. Chị “dạ” rất nhỏ, cố nén tiếng nấc nghẹn đang trào dâng. Chị không đủ can đảm nói cho má biết.

Những lần sau chị về thăm nhà, má nhắc đi nhắc lại chuyện đem mớ tóc đã cắt về cho má. Mà lần nào chị cũng nói “quên”. Ngay cả hôm má mất chị cũng “quên”. Chị ngồi bên cạnh má, vừa chải tóc cho má vừa khóc rưng rức. Những sợi tóc của má giờ trắng phau, lòa xòa trên trán, dài đến vai thôi. Giờ, nếu chị có đem tóc về má cũng đâu lấy làm tóc mượn mà chi, đâu thể nối thêm cho đầy búi làm gì!

Chị quỳ xuống, nói khẽ vào tai má, giọng nghèn nghẹn: “Vì suối tóc má đã cho con mà chồng con nhiều lần ghen bóng ghen gió… nên ảnh đã cắt… đốt đi rồi. Má ơi…”.
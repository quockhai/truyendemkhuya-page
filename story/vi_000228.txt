Bạn hỏi cái gã cao, gầy, khuôn cằm vuông xám mờ vệt râu được tỉa tót kĩ lưỡng, mái tóc vuốt gel cẩn thận, ăn mặc đơn giản nhưng không tuềnh toàng đang ngồi cắm cúi trong xó quán viết viết thứ gì đó ấy à? Gã là chủ tiệm cà phê Lít ở phố Tây, yêu thơ Hai-kư và mê sáng tác. Một vài mối tình đã đi qua cuộc đời gã trai trẻ này, biến mất không dấu vết. Gã sống cô độc trong quầy pha chế của quán một thời gian dài cho đến khi nàng xuất hiện và nhảy xổ vào cuộc sống của gã như một cô gái… “trơ trẽn” nhất thế giới.

Hình như gã khoảng hai bốn, hai lăm gì đấy.

Gã có cái tên khá đẹp: Lam Phong.

*
*    *

Nàng rời Buôn Mê, từ bỏ công việc quản lý chất lượng sản phẩm trong công ty cà phê Trung Nguyên, thuê xe máy lồng lên Đà Lạt, đứng trên một con đèo mờ sương nhìn xuống những ruộng cà chua chín đỏ, chụp vài pô hình không cần chỉnh máy rồi bắt xe đò lên Sài Gòn để thử sức trong công việc hoàn toàn mới mà mình đam mê mãnh liệt.

 
  Ảnh: Phạm Duy Tuấn
Nàng ngồi trên chuyến xe bus với lộ trình sẽ ngang qua đài phát thanh. Bấm số của người yêu, nàng nói lời chia tay bằng một câu khá sến: “Em biết giờ này anh đang xem đua xe công thức 1. Hãy cứ tập trung xem đi nhé, đừng rời mắt khỏi ti vi vì em sẽ nói nhanh thôi… - Nàng dừng lại, để lặng một hai giây rồi tiếp - Em nghĩ anh không phải là giấc mơ của em! Em là một con bé ngố tệ. Suốt tuổi thơ, em đã luôn ảo tưởng rằng mình có thể khóc nhè ra nước hoa và tin có những người tí hon ẩn náu giữa những bó rau hay củ cà rốt trong tủ lạnh. Có lần em kể với anh về ước mơ trở thành một DJ rồi nhỉ? Em đang ở một thành phố lạ, ngồi trên một chuyến xe bus, cô đơn như một con nhện để thực hiện ước mơ đó.”. Nàng tắt điện thoại, tháo pin, bẻ sim và mở hé cửa kính xe vứt nó xuống đường rồi ôm mặt thút thít.

- Này, nếu muốn xì mũi thì cứ lấy!

Nàng ngẩng mặt nhìn bịch khăn giấy của một gã lạ hoắc ngồi ghế bên cạnh chìa ra trước mặt, rút lấy một miếng phảng phất mùi nước hoa.

- DJ hả? Nghề đó có gì hay chứ? - Gã trai hỏi với nụ cười đượm vẻ chế giễu.

Nàng vo viên miếng khăn giấy, tự lấy tay quệt sạch nước mắt:

- Tôi sẽ coi đây là câu hỏi vô duyên và đểu nhất trong ngày mà tôi phải nghe.

Gã trai xụi lơ, ngó chỗ khác.

Đến trạm gần phố Tây, gã xuống xe, quay lại nhướn mày nhìn nàng đầy vẻ bỡn cợt:

- Tôi xuống trước nhé! Cô cứ ở yên đó và gặm nhấm nỗi buồn của mình đi!

Nàng nhìn xoáy vào gã tính vặc lại câu gì đó thật chát đắng nhưng lại thôi khi nhìn thấy điệu cười nửa miệng kì lạ của gã. Điệu cười đó, giữa một rừng đàn ông, anh ta sẽ chẳng bao giờ… chìm.

Chợt nàng nhìn thấy chùm chìa khóa rớt lại trên ghế bên cạnh. Hẳn là của cái tên đáng ghét đó. Trong khoảnh khắc rất nhanh, nàng quyết định xuống xe, dù biết mình sẽ bị trễ buổi thử giọng ở đài phát thanh.

Nàng đi ngược con đường toàn khách du lịch nước ngoài. Mấy gã mũi lõ, mắt xanh nói “Hai” với nàng và mỉm cười thân thiện. Phải rồi, với một cô gái bé nhỏ và dễ thương như nàng thì đàn ông nên như vậy. Rồi nàng suy nghĩ lý do sao lâm ly, mùi mẫn để biện minh với nhà đài cho việc trễ buổi thử giọng của mình… Vừa đi vừa ngẫm ngợi, nàng va vào cột điện, bật ngửa ra đường. Mặt nàng đỏ như bít tết áp chảo.

Khi ấy, gã ngồi chồm hỗm trước Lít - một quán café nhỏ được xây dựng theo một kiểu đặc biệt quá đáng, không ra thể thống gì (Không thể gọi tên quán theo phong cách nào), cười cợt cú ngã của nàng…

- Cái này của anh?

Gã nhìn chùm chìa khóa trên tay nàng, hớn hở ra mặt.

- Ừ, phải! Thế quái nào cô có nó? À, hiểu, hiểu rồi. Xưa rồi nghe Diễm, chiêu này tôi biết. Bọn con gái các cô hay dùng cách đó để làm quen với anh chàng mà mình thích à? Đầu tiên là màn khóc lóc chia tay người yêu trên xe bus, sau đó lén lút rút chùm chìa khóa và vờ nhặt được rồi quay lại để trả cho chủ nhân với hy vọng mình sẽ được anh ta chú ý chút đỉnh. Tôi nói đúng chứ?

Nàng tát gã một cái, không đau nhưng đủ mạnh để hắn nhận ra sự lố bịch của mình:

- Tên tồi tệ này! Nghĩ gì vậy chứ?

 Nàng quay bước, vừa đi vừa lầm rầm nguyền rủa mình thật điên rồ tự dưng bỏ buổi thử giọng để quay lại đây. Thay vì nhận được từ hắn sự biết ơn thì lại để hắn sỉ nhục.

Gã chạy lẽo đẽo phía sau:

- Này, tôi sẽ nói cám ơn, dù không muốn tí nào. Và nghiêm túc, tôi rất muốn biết tên cô là gì!

- Pinh. Nhưng đừng lầm tên tôi với màu hồng. - Nàng đáp với một chút bực bội và gắt gỏng.

- Cô đã phải bỏ một điều gì đó để trả lại tôi chùm chìa khóa này. Tôi chẳng biết nó có thực hay không. Nhưng nếu thất nghiệp, hãy đến tiệm Lít của tôi. Cứ coi đó là sự đền đáp cho người mà tôi mang ơn. Và dù sao thì tôi vẫn sẽ gọi cô là cô gái màu hồng. 

*
*    *

 “Buổi thử giọng kết thúc rồi! Chúng tôi đã chọn được người cần tìm. Em có thể đến vào lần sau.”

Một chị nhà đài, rõ ràng là nói câu đó với nàng bằng tiếng Việt, vậy mà cứ như thể chị ta đang hát híp - hop bằng tiếng Do Thái, mãi nàng mới chịu hiểu ra, rằng, chậm mất rồi.

Nàng lếch thếch xách ba lô rời đài phát thanh, lặng lẽ băng mưa về gác trọ. “Sao mọi chuyện lại trở nên tồi tệ thế này nhỉ?”

Cái ước mơ mà nàng kiên cường giữ lấy một cách ngốc nghếch từ bao lâu nay tan vỡ vì chùm chìa khóa của một thằng cha vớ vẩn hết sức. Hắn, thật khốn kiếp.

Căn phòng sực mùi nước tiểu chuột, sáng lên thứ ánh sáng vàng vọt khi nàng chạm tay vào công tắc điện, vậy mà cũng phải trả một giá khá đắt cho chỗ ở này chỉ để sống chung với… chuột.

Nàng vùi mình trên tấm đệm cáu bẩn, nghe đi nghe lại một bản nhạc trong Mp3 từ giờ này sang giờ khác và kiểm kê lại hàng trăm lần tài khoản còn lại sau khi để phần lớn cho mẹ trước khi lên đây. “Mấy tháng tới con sẽ chẳng cho mẹ được nữa đâu, mẹ cầm lấy khoản này mua cái áo len và ít trà cho bố. Còn bao nhiêu mẹ để dành mà xài. Nếu không dùng đến thì nhét ống heo, mai mốt làm của hồi môn cho con cũng được”. Sẽ chỉ đủ cho một tháng tạm bợ, nếu như nàng có thể sống mà chỉ cần mút ngón tay.

Không dưng, nước mắt lũ lượt trào ra…

 
  Ảnh: Phạm Duy Tuấn

*
*    *

Cuối cùng thì nàng cũng đành dẹp cái tự ái của mình để tìm đến tiệm Lít của gã. Khi nàng đẩy cửa bước vào, thậm chí gã còn không ngẩng mặt lên, cắm cúi gõ vào chiếc laptop lạch cạch miệng nhả một câu nhạt thếch:

- Gì đấy?

- Sao? Với một người mà mình mang ơn, đến một câu chào tử tế cũng không làm được à? - Nàng đốp chát.

- À, hôm bữa quên không hỏi. Vụ đụng cột điện đó, ngã đau không?

- Quan tâm thái quá dễ khiến người ta nổi điên đấy!

- Thế hôm nay có bị ngã cú nào không?

…

- Đùa đến đây chắc là đủ rồi. Những cô gái rất dễ biến thành phù thủy khi họ bị chọc cho phát điên. Thế đã suy nghĩ về cách đền ơn của tôi hôm bữa chưa?

- Đã.

- Thấy thế nào?

- Chẳng thấy thế nào cả! Tôi muốn có một công việc để mưu sinh. - Nàng trả lời, không ngó gã mà nhìn chữ Lít rất to trên cửa kính.

Gã ngước mắt lên, sửa lại cặp kiếng trên sống mũi và nhìn nàng như một vật thể lạ ngoài hành tinh, giọng nói đột nhiên có sự rối rắm:

- Cô!... Vừa bảo gì?

- Chẳng lẽ hôm bữa anh chỉ nói chơi?

- À không! Tôi định thuê cô trông mèo và thay nước bể cá. Nhưng về nghĩ lại, sợ nước mắt thất tình của cô rớt xuống làm chết cá của tôi nên chắc không có việc gì cần tới cô nữa. - Gã bông phèng.

- Đành thôi! Tôi sẽ đi đâu đó kiếm việc khác.

Nàng đẩy cánh cửa kính, toan bước ra khỏi quán. Gã đứng lên, nói với nàng:

- À, này. Quán này của tôi nhỏ nhưng mà ấm nhỉ. Cô thấy đấy, mấy bậc cầu thang gỗ cũ kĩ nhưng rất xinh mà, phải không? Mấy bức tranh đó, tự tay tôi vẽ trong lúc phấn khích đấy. Tuyệt không?  - Vẫn cái giọng bông phèng nhưng giờ đã nghiêm túc hơn  - Ghé rồi thì làm một ly đã rồi hẵng đi. Cappuccino nhé!

Nàng gật đầu đồng ý, quay lại ghế ngồi trong khi anh ta pha chế ở quầy ba.

- Muốn nghe một bài gì đó vui vui không?

Chẳng cần biết ý kiến của nàng, gã bật bài Bonamana của Super Junior – một bài hát mà nàng rất thích.

- Có lẽ tôi cần một người sửa lỗi cho tập bản thảo tiểu thuyết mới và phụ bán hàng. Bao luôn cơm ba bữa, cứ ở lại nếu muốn - giọng gã nghiêm túc - Đằng nào thì buổi tối tôi cũng cần một người trông quán. Như thế, khi tôi về, cô ngủ có thể yên tâm không cần lo bọn chuột cống khoét tường vào phá cửa tiệm. Nhưng hãy hứa là phải làm việc thật chăm chỉ.

Cuộc trò chuyện kết thúc bằng nụ cười nửa miệng kì lạ của gã. Còn nàng thì ngồi liếm bọt Cappuccino dính trên mép một cách ngon lành. Nàng định kể cho gã nghe về việc đã lang thang đi mấy nơi tìm việc mà không thành, nhưng anh ta đã trở lại xó quán và bắt đầu tập trung viết lách…

Nàng bắt đầu quen với việc bưng cà phê, dọn bàn, và khi quán vắng khách ngồi phóng tầm mắt qua cửa kính ngắm mấy anh chàng ngoại quốc lưng đeo balo to sụ lang thang kiếm phòng để thuê và kiếm một quán ăn hợp khẩu vị. Quen luôn với việc Lam Phong hỏi mấy câu sến súa, đại loại như: “Cô có tin không? Đôi khi ta yêu ai đó đến mức ta câm lặng trước họ.”

Có lần, khi quán đang đông khách, bỗng nhiên Lam Phong gọi giật nàng lại, giọng nhão như một lon gạo nấu cùng một trăm lít nước:

- Pinh, đã bao giờ em yêu ai đó, thật lòng và sâu đậm chưa?

Pinh quay đi, nàng giấu sự ngại ngùng trong câu hỏi ngược:

- Anh có nghĩ thật phí thời gian khi cứ ngồi hàng giờ sắp xếp những khoảnh khắc mà mình đã từng hoặc chưa từng trải qua trên một trang văn?

Gã cũng chẳng trả lời câu hỏi, lại quay về công việc của mình, miệt mài theo suy tưởng về số phận, cuộc đời của những nhân vật hư cấu.
 
*
*    *

Trong một lần say bí tỉ, Lam Phong bất ngờ ghé quán vào giữa khuya, đi lảo đảo như con bù nhìn đưa theo chiều gió trên cánh đồng, gõ cửa rầm rầm. Khi nàng vội vã ra mở cửa thì gã xô nàng vào góc tường, lấy ngón trỏ day day lên trán, miệng phả ra hơi rượu nồng nặc và lảm nhảm mấy câu vẻ trách móc: “Em biết không, chín mươi chín những lần nhìn em, trong đầu tôi luôn mường tượng ra cảnh em hôn một thằng cha chết tiệt nào đó. Tự nhiên lúc đó, tôi muốn khóc đến trào nước mắt.”

Nàng sững người mất mấy giây, cứ đờ ra, không biết phải hành động thế nào. Trong khi đó, gã nhếch mép cười, rồi ọe đầy người nàng, sau đó nằm vật ra giữa sàn, ngủ như chết.

Nàng kéo gã lên cái giường xếp ở quầy ba, thở hổn hển, rồi mang Ipod ra cắm vào bộ loa của quán, mở mấy bài gào thét điên cuồng, vào phòng tắm gột rửa và nhấn chìm những rung động của chính mình. Lạ thay, những rung động ấy không chết chìm!

 
  Ảnh: Phạm Duy Tuấn
Buổi sáng, trời thật đẹp. Cao và xanh. Nắng và gió. Rực rỡ và ấm áp. Gió phóng khoáng và mây yên bình. Nàng ngồi ôm cây bút chì, muốn ghi lại cái long lanh ấy của ngày.

- Cô vẽ à? - Anh ta chui từ nhà vệ sinh ra trong bộ dạng nhàu nhĩ, hỏi thản nhiên cứ như thể chuyện hôm qua là một làn khói biến mất vĩnh viễn trong trí nhớ.

- Ừ, tôi có kí họa chút ít. Cho vui thôi. Mấy bức vẽ chẳng ra cái giống gì.

…

- Anh đã yêu ai chưa?

- Đã… Nhưng có vẻ chẳng tới đâu và chẳng ra làm sao…

Gã châm điếu thuốc, gương mặt thoáng buồn. Rít một hơi thật mạnh, gã bước tới cỗ dương cầm ở góc quán mà chẳng mấy khi có ai đụng đến:

- Tôi sẽ chơi vài bản nhạc vui! Cô muốn nghe chứ?

Những ngón tay chạy tung tăng trên những phím đàn màu trắng kết thành những giai điệu rực rỡ. Nàng đắm trong thứ âm nhạc mê hồn ấy và thấy niềm hạnh phúc cứ đầy lên trong tim mình.

- Hy vọng, tối qua tôi không làm điều gì bậy với cô... Tôi cũng chẳng thể nhớ nổi đêm qua mình đã nói gì…

Lam Phong bỏ dở khúc nhạc giữa chừng, nói với nàng điều ấy rồi đứng dậy bước ra ngoài. Nàng thấy niềm hạnh phúc ban nãy tan loãng đi…

*
*    *

Gã ngồi uống cà phê với gương mặt chảnh chọe, huyên thuyên về mấy đoạn văn mình tâm đắc rồi đột ngột chuyển chủ đề:

- Cô có bao giờ vì tình yêu mà khóc chưa?... À, quên. Rồi, đúng không! Cái hôm trên xe bus ấy. Tôi còn phải đưa khăn giấy cho cô nữa mà. Lúc đấy, thật là buồn, nhỉ? Thi thoảng tôi rất sợ cái cảm giác phải dần xa một ai đó mà không cách gì níu giữ được. Tôi rất sợ khi ai đó bỗng dưng bảo rằng mình phải hứa với họ là hãy sống thật mạnh mẽ. Thế rồi, người đó bỗng dưng biến mất như một con chấu chấu vùi mình giữa cỏ xanh, không cách gì tìm ra được…

- Khi không, nói mấy điều này là ý gì? Mấy lời đó sến và buồn cười như sư tử ăn chay ấy!

- Pinh, hứa với tôi là cô sẽ sống thật mạnh mẽ nhé!

- Cái quái gì đây! Anh nói cứ như thể mình sắp chết hoặc là sắp phải đi đâu đó rất xa và rất lâu vậy.

Lam Phong bật cười khanh khách:

- Xem cô kìa! Bị tôi hù cho sợ đến nỗi mặt xanh bợt bạt cả đi. Nhìn cô lúc này, y như mấy con sư tử in trên biểu tượng của đội tuyển bóng đá Anh. Ngố tệ!

Nàng dằn khay bánh mì cay trước mặt gã:

- Ăn đi lấy sức mà còn chế nhạo người khác! Tiện thể, tôi muốn thông báo một việc quan trọng. Hết tháng tôi sẽ nghỉ việc. Một gã Tây Ban Nha - À, cái gã ấy tên là… Ri… Đúng rồi, Rick. Hôm trước gã ghé tìm anh ở quán nhưng không gặp, tôi quên khuấy mất không báo lại - muốn lên Buôn Mê thăm thú. Hắn cần một hướng dẫn viên. Tôi nghĩ đây là cơ hội tốt, kiếm được cũng bộn mà lại được ghé qua nhà.

Gã khẽ gục gặc đầu, gương mặt thoáng cay đắng, rồi ngẩng lên nhìn nàng:

- Sẽ thế nào nếu em gặp lại… anh ta?

- Ai cơ?

- Cái anh chàng ngồi xem đua xe công thức 1 khi em gọi điện nói chia tay ấy?

- Em nghĩ mình sẽ nói xin lỗi dù câu đó hơi đau lưỡi.

- Và nếu anh ta tha thứ?

- Em cũng chưa biết! Em đã quậy tung cuộc sống vốn bình lặng của chính mình và giờ em nghĩ mình nên làm một việc gì đó đúng đắn để mọi thứ không còn lộn xộn nữa.

- Em nói thế làm tôi buồn lắm! Tôi nghĩ, thật đáng sợ… khi em đi!

Gã nói nhanh câu đó và bỏ đi, mất hút sau bờ tường gạch hốc hác đỏ sậm dưới nắng mùa đông như một cơn gió vô hình. Nàng thấy cái vóc dáng cô đơn ấy ám ảnh quá đỗi! Nàng đặt tay lên tim mình và tự hỏi “Thế là thế nào?”

*
*    *

Hôm nàng rời đi, trời mưa dai kinh khủng. Nàng quẳng lại cho Lam Phong bộ xếp hình to đùng mua được ở một góc hội chợ giảm giá, ghi ngắn gọn như muốn tiết kiệm mực: “Vui nhé! Khi nào xếp xong thì gọi cho tôi. Trước đây, anh từng hỏi tôi nghề DJ có gì hay ho. Tôi chưa trả lời thì phải. Giờ nghe nhé! Tôi ao ước được làm một DJ cho một chương trình âm nhạc theo yêu cầu trên FM. Vì tôi cứ vẽ ra trong đầu mình cái cảnh một đêm mưa buồn bã nào đó, có một ai đó đang rất cô đơn, họ nghe tiếng tôi dẫn chuyện tếu táo và thứ âm nhạc sôi động tôi chọn. Rồi họ cảm thấy tên DJ này nhìn thấu tâm can mình. Như thế, theo anh có tuyệt không?.... Mà nè, Lam Phong, ghét tôi thật nhiều vào vì đã đi mà không chào tạm biệt!”

 
  Minh họa: Phạm Minh Hải

Gã ôm bộ xếp hình Pinh để lại, bật khóc một lúc lâu.

Bình tĩnh lại, gã tự pha cho mình một cốc cà phê to, ngồi chống cằm nhìn ra ngoài mưa. Lát sau, gã lôi trong túi áo khoác ra tấm ảnh chụp lén lúc Pinh đang ngồi tư lự bên cửa sổ quán, vành môi cong lên bướng bỉnh. Biết bao lần, gã đã lôi bức hình ra và áp môi mình vào vành môi ấy thử tìm thứ cảm xúc khác lạ cái cảm giác mà mỗi lần Rick đặt môi lên môi gã và lờ mờ nhận ra rằng bản năng đàn ông của mình đang trở lại…

Lam Phong rút cây bút chì Pinh vót sẵn trước khi đi, định viết nắn nót tựa đề cho chương tiếp theo của tiểu thuyết nhưng không thể. Thay vào đó, gã viết những dòng cho Pinh.

“Em biết không, cô gái màu hồng? Ngay lúc gặp em lần đầu tiên trên chuyến xe bus, anh đã có dự cảm rằng cô gái này có một sức khuynh đảo cuộc sống của mình. Anh cố lảng tránh, cố làm em ghét anh. Vậy mà tại sao thế kia chứ, anh vẫn không thể kiểm soát nổi cảm xúc... Anh cố tình làm rơi chìa chùm chìa khóa và ngồi đợi em mang trả. Anh chọc tức em, khiến em phát điên. Anh bịa ra chuyện cái bể cá và con mèo. Anh trở nên điên hoặc là gàn dở… từ khi em xuất hiện trước mặt anh. Có lúc anh nghĩ, cái hôm anh giả say gõ cửa quán, giá như anh hôn em... Em làm anh nghĩ mình bình thường, vì mình yêu… một cô gái.

Cái hôm em bảo em sẽ thôi việc, để kiếm bộn tiền nhờ một phi vụ làm ăn với một gã Tây Ban Nha, anh giật thót. Anh biết Rick đã biết chuyện anh có cảm tình với em, và anh ta chọn cách đó - một cách khéo léo và hợp lý quá thể - để mang em về lại Buôn Mê, cách xa anh. Anh chẳng biết khoảng cách giữa Sài Gòn và Buôn Mê là bao xa, nhưng rõ ràng là Rick muốn dùng khoảng cách ấy để kéo anh lại, khi mà trái tim anh đang chênh chao vì em. Anh và Rick - vị khách Tây Ban Nha của em, đều đồng ý rằng em có nụ cười có khả năng truyền cảm hứng vô bờ cho người đối diện. Anh gặp anh ta cũng trên xe bus như anh và em vậy. Khi anh bước lên xe, anh ấy đứng lên nhường ghế, dù anh chẳng phải người tàn tật, phụ nữ mang thai hay già cả gì sất. Và anh ta… rất yêu anh.

Có một điều anh muốn khẳng định với em rằng, sẽ rất ít lần trong đời, mình nhận ra là mình yêu ai đó nhiều đến mức mình câm lặng trước họ. Vậy nhưng, em là một con ngốc với chỉ số IQ của một con gián, hay em là cái tay nắm cửa, một miếng lót ly, mà không nhận ra là có lúc anh yêu em chết đi được, hả?.”

Gã thần người, một cái gì đó rưng rưng dâng lên, những hình ảnh về Pinh trôi trong đầu như một thước phim.

Gấp lá thư lại, chưa kịp vuốt hết các cạnh giấy cho thật phẳng, gã đã vò nó và ném luôn vào sọt rác.

Ánh mắt gã bỗng nhiên sáng lấp lánh. Gã mỉm cười một mình, thở ra nhè nhẹ và đứng dậy vươn vai đầy khoan khoái.

Gã sẽ đi Buôn Mê, ngay bây giờ! 
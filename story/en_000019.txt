Saki là nhà văn danh tiếng người Anh, tên thật là Hector Hugh Munro (1870 - 1916). Đương thời, ông thường được so sánh với những nhà văn viết truyện ngắn danh tiếng như O. Henry hay Dorothy Parker. Ngoài truyện ngắn, ông còn thử sức ở lĩnh vực tiểu thuyết với những giai tác như Bassington Khó chịu (The Unbearable Bassington); Alice ở Wesminster (The Westminster Alice) lấy cảm hứng từ tiểu thuyết Alice ở xứ xở diệu kỳ v.v… Ông tự thừa nhận mình có nhiều ảnh hưởng từ các nhà văn như Oscar Wilde, Lewis Carroll, đồng thời, nhiều nhà văn sau này cũng chịu ảnh hưởng lớn từ ông như A. A. Milne hay Noël Coward.  "Tobermory" là một trong những truyện ngắn nổi tiếng nhất, thể hiện đầy đủ những góc cạnh độc đáo trong tư tưởng và sự nghiệp sáng tác của Saki.

Ngoài việc viết văn, Saki còn là một nhà báo nổi danh. Ông từng tham gia viết trên những tờ báo, tạp chí danh tiếng nhất trên thế giới như Daily Express, Morning Post và Outlook. Khi Thế chiến I nổ ra, mặc dù đã 43 tuổi và được miễn quân dịch, ông vẫn tình nguyện tham gia Quân đội Hoàng gia với tư cách là một người lính bình thường, từ chối mọi đặc quyền mà Hoàng gia ban cho ông. Tinh thần chiến đấu và yêu nước của ông là một điểm sáng của quân đội Anh trong suốt thời kỳ Thế chiến I. Tháng 11 năm 1916, tại pháo đài Beaumont-Hamel (Pháp), ông đã bị một lính bắn tỉa người Đức hạ sát.

Mỗi khi gia đình ngài Blemley mở tiệc tại tư gia, cả vùng Bristol như mở hội. Gần như toàn bộ giới thượng lưu đều tụ tập về tòa lâu đài tráng lệ này. Hôm nay, buổi tiệc diễn ra đông hơn bình thường. Có lẽ bởi sự góp mặt của một nhân vật thần bí nổi tiếng có tên: Cornelius Appin.

Có rất nhiều lời đồn thổi về Cornelius Appin. Nào là ông Appin biết đi xuyên tường, thậm chí biết bay. Đương nhiên có người thì bảo ông là thiên tài còn người khác thì bảo ông là kẻ bịp bợm.

Tiếp xúc mấy hôm nay thì bà Blemley thấy rõ ràng ông Appin chắc hẳn không phải là thiên tài và cũng chưa đủ tinh quái để trở thành kẻ bịp bợm. Bà đã hỏi thử xem ông Appin có thể góp vui cho buổi tiệc bằng một tiết mục đặc sắc nào không? Ông Appin đã thành thực trả lời rằng ông ta chẳng thể đi xuyên tường, cũng không thể bay. Bà Blemley đã đem điều này ra phàn nàn với ông Blemley: “Mình nuôi hắn cốt để thu hút đông người đến bữa tiệc. Hắn chẳng làm được trò gì vui thì nuôi cơm hắn mấy tuần nay làm gì?” Ngài Blemley cũng băn khoăn.

Đúng lúc hai vợ chồng đang bàn bạc thì ông Appin chạy đến mặt đỏ bừng nói to: “Bữa tiệc hôm nay tại nhà ông bà sẽ có một tiết mục độc nhất vô nhị”. Ông bà Blemley mừng rỡ: “Tiết mục gì vậy?” Ông Appin gãi gãi cằm gục gặc đầu đầy xúc động rồi hét lên: “Thực ra đó còn hơn cả một tiết mục. Đó là một phát minh vô tiền khoáng hậu, một phép lạ thực sự”. Ông bà Blemley hỏi dồn: “Thật vậy sao? Điều gì mà kỳ diệu đến vậy?” Ông Appin ngần ngừ rồi nói: “Chắc chỉ còn một phần thử nghiệm cuối cùng nữa mà thôi. Xin ông bà hãy để cho tôi giữ bí mật cho đến buổi tiệc”. Nói đoạn ông Appin đứng vụt dậy nhanh thoăn thoắt lao vào phòng riêng rồi đóng sập cửa lại. Ngài Blemley nhún vai. Còn bà Blemley thì cố nhịn chờ đợi.

Trước buổi tiệc, không hiểu thông tin từ đâu lan truyền mà toàn bộ quan khách đã đều bàn tán sôi nổi về tiết mục của ông Appin. Mọi người đều vô cùng háo hức và phấn khích. Nhưng dù đã chuẩn bị trước tinh thần, tất cả khách mời đều há hốc mồm khi nghe ông Appin tuyên bố.

Mọi người im lặng như tờ dễ chừng đến vài phút. Sau đó ngài Blemley không nhịn nổi hắng giọng thốt lên: "Ông Appin, ý ông muốn nói là ông đã tìm ra một phương pháp dậy thú vật biết nói tiếng người? Và con mèo Tobermory của gia đình tôi chính là con mèo biết nói đầu tiên trên thế giới?”

Ông Appin ưỡn ngực: "Chính xác thưa ngài Blemley! Sau biết bao vất vả, giờ tôi thành công. Tuy nhiên cũng phải nói rằng sự thành công của tôi phụ thuộc rất nhiều vào con mèo Tobermory. Nó là một con mèo cực thông minh, chỉ mất vài tuần mà đã học nói được tiếng người!”

Mọi người đều ồ lên hoài nghi. Một số thậm chí còn xì ra những tiếng cười giễu cợt. Dường như muốn khẳng định lại những điều mình vừa được nghe, cô Resker đã giơ tay hỏi: “Ông muốn nói rằng giờ con Tobermory đã có thể nói bập bẹ vài từ phải không?"

Ông Appin nghiêng mình: "Thưa cô Resker, con Tobermory thực ra đã hiểu được tiếng người từ rất lâu. Loài mèo có ngôn ngữ riêng, chẳng qua con người chúng ta không hiểu hệ ngữ của nó. Với phương pháp siêu việt do tôi đã phát mình ra, giờ con Tobermory có thể chuyển hai hệ ngôn ngữ qua lại và nó hoàn toàn có thể nói rất sõi".

Đám đông ồn ào. Lần này không còn lịch sự nữa, có nhiều tiếng huýt sáo và tiếng kêu láo nháo: “Đồ nói phét".

Ngài Blemley nhìn ông Appin đầy hoài nghi. Còn bà Blemley thì nhẹ nhàng làm dịu đám đông bằng lời đề nghị: "Cứ gọi con Tobermory ra đây, bảo nó nói vài câu là rõ trắng đen ngay".

Ngài Blemley chạy đi tìm con mèo. Mọi người ngồi xuống bàn tiệc nhưng dường như không ai chú ý đến đồ ăn. Tất cả đều háo hức chờ đợi. Một phút sau, Ngài Blemley chạy về phía bàn tiệc, mặt tái nhợt, giọng nói run lẩy bẩy: "Trời đất ơi, không phải chuyện đùa đâu!"  

Mọi người lập tức vây quanh hỏi han. Ngài Blemley gieo mình xuống chiếc ghế, với lấy khăn lau mặt, lập cập cầm cốc nước uống cạn rồi vừa nói vừa thở: "Tôi nhìn thấy con Tobermory đang ngủ lơ mơ bèn gọi nó đi ra ngoài này cùng mọi người. Nó hấp háy mắt nhìn tôi rồi bỗng nhiên nói như giọng trẻ con: “Mọi người cứ chờ một lúc, liếm lông xong Tobermory sẽ ra ngay”. Tôi suýt nữa thì té ngửa, vội chạy ra đây để báo cho mọi người trước”.

Lời nói của ngài Blemley như sấm động giữa trời quang. Ông Appin lúc trước bị mọi người  nghi ngờ, giờ giương mặt tự đắc, tự nhấm nháp kỳ tích của mình. Mọi người đua nhau xì xào bàn tán.

Đúng lúc đó, con Tobermory lững thững đi vào giữa đám tiệc. Tiếng bàn tán im dần. Dường như ai cũng cảm thấy khó khăn khi phải đối mặt với một con mèo biết nói. Và cũng chưa ai chuẩn bị tâm thế kỹ càng với việc một con mèo giờ lại có thể đối thoại ngang hàng với mình.

Cuối cùng, bà Blemley thẽ thọt: "Tobermory này, mày có muốn uống sữa không?"

Con mèo Tobermory dõng dạc: "Cảm ơn bà chủ, cho tôi uống một chút thôi".

Mọi người ồ lên kinh ngạc trong khi bà Blemley rót sữa mà tay run run khiến sữa bắn cả ra tấm thảm. Bà đưa ly sữa cho con mèo giọng phân trần: “Tao vô ý quá, đánh đổ cả sữa ra tấm thảm Ba Tư này rồi".

Con Tobermory vênh váo: "Tôi thấy bà đánh đổ sữa suốt mà. Sau đó bà toàn đổ tội cho cô hầu rằng cô ta đánh đổ. Vả lại bà tiếc làm gì? Tấm thảm đó có phải đồ xịn từ Ba Tư đâu? Tôi còn nhớ bà mua nó từ tay buôn lậu đồ Trung Quốc mà".

Mọi người đều khúc khích kín đáo che miệng nhìn bà chủ nhà cười thầm. Còn bà Blemley đỏ bừng mặt tẽn tò không biết trả lời ra sao.

Cô Resker hắng giọng: “Ông Appin bảo là mèo có ngôn ngữ riêng. Thế tiếng mèo có khó không? Như tao học tiếng mèo thì mất bao lâu?”
Con Tobermory lạnh lùng: “Người khác thì không biết thế nào. Riêng cô thì chắc sẽ mất nhiều thời gian lắm đấy”.
Cô Resker ngạc nhiên: “Tại sao lại thế?”

Con Tobermory gục gặc đầu: “Vì cô không thông minh. Khi bàn đến chuyện mời cô dự hội hôm nay, Ngài Blemley đã cằn nhằn rằng rằng tại sao bà Blemley lại mời một người đần độn như cô? Lịch sự là một chuyện còn thương hại những kẻ ngu si là một chuyện khác. Còn bà Blemley thì giải thích rằng chỉ những kẻ ngốc như cô may ra mới nghe lời tán tỉnh mà mua chiếc xe cổ lỗ sĩ của gia đình”.

Cô Resker mặt tái đi vì tức giận. Đúng là chỉ vài tiếng trước, bà Blemley vừa mới gợi ý rằng chiếc xe ô tô của gia đình họ rất phù hợp để dùng tại tư gia của cô ở Devonshire. Bà Blemley còn gợi ý là nếu thích thì cô cứ lấy xe về mà dùng, tiền nong có thể trả sau cũng không sao.
Ngài Blemley thấy vậy vội chuyển chủ đề: "Này Tobermory, hôm trước tao thấy chú mày thân thiết với con mèo mướp hàng xóm nhà bên lắm. Thế hôm nay ả bạn gái của mày đâu rồi?”

Con mèo nhìn ngài Blemley ngạc nhiên: “Tôi tưởng chuyện đó loài người không hay nói chuyện trước bàn dân thiên hạ? Thực ra tôi thân thiết với ả mèo mướp đó đâu bằng mối tình thân thiết của ông với bà góa hàng xóm Cornett?”
Ngài Blemley hồn vía lên mây lấm lét nhìn vợ. Còn bà Blemley thì quắc mắt nhìn ông chồng. Nhưng trước mặt quan khách, bà không dám to tiếng.

Những người dự tiệc bỗng nín thinh. Không ai dám hỏi con mèo Tobermory thêm một câu gì nữa. Ai cũng ngại. Nhỡ đâu con mèo Tobermory này lại nói phọt ra bí mật gì đó của mình thì đúng là mất hết sĩ diện. Nào ai biết được con mèo quái quỷ này đã chui rúc ở những xó xỉnh nào, đã nghe thấy gì, đã nhìn thấy gì? Khi con Tobermory liếc nhìn xung quanh, lập tức có người lấy tay che mặt, thậm chí một số còn lén lút lỉnh đi vì sợ con mèo bỗng nhiên nhận ra mình mà bép xép ra chuyện gì đó mờ ám thì chết. Ngài Blemley thì cứ cầm đĩa cá rán rồi bí mật nhấm nháy với Tobermory, ý rằng ông sẽ để dành cho nó mấy con cá rán giòn để nó biết điều mà câm mồm lại.

Không khí bữa tiệc chùng hẳn lại. Cô Resker sau khi biết rõ nguyên do tại sao mình được mời đến liền không nén được thất vọng than lên: “Biết thế này tôi đã ở nhà! Tại sao tôi lại tham dự bữa tiệc này cơ chứ. Thật là dại dột.”
Con mèo Tobermory lập tức chớp chớp mắt và nói một mạch: “Đâu có dại dột gì? Cứ theo những lời cô thổ lộ với bà Cornett thì cô đến đây cốt để chén một bữa ăn thật đã đời. Cô bảo rằng nếu không vì đồ ăn ngon thì cô đã chẳng đến đây làm gì. Cô còn nói thêm rằng gia chủ là loại người tẻ nhạt nhất trong số những người cô biết”.

Cô Resker nhìn ông bà Bembley lúng búng trong miệng: “Làm gì có chuyện đó? Con mèo này nói dối đấy! Hay là mày nhầm tao với ai hả?"

Con mèo Tobermory gục gặc: “Tôi nhìn nhầm làm sao được?” Rồi nó quay sang bà Cornett: “Chẳng phải bà đã nói lại chuyện đó với Đại úy Colt. Ông đại úy còn bảo rằng cứ hễ gia đình nào mở tiệc là nơi đó cô Resker có mặt. Đúng là cái đồ tham ăn, tham uống…”
Cô Resker lập tức nhìn sang đại úy Colt mắt tóe lửa. Ngài đại úy bình thường đạo mạo thế, giờ vội rối rít chống chế: “Đâu có! Đâu có!”
Đúng lúc đấy, con mèo mướp nhà hàng xóm xuất hiện phía gần bên bờ rào uốn éo người rồi kêu “Ngheo! Ngheo!”. Con mèo Tobermory vừa thấy bóng tình nhân lập tức vọt theo.

Mọi người thở hắt ra như vừa trút được một gánh nặng. Đại úy Colt vội phân trần với: “Con mèo này chỉ toàn nói láo!”
Cô Resker sau một lúc ngập ngừng cũng chỉ trích: “Nó là một con mèo điên. Những điều nó nói không có một chút nào là sự thật!”.

Ông bà Blemley cũng hùa vào xỉ vả con mèo: “Gia đình tôi đã nuôi nấng nó bao nhiêu năm, ai ngờ nó lại đổ đốn học ở đâu cái thói đặt điều nói không thành có như thế!”
Cuối cùng tất cả mọi người nhất trí đồng ý: “Con mèo Tobermory từ lúc biết nói đã trở thành một loài động vật cực kỳ nguy hiểm, cần phải loại trừ nó ra khỏi cộng đồng xã hội!”
Ông Appin yếu ớt phản đối bảo rằng con mèo có thể học nói được tiếng người nhưng không thể biết nói dối. Lập tức đại úy Colt bịt miệng ông Appin lại, lấy còng tay ra bắt giữ do tội dạy dỗ con mèo không nghiêm túc dẫn đến việc nó toàn vu cáo mọi người và tống ông vào nhà kho.

Một lúc sau, khi con mèo Tobermory vừa vác xác về nhà, ngài Blemley lập tức lấy cái bao tải chụp thẳng lên đầu nó, đại úy Colt vác gậy bóng chày đập túi bụi lên cái bao tải trong khi bà Blemley và cô Resker đã chuẩn bị một xô nước to sẵn sàng. Mọi người xung quanh đều hưởng ứng nhiệt liệt.

Con mèo Tobermory lập tức bị dìm xuống nước. Nó kêu cứu bằng tiếng người. Không ai thèm nghe cái con mèo dối trá ấy nữa. Cuối cùng, nó gào lên “Ngao! Ngao! Ngao!” Chắc trước khi chết, nó đã nhận ra rằng thật quá nguy hiểm khi biết nói tiếng người. 
Tôi là Marilyn, Marilyn
Tôi là người  anh hùng
Của tự sát và bạch phiến
Những bông hoa mẫu đơn này còn rực rỡ cho ai?
Điện thoại này còn với ai trò chuyện ?
Còn ai đâu để sột soạt áo da hươu?
Không chịu nổi. 
 
Không chịu nổi sống mà không yêu
Không chịu nổi khi thiếu xanh rừng liễu  
Không chịu nổi tự sát
Nhưng sống
Lại càng không chịu nổi hơn!
Những trò bán chác. Bệnh ngoài da.
Lão sếp hí lên như ngựa thiến
(Tôi nhớ Marilyn
Những chiếc xe hơi mải miết ngắm cô
Trên màn ảnh hàng trăm mét rộng
Trên vòm trời mở như trang kinh thánh giữa muôn sao
Trên thảo nguyên giữa xinh xinh những chiếc khung quảng cáo
Marilyn thở phập phồng, thiên hạ đắm say cô
Những chiếc xe khát thèm, mệt mỏi
Không chịu nổi)
 
Không chịu nổi
Mùi chó bốc lên khi cúi xuống chỗ ngồi!
Không chịu nổi cưỡng bức
Nhưng tự nguyện càng không sao chiu nổi!
Không chịu nổi sống mà không nghĩ suy
Càng không chịu nổi cứ trở trăn nghiền ngẫm
Bao dự định đâu rồi. Ngỡ chúng thổi ta bay,
Tồn tại là tư sát.
 
Tự sát là đấu tranh với cái xấu xa
Tự sát là dàn hoà với chúng,
Không chịu nổi bất tài
Nhưng tài năng lại càng không chịu nổi.
 
Chúng ta tự giết mình bằng danh vọng bạc tiền
Bằng bọn gái màu da rám nắng,
Lũ đào kép chúng ta đâu sống với mai sau,
Còn đạo diễn là một bầy cặn bã.
 
Ta bóp nghẹt người thân trong vòng tay ôm riết,
Những mặt gối còn in dấu vết
Trên gương mặt trẻ trung như vết lốp xe hằn,
Không chịu nổi.
 
Các bà mẹ ơi, các bà sinh con ra để làm gì
Khi mẹ biết con sẽ bị đời giày xéo?
 
Ôi, cái kiếp lạnh lùng những ngôi sao màn bạc
Chúng ta không có nổi đến cả chút riêng tư
Trong xe điện ngầm   
Trên xe  buýt,
Hay trong cửa hàng
 Lũ ngốc trố mắt nhìn: “Cô đấy à, chào nhé!"   
 
Không chịu nổi cứ thế trần truồng 
Trên mặt báo, trên mọi tờ áp- phích,
Mà quên mất có trái tim đang đập
Và người ta đem gói cá mòi,
Mắt mũi nát nhàu, mặt mày rách toạc
(Chợt rùng mình khi nhớ lại trên tờ “Người quan sát Pháp"
Tấm hình mình với cái mõm tự đắc vênh lên
Mà phía sau là cái xác Marilyn!)
 
Lão bầu gào to, miệng nhồm nhoàm bánh ngọt:
“Cô mới tuyệt làm sao, trán lấp lánh chuỗi cườm!"
Mà chuỗi cườm  mùi gì, ngài có biết chăng?   
Mùi tự sát!
 
Những kẻ tự sát là những kẻ phóng mô-tô,
Những kẻ tự sát vội vàng hưởng thụ,
Những bộ trưởng tái xanh dưới ánh chớp đèn pha
Những kẻ tự sát,
Những kẻ tự sát,
Đang dẫn tới một Hyrôsima toàn thế giới,
Không chịu nổi.  
 
Không chịu nổi cứ  đợi chờ hoài phút giây bùng nổ
Mà cái chính là ở chỗ
Điều không thể hiểu đến không sao chịu nổi
Lại giản đơn như tay sặc mùi dầu, 
 
Không chịu nổi
Cháy lên tự màu xanh
Những trái cam ly biệt...
Tôi sức gái yếu hèn. Hỏi làm gì được ?
Tốt hơn là - xin hãy nhanh lên!
                                              
                                              1963
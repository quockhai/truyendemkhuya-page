Cạo râu xong, tôi đang ngồi ăn sáng cùng gia đình, thì có tiếng chuông gọi cửa. Người gọi là vợ anh bạn cũ của tôi, Xaít Hana. Chị ta xộc vào phòng, gào toáng lên:
- Anh giúp tôi với! Người ta định đưa nhà tôi vào bệnh viện tâm thần.
- Xaít?
- Đúng, anh giúp tôi!
- Giúp cái gì? Lý do sao?
- Tôi không biết. Sự thực là, thời gian gần đây, anh ấy bị trầm cảm, luôn lẩm bẩm một mình cái câu: "Tôi biết, nhưng không thể nói". Mọi người chúng tôi đã năn nỉ hỏi có chuyện gì, nhưng anh ấy chỉ ôm đầu, kêu lên: "Tôi biết nhưng không thể nói".
- Anh ấy đang ở đâu?
- Ở đồn cảnh sát.
- Họ giữ anh ấy từ bao giờ?
- Từ cách đây hai ngày.
- Về tội gì?
- Tội gây rối trật tự công cộng. Sau đó, họ dẫn anh ấy đến gặp bác sĩ. Bác sĩ khám và bảo anh ấy không có năng lực tự chịu trách nhiệm. Tôi van anh.
- Thế nghĩa là sao? - Tôi tự hỏi - Xaít vẫn nghĩ mình bình thường. Anh không uống rượu. Không thể như thế!
Tôi mặc vội quần áo, đi theo vợ anh đến đồn cảnh sát. Viên cảnh sát trực nói:
- Rất tiếc, anh ấy đã bị đưa vào bệnh viện tâm thần.
Có điều rõ là nếu không cố cứu, thì anh ấy sẽ suốt đời bị nhốt trong bệnh viện tâm thần. Chúng tôi bắt tắcxi đến bệnh viện. Phải rất vất vả chúng tôi mới được gặp anh. Trông anh tiều tụy như đã ở đó hai năm chứ không phải hai ngày. Nhìn thấy tôi, anh cười buồn. Chúng tôi ôm nhau hôn thắm thiết. Tôi hỏi:
- Anh bị sao? Ốm lâu chưa?
Anh nhìn vào mắt tôi, lặng đi một lúc rồi bình thản, đáp:
- Tôi biết nhưng không thể nói.
- Anh bảo sao? Anh gặp các cơn ác mộng à?
- Đừng hỏi thế! Hãy nghĩ cách đưa tôi ra khỏi đây! Có thể, rồi tôi sẽ nói.
Nghĩ anh không bị sao, tôi đi gặp bác sĩ trưởng để trình bày quan điểm của mình. Lúc đầu, bác sĩ nhất định không muốn thả Xaít, nhưng sau khi tôi phải đấu tranh với nhiều cấp khác nhau, chúng tôi mới đưa được anh về nhà. Ở nhà, anh trở nên đăm chiêu, buồn bã. Tôi gặng hỏi mãi, anh mới đáp:
- Tôi biết nhưng không thể nói.
- Xaít, anh nghe đây! Chúng ta đã là bạn với nhau trong nhiều năm. Anh luôn tin tôi. Nói đi! Anh bị sao? Tôi sẵn sàng giúp. Anh biển thủ?
- Không.
- Hay giết người?
- Tôi trói gà còn không nổi, thì làm sao giết được người?
- Thế, có chuyện gì?
- Chẳng có chuyện gì. Chỉ là tôi biết nhưng không thể nói.
Tôi hết chịu nổi. Tôi mang cuốn Kinh Côran đặt trước mặt anh, thề rằng sẽ không nói điều bí mật của anh với ai.
Nghĩ một lát, anh ngửng đầu, hỏi:
- Anh thề chứ!
- Thề.
Anh cúi xuống, nói thầm vào tai tôi :
-  Điều bí mật đó là sau ngần ấy ngày, tôi sẽ có thể ngủ ngon.
Anh bỏ đi. Nhưng suốt 10 ngày sau đó, tôi như con gà bị cắt cổ. Điều bí mật kinh khủng đó đã không để tôi yên. Tôi bị điên. Tôi muốn kể với mọi người, nhưng tôi không thể! Tôi thề sẽ giữ im lặng! Tôi biết nhưng không thể nói!
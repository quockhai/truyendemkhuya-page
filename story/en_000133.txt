1. - Phải công nhận mốt trang điểm hiện đại rất có lợi cho phụ nữ hiện nay!
 
- Vì sao vậy?
- Dễ hiểu thôi. Vì, từ khi phụ nữ dùng chì và mực vẽ để kẻ mắt và lông mi thì họ ít khóc hơn trước rất nhiều.
 
2. Sau hơn một giờ đồng hồ trang điểm, từ phòng trong bước ra, vợ hỏi chồng:
- Anh trông em có xinh không?
Vừa lúc đó có tiếng chó sủa, cô vợ lại giục chồng:
- Anh ra xem có ai vào?
Anh chồng thấy con chó Béc vẫn quay đầu vào nhà gâu gâu, liền nói:
- Không có ai vào đâu. Con Béc nó thấy người lạ ngay trong nhà đấy!

Minh họa: 123RF
3. - Anh xem em trang điểm như thế này có trẻ không?
- Trẻ, trẻ - người chồng đáp - Ít nhất cũng trẻ ra được 10 tuổi.
- Mới có mười tuổi thôi à? - Người vợ nũng nịu nói - Em muốn trang điểm thêm chút nữa, em muốn em trẻ thêm 15 tuổi cơ.
- Thôi được rồi em ơi! - Người chồng can ngăn - Trẻ hơn 15 tuổi người ta không cho vào rạp đâu!
- Tại sao vậy? - Cô vợ ngạc nhiên hỏi.
- Vì phim hôm nay người ta qui định cấm trẻ em.
 
4. Cậu con trai hỏi bố:
- Bố làm quen với mẹ như thế nào?
- À, lần đầu tiên gặp mẹ mày, bố thấy dáng quyến rũ quá - Ông bố vừa nói vừa liếc vợ - Rồi mẹ mày quay đầu lại và bố thấy mặt mẹ trang điểm... cũng đẹp và quyến rũ không kém.
 
5. Diễn viên phim cao bồi đóng vai tù trưởng Da đỏ sắp lao vào cuộc chiến giáp lá cà, ngồi cho chị nghệ sĩ hóa trang sơn sửa tật lâu, sốt ruột hỏi:
- Thế nào, xong chưa? Tôi lên sàn diễn được chứ?
- Được đấy, nhưng chờ tôi treo vào cổ anh cái biển để báo động cho các bạn diễn của anh.
- Báo động cái gì?
- Sơn còn ướt!
 
6. - Con nhỏ này từ ở quê lên sao bây giờ trang điểm, ăn diện model, nhảy đầm điệu nghệ thế?
- Mới ở quê lên là tập 1, bây giờ đã là tập 5 rồi, ông ạ - Cô  gái thản nhiên đáp lại.
 
7. Nhà sinh học đang giảng về cấu tạo mắt thì một học viên hỏi:
- Tại sao phụ nữ liếc giỏi hơn đàn ông?
Nhà sinh học trả lời:
- Vì mắt của các cô ấy được kẻ dài hơn.
 
8. Một bà tuổi đã ngoài sáu mươi nói với chồng:
- Này, khi em đã cắt tóc rồi, trông em không còn giống một bà già nữa chứ?
- Tất nhiên rồi. Giờ thì trông giống một ông lão hơn!
 
9. Chàng tặng nàng viên kim cương nhân lễ cầu hôn:
- Em nhìn xem: Viên kim cương không một chút khuyết tật.
- Tất nhiên là như thế, anh yêu! Nó bé tí thế này thì làm sao mà có khuyết tật được?
 
10. - Anh thấy em hôm nay có tuyệt mỹ không? Nước da trắng hồng, thân hình cân đối, gương mặt kiều diễm... Này, nếu anh là chủ cuộc thi, anh chấm em mấy điểm?
- Anh sẽ cho em điểm 10.
- Ồ, tuyệt quá...
- Nhưng điểm mười về trí tưởng tượng phong phú của em!
 
11. Anh chàng nói với người yêu:
- Sao dạo này các cô lại tốn nhiều thời gian vào việc tô son đỏ, đánh phấn hồng em nhỉ?
- Thế các anh thích xem phim màu hay phim đen trắng?
- ?!...
 
12. Một bà có vóc dáng đẫy đà mặc áo màu đỏ chói vào một hiệu may nổi tiếng và hỏi chủ hiệu: 
- Chẳng hay theo ông, màu nào hợp với tôi? 
- Thưa bà, khi Thượng đế tạo ra con chim sâu và con bươm bướm thì Người khoác cho chúng những màu sắc sặc sỡ. Nhưng khi Người tạo ra con voi thì Người mặc cho nó màu xám vậy.
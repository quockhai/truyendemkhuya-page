Nhà văn Tuyết Mạc sinh tại tỉnh Cam Túc năm 1963, là một trong nhà văn hàng đầu Trung Quốc đương đại, đồng thời cũng là một hành giả Đại Thủ Ấn. Hiện nhà văn là hiệu trưởng của Viện Văn hóa phái Shangpa Kagyu (Phật giáo Tây Tạng), Quảng Châu.
 
Tuyết Mạc vừa là nhà văn vừa là một hành giả. Ông là người đi tiên phong trong việc sáng tác văn học đương đại thông qua Phật giáo Tây Tạng. Những cuốn sách của ông đã đạt được nhiều giải thưởng văn học. Trong buổi hội thảo do Hội Nhà văn Trung Quốc tổ chức vào năm 2011, các nhà phê bình đồng ý rằng, Tuyết Mạc là nhà văn xứng đáng đứng “vào tầm nhìn của văn học thế giới”.
 
Tuyết Mạc kiên trì suy nghĩ và viết về cuộc đời và linh hồn, về sự khôn ngoan, sự tồn tại dai dẳng nhất và lương tâm trong sạch của người dân Trung Quốc. Trong bộ mặt của nền văn minh nông nghiệp đang mờ dần các tác phẩm của Tuyết Mạc phản ánh sâu sắc và trải lòng từ bi lớn lao đến sự phân hạch đất tại các vùng nông thôn Trung Quốc, thực tế nghiệt ngã của sự sống và lịch sử trải qua những cơn ác mộng kinh niên.
 
“Ông Lão Tân Cương” là một câu chuyện phản ánh sự thăng trầm của cuộc sống tại vùng nông thôn Trung Quốc.
 
 
 
.TUYẾT MẠC (Trung Quốc)
 
Ông lão Tân Cương bắt đầu gói ghém gian hàng của mình. Trời vẫn còn sớm. Mặt trời, một màu vàng trên nền những đám mây trắng như váng sữa ở phía đông chỉ mới bắt đầu di chuyển về phía tây. Một làn gió nhẹ lướt qua mặt đất, nghe rõ tiếng sột soạt của lá rụng, mang theo mùi của mùa thu. Ông lão Tân Cương gói ghém trái cây sau đó đến những quả trứng. Gian hàng của ông không có gì ngoài hai chiếc giỏ và hai mảnh bìa các tông. Những quả trứng được xếp trong một chiếc giỏ, giỏ kia thì đựng những trái lê, trái nào trái ấy khi chạm vào đều mềm mềm, da mỏng và khi cắn một miếng, dòng nước ngọt lạnh tới tận họng rất tốt cho những người bị bệnh ho. Trứng và lê... đó là tất cả những gì lão có, rất đơn giản để bày ra và gói ghém lại để cất đi. Lão đã trả 40 xu để mua trái cây và bán chúng với giá 45, mua mỗi quả trứng 20 xu, bán 22 xu. Lão xoay sở kiếm sống từ gánh hàng này, không nhiều hơn thế.
 
Ông lão Tân Cương treo mỗi chiếc giỏ vào mỗi đầu đòn gánh, nhấc lên vai và đi về phía đông cuối làng. Dáng ông lão cao và gầy, đổ một bóng dài như đang bò bên cạnh giống một loài động vật nhiều chân khổng lồ. Lão nhanh chóng vướng vào một vụ xô xát, mắt lão sáng lên. Người dân trong làng đều đứng nhìn ông lão. “Lão đang đi đâu đó”, có người hỏi. “Đến nhà bà ấy,” ông lão trả lời. Họ không hỏi bà ấy là ai. “Để đưa tiền cho bà ta à?” Một giọng lẩm bẩm. “Và lão sẽ thỏa mãn khi trở về chứ?” Những người khác phá lên cười. Ông lão Tân Cương bối rối, cố gắng tách ra khỏi đám đông nhưng lão lại bị bao quanh. “Lão vẫn làm điều đó, đúng không?” Ông lão Tân Cương hạ quang gánh xuống đất và lấy tay đấm cái lưng đau. “Đừng nói chuyện vô nghĩa nữa. Tôi chỉ là một lão già.” Những tiếng cười lại rộ lên. “Lão sẽ không bao giờ già nếu lão không bận tâm đến tuổi tác”, một người nói. Và một người khác thêm vào: “Nếu công cụ đã vô ích, thì có vấn đề gì mà không dùng tay? Một chút cọ xát sẽ tạo nên sự hưng phấn đấy.” Không thể chịu đựng thêm được nữa, lão Tân Cương lại quẩy quang gánh lên vai và nhanh như có thể bỏ đi giống như một con thỏ.
 
“Nhà của bà ấy” là một căn lều dột nát với những mảnh thạch cao bong ra khỏi bức tường như lớp da bị bệnh. Khi lão đến, người đàn bà đang bận rộn đào một con mương, quần áo và khuôn mặt của bà đầy bụi. Bà đặt cái thuổng gỗ xuống và phủi bụi trên quần áo. Họ chào nhau. Lão đi vào trong. Tấm giấy dán cửa sổ khiến ánh sáng hắt vào lờ mờ. Có một cái giường bằng gạch bùn, sưởi ấm bằng một ống khói từ bếp. Một ông già với đôi mắt đỏ ngầu ngồi đó, tay cầm tẩu thuốc. Ông châm một mồi thuốc từ ngọn đèn dầu, rít một hồi trong bát ống cho đến khi khói tỏa ra khỏi hai lỗ mũi của mình. Ông chuyển mình khi nhìn thấy lão già Tân Cương và lầm bầm chào. Ông lão Tân Cương lấy một chiếc ghế thấp và ngồi xuống bên cạnh, im lặng.
 
“Lại một vụ mùa thất thu trong năm nay”, ông già mắt đỏ than thở.
“Tệ thật”, ông Tân Cương đồng ý.
“Không biết năm sau sẽ thế nào nữa?”
“Ai biết được?”
“Đó là cuộc sống, phải không?”
Người đàn bà đi đến, vừa phủi bụi từ quần áo của mình.
“Ông có lạnh không?” bà hỏi, nhìn ông lão Tân Cương.
“Không lạnh lắm.”
“Ông nên mặc áo khoác vào chứ.”
“Ừ.”
“Ông cũng nên giặt quần áo đi.”
“Ừ.”
“Ngày mai tôi sẽ hái rau quả, và sẽ rửa sạch cho ông.”
“Để tôi hái rau cho”, ông lão đề nghị, “Bà hãy giặt đồ cho lão ta đi. Không biết trước thời tiết sẽ thế nào đâu.”
“Hãy ở lại đây ăn với chúng tôi”, người đàn bà bảo, “Tôi sẽ nấu mì cho ông.”
Nhưng lão Tân Cương từ chối: “Không, tôi sẽ không ở lại đâu. Tôi phải đến gặp bác sĩ. Tôi bị cảm lạnh thì phải.”
“Ông nên mặc áo khoác vào.”
“Đúng vậy”, ông lão Tân Cương gật đầu và quẩy quang gánh lên vai bỏ đi. Đôi vợ chồng già không bận tâm đóng cánh cửa.
 

Minh họa: Lê Trí DũngBên ngoài gió lạnh, mũi của lão bắt đầu ngứa và muốn hắt hơi. Lão có cảm giác kỳ lạ như là có một con rệp trong lỗ mũi của mình đang tìm cách bò ra. Lão muốn được tiêm một mũi, lão vừa nghĩ vừa lau nước mũi. Nhưng đó chỉ là do quá lạnh khiến lão hắt hơi liên tục - lão không thể phàn nàn thêm bởi vì lão đã không mắc bệnh trong năm nay. 
 
Hầu như không có ai ở phòng bệnh, ngoài hai người đàn ông và một đứa trẻ. Lão đã chọn ra một trong những quả lê, đưa cho đứa bé, và ngồi xuống. Lão đợi cho những người đàn ông nói điều gì đó, nhưng họ vẫn ngồi im lặng, nhìn đứa trẻ nhai lê nhóp nhép, nước miếng và nước ép quả lê chảy trào cả ra khóe miệng. Lão sẽ không cho họ những trái lê của mình, lão nghĩ rằng sẽ không thể cho tất cả. Những người đàn ông lần lượt vào khám, người đầu tiên, sau đó đến người khác.
Khi đến lượt mình, lão nói với bác sĩ: “Tôi muốn một mũi tiêm. Hãy cho tôi một mũi penicillin, được không? Đó là thứ duy nhất tôi biết.”
 
Vị bác sĩ cười tự mãn: “Lão cần được nghỉ ngơi khi bị cảm lạnh. Đừng đeo đuổi đàn bà nếu không lão sẽ kiệt sức và đó sẽ là sự kết thúc của lão!”
 
Ông lão Tân Cương đỏ mặt. “Anh nói vớ vẩn gì vậy! Anh là một người đàn ông có học thức, bác sĩ, không phải là một người dân làng dốt nát...”
 
 “Có thật sự là lão đã không làm điều gì?” vị bác sĩ bình tĩnh hỏi. “Tôi có thể làm thế nào? Khi một người phụ nữ đã kết hôn với một người khác, điều đó hoàn toàn là sai trái” Ông lão Tân Cương cảm thấy một giọt mồ hôi nhỏ xuống ở cuống mũi của mình. “Những điều đáng kể đến trong cuộc sống này là sự trung thành với bạn bè của bạn.”
 
Bác sĩ nhìn ông lão: “Nhưng bà ấy là vợ của ông trước mà. Chả có gì tội lỗi gì khi ngủ cùng bà ta.”
“Bà ấy ... bà ấy ...” Ông lão Tân Cương lắp bắp, mặt ông tái đi.
“Lão bao nhiêu tuổi khi bọn đi bắt lính đến?”
“Hai mươi.”
“Có phải đó thực sự là buổi sáng đầu tiên sau đêm tân hôn không?”
“Đúng vậy.”
“Và lão thực sự đã trở về từ Tân Cương bằng cách đi bộ ư? Lão đã không gặp một chuyến xe nào ư?”
“Đúng vậy.”
 
Ông lão Tân Cương không cảm thấy ngại ngùng để trả lời bất cứ điều gì. Lão đã được hỏi những câu hỏi như vậy hàng trăm lần, bởi người này người kia, và lão đã quen với việc chịu đựng nó. Đúng, lúc đó lão đã hai mươi, hoặc có lẽ hơn một chút. Đã quá lâu rồi - những kỷ niệm của lão đã phai mờ, giống như một giấc mơ. Những gì lão nhớ là Tân Cương rất xa, và lão bị buộc phải ra đi. Có rất nhiều người trong số họ, họ thậm chí bị trói lại với nhau. Bọn bắt lính thực sự đã đến và kéo họ ra khỏi nhà và đưa họ đến trại quân đội. Lão đã ở trại lính trong nhiều năm... Sau này khi người ta hỏi Tân Cương như thế nào? Lão chỉ nói: “Tôi không biết. Tôi chỉ nghĩ về vợ tôi”. Lão thậm chí chưa được ngắm khuôn mặt bà rõ ràng, nhưng bà vẫn còn là vợ của lão. Vì vậy mà lão bỏ trốn.
 
Mấy lần đầu tiên, lão bị bắt và bị đánh đập nửa sống nửa chết. Mãi đến lần thứ năm, lão mới thoát được trở về nhà. Không biết lão đã đi mất bao lâu? Lão không rõ. Lão chỉ nhớ dù thế nào lão vẫn tiếp tục, cả ngày và đêm, đôi khi có ngủ một chút. Lão có đã mất một tháng trên đường, hoặc có thể là một năm. Lão không nhớ được nữa? Khi lão về tới nhà, vợ của lão đã kết hôn với một người đàn ông khác. Anh trai của lão không đủ khả năng để nuôi cô ấy và nghĩ rằng lão đã chết. Vì vậy, anh trai của lão đã bán cô đi, và vợ của lão Tân Cương đã thuộc về một người đàn ông khác. Lão Tân Cương không có đồng xu nào để chuộc lại vợ mình. Người đàn ông khác có lẽ sẽ tốt hơn trong những ngày đó, vì vậy bà đã đi theo ông ta, hy vọng có một cuộc sống tốt đẹp hơn. Là như vậy đó. Nhưng mọi người vẫn cứ tiếp tục hỏi, và hỏi...
 
“Thật sự quá bất công với lão”, vị bác sĩ nói, “Chỉ được một lần cùng bà ấy.”
Lão Tân Cương mỉm cười và thầm nghĩ, tôi đã thậm chí không nhận được điều đó. Lúc đó cô ấy đang đến tháng.
“Lão không giận anh trai của lão ư?”
“Được gì khi tức giận đây? Anh chỉ nhận được những gì cuộc sống ném vào anh mà thôi.”
“Tại sao lão không tái hôn nữa?”
“Tại sao phải bận tâm điều đó? Tôi có những gì cuộc sống đã ban cho tôi.”
Lão Tân Cương liếc nhìn bầu trời bên ngoài cửa sổ, những tán cây dưới vòm trời và những chiếc lá vàng rụng xuống trong cơn gió mùa thu. Khuôn mặt lão như đã được chạm khắc trên gỗ. Như thể toàn bộ câu chuyện này không có gì đáng nói với lão.
 
Vị bác sĩ liếc nhìn xuống cánh tay của lão. “Cởi quần ra đi nào”, ông nói. Lão già Tân Cương kéo quần xuống, để lộ một cặp mông gầy.
“Hãy cho tôi một mũi vào thịt ấy”, ông nói. “Lần trước, anh ấn vào xương và tôi không thể ngồi xuống trong một tuần.”
 
Bác sĩ cười: “Lão chả còn tý thịt nào cả, tôi chỉ có thể giữ lệch một chút vào da, đó là tất cả. Lão nên ăn nhiều vào, thay vì dành từng xu có được cho bà ấy. Bà ấy đã kết hôn với một người khác. Tại sao lão lại làm phiền bà ấy?” Lão Tân Cương không nói gì. “Lão đã hành hạ bản thân quá nhiều rồi”, bác sĩ tiếp tục. “Nó chả mang lại điều gì tốt đẹp cả.”
 
“Anh lại nói thế nữa rồi”, Lão già Tân Cương than thở “Và anh, một người đàn ông có giáo dục...” Vị bác sĩ chèn da lại và tiêm cho lão. “Lần này mũi tiêm đã đi vào thịt, tôi chỉ thấy đau một chút thôi”, lão già Tân Cương nói.
Vị bác sĩ cười và phát vào mông của lão như thể lão là một con ngựa: “Lão có thể tỉnh táo ngay bây giờ, nhưng chú ý đừng làm rạn nứt quan hệ vợ chồng với những chiếc xương nhọn này.”
“Ôi, đau quá”, lão Tân Cương phàn nàn.
“Xem này, những chiếc xương của lão kêu như tiếng chuông tu viện”, bác sĩ nói.
Lão Tân Cương về nhà và cất giỏ của mình. Chúng nhẹ hơn đáng kể, và lão cảm thấy bứt rứt. Lão lắc đầu. Lão tự nghĩ mình cần phải thông minh hơn trong cuộc sống này.
 
Nhà của lão rất nhỏ. Có một cái giường, một cái bếp lò bằng gạch bùn, và một cửa sổ cao hẹp. Những vách xà và các bức tường đen kịt vì khói, giấy bọc cửa sổ đã ố vàng khiến căn phòng tối tăm hơn. Nhưng lão thích như vậy. Lão sống một mình.  Lão thấy ấm cúng và lão có thể đóng cửa với thế giới bên ngoài chỉ bằng một cánh cửa. Một cảm giác ấm áp lan tỏa khắp người. Ngôi nhà này thật tốt đối với lão. Nó vẫn che được mưa và gió, và không quấy rầy lão với những câu hỏi. Lão sợ các câu hỏi của họ. Sau nhiều năm, lão chỉ muốn bỏ tất cả lại đằng sau. Những câu hỏi đã gợi lại những kỷ niệm buồn và đau khổ.
 
Lão già Tân Cương nhóm lửa, rửa sạch khoai mỡ và cắt nó thành từng miếng nhỏ trên thớt. Khoai mỡ rất ngon. Chỉ cần đảo chúng trong chảo vài phút cho mềm là lão có thể nuốt chửng chúng một cách dễ dàng. Răng của lão đã rụng hết từ những năm trước. Lão không thể nhai các loại rau quả khác và chúng làm lão thấy khó tiêu. Lão cắt nhỏ khoai thành từng miếng để chúng sẽ mềm một cách nhanh chóng nhưng cũng có thể dễ dàng gắp bằng đũa. Tay của lão không run nhưng chúng thật vụng về.
 
Thớt nhỏ, chỉ rộng bằng gang tay. Lão đã có nó suốt nửa đời và thường xuyên phải sử dụng nó. Nó thực sự rất hữu ích. Lão có thể cắt bất cứ thứ gì trên đó mà không để lại dấu. Có lẽ người thợ mộc muốn làm cho lão một cái mới, nhưng lão thấy không cần thiết phải dùng cái khác. Cái thớt này là của riêng lão và chỉ một cái này là đủ. Trong những năm qua, những người khác thay đổi nhiều cái thớt mới, nhưng lão vẫn sử dụng cái này. Đúng, loại gỗ này thực sự tốt. Sau nhiều năm, nó chỉ có mỏng hơn một chút.Điều đó cũng tốt thôi. Nó sẽ nhẹ hơn. Bây giờ lão đã già, nên lão rất vui vì nó nhẹ hơn.
 
Khi đã thái khoai xong, lão liếc nhìn vào bếp. Bếp lò bằng gạch bùn rất dễ sử dụng, vì gạch bùn bén lửa rất nhanh. Lão đặt chiếc chảo nhỏ lên và cho  dầu vào. Lão lấy đũa gắp từng miếng, ngâm chúng vào chảo dầu. Một mùi thơm ngậy bốc lên. Đó là dầu mè, loại mà lão thấy thích hơn so với dầu hạt cải. Mặc dù khi không có dầu mè, lão vẫn phải sử dụng dầu hạt cải nhưng mùi vị của nó vẫn rất hấp dẫn. Và nếu như không có mùi thơm bốc lên, điều đó chứng tỏ lão đã nấu mà không có dầu. Lão luôn luôn có mì và khoai mỡ trong nhà. Ngoài ba năm của nạn đói, khi họ không có khoai mỡ, hoặc bất cứ thứ gì khác, lão đã sống nhờ vào những hạt kê. Dù sao đi nữa lão đã không bị chết đói trong khi rất nhiều người đã phải bỏ mạng. Lão là người may mắn. Rất may mắn. Lão đã vượt qua nạn đói mà không mắc bất kỳ bệnh nặng hay thiên tai nào. Nhưng sau đó, lão vẫn phải hứng chịu những gì cuộc sống đưa đến, cả tốt hay xấu...
 
Ngôi nhà khá yên tĩnh, tiếng ồn thường xuyên chẳng gì ngoài tiếng lầm bầm của chính lão. Những lát khoai lang kêu xèo xèo trong chảo dầu. Đó là âm thanh còn hay hơn các bài hát trên loa phóng thanh của làng. Không có bất cứ điều gì lạ lùng trong giọng của người phụ nữ, nhưng những gì lão thực sự thích là giọng hát vùng Thiểm Tây, giọng hát có âm vực cao và nhịp điệu bốc lửa. Lão chưa bao giờ mua một đài phát thanh cho riêng mình nên lão đã không được nghe trong nhiều năm. Nhưng đối với lão, âm thanh réo lên của khoai mỡ nóng nghe thật vui tai. Đáng tiếc nó đã không kéo dài, vì lão phải cho thêm nước.
 
Lão Tân Cương bày ra một nồi nước. Đó là tất cả những gì lão có trong mỗi bữa ăn. Nó không phải là một chiếc nồi lớn, kích thước chỉ bằng một cái bát, nhưng chỗ nước đó là đủ cho một bữa ăn. Nó lơ lửng trong bình nước cả ngày, bồng bềnh nhẹ nhàng từ bên này sang bên kia. Đây là một vật dụng khác lão đã có trong nhiều thập kỷ. Nó không có nắp đậy, nhưng cũng chả sao. Ban đầu cũng có đấy, nhưng một hôm lão muốn để nó trên bếp lò để đun nước và con mèo với cái mũi trắng đã khiến nó bay đâu mất. Mặc dù vậy nó vẫn còn hữu ích như một chiếc nồi.. Nhưng một chiếc nồi không vung cũng còn tốt chán. Thật khó để giải thích những điều trong cuộc sống này, một chiếc nồi không vung rất hữu dụng cho một số việc và một chiếc nồi có vung lại tốt trong những điều kiện khác. Và những người sử dụng mới là quan trọng hơn?
 
Nước đã sôi. Lão Tân Cương đã bắt đầu chế biến mì. Lão có một chiếc bát lớn của Trung Quốc. Nó dày, nặng và cứng. Bạn không thể mua được cái bát như thế trên thị trường nữa. Những loại rắn chắc có nhiều công dụng, và lão đã dùng cái bát này vừa để ăn vừa để trộn bột mì. Nó cũng đỡ tốn hơn khi mua một bát trộn đặc biệt. Lão cho một muỗng bột và một ít nước vào trong bát, rồi nhào bột. Lão đã nhào bột một cách nhanh chóng và vo lại một nắm bằng nắm tay của mình. Sau đó, lão san phẳng nó lên thớt và cắt thành những dải dài với con dao của mình. Từng dải một, lão vo vo mỗi dải giữa lòng bàn tay của mình. Đó là một công việc đơn giản và nhanh chóng. Một món ăn không mất nhiều thời gian để chuẩn bị.
Lão đã ăn như thế này trong nhiều năm.
 
Lão thực sự đã già, và thực phẩm giàu chất sẽ làm cho lão khó tiêu. Lão thích ăn đơn giản với nhiều chất lỏng. Cũng như một bữa ăn đơn giản, giống như lấy một cái ghế và ngồi xuống để ngắm nhìn các ngôi sao và mặt trăng. Mặt trời lên và xuống, lá cây bắt đầu xanh và dần chuyển màu vàng. Đó là những điều tốt đẹp trong cuộc sống và không ai có thể đánh cắp của lão.
 
Đã chạng vạng tối và bóng tối dần bao phủ xung quanh lão. Bữa tối của lão Tân Cương đã được nấu xong, lão mang bát ra cửa ra vào và ngồi xuống. Lão gắp một miếng cho những linh hồn người giám hộ. Sau đó, lão bắt đầu húp xùm xụp bát mì của mình. Hơi nước bốc lên từ bát nước, bay qua đầu lão. Lão để một bát mì khác ở đằng trước mặt, chuẩn bị cho một người bạn. Ngay sau đó, một con chó đen từ từ xuất hiện từ ngôi nhà của người phụ nữ ở phía đông của ngôi làng trong ánh trăng nhợt nhạt. Yên lặng thưởng thức bữa ăn tối của nó, sau đó ngẩng đầu lên ra vẻ thích thú lắm. Đó là lúc mà lão Tân Cương cảm thấy hài lòng nhất. Chỉ có lão và con chó, lão có thể quên chính mình và tất cả dân làng.
 
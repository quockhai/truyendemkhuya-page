Giữa đám con gái mà tôi biết, Inem là người bạn tốt nhất của tôi. Nó mới tám tuổi, hơn tôi đúng hai tuổi. Nó dễ thương và đẹp nhất trong đám con gái hàng xóm của tôi. Nó sống trong nhà bố mẹ tôi. Đổi lấy chỗ ăn ở đó, nó phụ việc bếp núc và trông nom tôi cùng với mấy anh em ruột thịt của tôi.

Thêm vào cái đẹp vốn có, Inem rất lịch lãm, thông minh và chăm chỉ. Tiếng lành đồn xa làm nó nhanh chóng hoà nhập với láng giềng. Chẳng bao lâu nữa nó sẽ trở thành cô con dâu tốt, người ta nói vậy. Và để khẳng định điều này, một buổi khi Inem đang đun nước uống trong bếp, nó tuyên bố với tôi: “Tôi sắp đi lấy chồng”.

“Không, cô không thể”. Tôi nói với cô ấy.

“Tôi sẽ, thực sự tôi sẽ lấy chồng. Có lời chạm ngõ từ tuần trước. Gia đình tôi cùng những người thân đều nghĩ đó là ý tưởng hay”

“Ôi dào, thật nực cười”. Tôi gào to lên.

“Chắc chắn sẽ như vậy”. Cô ấy đồng ý. “Họ sẽ sắm cho tôi tất cả quần áo đẹp và mới. Và tôi sẽ mặc váy cô dâu, trang điểm lộng lẫy tay ôm hoa, tôi sẽ giống như vậy này.”

Inem đã nói đúng sự thật. Vào một buổi chiều sau đó không lâu mẹ của Inem đến và đưa nó rời khỏi nhà tôi.

Mẹ Inem kiếm tiền bằng cách may áo Batik, cũng tần tảo như cái cách những người đàn bà lao động trên cánh đồng lúa. Đàn bà vùng này, một số làm Kain, Batik, trong khi vài người khác làm khăn quấn đầu. Những người đàn bà nghèo hơn như mẹ Inem có thể làm khăn quấn đầu. Trung bình một phụ nữ có thể làm khoảng từ tám đến mười một chiếc khăn trong một ngày. Toko Ijo, cửa hàng sẽ mua những chiếc khăn do mẹ Inem làm, họ cung cấp tất cả phụ liệu như vải bông và sáp nến. Cứ sau hai chiếc khăn mẹ Inem làm xong, bà sẽ được trả một phẩy năm xu Dutch.

Bố Inem là tay cờ bạc - đặc biệt là chơi chọi gà. Khi con gà chọi của ông ta bị thua, ông ta phải nộp hai phẩy năm Rupiah hoặc ít nhất cũng phải 75 xu Dutch. Khi ông ta không chơi trò này thì ông ta chơi bài với hàng xóm với một xu trong tay.

Đôi khi bố Inem đi kiếm ăn, không về nhà hàng tháng, có khi lâu hơn. Nói chung khi ông về nhà có nghĩa là trong tay đã kiếm rủng rỉnh tiền bạc.

Một lần, mẹ tôi kể rằng, nguồn thu nhập chính của bố Inem là từ các vụ cướp bóc trên đường chạy dọc theo các đồn điền cây lấy gỗ nằm giữa Blora, thành phố của chúng tôi và thành phố biển Rembang. Lúc đó tôi mới học lớp một, đã được nghe nhiều về các băng nhóm tội phạm, giết chóc và trộm cướp. Do tiếng xấu về họ và vì những gì mẹ tôi kể, tôi đâm ra rất sợ bố Inem.

Ai cũng biết bố Inem là trộm cướp, nhưng không ai dám tố giác ông ta với cảnh sát. Hơn nữa vì không một ai có thể chứng minh ông ta là tên trộm, nên ông ta không bao giờ bị bắt. Ngoài ra, hình như phần lớn người thân tín họ hàng bên mẹ Inem đều là cảnh sát. Một người còn làm tới thanh tra. Thậm chí có tin cả bố Inem đã từng làm cảnh sát, nhưng bị mất việc và sa thải do nhận hối lộ.

Mẹ Inem kể với tôi rằng, bố Inem đã là kẻ trộm từ trước khi thành cảnh sát và rằng khi chính phủ dùng các biện pháp để trấn áp tội phạm trong vùng đã mời ông tham gia cảnh sát trong đội ngũ cộng tác viên, cho đến khi đuổi hết được bọn cướp. Nhưng điều này cũng không làm bà và những người khác hết nghi ngờ ông.

Ngày mà mẹ Inem đến gọi con về lấy chồng Inem đang ở trong bếp đun nước. Khi mẹ ra chào hỏi khách, tôi quan sát thấy họ trang trọng như ngồi họp trong phòng khách. Tất cả mọi người ngồi trên một chiếc phản gỗ thấp.Chính mẹ Inem là người mở đầu trước:

“Thưa bà, tôi đến để xin cho cháu Inem về nhà”.

“Nhưng tại sao cơ chứ? Ở đây không tốt sao?” Mẹ tôi chất vấn. “Bà không phải trả gì hết cho con bé ở đây và nó đang học nấu ăn.”

“Tôi biết vậy, thưa bà, nhưng tôi đã định gả chồng cho cháu sau vụ mùa thu hoạch tới.”

“Lấy chồng ư?” Mẹ tôi ngạc nhiên.

“Vâng, thưa bà. Nó đã đủ lớn thưa bà, đủ tám tuổi rồi.” Mẹ Inem nói và khẳng định như vậy.

Lúc đó mẹ tôi cười, phản ứng lại sự ngạc nhiên của vị khách.

“Mới tám tuổi? Không sớm quá sao?” Mẹ tôi hỏi lại.

“Chúng tôi không thuộc tầng lớp người giầu có, thưa bà…”

Mẹ tôi cố an ủi bà, nhưng mẹ Inem chẳng nghe thêm nữa. Bà chỉ cảm thấy hạnh phúc vì có lời cầu hôn của người ta. Bà tranh luận: “Và nếu chúng tôi bỏ qua lời cầu hôn này thì cháu sẽ không còn dịp nào khác. Không có người nào dám đến dòm ngó nữa đâu. Hãy hình dung xem mình có cô con gái chết già trong nhà. Ngoài ra, khi nó đi lấy chồng sẽ là tấm gương tốt cho các em trong nhà”.

Mẹ tôi cho qua chuyện và bảo tôi đi lấy khay trầu. Tôi làm như sai bảo.

“Thế còn ông nhà bà, ông nói sao về việc này?”

“Ôi, bố Inem đồng ý quá đi chứ,” mẹ Inem khẳng định. “Đặc biệt là Markaban - đó là tên cậu bé cầu hôn con gái tôi - con trai một gia đình giầu có và là đứa con trai độc nhất. Nó đã bắt đầu buôn bán giúp cha, tung tẩy từ Rembang, Cepu, Medang, Pati, Ngawen và ngay cả ở đây, tại Blora”.

Thông tin này làm mẹ rất vui. Sau đó bà gọi Inem, lúc này vẫn còn đang nấu nướng trong bếp. Khi Inem bước vào phòng, bà mẹ hỏi ngay “Inem con có muốn đi lấy chồng không?”

 Inem cúi đầu, nó khoác tay mẹ với sự kính trọng và luôn luôn phục tùng bà. Tôi nhận ra rằng Inem luôn luôn mỉm cười, duyên dáng. Nếu bạn cho Inem một cái gì đấy làm nó hạnh phúc, nó luôn đáp lại bằng một nụ cười rộng mở. “Xin cám ơn” không phải là câu cửa miệng mà Inem thường nói. Giữa những người dân trong vùng chúng tôi ở, từ “cám ơn” vẫn còn rất xa lạ. Chỉ một nụ cười mãn nguyện, cái nhìn hạnh phúc trên gương mặt ai đó là cách thể hiện lòng cám ơn chân thành rồi.
“Vâng, thưa mẹ,” Inem cuối cùng cũng thì thầm trả lời, hoàn toàn tự nguyện.

Mẹ tôi và mẹ Inem mỗi người lấy một miếng trầu cau. Mẹ ít khi ăn trầu, bà chỉ ăn khi có khách là đàn bà đến chơi nhà. Sau vài phút im lặng trong phòng vang lên tiếng nhổ nước trầu vào ống nhổ.

Sau khi Inem đi vào bếp, mẹ tôi từ từ tuyên bố: “Thật không phải, không được phép cho trẻ con đi lấy chồng”.

Mẹ Inem nhíu đôi lông mày, nhưng bà không đáp từ. Tôi nhận thấy trong đôi mắt bà không chút tò mò, không mảy may ngạc nhiên.

“Tôi lấy chồng khi tôi mười tám tuổi”, mẹ nói.

Cái nhìn trên gương mặt của mẹ Inem thất sắc. Câu hỏi không nói ra chính là câu trả lời. Dù sao thì bà cũng không nói gì.

“Thật không phải phép khi bắt trẻ con đi lấy chồng.” Mẹ nhắc lại.

Một lần nữa, mẹ Inem ngạc nhiên tựa như bà không biết nói thế nào cho phải.

“Trẻ con của chúng ta sẽ bị thui chột hay sao”.

Một lần nữa cái nhìn bối rối thể hiện trên gương mặt của mẹ Inem.

“Tôi tin chắc, bà nói đúng đấy ạ...” Sau đấy bà lại dẫn chứng, “Mẹ tôi cũng đi lấy chồng khi bà mới có tám tuổi đầu”.

Tựa như không nghe thấy bà khách nói gì mẹ tôi tiếp tục: “Trẻ con không chỉ bị thui chột đi, mà sức khoẻ cũng bị ảnh hưởng nghiêm trọng đấy.”

“Tôi tin bà nói đúng đấy ạ. Nhưng gia đình tôi có tuổi thọ rất cao. Mẹ tôi vẫn còn sống đấy thôi, bà đã năm chín tuổi. Bà tôi cũng đang sống. Bà có lẽ ở độ tuổi bảy mươi tư. Bà vẫn còn rất khỏe, khỏe đủ để nhai ngô rang nữa cơ mà.”

Vẫn không để ý đến mẹ Inem, mẹ tôi nói thêm, “đặc biệt nếu chồng cũng trẻ con nữa.”

“Tất nhiên, thưa bà. Nhưng Markaban đã mười bảy rồi.”

“Mười bảy ư! Chồng tôi ở độ tuổi ba mươi khi lấy tôi đấy.”

Mẹ Inem không nói thêm gì nữa. Bà đưa miếng trầu nhai, cầm ra tay rồi đưa lên đưa xuống vòng quanh miệng và môi...

Mẹ tôi không tiếp tục tranh cãi nữa, tránh gây căng thẳng cho bà khách đến chơi nhà.

“Thôi nếu bà đã quyết gả chồng cho Inem, tôi chỉ hy vọng rằng nó sẽ có được người chồng biết quan tâm chăm sóc nó. Tôi chỉ mong sao chồng nó là người đàn ông đích thực của nó, người có thể quyết định số phận của nó sau này.”

Mẹ Inem rời khỏi nhà tôi, trước khi đi còn lấy thêm điếu thuốc lào lau quanh miệng. Sau khi bà ấy đi khỏi, mẹ nói nhẹ nhàng với tôi: “Mẹ mong không có điều gì tồi tệ sẽ xảy ra với đứa trẻ”.

“Tại sao điều gì xấu có thể đến với nó cơ chứ?” Tôi hỏi mẹ.

“Thôi, không có gì, đừng để ý đến mẹ nữa”, bà nói với tôi, trước khi thay đổi đề tài tranh luận. “Thậm chí, nếu hoàn cảnh gia đình được cải thiện hơn thì làm như vậy, chúng ta đã có thể huỷ hoại những đứa bé như gà con”.

“Thật tội nghiệp”, bà nhắc lại, sau đó nói chậm rãi, như nói cho riêng mình: “Và chỉ là một đứa bé. Tám tuổi đầu. Nhưng họ cần tiền, tôi nghĩ thế, chỉ có cách duy nhất kiếm tiền là gả con gái đi lấy chồng sớm.”

Nói xong câu này, mẹ rời khỏi nhà đi ra vườn, một lúc sau quay lại với nắm đậu đũa trên tay để nấu bữa trưa cho cả nhà.

Mười lăm ngày sau, mẹ Inem lại đến nhà tôi lần nữa. Thời điểm này rất thuận lợi cho Inem đi. Bà cảm thấy hoàn toàn thoải mái khi Inem không có phản ứng gì để ra đi. Khi Inem đã chuẩn bị rời đi, cô đi tìm tôi ngay trước cửa bếp.

“Tạm biệt” cô nói với tôi, “Tôi chuẩn bị về nhà đây.”

Cô nói vậy rất nhỏ nhẹ, như cô ấy luôn luôn nói nhỏ nhẹ như thế.  Tại thành phố nhỏ của chúng tôi đó là cách để tỏ lòng kính trọng. Và sau đó cô ra đi, vui mừng như một cô gái trẻ đang mong được nhận chiếc váy áo mới.

 

Sau khi Inem không còn sống trong nhà tôi nữa, tôi cảm thấy thực sự thiếu vắng những người bạn thân thiết. Và từ đó trở đi không phải là Inem, người thường lôi tôi vào nhà tắm rửa chân cho tôi trước khi đi ngủ, thay vào đó là một bà giúp việc đã già.

Vào lúc đó tôi rất khó vơi đi nỗi buồn nhớ Inem. Thường mỗi khi lên giường đi ngủ, tôi nhớ lại hình ảnh mẹ Inem dắt tay, đưa cô ra khỏi ngôi nhà thân yêu của tôi, dẫn nó vào ngôi nhà xa lạ của họ nằm ngay cạnh nhà tôi, chia cách như quyền sở hữu chỉ bằng một bờ hàng rào gỗ ngăn cách.

Trong tháng đầu tiên sau khi Inem ra đi, tôi thường đến nhà cô ấy chơi, nhưng mỗi khi mẹ tôi tìm thấy tôi đang ở đấy, bà giận dữ quát mắng: “Mày đến đấy làm gì cơ chứ?” . “Mày sẽ không học được gì hay ho ở gia đình nhà ấy đâu.”

Tôi có thể nói được gì? Tôi đã không trả lời. Mỗi khi bị mẹ mắng, bà có lý do của mình, và mỗi lời nói, mỗi cử chỉ của bà như quả đấm thép hoặc bức tường thành mà không có lời lẽ xin lỗi nào có thể lọt qua. Cách tốt nhất là không nên nói gì cả.

Nhưng sau đó, khi tôi không nói gì, bà lại tin tưởng nói tiếp: “Mày chơi với Inem làm gì? Không còn đám trẻ nào khác cho mày chơi hay sao? Inem là đàn bà đang chuẩn bị đi lấy chồng rồi.”

Mặc cho sự cấm đoán của mẹ, tôi vẫn tiếp tục mò đến nhà Inem. Tôi hiểu ra rằng rất nhiều sự ngạc nhiên trong cấm đoán của ba. Tựa như đó là lý do duy nhất để tồn tại sự cấm đoán bằng bạo lực. Dùng bạo lực để cấm đoán tôi có lẽ sẽ mang lại sự dễ chịu nào đó. Với một đứa trẻ như tôi, ở thời điểm đó, ở nơi đó có hàng vạn nguyên tắc và điều cấm kị. Tựa như cả thế gian này đều dõi theo con trẻ, ngăn cản không cho chúng làm những gì mà chúng ham thích. Nơi đây hầu như không tồn tại khái niệm nhạy cảm mà thế giới này chỉ có nghĩa dành cho người lớn mà thôi.

Năm ngày trước đám cưới của Inem, gia đình cô ấy chuẩn bị đồ ăn cùng bánh đặc biệt. Tôi đã dành nhiều thời gian ở bên nhà cô ấy.

Một ngày trước đám cưới, của hồi môn cho cô dâu được chuẩn bị. Mẹ bảo tôi mang sang nhà cô ấy với năm kg gạo và 25 xu như là đóng góp cho sự kiện này. Buổi chiều, khi Inem chuẩn bị xong, tất cả trẻ con trong xóm tập trung tại nhà cô, trầm trồ ngưỡng mộ ngắm nhìn cô dâu mới.

Cặp lông mày của Inem, lọn tóc tuyệt đẹp quanh vầng trán trên gương mặt mịn màng phủ lớp phấn hồng. Tóc cô như dài hơn, mỏng mảnh tết thành bím, quấn thành những lọn tóc rất đẹp. Trên đầu cài hoa và vương miện thật đáng yêu. Kebaya của Inem, Satin và Kain thuộc loại đắt tiền  của Batil từ Solo. Gia đình Inem đã nhận những thứ đó từ cửa hàng cho thuê đồ cưới của khu phố Tầu gần quảng trường. Nhẫn vàng và vòng cổ cũng là đồ đi thuê cả.

Ngôi nhà được trang hoàng lộng lẫy bằng lá chuối, lá dừa, giấy mầu đỏ trắng xanh kết thành giải dài chạy khắp nhà. Những cột nhà cũng được trang trí thành những chiếc cổng mầu vu quy tuyệt đỉnh.

Mẹ tôi cũng đến giúp chuẩn bị lo đám cưới, nhưng sau gần một giờ bà quay về. Bà phải làm như vậy là vì tình nghĩa với hàng xóm gần cạnh mà thôi. Đúng lúc đó vật sính lễ bên nhà chồng Inem được đem đến: Một giỏ bánh to, một con dê, một bao lớn gạo, một túi muối, vài chùm dừa và nửa bao tải đường.

Vì đám cưới tổ chức sau ngày mùa, nên gạo rất rẻ. Khi gạo rẻ thì các sản phẩm lương thực khác cũng rẻ theo. Vì vậy sau vụ mùa là thời gian thích hợp cho đám cưới và cũng do vậy gia đình Inem không có khả năng thuê biểu diễn Wayang kulit; vì tất cả các đội biểu diễn trong vùng đã đi theo hợp đồng của các gia đình khác trong xã.

Vì không có đội nào để thuê, gia đình Inem quyết định thuê nhóm vũ nữ. Một mặt có những nguyên nhân hết sức tế nhị. Phía bên mẹ Inem vốn nổi tiếng có nhiều người mộ đạo, mặt khác bố Inem cũng không về.

Trong vùng chúng tôi ở, biểu diễn tayuban nói chung chỉ dành cho những người đàn ông đã luống tuổi cùng với đám trẻ mới lớn, những người mà kiến thức về tình dục không vượt xa ngoài những chiếc hôn và những ai đến chủ yếu là đứng nhìn ngắm. Với phụ nữ, những người đàn bà được kính trọng không bao giờ đến. Để khơi dậy niềm phấn kích của đám đông đàn ông biểu diễn tayuban luôn song hành cùng với đồ uống như bia, rượu dừa, witsky và gin.

Tayuban phục vụ đám cưới của Inem đúng hai ngày hai đêm. Lũ trẻ con chúng tôi vui chơi thỏa thích, ngắm nhìn đàn ông đàn bà nhảy nhót và hôn nhau, chạm ly rượu bia bắn bọt tung toé vì họ vừa nhảy vừa hát.

Tuy vậy mẹ vẫn cấm đoán tôi không cho xem, tôi phải trốn đi xem một cách bí mật.

“Tại sao con cứ luôn luôn thích đến những chỗ đó? Họ chính là những tội đồ xấu xa. Hãy xem cha dạy kinh thánh của con kìa. Ông có đến đấy không? Không ông không đến đó. Con phải nhận ra điều đó chứ. Và ông ấy chính là chú của Inem!”

Chú của Inem, cha dậy kinh thánh của tôi, cũng sống ngay cạnh nhà chúng tôi. Thật đúng thế, sự vắng mặt của ông tại buổi biểu diễn tayuban làm cho tất cả mọi người đều nhận ra ngay. Mọi người nhận ra và truyền miệng từ người này qua người khác rằng chú của Inem là một tín đồ tôn giáo, nhưng bố của Inem là tên trộm cướp chuyên nghiệp.

Mẹ tôi đã chứng tỏ sự tức giận của mình làm tôi lúc đó không sao hiểu nổi. “Những người đó không biết kính trọng phụ nữ. Con có biết điều đó không?” bà hỏi tôi một cách miễn cưỡng.

Khi bà mối đến nhà chính thức giới thiệu cô dâu cho chú rể xem mặt, Inem ngồi trên kiệu cưới, được dẫn ra phía trước nhà. Ngay tại bậu cửa, nơi bà mối đang chờ sẵn, cô quỳ gối trước người chồng và chứng tỏ sự ngoan ngoãn của mình nghe theo chồng bằng cách rửa chân cho chồng bằng nước hoa từ bình nước. Đôi uyên ương đứng bên trong sau đó cùng nhau dẫn lên xe hoa.

Khách khứa nói, giống như cặp  mantra, “Một đứa trẻ thành hai. Một đứa trẻ đã thành đôi. Một đứa trẻ đã thành đôi...” Gương mặt những phụ nữ cúi xuống tự hồ như họ là những người đã được đón  nhận hạnh phúc đang đến. Chính tôi nhận ra rằng Inem đang khóc. Nước mắt cô giàn giụa trên gương mặt trát phấn son, nước đọng lại thành giọt nhỏ xuống trên khuôn mặt cô.

Muộn hơn sau khi về nhà tôi hỏi mẹ sao Inem lại khóc, mẹ nói với tôi rằng: “Khi cô dâu khóc, là vì rằng cô đang nghĩ đến tổ tiên ông bà đã ra đi. Tâm trạng của họ tại lễ cưới, họ đang hạnh phúc vì cảm thấy sắp được người chồng che chở thương yêu”.

Tôi không nghĩ nhiều về những câu trả lời của mẹ, nhưng sau đó ít lâu tôi biết rằng Inem khóc là do cô muốn đi tiểu nhưng không dám nói với ai. Đám cưới kết thúc, mọi việc lại diễn ra như thường lệ tại ngôi nhà Inem. Không còn khách khứa đến thăm hỏi giúp đỡ nữa. Và bây giờ thay vào đó là những người đòi nợ bắt đầu gọi tới tấp. Và lúc này, bố Inem vẫn rời thành phố trên hành trình xa xôi của ông.

Sau đám cưới, Inem cùng mẹ ngày đêm ngồi may áo mũ Batik. Nếu như có ai đó ghé qua nhà lúc ba giờ sáng có thể thấy họ vẫn đang cặm cụi khâu vá trong đám khói mù vì đốt nến và sáp để đính mũ áo.

Một lần, khi tôi đang ngủ với mẹ trên giường, có tiếng kêu đã đánh thức tôi: “Không, tôi không muốn đâu!”

Muộn hơn trong ngày hôm đó tôi còn nghe thấy vài lần tiếng kêu la như vậy, tiếng động vang xa như xuyên qua khe cửa của các nhà. Tôi biết đó là tiếng kêu la của Inem, tôi nhận ra giọng cô ấy.

“Tại sao Inem kêu thất thanh như vậy?” Tôi hỏi mẹ.

“Họ đánh nhau sao. Tôi chỉ hy vọng không có chuyện gì xảy ra với Inem.” Mẹ nói thêm không cần giải thích tiếp thêm nữa.

Tôi kiên trì với câu hỏi của mình: “Tại sao điều gì xấu có thể xảy ra với cô ấy chứ?”

Mẹ không trả lời tôi. Cuối cùng sau tiếng kêu rên, la hét là yên lặng và tôi cũng ngủ thiếp đi. Những đêm sau, và hầu như đêm nào cũng vậy, tiếng kêu, rên la ó không ngơi. Cứ mỗi lần nghe thấy vậy tôi đều hỏi mẹ, nhưng không khi nào tôi được nghe câu trả lời thỏa mãn cả. Tốt nhất là mẹ thở dài. “Thật tội nghiệp con bé, tội nghiệp quá đi thôi.”

Vào một ngày Inem xuất hiện trước cửa nhà tôi.  Cô đi thẳng vào tìm mẹ tôi. Gương mặt cô nhợt nhạt trắng bệch, ngượng ngùng và cảm thấy xấu hổ trước khi nói, cô bắt đầu khóc, nhưng rất nhẹ nhàng và lịch lãm.

“Sao cháu khóc hả Inem?” Mẹ tôi hỏi cô. “Các cháu lại đánh nhau à?”

Inem khóc oà lên nức nở, “Bà ơi, cho cháu được về với bà đi, bà ơi. Cháu hy vọng bà sẽ cho cháu về lại với bà.”

“Nhưng bây giờ cháu đã có chồng cơ mà, đúng không nào?”

“Làm ơn đi, thưa bà. Hãy xót thương cháu với. Hàng đêm tất cả những gì nó làm là đè đầu cưỡi cổ cháu.”

“Nhưng cháu không nói cho nó biết sao, rằng đừng làm như thế, anh yêu?”

“Cháu sợ lắm thưa bà. Cháu sợ anh ấy lắm. Anh ta to khoẻ như vậy mỗi khi anh ấy ôm ghì lấy cháu cháu nghẹt thở. Bà làm ơn cho cháu quay về nhà bà với?” cô van nài.

“Giá như cháu không có chồng, Inem, chắc chắn bà sẽ cho cháu quay lại nhà bà. Nhưng giờ cháu đã có chồng rồi...”

Nghe câu trả lời của mẹ tôi Inem bắt đầu khóc to hơn. “Nhưng cháu không muốn có chồng thưa bà”

“Cháu có thể không muốn nhưng cháu đã có rồi, Inem. Thời gian qua đi chồng cháu sẽ cư xử tốt hơn rồi hai cháu sẽ có hạnh phúc. Chính cháu đã muốn lấy chồng cơ mà?”

“Vâng, nhưng, nhưng, thưa bà...”

“Không còn cách nào khác đâu cháu, Inem. Vấn đề là ở chỗ có chồng thì phải phục tùng gia đình chồng, mẹ tôi khuyên.  Nếu cháu không làm thế, ông bà tổ tiên sẽ trách cứ, trừng phạt cháu cho mà xem.”

Inem lúc này còn đang khóc, nên cô không nói thêm được câu gì. Mẹ tôi tiếp tục: “Bà muốn cháu hứa, Inem, rằng cháu sẽ luôn nấu ăn cho chồng, mỗi khi công việc nhà kết thúc cháu sẽ cầu nguyện cho chồng bình an làm ăn phát đạt. Cháu phải hứa sẽ giặt giũ quần áo, và phải xoa bóp cho chồng mỗi khi nó mệt mỏi từ công việc về nhà. Cháu phải quan tâm đến chồng khi nó đau ốm mỏi mệt.”

Inem không nói gì nước mắt cô vẫn tràn đầy khuôn mặt.

“Về nhà đi, Inem, từ giờ trở đi cháu phải tận tuỵ phục vụ nhà chồng. Không quan trọng chồng xấu hay tốt, cháu vẫn phải phục tùng nó. Cuối cùng nó vẫn là chồng cháu cơ mà.”

Inem yếu ớt đứng như trời trồng.

“Giờ thì hãy đứng lên Inem và về nhà chồng cháu đi. Nếu...cháu nếu cháu có ý định rời xa chồng cháu thì hậu quả sẽ khôn lường cho cháu ngay bây giờ và cả cho tương lai.”

Đầu cúi xuống, Inem đáp lại lí nhí, “Vâng, thưa bà,” sau đó tự mình đứng lên và đi về nhà.

“Buồn quá, nó còn quá trẻ.” Mẹ nói sau khi  cô ấy đã đi.

“Mẹ ơi... Bố có bao giờ đánh mẹ không?” Tôi hỏi.

Mẹ cẩn thận tìm trong ánh mắt tôi. “Bố con là người đàn ông tốt nhất trên thế gian này.”

Sau đó bà đi vào bếp và trở lại với tôi cùng đống đồ nghề làm vườn.

Một năm trôi qua. Và một ngày, Inem lại đi đến nhà tôi. Lúc này cô đã lớn hơn, chững chạc như người lớn tuy mới gần chín tuổi đầu. Như lần trứơc cô đến thẳng tìm mẹ tôi. Khi thấy bà, cô ngồi xuống đầu cúi thấp ngay trên sàn nhà trứơc bà.

“Bà ơi cháu không còn chồng nữa rồi,” cô tuyên bố.

“Cháu nói gì thế, Inem?”

“Giờ cháu không còn chồng nữa rồi.”

“Cháu ly hôn rồi à?”

‘Vâng, thưa bà.”

“Nhưng tại sao?”

Inem không nói gì.

“Cháu có phục vụ chồng chung thủy và tận tuỵ không?”

“Cháu nghĩ là cháu đã là người vợ tốt của anh ấy rồi, thưa bà”

“Nhưng cháu có xoa bóp cho chồng mỗi khi nó đi làm về nhà mệt mỏi không?”

“Vâng, thưa bà, cháu đã làm tất cả những gì bà khuyên cháu.”

“Thế thì tại sao nó lại ly dị cháu?”

“Nó đánh cháu. Lúc nào cũng đánh”

“Nó đánh cháu sao? Một đứa bé gái như cháu?”

“Cháu nghĩ, cháu đã làm mọi thứ để chứng tỏ là người vợ tốt, thưa bà. Nhưng cứ để nó đánh cháu, thưa bà và hãy để cháu thấy đau, nỗi đau đớn về thể xác cũng là một phần của cuộc đời làm người vợ tốt thưa bà?” Cô nói với giọng quả quyết.

Mẹ không nói gì nhưng bà đã hiểu qua ánh mắt của Inem. “Nó đánh cháu ư...” bà tự nói thầm với chính mình.

“Vâng, thưa bà, nó đánh cháu - cũng như bố mẹ cháu vậy.”

“Có thể cháu chưa phục vụ nhà chồng chu đáo. Chồng không bao giờ đánh vợ nếu như người vợ thực sự tốt và đảm đang.”

Inem không nói gì, mà hỏi lại, “Bà có nhận cháu lại về nhà bà không?”

Mẹ tôi trả lời không do dự. “Inem, cháu là người đàn bà đã ly hôn và trong nhà bà còn những người đàn ông trẻ khác. Điều đó sẽ là lời đàm tiếu cho thiên hạ, cháu ạ, phải không cháu?”

“Nhưng họ không đánh cháu chứ”

“Bà không nói ý tứ như vậy, Inem ạ. Với người đàn bà đã ly hôn lại trẻ như cháu, cháu phải ở những nơi đàn ông nhiều mà không biết về quá khứ của cháu.”

“Bà ơi với cháu có điều gì đó không ổn, tồi tệ sao?”

“Không Inem, đó là câu hỏi về của hồi môn.”

“Cháu không biết bà định nói gì, thưa bà. Bà đang định nói về tại sao cháu không được lại đây sao?”

“Đúng rồi, Inem, đó là điều bà đang định nói đến.”

Inem ngồi trên sàn tựa như cô không biết đi đâu về đâu nữa.

Mẹ tôi cúi xuống nắm vào vai Inem và nói đầy chia sẻ: “Bà nghĩ tốt hơn hết cháu hãy về nhà phụ giúp cha mẹ kiếm tiền mà sinh sống. Bà thật sự lấy làm tiếc và thương cháu nhưng không thể nhận cháu vào làm nữa.”

Nước mắt chảy giàn giụa trên khóe mắt của người đàn bà con trẻ. Cuối cùng Inem đứng dậy. Nhẹ nhàng cô bước chân về nhà bố mẹ đẻ. Từ đó ít khi nhìn thấy cô nữa.

Sau đó, người đàn bà ly hôn mới chín tuổi đầu này không có gì khác ngoài sự căng thẳng và đau khổ của gia đình cô ấy. Cô có thể bị đánh bởi bất kỳ ai ngoài ý muốn, mẹ cô, các anh cô, các chú cô dì cô, và hàng xóm của cô. Nhưng cô không bao giờ đặt chân đến nhà tôi nữa.

Tôi đã thường nghe thấy tiếng nức nở đau đớn của Inem và khi cô kêu khóc, tôi đã phải bịt tai lại bằng hai bàn tay của mình.

Còn mẹ tôi, vẫn tiếp tục giữ gìn khuôn phép, bảo vệ danh tiếng gia đình mình. Và làm việc đòi chia tài sản, lấy lại tên thời con gái hộ Inem.
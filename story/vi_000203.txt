Trên Quốc lộ 1, từ Bắc vào Nam, đến cầu Nga địa phận Thạch Hà, có một cánh đồng nhỏ. Nơi ấy, thời kì chiến tranh chống Mỹ là điểm đứng chân của một trận địa pháo cao xạ 37 ly đã từng cơ động chiến đấu ở ngã ba Đồng Lộc, thị xã Hà Tĩnh và các huyện lân cận đánh máy bay Mỹ, bảo vệ làng xóm, huyết mạch giao thông, bắn rơi nhiều máy bay, bắt sống cả giặc lái. 

Sáng ngày 15-6-1966, Đế quốc Mỹ huy động hàng chục tốp máy bay cấp tập đánh phá cầu Nga, các đơn vị bộ đội phòng không, dân quân địa phương ngoan cường đánh trả. Buổi trưa, một loạt bom đánh trúng trận địa pháo, một khẩu đội hy sinh anh dũng, trong đó có Nguyễn Văn Định, quê xã Sơn Ninh - Hương Sơn - Hà Tĩnh. Liệt sĩ Nguyễn Văn Định đã để lại cuốn nhật ký với nhan đề: “Những ngày trở lại Quân đội” ghi lại những ngày tháng đánh máy bay Mỹ trên địa bàn quan trọng này. 

Nhân dịp kỉ niệm 65 năm ngày Thương binh liệt sĩ (27/7/1947 - 27/7/2012), Ban Biên tập Tạp chí Văn nghệ Quân đội trân trọng giới thiệu tới bạn đọc một vài trích đoạn cuốn nhật ký nhiều ý nghĩa được viết bởi một người con của mảnh đất Hà Tĩnh - Liệt sĩ Nguyễn Văn Định.         
                        
 
 Liệt sĩ Nguyễn Văn Định (trái) và một đồng đội - Ảnh: T.L
  
27/9/1965

Ngày thứ 2 ở trận địa.

Sáng nay khi ở đoàn bộ hành quân hỏa tốc xuống, hai chúng tôi phân công nhau vào làm nhiệm vụ của mình. So với mọi hôm thì ở đây mọi việc diễn ra hết sức khẩn trương như chuẩn bị ngụy trang, lấy hướng xạ kích. Đồng chí đại đội trưởng nhận định tình hình, nhắc thêm nhiệm vụ cho từng khẩu đội. Theo dự đoán, hôm nay địch có thể đánh ta vì: Cả đêm chúng bay lượn hai, ba lần. Sáng tinh mơ, khi ánh bình minh còn nấp sau sương mai dày đặc, máy bay đã trinh sát. Khoảng 9-12 giờ, ba lần báo động, hai chiếc F4H, 3 chiếc H4D bay từ hướng 32-14 và 12-34. Trinh sát chưa có hành động gì.
 

29/9/1965

Hôm nay nữa là ngày thứ 4 ở đơn vị chiến đấu, ước mơ buổi đầu sẽ cùng đồng đội đánh những chiếc máy bay đến đây quấy nhiễu. Quyết tâm của đơn vị là hạ cho được máy bay tại chỗ để đạt mục đích thi đua mà chi bộ, chi đoàn phát động. Thế mà có được như ý định đâu. Đơn vị về đây phục kích 5 hôm rồi, ngày ngày chỉ chờ chúng tới, sà thấp xuống với cự ly hiệu quả, bốn khẩu pháo chễm chệ trong công sự vươn nòng sẽ khạc những loạt đạn đầy căm hờn. 

Những “con ma” “thần sấm”  không thể nào thoát được lưới lửa quyết tâm, quyết thắng này. Hôm nay đơn vị đã ba lần về cấp I nhưng mục tiêu chỉ lởn vởn tít mây xanh, lượn đi lượn lại mấy lần rồi cút mất. Mình chỉ khao khát nổ súng chiến đấu tiêu diệt được mục tiêu cho đáng công một nắng, hai sương, ngồi hầm ngủ lán và cũng đáng công bà con ở vùng này ngày đêm động viên, giúp đỡ.

  
 Ảnh: st

30/9/1965

11 giờ 50 trưa, máy bay xuất hiện hướng 32 gồm 2 chiếc  F4H; 4 chiếc A4D cùng lúc bay tới vòm trời thị xã Hà Tĩnh. Lợi dụng tầng mây thấp, cặp hung thần F4H bổ nhào bắn 2 tên lửa không đối đất xuống khu doanh trại công an vũ trang, 4 chiếc A4D lao xuống thả 4 chùm bom vào khu vực ấy. Trước đó 20 phút, đơn vị đã về cấp I một lần vì mục tiêu hướng 12 xuất hiện. Sau đó nó bay về hướng 32 rồi chuồn mất. 

Bọn tôi được về cấp II. Vừa mới ngả lưng trên phản, hướng 32 xuất hiện, tiếng động cơ máy bay rú lên. Đơn vị lại nhanh chóng trở về cấp I. Trinh sát đo xa liên tục báo mục tiêu, cự li: 3500, 3000, 2800, chiếc thứ nhất bay qua chưa bắn. Ý đồ của thủ trưởng đợi chiếc thứ 2 bổ nhào trong cự li hiệu quả nhất. Một đơn vị bạn đã bắn. Ngọn cờ trên tay chỉ huy dập xuống, 4 khẩu pháo nhả đạn. Bọn cướp ngày hoảng sợ bay vút lên cao rồi chuồn thẳng. 

Tuy chưa đạt được ý đồ bắn tiêu diệt, song ở trên trận địa đã rút được kinh nghiệm, đặc biệt hơn lúc này tôi trực vô tuyến điện, càng nắm được thủ tục, tình huống xử lý. Ước mơ xuống đơn vị trực tiếp đánh giặc, hôm nay là ngày thứ 5 rồi mới có dịp thực hiện. Chắc còn được tham dự những trận chiến đấu quyết liệt hơn nữa trong những ngày tới.


5/6/7/10/1965

Từ lúc các khẩu pháo của 2 đơn vị X-Y rời khỏi trận địa, khoảng hai giờ sau, mấy chiếc máy bay tới thả pháo sáng, thả bom xuống bệnh viện, cửa hàng ăn uống phía Thạch Hưng. Sang ngày thứ 2, ban ngày chúng chỉ bay lượn trinh sát, đến đêm lại tiếp tục thả pháo sáng. Khoảng 3 giờ sáng, 2 chiếc nữa lại tới bắn rocket, 1 chiếc bắn 12,7 ly, 1 chiếc bắn loại 40 ly xối xả xuống bệnh viện. Đêm thứ 3, mới 7 giờ 30 tối, 2 chiếc A4D tới thả 2 pháo sáng, ném bom xuống cầu Đò Hà. Trong ba ngày, ba đêm, ba đồng chí cùng tôi trực cả ngày lẫn đêm.
 

16/10/1965

Được lệnh đi công tác, mình phần phấn khởi, phần lo lắng. Suốt đêm, sau khi nhận nhiệm vụ về cứ suy nghĩ miên man, hết việc này đến chuyện nọ. Khoảng ba giờ chiều sắp xếp xong chăn màn, chuẩn bị mọi cái cần thiết để vượt đường dài 100 km, trong thời gian gấp rút.

Bảy giờ sáng 16-10 xuất phát, trên đường đi qua mấy cây cầu trọng điểm. Cầu Cày, cầu Già, Nghèn, gặp lúc phi cơ đến ném bom nhưng mình vượt được hết. Suốt 12 giờ hành quân bộ một mình nhưng chỉ nghỉ mỗi nơi 20 phút để nhập tâm vượt qua các trọng điểm. Vượt qua rồi lòng vui như trẩy hội. Tối nghỉ ở Đức Tùng, sáu giờ sáng đi một mạch tới Nam Liên.
 
 
  Ảnh: st
18/10/1965

Thật quá tưởng tượng đối với mình lúc ra đi, đến đây được sự đón tiếp nhiệt tình của Quân khu  đối với các đại biểu về họp.

Qua báo cáo mình mến phục đồng chí đại diện Đoàn đảo Cồn Cỏ anh hùng,  ý chí kiên quyết của người chiến sỹ cách mạng trên đảo nhỏ tiền tiêu của Tổ quốc. Đây mới là ngày đầu, mình sẽ còn học được nhiều tấm gương để khi về đơn vị truyền lại cho anh em.

Chiều 18-10, qua báo cáo của Đoàn 14 - Cồn Cỏ và các mẩu chuyện trên thuyền vượt bom, pháo tiếp tế cho đảo, chuyện liên lạc 2W, biết bao nhiêu hình ảnh, điển hình là anh dân quân 49 tuổi. Không chỉ riêng mình mà cả đại hội đều hài lòng, cảm phục ý thức trách nhiệm cao, tinh thần dũng cảm tuyệt vời mà chúng ta là quân nhân không thể không hành động như vậy. Buổi tối nghe hai mẩu chuyện bắn máy bay ở Nghi Xuân, Hương Khê, câu chuyện về Nguyễn Viết Xuân thứ hai là Đậu Văn Vịnh, thêm bài học của đời chiến sỹ.


3/11/1965

Tiết trời đã sang đông lâu rồi, thế mà mấy hôm nay vẫn nắng như đổ lửa. Ban đêm ngủ chẳng cần chăn gối, nóng như mùa hè vậy. Từ tối hôm qua, gió bắc đã tràn về mang theo những đám mây đen nặng trĩu tưởng như trời sẽ đổ mưa rào, nhưng rồi chỉ có lắc thắc một lớp đủ phủ mặt đất. Trưa này, sau bữa cơm, cầm tờ báo xem chưa hết, mắt đã lịm dần, cho tới khi có tiếng máy bay đến. Hai đơn vị đã bắn. Tôi bật dậy thì đã thấy chiếc AD4 phụt khói bốc cháy trên bầu trời.

Thị xã 14 giờ 30

Hôm qua, nhận được lá thư hậu phương gửi tới, theo yêu cầu học tập của các cháu nhỏ, giấy bút thiếu thốn, có lúc phải nghỉ học vì không còn giấy. Trưa nay mượn của bạn một tập, gửi mua được một xếp, nhờ người gửi về. Chắc các cháu khi nhận được món quà đó sẽ mừng hơn những lần chú về cho bánh kẹo. Chú cũng thỏa lòng là giải quyết được một khó khăn nhỏ cho các cháu.

 
8/12/1965

Ngọn đồi Củ Bắp chẳng lấy gì làm cao lắm, nhưng so với vùng này nó là điểm cao nhất. Đài chỉ huy sở đặt trên ấy. Nhìn bốn phía thấy rõ tuyến công sự do nhân dân làm cho từ hôm trước. Khi lên đỉnh đồi, tôi thấy nơi đây là địa điểm tốt cho pháo mặt đất hơn là sở chỉ huy pháo phòng không. Ở đây gió thổi ào ào. Mây mù hết lớp này đến lớp khác tràn qua, quần áo 12 chiến sỹ đều ướt sũng, mọi người chen chúc trong túp lều nhỏ để tránh gió quái ác. 

Đúng 11 giờ 18 phút, một tốp máy bay AD6 bay qua trận địa, một lát, hai chiếc quay lại ở độ cao 1000m, cự ly trong 2500m. Phú Mỹ đã hạ lệnh bắn. Chiếc thứ nhất bị thương. Tiếp tục bắn chiếc thứ hai. Hoài Nhơn hạ lệnh phát hỏa, trong mây mù đạn ta sáng loáng vây bọc hai con quạ sắt. Chúng tăng tốc, bay sang hai ngả, một chiếc bay xuống cửa khẩu, dập bom bừa bãi. Không hiểu nó có rơi xuống biển hay không?

Cùng ngày 8-12, thấy ở trên Củ Bắp này không thuận lợi lắm, 17 giờ chúng tôi rút về phía làng CT, xã Kỳ Trinh, nghỉ lại một đêm.

Sáng ngày mồng 9, chỉ huy sở được xây dựng trên một gò đất giữa đồng, quan sát các hướng rất tốt, nhìn về hai trận địa rõ ràng, các việc khác cũng có bề thuận lợi hơn. Trưa nay mọi người ngủ bù cho đêm hôm trước thức trắng. Tính ra, đã một tuần ba đêm, chúng tôi không ngủ. Chắc trong đợt hành quân dã ngoại này còn nhiều đêm thức suốt. Ta sẽ xem sự đấu tranh của bản thân chịu đựng gian khổ đến được mức nào? Và ta tin rằng quyết tâm sẽ thắng vì Đinh Trọng Biên bảy ngày đêm thiếu ăn, không ngủ vẫn làm việc bên máy vô tuyến điện tốt.
 
 
  Ảnh: st
20/1/1966

Tết lại đến, hôm nay là ngày cuối cùng của năm Ất Tỵ. Sáng nay giữa trận địa lòng tôi rạo rực khát vọng, ôn lại những năm qua cũng đã ba lần ăn tết ở nơi chân trời hẻo lánh, vui tết đón xuân là những cuộc hành quân vạn dặm và sẵn sàng chiến đấu.

Năm nay lại được thưởng thức một cái tết, bản thân mình được đón xuân đậm đà tình cảm bộ phận cũ (tiểu đoàn bộ) và đơn vị mới bên khẩu pháo nòng dài tại Hoài Nhơn. Chỉ tiếc rằng, trong lúc này vợ và 3 đứa con, bố, mẹ, anh chị, các cháu, thấp thoáng nhìn theo bóng anh bộ đội, có phải đứa con xa nhà được về đoàn tụ vui tết chăng. Nhưng không phải đâu. Trên nét mặt người thân tôi giọt lệ lại từ từ chảy.

 
Cẩm Lạc 31/1/1966

Mùa xuân về đã được mười ngày, song cảnh xuân trong thời gian này hình như chưa thấy mà còn giống như mùa đông vậy. Xuân sang bao nhiêu ngày là bấy nhiêu hôm mưa và rét, con người cảnh vật trông chẳng thảnh thơi, vui vẻ tý nào. Nhất là các chiến sỹ pháo phòng không càng rét hơn bao giờ hết. Mỗi đợt mưa lại tăng thêm ướt át, lán trại, trận địa đã lầy lội lại càng bẩn hơn, da dẻ con người ỉu xìu. Nhưng với các chàng lính trẻ vẫn lạc quan yêu đời, rét mướt, gió mưa với họ chẳng là gì. Nhưng mưa rét kéo dài cũng gây nhiều ảnh hưởng.

Hôm nay là ngày cuối của tháng một năm mới. Buổi sáng trời nắng, có nhiều tiến triển tốt, gió đông bắc nhẹ nhẹ thổi, ánh hào quang phía đông đang tiến thẳng lên, xuyên qua, phá vỡ từng mảng mây đen nặng trĩu nước, buộc chúng phải tách rời, lởn vởn lên cao.

Nhận định tình hình địch, ai nấy trong đại đội đều khẳng định, tốt trời là chúng sẽ hoạt động. Đúng vậy, 10 giờ 10 phút có 2 chiếc F105 xuất hiện  hướng 12 thấp, bay lượn mấy vòng rồi đi qua hướng 32 sau dãy núi để đánh lừa đối phương. Nhưng nó tránh sao khỏi hàng trăm đôi mắt của đơn vị. Nó bay sang hướng 1-14 qua bên trận địa của 2 đơn vị An Lão và Hoài Nhơn. Lệnh bắn liền phát ra, từng đàn chim lửa  đuổi theo, đón trước, chặn sau, bao trùm thân chúng. Buộc chúng phải lao về phía biển không dám ngoái cổ. Tiếng rú của động cơ, tiếng đạn nổ xé trời như đánh tan không khí nặng nề mấy ngày qua. Sắp tới chúng ta sẽ đón nhiều tươi vui, lành mạnh, đẹp đẽ như MÙA XUÂN CHIẾN THẮNG giặc Mỹ. Một chiếc F105 bị bắn rơi ở Cẩm Nhượng, một chiếc khác rơi lúc 16 giờ ở Cẩm Hưng.

Hoan hô, hoan hô An Lão, Hoài Nhơn, các khẩu đội 12,7 ly và súng trường của dân quân địa phương đã nổ loạt đạn đầu hạ F105, mở màn cho mùa xuân 1966.

 
 
7/3/1966

Đẹp lắm - khẩu 3 bắn đẹp lắm, đạn bao trùm mục tiêu. Ở trong công sự mình làm nhiệm vụ theo dõi mục tiêu quan sát vệt đạn. Chiếc thứ nhất bắn chuệch choạc, vượt trước, tạt sau. Nếu lúc này có máy điều chỉnh nhạy, mình sẽ lái những viên đạn vào chiếc này! Tiếc quá, chiếc F4 cứ bay một chiều tiến về hướng 12. Chiếc thứ 2 tới. Bắn cứ như xem đá bóng, lúc cầu thủ đội A sút vào khung thành đội B. Hồi hộp quá, mình tưởng chiếc này sẽ nổ toanh tức khắc, song nó cố lết lên hướng Hương Khê mới nổ tan xác.


19/3/1966

Máy bay bay thấp. Tiếng từ chỉ huy phát ra, ở các khẩu đội báo về, trên trận địa trong chốc lát, khí thế tinh thần thay đổi hẳn. Lệnh chỉ huy bắn chiếc đầu, chiếc thứ 2. Báo cáo 14… 34… 12… vút 1 chiếc, 2 chiếc. Những cặp mắt vòng lên nảy lửa, sáng như đèn pha. Nhìn trái, nhìn phải. Bọn Huê Kỳ đã nhiều lần lợi dụng mây đen đặc để lảng tránh va vào vật thể, song làm sao thoát được, bay chạy đi đâu? Trốn đằng trời! 

Đại đội về cấp I đã hơn 29 phút. Một tốp A4D bay lượn 2 vòng trên không phận. Lần thứ 3 nó quay lại. Đơn vị đang bám sát mấy chiếc này thì hướng 12 lù ra 1 cái rồi 2 cái thấp lắm, to, cái ống phản lực, cái cần anten, các khớp trong rõ mồn một, lệnh chuyển mục tiêu chiếc thứ nhất… Bắn. Đoàng… đoàng - ục ục… máy bay cắt bom, ba quả đen sù sì mang theo cánh chong chóng như cái thập ác rơi tỏm xuống nổ uỳnh uỳnh… 

Bắt được chiếc thứ 2. Bắn. Từ các công sự lại phát ra tiếng ùng ục, máy bay cắt bom, thằng này liều chết đưa bom tới gần trận địa cách K1 khoảng 30m, quả rơi xuống hồ, quả xuống đồng lúa, nước lạnh cũng sôi lên giận dữ. Đạn ta bắn ra mạnh quá. Thằng địch hốt hoảng vút bay một mạch ra biển - thoát chết ở đất liền - Cầu Đông đang nổi hận, ba quả bom xấu hổ lủi xuống bùn không nổ nổi.
 

5/4/1966

16 giờ 30 phút ngày 14-4, tiếng còi trực ban báo hiệu giờ học hàng ngày đã hết, anh em đang đi vào rút kinh nghiệm trong ngày, các bộ phận kỹ thuật làm công tác bảo quản phương tiện.

Bỗng có tiếng động cơ, đơn vị nhanh chóng về báo động cấp I. Đâu đấy đã về vị trí xong. Từ H1 xuất hiện 2 AD4 bay lên hướng 12, trở lại ném bom cầu C. Hướng 12 xuất hiện 3 cái bay thẳng vào trận địa. Tốp này nghi binh thu hút mục tiêu, nó bay lướt qua đỉnh đầu. Lại một tốp H12 nữa lợi dụng mặt trời bay thẳng vào bắn rốc két xuống trận địa. Hướng 34 tốp 2 trở lại cùng lúc đánh vào T25, quả gần nhất cách mình 5m. 

Đất, cát, bụi bám đầy người, đầy mặt. Đối diện với địch thủ, chiếc mũ quí hóa làm sao? Cuộc chiến đấu thật ác liệt. Khắp T5 đều bị đạn địch, đồng chí khẩu đội trưởng hy sinh, 2 đồng chí nữa bị thương nhẹ. May mắn cho mình vẫn giành phần thắng và dạn dày thêm kinh nghiệm mà thôi. Thật là một giây phút ghi sâu ở đời bộ đội với trận hôm nay.
 

17/4/1966

Kể từ khi đồng chí khẩu đội trưởng hy sinh, các cuộc chiến đấu lại càng ác liệt, vật thể cầu Đông hình như là một chướng ngại, một kỳ công mà bọn lầu Năm Góc đau đầu, nhức mắt bởi những cú “sút” của phòng không miền Bắc trong những trận đánh đầu xuân năm nay. Vì thế chúng tức tối quá độ, tập trung các thế không lực đánh trả “bom, đạn, rocket, tên lửa” nhưng cầu Đông vẫn đứng vẫn đứng chễm chệ, vắt mình qua sông, trải bóng trên dòng nước vàng đục bởi bom đạn Huê Kỳ làm ô uế. 

Ba giờ chiều ngày 15-4, bọn Mỹ tập trung một lực lượng không quân khổng lồ, tưởng lên nuốt sống cầu Đông trong chốc lát và dập vùi hỏa lực đối phương. Từ hướng 34 xuất hiện 3 chiếc F8, 8 chiếc A4D dàn thành hàng ngang bay thẳng về mục tiêu, sau đó có thêm 8 chiếc A4D nữa từ hướng 34 bay tới. Lúc này trên đầu chúng tôi có 19 chiếc lồng lộn, gầm rú, bổ nhào, bay là, bay ngoài, hòng phát hiện hỏa lực đối phương, cường kích, cắn trộm. Thật giống như đàn quạ đói gặp mồi - bom rơi, đạn nổ, khói bụi đất mù mịt. 

Các cỡ pháo phòng không của bộ đội và dân quân đánh trả kiên cường. Trong chốc lát đội hình hung hãn của không lực Hoa Kỳ bị phá vỡ. Hàng chục tấn bom dội xuống đồng, xuống sông, xuống đường nhưng cầu vẫn đứng vững. Bọn khát máu lủi thủi rủ nhau chuồn hết.

Tối hôm nay trên đoạn đường đầy mùi thuốc súng, hàng trăm người lại cuốc xẻng, gánh gồng tu sửa con đường Hồ Chí Minh, xe lại bon chạy.
 
 
  Ảnh: st
27/4/1966

Mấy ngày nắng gắt, ở trận địa mà chẳng khác ngồi trên lò lửa. Cái nước da bóng mịn của mấy anh pháo thủ, bữa đầu xuân ai cũng muốn sờ lên gò má xinh xắn, chỉ mấy ngày thôi mà anh nào anh nấy đã sạm màu. Da bánh mật thì nay chuyển sang nước da chum. Mấy anh có nước da trắng bóc sau mỗi buổi trực gò má y như vết sẹo cháy. Gớm thật, cái nắng tác quái! 

Chiếc áo mới nhuộm màu cỏ úa, trông cũng ưa đáo để, thế mà thuốc tẩy thiên nhiên đã phá mất cái màu buổi đầu, từ màu cỏ úa chuyển sang màu cứt ngựa, nay lại về da bò vàng gầy. Tuy gian khổ là vậy nhưng chẳng ai chùn bước, vẫn say sưa luyện tập, chiến đầu và chiến đấu liên tục. 

Các mẹ, các chị ở địa phương cũng chạnh lòng. Buổi trưa hè, nước chè xanh, hết gánh này đến gánh khác, chuyển ra cho bộ đội. Câu nói ngắn gọn của bà mẹ Thạch Phú - Cầu Đông cảm động làm sao: “Trời nắng, máy bay giặc hoạt động nhiều. Thương các con, nhiều mẹ, nhiều chị muốn ra thăm các con, nhưng ban ngày đi đông không tiện. Mẹ thay mặt ra đây thăm, cũng chỉ có gánh nước chè cho các con, các con uống đi cho đỡ khát. Chúc các con khỏe bắn trúng máy bay”. 

Uống bát nước chè xanh quê hương vừa thơm, vừa đậm, lòng giục lòng hãy làm gì đây, khó khăn gian khổ nguy hiểm là thế, nhưng bên cạnh quần chúng nhân dân đang đêm ngày mong đợi. Làm sao hết chiến tranh, làm sao được hòa bình? Xóm thôn, xưởng máy, nông trường đang sẵn sàng đón tiếp đoàn quân chiến thắng trở về.

…

(Trích Những ngày trở lại Quân đội)
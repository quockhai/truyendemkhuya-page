Chuyện ùm lên vào một sớm mù sương đầu năm.

Số là mụ Ếm dắt trâu ra sông Nghĩa Giang cho uống nước sớm và tranh thủ giặt giũ chậu quần áo đầy có ngọn. Nước sông buổi sớm hơi lạnh nhưng tinh khiết. Mới lại trâu nhịn nước cả đêm rồi. Đang ngoan ngoãn đi đến sát mép nước thì con trâu chùn lại không chịu bước. Nó vùng vằng, khuỵch sừng vào người, mõm phì phì như hổ mang bành. Mụ Ếm gò thừng giục:

- Trâu, đi nào, tao đang vội đây!

Con trâu vẫn xuống tấn tại chỗ. Mụ Ếm vòng ra sau lấy thừng quất vào mông đen đét.

- Đi! Sao hôm nay mày đổ đốn thế!

Đáp lại vẫn là những cú khuỵch sừng hết sang phải lại sang trái. Chẳng lẽ dưới sông có ba ba thuồng luồng hay sao. Bà xuống kiểm tra, nếu không có gì thì chết với bà đấy, nghe chửa, đồ con tườu.

Mụ Ếm tụt váy áo lội bòm bõm xuống sông. Vệt nước đục cuộn lên theo bước chân. Con sông Nghĩa Giang này vào mùa xuân, người lớn có thể kiễng chân lội qua được. Về mùa nước nó cũng chỉ rộng đáng bậc cháu chắt so với sông Đuống, sông Cầu. Nhưng dân Nghĩa Trụ từ nghìn đời nay vẫn trân trọng gọi nó là sông. Nó là nguồn nước chính tưới tiêu ruộng đồng, nơi cung cấp tôm cá trai hến và là nơi tắm táp giặt giũ. Mới lại, cả vùng lúa mênh mông này toàn ngòi, rãnh làm gì có thứ nào đáng mặt so với Nghĩa Giang. Mò mẫm hồi lâu mụ Ếm chỉ tìm được một vật lạ duy nhất là chiếc giày da khá mới. Mụ ngước nhìn về phía cây cầu gỗ xộc xệch răng bà lão ước chừng, chắc là ai đó vô ý đạp phải chỗ thủng tụt mất giày rồi bị lạng về đến đây. Mụ Ếm mang giày lên dí vào mõm trâu hỏi:

- Có cái này mà sợ à. Rõ là đồ trâu bò.

Con trâu sì sì vẻ biết lỗi, rồi tự bước ra sông vục đầu uống ì ọp.

Cầm chiếc giày lạ trên tay mụ Ếm vẩn vơ nghĩ. Con trâu không nhìn thấy giày sao lại biết, lại sợ? Thế mới lạ. Việc này giông giống việc gì ấy nhỉ? Sao tự dưng chợt không nghĩ ra. À, nhớ rồi, giống việc ngựa sợ hài. Hình như cũng ở đoạn sông này. Thế này chắc là mình được trời đất run rủi chọn làm việc gì đây. Xưa vì có chuyện ngựa sợ hài mà chị em nhà Tấm (à quên, xin thánh tha tội, con xin được sửa là Đớn) và Cám được nhập cung. Còn mình thì được gì?. Phải mang ngay chiếc giày này về trình trưởng thôn mới được.

                                                                                    *****

Làng Nghĩa Trụ nằm lọt thỏm giữa vùng đồng trũng năm nghìn mẫu. Từ làng ra đường cái quan phải qua mấy xứ đồng. Con đường như cái cuống nhau nối bào thai làng với đất nước. Có điều cái cuống nhau này bao đời nay vẫn thuần khiết đồng đất làng nên vào ngày mưa thì trơn toẽ ngón chân, vào ngày nắng thì vó câu khấp khểnh bánh xe gập ghềnh, còn vào ngày râm thì dù ai có niệm thần chú “trời mưa tránh trắng, trời nắng tránh đen” vẫn cứ bị sa vào ổ bùn. Trước khi vào làng người ta còn bị thử thách bằng cây cầu khỉ vượt sông Nghĩa Giang. Vác xe mà lần. Mãi đến thời Hợp tác xã mới bắc được cây cầu gỗ răng bà lão để tăng năng suất lao động xã viên. Trải mấy mươi năm cầu đã lên lão dân làng chẳng quan tâm lắm vì họ quen lội sông từ ngày lập làng rồi. Vừa mát mẻ vừa không sợ tuột chân sẩy tay lăn tòm xuống nước. Thế mới có chuyện ma trêu, người Nghĩa Trụ ai cũng thuộc. Cái thời lão Ấm, cha đẻ mụ Ếm còn khoẻ, ngày nào cũng lọ mọ chài cá sớm. Cái hôm đại hàn nước buốt kim châm lão vẫn đi kiếm. Lão biết, càng giá càng nhiều chép. Tôm đi chạng vạng, cá đi rạng đông. Vừa giăng lưới xong thì có tiếng gọi:

- Bác chài ơi giúp em qua sông với.

Lão Ấm nhìn lên cầu, thấy có cô gái trẻ mặc bộ đồ trắng toát đang gần như bò trên cầu.

- Bác cõng em qua sông, muốn trả công gì em cũng chiều.

Lão Ấm thực ra vừa “trót dại” với vợ trước khi ra sông, vốn liếng tự biết chẳng còn bao, nhưng được cô gái trẻ khuyến khích nên ra tay anh hùng giúp mỹ nhân. Do phải cõng người nên lão Ấm bị ngập nước ngang lưng, nước ngấm ướt gần hết áo. Răng giã gạo. Lên bờ, cô gái bảo:

- Nào bác làm gì thì mau mau lên kẻo trời sáng.

Lão Ấm không khách khí, nằm luôn lên người cô gái hòng tìm chút hơi ấm. Ai dè lạnh như khối băng. Của quý của lão sun vào như đỉa gặp vôi không thể “làm gì” được. Dở nhất là cảnh trên bảo dưới không nghe này. Lão thở dài đánh thượt, thò tay xoa ngực vớt vát tý chút. Nhưng giời ơi, gái trẻ sao ngực lại nhão nhoét thế này. Bất giác lão quở:

- Ngực nát như ngực ma ấy!

- Thì đây là ma mà lại.

Lão Ấm đờ mắt nhìn lại thì đớ người nhận ra mình nằm trên bộ xương, cái đầu lâu có hai hốc mắt đen ngòm. Kinh hãi quá lão Ấm ù té chạy, mồm gào “ma, ma” nhưng không phát thành tiếng. Tóc gáy dựng ngược cả lên. Chạy về đến nhà mà vẫn nghe rõ tiếng chân chạy huỳnh huỵch ngay sau lưng. Chuyện lão giấu kín, mãi đến khi đặt tên con gái là Ếm lão mới kể chuyện ma trêu mà đặt tên thì người làng mới biết. Vậy mà người làng vẫn không bỏ được thói quen lội sông.

Nhưng chuyện ma trêu ở đoạn sông này chưa thể sánh được với chuyện hoàng tử được hài, rồi mở hội thi hài kén được vợ. Chính cung hoàng hậu Thị Tấm nay vẫn được thờ làm thành hoàng của làng (xin đức thánh Thị Tấm xá tội, đáng lẽ phải gọi chệch theo lệ làng là Thị Đớn nhưng kẻ hậu sinh này sợ như thế thì người đời không hiểu cho Thị Đớn cũng là Thị Tấm. 

Còn con đĩ Cám lười biếng, xảo trá, độc ác tất nhiên chẳng cần kiêng khem gì, cứ thẳng tên tục Cám mà phết). Cứ tưởng đức thánh Thị Tấm cứ đàng hoàng tĩnh toạ trong niềm tôn kính của dân làng, thì một ngày kia có kẻ láo lếu là thầy khoá hỏng thi Nghĩa Giang Phu Tử về làng không lội sông như thảo dân lại đi cầu khỉ, rồi chẳng may trượt chân ngã xuống sông, giày tuột lạng đâu mất, thế là thày khoá hỏng thi về nhà mài mực viết liền một lèo bài luận ba vạn tám ngàn chữ về việc “Thí hài kén vợ”. Thầy lớn giọng chê bai hoàng tử là kẻ thấy chân không thấy người, lấy vợ kén chân chứ không kén nhan sắc, hình dung, nết người. 

Ra thế nghĩa là chê Đức thánh Thị Tấm vừa mất nết, vừa mất người à. Ấy vậy mà lũ dân đen lại thích, lại truyền tụng mới lạ chứ lị. Bấy giờ có vị Hàn lâm học sĩ hưu trí (hình như chính là người theo lệnh của Chính cung Hoàng hậu Thị Đớn chắp bút nên thiên truyện “Tấm Cám”) đọc được bài luận “Thí hài kén vợ” đã nổi máu lên liền sai người mài mực, sắp giấy hoa tiên để ngài thảo một tấu biểu dâng triều đình kết tội Nghĩa Giang Phu Tử, dụng tâm phá nát thuần phong mỹ tục. Nhân thừa giấy thừa mực, lại đang sẵn cảm hứng giữ gìn thuần phong mỹ tục ngài liền viết một hồi xong bài luận dài ba vạn chín nghìn chữ “phản biện” Nghĩa Giang Phu Tử, cho rằng thảo dân làm sao hiểu được suy nghĩ, tình cảm của đế vương, cũng như dê cừu làm sao hiểu được suy nghĩ, tình cảm của sư tử. 



Tiếp được bài luận “Dê cừu bất thị sư tử” này thầy khoá Nghĩa Giang Phu Tử chỉ cười mim mỉm, đoạn viết luôn bài luận “Dê tính luận” ba vạn chín nghìn chín trăm chữ thẳng thừng chê bai kẻ hủ nho chỉ biết học để mũ cao áo dài, ám chỉ vị Hàn lâm học sĩ. Tiếc rằng Hàn lâm học sĩ tiếp nhận bài luận này thì bỗng phát bạo bệnh chưa kịp có bài luận đáp lại đã chết. Có kẻ đầy tớ phản chủ phọt ra ngoài cái tin đọc xong bài luận ngài uất quá hộc ba đấu máu tươi chỉ kịp trăng trối câu “loạn rồi” rồi chết, chứ không phải chết vì bệnh tật gì đâu. 

Vị thái y triều đình về khám nghiệm cũng khẳng định ngài không có khối u, không có con hát i vê, không phải do sốc thuốc phiện, không phải trúng độc trúng phong. Ngài chết do hộc ba đấu máu tươi. Thế là Nghĩa Giang Phu Tử thu được toàn thắng. Cuộc tranh luận Nghĩa Giang Phu Tử – Hàn lâm học sĩ được kẻ sĩ thời ấy đánh giá rất cao, nào là tạo được không khí dân chủ, nào là nêu cao được tính học thuật, nào là mở ra trang mới cho văn đàn. Ba bài luận “đã thâu tóm hết chữ nghĩa thiên hạ” đáng là mẫu mực để sĩ tử đời sau học tập.

Lại nói về việc Trưởng thôn Nghĩa Trụ nhận được chiếc giày và tình tiết “trâu sợ giày” giống việc ngựa sợ hài khi trước, nhận định đây là việc tiền duyên tiền kiếp liên quan đến thành hoàng Đức Thánh Thị Đớn, nghĩa là liên quan đến vận mạng dân làng, nên liền triệu tập hội nghị Quân dân chính bất thường. Mụ Ếm được đặc cách mời dự làm nhân chứng.

Thoạt nhìn chiếc giày Bí thư đoàn đã nói:

- Giày khủng bố da mềm xi nâu, mốt thời thượng Âu-Mỹ.

Thôn đội trưởng dè dặt:

- Thám báo, Phun-rô cũng đi giày khủng bố. Phải chăng bọn “Đề Ga” tung cả người ra đây?

Trưởng công an xem xét kỹ chiếc giày rồi mới nhận xét:

- Giày mới, ngoại cỡ, mác “Made in Italian”. Có thể khẳng định người đi giày là đàn ông chân cực to, có thu nhập cao, và rất có thể là người Tây đi giày Tây chứ không phải người ta đi nhầm giày Tây. Vậy người này đến làng ta với mục đích gì mà không đi theo đường công khai có đón rước trình báo. Liệu có thể là móc nối các phần tử đi nhầm giày Tây còn nằm im chờ thời không? Hay có thể bí mật quan sát địa thế để chuẩn bị thuê đất đặt nhà máy? Hay có thể chỉ đơn giản là đi tìm “gái sạch” thôi? Tôi đề nghị mở cuộc điều tra quy mô lớn làm rõ vấn đề vì tình huống nào xảy ra cũng đều ảnh hưởng trầm trọng đến vận mệnh dân làng, đúng như đồng chí Trưởng thôn đánh giá. Là địch thì có biện pháp bảo vệ an ninh. Là bạn làm ăn thì chuẩn bị tâm thế thương thuyết giá đất và giải phóng mặt bằng. Lưu ý thêm đây là vấn đề nhạy cảm dễ thành điểm nóng, kinh nghiệm nhiều nơi từng có khiếu kiện tập thể, vượt cấp, kéo dài. Còn là kẻ chơi gái thì có biện pháp cảnh báo, giáo dục con cái và kiểm soát ngăn chặn nạn HIV-ết.

Hội trưởng người cao tuổi đề nghị:

- Nếu quả thật có chuyện trâu sợ giày giống việc ngựa sợ hài thì có nghĩa là có quý nhân đến làng. Vậy trước hết ta hãy sắm một cái lễ tươm tươm một chút cáo thánh cầu an là hơn cả.

Trưởng ban mặt trận đề xuất:

- Thực hư thế nào chưa rõ, ta nên học người xưa mở hội thử giày, và từ nay lấy đây là lễ hội truyền thống độc đáo của làng. Chẳng gì làng ta đã từng tổ chức hội thử hài để lại đậm dấu ấn trong dân gian và lại đang thờ Đức thánh Thị Đớn cả nước biết tiếng. Khôi phục được hội thử hài vừa đem lại danh tiếng cho làng, vừa nêu cao được nét bản sắc văn hoá dân tộc. Rồi đây khách thập phương, khách du lịch sẽ đổ đến tấp nập, kinh tế sẽ trỗi dậy chứ còn tìm đường nào nữa.

Ý kiến của trưởng ban mặt trận được các vị đại biểu sôi nổi thảo luận ngả theo. Trưởng thôn tóm lại:

- Lâu nay ta có vốn mà không biết phát huy, thật tiếc. Nay ta sẽ phát huy hết nội lực. Sẽ có lễ ra đình. Sẽ có hội thử hài. Hội tổ chức đúng ngày đình đám vào đầu tháng sau. Ngay từ bây giờ ta đã phải lo công tác chuẩn bị. Hội phải thật ấn tượng. Giao cho ban công an bảo quản nghiêm cẩn chiếc giày, ai nhận cũng không cho xin, phải chờ qua hội thử hài mới trả, hơn nữa, còn trao phần thưởng của làng thật trọng thể. Giao cho văn hoá thông tin lo tuyên truyền cổ động hâm nóng lễ hội ngay từ bây giờ. Giao cho nông dân… Giao cho phụ nữ… Giao cho thanh niên… Cuối cùng là việc của mụ Ếm, mụ sẽ dắt trâu từ bờ sông theo sau đội rước hài về đình. Yêu cầu cả trâu cả người phải trang điểm rực rỡ . Thế nhé!

                                                                                 *****

Nghe tin làng Nghĩa Trụ mở hội thử hài, dân hàng phủ nườm nượp đến xem. Ô tô, xe máy nối đuôi nhau cào bụi ở đoạn cuống nhau. Rồi dồn ứ bên cầu răng bà lão. Đội Cờ Đỏ mau mắn đóng cọc chăng thừng thành bãi gửi xe độc quyền, lấy giá vé theo mức hội Lim, hội Gióng. Chồng mụ Ếm thấy khách run rẩy qua cầu liền hạ con thuyền Nô-ê phòng lụt trên xà nhà xuống chở khách. Mấy bà tháo vát sai con cắm cọc chăng bạt bày bán quà, chè đỗ đen suốt từ bãi đỗ xe về đến tận đình làng. Trưởng thôn đi kiểm tra gật gù: kinh tế trỗi dậy từ du lịch chứ đâu xa. Mà kinh tế du lịch là công nghiệp không khói cơ đấy, ít đầu tư, ít hại môi trường mà thu lợi lớn. Khách đến với hội thử hài cũng nhẹ bước chân vì cánh văn hoá thông tin đã mắc thêm loa, lại chọn phát toàn bài ca có chủ đề giày dép nổi tiếng. Nào là “Quần lĩnh áo the mới, tay cầm nói quai thao, chân đi đôi guốc cao cao”. Nào là “Ta vượt trên triền núi cao Trường Sơn, đá mòn mà đôi gót không mòn”. Trưởng thôn gật gù hài lòng. Cánh văn hoá thông tin làng ta có tính chuyên nghiệp, thạo việc ra phết đấy chứ.

Đến giờ hành lễ mụ Ếm mặc áo tứ thân ngũ sắc giày thêu kim tuyến, khăn vấn hồng, yếm đào, mặt hoa da phấn phớn phở dắt con trâu khoác áo choàng tua kim tuyến theo đội rước giày, chính chiếc giày trâu và mụ phát hiện ra. Mụ có quyền phớn phở lắm chứ, vì mụ đâu chỉ phát hiện ra chiếc giày, mà phát hiện ra cả hội thử giày này đấy chứ.

Trong khi mụ Ếm phớn phở dắt trâu đi thì từ một mảnh chĩnh vỡ ở bờ tre ao Cả gần đình làng xuất hiện một làn khói trắng mỏng manh bay lên. Làn khói bay theo đoàn rước hài. Khi chiếc giày được trịnh trọng đặt lên bàn thử trên sân khấu mới dựng trước sân đình thì có ai đó gióng trống rung chuông để đội rước rước thánh ra khai hội vào việc. Làn khói lượn một vòng trên đầu mụ Ếm rồi tụ vào người mụ mất hút. Mụ Ếm rùng mình ớn lạnh, mặt ngơ ngáo, mắt long lanh đảo điên. Rồi như một diễn viên tuồng ưu tú mụ Ếm bật cười khanh khách rẽ đám đông chạy lên sân khấu cướp diễn đàn. Dải áo ngũ sắc phấp phới lượn theo như hình làn khói mỏng bay khi trước. Mụ hoa chân múa tay mấy vòng, đoạn kiểu cách hai tiến một lùi đến gần chiếc giày. Kịch hơn cả kịch. Mụ Ếm ngắm nghía soi xét, rồi ngửa mặt ngưỡng thiên cất tiếng du dương:

- Bớ người ta! Ta ra đây cũng nên xưng danh trước. Thị Cám làng Nghĩa Trụ là ta. Mấy trăm năm qua ta chìm trong oan khuất nên hồn chưa siêu thoát. Nỗi oan đời ta từ hội thử hài. Ta chờ mãi, chờ mãi đến hội thử hài lần thứ hai này mới được phép giãi bày oan khuất. Bớ người ta! Nhà ta có hai chị em, Cám ta là em, con dì ghẻ của Tấm. Ta bé bỏng lại bấy bớt, tất nhiên việc đồng áng việc nhà dồn hết lên vai mẹ ta và chị Tấm. Với ta chỉ cần biết ăn ngủ, biết coi nhà đã là ngoan. Những khi rỗi việc đồng ta theo chị Tấm đi xúc tép ngoài sông cho vui chị đâu khiến ta mang giỏ. Thỉnh thoảng chị ném cho con sinh sôi để ta nghịch cho đỡ chán. Chị bảo “Sinh sôi lớn thành chuồn chuồn bay lên trời đấy”. Ta hỏi: “Có ăn được không”. Chị bảo: “Ăn được, béo ngậy đấy”. “Ăn thế phí mất con chuồn chuồn chị nhỉ”. Ta như thế mà người ta nỡ gán cho lười làm, lại còn lừa chị đầu chị lấm chị ngụp cho sâu để trút sạch giỏ tép của chị. Cái hôm ta khệ nệ mang giỏ tép về là do hôm ấy xúc được ít, chị bảo ta mang về trước để mẹ rang ăn cơm kẻo muộn. Hôm ấy mẹ có mắng chị mấy câu vì sợ ta bị lấm bẩn và sợ nghịch tép bị dây mùi tanh. Có thế thôi mà người ta vẽ rắn thêm chân, cứ theo câu “bánh đúc không xương” mà đổ xấu cho mẹ con ta. Người đời nghe thì thương Tấm lắm, ghét mẹ con ta lắm. Khi ta hơi lớn, tuy đã tập làm đồng nhưng đâu có thể tháo vát như chị. Bù lại ta khéo may vá thêu thùa. Ta thêu đôi hài cho ta, cũng thêu cho Tấm đôi hài tương tự. Hội làng năm ấy mẹ và chị còn phải dọn dẹp nấu nướng, ta xỏ hài lỉnh đi chơi trước. Chỉ là muốn khoe hài thôi. Ta đi ra đình. Ta đi ra chùa. Ta qua chợ. Ta sang xóm trước vòng về xóm sau. Rồi ra sông. Mọi ngày đi cầu chân không trơn trượt, nay có hài chắc đỡ trơn hơn, thế là ta quyết thử xem sao. Vừa đi vừa ngắm hài mãn nguyện. Bất chợt hài mắc mấu tre làm ta trượt chân suýt ngã. Trong lúc luýnh quýnh hài bên trái tuột khỏi chân rơi xuống nước. Như con thuyền nhỏ nó lững thững trôi một đoạn rồi chìm nghỉm. Tức phát khóc lên được. Ta đành thập thễnh quay về, tính lấy hài của chị, chẳng gì cũng là hài do mình khâu được. Về đến nhà mẹ và chị đã đi rồi. May quá chị quen đi đất nên chẳng buồn xỏ hài đi hội. Hài rộng, ta phải độn thêm vải quấn vào bàn chân mới đi được. Ra đến đình đang xảy ra hội thử hài rồi. Nhận ra hài của ta, ta sấn lên xin lại. Người ta cười ầm lên bảo nhận vơ là vợ thằng nhân, nó cho ăn bún nó vần cả đêm. Ta xấu hổ quá định bỏ chạy. Đúng lúc ấy chàng đứng chắn trước mặt, cầm tay ta mời thử hài. Người ta nóng như bị sốt. Nhưng hỡi ơi, hài của ta khâu vừa khít chân ta, thế mà lúc thử lại lọt thỏm. Đó là chân ta quấn vải nên bị bó lại. Cái chính là vải làm hài ngâm nước đã mềm ra, lại bị nhiều chân to thục vào làm giãn ra. Chàng bảo: Hài này giống hài em đang đi, nhưng là hài của người lớn hơn chứ không phải của em. Em đừng buồn nhé. Người ta lại cười ầm lên hát nhạo: chân ải chân ai, lọt thỏm thế nai, cho truyền người khác. Ta chạy về khóc tức tưởi. Hài của ta sao chàng bảo của người khác. Cuối buổi ta giật mình thấy chàng đến nhà. Tưởng chàng đã chịu biết hài ấy là của ta. Hình như miệng ta đã cười tươi. Nhưng không, chàng dắt tay chị. Hoá ra chính chị đã đi vừa hài của ta. Chàng muốn chị đi cả đôi. Tất nhiên chị không thể đi vừa chiếc còn lại. Tất nhiên ta xỏ vừa như in. Ta kể cho chàng biết chuyện khâu hai đôi hài, chuyện ban sáng leo cầu tuột hài. Chàng cho đón luôn cả ta. Chị là Đông cung, ta là Tây cung. Bấy giờ ta còn bé, chị đã là thiếu nữ phổng phao nên chàng chỉ nhất yêu mình chị. Chị cũng quý yêu ta lắm. Chị luôn bảo nhờ em khéo tay thêu hài, nhờ em làm rơi hài mà hai chị em mới được sung sướng vinh hiển thế này. Ta sượng sùng chỉ bảo đó là duyên lạ, là số trời định thế. Đến ngày ta dậy thì chàng bắt đầu chú ý và năng đến Tây cung hơn. Chị hậm hực bóng gió, gặp ta là quăng thúng đá niêu. Ta sợ không dám sang gặp chị nữa. Ta tưởng chị có bầu nên thay tính đổi nết thế. Thấy bảo người có bầu thì khó ở. Rồi chàng cầm quân nam chinh. Liền đó có người tố cáo ta làm hình nhân thế mạng mưu hại chính cung hoàng hậu tranh quyền nhiếp chính. Ta bị kết tội tùng xẻo. Chị ngồi trên ngai chăm chắm nhìn ta chịu đau đớn đến chết. Chỉ đến khi ta thật chết rồi, chị mới vấn vội vành khăn trắng khóc hờ đứa em dại dột, ác độc. Hàn lâm học sĩ dựa vào đó viết nên câu chuyện ác giả ác báo do ta đáng tội chết cả trăm lần. Ta chết rồi biết cãi làm sao. Văn võ trong triều ai dám bênh ta để mà mang vạ tru di tam tộc. Ta toàn thua thiệt. Thua trong hội thử hài. Thua trong chốn hậu cung. Thua trong miệng thế dân gian. Thua cả sau khi chết không người thờ cúng. Bao năm qua ta chỉ là mảnh chĩnh vứt ngoài bờ tre chứ đâu được là chuông khánh như người đời vẫn giễu. Bớ người ta! Nỗi oan của ta ai đã thấu hiểu chưa.

Trong khi mụ Ếm đang lên đồng thì các cụ ban khánh tiết đã rước Đức thánh Thị Đớn ra đến nơi khai hội. Mụ Ếm bỗng rú lên khủng khiếp cứ như đang bị tùng xẻo đến chết. Các cụ vén khăn che mặt tượng thánh lên, nhưng lạ lắm, khăn dính chặt như bị sơn bết lại, từ rìa khăn nhỏ ra từng giọt nước đỏ như máu!

Hội thử hài làng Nghĩa Trụ đã rã đám mà vẫn chưa tìm được chân nào xỏ vừa giày. Mụ Ếm đòi giày nhưng làng không cho.
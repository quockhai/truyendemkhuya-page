Đây là chuyện đã xảy ra rất nhiều năm rồi.

Ngày hôm ấy, khi tôi mang sọt tre đến sở quản lý lương thực của xã tìm bố tôi để lấy chìa khoá, thì lão Điền đầy mặt nung núc mụn cóc nhìn thấy dưới đáy sọt tre lót bằng một tờ giấy có in dòng chữ “Xác định quyết tâm, không sợ hy sinh, vượt mọi khó khăn, giành lấy thắng lợi”, bèn nói tôi là tên phản cách mạng nhóc con. Tôi cãi lại một câu, lão ta bèn kéo tôi đến công xã, vừa đi vừa nói: “Mày lấy chỉ thị tối cao để lót trôn sọt tre, mày có biết phạm tội gì không?”

Tôi nói: “Cháu phạm tội nào đây? Sọt tre không có trôn, cháu đi ra ruộng đào rau dại lấy tờ giấy lót để khỏi rơi rau, tôi phạm tội gì nào?”

Lão Điền trừng mắt nói: “Mày là tên phản cách mạng hiện hành!”

Nói xong, lão xông đến giật chiếc sọt tre của tôi, tôi liều mạng giữ sọt và lùi lại phía sau. Lão nhấc bổng tôi và chiếc sọt tre lên, hai chân tôi rời khỏi mặt đất giãy dụa. 

Khi lão kéo tôi đến một cái cổng có treo tấm biển “Uỷ ban cách mạng Công xã”, tôi mới không chịu đựng được nữa chửi lão một câu. 

Khi ấy, sau khi nghe người khác nói lại, bố tôi vội chạy lại, vừa chạy vừa gào: “Chủ nhiệm Điền! Chủ nhiệm Điền! Trẻ em nhỏ không hiểu chuyện đời, ông không nên đối xử với trẻ con như vậy!”

Khi ấy, lão Điền mới buông tôi xuống, nói: “Con ông dám dùng chỉ thị tối cao lót trôn sọt, có phải ông xui nó không?” 

Lúc ấy, tôi thừa cơ chạy thoát, đứng chỗ không xa lắm, lau nước mắt, vừa khóc vừa chửi lão Điền. Đang chửi, từ khe hở giữa các ngón tay, tôi nhìn thấy bố tôi đang giải thích với lão Điền, đột nhiên ông quay đầu lại, lao thẳng đến chỗ tôi. Tôi cảm thấy chuyện xấu rồi, vội quay người chạy. 

Bấy giờ, tôi mới bẩy tuổi, thân hình nhỏ thó, làm sao có thể chạy bằng bố tôi chứ? Khi tôi chạy được 20 mét, bố tôi chạy đến sát lưng tôi. Tôi ngoảnh mặt lại nhìn thấy nét mặt hung ác dữ tợn của bố tôi. Tôi tuyệt vọng rồi. Lúc ấy, bố tôi đã tóm được véo tai tôi. Tôi đau đớn không chịu được. Tôi cảm thấy tai tôi bị bố giật cụt mất. Tôi cầu xin ông tha tội, nhưng bố tôi vẫn không chịu buông tay. Tay của bố tôi túm tai càng chắc hơn, tôi hôn mê bất tỉnh ngã vật xuống đất…

Thời gian thấm thoắt thoi đưa, tôi đi học, đi lính, tham gia công tác, luôn luôn canh cánh trong lòng mối hận với bố, không muốn nói chuyện với bố, dù chỉ một câu,      tình cảm cha con luôn luôn không hợp nhau. Năm tháng vô tình, tôi cảm thấy bố tôi mỗi ngày mỗi già nua, tính tình cũng thay đổi, ngày càng muốn tiếp xúc gần gũi với tôi. Nhưng có một sức mạnh hữu hình và vô hình cứ luôn luôn khuấy đảo trong lòng tôi. “Bố ơi! Ông có phải là bố đẻ của tôi không, mà lại đối xử như vậy với tôi, ra tay mạnh  quá, ác quá như vậy!” 

Đến một buổi tối, đúng lúc ông hấp hối sắp chết, bố tôi nói: “Trong suốt cả cuộc đời này, bố có lỗi với con nhất là khi con lên bẩy tuổi, con va chạm với lão Điền phụ trách Sở quản ký kho lương thực, con chửi người ta, bố đã đánh con” 

Khi ấy, tôi không hề ngạc nhiên, song, tim tôi bỗng run lên, nói: “Bố vẫn còn nhớ ư?”

Bố tôi nói: “Nhớ chứ!”

Tôi nói: “Con vẫn nhớ  như vừa mới xẩy ra!”

Bố tôi nói: “Đó là sai lầm của bố!”

Bố tôi chảy nước mắt, nói: “Khi ấy, lão Điền là phái tạo phản, bố lại làm việc dưới quyền lão ta, nếu như bố không đánh con như thế, thì gia đình chúng ta có khả năng bị quy chụp là bọn  phản cách mạng hiện hành.”

Tôi im lặng không nói không rằng. Bây giờ, tôi biết khi ấy mà bị quy kết là phản       cách mạng hiện hành thì hậu quả sẽ thế nào đây. Ân oán giữa hai bố con mấy chục năm vẫn có thể hoá giải trước khi bố tôi sắp qua đời. Nhưng, tôi mong muốn những năm       tháng như thế mãi mãi không tái diễn nữa, những chuyện như vậy sẽ vĩnh viễn không xẩy ra nữa.
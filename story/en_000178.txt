Rudolph Kaltrik, nhà văn Séc nổi tiếng với những sáng tác về cuộc chiến đấu anh dũng của nhân dân Trekhoxlovaky chống phát xít Đức xâm lược trong Đại chiến thế giới lần II. Phần lớn tác phẩm của ông nói về vùng nam Trekhi (Séc), nơi trải qua những năm tháng tuổi trẻ của tác giả. Có những truyện ông viết về số phận của những người Do Thái trong chiến tranh, truyện “Con dao” là một trong số đó.
 
.Rudolph Kaltrik (Séc)
 
Đêm, chúng ở đâu đó trên núi Novograđ. Trong núi ẩm ướt và băng giá. Nửa sáng hôm sau, chúng vòng trở lại theo lối mòn, đi xuống sườn núi phía đông, hậu phương quân đội Xôviết. Bọn chúng khoảng 300 tên. Chúng rảo bước, một đám xám xanh, và bám sủa những chiếc xe tải của chúng là cả một bầy những con chó săn sói. Người thì chúng tránh mặt.
Hôm đó, vào lúc 4 giờ rưỡi sáng, Ruph như được sinh ra lần thứ hai.
Sự việc xẩy ra ở trong thành phố, nơi không ai biết cô là ai và mấy ngày trước đó cô đã chạy từ đâu đến. Từ nơi ẩn náu của mình ở trên gác 4 của toà nhà hiệu thuốc, cô quan sát đường phố gần Yinđpjkhova - Građs. Đầy đường inh ỏi tiếng còi xe và lính Đức; bám đuổi theo sau là những đội tuần tiễu cưỡi ngựa tập tễnh đi cuối cùng.
Tiếp đến là một đoàn xe tải màu xanh, đó là những người lính Xôviết. Cứ như một giấc mơ, Ruph sung sướng khóc, xuống cầu thang, và thận trọng đi ra quảng trường. Ánh nắng giây phút ấy trở nên vàng rực, và bầu trời - xanh lơ, xanh biếc hơn, cô thấy trên quảng trường một vị tướng Xôviết trẻ với đôi mắt đẫm lệ, ôm hôn uỷ viên hội đồng nhân dân và một em bé, rồi đi thăm các bà già. Bỗng trên đường phố bùng lên một tràng súng máy tự động CC.
11 người ngã xuống một loạt trên đất ẩm ướt, no nước của trận mưa ngày Chủ nhật. Người ta chôn cất họ dưới chân tường thành phố; họ được xếp cạnh nhau thành một đội ngũ đáng sợ: những bé gái xanh xao, những người đàn ông còn đội mũ sắt, những người phụ nữ rất rõ khuôn mặt, các sinh viên còn nắm chặt tay, mắt họ bây giờ không còn nhìn thấy gì nữa.
Những chiếc xe chở những đội cận vệ đến trong tiếng chuông thành phố đón chào, những cái chuông bọn Đức đã không kịp lấy đi, cho vào nhà máy luyện, rồi đưa đi tiếp, theo hướng Buđeovis, đến thung lũng Vltava, cùng với xe của người Mỹ. Chúng chỉ còn để lại một nhúm lính, công cụ để chốt chặn ngã ba, nơi hai con đường gặp nhau ở chỗ đất cứng giữa vùng sình lầy.
Trời đã về chiều. Mặt trời vội lặn xuống sau núi Novograđ.
Dân thành phố kéo  nhau ra đường ăn mừng, tò mò xem những người lính Xôviết - trung đội của thiếu tá Surov, sờ tay vào vũ khí của họ, ngắm những khuôn mặt mệt mỏi, những bộ quân phục tốt bền, những chiếc thắt lưng da siết chặt. Đó là cả một cuộc cổ vũ lớn. Dân phố đi cả nhà cùng với bọn trẻ, cờ trong tay, nào là những ngư dân, công nhân nhà máy bia, học sinh trung học, cả cô gái gầy, nhỏ thấp, mặc váy của một em bé gái 14 tuổi, ở hiệu thuốc, cần được giúp đỡ.

Những người lính vây quanh một thiếu tá dáng không cao lắm, còn rất trẻ, hoàn toàn khác với sự hình dung của người dân thành phố do bọn giặc thêu dệt. Các bà địa chủ và nông dân nói cười với những người lính Nga. Những người đàn ông đề nghị Hồng quân cùng uống với họ một ngụm Vođka trong những cái chai cuối cùng như có phép lạ là còn nguyên vẹn. Từ những ngôi nhà thấp lè tè ở gần ngã ba, chủ nhà bưng ra xúp, còn bọn trẻ mang hoa tử đinh hương hái trong vườn và những củ hành mùa xuân còn xanh non và giòn.
Từ phía đầu kia chạy lại là một thằng bé chừng 12 tuổi, gặp ai cũng lo lắng thông báo rằng nó vừa nhìn thấy ở gần ngã ba, trong rừng có bọn Đức. Đầu tiên không ai nghe thông tin nghiêm túc đó. Không còn là giờ đầu tiên yên tĩnh. Chiến tranh, lạy Chúa, đã chấm dứt rồi. Nhưng sau đó một người đàn ông đã dẫn thằng bé đến chỗ thiếu tá. Nó chắc chắn rằng có bọn Đức và chúng không ít hơn 100, hoặc có thể hơn. Mấy chàng lính trẻ nghe thế cười ầm, Surov cũng bật cười theo: đã mấy tuần qua, những toán phát xít không nhiều, chừng ba, bốn tên lại đi tắt qua tuyến mặt trận về phía tây, thường đi với chúng còn có phụ nữ. Có thể đứa bé đã nhìn thấy ai đó. Cũng có thể là nó hoàn toàn không thấy gì, chỉ là do tưởng tượng mà ra.
Chợt nó nói thêm:
- Chúng có cả ngựa và xe tải.
- Có thể đó là những người chạy nạn. - Có ai đó lên tiếng.
- Không phải đâu, các chú ơi! - Thằng bé lại nói - Họ có cả ngựa và xe mà!
Những người phụ nữ im lặng; trên những khuôn mặt mấy phút trước đây còn hân hoan mừng chiến thắng, bây giờ đã biểu lộ sự sợ hãi. Các ông chồng cầm lấy vũ khí ở nơi cửa treo quần áo, cười sặc, đấm nhau, kể cho thiếu tá biết sáng chủ nhật qua, đi tắt ngang thành phố về phía tây có khoảng 300 tên tản ra, trang bị đến tận răng, còn rất trẻ, chừng 19 tuổi, đó là những tên xã hội - cách mạng.
Người sĩ quan gật đầu và nhìn thẳng về phía trước mặt, phía khu rừng.
Có mấy gia đình đã vội vã tìm chỗ ẩn nấp sau bức tường thành. Những nhà khác thì tìm chỗ kín đáo sau mấy ngôi nhà gần ngã ba, đề phòng đạn nổ.
Chỉ còn một nhóm đàn ông thành phố là không nhúc nhích khỏi chỗ. Họ nhìn ra đường chờ đợi, nơi họ vừa mới chạy qua, giờ con đường đã mất hút trong bóng hoàng hôn sớm của rừng, nhìn ra cánh đồng rộng, phẳng, theo hai hướng con đường, ven bờ đã mọc cỏ non. Đó từng là những bãi vui chơi hồi nhỏ của họ; họ cũng đã từng dạo chơi trong rừng cùng với những cô gái.
Con đường ấy lúc này có thể là hướng bọn giặc kéo đến.
Một chiến sĩ đi trinh sát về.
Đó là một cuộc trinh sát kỳ lạ, tình cờ, một ý tưởng chỉ có thể nẩy sinh trong những ngày tháng 5/1945. Người lính đi trên một cái xe đạp của ai đó, một tay cầm súng, người cúi sát ghi đông, đạp xe theo đường thẳng vào rừng. Lát sau nghe tiếng súng nổ. Lập tức gần ngã ba cũng nghe súng nổ dữ dội, loạn xạ. Có mấy viên đạn lạc vào tận nhà dân. Phóng qua dưới làn đạn địch, xe đạp trinh sát vẫn bình yên vô sự.
Chẳng bao lâu tiếng súng im bặt, và nơi ngã ba yên tĩnh vô cùng.
Đồ đạc cồng kềnh trên vai, trẻ con bồng trên tay, dân ngoại ô chạy vào trong thành phố. Thành phố Yuznotresky nhà cửa dường như dày đặc hơn, rải ra khắp ven đầm lầy, những nhà mái vòm vây quanh quảng trường, dường như đang co chân lại, chuẩn bị nhẩy xa hơn. Nó vừa sáng lên những cửa sổ, giờ lại bao trùm bóng đen chiến tranh. Sáu năm chiến tranh đi qua nơi đây không có một tiếng súng. Vậy mà bây giờ thành phố lại chuẩn bị bước vào một trận đánh. Những người đàn ông sẵn sàng vũ khí trong tay, nhanh chóng chiếm các đường phố, tiếp cận cổng thành cổ. Còn kia là nhóm chiến sĩ dưới sự chỉ huy của thiếu tá Surov đang chốt trong công sự gần ngã ba.
Surov chờ đợi thời khắc tấn công. Anh nghĩ là địch sẽ nổ súng khi bóng đêm sập xuống. Anh cũng cho rằng đến sáng mai thì sẽ một chọi một - trung đội anh chỉ có vài chục đàn ông và thanh niên. Đêm xuống, bọn địch cũng ngừng chuyển quân, để rồi sáng mai lại đi sớm. Từ giờ đến trước lúc rạng sáng, anh cần phải chốt ở đây, nơi mặt đất bằng phẳng, gần ngã ba, giữa đầm lầy, xung quanh chỉ có vài ngôi nhà một tầng.
Người sĩ quan nhảy lên trên mặt đất để quan sát.
Trên những mái nhà lặng lẽ tối om là bầu trời xanh đen, và những ngọn gió ẩm lạnh khẽ rung trên cành lá xum xuê của vườn cây ăn quả.
Đường phố vắng tanh.
Chợt anh nhìn thấy có một người con gái nhỏ gầy ở đầu phố.
Điều đó anh không thích tí nào. Một người con gái cô độc làm thiếu tá sợ hơn là có mìn nổ. Lặng lẽ, anh đi đến gần cô gái. Cô ta có đôi má hóp và đôi mắt rất to.
- Sao cô lại đứng đây? - Anh hỏi gay gắt. - Về nhà đi!
Cô gái không biết trả lời sao, chỉ kịp há hốc mồm.
- Nào, nhanh lên, chạy đi! - Anh giục - Kẻo người nhà đợi!
Cô gái lắc đầu:
- Không có ai đợi tôi cả... tôi không có nhà... - Sau cô nói thêm - Tôi trốn trại tập trung. Tôi là người Do Thái.
Cô gái nói những lời đó rất khẽ, nghe như khóc, cứ như lúc này nói điều đó cô cần phải hổ thẹn.
- Vậy thì lại đây! - Surov nói, lúc này đã bớt gay gắt.
Cô gái chần chừ bước đi mấy bước. Chàng sĩ quan quay lại, nhìn vào đâu đó trong bóng tối:
- Aizenstam, em gái cậu kia kìa!
Từ dưới đất đi lên một chiến sĩ cao to. Trong bóng tối cô gái không nhìn thấy gì hơn và cũng không phân biệt được gì cả.
- Tôi là Aron.
- Còn tôi là Ruph. - Cô gái đáp lại.
- Anh sẽ chăm sóc em, em gái - Anh nói với cô. Anh ta đi đến chiếc xe tải gần đó, phút sau quay lại, mang theo một tấm chăn. Aron lặng lẽ khoác tấm chăn lên đôi vai gầy của cô gái. Cái chăn thòng xuống sát đất. Cô gái thấy vậy, nhấc lên bằng những ngón tay gầy, run run vì cảm động. Một giây họ đứng lặng đối diện nhau. Sau anh chàng hỏi:
- Em biết bắn không?
- Không. - Cô gái nói thầm thì.
Cô bị lùa vào trại tập trung từ lúc 14 tuổi cùng với cha mẹ, chị gái. Vũ khí - cô trông thấy trong tay bọn Đức. Cô bị rớt lại ở đầu kia, nơi ngã ba, khi những người khác đã bỏ chạy cả, vì thế mà cô biết là sắp có giao chiến. Cô không biết đánh trận, nhưng cô rất muốn chiến đấu bên các chiến sĩ Hồng quân, ở nơi ngã ba, gắn số phận mình với số phận họ. Hơn nữa, thì cô không thể.
Từ trong bóng tối vang lên tiếng thiếu tá:
- Trong ngôi nhà cuối cùng ở đầu kia, người ta bỏ lại một bà già. Hãy đến ở với bà, cô gái! 
Ruph liền nẩy ra một ý định.
Điều đó đối với cô như là một sự giải phóng.
- Vâng - Cô nói và vội vàng bước đi trên con đường lạnh giá.
Khó khăn lắm cô mới tìm thấy cái cửa giậu; những năm ăn đói đã làm cô bị mắc bệnh quáng gà - mắt cô nhìn rất kém vào lúc chập tối. Cô dò dẫm đi qua sân, và sờ soạng mở cửa, lách vào phòng ngoài.
- Ai đấy? - Tiếng người đàn bà trong nhà gọi giật giọng, khi thấy Ruph đi vào trong bếp.
Cô lên tiếng nhẹ nhàng:
- Đừng sợ, bà ơi!...
Cô nhận ra cái cửa sổ hình tứ giác, liền nhấc tấm chăn trên người mắc lên cửa sổ, che kín các kẽ hở. Sau đó cô quay lại chỗ cửa vào, mò trong bóng tối châm đèn. Cô thấy ở góc nhà, nằm đắp chăn là một người đàn bà nhợt nhạt, bại liệt.
- Chẳng có ai cần đến tôi - Bà ta bắt đầu khóc, khi nhìn rõ trước mặt là một người con gái. - Cháu đi đi, người ta đã vứt ta lại đây cho chết...
- Không, người ta bảo cháu đến đây ở với bà - Ruph nói - Cháu đã gặp người của bà và họ đã bảo cháu đến đây.
- Cháu là ai?
- Cháu là dược sĩ - Cô nói dối.
- Dược sĩ - Bà già gật đầu, có vẻ yên tâm.
Ngay lúc đó, Ruph nghe có tiếng bước chân ai đi vào phòng ngoài. Cô liếc mắt vào bếp. Trên bàn, gần lò sưởi có một con dao, cô cầm lấy, giấu sau lưng. Có tiếng gõ cửa.
- Vào đi - Cô nói nhỏ.
Aron bước vào. Trên tay anh là một khẩu súng máy loại nhẹ. Cô mỉm cười và đặt con dao vào chỗ cũ. Người lính khẽ cúi chào bà già và chỉ vào con dao:
- Sợ à? - Anh hỏi cô gái.
- Không. - Cô nói.
Trại tập trung, nơi cô đã sống qua nhiều năm, điều tồi tệ nhất – nỗi sợ hãi.
- Cho tôi uống ít nước. - Anh nói.
- Khi anh đưa cốc nước lên miệng, cô gái chợt hiểu là anh chỉ lấy cớ đến để được thấy mặt cô. Đối với cô, đó là điều bất ngờ, và cô chợt trào nước mắt. Ôi, cô muốn đứng trước anh, với gương mặt thanh xuân, không hề kêu than về những năm tháng khổ ải, ở trong vòng tay anh với khuôn ngực đầy đặn, trong ánh sáng tươi đẹp của tuổi trẻ... Cô đã 19 tuổi, nhưng lại mặc váy thời con gái 14. Cô buồn đau vì tất cả, và bất giác đưa những ngón tay gầy, mảnh mai lên chùi khoé mắt.
- Đừng khóc - Anh an ủi - Sáng mai gặp lại nhé. - Và anh đi ra cửa, nói - Hãy mở cửa sổ ra. Rất tiếc là nhà cửa kính. Nếu sợ thì tôi ở gần đó, chỗ bờ giậu ấy. Không sao.
Anh đi ra khỏi nhà là cô liền mở cửa sổ. Căn phòng lập tức tràn ngập hương vườn. Cô khoan khoái như uống lấy hương thơm không biết chán, đôi mắt cô mở to, như một đứa trẻ đang đói.
Và ngoài xa kia trên bình nguyên xuất hiện những tín hiệu của tên lửa. Ruph thò đầu ra ngoài cửa sổ. Trong giây phút ấy, trong chớp mắt ấy, cô lại nhìn thấy họ - những dáng hình thân quen của những người bạn tù - nhìn thấy họ nhảy, chạy qua cánh đồng, đến chỗ ngã ba, trong một hàng ngũ siết chặt. Cô chạy xuống bếp, cời lên ngọn lửa và ngồi trùm chăn cạnh bà.         
Đã bắt đầu cuộc tấn công.
Tiếp theo là đợt hai, đợt ba, và giữa các đợt, khi thì kéo đến gần, khi thì đẩy ra xa, là sự  im lặng khó hiểu, rồi sau đó lại rộ lên tiếng súng của Surov, không khí quanh bà già và cô gái chấn động bởi những tiếng nổ, bụi trên tường rơi lả tả, cửa kính leng keng, mặt đất và bầu trời bùng lên những đám lửa, sau lại tối om trong tiếng đạn réo, xa xa - đâu đó gần khu rừng, và rất gần - ngay dưới cửa sổ, nơi Aron đang bắn.
Có cảm tưởng đêm dài vô tận.
Nó bùng lên dữ dội và dịu đi, rền vang, rung chuyển rồi lại yên tĩnh, mất hút như con tầu trong vực biển.
Trong phút lặng yên như thế, Ruph nghe có ai gọi tên mình. Cô đứng bật dậy, rời khỏi chăn, ra giữa bếp, lắng nghe phía có tiếng gọi. Lại có tiếng gọi tiếp, rồi tiếng gõ cửa, phía có lối ra vườn.
- Ruph!
Bước khẽ, thận trọng, cô đi ra gian ngoài, lần xoay tay nắm cửa, bám vào tường, bước ra vườn. Dáng nhỏ yếu, cô bước đi không một tiếng động. 
- Ruph...
Trời loé sáng như chớp, cô bước trên thảm cỏ đến góc vườn. Đó là một người cần được giúp đỡ. Không ai nói với cô điều đó, cô tự hiểu. Cô biết điều đó qua tiếng gọi tên cô.
Không nhìn thấy Aron, cô chạy lại phía có tiếng thở nặng nhọc của anh.
- Ruph - Anh rên rỉ và động đậy.
Cô cúi xuống xem và hiểu: anh sắp chết.
- Em đây - Cô hổn hển - Em đây mà, Aron! Em ở bên anh. Em sẽ giúp anh. Em sẽ lấy băng để băng cho anh. Em sẽ đi gọi thiếu tá. Nhưng em băng bó cho anh đã, sau đó chúng ta sẽ gọi anh ấy. Mọi việc sẽ tốt thôi...
Và cô cuống cuồng lục tìm băng trong túi của anh.
- Nằm xuống - Tiếng anh nói rất nhỏ.
“Không”, cô tự nhủ, và không tránh khỏi tuyệt vọng.
Ở trại tập trung, nơi cô đã trải qua, cô đã từng quen với cái chết. Cái chết hiện ra trên mặt người, trong con mắt, qua cử động, trong giọng nói, trước khi nó đến. Cô đọc được trên mặt mỗi người. Và lúc này co đã nghe “nó” đến trong giọng của Aron.
Cô quấn chặt băng vào cánh tay anh.
- Nằm xuống - Anh nhắc lại - Tiếng súng không lúc nào ngớt... Chúng nó không được giết em - Cô nằm xuống cạnh anh, bên công sự nhỏ ở góc vườn.
Bên kia cánh đồng, đâu đó gần khu rừng, có tiếng ngựa hí. Trong yên lặng, tiếng ngựa hí vang lên như là tiếng súng. Người chiến sĩ nói:
- Ruph, hãy hứa với anh...
- Vâng?
Nhưng lúc đó cơn đau lại hành hạ anh. Anh cắn răng chống lại nó. Rồi cơn đau dịu xuống, hai người nằm im, Ruph nghĩ hình như anh đã chết. Anh lại nói, nghe như vừa ngủ dậy vậy:
- Hãy lấy những gì có trong túi của anh.
- Không, Aron - Cô nghẹn ngào, liếc nhìn mặt anh và bật khóc.
Nhưng rồi cô cũng lấy hết các thứ trong túi ra, đặt trên nền đất sét ngay trước mặt: một bó thư, cuốn nhật ký, tập giấy tờ, mấy đồng tiền, hộp diêm, con dao dài chiến lợi phẩm của Đức.
- Giấy tờ thì đưa cho thiếu tá, anh ấy biết cái gì và làm thế nào.
Lại nghe rộ lên tiếng ngựa hí. Sau đó là tiếng rít và tên lửa vút lên: Surov chờ đợt tấn công mới.
- Tiền thì giữ lại - Người lính nói tiếp - Nó cần cho em, trong tay em không có đồng nào. Còn con dao... anh đã lấy ở chỗ... - Anh lặng đi chừng một phút, sau khẽ cử động cánh tay trái: - Đồng hồ... hãy cầm lấy.
- Không - Cô gái nấc lên - Em không cần gì cả. Anh hãy giữ lấy, nó cần nhất đối với anh. Còn em... có đồng hồ ở chỗ hiệu thuốc... Rồi anh lại phải mua cái khác. Em không muốn lấy của anh, Aron. Em không muốn. Hãy giữ lại. Nó là của anh, nó cần cho anh.
Anh ngoái cổ nhìn cô. Gương mặt anh sát gần.
- Giá mà được hút thuốc.
Ôi, thế nghĩa là anh sắp chết. Cô sờ soạng lục tìm thuốc lá trong túi nhưng không được, tay lóng ngóng. Cô xoay lưng về phía khu rừng mới bật diêm để bọn giặc không thấy, châm thuốc và đặt vào môi người lính lúc này đã không còn sức sống. Anh bập bập môi rít vài hơi, cứ như là rít hơi thở cuộc sống, và tắt thở.
Mặt đất trở nên ẩm ướt, nồng nặc mùi bùn của sình lầy.
Cô gái muốn gào lên thật to - không phải sợ cái chết, điều đó từ lâu cô đã không còn cảm thấy nữa, mà là từ nỗi đau khủng khiếp, sự đau đớn mấy phút trước đây con người này đã trải nghiệm, chịu đựng, bây giờ anh ấy đã nằm chết lạnh bên chân cô. Cô ngồi lặng đi, diêm cầm trong tay. Ngày hôm đó đã bắt đầu một cách tuyệt vời, vậy mà lúc này tất cả bỗng đổ sập... Nỗi đau thương tràn ngập lòng cô. Cô nằm trên mặt đất lạnh, đưa tay yếu ớt lên lau nước mắt chảy xuống má. Cô than khóc mình, than khóc những giọt máu của anh, than khóc con người đã rất muốn giúp đỡ cô, chăm sóc cô, bởi cô không còn người thân, nhà cửa.
Trong giây phút ấy, bọn Hitler lại mở cuộc tấn công mới.
Trên con đường bằng phẳng từ khu rừng đến ngã ba, những con ngựa khiếp sợ tiếng súng, phóng như bay. Theo sau là những chiếc xe tải rú ầm ầm, xóc nẩy lên, lao như mất trí. Tiếng rú rít, tiếng súng nổ, và bóng tối; và trong bóng tối đó, từ hai phía cánh đồng, các đảng viên xã hội - cách mạng điên cuồng tấn công để chọc thủng phòng tuyến, tháo chạy về phía tây. Chúng bò rạp trên đường như những con rắn xanh. Đó là những kẻ tuyệt vọng. Cái chết, bao nhiêu năm chúng nắm trong tay đem đi gieo rắc khắp nơi, bây giờ quay lại bám riết lấy chúng. Chúng chạy nó, chúng hoá rồ vì khiếp sợ, giống như súc vật kéo xe lồng lên trên đường, trái khoáy với cái tên “đội quân bầu trời” của chúng, cái tên chúng mang trên người, giờ bị bật tung ở nơi ngã ba cùng với những trái lựu đạn của chúng.
Lúc đó ở trong vườn ngoại ô có một cô gái Do Thái nhỏ. Cô rờ rẫm trên mặt đất quanh mình, và tay cô chạm vào xác người lính. Rồi tay cô chạm vào con dao. Nó bây giờ thuộc về cô. Bỗng cô chạy vụt ra đồng, dừng lại nơi cây thông đuôi ngựa đầu tiên.
Đạn không bắn trúng cô. Người cô gầy guộc - một mục tiêu tồi.
Còn bọn Đức thì đã xáp gần. Chúng tiến sát sau cô, và Ruph không muốn kiềm chế thêm nữa.
Cô nằm ép xuống mặt cỏ xanh, và bắt đầu bò lên, thở nặng nhọc, luôn phải dừng lại nghỉ, lúc đó cô nghe từ phía khu rừng có tiếng bước chân của một tên đi lại gần cô. Trước khi hắn kịp tự vệ, cô đã đâm thẳng lưỡi dao vào lưng hắn.
Nhát thứ nhất, rồi tiếp những nhát khác.
Khi hắn không còn động cựa gì nữa, cô mới khom lưng chạy trở lại tren mặt đất sình lầy, và cô cảm thấy đau khổ về sự yếu ớt của mình, còn trái tim thì hồi hộp lo lắng về những con người, mà số phận của ngã ba phụ thuộc vào họ.
Như thế, cô chờ đợi cho đến sáng.
Những con đường hồi sinh, còn tiếng súng thì im bặt.
Cô nhìn thấy bọn Đức đi ra khỏi rừng - Chúng còn trẻ, bệch bạc, tay giơ cao quá đầu - Và chúng mất tự tin đi về phía ngã ba. Lúc đó cô cũng đứng dậy, bước đi giữa cánh đồng, gần đó có một xác chết, nằm như một đám cỏ khô từ năm ngoái, và cô kinh tởm chạy lên phía sau những tên hàng binh, cô cũng đi đến đó, thành phố còn đương ngái ngủ, nơi ấy có Aron, bà lão, và ngôi nhà mà gần đó thấp thoáng bóng dáng bồn chồn của thiếu tá Surov.
Đó là buổi sáng mùng 9 tháng 5.
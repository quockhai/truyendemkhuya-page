Hai hôm trước, vợ Trương Tam gọi điện cho anh ta, lời lẽ ngọt ngào tràn đầy yêu thương, rằng rất nhớ, rất muốn gần chồng. Nghe vậy, trong lòng Trương Tam phấn khích lắm. Trương Tam quyết định xin nghỉ mấy ngày phép để về với vợ.

Nhưng có một vấn đề lớn đặt ra trước Trương Tam: Đó là làm thế nào để xin ông chủ cho nghỉ được đây? Việc quản lý lao động của nhà máy xưa nay còn chặt chẽ hơn cả nhà tù. Hơn nữa, công việc sản xuất đang vào giai đoạn khẩn trương, công nhân viên muốn xin nghỉ phép rất khó khăn. Làm thế nào đây? Nếu nói thật thì chắc chắn không thể được. Nghỉ việc để về chơi với vợ ư? Ông chủ sẽ cho là thần kinh anh ta không bình thường. Có lẽ phải nói dối thôi, nhưng biết lấy lý do gì? Bố mẹ bị bệnh, hay là cha mẹ vợ có chuyện…đều là những lý do đã nhàm cũ cả rồi, ông chủ nhất định sẽ không tin. Phải rồi, tốt nhất là nói rằng vợ sắp sinh con gái.

Nghĩ sao làm vậy, Trương Tam lập tức chạy đi tìm ông chủ để xin nghỉ phép.

- Thưa ông chủ, tôi, vợ tôi sắp sinh con gái rồi! - Trương Tam nói.

- Ồ, đúng thế chứ? - Ông chủ gục gặc đầu - Vậy thì chúc mừng anh được làm bố nhé!

- Cảm ơn ông chủ! Là…thế này, tôi muốn…xin nghỉ mấy ngày phép, ông chủ xem có được không ạ?

Mặt ông chủ lập tức sa sầm:

- Trương Tam ơi! Nhà máy chúng ta hiện đang rất tình hình, anh cũng biết rồi đấy!

- Tôi biết! Tôi biết thưa ông. Thực lòng tôi cũng chẳng muốn nghỉ đâu, nhưng tình hình sức khỏe của vợ tôi không tốt, bác sĩ bảo có khả năng đẻ khó…- Trương Tam cố bằng mọi giá, làm cho mọi việc có vẻ nghiêm trọng hơn để mong ông chủ động lòng.

Ông chủ nhẹ giọng nhưng vẫn cứng rắn:

- Sà! Sao đầu năm nay ai cũng làm khó tôi thế nhỉ. Trương Tam này! Việc sinh con là của vợ anh, anh có về e rằng cũng chẳng giúp ích được gì nhiều. Theo tôi thì anh nên hoãn lại đi!

Có lẽ thất bại rồi! Trương Tam bức xúc, mặt xìu xuống như quả dưa héo, chỉ còn thiếu nước quỳ xuống trước mặt ông chủ, nằn nì:

- Ông chủ! Ông xem, đã hơn một năm nay tôi chưa được về qua nhà lấy một lần, bây giờ là lúc vợ tôi đang khó khăn và cần tôi nhất, ông hãy cho tôi về mấy hôm đi mà! - Trương Tam vừa nói, vừa nghĩ rằng, ông chủ hà khắc, khó khăn như vậy thì anh ta phải lập tức vào vai kịch, mặt mũi trở nên cay đắng, rầu rĩ, nước mắt, nước mũi dàn dụa.

Ông chủ im lặng hồi lâu. Trương Tam rất hồi hộp liếc nhìn ông ta. Có lẽ màn kịch của mình đã khiến ông chủ lòng sắt dạ đá mềm lòng rồi chăng? Vậy là có hy vọng rồi! Chẳng thể nào ngờ, sau mấy khắc im lặng, ông chủ lạnh lùng buông một câu:

- Thôi, anh không cần phải về với vợ nữa đâu!

Cái gì?! Nhìn bộ mặt lạnh băng của ông chủ, Trương Tam cố nén tức giận nhưng hai bàn tay anh ta từ từ nắm chặt lại, ước gì được nhảy lên, giáng vào cái mũi đáng ghét của ông ta mấy quả đấm.

- Trương Tam ơi là Trương Tam! Đừng có trách rằng tôi không nhắc nhở anh nhé - Ông chủ nhìn Trương Tam ôn tồn nói - Hay thật, hơn một năm qua anh chưa về qua nhà, vậy mà bây giờ vợ anh lại sắp sinh con, chẳng lẽ anh không nhận thấy lý do xin nghỉ phép của anh lạ lùng lắm sao?
1. Chiếc xe máy rẽ phải rồi kéo ga tăng tốc độ khi vừa qua một ngã tư, bất thần ô tô trong hẻm lao ra. Chàng trai ngã ngay trước mũi xe, lồm cồm bò dậy, nhưng cô gái ngồi phía sau và xe máy đã nằm gọn trong gầm xe ô tô. Một tình huống tai nạn cổ điển. Lúc ấy nắng Sài Gòn vẫn hanh hao như vốn dĩ nó thế, những chiếc hoa dầu xoay xoay trong gió quẩn lên một góc chiều.

Sáng hôm sau, ở trang An ninh trật tự ngày qua, mọi người vẫn đùa là trang Cướp, hiếp, giết, các báo đồng loạt đưa tin có một vụ tai nạn thương tâm với đôi bạn trẻ đang trên đường đi mời đám cưới. Chú rể tương lai không bị chấn thương gì, cô dâu tương lai bất tỉnh ngay tại chỗ.

Cách vị trí tai nạn không xa, trong bệnh viện, bác sĩ kết luận chấn thương của nạn nhân không ảnh hưởng tới não trạng. Tuy nhiên, nguyên chân trái của cô gái xem như chỉ để… làm cảnh, vì toàn bộ cẳng chân đã bị xe nghiền nát, và không thể phục hồi lại được.

2. Thành phố mưa. Đợt mưa kéo dài phải đến cả chục ngày. Sáng ra trời vẫn nắng hồng lên, vàng ươm nhưng ngã sang chiều thì bắt đầu dịu lại, và cuối chiều tới nửa đêm là mưa. Cái sự lặp lại ấy tuần tự cứ như tiếng chuông nhà thờ tới giờ là điểm. Bởi mưa nên quán Tiếng Xưa thưa khách. Và thường những hôm mưa như vậy, nếu có khách người đàn bà chủ quán cũng cáo lỗi, rồi ngồi gặm nhấm thời gian bên cây guitar với duy nhất bản Khúc thụy du.

Vẹn nguyên là gì? Hơn một lần người đàn bà ấy tự hỏi. Hỏi rồi lại cười, ở đời có thứ nào được xem là vẹn nguyên? Vẹn nguyên là đang chết. Mà chính cái sự đang chết vẫn mang trong nó sự đổi thay. Phải bào mòn, hư hao tới kiệt quệ mới chết được. Thế giới sống vận động không ngừng. Lớp lớp thời gian đè lên nhau, lấn át nhau, phủ lấp nhau. Nhưng sao cảm thấy ngày xưa cứ vẹn nguyên đi về. Liệu có phải mỗi khắc đã qua đều có lí lẽ riêng để tồn tại, mãi mãi hay sẽ bị phủ định vào một lúc nào đó? Vậy cái lí lẽ để tồn tại trong người đàn bà ấy là gì?

Mười lăm năm, thời gian đủ để chưng cất, luyện những ngày ấy thành một viên linh đơn hoàn tán bé xíu xiu có thể nuốt trôi đi và không còn vương lại dấu tích gì nữa. Số báo ra ngày ấy, có lẽ chẳng ai buồn giữ, để thi thoảng lưu tâm, nhắc lại, nhất là điều ấy chỉ là một cái tin bé tí ti về tai nạn giao thông, vốn được thế giới xem là “đặc sản” của đất nước miền nhiệt đới này. Mấy chục vạn số báo chắc đã được các bà đồng nát mua về, bán lại, rồi nhà máy nghiền nát hóa kiếp để làm lại giấy tới vài trăm lần, không thì cũng gói bánh mì rồi tan trong vài thớ đất. Duy chỉ người đàn bà ấy vẫn giữ. Giữ chẳng phải để nâng niu, níu kéo gì, thậm chí là chỉ cho lòng nặng hơn. Nhưng chẳng thể quăng đi, hay đúng hơn, người đàn bà ấy không đủ dũng khí để bỏ. 

3. Người đàn bà ngày ấy là cô ca sĩ Lenka, Lê Lenka, chuyên hát tình ca ở phòng trà Đêm mơ. Chỉ riêng tên Lenka, cái tên Đức được bố mẹ dành cho để nhớ về tình yêu sôi nổi một thời bên Đông Đức, của cô ca sĩ đã gây sự chú ý với người nghe trong mớ những cái tên thời thượng gắn với hương với hoa thời ấy như những Hồng, những Phượng, những Hương…, cộng thêm chất giọng trầm trầm khàn khàn đục đục, như rút ruột ra mà ca đã làm say lòng bao người nghe nhạc khó tính.

Trong số những người khó tính ấy có Lê, chàng kiến trúc sư tây học mới về nước. Lê ngợp ngay lần đầu tiên Lenka bước chân lên sân khấu. Đầu tiên là chất giọng, sau đó là ánh mắt. Cái ánh mắt không phải đen mà là nâu, màu nâu của loài sóc, cứ ánh lên và thăm thẳm hút mọi thứ đối diện vào như chính ánh mắt ấy được trời phú riêng cho một thứ từ trường đầy hấp lực. Để từ ánh mắt tới trái tim. Từ trái tim tới… ngôi nhà và những đứa trẻ. Tình yêu tới. Cuồng nhiệt như những cơn gió chiều chiều bên bến Bạch Đằng. Cứ tưởng mọi thứ đã ở trong tầm tay. Hai người trẻ tung tăng đùa giỡn, lúc nào thích chỉ việc nhón tay lên hái trái… hạnh phúc. 

Nhưng chính khi hạnh phúc ngỡ rất gần lại hóa chỉ là mơ. Chiếc xe ô tô chiều ấy đã lấy đi một đám cưới đã lên sẵn kịch bản. Và khi Lenka rời viện trên xe lăn cũng là ngày người chồng sắp cưới lên tàu vượt biên tới “thiên đường nước Mĩ”, không một lời từ biệt.

Lenka vỡ mộng tới hai lần.



4. Ngày người đàn bà ấy quyết định mở quán Tiếng Xưa cả nhà và bạn bè thân quen đều phản đối. “Người ta ba đầu sáu tay, học kinh tế ngoại thương kinh doanh còn chẳng ăn ai, huống hồ một con ca sĩ ngồi xe lăn”. Nhiều người khuyên can nhẹ lời mãi không được đã xổ toẹt ra như thế. Nhưng Lenka phải làm gì đó để quên đi. Đồng tiền nhiều khi chỉ là nguyên cớ chứ chẳng phải nguyên nhân để con người phải sống chết với nó.

Và Tiếng Xưa ra đời.

5. Tiếng Xưa. Nghe cứ ngỡ tên một quán cà phê hay karaoke nào đó. Nhưng không, Tiếng Xưa là… quán nhậu. Có lẽ lục tung cả xứ sở này cũng không tìm đâu ra tên một quán nhậu nào đại loại như thế. Quán nhậu thời này phải là những tên kiểu đập ngay vào vùng men trên não trạng những tay nhậu như “A, đây rồi, thịt cầy” hay “Vitamin gâu gâu”, rồi “Hải sản còn bơi, còn trườn, còn cựa”, “Đặc sản rừng còn gáy, còn chạy, còn hú”….

Khách tới Tiếng Xưa một phần để nhậu, nhưng quan trọng hơn là để được nghe lại giọng ca xưa. Người đàn bà ấy có thể hát say sưa cho các cuộc vui bằng những tình khúc Trịnh Công Sơn, Vũ Thành An, Ngô Thụy Miên… bất tận từ sớm tới khuya. Bởi vậy, khách tới đây là những khách quen, và toàn những vị khách thích nhậu bằng miệng thì ít mà bằng tai thì nhiều.

Trong những vị khách quen, tri thức có, văn nhân có, doanh nhân có…, đã nhiều người động lòng cảm mến cô chủ. Họ thật lòng khâm phục và muốn sẻ chia. Tất cả, người đàn bà ấy đều trân trọng, và xin được chối từ. Chẳng phải người đàn bà ấy mặc cảm. Chẳng phải người đàn bà ấy sợ nhận nhầm phải lòng thương hại. Khi tới một độ tuổi nào đó người ta đủ nhạy cảm để biết đâu là thật là giả, huống hồ lại là người đàn bà nội tâm ấy. Người đàn bà lắc đầu, chỉ bởi, trong người đàn bà ấy, tình yêu chỉ có một.

Vẫn biết, khi tình yêu gieo trên một mảnh đất xấu, người ta có quyền bấng nó lên và trồng vào một mảnh đất tốt hơn, nơi nó xứng đáng được quyền sống và hưởng thụ đầy đủ dinh dưỡng.

Nhưng hạnh phúc đôi khi chỉ là được sống hết mình với những gì mình nghĩ, dẫu điều mình nghĩ có là quá khứ, hiện tại hay tương lai. Mỗi con người có thế giới cảm quan riêng. Ai bằng lòng với thế giới ấy của mình thì có thể gọi lên tên hạnh phúc. Người đàn bà ấy bằng lòng với tình yêu đã xanh rêu và vương lại trên những khúc tình ca.

6. Rất nhiều người bảo lạ, không hiểu sao càng có tuổi người đàn bà ấy hát càng nồng nàn và đau đáu hơn. Đôi mắt ấy, nhắm nghiền lại, phiêu bồng; mi mắt ấy, lay lay nhẹ, da diết; ánh mắt nâu và sâu, không đáy, chênh chao. Nó như hút những bản tình ca vào.

“Hãy nói về cuộc đời/ khi tôi không còn nữa/ sẽ lấy được những gì/ ở bên kia thế giới/ ngoài trống vắng mà thôi/ Thụy ơi và tình ơi…”. Chiều nay người đàn bà ấy vẫn ngồi đó. Trên bậu cửa sổ những chùm tường vi khẽ đung đưa, hồng lên nhạt nhòa trong mưa. Phố bắt đầu lên đèn. Tiếng xe vẫn còn nhộn nhịp lắm. Không biết có ai chạy xe mà nghe vẳng theo tiếng guitar hòa trong lời bản Khúc thụy du ấy không?
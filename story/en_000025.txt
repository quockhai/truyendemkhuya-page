- Này, cẩn thận không đêm mai trộm sẽ viếng nhà đấy - Giọng cô vợ vừa ly hôn cảnh báo gấp gáp qua điện thoại.
Về chuyện này thì tôi hoàn toàn tin tưởng, bởi quá tỏ tường bản tính của cô ta. Một người không bao giờ biết nói dối. Thậm chí nàng bỏ tôi bằng lời tuyên bố thẳng thừng: "Do anh quá nghèo!". Dù sao tôi vẫn còn yêu nàng, nên cuối tuần rồi tôi gọi điện bí mật thông báo để nàng nghĩ lại, rằng tôi đang sở hữu cỗ vi tính xách tay loại xịn cùng mười nghìn Mỹ kim tiền mặt trong két. Tuy tỏ vẻ không tin, nhưng hẳn cô ta đã kể lại cho chàng nhân tình nên mới ra cơ sự này. Nghe đâu hắn là một tay sống phong lưu bằng nghề đột vòm chuyên nghiệp.
Hắn lẻn vào sau khi đã cậy lớp gíp trát cửa sổ bếp, gỡ kính ra mà không có bất cứ tiếng động nhỏ nào. Hắn thận trọng kiểm tra xem tôi có lắp chuông báo động điện tử không. Tiền đâu ra mà phung phí vậy, tôi chợt nghĩ khi thấy bóng hắn vọt qua ô cửa trống trơn. Sơ đồ căn hộ tôi ngụ chắc hắn đã rõ như lòng bàn tay. Cô vợ cũ thể nào chẳng tham mưu đắc lực cho chàng bồ mới.
Tôi quyết định không làm hắn bất ngờ. Dại gì ra tay lúc này, dễ mất mạng như chơi. Tôi kiên nhẫn nán dưới gầm giường dõi theo mọi cử chỉ của hắn. Dường như hắn đang cân nhắc xem nên nẫng những gì đáng lấy nhất… rồi xộc thẳng tới bàn làm việc kê cuối phòng khách. Do vội nhấc của báu, hắn đã gạt luôn đống giấy bút tôi cố ý xếp ngổn ngang gài bẫy. Tôi chờ đợi một câu chửi thề nhưng chẳng thấy đâu. Đúng là một kẻ chuyên nghiệp, biết giữ lưỡi trong miệng lúc cần. Lát sau lại có tiếng va đập, lần này là tiếng kim loại. Chắc đồ nghề hay chìa khóa gì đó… À đúng rồi! Tiếng khóa mở két sắt. Chiếc két tôi giấu tuốt sau chiếc giường nôi trong phòng trẻ em mà đương sự vẫn tìm ra, tài thật. Thêm một điểm cho hắn.
Khi thủ phạm đã khuất dạng sau ô cửa bếp, tôi liền gọi điện cấp báo cho cảnh sát. Lúc này trời vừa tảng sáng. Họ xuất hiện ngay. Tôi linh cảm rằng dường như những người đại diện cho công lực sẵn lòng quy kết tôi tội… đồng phạm. Tôi cố trình bày rằng không thể nhúc nhích lúc tên trộm đêm đang hành sự, bởi dù sao tôi vẫn quý mạng sống của mình hơn.
- Sao ngài không lắp hệ thống báo có người lạ đột nhập? - Viên trung úy hình sự gay gắt hỏi.
- Lương viên chức quèn như tôi làm sao kham nổi điều xa xỉ ấy?
Viên cảnh sát thứ hai tiến lại sau khi đã quan sát kỹ hiện trường:
- Một kẻ chuyên nghiệp thực thụ. Duy nhất chỉ có đống giấy bút vương vãi dưới sàn là bằng chứng cho cuộc đột nhập.
- Thế còn cái két - Trung úy hỏi.
- Hình như hắn dùng chìa khóa vạn năng thì phải.
- Có mất gì không?
- Chỉ chủ nhân mới biết rõ được.
- Ngài có thể khai báo để chúng tôi lập biên bản - Viên trung úy nói - Ngài bị mất những gì?
- Trước hết là chiếc máy vi tính xách tay hiệu HP - Tôi bắt đầu liệt kê - Kế đến là tiền trong két.
- Bao nhiêu?
- Mười nghìn đô la - Tôi đáp.
Viên cảnh sát thứ hai há hốc mồm.
- Sao ban nãy vừa kêu là không có tiền? Xem ra ngài nên về đồn trao đổi thêm về chuyện này mới được.
- Tôi sẵn lòng đến đó ngay bây giờ - Tôi nói giọng hồ hởi - Bởi tôi muốn giải thích ngọn ngành mọi chuyện.
                             *
Một chiếc máy thu phát mini được gắn cực khéo trong ngăn chứa pin của cỗ laptop, cùng công suất đủ mạnh bao trùm cả Luân Đôn lẫn vùng phụ cận. Bạn bè trong hãng điện tử đã thiết kế riêng theo yêu cầu của tôi. Khi mở vi tính, đồng thời tín hiệu cũng được khuếch đại. Tôi thông báo tần số cho cảnh sát và họ đã nhanh chóng lần ra nơi đặt máy. Thật khó khăn khi cố thuyết phục đội tuần tra cho đi cùng.
Sau tiếng chuông gọi cổng là sự xuất hiện của một người đàn ông trung niên có mái tóc vàng cắt kiểu đầu đinh, cùng cơ thể cường tráng mang dáng dấp vận động viên thể hình. Cỗ xe công vụ chuyên dụng gắn còi hụ đậu trước mặt chẳng làm đương sự bối rối. Hắn tựa lưng vào bậu cửa và hỏi vọng ra:
- Các ngài cần gì?
- Chúng tôi được lệnh khám xét toàn bộ nơi đây.
Kẻ đầu đinh lưỡng lự giây lát, rồi dấn bước ra mở rộng cổng cho đội tuần tra tiến vào.
Theo chân đội truy lùng khắp các phòng cả trên lầu lẫn dưới nhà mà chẳng có kết quả (lúc này máy của hắn đã tắt sóng), kế tiếp trong tầng ngầm và ga-ra để xe cũng vậy. Tôi để ý chủ nhân ngồi trong bếp đăm chiêu nhìn qua cửa sổ, trước mặt là ly cà phê bốc khói cùng chiếc chìa khóa xe dài thượt vứt lăn lóc bên cạnh… Tôi vội kéo trưởng nhóm lại thầm thì:
- Thưa trung úy… - Tôi chỉ ra phía ngoài nơi tọa lạc cỗ xe thể thao đời mới.
- Xin vui lòng mở cốp xe có được không? - Trưởng nhóm thò qua cửa gian bếp đề nghị.
Kẻ tóc vàng ngoái đầu lại… Sau đó hắn chậm rãi tiến ra mở khoang hành lý vẻ đầy miễn cưỡng.
Trong cốp xe ngoài chiếc biển báo hình tam giác nằm vắt lên túi đồ nghề, vài chai nước suối cùng hộp thuốc dự phòng, là chiếc túi thể thao nhiều tầng. Một cảnh sát viên thò tay sâu vào túi, lôi từ dưới đáy ra cái bao nilon to màu đen, bên trong chính là chiếc máy tính xách tay quen thuộc. So sánh với biên bản đã biết thì hoàn toàn trùng lặp, cả về thương hiệu, đời máy, cũng như số thứ tự của nhà sản xuất. Thêm một điểm cho tôi.
- Ngài lấy đâu ra thứ này? - Trưởng nhóm hỏi.
- Của một người quen đấy - Đương sự thản nhiên đáp - Nó không làm việc nên anh ta nhờ tôi sửa. Có vấn đề gì không?
- Đây là đồ ăn cắp.
- Như vừa nói thì đâu phải của tôi. Thậm chí tôi cũng chưa có thời gian mở nó ra xem sao nữa.
Tôi đinh ninh rằng hắn đã cẩn thận xóa sạch dấu vân tay còn sót lại. Khó có bằng chứng hòng buộc tội hắn, ngoại trừ phải tìm cho ra khoản tiền kia…
- Chúng tôi bắt quả tang đồ chôm chỉa… - Trung úy tiếp tục truy xét.
- Nếu ngài nói ngay từ đầu, rằng cần tìm chiếc vi tính xách tay thì tôi sẵn sàng giao nộp không chút câu nệ.
- Thêm điều này nữa, yêu cầu mở ngay cái két gắn sau tấm gương trong phòng ngủ để chúng tôi tiến hành kiểm tra - Trung úy truy tiếp mà không cần để ý tới thái độ của đương sự.
Khi hắn loay hoay lần theo mã số, hai cảnh sát viên áp sát bên cạnh.
- Được rồi, giờ hãy lùi ra - Một trung sĩ trong nhóm ra lệnh - Chúng tôi sẽ tự làm lấy.
Chỉ có chiếc túi đựng hồ sơ và vài cái bao thư. Bên trong các phong bì chứa những xấp ngoại tệ mệnh giá một trăm USD còn mới.
- Hãy kiểm tra cẩn thận những tờ giấy bạc này - Trưởng nhóm lệnh cho trung sĩ đem mớ bao thư đổ ra giữa chiếc giường kề đấy.
- Vậy những thứ của nợ này có phải của ngài không? - Trung úy quay qua đương  sự nghiêm giọng chất vấn.
- Tôi đâu có tội khi tàng trữ chúng, phải không? - Kẻ tóc vàng vẫn chưa chịu khuất phục - Tôi có quyền cất tiền riêng của mình, cho dù đó là ngoại tệ.
- Nhưng nếu là tiền giả, ai tàng trữ nó ắt đều bị truy tố - Tôi khó chịu lên tiếng.
Lại thêm một điểm cho tôi. Còn kẻ tội phạm chuyên nghiệp vẫn cố vớt vát:
- Hiển nhiên tôi sẽ bị mất số vốn lớn, nếu đấy là bạc giả. Dù sao tôi cũng xác định được xuất xứ của chúng - Hắn ranh mãnh lấp liếm - Các ngài sẽ bắt được thủ phạm để lôi ra tòa, có đúng vậy không?
- Ngài sẽ chỉ khoản hiện kim có được ấy xuất hiện từ đâu chứ gì? - Trưởng nhóm vặn lại.
- Chính xác hơn là tôi mua chúng từ quầy đổi tiền nào…
- Sao mà chứng minh được? Ngài có ghi số xê-ri cụ thể không?
- Cần gì chuyện ấy - Kẻ đầu đinh phản ứng - Chắc ở chỗ các ngài có đầy đủ mọi thông số cả ư?
Trung úy ném ánh mắt về phía tôi, rồi quay sang cười phá lên với kẻ đang bị tình nghi:
- Đúng, chúng tôi có đủ nhằm thỏa mãn sự thật.
Chủ nhân cân nhắc giây lát, trước khi tiếp tục ngoan cố chống chế:
- Chuyện kỳ lạ? Nếu như các ngài nắm được số xê-ri bạc giả, sao chẳng công khai ra để dân chúng khỏi lầm, cũng như giúp thị trường tiền tệ đỡ rối ren…
- Dẹp ngay cái mớ giáo lý hổ lốn nhức đầu ấy đi - Trung úy nghiêm sắc mặt - Nếu ngài đủ minh mẫn hẳn biết rõ tội của mình. Tàng trữ và tiêu thụ tiền giả luôn được xếp vào dạng trọng tội đấy.
Kẻ tóc vàng vẫn cố vớt vát:
- Giả sử đó là đồ chôm chỉa và tôi sẽ chỉ cho các ngài chỗ đánh cắp… Ai sẽ chịu trách nhiệm về số tiền ấy?
- Chưa biết được - Trưởng nhóm truy lùng tang vật phẩy tay - Nhưng đến lúc này chúng tôi có thể khẳng định, thủ phạm vụ trộm đích thị là ngài. Nhiệm vụ đến đây kết thúc, mọi chuyện còn lại sẽ do cơ quan tố tụng giải quyết.
- Sao ngài có thể kết luận một cách hàm hồ thế? - Kẻ đầu đinh tỏ vẻ ngạc nhiên khi thấy chiếc còng số tám bằng inox sáng loáng đang chìa ra từ tay viên trung sĩ - Thì ra có sự giăng bẫy sẵn?
Hắn làm sao biết được, rằng số tiền mà tôi cố trưng ra với cô vợ cũ chỉ là những tờ giấy được in từ máy photocopy ba chiều với một xê-ri duy nhất; cũng như chiếc laptop rặt đồ… cũ có thể bỏ đi.
- Muốn nghĩ sao cũng được - Trung úy độp lại.
Khi nhân viên công lực áp giải tên đầu đinh ra khỏi phòng ngủ, hắn trừng trừng nhìn tôi vẻ thách thức, như muốn thẩm định lại kẻ đối đầu mà hắn đã vô tình coi thường…

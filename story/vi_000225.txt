Đêm đã vào sâu.

Vườn cây xào xạc lá. Ngoài cửa sổ, trăng cuối tháng lúc đổ tràn ánh bạc, dát lên những tán lâu
niên; lúc mây trôi dày đặc gói mảnh trăng cong lưỡi liềm dấu biệt vào âm âm u u. Người ta bảo:
mỗi con người là một tiểu vũ trụ có mối liên hệ vô hình với vũ trụ mênh mông vô tận. Mặt trăng
rất ảnh hưởng đến cảm xúc, tình cảm con người. Đêm nay, có thể đang động trời, Kiên thao
thức, không tài nào ngủ được. Nhưng, nỗi buồn, da diết thì không phải do mảnh trăng hạ huyền
bị mây che lúc tối lúc tỏ kia. Nó bắt đầu từ ký ức trào lên bất chợt ùa về lúc cuối ngày, nó đeo
bám dai dẳng, ám ảnh Kiên không dứt.

 
  Minh họa: Lê Trí Dũng
Giật mình! Ngỡ ngàng. Kiên không tự chủ được, lòng xao xuyến, khi người cuối cùng bước vào
trả lời phỏng vấn là cô gái người Khơ Me nói tiếng Việt khá sõi. Váy bò te tua, bắp chân thon
chắc màu bánh mật, tóc xoăn đen, mắt bồ câu lóng lánh. Hình như, Kiên đã bắt gặp cô gái ở
đâu đó rồi?! Anh cố moi trong ký ức mù mờ, xa lắc một hình ảnh quen thân nào đó mà vẫn tịt mít
không ra.

Kiên hỏi:

“Tên bạn là Sa Von?”

“Vâng! Tên em là Sa Von. Thưa ông!”

“Bạn đang làm việc ở công ty viễn thông AIS - Thái Lan, lương cao, gần nhà. Tại sao bạn lại thi
tuyển vào Công ty Viettel Cambodia do Viettel đầu tư ở Campuchia?”

Cô gái Khơ Me chớp chớp đôi mi dầy, cong. Đôi mắt bồ câu ướt long lanh, miệng tươi như hoa:

“Thực ra em không phàn nàn gì khi làm việc ở công ty cũ. Nhưng, công việc em đang làm được
ví như cái áo quá chật; và em không muốn da thịt mình lớn căng phá bung các đường chỉ may.
Em muốn được mặc cái áo rộng hơn ở Viettel Cambodia.”

Kiên ngạc nhiên thực sự:

“Ồ! Thì ra là như vậy. Sa Von, bạn có hiểu gì về cái nơi mình sẽ đến làm việc không? Ví dụ như
triết lý thương hiệu của Viettel chúng tôi là: “Caring Innovator?”

Bặm môi một lát, cặp môi đỏ hơi dầy và gợi cảm, cô gái trả lời:

“Thưa ông! Trong tiếng Anh: Caring là tính từ, có nghĩa là QUAN TÂM, CHU ĐÁO; còn Innovator
là danh từ, có nghĩa là NGƯỜI SÁNG TẠO; NGƯỜI ĐỔI MỚI. Em nghĩ: Người Viettel các ông
chọn triết lý này vừa kết hợp được hai nền văn hóa phương Đông và phương Tây: (Caring) vừa
hướng nội quan tâm, chăm sóc, dịu dàng, chu đáo; (Innovator) vừa sáng tạo, đi trước mở lối,
đột phá…, mang hơi thở của khoa học kỹ thuật hiện đại, hướng đến nhân văn và tôn trọng con
người.”

“…”

Tự tin. Chững chạc. Trả lời rất khác biệt theo cách của riêng mình, lưu loát, khúc chiết và hàm
súc, Sa Von mang phong cách trẻ trung, có cái gì đó hơi nổi loạn phá phách của bọn trẻ Tây Âu.
Nhưng, Kiên “đọc” được con người này đầy ắp sự chân thành nồng nhiệt của người Khơ Me
kết hợp với cái nhanh nhẹn, quyết liệt với lối sống hiện đại có thể được rèn rũa trong thời gian
làm việc ở Công ty viễn thông AIS - Thái Lan. Hình thể: đứng ở tốp đầu. Chuyên môn: Giỏi. Kinh
nghiệm chuyên môn: Ba năm. Phỏng vấn: Xuất sắc. Vượt qua tất cả những thí sinh khác, nhất là
phần thi phỏng vấn, cô gái Khơ Me sõi tiếng Việt, mắt bồ câu trúng tuyển đầu sổ, không hề có sự
ưu tiên, cảm tình riêng nào…

* 
*          *

Vẫn câu hỏi: mình đã gặp cô gái ấy ở đâu đó rồi? Và Kiên không trả lời được. Khỉ thật! Hơn bốn
mươi tuổi, độc thân; anh đang ở độ chín của đời người. Vài ba mùa trận mạc ở đất nước Ăng
Co, rồi về nước học hành đến nơi đến chốn, quân hàm thượng tá, kỹ sư; thông minh hơn người,
Kiên có lối tư duy lý trí hài hòa với duy tình trực quan sinh động. Không lẽ, cô gái Sa Von người
Khơ Me vừa gần gũi quen thuộc vừa xa xăm kia vẫn là một ẩn số gói kỹ trong đầu Kiên?

Áo quân phục khoác hờ vai, Kiên bước ra ngoài ban công. Anh nhận ra: Sông Tonle Sap vẫn
quặn lòng chảy xa xá và thở than như mấy chục năm về trước. Đã mất ngủ thì thao thức luôn
cùng sông nước đất trời Phnompenh cả đêm, Kiên tự nhủ, chẳng mấy khi trở lại cái nơi mình
từng gắn bó thời trận mạc đầy mồ hôi, nước mắt và cả máu nữa. Bên kia sông, nếu Kiên nhớ
không nhầm thì nó vốn là một dải đất bãi, bạt ngàn dây bí đỏ, quả lớn quả bé lúc nhúc như lợn
con. Trận địa cao xạ của Hanh - thằng bạn cùng làng đóng ở đó. Kiên đã vài lần chèo thuyền

vượt sông Tonle Sap sang chơi. Một buổi sáng, cả khẩu đội không thấy Hanh đâu, bổ nhau đi
tìm. Giời đất ơi! Hanh mặc độc cái quần lót, bị căng người ra. Một cọc tre tươi đóng vào miệng.
Bốn cọc nữa đóng vào hai bàn tay, hai bàn chân ghim chặt cứng xuống đất. Máu chảy ra đông
đặc, xám xịt. Kiến đen chi chít, râu ria ngoe ngẩy, bâu vào các đám máu khô. Quân dã man!
Người ta phỏng đoán, đêm Hanh đi tiểu, tàn quân Pôn Pốt mò vào tóm được anh, điểm huyệt và
lôi ra bãi bí ngô hành quyết kiểu man rợ thời trung cổ. Từ Campuchia về Trường sĩ quan Thông
tin nhập học, Kiên được nghỉ phép kết hợp thì người ta cũng đã báo tử Hanh trước đó vài năm.
Mẹ Hanh chạy cùn cụt đến nhà, cứ ôm lấy vai Kiên, khóc ời ời: “Kiên ơi! Khi đi thì có anh có em,
sao lúc về cháu không rủ em về với cô…” Rủ làm sao được nữa cô ơi. Kiên không dám kể cái
chết tang thương của người bạn chí cốt, anh nuốt nỗi đau vào lòng…

Cũng bên dòng Tonle Sap này, nơi thượng nguồn xa lắc, lính ta ở phía bắc mặt trận kể: Sau một
cuộc phục kích bất ngờ vào tổ lính thông tin, bọn tàn quân Pôn Pốt bắt được một người lính bị
bắn gãy chân. Lính cao xạ, lính thông tin phải cầm súng bộ binh đánh giặc có nghĩa là không
chuyên nghiệp, khốn nạn rồi. Thương quá. Lính ở phía Nam mặt trận thì lại kể: Bọn tàn quân
Pôn Pốt bắt được người lính bị thương gãy chân ấy. Chúng giật đứt đường dây hữu tuyến của ta
đang liên lạc và lấy cả dây còn nguyên trong cuộn, quấn từng vòng từng vòng dây đôi đen nhánh
chồng lên nhau kín hết thân thể người lính. Và đào lỗ… chôn sống. Tất nhiên, chúng không quên
gạt cả những người lính quân tình nguyện đã chết xuống cùng chung một hố… Ôi trời! Ký ức
một thời đầy máu và nước mắt nhức nhối, ám ảnh Kiên không thể nào quên được.

Có thể là ban đêm, Kiên không nhận ra cái vườn xoài và khu biệt thự có tường đá xanh rêu ẩm
ướt ở chỗ nào. Cả bến tắm nữa cũng biến mất vào thời gian hoang phế. Hai cái bến tắm có thể
lui về phía thượng nguồn gần 1 cây số; đứng ở đây, Kiên chỉ thấy nước sông chảy mênh mông.
Bọn Kiên đã từng chèo thuyền, thả lưới đánh cá, ngụp lặn, đùa giỡn với dòng sông. Và con sông
xưa ôm ấp vỗ về những thằng trai đánh giặc quanh năm khô hạn ở trong lòng.

Ngày ấy, cứ chiều chiều là bọn Kiên ra bến sông tắm. Cách nhau khoảng vài chục thước, bến
đàn ông ở phía thượng nguồn; bến con gái, đàn bà ở phía cuối nguồn. Phải rất lâu, sau này Kiên
mới nhận ra, phụ nữ Khơ Me tắm sông khác phụ nữ Thái vén váy tắm suối. Con gái Thái lội ra
sâu nước từ từ ngập thì cũng từ từ vén váy lên cho khỏi ướt; nước ngập mông thì vén lên ngực.
Lội ra sâu tiếp thì vén váy cao thêm, vén dần qua ngực; vú chìm trong nước thì cởi váy quấn lên
đầu. Rất kín đáo, dịu dàng, ý nhị và lãng mạn; dù đứng gần mấy thước cũng không nhìn thấy da
thịt tươi mởn của người con gái. Nước suối trong mát tha hồ mà đùa giỡn, mơn man, vỗ về da
thịt. Người Khơ Me gọi váy là xà rông; con gái tắm sông cứ để nguyên váy áo lội xuống nước.
Ngụp lặn. Bơi lội. Rồi thò tay vào váy áo kỳ cọ. Bến xa nhà, thì mang theo xà rông, áo tìm chỗ
khuất trên bờ thay đồ. Ở gần thì cứ thế lướt thướt, váy áo mỏng dán vào da thịt, nước chảy theo
bước chân về nhà mới thay.

Bọn Kiên khi ấy còn rất trẻ. Nhìn xuống bến tắm con gái, một thoáng lòng xao xuyến và ngượng
ngùng qua mau, cả lũ con trai hò hét vang mặt sông. Bến bên này trêu ghẹo bến bên kia; bến
bên kia hò đối lại bến bên này; có lúc còn ngụp xuống lấy bùn đất ném sang. Lúc đầu, hai bến
còn xa, trêu chọc nhau hóa thân mật, gần gũi; và do cả nước chảy người trôi nữa; hai bến nhập
một lúc nào chẳng biết. Không còn e thẹn, ngại ngùng, Kiên, Chương còn chìa lưng cho Sa Ly,
Dên và chị Rêu kì cọ. Bọn Kiên thi nhau bơi, ngụp lặn. Tuổi thơ Kiên bì bõm với con trâu sừng
cánh ná đen bóng dưới dòng nước, đi tát múc ở đồng sâu, thùng đấu nên các cuộc thi nào Kiên
cũng nhất. Có lần, Kiên lặn sâu xuống nước và chẳng lên. Mọi người hốt hoảng lội uồm uồm,
ngụp lặn tìm anh. Nhưng rồi, bất chợt Kiên ngóc đầu chuồi lên. Ngẫu nhiên, hoặc do định phận
sắp đặt để cái đầu Kiên vô tình chui vào giữa hai đùi Sa Ly và đội xà rông ngoi lên ướt sũng. Vạt
xà rông lập lờ trên mặt nước. Một trận cười vang mặt sông. Kiên cũng cười ngượng ngùng, cứ
vuốt mãi vuốt mãi gương mặt đẫm nước. Sa Ly e thẹn, cúi đầu, vốc nước vã lên gương mặt đỏ
rần rần…

 
 Ảnh: HTO 
Ôi trời! Cái bến nước ngày xưa chỉ còn trong ký ức mù xa. Nhưng, nó không hề mất đi, cứ lấp ló
ẩn hiện trong những buổi chiều hoàng hôn cháy đỏ nơi quê nhà, và những đêm thao thức không

ngủ ở Hà Nội. Kiên chẳng ngờ cuộc đời như một vòng tròn, đi loanh quanh mỏi mệt rồi cũng có
lúc quay về bến bờ xưa cũ xa lăng lắc. Bây giờ, Kiên đã trở về bên dòng Tonle Sap.

Gió đầy hơi nước. Mát rượi. Sóng vỗ ì oặp. Kiên nhận ra phía bờ bên kia, lui lên mạn bắc vài cây
số là Hoàng cung. Vẫn ánh điện hắt xuống dòng sông, nhưng ánh sáng trắng rực rỡ, chứ không
yếu ớt, đổ xuống mặt nước vô vàn ánh lân tinh xanh đỏ như ngày xưa nữa. Cây xanh trên bờ ít
lắm và nhà cao tầng chen nhau mọc lô nhô. Cái hoang sơ, trữ tình bên bờ sông Tonle Sap ngày
xưa ấy chỉ còn trong dĩ vãng.

Nhanh quá! Thế mà đã hơn hai mươi năm. 

* 
*          *

Thời gian công tác của Kiên ở Viettel Cambodia sắp hết thì xảy ra câu chuyện khá nan giải: Có
một Trạm BTS phát sóng di động, theo tính toán của bộ phận thiết kế thì phải đặt ngay trong
vườn nhà Sa Von. Bà mẹ Sa Von kiên quyết không chấp nhận. Đội trưởng kỹ thuật năn nỉ Sa
Von giúp, về thuyết phục mẹ đồng ý. Cô lý sự: việc nào ra việc ấy, không vì cô đang làm ở Viettel
Cambodia mà nhất nhất phải đặt cái cột ấy trong vườn nhà cô, dù mỗi tháng sẽ được trả đều
đều một món tiền không nhỏ. Mẹ cô là một tiến sĩ văn học dân gian Khơ Me chứ không phải một
người dân bình thường; bà không đồng ý, chắc chắn có lý do riêng xác đáng. Cô tôn trọng mẹ cô
như tôn trọng một cá thể trong xã hội.

Kiên nghĩ: Cô bé bướng bỉnh này sắc sảo và bao giờ cũng chủ động kết thúc mọi việc. Cần phải
biết lắng nghe. Và, phải có một chuyện gì đấy nếu không rất hệ trọng thì cũng có hại. Ví dụ như
đặt Trạm BTS ở đó sẽ làm xấu không gian khuôn viên? Hoặc một lý do nào đó thuộc về đời sống
riêng tư? Kiên tự nhủ: Mình sẽ đến đó xem sao?! Cần phải tìm hiểu, cần phải lắng nghe và tôn
trọng những cá thể riêng biệt; đó là triết lý sống của Kiên.

* 
*          *

Mùa xuân năm ấy.

Bọn Kiên đóng quân ngay bên bờ sông Tonle Sap, cách cầu Monivong hơn một cây số. Bên kia
vườn cây là khu nhà ở của tổ phiên dịch. Mười cô gái, chàng trai người Khơ Me biết tiếng Việt,
ngoài chị Rêu khoảng ba mươi tuổi còn lại sàn sàn mười tám, mười chín, hai mươi. Chị Rêu khá
sành tiếng Anh và tiếng Pháp. Chị đang học năm cuối khoa tiếng Anh thì giải phóng Phnompenh,
bọn Pôn Pốt từ rừng về thành phố, trường đại học bị đóng cửa. Bên này vườn cây là khu nhà
ở của tổ công tác dân vận thuộc sư đoàn bộ. Kiên và mấy chàng chuẩn úy ở chung một nhà.
Còn mấy anh nữa lớn tuổi hơn ở một nhà khác. Gọi là nhà nhưng thực ra là các biệt thự làm
theo kiểu nhà sàn; cột, ván làm bằng gỗ pơ mu. Nhìn kiến trúc, cây cối trong vườn có thể đoán
được các chủ nhân của chúng là những người khá giả, sang trọng. Lúc bọn Kiên vào tiếp quản
Phnompenh; các nhà đều vắng chủ, im ỉm cửa đóng, có nhà cửa giật bung ra, nhưng điều hoà
nhiệt độ vẫn chạy ro ro. Có thể họ đã chạy đi nước ngoài hoặc bị bọn lính Pôn Pốt xua đuổi đang
phiêu dạt ở một phương trời nào đó.

Công việc sau ngày giải phóng Phnompenh bận rộn và vất vả. Bọn Kiên hàng ngày phải chia
nhau về các phum sóc ở vùng ven đô giúp chính quyền địa phương của bạn ổn định đời sống
nhân dân. Thủ đô Phnompenh nhỏ, hoang tàn như thành phố chết. Đi khỏi nơi đóng quân độ một
giờ đã gặp rừng: rừng thốt nốt, rừng cây tạp um tùm và suối nước chảy trong veo.

Ngày nào, Kiên cũng gặp từng đoàn người lớn, trẻ con lũ lượt trên đường tìm về quê cũ. Người
ở Battambang về, người ở Xi Ha Núc Vin lên, người ở Kompongcham xuống... Cả đất nước
Campuchia đang lầm lụi làm cuộc hành hương khổng lồ. Lúc đầu, dân các phum còn ít, sau
người về cứ đông dần. Bà con giúp nhau dựng, sửa lại nhà, làm vệ sinh phum, sóc; bầu phum
trưởng... Mấy ngày đầu về tổ phiên dịch, Sa Ly, chị Rêu, Dên... còn là các cô gái gầy yếu, xanh
xao, gương mặt đờ đẫn, ngơ ngác thì sau một thời gian ngắn da dẻ đã căng mịn, mặt mũi tươi
tỉnh và nụ cười luôn ở trên môi. Đúng là cả dân tộc Campuchia hồi sinh, các cô gái cũng tươi tắn
có hồn. Sa Ly cũng vậy, càng ngày cô càng đẹp ra. Mẹ Sa Ly là người Việt, bố người Khơ Me.
Cô mang gene trội, được thừa hưởng những gì tốt đẹp nhất của dòng máu hai dân tộc nên cái
đẹp vừa gần gũi vừa xa lạ. Vẻ đẹp âm thầm ấy cứ dần dần, ngấm ngầm chinh phục Kiên.

Chương cùng lứa Kiên được phong vượt cấp từ trung sĩ lên chuẩn úy ngay ở chiến trường và họ
khá thân thiết với nhau. Tóc xoăn tít, lông mày rậm, nước da như đồng hun, mình Chương chắc
lẳn như con cá trắm đen. Về sau, Kiên mới biết mẫu con trai lý tưởng như Chương là niềm ao
ước, thèm muốn của không ít con gái Khơ Me. Chương ở đại đội thông tin trực thuộc trung đoàn,
được điều về tổ công tác dân vận của sư đoàn bộ sau Kiên vài tuần. Có lẽ, câu chuyện nỗi buồn
phiền, day dứt và ám ảnh Kiên bắt đầu từ khi Chương có mặt ở đây.

Đêm Lăm vông. Kiên đã ngây ngất nhìn Sa Ly cùng các cô gái múa, hát Chô thây nam thơ mây.
Thân hình thon thả uốn, xoay, uyển chuyển như lướt nhẹ trên mặt đất. Các ngón tay Sa Ly như
búp măng trắng hồng cong cong theo điệu múa. Hình như Chương cũng có cảm tình với Sa Ly,
mắt rực cháy có đốm lửa. Ngắm Sa Ly múa, Chương mê quá thốt lên: “Trời đất! Như vũ nữ trên
tường đá Angkor”.

Dạo đó kỷ luật dân vận rất nghiêm, quân tình nguyện không được phép lấy bất cứ cái gì dù nhỏ
nhất như cái kim sợi chỉ của dân. Toàn bộ lương thực, thực phẩm của quân tình nguyện Việt
Nam đều chở từ bên nước sang. Tổ công tác dân vận xuống phum phải mang theo lương khô
hoặc cơm nắm để ăn trưa. Tối về, đi qua vườn cây, chân bước đạp lên quả xoài chín nẫu vừa
bị gió đập rụng, tay va vào quả vú sữa chín mọng, nhưng vẫn không ai dám tơ hào một miếng.
Đêm, nằm ngủ cành xoài loà xoà, thò cả quả chín mũm mĩm vào cửa sổ cũng chỉ nuốt nước
miếng chống cơn thèm, quyết không vin cành bứt quả.

 
  Ảnh: HTO
Chuyện cái kim sợi chỉ nhỏ nhoi đã phải giữ gìn nghiêm khắc như vậy huống hồ là chuyện yêu
đương với con gái nước bạn. Kiên biết mình đang vướng vào một chuyện động trời, nhưng trái
tim có nhịp đập riêng của nó. Càng cố kìm nén tình cảm, Kiên càng khát khao yêu mãnh liệt, âm
thầm. Nếu có thể quên được Sa Ly thì Kiên đã đỡ phải xốn xang, thẫn thờ. Những tối không họp
hành, Kiên đều tranh thủ sang nhờ Sa Ly dạy thêm tiếng Khơ Me, một phần kiếm cớ để gặp cô.
Có hôm sang đã thấy Chương đến trước ngồi ở đó rồi. Lúc ra về, Sa Ly tiễn Kiên xuống chân
cầu thang là quay trở lên. Nhưng, cô lại thường xuyên tiễn Chương đi thêm một đoạn đến chỗ
cây vú sữa rồi mới quay lại.

Chương sống chân tình, bộc trực, hồn nhiên. Kiên kín đáo, giấu mình. Cái kim bọc lâu ngày cũng
chui ra khỏi vải. Hóa ra, Chương và Sa Ly thân thiết từ lúc Chương còn đang ở đại đội thông tin.
Chương đưa quân đi rải dây hữu tuyến qua bến tắm và họ đã quen nhau. Lúc đầu định thả dây
ngầm xuống tận đáy, vượt sông, Chương và đồng đội đã bơi và khảo sát nhiều lần. Nước sâu
quá, lại chảy xiết nên phải chấp nhận phương án hình chữ U, tốn dây đi vòng xuống qua cầu
Monivong rồi dẫn ngược lên. Chương đã nhiều lần ngồi thu lu giấu mình trong bụi cây, ngó trộm
Sa Ly tắm… Biết chuyện này, Kiên buồn bã, đêm mất ngủ, ngày bơ phờ. Nhưng, Chương vẫn
không hề hay biết tình cảnh thảm hại của Kiên.

Kiên vẫn nhớ có một buổi trưa im gió, anh ngồi trên ban công nhìn xuống vườn cây. Chương và
Sa Ly đang đứng dưới bóng râm đổ dài từ tán lá thốt nốt. Mái tóc Sa Ly dài óng mượt xõa xuống
bờ vai trắng ngần. Cô vừa gội đầu xong, đang bưng chậu quần áo đứng tần ngần bên Chương.
Hai người nói chuyện với nhau, Kiên nghe rõ mồn một:

“…”

Sa Ly hỏi:

“Dạo anh còn ở đại đội thông tin, em thấy một anh khoác cái máy to như cái máy lạnh nhỏ, có hai
cần ăng ten vắt vẻo như râu con xén tóc, hành quân đi ngang qua?”

Chương bật cười:

“Máy vô tuyến điện PRC 25W đấy cô bé ạ. Ở hai phương trời hai máy, người ta có thể đánh mật
mã liên lạc với nhau xa hàng chục cây số, hàng trăm cây số, em ạ.”

“Có nói được với nhau không?”

“Được chớ. Nhưng, thông tin quân sự hoàn toàn bí mật, liên lạc mà nói chuyện như anh và em
sẽ lộ. Bom đạn dội xuống đầu ngay”.

“Phnompenh không còn bom đạn nữa. Ước gì anh và em mỗi người có một máy để nói chuyện
với nhau nhỉ?”

Chương chợt buồn lặng:

“Ngày anh còn ở đơn vị cũ, có hôm đi kiểm tra đường dây ở bờ bên kia nhìn thấy em như cái
chấm nhỏ, anh gọi khản tiếng mà em vẫn lặng lẽ đi xuống bến tắm”.

Sa Ly bảo:

“Thì em có nghe được đâu. Sông rộng quá mà.”

“Ừ. Em không nghe được. Anh chỉ muốn bơi sang với em. Lúc đó, anh ước mơ: giá như có cái
máy đàm thoại phát sóng vô tuyến chỉ to bằng hòn gạch thôi, bỏ vào túi xách, hay túi cóc ba lô
cũng được. Máy của hai người kết nối qua một trung tâm với cùng bước sóng. Chúng ta bấm số
như điện thoại hữu tuyến là trò chuyện được với nhau.”

“Ôi. Thích quá nhỉ.”

“Em sẽ gọi nói chuyện suốt ngày, suốt đêm với anh chứ?”

“Nói suốt ngày suốt đêm? Anh sẽ chán em ngay”.

“Không! Em hãy nói theo cách của riêng em thì anh sẽ không chán”.

“Và anh sẽ chăm chú lắng nghe em nói chứ?”

“Tất nhiên! Anh cũng sẽ nghe em nói suốt ngày suốt đêm, nói và nghe đến khi cái máy nó nóng
bỏng rẫy lên, em yêu ạ.”

“Rồi em sẽ may cho anh một cái túi thổ cẩm to bằng viên gạch đựng vừa cái máy đàm thoại của
anh.”

“…”

Câu chuyện tình tứ quá. Dù buồn bã, Kiên vẫn cố kìm nén được tình cảm, không để phát lộ ra
ngoài. Kiên cay đắng cười khẩy một mình: Hãy nói theo cách của riêng em! Cách gì? Cách của

tình yêu ư? Máy đàm thoại bé bằng hòn gạch ư? Hão huyền. Ước mơ rồ dại, viển vông. Làm
được thì người ta đã làm rồi, chẳng ai dại gì mà dùng cái máy nặng mười mấy cân, khoác è cổ.
Nhưng, Kiên phải công nhận rằng: Thằng cha này có ý tưởng rất hay. Cấp trên nên điều chuyển
phắt nó về nước, tiếp tục học năm thứ hai Khoa Vô tuyến điện - Đại học Bách Khoa; vài chục
năm sau thằng cha này thành nhà khoa học. Rất có thể nó làm nên chuyện đấy. Tất nhiên, mình
chỉ cần loại được một tình địch.

Câu chuyện tình của bộ ba Kiên - Sa Ly - Chương sẽ chẳng xảy ra to tát nếu như tối hôm sau
Kiên kìm nén được tình cảm của mình.

Đêm ấy, trăng lên sớm ngoi lên và neo trên những ngọn cây thốt nốt. Dòng sông Tonle Sap tràn
ngập ánh vàng. Gió dưới sông rười rượi thổi. Cầu Monivong mờ xa im lìm mệt mỏi nối hai bờ.
Kiên tắm muộn; ngụp lặn dưới sông xong, vơ quần áo ướt đi về nhà. Vườn xoài đang mùa trĩu
quả chín, thỉnh thoảng lại có trái rơi bộp... bộp. Đi ngang qua khu nhà tổ phiên dịch, đến gần cây
thốt nốt lỗ chỗ vết đạn ghim đã cũ, Kiên bất chợt nghe tiếng con gái gọi Chương thì thào:

“Boòng Chương. Anh Chương”.

“Sa Ly. Sao lại đứng một mình ở đây?”

“ Em đứng chờ anh”.

“Sa Ly lên nhà đi. Đứng thế này không tiện. Đêm hôm ngại lắm, mọi người nhìn thấy. Nguy hiểm
lắm”.

À thì ra là dấm dúi gặp nhau cơ đấy. Tay Chương ghê thật. Kiên nghĩ miên man.

“Boòng sơ lanh on tê?” (Anh có yêu em không?).

Bất ngờ quá, Chương đâm hoảng:

“Trời đất ơi. Anh trả lời nhiều lần rồi. Yêu thương để trong lòng, không nhất thiết phải nói ra từ
miệng. Chúng mình đang phải giấu. Em đừng nói thêm lời nào nữa. Nguy hiểm lắm”.

“Boòng sơ lanh on tê?”

Vẫn lại câu hỏi ấy, giọng quyết liệt, nồng nàn, rồi Sa Ly choàng hai cánh tay thon thả ôm chặt
cứng hai vai Chương. Tấm thân nuột nà, mông căng tròn và eo hông hơi thắt, bờ vai nhỏ run rẩy
như đánh đu trên vai Chương, như vít đầu Chương xuống. Kiên nghe được cả tiếng thở gấp hào
hển của Sa Ly.

Mặt Kiên nóng bừng bừng, máu chạy giần giật thái dương. Lòng dạ cồn cào, Kiên đã không kìm
được sự ghen tuông nhỏ nhen, đố kị:

“Đồng chí Chương. Đồng chí có nhớ kỷ luật dân vận không?” 

 
  Ảnh: HTO
Đôi tình nhân giật mình, buông nhau ra. Chương ngượng nghịu, hai tay vò vò mái tóc xoăn đen,
giọng nói đầy lo lắng:

“Ki…ên. Mình...”

“Tại em. Anh Chương không có lỗi”. - Sa Ly khẩn khoản.

“Thôi. Cô không phải bênh no…ó”.

Giọng Kiên lạnh lùng, nghe ghê răng như miệng ai đó đang nghiền mảnh sành chai xào xạo.
Dường như không kìm nén được nữa, Chương bóp chặt tay Kiên, nói nhỏ nhẹ và kiên quyết:

“Cậu hãy để cho trái tim chúng tôi nói bằng cách của trái tim”.

“Văn hoa, triết lý quá. Câu chuyện tiếng nói trái tim này không hợp ở chiến trường đâu. Lạc lõng
lắm! Cá nhân! Khác biệt lắm!”

Tình huống căng thẳng dần lên, sẽ diễn biến thành chuyện to tát nếu lúc đó không gặp trung tá
chủ nhiệm chính trị họp ở bộ chỉ huy sư đoàn về muộn đi qua. Kiên báo cáo luôn mọi sự việc một
cách nhanh gọn, trôi chảy, hào hứng như vừa lập được chiến công ngoài mặt trận. Ông trung tá
dạn dày kinh nghiệm sống và đầy nhân ái bảo đừng làm mọi chuyện ầm lên, không có lợi cho
công tác dân vận, về phòng làm việc của ông giải quyết.

Kiên vừa đi vừa thấy hả hê trong lòng. Kiên liên tưởng đến cái mặt bạc phếch, van xin và lo sợ
của Chương trước ông chủ nhiệm chính trị sư đoàn. Không bị tống ra chiến trường thì Chương
cũng bị kỷ luật đưa về nước. Chương sẽ mất mặt trước Sa Ly...

Trái với suy nghĩ và sự liên tưởng của Kiên, gã tóc xoăn đen ngồi trước mặt ông trung tá trình
bày một cách bình tĩnh, rõ ràng, rành mạch, xin nhận tất cả lỗi về mình và chấp nhận bị kỷ luật.
Còn Sa Ly thì sụt sịt. Cô bảo: “Mẹ cháu cũng là người Việt Nam. Chúng cháu đều chưa vợ
chưa chồng, chả lẽ không được yêu nhau à?”. Rồi cô lại nhận lỗi hết về mình. Sa Ly khóc, năn
nỉ: “Cháu xin chú đừng kỷ luật anh Chương.” Ông chủ nhiệm chính trị bảo: “Cháu bình tĩnh lại
nào.” “Không! Hãy để cho cháu nói từ cách nghĩ của cháu. Cũng là do cháu quá thương, không
kiềm lòng được. Chúng cháu yêu nhau thật lòng. Dù lúc này đang thời chiến, chưa cho chúng
cháu yêu nhau, lấy nhau thì khi thanh bình cháu cũng tìm về Việt Nam, cháu muốn sống với anh
Chương trọn đời”. Rất quyết liệt! Một cô gái nhỏ bé mong manh, yếu ớt nhưng ngùn ngụt ý chí
bảo vệ tình yêu. Kiên ngỡ ngàng. Chương ngỡ ngàng. Và ông trung tá chủ nhiệm chính trị sư
đoàn cũng ngạc nhiên. Hai mắt Sa Ly mọng nước, lấp loáng ánh đèn và đầy kiên nghị.

Sự việc không như Kiên nghĩ, họ đã yêu nhau hết lòng, yêu theo cách của họ và sống chết vì
nhau. Cái kiểu yêu đương này thật lạ! Kiên thấy mình tẽn tò quá; tự nhiên thừa ra, đơn độc, thấp
bé và đớn hèn quá. Ông chủ nhiệm chính trị ngẫm nghĩ, cân nhắc, mặt ông đanh lại. Ông điềm
đạm, từ tốn nói, đại ý rằng: “Chính quyền nước bạn còn rất non trẻ mà công việc đang bộn bề,
phức tạp. Tàn quân Pôn Pốt chưa diệt xong. Hai nước chưa chính thức có hiệp định về hôn
nhân. Lúc này, cần phải biết tạm gác hạnh phúc riêng tư và chờ đợi, đừng để kẻ xấu lợi dụng
chuyện đó phá hoại tình đoàn kết hai dân tộc...”. Ông còn nói nhiều về tuổi trẻ, ước mơ, tình
bạn, sự chờ đợi, hy sinh cho đất nước khi cần thiết... Và cuối cùng chuyện được khuôn lại chỉ có
chừng ấy người biết.

Từ hôm sau, bộ ba Kiên - Sa Ly - Chương có một sự thuyên chuyển vị trí công tác nho nhỏ.
Sa Ly đi với tổ dân vận khác xuống phum, Kiên đi với Dên, còn Chương đi với chị Rêu. Thỉnh
thoảng gặp, sáp mặt với nhau, Chương vẫn nói chuyện, trao đổi bình thường không có ý giận;
coi như chưa có việc gì xảy ra. Kiên cảm thấy thèn thẹn, có một chút ân hận, ngại ngùng.
Nhưng, anh vẫn biện hộ cho việc làm của mình là đúng với tinh thần chấp hành kỷ luật dân vận,
với ý thức trách nhiệm của người lính.

Một tuần sau, Chương khoác ba lô đến chào Kiên:

“Mình trở về đại đội thông tin của trung đoàn cũ chiến đấu. Làm thằng lính chiến mà ở nơi yên ả
thế này, chán quá. Nằn nì mãi chủ nhiệm chính trị sư đoàn mới cho chuyển. Thôi mình đi kẻo xe
chờ”.

Quá đỗi bất ngờ, Kiên bảo:

“Hượm đã, Chương. Sa Ly biết cậu đi chưa?”

“Sa Ly không biết đâu. Biết, càng khó đi. Mà mình sẽ quay lại cơ mà”.

Kiên gãi tai, bối rối. Tự nhiên lúc đó, Kiên có nhu cầu thanh minh, giãi bày. Không nói ra được
điều day dứt, ân hận chứa chất trong lòng thì biết bao giờ nói được. Chiến tranh vẫn còn liên
miên. Cái giống tàn quân Pôn Pốt đánh lại quân tình nguyện toàn dùng chiến thuật du kích tập
kích ban đêm. Lính pháo, lính tăng, lính thông tin… cũng phải cầm súng bộ binh truy quét địch.
Rừng - nơi tàn quân địch trú như thiên la địa võng, ai biết trước được đây không phải là cuộc
chia li cuối cùng.

“Chuyện hôm trước, thực lòng tớ không muốn làm thế… Tớ trẻ con quá”.

“Thôi, chuyện qua rồi, Kiên ạ”.

“Dưới đơn vị bom đạn ác liệt lắm…Mình muốn nói sao Chương lại…”.

“Mỗi người sống và hành động theo cách của mình, không ai giống ai đâu, Kiên ạ. Thôi, mình đi
đây”.

Chương nắm tay Kiên rất chặt rồi bước nhanh.

Một thoáng ngây người rồi chợt tỉnh ra, Kiên vùng căng chạy sang khu nhà tổ phiên dịch. Từ
chân cầu thang, Kiên gọi toáng lên: Sa Ly…y. Sa Ly…y.

Như có linh tính báo điềm chia li, Sa Ly tối sầm mặt, lập cập chạy ra. Kiên bảo: “Chương về đơn
vị cũ rồi. Chạy nhanh kẻo xe chạy mất”. Họ chạy đến nơi tập kết thì xe đã bon bon trên đường
cuốn bụi mù. Sa Ly gục đầu vào vai Kiên khóc. Tóc bết nước mắt, nhoè vai áo. Lúc ấy, Kiên cảm
thấy mình như vừa bị mất một cái gì đó rất lớn lao mà không gọi tên được.

Mùa khô qua rồi lại mùa mưa.

Người ta thuyên chuyển Kiên đến công tác ở ban chỉ huy tiền phương của sư đoàn đóng tận
Amleng cách xa Phnompenh cả trăm cây số. Một thời gian sau, Kiên về nước học ở trường sỹ
quan thông tin. Từ đó, họ không còn gặp nhau nữa.

Chuyện cũ buồn vương vấn. Tự nhiên, vang ngân trong lòng Kiên một tiếng gọi thì thầm, thành
khẩn: “Chương ơi! Anh Chương ơi! Có thể bây giờ anh đã phục viên, chuyển ngành; anh đã có
vợ con hạnh phúc; có thể anh vẫn còn ở quân đội như tôi. Anh có còn nhớ Sa Ly, nhớ chị Rêu
và Dên… không? Anh còn nhớ hay anh đã quên cái chuyện ngày xưa ấy, tôi - Sa Ly và anh khi
chúng ta còn rất trẻ”.

*
*          *

Trước ngày ra sân bay về nước, Kiên đến thăm nhà Sa Von theo lời mời của cô vào một buổi
tối nhiều sao trời. Kiên nghĩ: Chắc bà mẹ đã suy nghĩ lại về cái trạm BTS sẽ đặt ở vườn nhà
mình, hoặc cô bé định thanh minh điều gì đó. Không! Có thể chỉ là lời mời thăm nhà xã giao lịch
sự bình thường. Thực ra, anh cũng muốn đến nhà Sa Von với tư cách như một người quản lý,
lãnh đạo đến thăm nhà nhân viên; một phần cũng vì việc đặt cái trạm BTS ở đó không thành. Và
lặng thầm trong lòng Kiên cũng náo nức muốn tìm kiếm, khám phá tiếp những điều bất ngờ, bí
ẩn nơi cô gái Khơ Me khác biệt, xinh đẹp này.

Xe qua cầu Monivong. Sao trời rắc chi chít xuống dòng sông Tonle Sap. Mỗi lần bắt gặp con
sông huyền bí, dịu dàng chở bao nhiêu kỷ niệm một thời, lòng Kiên lại xao xuyến, thổn thức. Bất
chợt, chuông điện thoại reo. Ồ, lại 098… Còn quá sớm, không phải số điện thoại của Sa Ly mà là
của bố.

“Có chuyện gì đấy bố ơi?”

Giọng bố anh chậm dãi, da diết:

“Kiên ơi! Bố già rồi. Bảy mươi tuổi rồi, sống chẳng được là bao nữa. Bố mong có cháu đích tôn.
Xấu đẹp gì con cũng lấy vợ đi cho bố mừng. Đừng sống độc thân mãi như thế! Con khổ. Mà bố
cũng khổ…”

 
  Ảnh: HTO
Kiên lặng người. Lại câu chuyện lấy vợ của anh. Thương bố đến nao lòng, nhưng anh chưa tìm
cách diễn đạt thế nào để bố hiểu. Đã vài ba cuộc tình đi qua đời Kiên. Bạn bè hỏi: toàn cô gái trẻ
trung xinh đẹp tốt nết, nghề nghiệp ổn định, mà sao mày cứ yêu rồi thôi? Kiên không trả lời cụ
thể. Độc thân có phải là một cách sống? Tình yêu ơi ở đâu? Anh vẫn chưa tìm thấy cái nửa thật
sự của mình. Cái nửa phải rất khác biệt, không thể lẫn trong đám đàn bà, con gái ồn ào, nhạt
nhẽo và nông cạn. Hay hình ảnh cô gái Khơ Me ngày xưa đã choán hết tâm hồn tình cảm của
anh rồi?

Cổng nhà Sa Von mở ra. Xe chạy thẳng, chui vào gara. Kiên và Sa Von quành sang đi bộ thư thả
trên lối nhỏ rải sỏi màu trắng.

Căn biệt thự nhỏ xinh xắn, thoảng hoặc có đám rêu phong và cây trắc lá nhỏ mỏng leo chân
tường ốp đá sần. Khuôn viên khá rộng. Kiên bắt gặp một phong cách kiến trúc khá độc đáo,
chứng tỏ chủ nhân của nó là một người sống không nhạt nhoà, không dễ lẫn vào đám đông. Đèn
ở vườn sáng toả nhẹ. Thảm cỏ Úc xanh ngắt mịn như nhung hứng những cái bóng đổ dài từ
lùm cây thốt nốt. Các lối nhỏ lát đá dẫn đến mảnh hồ bán nguyệt có hòn non bộ và các vòi phun
nước. Mùi hương hoa hồng dìu dịu phảng phất xa gần lẫn mùi hương hoa ngọc lan xao xuyến.
Bước chân người lính mách bảo rằng, anh đang ở một nơi sang trọng và quý phái.

“Hôm nay, em sẽ dành cho ông một bất ngờ lớn.”

Kiên bảo:

“Đời tôi luôn luôn là một chuỗi bất ngờ. Thêm một bất ngờ nữa cũng thú vị đây.”

Người giúp việc ra, chào lễ phép bằng tiếng Khơ Me. Kiên đáp lại cũng bằng tiếng Khơ Me làm
chị ta ngạc nhiên. Sa Von kêu một cà phê đen đá. Kiên gọi một ly cà phê nâu. Dưới ánh áng
neon xanh dịu; nhạc nền dân ca Khơ Me nhẹ nhàng đầy ắp căn phòng, Sa Von có vẻ hiền từ, dịu
dàng hơn lúc ban ngày làm việc ở công sở. Kiên lại bắt gặp hình ảnh gương mặt và mái tóc xoăn
thân quen trong ký ức. Anh bồi hồi nhớ buổi khảo thí tháng trước với kiến thức vững chắc và khả
năng biến hóa ứng xử của cô bé Sa Von. Thật thú vị! Chưa bao giờ anh gặp một thí sinh thông
minh và sắc sảo như vậy.

Hôm ấy, Kiên hỏi:

“Say it your way - Hãy nói theo cách của bạn”. Câu slogan này của Viettel, bạn hiểu ý nghĩa của
nó như thế nào?”

Cô gái Khơ Me tóc xoăn ngước đôi mắt bồ câu đưa một ánh nhìn đối thoại chứ không phải trả
lời:

“Thưa ông! Em nghĩ, Người Viettel muốn nói với khách hàng là: Viettel đã, đang và sẽ luôn luôn
quan tâm, lắng nghe và nỗ lực đáp ứng làm thỏa mãn mọi khát vọng riêng biệt, nhu cầu giao tiếp
thông tin liên lạc của mỗi người.

Chọn “Say it your way”, Người Viettel muốn khẳng định sự quan tâm, lắng nghe, thấu hiểu của
lãnh đạo đối với từng cá nhân trong ngôi nhà chung Viettel. Thông điệp khuyến khích mọi người
phát triển bản sắc cá nhân và ủng hộ mỗi cá thể sáng tạo để làm phong phú tư tưởng và hành
động của Viettel…

Thưa ông! Em muốn làm một người khác biệt trong ngôi nhà chung Viettel của các ông. Vì chính
các ông tôn trọng, tạo điều kiện cho mỗi cá nhân được nghĩ, được sống, được yêu, được nói
theo cách của riêng mình để sáng tạo và hành động là một trong những nguyên nhân mà em thi
tuyển vào Viettel. ”

Kiên lại hỏi:

“Sa Von. Còn nguyên nhân nào nữa? Dù không trả lời câu hỏi này, bạn vẫn chắc chắn trúng
tuyển hàng đầu vào Viettel.”

“Chuyện riêng tư của gia đình. Xin ông hãy hiểu: Chính cái nguyên nhân không nói thành lời ấy
là động lực lớn nhất làm em bỏ công ty cũ đến với Viettel Cambodia của Việt Nam các ông một
cách quyết liệt”.

Không phỏng vấn nữa. Chẳng có gì đáng phàn nàn, Sa Von sắc sảo, giàu hàm lượng tri thức
chuyên môn, xã hội, được trình bày cô đọng với một giọng trầm ấm, vang ngân. Lúc đó, Kiên đã
nghĩ: cô gái này sẽ còn đầy bí ẩn.

Bây giờ, cô gái Khơ Me mắt bồ câu sẽ mang điều bất ngờ lớn nào đến với Kiên nữa đây? Kiên
đưa mắt nhìn quanh căn phòng khách khá rộng. Một cây chuối lùn nở hoa đỏ chói chang ở góc
nhà. Một bình cổ đất nung màu đỏ nâu. Một bức sơn dầu phong cảnh đền tháp Ăng co hùng
vĩ. Và một bức ảnh nhiều người đen trắng to bằng nửa cái cánh cửa. Ôi trời ơi! Có lẽ nào mình
đang ngự trên bức tường kia?

Kiên bật dậy. Đây rồi! Là Kiên, là Rêu, Dên, Sa Ly, và Chương. Bức ảnh đen trắng chụp từ dạo
bọn Kiên ở tổ công tác của sư đoàn bộ ngày xưa…Kiên đưa bàn tay lóng ngóng đặt lên vai, dờ
dẫm từng gương mặt, rồi gục đầu vào bức ảnh. Kiên lặng người…

 
 Ảnh: HTO
“Đó là nguyên nhân lớn nhất để cháu bỏ công ty cũ về làm ở Công ty ViettelCambodia. Mẹ con
cháu bao nhiêu năm nay đi tìm hai người đàn ông trong ảnh.”

Kiên buông một tiếng thở dài:

“Và mẹ con cháu đã từng thất vọng…”

“Vâng! Thất vọng rồi lại hy vọng, dù hy vọng rất mong manh.”

“Mẹ cháu đã kể những gì?”

“Cha cháu hy sinh mà không biết mặt con. Mẹ ít nhắc lại vì mỗi lần kể là một lần đau lòng. Mẹ
khóc. Đội quy tập mộ liệt sĩ từ Việt Nam sang tìm kiếm cất bốc, nhận ra hài cốt cha cháu dưới
hố chôn bởi trùng điệp những sợi dây thông tin hữu tuyến quấn quanh. Người ta đưa hài cốt cha

cháu về quê hương Việt Nam. Mẹ theo đưa tiễn cha về tận cửa khẩu Mộc Bài. Về nhà, mẹ nằm
bệt, âm thầm khóc. Cháu thì quá nhỏ để chưa biết việc đời việc tình việc nghĩa. Ba ngày sau, mẹ
dậy trở lại hố cải táng, đào đất lên, lặng lẽ nhặt nhạnh những sợi dây thông tin hữu tuyến lấm đất
ấy mang về chôn ở vườn nhà. Mẹ giữ kỷ vật đau thương linh thiêng trong lòng đất…”.

Kiên lặng im. Có một vùng ký ức thời loạn mênh mông trước mặt anh. Kiên hình dung ra hình
ảnh thiếu phụ Khơ Me trong dáng chiều tà hoang vắng ngồi khóc bên đống dây thông tin hữu
tuyến dính bùn đất. Những con chim kiếm ăn xa đập cánh xập xệ tìm về nơi tổ ấm…

“Thực ra, lúc đầu mẹ cháu rất ngại đặt cái cột trạm phát sóng di động BTS ở trong vườn nhà
mình. Mẹ bảo: Cứ mỗi lần nhìn thấy cái cột ấy, mẹ sẽ nhớ lại chuyện ngày xưa. Sau đó, nghĩ lại,
mẹ bảo: Mẹ đang rất xa, bọn con ở nhà không biết lại đào phải nơi chôn đống dây thông tin hữu
tuyến ấy. Mẹ cần có thêm thời gian để nghĩ về việc này…”

Sáng, Kiên phải ra phi trường Pochentong để về Việt Nam. Anh không còn thời gian để chờ Sa
Ly đáp máy bay từ Paris về Phnompenh. Đêm hôm ấy, anh chìm đắm trong một giấc mơ huyền
ảo.

Sa Von và anh ngồi ở quán café Bến Xưa. Đèn màu hồng lung linh sáng. Cái lầu vọng nguyệt
nhô ra mặt nước sông Tonle Sap. Trăng đổ tràn ánh bạc. Kiên thấy lòng mình bâng khuâng, cảm
giác gần gũi, ấm cúng đầy ắp. Sa Von vẫn cái giọng tinh nghịch theo lối xưng hô phương Tây:

“Sáng mai, ông về Việt Nam. Em muốn dành cho ông một bất ngờ cuối cùng.”

Lại bất ngờ! Kiên cảm thấy thật thú vị.

“Ông hãy nhìn kìa…”

Kiên nhìn ra cửa theo tay Sa Von chỉ. Một thiếu phụ mặc xà rông màu lửa, áo trắng thêu vài hoạ
tiết nơi ngực trái, khoan thai bước vào quán. Kiên nhấp nhổm và đứng phắt dậy, bàng hoàng.

Thiếu phụ cũng quá đỗi bất ngờ, ngạc nhiên khi nhìn thấy Kiên. Chị dừng bước, hai chân bủn rủn
khuỵu xuống. Sa Von nhào ra đỡ và dìu thiếu phụ đến bên bàn. Không thể tin ở mắt mình, Kiên
thảng thốt:

“Sa… Ly? Có phải Sa Ly không?”

Thiếu phụ nức nở:

“Kiên. Anh Ki…ên!”

Hai bàn tay nắm chặt hai bàn tay. Rồi thiếu phụ nhào đến, gục đầu vào bờ vai xưa cũ của Kiên
như ngày xưa thẫn thờ thất vọng nhìn xe ô tô chở Chương về đơn vị trong bụi cuốn mù. Da diết
quá! Kiên cảm thấy tim đang đập thình thình. Cuộc gặp gỡ bất ngờ không hẹn trước sau mấy
chục năm. Thời gian làm cho nàng đã đi vào mùa thu cuộc đời, nhưng vẻ đẹp ngày xưa thì vẫn
còn lưu lại không hề mất đi.

“Thưa ông! Tối nay, xin ông nán lại ngồi với mẹ tôi. Và xin nhường không gian này cho mẹ và
ông.”

Sa Von ý nhị chào rồi quay bước. Sa Ly cầm khăn mù soa chấm nốt giọt nước mắt cuối cùng,
nàng cười:

“Con bé này đã làm tất cả mọi điều cho mẹ vui vẻ. - Giọng nàng chậm rãi, bâng khuâng - Lâu
quá rồi anh Kiên nhỉ. Anh còn nhớ cái bến tắm ngày xưa?”

“Vật đổi sao rời. Anh không nhận ra.”

“Cũng phải thôi, hơn hai mươi năm rồi còn gì nữa. Chúng ta đang ngồi bên cái Bến xưa đấy.
Kiên ạ”

Bến Xưa! Cái bến nước ngày xưa lại vang vọng trong lòng Kiên, nhưng không phải tưởng tượng
ra hình ảnh Sa Ly nữa; nàng đang ngồi trước mắt anh, bằng thân hình eo thắt mềm mại, bằng
mùi hương da thịt thật sự. Lúc này, quán vắng, dường như chỉ có hai người. Lòng Kiên nôn nao
một nỗi vui buồn khó tả. Ký ức ngủ yên lại một lần thức giấc. Những hình ảnh mờ ảo cứ rõ nét
dần và như hiện ra ngay trước mặt anh…

Tỉnh dậy, và trên đường ra phi trường Pochentong bay về nước, Kiên cứ tiếc hoài giấc mơ.

Tháng 4 – 2009
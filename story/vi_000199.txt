Sự kiện anh Trần Suyền, nguyên Khu ủy viên khu V, Bí thư Tỉnh ủy Phú Yên được Đảng, Nhà nước truy tặng danh hiệu Anh hùng lực lượng vũ trang nhân dân vì có công lao, thành tích đặc biệt xuất sắc trong sự nghiệp chống Mỹ cứu nước là niềm vui lớn của đảng bộ và nhân dân tỉnh Phú Yên. Nhìn vào tấm bằng tuyên dương công trạng tôi thầm nghĩ giá như đằng sau hai chữ Trần Suyền mở ngoặc ghi thêm hai chữ Sáu Râu thì hay biết bao. Vì Sáu râu là tên riêng (biệt danh) của anh được dùng suốt trong thời kỳ chiến tranh chống Mỹ cứu nước. Nó rất gần gũi với cán bộ, chiến sĩ thời chiến tranh. Thời đó, đồng chí, đồng đội biết anh Sáu Râu mà nhiều người không biết anh là Trần Suyền.

 
 
Khoảng năm 1967, tôi được chọn vào đơn vị phục vụ cuộc chỉnh huấn cán bộ do cấp trên tổ chức. Đây là cuộc chỉnh huấn đặc biệt quan trọng. Tình cờ tôi được nghe các chú cán bộ thì thầm với nhau: Đợt chỉnh huấn này quan trọng lắm, có đồng chí Sáu Râu về chỉ đạo đó. Tôi tò mò dọ hỏi thì mới biết đó là đồng chí bí thư tỉnh ủy, người lãnh đạo cao nhất tỉnh. 

Hồi đó tôi còn trẻ lắm, mới được kết nạp vào Đoàn nên được lãnh đạo chọn phục vụ chỉnh huấn. Trong trí tưởng tượng của tôi, đồng chí Sáu Râu là người quắc thước, nhiều râu. Mấy hôm sau tôi thấy một người đứng thuyết trình trên bục cử tọa, dáng người ốm, tầm cao trung bình, tôi nghĩ đây nhất định là người lãnh đạo cao nhất rồi, đồng chí Sáu Râu chứ ai, nhưng sao không có râu? 

Tìm hiểu thì được biết là trong thời kỳ hoạt động bí mật, các cán bộ lãnh đạo đều có bí danh riêng để gọi, xưng hô nhằm giữ bí mật, che mắt địch. Tên bí danh thường là do anh em, đồng đội đặt cho. Đồng chí Trần Suyền không có râu, nhưng có thói quen khi nói chuyện diễn thuyết lúc nào cũng chậm rãi và hay sờ vào cằm như vuốt râu; khi nghĩ ngợi vấn đề gì khó khăn, hóc búa cũng mân mê, sờ cằm. 

Phát hiện đặc điểm này của anh, đồng đội chọn đặt cho anh cái tên Sáu Râu. Thế là Sáu Râu đã thành tên riêng của anh. Nó được anh em, đồng đội yêu mến, trân trọng chỉ gọi anh là anh Sáu Râu trong suốt thời kỳ chiến tranh. Cái tên riêng Sáu Râu đã có tác dụng giữ gìn bí mật, che mắt địch. Năm 1975, tôi được đọc một tài liệu thu được ở ty chiêu hồi ngụy: báo cáo điều tra, nghiên cứu lý lịch của các cán bộ lãnh đạo cộng sản của cơ quan thông tin - chiêu hồi của ngụy quyền trong đó có đoạn viết như sau: Bí thư Tỉnh ủy Phú Yên là Sáu Râu, đặc điểm là người tầm thước và có nhiều râu(?!)...

 
 
Bạn bè, đồng chí, đồng đội một thời chiến tranh máu lửa nhớ về anh không chỉ nhớ một đồng chí lãnh đạo tài năng, mẫu mực mà còn nhớ cả những câu chuyện đời thường rất thật mà nghe như giai thoại. Xin kể ra đây vài ba mẩu chuyện điển hình:

Sau năm 1954, chính quyền Mỹ - Diệm ở miền Nam thực hiện chính sách đàn áp, khủng bố dã man đối với cán bộ, đảng viên và những người tham gia kháng chiến hòng dập tắt phong trào cách mạng ở miền Nam. Số đồng chí ở lại bám trụ gầy dựng phong trào đều phải hoạt động bí mật. Tình hình lúc ấy khó khăn lắm, không có nguồn tiếp tế, thiếu thốn đủ bề. Một lần anh Sáu Râu dẫn đoàn công tác, vì phải tránh những nơi địch có thể phục kích nên đường đi phải băng đồng lội suối, cỏ ba dao, cỏ vắt, gai mắc cỏ tây, lau lác cào cứa rách da. Đau quá, một số đồng chí không chịu nổi định lấy quần dài ra mặc cho đỡ đau. Thấy vậy, anh Sáu từ tốn nói: Các đồng chí cố cắn răng chịu đau: da rách nó lạnh lại chứ nếu quần rách hết lấy gì mặc để gặp dân, gặp cơ sở.

Sống ở rừng núi thiếu thốn đủ thứ, một lần trời mưa dông tối đến anh em trông cơ quan bắt được nhiều cà khé lắm (một loại cua đá). Anh em đinh ninh thế nào cũng được cải thiện, bữa ăn có chất tươi. Sáng ra anh Sáu dậy sớm xuống suối anh mở lồng thả hết những con cua cái. Đồng chí cán bộ phục vụ thấy vậy tức quá, đồng chí quát ngay thủ trưởng: Tại sao đồng chí thả hết đi, đồng chí có biết là anh em phải lặn lội cả đêm mới bắt được chừng ấy không? Anh Sáu từ tốn trả lời: tui chỉ thả những con cua cái thôi mà, thà mình ăn ít một chút, để con cái lại nó còn sinh sản có mà ăn nữa chứ.

Một lần khác, địch vây ráp dài ngày quá không nhận được lương thực tiếp tế tiêu chuẩn gạo mỗi bữa ăn phải đong bằng đít lon sữa bò. Anh nuôi phải sử dụng cả bẹ cây môn nước để nấu cháo với số gạo ít ỏi đó. Anh Sáu lững thững xuống bếp thấy anh em chỉ lấy một phần ba bẹ môn, bỏ phần cọng và lá. Thấy vậy anh Sáu phê bình sao các đồng chí bỏ đi nhiều thế còn đâu ăn chờ đến khi lấy được gạo. Bị phê bình oan, ức quá, đồng chí phục vụ nấu món cháo lá môn đem cho thủ trưởng ăn, không ngờ thủ trưởng ăn hết phần cháo lá môn. Các đồng chí phục vụ đều kinh ngạc, lá khoai môn ngứa thế, mình nếm thử một tí mà đã thấy ngứa kinh khủng, sao thủ trưởng ăn không nghe kêu ngứa gì cả, tài thật.

Có lẽ đồng chí Trần Suyền - anh Sáu Râu là một trong số rất ít đồng chí bí thư tỉnh ủy được nhà nước tuyên dương danh hiệu Anh hùng lực lượng vũ trang nhân dân. Mặc dù anh Sáu Râu là người có thời gian làm bí thư tỉnh ủy Phú Yên lâu nhất, nhưng không phải vì thế mà “sống lâu lên lão làng” để trở thành anh hùng. Vì đồng chí là cán bộ lãnh đạo cấp cao nên Ban Thường vụ Tỉnh ủy Phú Yên lúc đó đã thảo luận, cân nhắc rất kỹ mới đề nghị cấp trên xét phong tặng danh hiệu anh hùng. Nhìn vào bảng thành tích, công trạng của anh, chúng ta thấy có những mốc son tiêu biểu, nổi bật như sau:

Chỉ đạo tổ chức thành công cuộc đồng khởi Hòa Thịnh (22-12-1960). Sau đồng khởi Bến Tre, đồng khởi Hòa Thịnh đã giương cao ngọn cờ đi đầu phong trào nhân dân đứng lên khởi nghĩa đập tan bộ máy ngụy quyền, thiết lập chính quyền nhân dân tự quản ở liên khu V.


 
 Chỉ đạo và tổ chức thành công kế hoạch giải thoát luật sư Nguyễn Hữu Thọ, đưa luật sư về Trung ương cục an toàn (10-1961) để sau đó Luật sư được bầu làm Chủ tịch đoàn chủ tịch Ủy ban Trung ương Mặt trận dân tộc giải phóng miền Nam Việt Nam.

Là người tổ chức và chỉ huy bến tàu không số Vũng Rô trên hành lang đường mòn Hồ Chí Minh trên biển, Bến cảng tồn tại dài ngày và  tiếp nhận được nhiều chuyến tàu nhất (1964 - 1965).

Tổ chức chỉ đạo quân và dân Phú Yên chống càn bẻ gãy một trong năm mũi tên “tìm diệt” của lực lượng quân đội Mỹ vào mùa khô năm 1966, mà tiêu biểu với chiến thắng Gò Thì Thùng ta tiêu diệt cả tiểu đoàn không vận Mỹ đổ bộ bằng đường không.

Lãnh đạo, tổ chức chiến dịch tết Mậu Thân (1968) tổng tiến công và nổi dậy đánh chiếm thị xã Tuy Hòa và một số quận lỵ làm chủ nhiều ngày. Góp phần cùng cả nước buộc Mỹ phải xuống thang chiến tranh, ngồi vào bàn đàm phán, rút quân về nước, kết thúc chiến tranh.

Mỗi cá nhân gắn liền với lịch sử thời đại mà họ đang sống. Chúng ta tự hào về truyền thống lịch sử của quê hương mình, tự hào về đảng bộ và nhân dân Phú Yên anh hùng. Chúng ta coi những thành tích, chiến công to lớn ấy là của đảng bộ, quân và dân Phú Yên, là đương nhiên có phần công lao xứng đáng của anh với trọng trách là người đứng đầu. Đảng bộ và nhân dân Phú Yên tự hào về truyền thống vẻ vang, anh hùng  của mình và cũng tự hào về anh là người bí thư tỉnh ủy anh hùng, vì chính anh đã làm vinh quang thêm cho quê hương.
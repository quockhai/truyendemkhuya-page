Lý Trần Quán đỗ tiến sĩ năm Cảnh Hưng thứ 27, triều vua Lê Hiển Tông, rồi nhận chức Tư nghiệp Quốc Tử Giám thiêm sai tri bình phiên. Là người trung tín, hiếu nghĩa, Quán được cả vua Lê và chúa Trịnh tin dùng.
 
Trong phủ, công việc của chúa Tĩnh Đô vương Trịnh Sâm đang tiến triển khá hanh thông thì bỗng  có người đàn bà đẹp tên là Đặng Thị Huệ xuất hiện, từ địa vị một cung nữ nàng nhanh chóng trở thành tuyên phi của chúa, rồi mang thai sinh ra Trịnh Cán thì trong vương phủ bắt đầu có những đảo lộn, hình thành hai phe, và cả hai đều nhằm vào cái ngôi thế tử kế nghiệp chúa mà tranh giành. Phe tôn phò Trịnh Khải (còn có tên Trịnh Tông) là số đông nhưng ít ra mặt lộ diện, bởi Trịnh Khải dù là con trưởng nhưng không được chúa yêu. Thân mẫu Khải là Dương Thị Ngọc Hoan, một cung tần, sau được phong tu dung, sinh ra trong một gia đình bình dân. Nàng không phải không có nhan sắc, nhưng nhập cung đã lâu vẫn chưa được chúa yêu, chưa được vào cung cấm với chúa lần nào. Rồi một đêm, Ngọc Hoan ngủ mơ thấy con rồng trong một bức tranh. Sáng hôm sau, nàng kể lại với viên hoạn quan Khê Trung hầu. Ông này cho đó là điềm báo nàng sẽ sinh vương tử, liền khải lên chúa. Trịnh Sâm nghe xong thì phán: “Mơ thấy rồng thì sẽ sinh ra con làm vương. Nhưng nàng mơ thấy rồng vẽ trên giấy, lại không có đuôi thì nghiệp không bền!”. Trịnh Sâm vẫn từ chối chuyện chăn gối phòng the với nàng. Ngọc Hoan buồn não nề. Khê Trung hầu bảo nàng: “Đừng vội nản lòng. Hãy bền gan chờ đợi. Thời cơ rồi sẽ đến!”.


Minh họa: Nguyễn Đăng Phú
 
Vận hội đến thật. Đêm ấy, Trịnh Sâm sai Khê Trung hầu gọi nàng cung nữ có tên Ngọc Khoan. Khê Trung Hầu giả đò ngãng tai không nghe rõ, liền đưa Ngọc Hoan vào thay. Chúa nhận ra mưu của viên hoạn quan, nhưng không hiểu sao chúa lại không nỡ đuổi Ngọc Hoan.
 
Ngọc Hoan mang thai. Đủ tháng đủ ngày sinh ra Trịnh Khải. Càng lớn Khải càng khôi ngô, tuấn tú. Lý Trần Quán làm thầy dạy Trịnh Khải từ nhỏ, ngài nhận ra Khải có cái đầu khá thông minh và mang tư chất một đấng quân vương. Quán nghĩ, vận nước còn, chính đạo và vương phủ không vướng vào những chuyện ái ố, tham sân si mà sát hại nhau thì Trịnh Khải sẽ là một minh chủ có khả năng điều hành triều chính thoát ra khỏi hiện trạng o bế suy vi này. Bởi vậy, Quán mang hết khả năng tôn phò, giúp rập Khải. Thấy chúa Tĩnh Đô vương Trịnh Sâm thờ ơ với Khải, nhiều lần Quán lựa lời hơn thiệt mong chúa nghĩ lại. Chúa cũng đã có những lúc mềm lòng, để mắt đến Khải. Nhưng đúng lúc ấy thì Đặng Thị Huệ được tiệp dư Trần Thị Lộc bảo lãnh đưa từ Kinh Bắc về nhập cung. Vẻ đẹp của Huệ như có ma lực, như cơn lốc hương hoa huệ cuộn xoáy làm chúa ngây ngất, khiến ngài nhào nặn nàng từ thân phận một con cung nữ thấp hèn trở thành một tuyên phi đầy uy quyền. Chúa xây riêng cho nàng Bội Lan Thất rất nguy nga, tốn kém, có vườn Huệ bao quanh. Đêm đêm chúa ngập chìm trong những cơn mê ái tình. Đầu óc chúa không còn nhìn thấy gì cho rành rẽ hơn một thân xác đàn bà. Nhất là từ khi Đặng Thị Huệ sinh Trịnh Cán thì tình yêu của chúa nghiêng hẳn về Cán, thờ ơ lạnh nhạt với Khải như người dưng nước lã. Các trọng thần cũng vì thế mà ngả dần về phía mẹ con Đặng Thị Huệ mỗi ngày một đông.
 
Lý Trần Quán thấm nhuần đạo Khổng. Quán cho rằng Cán là con thứ, thể trạng ốm o, trí lực tăm tối, dựng Cán làm thế tử là trái đạo, rồi sẽ sinh tranh chấp, phế lập, chém giết, loạn ly từ đấy mà ra. Quán nhận định không sai: vì cái sự thiên vị thiển cận của chúa Tĩnh Đô vương mà trong phủ đã hình thành hai phe, ngấm ngầm tìm cơ hội tiêu diệt nhau ngày càng quyết liệt. Quán đứng về phe Trịnh Khải, dù biết phe này đang núng thế, có đầu rơi máu chảy là điều đã nhìn thấy.
 
Tĩnh Đô vương Trịnh Sâm ốm nặng. Không để chúa kịp truyền ngôi cho Trịnh Cán, phe Trịnh Khải toan tính làm một cuộc binh biến phế trừ mẹ con Đặng Thị Huệ. Nhưng họ chưa kịp hành động đã bị bại lộ. Chúa mắng Khải là đứa con bất hiếu, giáng từ con trưởng xuống làm con út, tống giam. Các trọng thần tôn phò Khải đều nhận án tử. Lý Trần Quán là một trung thần, tận tâm tận huyết nhất thì may sao thoát chết. Điều này chỉ có thể giải thích là ý trời chưa bắt Quán chết. Ngay sau đấy, chúa truyền ngôi cho Trịnh Cán khi Cán mới 5 tuổi. Không hề nao núng, sợ hãi, Lý Trần Quán vẫn lặng lẽ bền gan nuôi chí tôn phò giúp rập Khải. Rồi thời cơ ấy đã đến. Chúa Trịnh Sâm băng. Binh lính tam phủ nổi lên phế truất mẹ con Đặng Thị Huệ, dựng Trịnh Khải lên ngôi chúa, hiệu là Đoan Nam vương.
 
Ngỡ rằng kể từ đây Trịnh Khải cứ thẳng đường mà tiến, nào ngờ bọn binh lính tam phủ có công dựng Khải bây giờ say mừng chiến thắng quá đà hoá thành một đám kiêu binh. Chúng nói năng với Dương thái phi, với chúa Đoan Nam vương và các trọng thần thô lỗ, bác bậc như với kẻ bề vai, chẳng còn lễ nghĩa gì nữa. Tham tụng Nguyễn Lệ và quốc sư Dương Khuông, hai trụ cột của súy phủ như hai cánh tay của chúa bị chúng truy đuổi, người phải chạy sang Sơn Tây, người chạy lên Từ Liêm lánh nạn. Dinh thự của hai ông ở phố Hàng Bồ đều bị đốt cháy, san phẳng. Võ hầu Nguyễn Chiêm cũng bị chúng đốt nhà rồi xông vào tận Tử cát đường súy phủ truy bắt, lôi qua cửa Tuyên Vũ chém chết, băm xác thành nhiều mảnh  ném xuống sông Tô. Khắp kinh thành chỗ nào cũng có kiêu binh. Ở những vị trí trọng yếu như cửa Tuyên Vũ, Quyền Bồng điếm, Tiểu Bút điếm, Các Môn, Trạch Các thì kiêu binh tụ tập dầy đặc. Lý Trần Quán chứng kiến có cả những đám kiêu binh chia lẻ đi cướp bóc, đập phá nhà cửa thường dân, hãm hiếp đàn bà con gái ngay giữa ban ngày ban mặt. Bọn này còn đòi được ăn chia những mối lợi nhà chúa thu được từ nơi đồng ruộng, sông hồ, đầm bãi, chợ búa, cửa ải. Rồi đến một ngày chính Lý Trần Quán cũng bị chúng đe doạ đốt nhà nếu ông không nhanh tay hối lộ chúng. Lính với dân trở thành kẻ thù không đợi trời chung. Dân căm ghét lính thì không thể không hận thù cả quốc cữu và vương gia.
 
Lý Trần Quán dư thừa linh nghiệm để nghĩ rằng, thời nào cũng vậy, dân đã lạnh nhạt với triều đình thì quá trình suy mạt đã đến rất gần, sụp đổ là điều không tránh khỏi. Dù rất tâm phúc giúp rập chúa, nhưng luân thường đạo lý bại hoại đến nước này, Lý Trần Quán cũng đành bó tay. Ngài gặp Đoan Nam vương, tâu:
 
 - Khải chúa thượng. Bấy lâu nay thần theo chính nghĩa mà tôn phò giúp rập chúa thượng. Có phen cái chết kề bên thần cũng không run sợ. Nay công việc tuy chưa hoàn mãn, nhưng để tránh phiền hà tới chúa thượng, thần xin phép tạm lánh về quê. Khi nào chúa thượng gọi, thần sẽ có mặt.
 

Ảnh: Phạm Duy Tuấn 
 
Đoan Nam vương Trịnh Khải vốn rất nể trọng Lý Trần Quán. Không chỉ vì Quán có công lớn dạy dỗ, dựng nghiệp cho chúa mà còn vì ngài là một văn thần trung hiếu tiết nghĩa có hạng. Chúa từng biết chuyện, hồi mẹ mất, Quán dựng lều cạnh mộ mẹ, rồi ở đó ăn kiêng, cho đến lúc đoạn tang, thân thể Quán chỉ còn da bọc xương, khô rạc như xác con ve sầu. Cái thời Khải bị giáng làm con út, suốt ngày sống lủi thủi trong phòng có lính canh, không ai dám đến gặp, nhưng Lý Trần Quán thì vẫn đến. Trước mặt bọn lính, Quán không nói với Khải được điều gì, nhưng chỉ bằng sự có mặt của ngài cũng đủ làm điểm tựa tinh thần cho Khải dám chịu mọi đớn đau mà nuôi ý chí phục thù. Nhưng giờ đây, chính sự diễn biến theo hướng rất khó đoán định, chúa buộc phải đồng ý cho Quán lánh đi. Chúa đồng ý nhưng chúa lại lo khi cần sẽ không biết tìm Quán ở đâu nên mới hỏi:
 
- Quan tư nghiệp định về quê Văn Canh, Hoài Đức hay tìm nơi trí ẩn ở đâu?
 
- Thần về làng Hạ Lôi, nơi thời trẻ thần từng đến đó dạy học. Ở đấy  có một gia đình từng bao nuôi thần như con, lại có những học trò trưởng thành, họ sẽ  giúp thần mở lớp dạy học, sống tạm qua ngày.
 
- Trẫm đồng ý để khanh đi trong lúc này cũng chỉ vì muốn giữ tính mạng cho khanh - Đoan Nam vương Trịnh Khải nói - Bởi vương triều đang rất rối ren, kẻ gian người ngay lẫn lộn, rất nguy hiểm. Nhưng ở vị thế của trẫm không thể bỏ kinh thành. Kinh sư là gốc của nước. Vua chúa bỏ gốc nước thì nước sẽ mất. Tạm thời trẫm phải chiều theo những đòi hỏi của đám lính tam phủ, rồi dần dà lựa cách mà tiêu diệt chúng!
 
Lý Trần Quán cởi triều phục bỏ lại, mặc bộ đồ vải đũi thâm, chít khăn xếp, đi dép cói, vai đeo tay nải, trông ra dáng ông đồ. Trước khi bước ra khỏi cung, Quán còn nói thêm:
 
- Khải chúa. Còn điều này nữa, thần muốn nói thật để chúa anh minh xem xét. Vua Lê Hiển Tông lâu nay bị coi như một thứ bù nhìn, cũng một phần do bên phủ ta áp chế, lấn lướt. Để giữ thân, ngài buộc phải chọn cách sống ấy. Nhưng thần biết đó là một con người trải đời và có nhân tâm. Nếu chúa thượng vì sự an thịnh của xã tắc mà trải lòng thành với ngài, thần tin ngài sẽ có những kế sách hay góp vào nghiệp trị nước của chúa thượng.
 
Nói đến đó, không cần phải quan sát thái độ của chúa, Lý Trần Quán khấu đầu lạy tạ lần cuối rồi đứng dậy vắt cái tay nải lên vai mà bước đi. Ra khỏi cổng phủ, Quán đi về phía bờ đê sông Nhị Hà. Tới Chèm, Quán ghé vào đền thờ Lý Ông Trọng thắp nén hương, xin phù hộ độ trì. Quán có gốc họ Đặng. Mẹ Quán từng đến cầu tự ngôi đền này mà sinh ra ngài. Để đền ơn, ngài xin đổi thành họ Lý, theo họ của thần đền.
 
Ra khỏi cổng đền, Lý Trần Quán tiếp tục đi theo con đê phía hữu ngạn sông Nhị ngược lên, đến làng Hạ Lôi. Giờ đây, dân làng Hạ Lôi chừng như đói khổ hơn trước. Gặp ai cũng thấy mặc quần áo vá chằng vá đụp hoặc đóng khố. Nắng tháng sáu mà có người phải đeo áo tơi kết bằng lá cọ che cái thân trần truồng gầy nhẳng như que củi. Quán tìm đến nhà Nguyễn Mộng Trang, một học trò cũ của ngài. Trang có ngôi nhà gỗ lim “thượng bò hạ kẻ”, mái lợp lá gồi. Hồi trước về đây, Trang cho Quán mượn ngôi nhà này làm lớp học, kê cánh cửa xuống đất làm bàn ghế, và Trang thì trở thành một học trò “ruột” của Quán. Bây giờ Quán trở lại, nhờ có chút chữ nghĩa, Trang đã mua được chức tuần huyện. Trang mở lớp dạy học cho đám trẻ trong làng. Nhìn Trang dạy học, trong lòng Quán khơi lên một chút tự hào bởi đứa học trò Quán dạy từ chữ nhất bây giờ cũng đã làm thầy. Nhận ra thầy cũ, Trang giới thiệu Quán với học sinh rồi cho họ nghỉ sớm để Trang tiếp thầy.
 
- Ngoài Thăng Long đang có tệ kiêu binh, chém giết ghê gớm lắm, thầy mới lánh về đây - Lý Trần Quán nói.
 
- Mấy hôm nay dân Thăng Long chạy loạn qua vùng này, con hỏi họ cũng nói như thế - Trang nói.
 
- Bởi vậy - Quán nói - Thầy xin ở tạm nhà con một thời gian, được không?
 
- Ai chứ với thầy, con nỡ nào từ chối - Trang đáp - Nhờ ơn dạy dỗ của thầy con mới được như hôm nay. Nhưng con chỉ mới dạy được hết sách Tam tự kinh. Thầy về đây rồi, kể từ nay, con muốn thầy dạy giúp Ngũ thiên tự.
 
- Thế thì lão già đang sắp chết đuối này vớ được cọc rồi! - Quán nói, không giấu được sự mừng rỡ.
 
Kể từ hôm đó trong ngôi nhà gỗ của Trang có hai lớp học. Lớp buổi sáng Trang dạy Tam tự kinh. Lớp buổi chiều Quán dạy Ngũ thiên tự. Xa cách sự chen vai thích cánh nơi kinh thành, sống bên đám học trò thôn quê chất phác, Quán cảm thấy cuộc sống đỡ đáng ghét hơn.
 
Trang đông con, nhưng sáng nào vợ Trang cũng cắp rổ ra chợ bến sông mua con cá, mớ tôm, mẻ hến về nấu nướng làm món nhắm cho hai thầy trò. Vợ Trang nấu hẳn một vò rượu nếp bắc, loại lúa trồng trên đồi, cho hai thầy trò uống dần. Học trò quê nghèo. Mỗi lớp chỉ vài ba người. Mỗi tháng mỗi người trả công thầy vài bơ gạo. Vậy mà vợ Trang vẫn lo cho chồng và thầy ngày hai bữa ăn tươi khiến không khỏi có lúc Quán áy náy, khó xử.


Ảnh: Phạm Duy Tuấn 
 
Một đêm đang say trong giấc ngủ, Lý Trần Quán bị đánh thức bởi những âm thanh lạ phía sau nhà. Quán lặng lẽ ngồi dậy nhòm qua khe cửa hậu, lối ra bếp, thì nhìn thấy dưới ánh nến le lói, Trang cùng mấy người đàn ông đang cầm dao giết một con lợn rất to. Giết lợn mà không cho lợn kêu là nghĩa làm sao? Câu hỏi ấy càng làm Quán thêm tò mò. Nhìn Trang trong chiếc quần cộc, mình trần, lưng vồng lên, tay cầm dao bầu thọc vào cổ con lợn một cách thành thục, rất giống một tay đồ tể chuyện nghiệp.
 
Sáng hôm sau, Lý Trần Quán thức dậy đã thấy Trang chỉnh tề trong áo the, khăn xếp, guốc gỗ đang ngồi dạy học. Lúc này trông Trang lại ra dáng một ông thầy đồ có hạng. Học sinh vừa tan lớp, vợ Trang đã bưng một mâm cơm có lòng lợn và tiết canh lên mời hai thầy trò. Thầy trò nhấp đến chén rượu thứ hai, Quán hỏi Trang:
 
- Đêm qua nghe tiếng động phía sau nhà, thầy thức giấc nhìn ra thấy con đang giết lợn. Sao con lại phải giết lợn lúc nửa đêm?
 
- Hoá ra thầy đã biết - Trang nói - Vậy thì con không thể giấu thầy được nữa. Chúng con phải làm kín đáo là bởi con lợn ấy chúng con bắt trộm của người thôn dưới.
 
Lý Trần Quán rất đỗi ngạc nhiên, hỏi:
 
- Con là ông tuần huyện, lại là thầy giáo sao lại có thể làm cái việc của bọn thất phu như thế được? Thầy không tin!
 
- Chuyện thật mà thầy! - Trang nói - Băng trộm này do con làm thủ lĩnh. Để sống được ở cái thời buổi loạn lạc, đói kém này, ban ngày con làm thầy, con mang cái danh ông tuần huyện, ban đêm con phải làm kẻ trộm. Nhưng dăm bữa nửa tháng chúng con mới làm một vố để có thịt ướp muối ăn dần thôi. Chứ đêm nào cũng đi thì sa lưới bọn lính lệ, trương tuần như bỡn, thầy ạ.
 
Lý Trần Quán lúc này mới quan sát kĩ đứa học trò cũ. Y mập ra, cái đầu tròn múp, mặt nhờn nhẫy mồ hôi. Quán khẽ lắc đầu tỏ nỗi ngao ngán, thất vọng. Đoán được bụng thầy, Trang nói:
 
- Xin thầy chớ lo. Với con việc gì ra việc ấy. Cho dù lúc làm thầy, lúc làm kẻ trộm, con vẫn coi thầy là ân nhân. Con sẽ bảo vệ, che chở thầy!
 
Nghe Trang nói thế, Quán tạm yên lòng. Vả, trong lúc này Quán vẫn phải nhờ cậy Trang, đành phải chấp nhận vậy.
 
*
*     *
 
Tháng 5 năm Cảnh Hưng thứ 47 (1786), biết phủ chúa đàng ngoài đang rối ren, suy vi, quân Tây Sơn lấy cớ “phù Lê diệt Trịnh” tiến ra Bắc. Vua Lê Hiển Tông bàn với chúa Trịnh Khải mang quân ra chống đỡ. Nhưng bọn kiêu binh đã quen thói hưởng thụ, ham sống, rất sợ chinh chiến. Chúng nhao nhao kể công trạng, đòi tăng lương thăng chức. Nhưng tăng lương thăng chức xong chúng vẫn chây ì không chịu ra trận. Đám lính mới tuyển dụng thì vừa thoát khỏi nạn đói khủng khiếp từ các làng quê, người nào cũng lẻo khẻo, dặt dẹo như lũ cô hồn, không đủ sức múa một đường kiếm, đi một đường đao. Khi Quang Trung và Nguyễn Hữu Chỉnh dẫn quân vào đến Thăng Long, thì cả lính cũ, lính mới bỏ chạy tán loạn. Chỉ còn một số không biết chạy đi đâu thì ở lại với súy phủ. Chúa Trịnh Khải cưỡi voi, tay cầm cờ lệnh trực tiếp chỉ huy các tướng Hoàng Phùng Cơ, Đinh Tích Nhưỡng, Nguyễn Lệ… chiến đấu với quân Tây Sơn. Mặc dù khi ấy đội quân của chúa đã có hai khẩu súng thần công do người Hà Lan đúc, nhưng tinh thần quân sĩ bạc nhược, thần công trở nên vô dụng. Đội quân của chúa chỉ cầm cự được một lúc thì phải rút chạy.
 

Ảnh: Phạm Duy Tuấn 
 
Chúa Trịnh Khải xuống khỏi lưng voi, cởi bỏ hết triều phục thất thểu đi bộ theo bờ sông Nhị Hà đến làng Hạ Lôi, nơi mà ngài biết Lý Trần Quán đang ẩn lánh. “Ta bỏ kinh sư tức là đã bỏ nước. Nhưng trong tình thế này đành phải tự cứu lấy thân đã!”. Những ý nghĩ ấy cứ day trở suốt chặng đường chúa đi.
 
Lý Trần Quán không hề ngạc nhiên khi chúa xuất hiện một cách đường đột nơi làng quê nghèo xơ xác này. Nhìn chúa trong bộ thường phục lấm lem bụi đường, Quán biết được thế sự, hiểu rằng trong cơn bĩ cực chúa đang cần sự giúp đỡ của ngài. Quán đành phải nói dối tuần Trang:
 
- Quan lớn đây là tham tụng Bùi Huy Bích, một đồng sự thân tín của thầy!
 
Trang nhìn người khách với vẻ thăm dò, rồi khoanh tay cúi đầu nói:
 
- Dạ, con đã có hân hạnh được nghe thầy Quán giảng thơ của quan lớn ạ.
 
Trịnh Khải lúng túng chưa biết phải đáp lễ như thế nào thì Lý Trần Quán đã ý tứ nói:
 
- Quan tham tụng đi bộ đường xa đang mệt, chưa thể nói chuyện thơ phú được. Ngài về đây lánh nạn, có ý nhờ thầy trò ta giúp đỡ!
 
Trang nói:
 
- Vâng, xin mời quan tham tụng ở lại nhà đây với thầy trò con!
 
- Nhà con thì quá tốt rồi - Quán nói - Nhưng lại có lớp học, kẻ ra người vào cũng bất tiện. Con xem có chỗ nào kín đáo hơn không?
 
Trang ngẫm nghĩ một lát rồi bảo:
 
- Phía cuối làng, giáp với cánh đồng có ngôi miếu thờ thuỷ thần, con sẽ đưa quan tham tụng đến đó trí ẩn. Cơm nước hàng ngày gia đình con sẽ lo!
 
- Phải lắm - Lý Trần Quán nói, rồi quay sang Trịnh Khải - Ông tuần  Trang đây là học trò cũ của tôi. Bây giờ quan lớn cứ yên tâm đến ngôi miếu an toạ nghỉ ngơi. Ngày ngày thầy trò tôi sẽ ra đó hàn huyên với ngài!
 
Trang vốn dĩ là kẻ đa nghi, lại lăn lóc mưu sinh cùng đám du thủ du thực nên rất tinh quái trong cách nhìn nhận con người. Quan sát sắc mặt, cử chỉ của Lý Trần Quán và khách nói với nhau, y không tin khách là tham tụng Bùi Huy Bích. Sắc mặt ấy, điệu bộ ấy, giọng nói ấy chỉ có ở các đấng bậc vua chúa. Trên đường từ nhà đến ngôi miếu thuỷ thần, Trang và khách cứ bước lầm lụi mà không nói gì. Nhưng khi đến trước cửa miếu thì đột nhiên Trang dừng chân quay ngoắt lại chiếu cái nhìn sắc nhọn vào gương mặt nhợt nhạt của khách:
 
- Ngài nói thật đi, có phải ngài là chúa Đoan Nam vương Trịnh Khải không?
 
- Ta là tham tụng Bùi Huy Bích, thầy Quán của anh đã giới thiệu về ta rồi mà!
 
- Thầy Quán có dạy tôi về thơ Bùi Huy Bích - Trang nói - Vậy nếu ngài là Bùi Huy Bích thì hãy đọc một bài thơ trong Bích Câu thi tập của ngài để tôi xem có đúng không?
 
- Ta nghe ông Quán nói nhà ngươi là nho sinh mà sao cư xử, ăn nói thật hỗn hào! - Quên mất mình đang phải đi lánh nạn, Trịnh Khải nói bằng cái giọng hách dịch, khinh mạn thường có ở một quân vương - Ngươi lấy cái quyền gì để hạch sách ta? Ngươi hãy làm trọn bổn phận mà thầy ngươi giao phó đi!
 
- Nghe cái giọng của ngài đủ biết ngài chính là Đoan Nam vương Trịnh Khải - Trang nói - Tôi biết nhà Tây Sơn đang rất mạnh, vì họ có thủ lĩnh giỏi, ngài nên ra đầu thú thì hơn!
 
Bị kích vào nơi sâu thẳm tâm can, chúa không thể giấu nữa. Ngài to giọng:
 
- Phải, nhà ngươi đã đoán đúng! Ta chính là Đoan Nam vương Trịnh Khải. Vương triều ta đã yếm thế. Ta có chết cũng do mệnh trời. Nhà người đừng bao giờ xui ta ra đầu hàng lũ giặc cỏ Tây Sơn!
 
- Vậy thì chúng tôi sẽ nộp ngài cho họ!
 
Trang nhờ người gọi đám đồ tể đàn em ra sân miếu, lấy dây thừng buộc hai cổ tay Đoan Nam vương Trịnh Khải dắt đi. Được tin báo, Lý Trần Quán chạy ra đã thấy nhóm đồ tể dắt Đoan Nam vương ra khỏi sân miếu. Chúa đi đường làng không quen, cứ bước sấp bước ngửa như người lên đồng. Quán nhìn cảnh đó lòng như sát muối. Chạy vượt lên trước, Quán quỳ xuống chân chúa vái lạy:
 
- Chúa thượng bị làm nhục thế này tà tội của thần! Thần đáng tội chết!
 
Chúa nói:
 
- Khanh đứng dậy đi. Lòng trung của khanh trẫm đã tỏ. Nhưng có lẽ số trẫm đã tận!
 
Quán đứng dậy quay sang nói với Trang:
 
- Thầy từng mang cả sách Ích trí để dậy con: Quân thần bất tín, quốc bất an. Phụ tử bất tín, gia bất mục. Huynh đệ bất tín, tình bất thân. Bằng hữu bất tín, giao dị sơ… con còn nhớ không?
 
- Thưa thầy, con vẫn nhớ - Trang đáp - Con dịch để thầy nghe: Vua tôi chẳng tin nhau, nước chẳng yên. Cha con chẳng tin nhau, nhà chẳng hoà thuận. Anh em chẳng tin nhau, tình chẳng thân. Bầu bạn chẳng tin nhau, nghĩa kết giao dễ sơ.
 
- Đạo quân tử vẫn thuộc làu làu, sao con nỡ bắt chúa mang đi nộp cho giặc?
 
- Nhưng khi quân Tây Sơn đến đây bắt con, thầy có mang đạo ấy ra xin tha cho con được không? Sợ thầy không bằng sợ giặc! Yêu chúa không bằng yêu thân mình. Xin thầy nhớ cho! - Trang nói thản nhiên đến lạnh lùng.
 
- Ta thực lòng không tin con lại có những lời lẽ khinh bạc như thế!
 
- Con biết, việc con mang chúa đi nộp cho quân Tây Sơn hay cả việc con đi kiếm ăn ban đêm, đều là trái với đạo lý thầy dạy. Nhưng thời buổi bây giờ là thế. Nghèo hèn sinh đạo tặc, thầy chả từng khuyến dụ như thế đó sao! Tình trạng đói kém mà còn kéo dài thêm thì cả nước thành đạo tặc chứ chẳng riêng mấy đứa chúng con.


Ảnh: Phạm Duy Tuấn
 
Nỗi đau thống khổ làm Lý Trần Quán khuỵu xuống giữa đường, chỉ còn biết ngẩng mặt cầu trời. Trang và đám đồ tể vẫn cứ đi. Tay chúng cầm sợi dây thừng nối với hai tay chúa. Trời tháng sáu nóng như thiêu đốt. Đi thêm một đoạn, gặp quán nước ven đường, chúa dừng lại, kêu khát nước đòi uống. Đám đồ tể tạm tháo dây thừng cho chúa. Chúa  ngồi xuống cái chõng tre, bưng bát nước trà xanh uống một nửa. Nhìn thấy con dao dùng để têm trầu, chúa cầm lên tự đâm thẳng vào cổ mình. Trang nhìn thấy vội giằng dao rút ra. Chúa thò tay vào vết đâm móc cuống họng mình lôi ra ngoài, máu tuôn như suối. Chúa còn tự bưng bát nước trà xanh uống cạn rồi mới gục xuống tắt thở. Trang và đám đồ tể bỏ xác chúa vào cái võng đay, xỏ hai đầu võng vào một cái đòn tre khiêng đi. Đây là cái cáng chúng vẫn dùng để khiêng lợn mỗi lần đi ăn trộm. Khiêng chúa xuống đến gần chùa Quán Thánh thì gặp quân Tây Sơn, chúng nộp xác chúa cho họ. Quân Tây Sơn bêu xác chúa trước cửa Tuyên Vũ cho dân chúng nhìn thấy. Hai ngày sau họ mới hạ xuống khâm liệm, chôn cất.
 
Cái dáng mảnh khảnh của Lý Trần Quán bước nghiêng nghiêng sấp ngửa trở lại sân miếu thủy thần. Quán gặp một người quen biết cũ, từ lần trước về đây dạy học. Quán nói với ông ta:
 
- Tội của tôi hai lần đáng chết. Tội bất trung với chúa là một. Tội dạy học trò trở thành kẻ lưu manh phản phúc là hai - Quán thò tay vào túi móc ra một xấp tiền đúc bằng đồng đưa cho ông ta - Tôi nhờ ông mua hộ một cỗ quan tài, mấy vuông vải trắng…
 
Nắm bắt được ý đồ của ông thầy giáo, người đàn ông nói lời can ngăn tha thiết. Nhưng ý Quán đã quyết, ông ta đành làm theo.
 
Hôm sau người đàn ông cùng mấy người hàng xóm khiêng chiếc quan tài về. Theo sự chỉ dẫn của Lý Trần Quán, họ khiêng quan tài ra khu ruộng đằng sau ngôi miếu. Họ đào huyệt rồi hạ quan tài xuống. Lý Trần Quán chắp tay vái lạy cám ơn, rồi tự cuốn vải trắng khắp người, chui vào quan tài nằm xuống. Sực nhớ ra điều gì, Quán nhổm dậy nói với mọi người:
 
- Còn điều này, nếu con cháu tôi đến đây nhận mộ, nhờ quý ông quý bà nói hộ với chúng rằng, hãy khắc hai câu này lên gỗ treo hai bên bàn thờ tôi: Tam niên chí hiếu dĩ hoàn. Thập phần chi trung vị tận. Thôi, mọi chuyện cần nói tôi đã nói. Bây giờ cảm phiền bà con lấp đất xuống huyệt giúp tôi.
 
Nói xong, Quán nằm xuống tự đưa tay kéo sập nắp tấm thiên. Mọi người vừa lấp đất xuống huyệt vừa khóc thương thân phận ông thầy giáo từng có những năm dạy học tận tình tại làng quê của họ, từng thi đỗ tiến sĩ rồi làm đến chức Tư nghiệp Quốc Tử giám ngoài Thăng Long. Lấp đất đầy huyệt, họ còn đắp cao lên thành ngôi mộ, xoa vuốt cho vuông thành sắc cạnh, rồi đặt lên đó bát hương nghi ngút khói. Sực nhớ mấy câu Quán dặn, một người nói:
 
- Tam niên chí hiếu dĩ hoàn nghĩa là Đạo hiếu ba năm đã trọn. Thập phần chi trung vị tận nghĩa là chữ trung mười phần chưa xong. Thấm thía quá. Hiếu với cha mẹ thì ông giáo có thừa. Nhưng đúng là trung với vua thì chưa trọn thật.
 
- Nhưng tôi nghĩ - một người khác nói -  Ông giáo tuẫn tiết còn vì gã học trò phản phúc nữa, sao không có câu nào nói về việc này? Ngài chưa kịp nghĩ ra hay ngài không muốn khắc điều này lên gỗ treo trước bàn thờ, e rằng sẽ gợi nỗi ô nhục cho tổ tiên và con cháu ngài chăng?
 
Mọi người ngớ ra trước sự phát hiện ấy. Nhưng không ai giải nghĩa cho thật rành rẽ và thuyết phục cả. Có lẽ đó cũng là bí mật của một kiếp người.
 
 
Hà Nội, hè 2011 - thu 2012
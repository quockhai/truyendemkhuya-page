Khi những cơn gió nam non đầu hạ đổ về làng cát thì những đám muống biển mọc cụm thành luống, thành dề đã bắt đầu trổ hoa. Trên từng doi cát trắng phau phau sắc hoa tím bật lên trong cái nắng hanh hao.

Bọn trẻ con một thời dưới chân núi Đá Bia đầy biển mặn cùng nắng cháy cứ rủ nhau vượt qua từng bãi cát để bước lên đám muống biển. Nơi ấy có những bầy dông tránh nắng ban trưa, lũ còng gió đào hang buổi sáng, lũ thỏ rừng kéo bầy đi tìm đọt muống biển buổi chiều. Cả bọn tung tăng đùa nghịch, đào còng, rượt dông, bẫy thỏ rừng bên màu tím của hoa muống biển. Con gái gom từng chùm hoa để xâu vòng đặt lên đầu, choàng lên cổ làm duyên và ngóng mắt về những con sóng dào dạt xô bờ tìm lũ con trai đang ngụp lặn lấy vỏ ốc, vỏ hàu rồi la ré khi cả bọn ném từng vốc cát, từng chùm hoa muống tung về mặt sóng. Từng dề từng dề tim tím ấy cứ trôi nổi dập dềnh, dập dềnh. Mỗi dề hoa muống biển kia như định hình cuộc sống trôi nổi, nhọc nhằn của người dân vùng cát.

 
 Minh họa: Thành Chương

Họ có ba người, đang ngồi trên một doi cát cong cong hình vạt nón, phía sau là rừng dương trải dài ngút tầm mắt và phía trước là biển cũng xanh ngút tầm mắt với những con sóng miên man. Ba người ngồi khá lâu trước các vạt muống biển đang ra hoa từ lúc bóng nắng đổ trên ngọn dương đến lúc bóng nắng chếch dần sang doi cát áp biển. Từ lúc có một màu tím rực như rải thảm miên man hoa nở đến lúc những nụ hoa bắt đầu úp cánh với màu trắng đặc trưng khi dần về đêm.
- Uống nghe Xuân…

Người đàn ông có mái tóc bạc nhất trong ba người đang nghiêng chai rót rượu ra ly cho người đối diện.
- Lại vào mùa hoa muống biển, lại về với một ngày...

Người đàn ông tên Xuân lẩm bẩm rồi với tay vào ly rượu và ngửa nhẹ cổ:
- Uống nghe Nam và rót cho Lâm.

Đó là kiểu uống của người miền này, cứ xoay vòng khắp lượt như gió nồm nam đổ từ biển vào, cuộn tròn đập qua mũi Điện rồi thốc tháo quay ngược sang Vũng Rô cuốn tung bụi cát vừa ngột ngạt oi nồng, vừa hanh hao rát bỏng. Gió cứ đến và đi, hoa muống biển cứ mùa gọi mùa qua bao tháng năm đổ xuống dãy Thạch Bi Sơn.
- Ngày mai đến thăm Hiên trước há? Xuân bảo hai người.
- Ừ, Hiên trước rồi sang chú Sáu - Lâm thốt nhỏ, tiếng chao trong gió.
- Mau thật, ta càng lúc càng già, nghĩ đến là như mới ngày nào - Nam nheo mắt nhìn biển.

*
*     *

Mới ngày nào… Những năm cuối thế kỷ trước vùng đất này còn hoang sơ, dân cư thưa thớt, thôn này qua qua thôn nọ cách doi cát dài hay những con lạch nước váng phèn cùng dứa dại, những bụi lông chông, hoa duối dẻ chen trong gai bàn chải. Ngày ấy nhiều nơi đang giao tranh dữ dội nhưng nơi đây vẫn im lặng. Cái im lặng của núi lửa trước lúc vặn mình tuôn trào nham thạch.

Bộ tứ của họ có ba trai một gái. Hiên, cô bạn nữ ở thôn Bắc, cùng học ngôi trường tiểu học cộng đồng dành cho xã vùng heo hút, nghĩa là cách xa cả giờ đồng hồ đi bộ qua từng doi cát, từng con lạch nhỏ nước chảy như cọng chỉ, qua từng ngọn đồi đá đầy dây rừng và những con dông sặc sỡ sống trên cát. Mùa nắng thì đỡ dù nóng nung người, nhưng mùa mưa gió tạt ngang, nước tạt dọc là người hay bị chúi ngã lăn lông lốc như những bụi lông chông đang mùa thiên di xoay tròn trên mặt đất.

Bộ tứ lớn lên lầm lũi qua từng mùa. Mùa đi núi trên dãy Thạch Bi Sơn hái hạt ư, chặt đót quấn chổi, hái hạt đát và tìm sim trên những ngọn đồi trọc; mùa đi biển vớt sứa, quây ruốc cận bờ vì khi ấy không nhà nào có ghe thuyền lớn đi khơi. Xa kia là biển, gần hơn là đồng với những mảnh ruộng trên cát, ruộng lúa toàn phèn hứng nước trời mỗi năm một vụ còn lại dành cho cát trôi, cát lấp, con cua con rạm nhỏ như ngón tay nhưng chiếc mai tím sẫm, dày cộp luôn vùi mình trong cát. Sống đã quen, khổ đành chịu, vì cũng chẳng còn đi lối nào khi chiến tranh cứ mỗi lúc đến gần.

Khổ nhất là vào đận lấy nước, con rạch dài như cọng chỉ có vài vũng nước mà tranh giành nhau dù chơi thân với nhau. Để tránh cái nóng ngột ngạt oi bức nên phải lấy nước vào ban đêm. Vui nhất là những mùa trăng,  đi dọc theo triền cát ra ruộng thì chuyện kể mãi cũng hết, vậy là nghịch cát. Ngày thì cát bay cát nhảy còn đêm thì cát cười cát réo, cát chạy tung tăng, cát lăn cát múa… Tiếng cười con trai con gái khanh khách, tít mù. Nhà Hiên toàn phụ nữ, ruộng lại xa lạch nước, mỗi lần lấy phải vục hơn ba ngàn gàu đếm nên luôn về muộn và Xuân luôn là kẻ ở lại phụ giúp. Trong ánh sáng mờ mờ của sao trời, Xuân thấy bàn chân Hiên kề cạnh, đôi chân trần trắng lôm lốp kia khác với con gái vùng cát và hơn ba ngàn gàu sòng đi qua, Xuân luôn nhận từ tay Hiên cánh hoa dủ dẻ vàng ủ trong túi áo để cho làn hương thêm đậm. Một lần trên đường về trời đầy sao, Hiên bảo “Xuân sẽ rời làng đi xa chứ?”, Xuân cười “Ừ, sẽ đi, nơi này khổ quá…” - “Bỏ Hiên ở lại nơi này một mình à?” - “Còn Lâm, còn Nam còn mọi người” - “Nhưng…” Hiên bỏ lửng câu. Xuân nghe tiếng gió tạt ngang réo rắt. Đang vào mùa gió nam cồ...
- Thời gian đi qua và mọi việc không tưởng được - Xuân thở dài, tay nhón lấy cọng khô mực và ngửa cổ dốc cạn ly rượu từ tay Nam. Nam cười nhẹ:
- Ông là người hạnh phúc nhất, còn tụi tôi và cả Hiên nữa phải chịu….

Ừ. Cả Hiên nữa. Bắt đầu khi cả bọn lên cấp hai vào lớp đệ thất phải ra trường quận thì chỉ còn Xuân đi học. Mỗi cuối tuần trở về làng Cát, Xuân thấy cả bọn như đã lớn và Hiên đẹp ra. Chiếc áo bà ba cổ trái tim đã lộ rõ khuôn ngực phập phồng, mắt lóng lánh nước, đôi môi cắn chỉ hồng rực. Hiên luôn đợi Xuân nơi ngã ba làng cát mỗi khi anh về. Cả hai đi dọc theo đám muống biển, Xuân tìm một đóa đẹp nhất cho Hiên và nhận từ tay Hiên chùm hoa dủ dẻ, anh hỏi Hiên ai đã thay anh tát nước gàu sòng cho vào ruộng lúa, Hiên lắc đầu nhưng Xuân vẫn biết người đó là Nam.

 
  Ảnh: Minh Khuê

Cha Xuân ngày mỗi trăn trở trên cánh võng và mỗi lúc nghe đài nhiều hơn, cứ mỗi chập tối là ông vào Vũng Rô. Bản vọng cổ phương Nam ông ưa thích “Từ là từ phu tướng… bảo kiếm sắc phong lên đàng… người đi lắng trông tin nhạn…” đã không còn và tin chiến sự nhiều hơn. Năm lên đệ ngũ, ngày Xuân về, ông bảo, tối nay cùng cha qua phía Thạch Bi Sơn khi má lặng lẽ dọn cơm. Đêm ấy, Xuân gặp mọi người trong gộp đá và có cả Nam, Lâm. Mới biết cha là người đàng mình, Nam cùng Lâm đã là du kích và Hiên là cô giao liên. Chú Chín, người từng xuống nhà Xuân trong mỗi đợt gió nam thổi về đầy se sắt bảo mọi người “Chúng ta luôn mong ngóng mọi sự và chờ đợi khi có dịp. Xuân đã lớn, nếu ở lại học tiếp sẽ bị bắt lính. Tổ chức sẽ bố trí để Xuân vượt Trường Sơn ra Bắc để đưa tàu về, còn mọi người ở lại làm việc của mình... Riêng Lâm, sẽ phân việc khác nên không còn ở đây nữa”.

Những ngày cuối cùng trên làng Cát, bộ tứ gặp nhau, ngượng nghịu. Xuân biết, Nam và Lâm điều yêu Hiên. Đêm đêm khi mọi người đã ra về, Xuân đưa Hiên dọc theo dải cát, ánh trăng hắt lên mặt cát muôn vạn hạt sáng lung linh, tiếng gió rì rào cùng tiếng sóng biển vỗ cồn cào, Xuân nắm tay Hiên đầy ấm nóng khi Hiên hỏi “Anh sẽ về lại chứ?”, Xuân gật đầu nhưng vẫn bảo “Anh thì xa, Nam và Lâm thì gần… Em nên chọn…”. Hiên lắc đầu “Em sẽ chờ anh, sẽ chờ…”. Vạt hoa muống biển đã chuyển sang màu trắng như một dải tinh khôi lành lạnh trong trăng đêm, cả hai oằn mình trên vạt hoa ấy đến khi tiếng vạc kêu sương trôi ngang trên bầu trời sắp rạng. Xuân bảo “Mình về đi Hiên, chiều mai anh đi”. Hiên gài lại vạt áo, với tay ngắt chùm hoa muống biển “Anh mang chuỗi hoa này đi, sẽ nhớ về làng cát và em…”

*
*     *

- Uống đi Nam, mỗi năm mới có dịp về - Xuân chuyền ly rượu qua Nam lúc Nam đang dõi mắt về dãy Thạch Bi Sơn đang tím sẫm màu chiều, cơn gió nồm nam cuối ngày thổi qua lành lạnh.

Nam trở thành trung đội trưởng du kích làng Cát khi Xuân đã đi gần năm, Lâm vào Nam tham gia vào một lớp học gì đó của địch. Chỉ còn Nam và Hiên gặp nhau khi chiến sự mỗi lúc một gần vùng này. Làng đã có những đồn bót và bóng lính sớm chiều ẩn hiện trong dãy thép gai, đêm đêm đã có những đốm hỏa châu lơ lửng bầu trời và từng tràng đại liên bắn vu vơ. Khu gộp đá dưới chân dãy Đá Bia đang là căn cứ phía nam của tỉnh nên giao liên đưa người về ngày mỗi nhiều và Hiên là một giao liên thông thạo địa hình nhất. Những lúc yên tĩnh, cả hai đi dọc triền đá bên bờ sóng vỗ để kể những chuyện ngày xưa khi còn có Xuân và Lâm. Đây là lúc Nam cận kề Hiên nhất vì không còn những người bạn nơi này. Bao lần Nam nắm tay Hiên nhưng Hiên vẫn nhẹ nhàng duỗi ra và bảo: “Em nhớ Xuân, giờ này Xuân ở đâu hả anh?”. Nam im lặng, một chút dỗi thoáng qua, nhưng vẫn bảo “Xuân sẽ về…”

 Xuân đã về thật dù thời gian dài đến ba năm. Chuyến tàu không số đầu tiên cập bến Vũng Rô êm thấm, khi bót Bơ Tý trên dãy đèo Cả vẫn im lìm trong sương phủ. Và hàng đã chuyển lên bãi giấu dưới những gộp đá. Xuân nắm tay Nam lắc mạnh khi gặp “Tất cả đều khỏe hả?” - “Ừ, khỏe!”. Thấy Xuân nhìn quanh, Nam cười: “Hiên sắp về, đang đưa cán bộ trên khu xuống Đá Bia” - “Mình ngại cô ấy không muốn gặp, lâu rồi…”. Xuân nhìn người bạn quê nhà nay rắn rỏi hẳn với bộ đồ bộ đội, vai choàng khẩu AK chân dép lốp khi anh vẫn quần áo dân biển để ngụy trang. Nam bảo: “Không, Hiên vẫn nhớ, nhắc ông mãi”. Chiều ngày sau, khi mặt trời sắp lặn để chuyến tàu mở đường đầu tiên sẽ rời bến, nơi bãi vắng chỉ còn những đám muống biển trổ hoa tím ngát theo từng vạt dài lắt lay trong gió thì Hiên mới gặp được Xuân. Cả hai ôm choàng lấy nhau, Hiên thở dồn, nói ngắt quãng “Anh lại đi nữa sao?” - “Ừ, anh phải quay trở ra vì cả tàu chỉ có anh thông thạo địa hình. Anh đi rồi trở lại...”. Những nụ hôn vội vàng, vòng tay cuống quít vội vã rồi cả hai chạy ra biển, sóng đập vào chân mát lạnh. Lúc Xuân nâng bổng Hiên lên mới chợt thấy mặt trăng đỏ ối nhô lên đường chân trời của đêm trăng mười sáu, anh vội nói: “Chờ anh về nghe Hiên…”. Xuân lao về chiếc tàu đang đợi phía bãi Môn, lúc quay nhìn lại, anh vẫn thấy bóng Hiên quỳ trên cát trắng úp mặt vào đôi tay, đó là lần cuối cùng…

 
  Ảnh: st
Trăng đã lên phía vòng cung biển, đỏ sẫm và to tròn vành vạnh, gió cũng ngừng thổi, phía xa kia dãy Thạch Bi Sơn đen dài chìm vào màn đêm khi mũi Điện đèn biển đã bật sáng hai chớp một tắt dẫn luồng cho các tàu qua lại. Lâm vỗ vai Xuân:
- Tất cả đã qua, mỗi năm mình gặp nhau vài ngày là vui rồi.

Nam nhìn cả hai, bảo:
- Câu chuyện bọn mình kể đi kể lại đến thuộc làu nhưng vẫn thấy thiếu…

Ừ thiếu. Mỗi năm góp nhặt một ít. Chuyến ra Bắc của Xuân đã khai thông luồng lạch nhưng anh được chuyển công tác qua hàng hải và sang Liên Xô học tiếp. Nơi quê nhà, chuyến tàu thứ hai an toàn, đến chuyến thứ ba thì xảy ra sự cố, một sự cố trên đường mòn của biển làm cả quân địch rúng động khi phát hiện đường vận tải từ Bắc vào Nam là có thật. Báo đài quốc tế đưa tin con đường trên biển của Bắc Việt đã chi viện cho chiến trường miền Nam biết bao vũ khí, lương thực. Và một đội quân hỗn hợp của Mỹ, của lính đánh thuê Nam Hàn và quân đội Việt Nam Cộng hòa cùng một lượng lớn phi pháo, máy bay đổ quân càn đi, quét lại vùng đất toàn cát có những bụi gai bàn chải, duối dẻ, xương rồng. Những ô ruộng ăn nước trời với từng con lạch nhỏ. Những bánh xích xe bọc thép đổ về xáo tung tất cả, lùa đi những người dân từng sống nơi hẻo lánh nhất.

Nam chỉ huy cả trung đội du kích cùng bộ đội tăng cường ở khu về để chuyển hàng từ tàu không số đã chống trả. Cuộc chiến không cân sức nhưng các gộp đá đã thành một chiến lũy vững chắc giúp anh ngăn từng bước tiến của địch. Một ngày, hai ngày đầy lửa khói, hàng đã thông, tàu không số đã đặt bộc phá đánh đắm, tất cả đang dần rút thì bị lính Nam Hàn chặn đường. Vậy là lại đánh. Từng tràng đạn bay xoe xóe đập chan chát trên các gộp đá, lựu đạn cong vòng ầm ào, M.79 cốc giòn giã, tiếng rú, tiếng rên ngập ngụa trong khói đến chiều tối. Khi mọi người về cứ an toàn thì tin giao liên báo đến, ngay ngày hôm sau, bọn địch đã lùa dân làng tập trung ra trảng cát, và  từng tràng đại liên quét ngang quét dọc, thây người đổ xuống trên cát, máu hòa trong cát, hòa vào gió nồm nam thông thống đổ về. Từng dải hoa muống biển nhuốm màu đỏ bầm trong nắng trưa chao chát. Trong ấy có gia đình của Xuân, của Nam, của Lâm và cả của Hiên. Ngày của cả làng làm đám cho làng.

Nam và Hiên lên cứ, khi vùng đất cũ đã bị san ủi để xây dựng các đồn bót giặc, cho chi khu thép gai giăng dày đặc và sân bay Đông Tác mọc lên với Vũng Rô là cảng quân sự mà tin tức của Xuân và Lâm hầu như không hề được biết.

*
*     *

Lâm ngồi nhìn biển tai lắng nghe từng đợt sóng dội bờ như dội vào khoảng không ký ức. Ngày ấy…
- Thiếu úy Lâm, lên phòng an ninh gặp đại úy Bảo!

Sĩ quan trực ban đi ngang phòng gọi Lâm khi anh đang nghiên cứu đống hồ sơ của nhóm điệp báo vừa gửi. Lâm đứng lên sang phòng trưởng ban an ninh, lúc dập gót đứng nghiêm trước gã đại úy thì đã nghe truyền lệnh:
- Thiếu úy xuống phòng thẩm vấn xem lại con nữ Việt Cộng vừa đưa về hôm qua, hình như nó ở vùng của thiếu úy.

Lâm quay người đi dọc hành lang, bên ngoài khoảng sân rộng của tỉnh đường trồng nhiều cây dâu da và bàng tỏa bóng rợp mát, phía lô cốt thấp thoáng chiếc mũ sắt của tên lính gác và có tiếng máy bay trực thăng đang từ phía biển bay vào. Anh đi xuống tầng hầm đến phòng thẩm vấn. Một người phụ nữ ngồi rũ rượi trên ghế, mái tóc xõa tung bê bết máu đang bị ánh đèn pha chiếu thẳng vào mặt. Trời ơi Hiên! Lâm thốt nhỏ khi nâng khuôn mặt tím bầm lên và khoát tay cho tên trung sĩ ra ngoài.

Chưa khi nào Lâm phải suy nghĩ đến nát óc nhằm tìm giải pháp thật an toàn cho cả hai, chỉ cần sai sót sẽ lãnh hậu quả khôn lường. Hiên dần tỉnh sau một ngày đưa sang khu giam giữ và Hiên ngơ ngác nhìn Lâm trong quân phục ngụy...
- Thưa đại úy, người phụ nữ này ở vùng quê của tôi và ngày xưa tôi rất yêu cô ấy.

Đại úy Bảo gật gù:
- Vậy à, hay thật, cậu được làm vụ này nhưng nhớ cho rằng cần phải nhanh, có kết quả sớm. Nếu không, đừng trách…

 
 Ảnh: Thái An

Lâm đưa Hiên về căn nhà nhỏ của mình trong thành phố để chăm sóc. Ban đầu, Hiên từ chối nhưng sau thì im lặng chấp nhận. Lâm kể cho Hiên nghe về những kỉ niệm một thời trên vùng cát và bộ tứ ngày trước. Và anh cũng nói mình từng yêu Hiên, cho đến hôm nay vẫn vậy nhưng Hiên khinh bỉ lắc đầu “Tôi là người của Xuân, dù anh ấy không về…”. Lâm chỉ còn biết ém một tiếng thở dài nhè nhẹ. Lâm đã biết Hiên ốm vật vã bởi ngã nước do sốt rét rừng và những ngày thiếu thốn khi địch lợi dụng mùa khô mở các đợt càn quét chặn đường về hậu phương; lần đi mở lối mới, cả nhóm Hiên lọt vào vòng vây và Hiên bị bắt đưa lên trực thăng thả về Đông Tác để thẩm vấn. Anh ứa nước mắt bởi những lúc mê sảng, Hiên luôn gọi Xuân hay Nam mà quên hẳn Lâm. Hiên không biết rằng, chính anh là người báo tin về cứ để tổ chức kịp thời chuyển hàng khi chiếc tàu không số bị kẻ chỉ điểm trước lúc viên trung úy phi công lái máy bay vô tình phát hiện khi ngang Vũng Rô.

Hai tháng sau, Hiên lành thương và đẹp hẳn. Thời gian ấy Lâm giữ bề ngoài im lặng trước đại úy Bảo nhưng bên trong thì lao lung tìm cách cứu Hiên. Lâm biết Hiên sắp phải nhận những nhục hình khủng khiếp rồi thả lên hàng rào thép gai với tội vượt ngục….

Và ngày Lâm nhận lệnh đưa Hiên vào khu thẩm vấn đặc biệt để trực tiếp hỏi cung thì đã xảy ra chuyện. Chiếc xe chở Hiên bị ném lựu đạn phía trước cùng viên đạn găm thẳng vào gáy gã lái xe còn Lâm thì báng súng đập cho bất tỉnh. Ngay lập tức Lâm vẫn bị lột lon tống vào quân lao, tháng sau, do không có bằng chứng kết tội, Lâm bị điều ra bắc Bình Định rồi lên Phú Bổn, nơi chiến sự nóng nhất.

Đêm đã dần trôi, trên bầu trời sao mai đã hiện, tiếng vạc kêu sương phía dãy Thạch Bi Sơn vọng dần đến chỗ ba người đang ngồi im lặng với bờ vai, mái tóc phủ tràn hơi sương và hơi nước biển bám đầy… 

*
*     *

Buổi sáng, cả ba đi về phía nghĩa trang của xã. Biển phía xa xanh thẳm và không gợn sóng. Dãy Thạch Bi Sơn trước mặt nhận tia mặt trời sáng lấp lánh, vạt hoa muống biển dưới chân đã bừng nở cho một màu tím ngát. Xuân hái một bông đặt lên mộ Hiên khi Nam và Lâm đang cúi mình cắm hương cho từng ngôi mộ phía xa. Hiên ngã xuống trước ngày giải phóng làng Cát một năm, khi về lại nơi này và vướng phải mìn địch cài bảo vệ căn cứ sân bay Đông Tác. Nam đưa Hiên ra biển lần cuối, trong nhịp sóng vỗ của đêm trôi miên man, giọng Hiên đứt quãng “Em xin lỗi Nam… nhớ anh Xuân… và tội cho Lâm, tụi mình… ở làng…”

Cả ba đã kết xong vòng hoa muống biển nhắm hướng sóng vỗ, bước tới. Mùa này, những chiếc thuyền của làng đã ra khơi xa, chỉ còn lũ trẻ với tiếng cười trong vắt hòa vào nắng ban mai chiếu trên từng đợt sóng. Lâu đài trên cát, công chúa, hoàng tử, bầy còng, lũ dông… vẫn tái hiện trở lại như thời của ba người. Và hoa, hoa muống biển vẫn cứ tim tím, tim tím long lanh khoe sắc trong trời ban sáng, ban trưa, ban chiều có gió, có sóng dưới chân Thạch Bi Sơn này. Vòng hoa ba người vừa đặt vào biển xoay tròn, xoay tròn. Màu tím yêu thương đang tràn ra, tràn ra theo sóng nước…

Trại sáng tác Văn nghệ Quân đội
Phú Yên 4-2012
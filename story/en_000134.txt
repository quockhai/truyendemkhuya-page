Sau kỳ xả hơi cuối tuần tại khu nhà nghỉ trên núi, nhóm chúng tôi trở về thành phố. Cơn mưa kéo dài suốt đêm qua đã biến đoạn đường trước mặt thành một khúc sông, trong khi xe hơi lại không thể lội nước như ca nô được… Bằng bất cứ giá nào chúng tôi cũng phải có mặt ở sở làm đúng giờ quy định. Kỷ luật lao động là vậy.
- Tớ biết một lối khác - sau rốt anh bạn đồng hành Ivan lên tiếng - Tuy hơi lòng vòng một chút, nhưng bảo đảm hoàn toàn khô ráo…
   Xe chúng tôi ngoặt theo hướng Ivan vừa chỉ. Chậm chạp bò lên một đoạn dốc.
- Qua đỉnh đồi là tới một ngôi làng - Ivan tiếp tục - Ở đấy hiện chỉ có ba nhân khẩu gồm hai phụ nữ và một đàn ông…
- Nghĩa là hầu như trống vắng? - tôi mạo muội xen vào.
- Đúng vậy. Nhưng phong cảnh trữ tình lắm. Ngôi làng cổ có tuổi thọ tới vài thế kỷ. Rất nhiều đoàn làm phim đã đến tá túc tại đây, cả cánh họa sĩ mê thiên nhiên cũng vậy… Còn thường xuyên hiện diện chỉ có ba người duy nhất, cũng có tuổi cả rồi. Họ sống chủ yếu bằng nghề chăn nuôi, riêng củi trong rừng có đun cả đời cũng không hết.
Bánh xe lăn tới gần ngôi nhà đầu tiên. Tường cao cổng kín xây đá tảng nhưng chẳng thấy ai. Nhà kế tiếp lá cây phủ đầy sân vườn chứng tỏ không có người ở… Con đường độc đạo thoai thoải xuyên qua những khu nhà rêu phong ẩm ướt, mái ngói mốc xì lâu ngày không có dấu tích đảo lại… Chúng tôi quyết định dừng xe hòng thám hiểm một chốn quá đỗi hoang sơ giữa thời buổi văn minh náo nhiệt này. Cô bạn thân Miroslava sánh vai cùng Ivan đi dọc theo đoạn phố chính lát đá nham nhở; còn tôi và Bobi tốc ngược lên mạn sườn đồi, hướng tới ngôi nhà có khói bốc lên chứng tỏ có người cư ngụ. Bầu không khí u tịch lan tỏa khắp nơi… Bất thình lình có tiếng ai đó vọng tới từ bên kia hàng rào. Một giọng phụ nữ đang chì chiết:
- Cái con mụ nhẹ dạ ấy, dạng đàn bà quá ư dễ dãi!... Nó lại khiến đầu óc ông lú lẫn chứ gì? Quỷ tha ma bắt mụ ta đi cho rồi…
- Bà ám chỉ ai mới được kia chứ?- Giọng đàn ông lên tiếng.
- Còn ai vào đây nữa hả trời?! Con nào tối ngày tí ta tí tởn trước mặt ông ấy, còn cặp mắt ông cứ lúng la lúng liếng hết cả lên… Vậy mà cứ làm ra vẻ ta đây chỉn chu lắm.
- Ai, tôi ấy ư?... - người đàn ông lại lên tiếng giọng phân bua.
- Thôi đừng giả vờ nữa… Chẳng biết mắc cỡ gì sất! Xem thử có xứng làm gương cho xã hội noi theo không, ngần ấy tuổi rồi còn… “ham vui”.
- Trời ơi - người đàn ông thốt lên - Bà có điên không đấy? Ai lại đi bêu xấu thanh danh chồng…
- Điên hay không mặc tôi, nhưng nhất quyết tôi sẽ tố chuyện này với ngài xã trưởng, một khi người đại diện cho chính quyền có dịp ghé lại đây.
- Bà đừng có đem chuyện nhà ra tọc mạch người ta cười cho đấy.
- Thế hôm nọ ông làm gì mà thở hồng hộc trong chuồng ngựa thế?
- Tôi thay móng guốc mới.
- Còn con mụ kia biết gì mà cứ mãi sớ rớ xung quanh?
- Bà ta muốn học để tự đóng lấy khỏi phải phiền tôi.
- Vậy còn hôm gánh nước từ dưới suối lên thì sao?
- Bà ấy kêu mệt nên ngỏ lời nhờ…
- Lại còn chăm bẳm đi tưới cây hộ nữa?... Thể nào có ngày ta cũng bắt quả tang cặp tội phạm… Chưa kể cái lần…
- Xin bà chớ suy diễn lung tung. Dù sao người ta cũng là chỗ xóm giềng, lại côi cút góa bụa…
- Chính vậy mới khiến ông thêm lú lẫn đấy! Hãy giải thích giùm tôi xem cái lần mua hộ mụ ta cặp heo lứa nghĩa là sao?
- Bà im đi!
- Tôi không im. Hãy lý giải xem, tỉ như vừa sáng qua đây mới bảnh mắt ra đã sang nhà “nhân tình” thay yên cương mới…
- Bà ấy nhờ tôi mấy hôm nay rồi nhưng chưa rảnh, yên cương cũ rách tươm cả rồi.
- Sao không nhờ ai mà cứ chăm bẳm vào mỗi mình ông? Tối ngày tí tởn trong khi nom ông như một con gà trống hoa háu gáy… Coi chừng bị “sập bẫy” rồi hối không kịp đấy!
- Tôi chán nói chuyện với bà lắm… - người đàn ông như muốn kết thúc cuộc tranh luận.
Đột nhiên ông ta xuất hiện trước cổng nhà, vẻ ngỡ ngàng khi phát hiện ra chiếc xe đậu bên đường rồi dấn bước về phía chúng tôi. Đó là một cụ ông cỡ ngoài bảy mươi tuổi với tầm vóc nhỏ thó cùng mái tóc bạc trắng, vận đồ bảo hộ kiểu áo liền quần, bên dưới là đôi ủng cao ngang đầu gối. Cụ ông đánh tiếng hỏi xem chúng tôi làm gì ở đây, có phải như những người làm phim thường tới mua rau quả không. Sau khi vỡ lẽ rằng chúng tôi chỉ là khách qua đường và chỉ muốn thăm thú phong cảnh, cụ ông cười lớn với hàm răng rụng gần hết rồi bảo với giọng bông phèng:
- Lúc này tớ đang rảnh nên có thể làm hướng dẫn viên nghiệp dư được. Đây là nhà của vợ chồng tớ có từ thời cố nội. Chếch bên là nhà một bà góa, giờ này chắc bà đi vắng rồi… Đằng kia là nhà vợ chồng Bumbal chẳng có ma nào, bởi họ đã thành người thiên cổ. Cả gia đình Pisar cũng vậy, con cái họ đôi khi đảo qua chóng vánh rồi đi ngay. Không ai chịu ở lại lâu cả…

Minh họa: Nguyễn Đăng Phú
Im lặng một lúc như bồi hồi nhớ lại những người bạn đã khuất, cụ ông chợt lên tiếng:
- Sao các bạn không nán lại thêm chút ít. Tớ sẽ giới thiệu mọi người với bà xã. Bả có nhiều thứ thết khách lắm đấy - ông cụ vừa nói vừa đá lông nheo mắt trái vẻ hóm hỉnh.
Chúng tôi nêu lý do cần về gấp để đi làm nên không thể đáp ứng lòng hiếu khách từ cụ. Nghe vậy cụ tỏ ý thông cảm:
- Được thôi, nhưng nên nhớ là bất cứ lúc nào có dịp qua ngang đây cứ ghé nhé. Tớ luôn đợi ngày tái ngộ đấy.
Khi chúng tôi chuẩn bị ra xe thì xuất hiện một bóng người từ đằng xa đang tiến lại. Là một cụ bà khó nhọc chống gậy với chiếc áo khoác bạc phếch, đôi chân khẳng khiu xỏ trong cặp vớ bên ngắn bên dài. Vị chủ nhân hiếu khách hỏi thay lời chào người hàng xóm duy nhất:
- Bà lặn lội đi đâu về đấy?
- Không dám, tôi tranh thủ ra thăm huyệt mộ đào sẵn cho mình xem mưa có tràn nước vào không…
Thì ra đây chính là nhân vật “tình địch”, nguyên do của cuộc cãi vã lúc nãy.
Suốt chặng đường về cả nhóm chúng tôi sôi nổi tranh luận quanh đề tài tình yêu và sự ghen tuông, thì ra ghen là một bằng chứng của tình yêu. Còn yêu là còn ghen, cho dù đã ngấp ngé ngưỡng “gần đất xa trời”… 
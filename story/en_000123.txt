Cô gái có đôi mắt to màu xám. Anh chàng thì có đôi chân dài và mặc một chiếc quần ống loe.
Ông thẩm phán đã luống tuổi, trong đời mình ông đã từng chứng kiếm không ít những vụ ly hôn và giờ đây ông không hề muốn cái gia đình trẻ này bị tan vỡ.
- Vì sao anh chị lại muốn ly hôn? - ông thẩm phán hỏi.
- Bởi vì anh ta chẳng thấy gì hết ngoài bóng đá cả - cô vợ nói sau khi đưa cặp mắt phẫn nộ về phía chồng mình.
Ông thẩm phán liếc nhìn đôi vợ chồng trẻ với vẻ thông cảm.
- Anh chị lấy nhau khi nào? - ông hỏi.
- Ông cứ hỏi anh ta mà xem, ngay cả điều này chắc là anh ta cũng chẳng còn nhớ được đâu… - Trong mắt cô đã rơm rớm giọt lệ.
- Lại bắt đầu rồi đây! - anh chồng thở dài - Sao lại không nhớ chứ! Vào chính ngày hôm đó đội bóng chúng tôi đã thắng đội "Spartac" với tỷ số 1:0 đấy.

Thẩm phán chăm chú nhìn người chồng, sau đó lại quay sang cô vợ:
- Anh chị có con chứ?
- Vâng, một cháu trai… - Gương mặt cô thoáng chốc trở nên rạng rỡ - Nhưng ông cứ hỏi anh ấy mà xem. Không lẽ anh ta còn nhớ là mình lại có một đứa con trai nữa sao?
- Lại thế rồi - người chồng cau mày - Sao tôi lại không nhớ là con trai chúng tôi sinh vào tháng 11 cơ chứ! Vào ngày hôm đó chúng tôi đã chiến thắng vang dội trước đội "Dinamo" Moskva với tỷ số 11: 5. Thế mới gọi là đá bóng chứ!
- Anh chị đã cãi cọ với nhau lần đầu tiên vào khi nào?
- Bảy năm trước đây ạ. Thật là một năm khủng khiếp. Đội chúng tôi đã bị thua "Zenit" 1:2, mặc dù chúng tôi vẫn còn những cơ hội để về nhất. Thế mà bỗng nhiên đội "Dinamo" Moskva lại dẫn trước chúng tôi 2:1, cả hai bàn thắng đều do công của Trislenco, rồi sau đó chúng tôi đã thủ hòa với đội "Thợ mỏ"…
- Ông nghe thấy rồi chứ? - cô vợ nói đầy hàm ý và đưa khăn tay lên chấm mắt.
- Thế anh chị đã làm quen với nhau như thế nào? - thẩm phán hỏi.
- Cái ngày hôm ấy thật là đáng nguyền rủa - cô than vãn - Lần đấy là ở sân vận động và thế là chúng tôi đã gặp nhau…
- Đúng, đúng rồi, tại sân vận động. Ái chà, quái chiêu thật đấy, thế mới gọi là trận đấu chứ! "Dinamo" Moskva - "Dinamo" Tbilisi đấu với nhau. Tỷ số 2:1 nghiêng về "Dinamo". Tác giả hai bàn thắng này là Metrevili và Meshi. Các bạn tôi đã chơi bóng thật chuyên nghiệp. Misa Meshi thì đánh ở khung thành trái, Beliaev nhảy lên…
Đến đây thì ông thẩm phán đấm tay xuống mặt bàn:
- Điều thứ nhất, Meshi đã đánh quả bóng này bằng đầu. Và điều thứ hai, không phải Beliaev đứng bắt gôn mà chính là Lev Yasin. Còn điều thứ ba là, tôi sẽ không giải quyết cho anh chị ly hôn đâu. Chẳng hiểu gì về bóng đá cả mà cũng đòi là người đá bóng cơ đấy! Thôi, hãy đưa vợ về nhà đi và chừng nào mà còn chưa thực sự hiểu về bóng đá thì đừng có mà xuất hiện trước mặt tôi đấy.
Anh chồng đỏ mặt lúng túng, vụng về ôm lấy vai vợ rồi họ đi ra cửa, rời xa khỏi ông thẩm phán đầy uy quyền và hiểu biết…
- Thử nói xem nào, có thật là anh biết rõ về bóng đá như thế không đấy? - tôi hỏi ông thẩm phán khi đôi trẻ đã khuất sau cánh cửa.
- Tôi có thông thạo gì về bóng đá đâu - ông trả lời.
- Thế sao anh lại biết rõ về Lev Yasin như thế được?
- Đó là một cái tên cầu thủ duy nhất mà tôi nghe thấy đấy – Ông thẩm phán mỉm cười bối rối - Đành phải diễn một vở kịch nhỏ vậy. Tôi chẳng hề muốn phải chia rẽ cái đôi uyên ương đáng yêu này một chút nào cả.
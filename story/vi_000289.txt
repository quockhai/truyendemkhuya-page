Cơi sang làng Hèo tìm thằng Thất.
Hơn mười giờ đêm chị mới quay về làng Dạ.
Con đường men theo bờ suối sáng rợi ánh sao. Dưới vòm trời sao đêm hè, những lùm cây vả, cây lá cỏ mặn lóng lánh như phết một lớp lân tinh, có con chim quanh quý chốc chốc lại điểm nhịp đều đều ba tiếng một. Quanh quý...lọt! Quanh quý... lọt!
Tiếng chim gợi nhớ câu hát xưa: Quanh quý lọt! Quanh quý lọt! Chớ mải nghe chim quanh quý hót lỡ buổi cày. Cơi chưa bao giờ để quên lỡ một buổi cày. Thế đấy, ngày Cơi lấy chồng cũng là ngày chị bắt đầu tay cầm cầy, tay giật chạc trâu, đi những xá cầy đầu tiên. Sự kiện đó lạ lùng chẳng kém gì việc Cơi lấy anh Mìn thương binh cụt một chân mới từ trại an dưỡng trở về làng. Dạo đó không ít người mối manh, tỏ tình, ướm hỏi Cơi. Cơi xinh đẹp, hát hay, chịu thương chịu khó. Cũng không ít người can ngăn, ái ngại cho cuộc tình duyên này. Lo lắng nhất thì vẫn là ông bà, cha mẹ, chú bác, họ hàng nhà Cơi. Và chính Mìn cũng đã phải đắn đo nhiều ngày. Trong khi, vốn đã quen hơi bén tiếng từ lúc còn đi chăn trâu cắt cỏ với nhau. Đắn đo lắm! Tất nhiên, tình yêu, hạnh phúc gắn liền với con cái gia đình. Và về mặt này thì Mìn đã có lần bóng gió cho Cơi hiểu rồi. Anh chỉ có cái chân thương tật thôi. Còn tất cả đều lành lặn hết! A rổi!(1) Nghe vậy mà Cơi đỏ dừ cả mặt. Em đâu có nghĩ nhiều đến chuyện ấy, anh Mìn! Vậy thì đắn đo thuộc về Mìn cả thôi! Đũa gẫy một chiếc chẳng thành đôi được, liệu mình có trở thành gánh nặng cho Cơi? 
Thực tình là vợ chồng lành lặn ra ở riêng tự lực lập nghiệp cũng còn vất vả nữa là vợ chồng Mìn. Cái chân  trái cụt  quá đầu gối còn lạ lẫm quá với Mìn. Mó vào việc gì anh cũng bỡ ngỡ chuệnh choạng. Và do vậy, mọi việc từ trong nhà tới ngoài đồng, trên rừng, tất tật dường như đều dồn lên vai Cơi. Dồn lên vai Cơi cả cái gánh nặng gia đình Mìn, có ông bố già và chú em nhỏ. Xong ma chay cho ông bố chồng, cưới xin cho chú em nhỏ, thì lần lượt từng đứa con ra đời. Chà chà, thằng anh vừa lững chững men giường tập đi đã lại muốn có em rồi. Thỏ thẻ điều nọ với Mìn, Mìn đã dụi mặt vào má Cơi, cười khì khì: Anh đã bảo mà, anh chỉ bị hỏng mỗi một cái chân thôi. Với lại cũng vì anh chỉ có một chân, không đi xa đâu được, nên cả ngày cả đêm chỉ biết quẩn quanh bên vợ thôi. Đến đứa thứ ba thì Mìn không cười nữa. Người cười bây giờ là Cơi. Cơi bảo: Em đẻ con được thì em nuôi con được. Đông con như tre ấm bụi mà anh. Và Mìn thì như được thể, gật  gật gù gù: Ừ, mình ở rừng, đông con càng đỡ vắng vẻ. Với lại, để bù vào cái chân thiếu hụt của anh. Kết quả là những năm đứa đã lần lượt chào đời và bây giờ là đứa thứ sáu đang sắp sửa gia nhập vào cái gia đình đông đúc của vợ chồng Mìn với tất cả niềm vui sống và nỗi lo toan thường nhật. Miếng ăn ở dưới đất, ở trên rừng, phải đổ mồ hôi nước mắt mới có được!
Có ánh đuốc vàng ửng ở phía hạ lưu con suối. Mặt nước hừng lên màu đồng thau. Người ta đang đi chém cá. Mùa mưa sắp về rồi. Bên bờ suối đã thấy đặt những chiếc cụp, những chiếc rọ để bắt cá lớn. Giữa cái phai đá, nước đang đổ ào ào vào một cái hom giỏ, bắn lên những giọt sáng lóng lánh.
- Anh Mìn!
Ngược lên tới đỉnh đồi, nghe thấy tiếng nạng quen thuộc, Cơi dừng lại cất tiếng gọi. Mìn dựa vào một bên nạng, dún một nhịp chân đi tới.
- Anh Mìn ơi, con trâu sừng quặp bị người ta bắt mất rồi.
- Ai bắt hả, Cơi? 
- Bác Phàn. Người làng Hèo bảo nó ăn mạ rồi vặt trụi cả chuối bờ ao bác ấy.
- Thế thằng Thất đâu?
- Người làng Hèo bảo lúc chiều thấy nó lảng vảng ở quanh nhà bác Phàn rồi không thấy nó đâu nữa.
Mìn yên lặng. Những tầu cọ xanh bóng trên đầu hai người cựa mình. Hình như có gió lớn. Lá cọ cuộn mở xào xạc. Mìn nhìn vợ, ngập ngừng:
- Cơi à, bà con bầu anh làm đội phó.
- Anh ấy à?
- Ừ, chú Phủ làm đội trưởng.
- Chết thôi! Anh làm sao được!
Thảng thốt, Cơi kêu khe khẽ. Nhưng ngay khi ấy, chị hiểu rằng mình không nên nói thế. Anh có khả năng làm được chứ. Anh làm được. Chỉ thương anh thôi. 
- Anh từ chối không được. Thôi, khó khăn thì gỡ dần, em à.
Cơi nắm tay chồng:
- Anh đi đâu bây giờ?
Mìn chép miệng, nhưng mắt hưng hửng niềm vui:
- Con dại cái mang. Thì cũng phải sang nhà bác Phàn chứ làm thế nào được. Thôi em về nhà đi. Thằng Thất chắc là nó về nhà rồi đấy.
 

Ảnh: nationalgeographic
Thịch! Thịch! Thịch! 
Cái chầy trong tay bà Màng đang thọc đều đều xuống lòng chiếc cối gỗ sâu thẳm, cao tới lưng người. Mùi vừng rang quện với sắn luộc toả ra thơm phức cả gian nhà. 
Lát sau, nhấc cái chầy, thấy sắn nhuyễn trắng mút chặt vào đầu chầy, quay lại nhìn Cơi, gương mặt hình mặt trăng rằm của bà Màng sáng hừng lên dào dạt:
- Cô Mìn trông có giống bánh dầy đám cưới cô chú dạo trước không này.
Cơi rời bếp lửa, đứng dậy đi tới. Chị quỳ xuống, đưa tay vuốt đầu chầy, kéo ra một tảng sắn quánh dẻo, đặt vào chiếc rá lớn.
- Dạo ấy ông bà bên cô thách nhà chú ấy bao nhiêu vuông bánh nhỉ?
-  Lúc đầu bố em bắt mười hai vuông, sau rút xuống còn có ba vuông với một đôi gà vịt.
- Ông cụ cũng thương chú ấy nhỉ. Đêm giã bánh dầy ấy ở nhà tôi cô còn nhớ không? Cối là cối đá, chầy là chầy lim. Thậm thịch cả đêm. Vất nhất là lúc vắt bánh. Phải là tay thợ khoẻ. Lại còn phải xoa mỡ là tuỷ lợn rán chẩy ra kia. Đang lúng túng thì Ké Tó tới, ông cụ vuốt bánh, vê bánh khéo quá. Chiếc nào chiếc ấy tròn đầy như mặt trăng, mịn như da con gái. Đám cưới tôi với anh Chin, Ké Tó cũng tới vuốt bánh hộ đấy. Dạo đó mới giải phóng, Ké Tó mới thôi không phải làm cần khỏi nhà Lý Sang nữa.
Bà Màng ngừng nói, đưa hai ngón tay lên miết hai bên mép chùi quét trầu. Cơi ngẩng lên:
- Đám cưới bác với bác Chin chắc vui lắm nhỉ?
- Vui. Hồi ấy mới nổ võ trang tranh đấu để giải phóng tỉnh Lào Cai  mình mà. Anh em du kích tổ chức theo đời sống mới, nhưng đủ cả bánh dầy, gà vịt, bánh kẹo, thuốc lá. 
Ngoài cửa có bóng thằng bé thập thò. Cơi ngoái ra:
- Thao, buồn ngủ thì vào giường ngủ đi, con.
- Vào đây, bà cho ăn bánh sắn này.
Bà Màng véo một miếng bánh sắn, nắm chim chim trong lòng bàn tay. Cơi xua tay:
- Cháu nó vừa ăn cơm xong mà bà.
Thằng Thao chẳng nói chẳng rằng, nằm sấp xuống phản, lim dim mắt. Bà Màng vắt ra nửa tảng bánh, đặt lên cái giần lót lá chuối:
- Mai chịu khó rán cho trẻ nó ăn sáng, cô Cơi à.
- Húi! Bà cho cháu nhiều thế.
- Trẻ con nó đang sức ăn, sức lớn.
-  Úi dà, sáng nào lăn xuống giường mỗi đứa chả xúc ngay một bát cơm nguội hả bà.
- Thế Tết Thanh minh này đã có gì chưa?
- Dạ. Nhà con bảo thịt con ngỗng cho trẻ nó ăn. Năm ngoái Tết tháng ba, tháng năm, tháng bảy mỗi Tết mổ một con. Mấy con ngỗng bà cho làm giống đấy. Cứ đóng cửa chuồng lại là nó ấp đúng một tháng thì nở con.
- Gạo nếp còn chứ?
- Còn ạ. Nhà con bảo Tết này làm ít bánh lá ngải, lấy lá cẩm, nghệ vàng làm ít xôi màu cho trẻ nó ăn.
Bà Màng gật đầu:
- Ừ, chịu khó làm cho trẻ nó ăn. Dạo này đứa nào cũng có da có thịt đấy. Cái thằng Thao hôm nọ sang nhà, chị Vèn dấm được buồng chuối, nó ngồi ăn liền một lúc sáu quả với một bát xôi vừng.
- Húi, sao bà cho cháu ăn nhiều thế!
- Mặc nó, có sức ăn cứ để cho nó ăn.
Bà Màng lại đổ một rá sắn luộc nữa vào cối, rồi trút cả nửa bát vừng rang vào theo, tay nhấc chầy. Cái chầy ăn sùn sụt vào lòng cối.
- Nghĩ chỉ có đời anh Mìn là khổ. Tôi biết anh ấy từ bé. Mười ba tuổi mà đã phải đi cày rồi, người cứ đen sì như người U Ní bán công. Mười sáu tuổi vào du kích. Ông Chin nhà này chỉ huy không cho vào, cứ đòi vào bằng được. Nhỏ người nhưng chịu khó chẳng ai bằng. Tây càn, du kích đi phục kích. Phục kích xong, Tây chạy, chú ấy với ông Phàn bên Hèo lại về kéo cót ra đập lúa.
Cơi bế thằng Thao lên lòng, rung rung:
- Em cứ nghĩ lại cái hồi còn tổ đổi công lại thương anh ấy. Em mới sinh cháu Thích. Ba giờ sáng dậy đi cấy, anh ấy cũng dậy, ngồi ôm con. Con khóc, bố cứ tập tễnh, tay ẵm con, tay tựa nạng đi đi lại lại ru con ời ời. Được cái mấy cháu nó cũng ngoan. Chỉ có thằng Thất hư quá. Thằng ấy cai sữa là khó nhất, bà ạ.
Bà Màng đổi tay chày, nhổ quét trầu:
- Cứ dịu dàng mà bảo dần nó. Đừng quát mắng hại trẻ đấy.
- Có lúc nóng mặt, em cũng có nói nặng. Nhà em bảo: “Em mắng con làm anh tủi lắm”. Từ đó em không dám nặng lời với các cháu nữa.
- Còn chú ấy nữa, cô cũng phải bảo. Ai lại đã cụt mà hôm nọ dám lội xuống chỗ vực đánh cá. Đứng xa tưởng ai. Lại gần hoá ông tướng. Rêu trơn, nhỡ ngã có phải khốn không!
Cơi ẵm thằng Thao vào giường. Đặt con ngủ xong, chị quay ra thì cối sắn đã giã xong. Bà Màng vuốt tảng sắn vào rá rồi ra ngồi cạnh bếp lửa. Thôi thì còn thiếu chuyện gì nữa. “Đẩy kin nhòng mồ mả, thong thả nhòng tì rườn”, làm ăn được nhờ mồ mả, thong thả nhờ đất nhà, định ngày làm nhà lại đi rồi hợp tác sẽ giúp, Cơi à! Lại nữa. Cái thai trong bụng chín tháng mười ngày, phải giữ gìn kiêng cữ cẩn thận đấy. Đi làm thấy con rắn, đừng đánh, đẻ con lưỡi nó thè lè ra. Quả trong vườn, bụng mang dạ chửa cũng đừng hái. Tháng trong cữ, nhớ treo cành lá xanh trước cửa, đừng cho người độc vía vào nhà.
Và cuối cùng thì Cơi hiểu điều bà Màng muốn nói nữa với Cơi là gì, khi bà bắt sang chuyện Mìn vừa được bà con tín nhiệm bầu làm đội phó. Đúng lúc ấy Cơi nghe thấy tiếng gió lướt trên mái nhà, nhìn ra ngoài trời, chị bỗng như giật mình:
- Chết thôi, khuya mà sắp mưa rồi, nhà em không hiểu có còn ở trong Hèo không?
 
 

Ảnh: Trần Năm Phương
 
Mìn đứng ở chân cầu thang nhà Phàn. 
Thấy ánh đèn ở cạnh cái ao sau đám đao giềng rậm rà, đoán Phàn ở đó, anh liền cất tiếng gọi.
Mìn không phải chờ lâu. Loáng qua đám đao giềng một ánh đèn bão, anh xoay người lại và nhận ra Phàn cao lớn, đầu hói, mũi khoằm, cởi trần, vú xệ hai tảng, vai vác mai, tay xách con trắm bằng bắp chân. 
- Anh làm gì khuya thế! Vớt cá à?
- Chậc! Sắp mưa, sợ cái bờ ao lở, ra đắp lại. Thì gặp ngay con rái cá đang tha con này lên bờ. Mẹ cha cái giống khôn quá, lặn xuống bắt cá thế nào cũng cắn rập hai mắt con cá rồi mới tha lên bờ ăn. Thôi lên nhà đi. Làm bữa síu dề(2) #với nhau nhé!
- Phải đến phiền anh đây. Con dại cái mang. Có gì thì tôi xin nộp phạt, xin đền anh chị. Con trâu sừng quặp ấy mà!
Phàn như vỡ lẽ, xua tay:
- Nói gì mà gai đâm lòng nhau thế. Lên nhà đi. Tớ giữ con trâu sừng quặp của thằng Thất nhà cậu để bắt cậu đến đây đấy. Mẹo du kích mà!
Mìn bước lên cầu thang. Ánh đèn đi trước, nhoang nhoáng khe bậc. Thấy cái cầu thang chênh vênh quá, anh dừng nạng. Phàn quay lại:
- Có lên được không, để tôi đỡ?
- Được, được. Hồi mới về, không lên được, chứ bây giờ quen rồi.
- Sao bảo cậu lắp chân giả từ năm ngoái kia mà. Lại bỏ gà nuôi quạ à?
- Lắp rồi. Trông thì đẹp mắt nhưng lại hay vấp ngã. Chỉ hợp với anh làm văn phòng thôi. Còn mình, dân lao động chân tay, cái nạng xấu xí một tí nhưng tiện.
- Hây dà, cậu cứ cố làm gì cho nó khổ.
Mìn ngồi xuống sàn. Nghe câu nói ấy của Phàn, nỗi bừng bực như nấp sẵn ở đâu đó lại như rắp ranh trở về. Hồi còn trẻ, Mìn và Phàn là bạn. Phàn hơn Mìn năm sáu tuổi, nhà giàu có. Tình bạn giữa hai người có một kẽ nứt từ ngày Mìn ở trại thương binh về. “Chú mày về làm gì! Ai nuôi chú mày? Cứ ở trại, Nhà nước khắc phải nuôi có hơn không?” Tới thăm Mìn, câu đầu tiên Phàn nói là thế. Câu ấy Mìn nhớ mãi. Anh có cảm giác bị xúc phạm. Từ đó, cùng với cảm giác khó chịu về Phàn, anh còn nghe được bao nhiêu điều tiếng về ông. Hai người từ đó cứ xa dần nhau, xa dần nhau, mặc dầu chả có sự va chạm cụ thể, trực tiếp nào.
Bây giờ, ngồi ngắm cái dinh cơ của Phàn, Mìn thấy thiên hạ nói không ngoa. Hơn hai mươi năm qua, kể từ ngày Pháp chạy phỉ tan, Phàn mỗi ngày một thêm giàu có. Trước hết là căn nhà. Nó thật đồ sộ. Hơn bốn chục cây cột dựng, bốn mái theo kiểu nhà sàn cổ. Mỗi cây cột là một công trình lựa chọn, đẽo gọt, vê tròn, đánh bóng. Những cái phang mới dài làm sao! Chúng xuyên suốt dọc nhà, qua tám chín hàng cột mà tưởng sức nó còn đi được nữa! Hàng lan can ở cửa sổ trổ xuống vườn làm bằng thứ trúc kim quang dầu bóng nhẫy. Bốn mái nhà lợp gianh già, trải đều, sõng sượt như cói chiếu. Mặt sàn rộng thênh lát diễn vàng ánh. Giữa nhà là cái tủ cánh cong đặt trên chiếc bệ hình hoa sen nổi. Giữa khoang tủ, qua làn kính, thấy bộ hạc bằng đồng, đồng hồ, đài bán dẫn và hai cái phích mới vỏ in hoa hồng. Trước tủ là bộ bàn ghế mới đóng bằng gỗ nghiến bôi phẩm đỏ nặng chịch. Vách nhà treo những tấm thổ cẩm sặc sỡ. Gian cuối nghễu nghện một cái hòm thóc cao áp hàng sà trên, chiếm hẳn một góc nhà.
Ngoài trời cơn mưa lớn đã ập xuống từ lúc nào. Phàn đã luộc xong con trắm, gỡ thịt ra, rồi bắc cái chảo lên. Cá gặp mỡ sôi sèo sèo. Cùng lúc cái chiếu hoa đã trải trên sàn, nồi cháo đã được bưng ra và rượu đã rót vào hai cái chén, trong như nước mưa, sánh lạnh.
- Ăn đi, Mìn. Còn nhớ cái lần đói phải ăn nõn chuối rừng ở U Si Sung không? 
Phàn hai tay bê bát cháo lớn đặt trước mặt Mìn. Thảng thốt, Mìn lặng đi. Chao ôi! Làm sao mà Mìn không nhớ những ngày xưa gian khổ ấy. Lòng Mìn chưa tắt những kỷ niệm thời trai trẻ ấy đâu! Những năm bốn tám, bốn chín, những năm năm mốt, năm hai, Mìn đi làm giao thông rồi vào du kích, hết đánh Pháp lại tiễu phỉ. Bọn phỉ Lý Sang nổi dậy suốt một vùng từ Cam Đồng lên tới dẻo cao Xà Phiền, U Sì Sung. Tiễu phỉ ở Xà Phiền, mỗi du kích chỉ được phát có ba viên đạn. Thiếu đạn, chỉ có thế thôi. Ba viên đạn cho mỗi khẩu súng thế mà làm nên chuyện. Đi từ nửa đêm đến sáng tới Xà Phiền là đánh luôn. Ba viên đạn của Mìn đánh tan một ổ trung liên của địch. Phỉ tan tác chạy về Thanh Tôn, vùng Dao đỏ. Thế là lại bôn tập đuổi địch. Hết đạn chỉ còn lựu đạn. Nhưng khí thế lẫm liệt khiến bọn phỉ phải bỏ đất Thanh Tôn chạy về Đồng Tủm. Vừa hay tin Pháp thả dù biệt kích ở đấy, thế là Mìn lại theo anh em, vòng qua Tả Phù, Pạc Tà, Kim Đới. Qua cầu Kim Đới suýt nữa cả tổ du kích bị sát thương. Mìn cóc nổ đánh bốp, may mà nó thối. Đánh Đồng Tủm, suốt ngày súng nổ cắc cùm, cắc cùm. Bắt được một thằng phỉ mới mười hai tuổi. Nộp súng cho Mìn, nó nói: Em chưa bắn phát nào, không tin anh ngửi nòng súng xem. Mìn nhìn vào nòng súng thấy còn đặc mỡ. Thì ra là bọn Pháp vừa thả súng Mỹ xuống đây cho bọn phỉ để chúng đi cướp phá Cam Đồng. Cam Đồng có kho thóc của Chính phủ to lắm. Vì vậy du kích lại vòng lên U Sì Sung, rồi trở về Cam Đồng, và chính là ở đoạn này Mìn bị thương... 
Cái mũi khoằm đỏ ửng, Phàn lại cúi xuống rót rượu:
- Ăn đi, ăn đi Mìn. Ăn đi cho bõ những ngày cực khổ!
Mìn chống đũa xuống mâm:
- Lâu nay anh có gặp các đồng chí cũ không?
-  Có. Tuần trước gặp thằng Lẻo ở cửa hàng bách hoá tổng hợp huyện. Thế quái nào mà nó lên phó chủ tịch huyện rồi đấy.
- Anh Lẻo người cao cao ấy à?
- Lẻo, đội viên của tớ ấy. Cái thằng vua tếu táo. Hồi đánh Xà Phiền, trung đội phát cho mỗi người một hộp thịt hẹn ăn trong ba hôm, nó đem ra chén sạch một bữa, lại còn lý sự, biết có sống được ba hôm không. Giờ nó làm quan to rồi! 
-  Nghe nói anh ấy khá lắm. Năm sáu lăm làm chủ nhiệm hợp tác xã làng Giềng đưa năng suất lúa lên sáu tấn một héc ta bình quân, đứng thứ nhất tỉnh đấy.
Phàn nghển cổ, nghênh nghênh đôi tai dài như tìm bắt một âm thanh là lạ nào đó, rồi đặt chén, đứng phắt dậy, chạy ra cửa lập bập xuống thang. Lát sau Phàn quay lên, mặt nở nang hớn hở: 
- Tưởng cái ao vỡ, hoá ra không phải. Mìn à, năm ngoái không lụt mà trắm cỏ ở ao tớ không còn một con. Sau mới biết, hoá ra có mấy thằng nheo nó lọt vào. Phải bỏ công gạn mất một tuần mới bắt được mấy thằng ăn hại đó!
Ngồi xuống, xếp chân bằng tròn, chợt nhớ lại câu chuyện bỏ dở, Phàn nhấc chai rượu, thở đánh phào:
- Tớ bây giờ như bãi cát gặp kỳ nước cạn trơ trống ra rồi.
Mìn chíp chíp môi, nheo nheo hai con mắt:
- Anh nói thế là thế nào?
- Có công trồng lúa mà lúa có thành thóc đâu!
- Không thành thóc chỉ có là lúa ma, cỏ dại thôi.
Phàn lắc đầu:
- Tớ biết. Tớ biết giờ cậu đã là đảng viên rồi. Tớ chẳng ghen với cậu. Nhưng còn những thằng khác? Ai là người tham gia du kích đầu tiên ở làng Hèo này? Tớ! Thế nên hôm nay cậu ngồi đây với tớ, tớ sẽ cởi lòng cho cậu biết. Tớ tức lắm. Đêm đặt đầu xuống gối, ngủ có được say đâu. Cậu nghĩ xem, thế này thì ai chịu được. Mảnh ruộng bỏ hoang đấy, tớ cất công đào thành cái ao. Khoá sau tớ không làm chủ nhiệm nữa, thế là nhao nhao lên đòi tớ phải trả lại cái ao cho hợp tác. Mẹ nó chứ, thằng Tây tôi còn không sợ, sợ gì các anh!
- Thế cho nên bà con nhắc nhở mấy lần mà anh vẫn không chịu trả, cả đất trên đồi trong rừng nữa, có phải không?
- Trả là trả thế nào! Đất là đất hoang, đất rừng, đất chẳng là của ai cả. Cuốc thuổng trong tay tôi không quay như cái cọn nước thì đất tự nẩy ra được đồng tiền à! Ao cá tôi đào. Đồi gianh tôi quản. Đất hoang tôi trồng tre, trồng xoan, trồng đu đủ, trồng mía, trồng đao giềng, rồi tôi bán đi thu tiền về, chứ tôi ăn cướp của các anh à? Ghen tức với tôi, vào hùa với nhau kéo tới đo đất đo cát, rồi cắt chỗ nọ, lấy chỗ kia của tôi, sao lại dễ thế được! Nhổ bọt phải nhìn khe sàn chứ. Bắt nạt thằng này đâu có được. Tôi xin ra hợp tác. Tôi kêu lên tận tỉnh chứ. Tôi là du kích cũ mà dám bảo tôi là cường hào mới à!
Lâu lắm mới được thổ lộ tâm tình, càng nói nỗi uất ức của Phàn càng tăng. Chưa nói lại, im lặng, Mìn nhìn Phàn, càng lúc càng thấy ái ngại. Mìn hiểu, đối với Phàn, xã cũng có nhiều thiếu sót. Nhưng dù có thế nào đi chăng nữa, Phàn cũng không thể đổi đen thành trắng được. Làm đội trưởng mới có một năm mà Phàn đã chiếm đến hai khoảnh ruộng đào ao thả cá. Đất đồi, trồng được mía, nhưng Phàn không bán cho nhà máy đường, cứ đem ra chợ tiện khúc bán lẻ. Phàn chiếm cả mấy quả đồi gianh, cùng vợ con đi cắt, thu về bạc nghìn, chẳng nộp cho công quỹ đồng nào. Rõ ràng là Phàn biết làm giàu, nhưng lại làm giàu bằng cách chiếm đoạt của người khác, làm thiệt hại đến người khác!
Ngoài trời mưa gió vẫn ào ào.
Định nói vài lời với Phàn, Mìn bỗng chống tay qua nạng đứng dậy. Anh vừa nghe thấy tiếng người gọi. Đu người trên nạng, Mìn đi ra cửa. Trong cái vòng sáng vàng đẫm của ánh đèn pin, anh nhìn thấy Cơi ướt lướt thướt đang run rẩy đứng ở chân cầu thang.
- Anh Mìn. May quá, anh có bị mưa ở giữa đường không? Con trâu sừng quặp có ở đây không?
Vẫn ngồi ở trên chiếu rượu, Phàn nghển cổ ra phía cửa, oang oang:
- Cô Cơi đấy à! Lên nhà đi! Lên nhà đi! Sừng quặp hay một sừng, con trâu nào thì cũng ở đây rồi.
*
*     *
Tạnh mưa, vợ chồng Mìn mới từ nhà Phàn dắt trâu về. Trời đang chuyển sáng. Ánh ngày đang làm nhạt dần cái sắc xanh đen của vòm trời đêm. Tuy vậy, sao như vừa được mưa tưới rửa, trong suốt, rực rỡ, chói loà qua lần không khí không chút gợn bụi sương.
Cơi dắt con trâu sừng quặp đi trước. Mìn chống nạng theo sau. Sau cơn mưa, trời mát lạnh. Nước ở những thửa ruộng cạnh đường và từ trên những mảnh ruộng cao rót xuống mảnh ruộng thấp như những dòng bạc trắng. Con suối như một cơ thể vụt lớn, ồn ào chồm lên những tảng đá lớn ngổn ngang dưới lòng suối. Chim sâu đã dậy, kêu róc rách như tiếng nước chảy trong các lùm cây sung lá còn ướt đẫm.
Đứng lại, Cơi quay về phía sau chờ chồng. Sau trận mưa, nhiều đoạn đường sũng nước bầy nhầy bùn.
- Anh Mìn, khéo ngã anh nhé. Lúc mưa em lo cho anh quá. Em tưởng anh đương đi về cơ đấy. Sao anh ở lại nhà bác Phàn lâu thế?
Mìn đi bên cạnh vợ:
- Bạn cũ với nhau mà. Hồi xưa anh ấy là tiểu đội trưởng tiểu đội du kích của anh đấy.
- Tiểu đội trưởng à?
- Tiểu đội trưởng!
Trong ánh sáng của ngày đang rạng, Mìn nhìn rõ gương mặt của Cơi, đôi lông mày dướn cao, cặp mắt đầy vẻ băn khoăn. Cái trạng thái tình cảm ấy của Cơi khiến anh hiểu rõ Cơi hơn. Cơi không chỉ là một người phụ nữ tận tuỵ, dịu dàng, Cơi còn trong trẻo, hồn hậu, còn là một dòng suối nơi đầu nguồn.
Quay mặt lại phía Cơi, giọng Mìn bỗng nghèn nghẹn:
- Cơi à! Chính con đường này đã dẫn anh từ U Sì Sung xuống đánh bọn phỉ đấy.  Hôm ấy đang đi thì dẫm phải mìn, em à. Lúc ấy, chỉ nghe thấy tiếng nổ đánh “bốp” như cái ống nứa vỡ, rồi ngất lịm đi, chẳng biết gì nữa. Tỉnh dậy thì thấy nằm còng queo ở cách con đường mòn này sáu bẩy bước chân. Ngó xuống, chà, chân trái đã dập nát và bàn chân phải cũng đầm đìa máu. Đau, nhưng  không hiểu sức ở đâu mà vẫn chồm dậy được, rồi lết  đi. Đi đâu? Đi tìm khẩu súng gióp 3. Tìm  để bọn phỉ nếu có mò ra thì sẽ đánh lại. Hà! Thế đấy, một con đường đi từ lúc trẻ đến giờ lại đi tiếp! Cứ cố mà đi, em à. 
- Anh! 
Thốt nhiên, Cơi kêu khe khẽ, ngắt lời chồng và và đứng sát lại cạnh  anh. Và hình như chị đã chờ đợi, đã tưởng tượng ra, nên khi anh đưa tay đặt lên vai mình thì chị quay hẳn lại, ôm ngang sườn anh. Ngọn gió tươi mát  buổi  bình minh  lướt thướt đi qua. Quen quen và là lạ, hơi thở của hai người  chợt như  rộn rực quện vào nhau và cả hai cùng lúc dâng chan một cảm xúc  hòa đồng  thật thân  thương. Cuộc sống bận rộn và vất vả.  Đã có mấy khi họ có được khoảnh khắc riêng tư với nhau trong cảm giác thanh tân, tươi mới như thế này. 
“Vất vả cả ngày rồi, sao em còn lặn lội đi tìm anh thế, Cơi à!” Cúi xuống, sát vào bên tai Cơi, Mìn thì thầm. Và khi Cơi ngước lên hỏi lại anh rằng: Thế anh có  thích em đi tìm không? Rồi  thủ thỉ thú thật rằng, vắng anh chị không ngủ được. Thì anh choàng tay qua Cơi kéo vợ thật sát vào mình, rưng rưng và ngào ngạt. Trời ạ! Sao Cơi nói đúng lòng anh thế! Vợ chồng mình phải hơi nhau rồi. Cơi ơi! Vậy là đã gần hai chục năm anh được sống trong hạnh phúc là có em làm vợ và chúng mình thế là đã có một gia đình có đến sáu đứa con. Sáu đứa con! Nghe đến đấy thì mặt Cơi bỗng hừng sáng như có ngọn lửa thắp sáng từ bên trong. Rồi sung sướng mãn nguyện và bẽn lẽn chị kêu: Húi! Thế có nhiều quá không, anh? Còn anh thì dụi cằm lên tóc chị, nhắc lại cái câu nói hôm nào, rằng đó là do anh chỉ có một chân nên ngày đêm chỉ còn biết quẩn quanh bên vợ; trong khi chị vừa day mặt vào ngực anh vừa lắc đầu quầy quậy, nói: không phải! không phải!; đó là vì em yêu anh và em giống mè em, mè em có những tám chị em em cơ! 
Trong một cảm xúc dâng chan, Mìn nhìn vào cặp mắt của vợ, vừa trang trọng vừa thân thiết, nói rằng, anh phải nhiều lần cám ơn em mới phải. Vì nhờ có em mà anh vẫn đi được con đường anh đã chọn lựa, đã đi từ trẻ đến giờ.
Một con đường đi từ trẻ đến giờ. Con đường đương đầu với sự sống và trở thành một con người tự đứng dậy từ chết chóc, thương tật. Nhờ có chị, anh đâu có gục ngã quy hàng, hoặc đi con đường khác! Từ trại an dưỡng về, buổi sáng làm kế toán đội; buổi chiều, rỗi rãi chân tay không chịu được là chống nạng, dắt trâu đi thả. Ra ruộng mạ, gặp hôm nắng ráo, anh vừa nhổ mạ vừa nhấp nhổm trên cái ghế mây; gặp khi ruộng ướt, anh đứng, cái chân cụt tì vào nạng, lấy thế cúi xuống nhổ roàn roạt từng nắm mạ một. Đi gặt cũng thế. Có hôm còn được mười điểm, như công người đủ chân đủ tay. Thằng Thích mười hai tuổi, anh dẫn con vào rừng. Đến cửa rừng, anh bỏ lại một nạng, đi một chân một nạng vào rừng. Rồi bố chặt, con kéo một buổi được hai chục cây vầu to về rào vườn. Anh một con người bình dị, không tên tuổi, danh tiếng, sống bằng lao động lương thiện của chính mình.
*
*    *
Vợ chồng Mìn về tới nhà thì trời đã hửng sáng. Trần mây bóng lọng, xanh nhoáng vọng lên tiếng gia súc inh ỏi vừa thức giấc, ùa ra khuôn sân nhỏ.
Mìn đứng lại ở đầu hồi.
Trong bếp lép bép tiếng mỡ nổ. Thích đã dậy rán bánh sắn, nhìn thằng Thất ngồi cù rù bên cạnh bếp:
- Đói hả? Ai bảo mày ngồi ở ngoài bụi chuối để muỗi đốt sưng hết cả người lên thế! 
Thằng Thất lầu bầu:
- Ở trong nhà người ta họp bầu pò làm đội trưởng đội phó, em không dám vào. 
- Biết sợ thế sao lại còn để trâu đi ăn mạ?
Cầm bàn sản xúc một tảng bánh sắn vào cái đĩa nhôm, đưa em, mắt Thích gườm gườm:
-  Ăn đi. Mày hư lắm. Mày làm pò vất vả, làm mè vất vả. Cả đêm qua đi tìm trâu cho mày, pò mè có được ngủ đâu.  
Chợt thấy thằng Thất cầm đĩa bánh ngần ngừ chưa dám ăn, Thích liền hạ giọng:
-Ăn đi. Thế mày có định đi học không đấy?
- Có ạ.
Cái Thơm, thằng Thao đã dậy, ngồi quanh chảo bánh. Cái Thư vừa ra suối gánh nước. Thích lấy bánh cho vào bát, đưa cho từng em:
- Hôm nay chúng mày làm đúng công việc tao đã phân công nhé.
- Vâng ạ.
- Lớn cả rồi! Lớn thì phải biết suy biết nghĩ! Pò là thương binh, nay pò lại là người lãnh đạo, pò mè đều bận bịu nhiều việc, khó nhọc để nuôi anh em mình. Thế chúng mày tưởng tao không muốn đi học nữa à? Thế đấy! Chẳng lẽ tao lớn bằng từng này mà không giúp được pò mè gì à!
Thư đã gánh hai bắng nước về tới cửa. Chụp cái mũ vải nhựa lên đầu, chống hai tay lên mạng sườn, lướt mắt qua lũ em, Thích dằn từng tiếng:
- Tự động làm việc đi! Thằng Thao nhớ sang nhà chị Vèn, xin nghệ, xin lá cẩm, tối tao về tao làm xôi năm màu ăn Tết Thanh minh. Những đứa khác phải nhớ những điều tao dặn. Tao không chắc ở nhà mãi để bảo ban chúng mày đâu.
- Thế anh Thích đi đâu?
Thằng Thao nghển lên. Thích hênh hếch mặt:
- Đi đâu ấy à? Đi công trường thoát ly! Rồi đi bộ đội nữa chưa biết chừng.
Cái Thơm nhăn mũi:
- Anh bé thế ai người ta nhận!
Thích trợn mắt, người ngay đơ không nói được câu nào.
Cơi đã buộc con trâu sừng quặp vào búi tre. Quay lại, chị vẫn thấy chồng đang đứng ở đầu hồi nghe các con trong nhà trò chuyện.
- Anh về nghỉ đi. Cả đêm thức trắng rồi còn gì.
- Anh phải sang làng Giềng đây, sáng nay hội ý ban quản trị. 
- Nước suối sau cơn mưa đêm qua to lắm, anh không đi được đâu.
- Anh đi được. 
Nói rồi, Mìn quay người dấn nạng đi xuống dốc.
Con suối đã dâng nước lên gần tới dẫy lều đặt cối gạo. Vừa định bước chân xuống nước, nghe tiếng gọi, quay lại anh đã thấy Cơi vác cái cuốc hớt hải chạy xuống; cái thai hằn lên sau lần vạt áo trước.
- Anh Mìn, để em dẫn anh sang.
- Anh đi được mà.
- Để em dìu anh! Nhân thể em sang bên kia suối, giúp bà Màng vun mấy luống khoai.
Mìn chần chừ:
- Dìu anh dễ ngã cả hai lắm, Cơi à.
Cơi đã bước xuống suối. Cứ nguyên cả quần áo, y như hồi con gái đi hái rau dớn, đi vớt cá dưới bờ suối, nhưng tay quài ra phía sau.
- Đi theo em, anh Mìn.
- Khéo ngã đấy, Cơi.
- Theo lối em đi nhé. Khéo không lại chống nạng vào hòn đá rêu đấy, anh.
Trên bờ, Thích rõi theo từng bước đi của pò mè. Khi thấy pò đã sang được tới bờ bên kia, và mè đang cúi xuống vắt nước ở cái vạt áo dài lam, cậu mới thở phào; rồi chụp mũ lên đầu, cậu bước nhanh về phía công trường xây dựng đập nước ở hạ lưu con suối.
 
Lào Cai 1972- Hà Nội 2012 
M.V.K
---------
(1)Tiếng reo vui
(2) Bữa ăn tối
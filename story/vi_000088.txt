Mẹ có tóc sâu. Những sợi trắng bé xíu, sợi dài nhất chỉ độ một lóng tay, xoăn tít, thường làm mẹ ngứa ngáy khó chịu. Mỗi trưa, khi công việc của ngày đã xong phân nửa, tranh thủ nghỉ ngơi, mẹ hay gọi chị em tôi lại nhổ tóc sâu. Mẹ ngồi ở bậu cửa. Dáng nhỏ bé, gầy guộc, lặng thinh như tượng. Nắng lấp lánh rơi trên mái tóc mẹ. Sợi tóc sâu mỗi khi được nhổ khỏi đầu, gương mặt mẹ lại giãn ra khoan khoái. Mẹ cứ ngồi như thế, lim dim, đến khi chúng tôi mỏi tay thì mẹ cũng đã ngủ gật rồi.

Mẹ thích được tôi nhổ tóc sâu. Mắt tôi tinh nên nhổ vừa nhanh vừa nhiều. Mỗi lần được một sợi, tôi thường để lên tay áo mẹ. Thỉnh thoảng, ba đi công tác về cũng ngồi xuống nhổ. Mắt yếu, tay lại run nên ba nhổ chậm, cẩn trọng từng sợi một. Mỗi sợi tóc sâu nhỏ xíu đong đầy tình yêu thương của đôi vợ chồng ngoại ngũ tuần.

Em trai tôi đang tuổi ăn tuổi lớn. Cái chân nó thích chạy chơi hơn là ngồi. Mỗi lần mẹ nhờ, nó hay ra điều kiện:

- Một sợi năm trăm nghe mẹ!

Mẹ la toáng:

- Thôi đi! Khỏi, khỏi! Năm trăm một sợi, tiền đâu mẹ trả? Hết tiền rồi!

Thằng nhỏ lại kỳ kèo:

- Rứa hai trăm cũng được.

Mẹ hạ giá:

- Một trăm thôi. Không nhổ thì mẹ nhờ chị mi!

Em tôi làu bàu:

- Có mỗi một trăm. Nhổ mỏi cả tay!

Nói vậy nhưng nó vẫn vạch tóc mẹ ra tìm những sợi sâu. Tôi cười vang mỗi lần em như thế. Ba cũng lắc đầu, mắt lấp lánh. Hạnh phúc ngân nga…

Tôi đi học xa, lâu rồi không nhổ tóc sâu. Mỗi tuần về thăm nhà, tôi hay đi chơi với bạn hơn là ngồi cùng mẹ.

Chủ nhật này, tự nhiên thấy chán đi cùng lũ bạn, tôi ở nhà. Trưa, mẹ bảo tôi nhổ tóc sâu. Mẹ lại lặng lẽ ngồi bên bậu cửa. Lim dim. Gà gật. Tóc mẹ đầy nắng. Một sợi bạc. Hai sợi bạc… Năm sợi bạc… Tôi thả những sợi bạc ra cùng gió, chỉ để tóc sâu lên tay áo mẹ:

- Dạo này ít tóc sâu mẹ ạ! Thôi! Mẹ đi nghỉ đi!

Mẹ ngủ. Tôi ngơ ngẩn ra vườn. Vườn nhà đầy nắng và gió. Em tôi ngồi xuống cạnh bên. Không dưng, nó bâng quơ: “Bữa ni tóc mẹ bạc nhiều!”…
Đây là một trường trung học ở trên địa thế khá cao, có thể nhìn thấy biển cả. Khi lên lớp từ trong lớp học có thể nhìn thấy biển cả biến đổi không cùng.
 
Năm học ấy có khoảng 80 em học sinh mới nhập học, trong đó đại đa số là con em của những ngư dân hàng ngày đánh vật với biển lớn.
 
Đó là chuyện xảy ra trong tiết học đầu tiên tôi giảng dạy cho học sinh mới.
 
- Đứng dậy!
 
Mọi học sinh đều đứng dậy. Bởi vì là học sinh mới, cho nên các em đều rất chú ý thận trọng, nên trong lớp học vô cùng im lặng.
 
Nhưng tôi nhìn thấy có một học sinh có lẽ muốn chơi trội, nên không đứng dậy.
 
- Đứng ngay dậy! Vừa nhập học mà đã có thái độ như vậy là không được! – giọng nói của tôi bỗng nhiên đanh lại.
 

Ảnh: Phạm Duy Tuấn
 
Lúc ấy, có tiếng nói :
 
- Thưa cô! Em đứng đấy ạ!
 
Đúng thế, cậu ta, cậu A đang đứng đó, nhưng vì cơ thể cậu ta quá thấp, tôi nhìn thấy như ngồi.
 
Gay go rồi! Tôi đã làm một việc có lỗi với em A.
 
Tôi cảm thấy bất an về sự sơ suất thô bạo của mình, tạm thời chưa biết nói ra sao.Nếu như ngay lúc ấy mà tôi xin lỗi cậu A. Thế là, lúc ấy tôi chỉ nói một câu “Xin lỗi”. Các em học sinh ngồi gần đấy cười rộ lên. Chắc là em A rất đau lòng, tôi ý thức được rằng sau đó, em A có thể vì chuyện này mà bị các em khác coi thường, trêu trọc.
 
Sau khi hết tiết, tôi định xin lỗi em A, nhưng vì quá tất bật, tôi đã quên béng chuyện này. Buổi tối, tôi do dự có nên gọi điện thoại cho em A hay không. Song, gọi điện thoại xin lỗi thi quá mất lịch sự, thế là tôi đành phải cho qua.
 
Ngày hôm sau, trời trong nắng đẹp, biển cả vào mùa xuân sóng xanh vỗ nhẹ. Tôi giảng bài thứ hai cho lớp cậu A.
 
- Đứng dậy!
 
Lại im lặng một phút. Lúc ấy, bỗng nhiên vẳng lên một tiếng nói dõng dạc :
 
- Thưa cô! Em đang đứng ạ!
 
Đúng là em A, cậu ta đứng trên ghế, tủm tỉm cười.
 
Trong nụ cười tủm tỉm của em A, tôi nhận ra em làm như vậy không hề tỏ ra châm chọc đả kích, mà không biểu lộ thái độ chống đối. Tôi cảm nhận được thái độ chân thành thông cảm của em như muốn an ủi tôi “Thưa cô! Em không để ý đâu, không nên lo lắng cho em”, tim tôi như đau nhói.
 
Buổi tối, với một tình cảm tâm tư vô cùng phức tạp, tôi gọi điện thoại cho em A.
 
- Thưa cô! Đừng bận tâm, Đừng bận tâm!
 
Từ đầu dây bên kia truyền lại giọng nói gà tồ mà ngây thơ của em A.
 
Tôi cầu mong bầu trời ngày mai vẫn không mây nắng đẹp, biển cả vẫn sóng xanh vỗ nhẹ vào bờ.
Allegra Goodman (Mỹ), nữ, sinh năm 1967, nhà văn Mỹ, sống tại Thành phố Cambridge, bang Massachusetts. Bà sinh ra trong một gia đình trí thức ở Hawai, có bằng Tiến sỹ Văn học Anh, bắt đầu viết và vẽ từ năm lên bảy tuổi, đến nay đã xuất bản 07 tiểu thuyết và nhiều tập truyện ngắn. Cuộc sống mới là truyện ngắn in trong tuyển tập Người New York xuất bản năm 2010, được chọn in trong Tuyển tập truyện ngắn hay nhất nước Mỹ năm 2011.
 
 
**************
 
 
Ngày anh đi, Amanda lang thang trong nghĩa trang Colonial trên phố Garden. Cô cố gắng đọc những gì viết trên bia mộ, nhưng không thể được. Chúng đã cũ mòn và lấp chìm trong cỏ dại. Anh bảo cô là người đàn bà sầu não.
 
Quay về nhà, chui vào ngồi thu lu trong tủ, Amanda nhìn một lượt. Anh đã mang đi tất cả, cái tủ chỉ còn lại một nửa, một nửa trống không, lạnh lẽo. Chiếc váy cưới kiểu cổ điển của những năm năm mươi được treo trong một chiếc túi nilon sạch sẽ có in hàng chữ: “Không phải đồ chơi”.
 
Amanda mang chiếc váy đến chỗ làm. Cô treo váy vào cái giá trên tàu điện ngầm, chiếc váy bay phất phơ, sột soạt. Lúc mua nó ở Quảng trường Harvard, người đàn ông chơi ghita cạnh cửa ra vào đã nói với theo: “Chúc mừng”.
 
Amanda dạy học ở trường Garden. Ở đó cô dạy các môn nghệ thuật, từ kịch, rối, kể chuyện, đánh trống, nhảy múa đến vẽ trên vải. Cô trải tấm áo lụa trắng trên sàn phòng học nghệ thuật. Hai cô bé học trò lấy lông chim hồng gắn dọc viền váy. Mấy đứa khác quét màu xanh và màu tím lên chân váy. Một cậu bé tên Nathaniel nhúng bàn tay nhỏ bé vào sơn đỏ và ấn nó lên phần ngực váy như thể cái váy là con ngựa Ấn Độ. Buổi trưa, hiệu trưởng mời cô đến phòng làm việc.
 
- Cuộc sống của em thật u ám - Chồng sắp cưới của Amanda nói với cô trước khi bỏ đi - Em lúc nào cũng u buồn.
 
- Em đang buồn đây - Amanda đã đáp lại.
 
Hiệu trưởng bảo với Amanda, đối với một người làm nghề giáo, xác định được ranh giới giữa các vấn đề là hết sức quan trọng. Cuộc sống riêng của cô - bà nói - không phải là chủ đề nghệ thuật thích hợp cho chương trình lớp một. Lớp học của cô - bà nói tiếp - cũng không phải là nơi trình diễn các mối quan hệ của cô. Hãy mang cái váy cưới về đi.
 
Amanda bảo “Nhưng nó vẫn còn ướt”.
 
Mẹ cô không thể tin nổi sự thật. Bà đã gửi thiếp mời khắp nơi. Còn bố cô thề sẽ giết cái thằng khốn nạn đó. Cả hai cùng hỏi làm thế nào một chuyện như vậy lại có thể xảy ra. Chị cô, Lissah bảo, chị ấy không thể tưởng tượng nổi làm sao Amanda có thể vượt qua mọi chuyện. Chắc cô phải cảm thấy kinh khủng lắm. Không biết cô có phải viết gì giống như một cái thư xin lỗi hay đại loại thế tới khách mời không? Cô sẽ phải nói sự thật cho tất cả mọi người, đúng không?
 
Chồng sắp cưới của Amanda bảo: “Trong suốt thời gian dài anh đã chần chừ vì anh không muốn làm tổn thương em”.
 
Hết giờ dạy, cô đi uống vài ly với Patsy, cô giáo già có mái tóc vàng hoe dạy môn thể dục. Hai người đến quán rượu Cambridge Common gọi hai ly Gin Tonic.# Patsy nói: “Cuối cùng, cô sẽ phải nhận ra rằng đấy thật ra là một sự che đậy”.
 
“Chúng cháu có quá nhiều điểm khác nhau” - Amanda bảo.
 
Patsy nâng ly lên: “Nào, xin mời”.
 
“Có thể, cháu yêu anh ấy và anh ấy không yêu cháu”.
 
Patsy bảo: “Đừng ngạc nhiên nếu ngay lập tức thằng đấy đi lấy vợ. Đàn ông là thế!”
 
“Tại sao?” - Amanda hỏi.
 
“Nếu biết, tôi đã dạy học ở Harvard chứ dạy bọn trẻ con làm gì?” - Patsy thở dài.
 
Amanda đã cố viết một cái thư xin lỗi. Đầu tiên cô viết cô và vị hôn phu đã quyết định không kết hôn. Rồi sau đó cô viết lại rằng vị hôn phu của cô đã quyết định không kết hôn với cô nữa. Cô nói rằng, cô rất xin lỗi vì đã gây phiền phức cho mọi người. Cô cũng tái bút, dù sao đi nữa cô sẽ luôn cảm kích trước những món quà cưới.
 
Bố mẹ cô bảo đừng gửi thư xin lỗi. Bố mẹ sẽ đến chỗ cô một tuần. Cô bảo bố mẹ đừng đến, con đang sơn lại căn hộ. Dĩ nhiên là cô đã nói dối.
 
Mùa đông, Amanda cắt tóc ngắn như con trai.
 
“Ồ, tóc của cô - Patsy kêu lên - những lọn tóc quăn xinh đẹp của cô đâu rồi?”
 
Mùa xuân, hiệu trưởng bảo Amanda rằng, thật đáng tiếc, hợp đồng của cô sẽ không gia hạn trong năm tới, vì môn học nghệ thuật ở trường Garden sẽ chuyển cho bộ phận khác quản lý.
 
Mùa hè, vị hôn phu của Amanda kết hôn với một cô gái khác.


Ảnh: Tam Nguyen
 
Không dạy học nữa, Amanda kiếm được việc làm bảo mẫu cho Nathaniel, cậu học trò đã nhúng tay vào sơn đỏ. Mẹ Nathaniel yêu cầu cô giúp cậu các môn khoa học, các bài tập cuối kỳ và các hoạt động kỹ năng sống. Bố cậu chẳng yêu cầu gì cả.
 
Ngày đầu tiên, Amanda hỏi Nathaniel: “Con muốn làm gì?”
 
“Chẳng gì cả”.
 
Cô bảo: “Con thật hiểu ý cô”.
 
Cả hai cùng ăn sô cô la hình con chuột mua ở cửa hàng Burdick, sau đó đứng trước Trung tâm mua sắm Harvard, nghe nhạc Peru. Khi cùng lang thang trong nghĩa trang, Amanda bảo với Nathaniel rằng những cái bia mộ là răng của rồng. Lúc đi xuôi theo dòng sông cô bảo: “Nếu đi ngược dòng, lên tận thượng nguồn, con sẽ tìm thấy một cái hang thần”. Rồi họ bắt tàu điện ngầm đi từ Boston, cùng nhau đứng xếp hàng đợi thuyền đạp vịt ở Công viên. Cô bảo: “Đêm xuống, những cái thuyền sẽ biến thành thiên nga thực sự”.
 
Nathaniel bảo: “Cô thật là có trí tưởng tưởng phong phú”.
 
Mẹ cậu bé sống ở toà nhà Victoria trên phố Buckingham. Chị ta làm việc ở Phòng truyền thông của M.I.T và gặp chị thực là khó. Ngôi nhà có khu vườn rực rỡ nhưng Nathaniel không chơi ở khu vườn, đơn giản là vì, ở đó người ta không cho đào đất.
 
Bố cậu bé sống trong một căn nhà lộn ngược ở phố Chauncy. Phòng ngủ ở dưới sàn, bếp và phòng khách ở trên trần. Anh ta hay về nhà muộn vì đang bận viết một cuốn sách.
 
Amanda và Nathaniel cầm pizza đến nhà bố cậu, xem phim hài Sác lơ ở trên trang Hollywood Express. Một lúc, họ trải giấy lên đi-văng và cùng nhau ăn đầy một thố bỏng ngô.
 
- Ở bên em quả thực là không dễ dàng - chồng sắp cưới của cô nói - Anh luôn cảm thấy nghẹt thở.
 
- Thế thì mở toang cửa sổ ra - Amanda bảo.
 
Khi bộ phim vẫn đang chiếu, Amanda thu dọn tờ giấy và bước ra ban công rũ sạch những mẩu vụn.
 
Amanda và Nathaniel có một ngày vui chơi ở hồ Walden Pond. Khi bơi xuồng trên sông Charles, Nathaniel đánh rơi mái chèo xuống nước. Amanda gần như  phải nghiêng cả cái xuồng để cố vớt mái chèo cho thằng bé. Họ cùng nhau viết một quyển truyện về những tên cướp, Nathaniel nói ý của câu chuyện, còn Amanda gõ vào máy tính trong phòng làm việc của bố cậu. “Aarrr, chiến hữu - cô gõ - tôi đang bị mắc kẹt trên tàu”.
 
Đến giờ Nathaniel đi ngủ, bố cậu vẫn chưa về. Amanda cho Nathaniel vào giường còn mình đọc những cuốn sách trong phòng làm việc. Có những quyển sách về lịch sử nước Mỹ. Mỗi quyển cô đọc vài trang, cuối cùng cô nhận ra hầu như chả học thêm được cái gì.
 
- Nếu em dừng lại để nghe - anh từng nói - em sẽ hiểu.
 
Cô ngồi trên ghế, ngước lên đọc gáy mấy quyển sách văn học. “Vở kịch thần thánh” của Dante, bản dịch mới nhất. “Mười ngày” của Boccacio, “Những câu chuyện ở Canterbury” của Chaucer, bản đầy đủ và “Cuộc sống mới#”, lại Dante.
 
Đọc “Cuộc sống mới” bạn sẽ biết bí quyết để trở thành một thi sĩ giỏi. Đó là hãy yêu một cô gái hoàn hảo nhưng đừng thổ lộ. Thay vào đó, bạn nên khóc. Rồi bạn nên giả vờ yêu một người con gái khác. Và bạn nên viết những bài thơ có ba phần. Cuối cùng, cô gái hoàn hảo của bạn nên chết.
 
Mẹ cô bảo: “Con còn cả một cuộc đời phía trước”.
 
Cô ngủ quên trên đi-văng cho đến khi bố của Nathaniel về. Tỉnh giấc, cô thấy anh ta đang quỳ trước mặt. Cô hỏi: “Có chuyện gì vậy?”
 
Anh ta bảo: “Không có gì. Xin lỗi. Tôi không muốn làm cô tỉnh giấc’
 
Nhưng anh ta đã đánh thức cô mất rồi. Cô về nhà và suốt đêm không ngủ được.
 
“Mình đi đâu đó đi”. Ngày hôm sau cô bảo với Nathaniel
 
“Đi đâu ạ?”
 
“Đâu đó thật xa”.
 
Họ bắt tàu điện ngầm đến Ashmont, điểm cuối của đường tàu đỏ. Cùng ngồi trên xe lắc và bàn luận về bánh rán.
 
“Con thích bánh rán mùi quế, nhưng ăn nó con hay bị ho” - Nathaniel bảo.
 
Ngủ trưa. Cô mơ thấy mình đang đi dạo với Nathaniel trong rừng thông. Cô bảo cậu đừng bước đến chỗ xác những con chim ruồi. Những con chim có lông cổ màu ngọc bích, một màu xanh lam chói lóa. Cô lấy trộm cuốn “Cuộc sống mới” nhưng hóa ra lại chỉ là cái bìa sách.
 
Chị cô gọi để hẹn gặp. Bạn cô là Jamie nói muốn giới thiệu Amanda cho một người quen biết. Amanda bảo: “Một ngày gần đây”
 
Jamie hỏi: “Chính xác cậu đang chờ cái gì?”
 
Bố của Nathaniel lờ cô đi. Amanda cũng giả vờ không chú ý đến đôi mắt đen của anh ta.
 
“Mẹ muốn hỏi con có kế hoạch gì vào tháng chín chưa?” - Mẹ Amanda hỏi cô qua điện thoại.
 
“Bố muốn hỏi con có dự định gì cho cuộc sống của mình chưa?” - Đến lượt bố Amanda.
 
Dante đã viết:
 
“Em, lữ khách trên đường yêu/ Chẳng biết anh vẫn dõi theo/ Bằng trái tim tan nát”
 
“Lúc nào con sơn xong mọi thứ - mẹ cô hỏi - căn hộ của con ấy?”
 
Bố cô bảo: “Bố đóng tiền ở Yale rồi”.
 
Cả ngày, Amanda và Nathaniel nghiên cứu về kiến lửa trên phố Buckingham. Họ thử rắc vụn bánh rồi quan sát mấy con kiến chạy lung tung để ăn. Nathaniel tự nhủ lớn lên cậu sẽ là nhà côn trùng học.
 
Ngày hôm sau, cậu quyết định không làm nhà côn trùng học nữa, cậu sẽ mở một cửa hiệu bán kem.
 
Họ đạp xe đến cửa hiệu kem Christina ở Quảng trường Inman. Nathaniel tự đạp cái xe nhỏ của cậu. Còn Amanda vừa trông chừng ô tô vừa đạp xe bên cậu.
 
Ở cửa hiệu kem, Nathaniel có thể đọc hầu hết các vị kem ghi trên bảng: đậu đỏ, mâm xôi đen, đường phèn, chuối sô cô la, cam sô cô la, bạch đậu khấu. Nathaniel nói: “Con ăn kem vani”. Họ ngồi ngay cạnh bảng thông báo, thấy trên đó dán chi chít quảng cáo lớp học ghi ta, gia sư và cả lớp dạy thiền.
 
“Thế nào là hiến trứng ạ?” - Nathaniel hỏi.
 
Một lần anh bảo với cô: “Anh muốn bên em suốt phần đời còn lại”. Rồi anh viết trên thiếp mừng sinh nhật cô: “Em là bạn thân nhất của anh. Em mang đến cho anh nụ cười, em mang đến cho anh tiếng cười”. Còn Dante thì viết “Tình khóc”.
 
“Con ăn kẹo cao su một tý được không?” - Nathaniel hỏi Amanda.
 
“Con vừa mới ăn kem”  - Cô đáp.
 
“Đi mà”.
 
“Không! Con vừa ăn kem. Con ăn kẹo để làm gì?”
 
“Đi mà, đi mà, đi mà!!!” - cậu bé năn nỉ.
 
“Cô vô cùng đáng yêu” - Bố Nathaniel thì thầm với Amanda khi anh ta mở cửa cho cô ra về vào đêm hôm đó.
 
“Đừng nói như vậy - Amanda thì thầm lại - Anh phải làm một bài thơ”.
 
Nathaniel nói rằng cậu sẽ biết cách làm thế nào nếu cảm thấy buồn. Cô bảo: “Nói cho cô biết đi, Nathaniel!”.
 
Cậu trả lời: “Đi đến vườn thú”.


Ảnh: Tam Nguyen
 
Nathaniel nghiên cứu lịch tàu chạy. Họ bắt tàu trên đường da cam đến ga Ruggles, sau đó bắt xe bus số 22 đến Vườn thú Franklin. Họ xem đười ươi ngồi chồm hổm, xé nát từng tờ báo. Họ trèo lên đài quan sát để nhìn mấy chú hươu cao cổ. Họ chạy men theo tất cả các lối mòn. Họ nhìn thấy rắn. Họ đến sân nuôi và một con dê đã làm Nathaniel sợ. Amanda bảo con dê chỉ muốn tới gần Nathaniel một tý thôi. “Bọn dê không ăn thịt con đâu”.
 
Trên đường về nhà, Nathaniel ngủ gục trên tàu điện ngầm. Cậu tựa vào Amanda và lim dim mắt. Người phụ nữ ngồi cạnh Amanda bảo: “Thằng bé thật xinh”.
 
Jamie tổ chức tiệc ở Somerville. Rượu vang thật khủng khiếp. Người bạn mà Jamie định giới thiệu cho Amanda bị say. Amanda cũng say, bữa tiệc trở nên vô nghĩa.
 
Ngày hôm sau cô đi làm muộn. Cô thấy Nathaniel ngồi đợi cô ở cổng nhà mẹ cậu. “Con nghĩ cô đã bị ốm” - cậu bảo.
 
“Đúng mà, cô đã bị ốm” - Amanda trả lời.
 
Cả hai đi bộ đến quảng trường Harvard xem ảo thuật đường phố. Rồi lại đi bộ đến nhà hàng Lê# ăn món gỏi cuốn mùa hè và uống trà lạnh của Thái.
 
“Vị này giống như viên phấn cam” - Nathaniel bảo.
 
Họ đi đến cửa hiệu tên “Little Russia” và nhìn thấy những con búp bê sơn mài.
 
“Nhìn xem, chúng có thể tách ra làm đôi nhé - Amanda bảo với Nathaniel - Con mở cô gái này ra, rồi sẽ thấy bên trong có một cô gái khác, rồi lại một cô gái khác, cứ thế”.
 
“Xin đừng chạm vào búp bê” - cô bán hàng bảo họ.
 
Hai cô cháu đi bộ xuôi dòng sông và ngồi trên thảm cỏ dưới một tán cây, tán phét về những con cún cưng đã nuôi.
 
“Labradoodle” - Amanda nói.
 
Nathaniel cười khúc khích: “Không, là Schnoodle”.
 
“Golden Streudel”.
 
Nathaniel hỏi: “Có phải đấy là con chó ngày xưa cô nuôi?”
 
Cô mơ thấy mình là một con búp bê Nga. Bên trong cô là bản sao thủa bé của cô, bên trong nữa lại là bản sao bé hơn.
 
Cô đặt mua một bộ búp bê gỗ trơn trên mạng và bắt đầu vẽ chúng. Đầu tiên cô sơn trắng. Rồi cô dùng những cái chổi đẹp nhất, bắt đầu vẽ bằng dầu bóng.
 
Con búp bê đầu tiên là một em bé cao vừa một inch, mặc áo tắm kẻ.
 
Con thứ hai, một cô bé học sinh bé nhỏ, đeo kính cận.
 
Con thứ ba, một cô sinh viên mỹ thuật, với cuộn giấy kẹp ở nách.
 
Con thứ tư, một cô dâu mặc váy trắng với mái tóc dài tuôn chảy.
 
Con thứ năm, một cô bảo mẫu mặc váy hai dây, đi sandal. Cô còn vẽ Nathaniel mặc quần sooc và áo phông có hình con tắc kè đứng trước cô. Cậu đứng ngang hông, tay cô đặt trên vai cậu.
 
Khi sơn khô, cô sơn nước bóng. Khi nước bóng khô, cô lại phủ thêm một lượt sơn bóng nữa cho đến khi tất cả màu sắc đều gợi lên cảm giác ăn được, những chỗ sơn xanh thì lấp lánh còn những chỗ sơn đỏ thì sáng rực lên như kẹo táo.
 
Cô mua thêm một bộ nữa và lại bắt đầu sơn phết từ đầu. Cô thức rất khuya để tô vẽ cho đám búp bê.
 
Chiều chiều Nathaniel hỏi cô: “Sao nhìn cô lúc nào cũng buồn ngủ?”
 
Sáng sáng mẹ cậu lại hỏi cô: “Sao cô luôn đến trễ vậy?”
 
Cô lăn ra ngủ với Nathaniel lúc đồng hồ mới chỉ tám giờ. Cô cuộn mình bên cạnh cậu ở giữa giường và thức giấc khi bố cậu về, giơ tay chạm vào má cô.
 
“Tôi tự hỏi, không biết cô có thể đến Cape cùng chúng tôi?” - Bố Nathaniel hỏi cô khi họ nhón chân đi trong phòng khách.
 
Cô lắc đầu.
 
“Chỉ vài ngày trong tháng tám thôi”.
 
Giọng anh ta hạ thấp. Mắt anh ta gần như van nài. Em quá đẹp - chồng sắp cưới của cô từng nói.
 
Cô vẽ bố của Nathaniel lên bộ búp bê Nga.
 
Đầu tiên, cô vẽ một em bé trai mặc quần yếm.
 
Tiếp theo, cô vẽ một bé trai trong bộ đồng phục nhỏ xinh của trường dòng, mặc quần sooc và đeo cà vạt.
 
Tiếp nữa, cô vẽ một chú rể, hớn hở trong bộ comle tối màu với bông hoa cài áo.
 
Sau nữa, cô vẽ một ông bố trẻ bế em bé Nathaniel trên tay.
 
Cuối cùng, cô vẽ một người đàn ông tóc muối tiêu, tay cầm một cái kính lúp. Cô vẽ bố của Nathaniel mập hơn và già hơn bên ngoài. Cũng không đẹp trai như vốn dĩ thế, mà với một cái bụng to như cái chuông theo khuôn hình của búp bê, nhìn anh ta giống như ông nội.
 
Như lần trước, cô sơn nước bóng cho đến khi màu sơn sáng lấp lánh.
 
Như lần trước, cô tỷ mẩn với từng con búp bê cho đến khi nó thật đẹp, thật hoàn hảo, nhưng cô vẫn mất nhiều thời gian cho con to nhất, con búp bê già nhất.
 
Sau đó, cô mua và vẽ thêm nhiều bộ khác. Cô vẽ những người thân quen và cả những người xa lạ. Một loạt chân dung về những người cô đã gặp, cứ một bộ có năm con. Cô vẽ Patsy, tóc càng lúc càng vàng hơn trong những hình hài khác nhau. Cô vẽ anh lúc là một cậu bé, lúc là một vận động viên, lúc là một sinh viên luật, lúc lại là một gã hói đầu bụng phệ, và cuối cùng là một cụ già ốm yếu hom hem. Nhưng cô không giết anh, cô chỉ làm cho anh già nua.
 
Cô xếp búp bê thành hàng thẳng rồi chụp ảnh. Cô nghĩ đến học bổng. Cô hình dung những buổi độc diễn và những buổi trình diễn theo nhóm. Và những buổi phỏng vấn vô nghĩa.
 
Cô đưa Nathaniel đến lớp học bơi. Cô đi ra cảng cùng cậu rồi họ ném bỏng ngô cho lũ mòng biển đang bay liệng trên bầu trời.
 
Bữa tiệc sinh nhật bảy tuổi của Nathaniel được tổ chức ở đảo Castle. Cậu cùng nhóm bạn đắp một thành phố bằng cát có tường hào bao quanh. Nathaniel làm kiến trúc sư. Amanda làm trợ lý cho cậu. Bố cậu chụp ảnh, mẹ cậu phục vụ bánh ngọt.
 
Cuối buổi tiệc, Amanda gom đống quà lại. Nathaniel sẽ đến Cape với bố, sau đó sẽ đến Vineyard với mẹ vào dịp nghỉ ngày Quốc tế lao động. Nathaniel nói: “Khi chúng ta quay về, lúc đó sẽ là tháng chín”.
 
Cô bảo: “Đúng rồi”.
 
Cậu hỏi: “Cô có đi với con không?”
 
Amanda trả lời: “Cô không thể. Cô đang sơn lại nhà”.
 
Cậu lại hỏi: “Màu gì?”
 
Cô nói: “Thực ra, cô đang chuyển nhà”.
 
“Chuyển đi xa?”
 
Cô bảo với cậu: “Con vẫn có thể gọi điện thoại cho cô”.
 
Nathaniel bắt đầu khóc.
 
Mẹ cậu bảo: “Con yêu!”
 
Cậu ôm chặt Amanda và khóc to hơn: “Tại sao cô không làm bảo mẫu của con nữa?”
 
“Cô sẽ đến New York” - Cô nói.
 
“Tại sao?”
 
Bởi mẹ cháu không thích cô, cô thầm nói với cậu. Bởi bố cháu muốn ngủ với cô. Bởi lý do duy nhất cô đến Boston là vì chồng chưa cưới của cô. Bởi câu hỏi đặt ra cho cô là cô sẽ làm gì với cuộc đời của mình. Nhưng cuối cùng cô chỉ nói được một câu rành rọt: “Vì đó là nơi cô đã ra đi”.
 
Cô quỳ xuống và đưa cho cậu tấm bản đồ cô vẽ. Cô đã lấy dao khứa lên mảnh giấy da để trông nó có vẻ như đã cũ.
 
Tấm bản đồ chỉ đường đến cái hang đầu nguồn của dòng sông Charles, xa xa là những cái thuyền vịt đang bay bay và những con chuột sô cô la ở cửa hàng Burdick. Cửa hiệu kem Christina, Ashmont và nghĩa trang với những cái răng rồng.
 
Mẹ Nathaniel ồ lên: “Đẹp quá”.
 
Bố Nathaniel nói: “Cô thực sự rất có tài”
 
Nhưng Nathaniel nói cậu không thích tấm bản đồ. Cậu nói rằng cậu sẽ xé rách nó.
 
Mẹ cậu bảo: “Nathaniel, đó là cách con đối xử với một món quà sao?”
 
Bố cậu bảo: “Thôi nào con”.
 
Nathaniel giật một miếng da từ tấm bản đồ. Cậu hét lên với bố mẹ: “Con không muốn bố mẹ”.
 
“Thằng bé mệt mỏi rồi - Mẹ Nathaniel nói với Amanda - Nó kiệt sức. Quá nhều sự phấn khích trong một ngày”
 
“Con không mệt” - Nathaniel hét lên, và cậu không cho Amanda đi. Cậu nửa như ôm chặt cô, nửa như xiết chặt cổ cô bằng đôi tay của cậu.
 
“Xem kìa, Nathaniel” - Bố cậu tỏ ra khó chịu.
 
Mẹ cậu ngắt lời: “Anh đang làm mọi thứ tồi tệ hơn đấy”.
 
Nathaniel khóc dữ dội. Cả người cậu rung lên. Không ai có thể dỗ cậu nín.
 
- Amanda nhắm mắt lại. Cô nói cô xin lỗi. Cô nói: “Nín đi”.
 
Cuối cùng cô đu đưa cậu trong vòng tay mình và bảo: “Cô hiểu, cô hiểu mà!”.
 
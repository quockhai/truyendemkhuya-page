CHINGHIX  AITMATỐP là nhà văn người Kirghixtan, thuộc Liên Xô cũ, sinh năm 1928, mất năm 2008, tác giả của nhiều tác phẩm nổi tiếng với bạn đọc thế giới và Việt Nam như: “Người thầy đầu tiên’, “Giamilia”, “Cây phong non trùm khăn đỏ”, “Và một ngày dài hơn thế kỷ”, “Đoạn đầu đài”…, đồng thời là một nhà hoạt động văn hóa, xã hội và chính trị có uy tín.

Lão Trôrđôn trở về nhà với một vẻ khó hiểu. Lúc thì lão bối rối và xúc động, lúc lão lại rầu rĩ, âu sầu - tóm lại là bà vợ có thể đoán được ngay: hẳn có điều gì xảy ra với ông lão. Và khi đã dò ra được nguồn cơn thì bà sửng sốt quá đỗi, không hiểu ra làm sao nữa. Trong nhận thức của những người bình thường, điều ông lão đang nghĩ chỉ có thể coi là một sự kỳ quặc, lẩn thẩn của người già, chứ tuyệt nhiên không thể là hành động tỉnh táo của một người còn sáng suốt.

Số là, ông lão vốn có một anh con trai, hai mươi năm trước đây đã hy sinh ở mặt trận. Anh ta chết lúc còn rất trẻ, và ngoài chính lão Trôrđôn ra, không ai còn nhớ đến anh. Mà ngay cả ông lão, trong suốt bao nhiêu năm sống chung, cũng hầu như chẳng bao giờ nhắc đến người con ấy. Thế mà bây giờ bỗng chốc ông lão nẩy ra quyết định sẽ đi tới nơi trước chiến tranh con ông đã từng làm giáo viên. 

- Lúc nào tôi cũng ngỡ như nó đang còn sống; ngỡ như giờ đây nó vẫn còn ở đấy. Nó gọi tôi tới đó, và tôi  muốn gặp được nó - lão nói vậy. 

Bà vợ sợ hãi nhìn lão, thoạt đầu muốn bật cười: "Ông vẫn tỉnh táo đấy chứ, không lú lẫn đấy chứ?" Nhưng bà kìm được ngay. Qua đôi mắt ông lão nhìn bình thản và giản dị, qua giọng nói tin tưởng và chân thành của ông lão khi nói những lời ấy, bà đã hiểu rằng ông lão hoàn toàn nghiêm túc và chắc chắn là tỉnh táo. Vào phút ấy, bà cảm thấy  như có lỗi trước tất cả những khát khao phi lý đang dày vò ông lão như một đứa trẻ, lão - cái con người với gương mặt màu nâu già nua, với những nếp nhăn nheo nơi đôi mắt phúc hậu, với hàm râu bạc trắng, với đôi tay to lớn, mệt mỏi đang để yên trên đầu gối như những con cá lớn. Bà cảm thấy thương ông lão và nhận thấy tất cả sự vớ vẩn của những dự định điên rồ ấy. 

- Nếu vậy, tại sao trước đây ông không tới đấy? - Bà thận trọng hỏi. 

- Tôi cũng không biết nữa, - Lão Trôrđôn thở dài đáp - Bây giờ mới thấy cần. Tôi phải tới đấy chừng nào còn sống, trái tim tôi bảo thế. Sáng mai tôi sẽ lên đường. 

- Tuỳ ông thôi, việc của ông mà. Bà cứ đinh ninh rồi ông lão sẽ nghĩ lại. Bởi thật ra thì lão đến  đấy để làm gì, lão có thể tìm thấy gì ở cái xóm núi xa xăm ấy, lão sẽ thấy được gì ở đấy? Nhưng mong muốn của bà không trở thành sự thật. Lão Trôrđôn không thay đổi ý kiến. 

Cả cái làng ven núi này đã ngon giấc từ  lâu, mọi khung cửa sổ đều tối om, chỉ riêng trong nhà lão Trôrđôn vì việc ấy mà vẫn còn ánh lửa. Ông lão trằn trọc không yên. Suốt đêm,  mấy lần ông lão trở dậy mặc quần áo đi ra sân, và mỗi lần lại ném vào máng ngựa nửa bó cỏ. Mà không phải thứ cỏ vớ vẩn, lão chọn toàn thứ tốt nhất, vừa mới thu hái. Và lão cũng không dè xẻn dỡ cả từ cái kho cỏ khô để dành trên mái nhà hàng năm nay. Vào lúc khác, chắc  lão đã chẳng cho phép mình hào phóng đến thế: đến tận mùa đông lão cũng chẳng hề dám động đến chỗ cỏ khô - và bò và ngựa lão xua hết ra bên kia sân, dọc bờ rào trơ trụi, những đám đất lún phún cỏ mới mọc hay ra ngoài cánh đồng vừa gặt trơ chân rạ. Vậy mà bây giờ thì ông lão chẳng tiếc một cái gì hết, kể cả chỗ lúa kiều mạch giành đi đường đã chất đầy trong đó. 

Cứ thế ông  lão lục sục suốt đêm, và bà lão cũng chẳng thể nhắm mắt. Bà cứ giả vờ ngủ để khỏi quấy rầy công việc của lão, và khi lão bước ra khỏi nhà bà cứ thở dài sườn sượt. Thuyết phục ông ấy cũng vô ích. Lúc này nếu có thể bà cũng chỉ  nói được với ông lão: "Ông cứ nghĩ xem, ông sẽ đi đâu? Làm gì? Khéo không ông hoá ra trẻ con và mọi người sẽ cười cho đấy!". Nhưng bà im lặng. Bà sợ rằng ông lão sẽ nói với bà thế này: "Nếu bà là mẹ đẻ của nó, bà đã chẳng khuyên tôi như thế". Bà không muốn nghe những lời như vậy. Phải, bà chưa hề nhìn thấy con trai ông lão, bà chẳng biết anh ta ra sao. Bà vợ cả lão Trôrđôn chết cách đây mười năm, còn bà là vợ thứ của ông lão. Chẳng có điều ấy thì bà cũng đã đủ khó xử rồi, bà lúc nào cũng cảm thấy như có lỗi gì với ông lão, vì hai cô con gái của ông lão đã có chồng và đang sống với gia đình ở thành phố không hiểu tại sao không bao giờ tới thăm lão, không giữ quan hệ nào với  ông lão cả ngoại trừ một đôi lần, rất thảng hoặc, chính lão Trôrđôn có ghé thăm chúng những lúc lão có việc vào thành phố. Lão không hề đả động đến chúng bao giờ và bà lão cũng cố tránh không hỏi han gì đến. Trong trường hợp ấy, người dì ghẻ cứ đứng tách riêng ra là yên ổn hơn cả. Chính một phần vì thế mà bà đấu dịu với cái ý định kỳ quặc của ông lão. Sau rốt, bà ngẫm đi nghĩ lại và đi đến quyết định: "Con người cũng có thể có nỗi buồn đến như vậy. Cứ để lão tới đấy ít lâu cho tâm  hồn dịu lại, cho nguội bớt trái tim. Nỗi buồn của lão sẽ lắng xuống". 

Lão Trôrđôn trở dậy vào tảng sáng. Lão đi đóng yên ngựa, rồi trở vào mặc chiếc áo trépken mới, cầm lấy cái túi vải trên đường, đoạn cúi xuống khoảng tối nơi giường bà vợ mà thì thầm: 

- Tôi đi nhé. Bà nó đừng sợ, chỉ nội đêm mai là tôi về. Bà nó nghe thấy chứ?  Tại sao bà im lặng, hả? Bà nó phải hiểu: nó là con tôi. Tôi biết hết mọi điều, nhưng tôi vẫn phải tới đấy một chút. Lòng tôi đau đớn lắm, bà nó hãy hiểu… 

Bà vợ lặng lẽ nhỏm dậy và trong bóng tối, bà nhìn thấy ông lão đang đội cái gì lên đầu, liền càu nhàu: 

- Ông xem cái mũ của ông kìa. Vá chằng vá đụp. Ông đi thăm thú chứ có phải đi kiếm củi đâu - Bà sờ soạng mở cái nắp hòm đầu giường, lấy ra cái mũ da cừu được cất kỹ và trao cho ông lão - Ông đội đi. Cái mũ tàng kia không tiện đâu. 

Lão Trôrđôn đổi mũ và bước ra cửa. 

- Này ông , - bà lại gọi - Ông cầm lấy túi thức ăn để thành cửa sổ cho vào dỏ đi. Kẻo đến chiều tối lại đói dọc đường đấy. 

Lão Trôrđôn những muốn nói "Cám ơn" nhưng rồi lại làm thinh: lão không quen nói "cám ơn" với vợ. 
Làng xóm vẫn yên ngủ khi lão Trôrđôn cưỡi con ngựa cái đi qua sân sau ra đường để  khỏi đánh thức lũ chó, rồi vòng lại theo con đường làng đi vào núi.

*
Buổi trưa ngày hôm qua, cũng trên con đường này, với con ngựa cái này được thắng vào cỗ xe cũ kỹ, nứt toác, lão Trôrđôn chở củi về nhà. Phải tích trữ chất đốt cho mùa đông.

Lão chặt những lùm cây khô ở lối vào Khe Nhỏ, chất đầy có ngọn lên  chiếc xe ngựa hai bánh, rồi leo lên yên, gác chân lên càng xe, lão nhẩn nha đánh xe ra đường cái. Tiếng bánh xe quen thuộc vang lên lọc sọc trên những ổ gà. Chiếc xe kêu cót két. Một ngày bình yên và ấm áp. 

Vẫn thường như vậy, vào mùa thu, khi thời tiết dần dần chuyển lạnh, thì trước đó, như để chia tay, có những ngày bỗng bừng lên một vẻ trong sáng và quang đãng hiếm thấy. Vào lúc ấy, từ chân núi  có thể nhìn thấy rõ tất cả một vùng lân cận: những làng mạc dưới thung lũng, với những khu vườn u ám mà trong suốt, và những bức tường nhà trắng xoá, màu đỏ sẫm của những nương thuốc lá, những chiếc máy kéo đang cày xới cánh đồng đất ải, chiếc máy bay trắng bạc bay qua khoảng không, và tận phía chân trời một vệt khói xám bất động in trên vùng đất thấp của thành phố. Và trên tất cả cái thế giới ấy là đợt sóng những bầy chim cứ lượn vòn vun vút lặng lẽ và đen sẫm. 
    
 Lão Trôrđôn biết: đó chính là chim én. Buổi sáng, lúc lão đi kiếm củi, chim én bay từng đàn. Chúng đậu thành dãy dài trên đường dây diện, đối diện nhau, bất động, phơi cái ngực trắng như nhau, với những cái đầu nhọn như nhau và những chẽ đuôi lấp lánh cũng hệt như nhau. Chúng đậu yên lành, thỉnh thoảng truyền nhau những tiếng hỏi khe khẽ và dường như cùng đợi chờ đến một phút nào đó sẽ nhất tề cùng bay vút lên đường. Những lúc ấy trông lũ chim én có cái gì long trọng bởi dáng dấp trang nghiêm và một vẻ đẹp tuyệt vời. "Đây đâu phải là lũ chim sẻ" - lão Trôrđôn kiêu hãnh suy nghĩ.

Vào lúc này,  trước mắt ông lão, lũ chim én đang bay đi. Chúng lượn những vòng tạm biệt trên mảnh đất mà mùa hè vừa đi qua. Chúng lặng lẽ lao vun vút giữa không trung thành những mảng đen lấp lánh, mênh mông che lấp cả mặt trời. 

Lão Trôrđôn mải miết nhìn theo những cánh bay của chúng. Và kia, chúng còn vẽ nốt một lần nữa chiếc vòng lớn cuối cùng trên khu vườn hoang vắng của mùa thu, đoạn chúng như lưỡng lự giây phút, chuyển hướng, xếp lại thành hàng lối và lao nhanh về phía thảo nguyên mênh mông.  Đàn chim cứ bé dần, bé dần, lẫn vào nền trời xanh biếc, như dư âm của bài ca xa xăm ở giữa thinh không và cuối cùng chỉ còn lại một chấm đen nhỏ xíu. Bầy chim én đã bay về những miền xa lạ. Vừa ban sáng còn nhìn thấy chúng, ngắm nghía và lắng nghe giọng hót của chúng. Và thốt nhiên một nỗi buồn ngọt ngào, khó hiểu dâng lên trong lòng như một con sóng ngây ngất, giọt lệ tràn trên mi mắt, ông lão  chẳng còn nhìn thấy gì nữa, nhưng vẫn ngó mãi lên trời và bâng khuâng tiếc nuối cho một cái gì thân thiết, gần gũi đã mất đi không bao giờ trở lại. Giá như hồi còn trẻ, hẳn lão đã hát lên một khúc ca tiễn biệt. 

Lão Trôrđôn chợt dứt mạch suy nghĩ khi nghe có tiếng chân ngựa gần đâu đây. Có ai đó đang cưỡi một con ngựa khoẻ phóng lên đồi. Lão Trôrđôn  không nhận được ra ngay là ai. Thì ra đó là ông già Xaparalư ở làng bên. Cả hai ít quen biết, chỉ thỉnh thoảng gặp gỡ chào hỏi nhau trong những buổi ma chay,  giỗ chạp và thật ra  sự quen biết của họ cũng chỉ đến thế. Ông già Xaparalư chắc là đang đi thăm ai. Ông khoác chiếc áo trapan mới bằng vải sợi, chân đi đôi ủng mũi nhọn cũng mới tinh, đầu đội mũ lông cáo, tay khoác chiếc túi vải quai xách làm bằng gỗ thanh lương trà màu đỏ. 

- Nghĩ ngợi gì thế, hả ông thợ rèn? - Ông già Xaparalư ghìm cương ngựa và cất tiếng vui vẻ chào rất to. 
Phải, có dạo lão Trôrđôn đã làm nghề thợ rèn.

- Lũ chim én đang bay đi đấy - Ông  lão Trôrđôn  lúng túng nói giọng rời rạc. 

- Cái gì? Én ư? Chúng đâu nào?, 

- Chúng bay hết rồi. 

- Ồ, mặc cho chúng bay. Ông đi chở củi đấy à? 

- Phải, dự trữ cho mùa đông mà. Còn ông thì đi đâu vậy? 

Ông già Xaparalư nở nụ cười thoả mãn trên gương mặt đỏ gay trông còn trẻ trung với hàm râu đen nhánh: 

- Đi thăm thằng con. Chả thằng cháu nhà tôi làm giám đốc nông trường Ácxai, miệt Núi Lớn mà. 

Và ông già Xaparalu khua cái túi  vải thành một vòng rộng, chỉ về phía kia. 

- Tôi có nghe nói về Ácxai, phải, có nghe - Lão Trôrđôn gật đầu. 

- Tôi đi tới đấy đây. Con trai tôi nhắn: "Hai hôm nữa, bố phải tới đây ngay". Nó bảo thế. Dù ở đấy chúng có là thủ trưởng đi nữa, thì vắng bọn già chúng mình, chúng cũng chẳng  biết xoay xở ra sao. Chả là thằng cháu trai cưới vợ.  Nó cần tôi giúp đám cưới, vì theo tục lệ khách khứa sẽ rất đông, tôi phải lo tổ chức cuộc đua ngựa. 

Và ông già Xaparalư bắt đầu kể về công việc ở nông trường của con trai ông, rằng hiện nay ở đấy lượng thu hoạch lông cừu rất cao, những người chăn cừu được lĩnh rất nhiều tiền thưởng, rằng mọi người đều vừa lòng với ông giám đốc, có tin đồn là người ra định tặng huân chương cho anh ta nữa. 

Tất cả những điều ấy đều rất hay, nhưng lúc này thì lão Trôrđôn  lại đang nghĩ đến chuyện khác: ông lão lại chợt thấy nhói lên nỗi buồn xưa cũ bị nén trong lặng yên muôn thủa tận đáy sâu tâm hồn, nhưng vẫn sống, vẫn không nguôi trăn trở, cái nỗi buồn  mà mỗi bận chim ém bay đi lại thức dậy trong lòng lão một nỗi đau da diết. Và lúc này đây với một sức mạnh mới lại dâng lên trong ngực lão như một ngọn lửa rừng rực. Đó chính là nỗi buồn về đứa con trai, phải, đứa con trai đã từ lâu rời khỏi thế giới này. Nhưng đã có một thời nó làm việc ở đấy, gần Ácxai và cũng đã từng mời bố đến thăm nó. Và thốt nhiên như trong mê sảng, chẳng hiểu mình định nói cái gì, lão Trôrđôn lên tiếng ngắt lời ông già Xaparalư: 

- Thằng con tôi cũng mời tôi. 

- Con ông cũng  ở đấy ư? 

- Phải - lạnh toát người vì kinh sợ, lão Trôrđôn đáp thì thầm. 

- Ồ thế mà tôi không biết, - ông già Xaparalư vô tâm nhún vai - Nếu vậy thì hay lắm. Dù ở đâu đi nữa cũng cầu chúa cho cậu ấy sức khoẻ. Tạm biệt nhé! - Sau mấy lời ấy, ông già Xaparalư liền thúc ngựa đi. 
Và ngay khi ông già rời chỗ, lão Trôrđôn như chợt tỉnh. Cả một vực thẳm mênh mông lặng im và trống rỗng chấn động tâm can lão như sét đánh: "Mình đã nói gì vậy? Dối trá, mình đã ăn nói dối trá! Mà để làm gì nhỉ?..."

Lão Trôrđôn nhảy xuống đường  và chạy đuổi theo ông già Xaparalư: 

- Dừng lại, dừng lại đã, ông Xaparalư! - Lão hét lên và chạy theo ông già định để xin lỗi, để cải chính. 

Ông già Xaparalư quay ngựa lại: 

- Sao, ông làm sao thế? - Ông già lo lắng. 

Ông lão Trôrđôn chạy tới, thở hổn hển, những muốn giãy bày hết mọi chuyện, nhưng một sức mạnh không cưỡng nổi lại xuất hiện - lòng khát khao được nghĩ về đứa con như nó còn sống, sau khi lão vừa tình cờ làm cho nó sống lại trên cửa miệng của mình và trong ý thức của một người khác, đã không cho phép lão nói sự thật. Lão không thể chôn con mình một lần nữa. Lão không thể nói rằng con mình đã từ lâu đã không còn, rằng nó từ lâu đã ngã xuống trong chiến tranh. Lão muốn cho con sống thêm chút nữa, dù chỉ là vài phút. Rồi sau đó nói cũng còn kịp… 

- Ông có thuốc  hút không? Tôi sắp chết vì thèm thuốc đây,  xin ông một  ít - lão Trôrđôn  yêu cầu. 

- Ồ, quỉ bắt ông đi, làm tôi sợ chết khiếp! - Ông già Xaparalư thở dài nhẹ nhõm và sờ túi tìm thuốc. - Ông đưa tay đây. Tôi cũng chả lạ gì cái trò thuốc men chết tiệt này - Ông ta nói thêm và dốc từ cái lọ thủy tinh ra tay lão Trôrđôn  một dúm thuốc - Làm sao tay ông run thế, ông thợ rèn?  Già nua rồi đấy. 

- Phải, quai búa suốt trong chiến  tranh, già cũng phải thôi! Lão Trôrđôn  đáp- Xin lỗi vì đã làm phiền ông. 


- Ồ không sao. Thôi, tôi đi nhé. 

- Chúc ông thượng lộ bình an, -  lão Trôrđôn nói. 

Bây giờ thì chẳng tiện giữ ông ta lại nữa, và lão Trôrđôn thậm chí vui mừng vì ông già Xaparalư đã vội vã biến đi ngay khiến lão khỏi phải nói với ông ta về cái chết của thằng con. 
Sau khi ông già Xaparalư đi khỏi, lão Trôrđôn còn đứng trầm ngâm một lúc trên đường, đoạn lão xoè tay đổ nắm thuốc xuống đất và quay lại xe.

Lão bước chậm rãi, đầu cúi gầm "Mình làm sao thế nhỉ, mình điên rồi!". - Lão lẩm bẩm và dừng lại giữa đường, mơ màng nhìn quanh, lão đăm đăm ngắm mãi khoảng trời cao trên thảo nguyên bao la, nơi bầy chim bay đến, miệng thì thầm: "Không, tôi có một đứa con, phải, nó vẫn sống, rồi đột nhiên lão kêu lên, khàn khàn và đau đớn: - Tôi có một thằng con, đúng thế, tôi cũng sẽ đi thăm nó, tôi phải nhìn thấy nó, phải nhìn thấy". - và lão lại im bặt. 

Trên dọc đường về làng, lão Trôrđôn cố thuyết phục mình đừng xúc động, rằng mọi sự đã qua không bao trở lại, vậy mà nỗi ước được đến thăm cái xóm núi ấy vẫn như ngọn lửa rừng rực thiêu đốt tâm can lão. Quả là như  vậy. Cái ngọn lửa ấy từ bao năm nay vẫn cháy âm ỉ trong tâm hồn lão. Lão luôn luôn mơ màng, háo hức tơ tưởng được tới thăm nơi ấy để chiêm bái cái mảnh đất mà con trai lão đã sống những ngày cuối cùng trước khi nhập ngũ.  Và cuộc gặp gỡ với ông già Xaparalư chỉ là một cái cớ tình cờ. Giờ đây, trong ý thức của lão Trôrđôn, đứa con trai đã sống lại với một vẻ rất riêng, những năm tháng bị đảo lộn một cách kỳ quái, điều mong ước trở thành có thật,  tưởng tượng biến thành hiện thực. Lão hình dung như đang đi tới cái xóm núi ấy, như đang gặp con và hai người chuyện trò với nhau. Chắc hẳn nó sẽ vui sướng: "Bố, thế là cuối cùng bố đã tới!" - Và nó sẽ chạy bổ đến với lão. 

"Bố tới đây, con thân yêu của bố, chào con! Con vẫn thế. Mà bố thì, còn xem đấy, bố bắt đầu già rồi". 

"Ồ, không, bố ơi, bố chưa già đâu, có điều thời gian trôi qua cũng đã nhiều. Tại sao lâu thế bố không đến với con? Bao nhiêu năm rồi còn gì: hai mươi, hình như còn hơn nữa. Hay bố không buồn nhớ gì con?" 

"Lại còn không buồn ư? Bố buồn đến trọn đời con ạ. Con hãy tha thứ vì bố bắt con chờ mãi mà không thể đến được. Con cũng biết đấy, mẹ con đã mất rồi - mọi người đã lo cho mẹ con mồ yên mả đẹp. Sau khi con hy sinh ở chiến trường, mẹ con nằm liệt giường và không dậy nữa. Mãi bây giờ bố mới tới được để mặc niệm con đây. Bố cũng tới để chào những người đã từng sống bên con. Bố muốn chào mảnh đất này, những ngọn núi này, bầu không khí mà con đã thở, dòng nước mà con đã uống. Thế là bố con mình gặp nhau rồi, con trai của bố nhỉ. Con xem, nào, hãy chỉ cho bố ngôi trường của con, cái xóm núi của con nữa, con đã kể  bao nhiêu chuyện về nó mà…". 

*
Lão Trôrđôn cố moi óc nhưng vẫn không sao nhớ được tên người thợ săn mà Xultan, con trai của lão đã từng sống với ông ta. Lão chỉ biết rằng đó là một người tốt, con lão rất mến ông ta, bây giờ dễ ông ta cũng đã gần bảy mươi. Liệu ông ta còn sống hay đã chết? Ông ta vẫn thường nhắn lời mời lão Trôrđôn tới  thăm, cùng đi săn bằng chim đại bàng. Liệu con chim ấy có còn chăng? Đại bàng vốn sống lâu lắm.

Phải rồi, ông thợ săn có một đứa con nhỏ. Thằng bé học Xultan ở lớp một, và khi sang lớp hai thì chiến tranh nổ ra. Có lẽ thằng bé ấy bây giờ đã có gia đình. Bà vợ ông thợ săn cũng là một người đàn bà tốt bụng. Nhưng bà quá tất bật: nào việc ở nông trường, nào việc nội trợ ở gia đình. Bà ta đã từng đề nghị Xultan mang một đôi chó săn về cho bố. Bà kiệt lực vì phải nấu cháo  cho cả đàn chó. Có một lần Xultan đã mang đi con chó săn trắng có những đốm vàng ở hông. Ôi chao,  đấy mới thật là chó! Chỉ ở cái vùng núi tuyết Lớn ấy mới  có loại chó săn nhanh như cắt, và đẹp đến thế. Chúng lùa lũ dê hoang vào bẫy. Vậy mà một buổi sáng Xultan lại mang con chó về. "Bực mình quá, ông chủ ạ, - cậu ta nói - Cứ để tôi đi nấu cháo còn hơn". Và con chó săn tuyệt vời liền phóng theo  chiếc xe đạp. Lão Trôrđôn cũng tiếc phải chia tay với con chó, nhưng lão hiểu rằng đối với người thợ săn, con chó là cả một điều cực kỳ quan trọng mà lão thì chẳng hề  bò ra khỏi cái xưởng lò rèn. Lũ quỷ trắng ấy chẳng biết có bị bán chác chưa hay bây giờ vẫn còn đang săn cáo?. 

Mải nghĩ tới chuyện ấy lão Trôrđôn lại tìm thêm cho một được một cái cớ xác đáng.  Đúng, lão phải tới thăm người ấy. Nếu ông ta chết, thì nghiêng mình trước nắm tro tàn của  ông, còn nếu ông vẫn sống - thì  để xiết chặt tay ông, cảm ơn ông đã chăm sóc con mình. 

Duy chỉ có một điều là lão Trôrđôn cấm mình tuyệt nhiên không được nghĩ đến. Mỗi khi cái ý nghĩ ấy chợt loé lên là lão lại át đi ngay bằng đủ mọi thứ lo toan khác: nào là ước tính giá cả khoai tây và cỏ khô trong vụ đông này, nào là bao giờ cắt lông cừu thì tốt nhất, nào là nên để lại hay bán con bò cái tơ đi… 

Lão cố gắng để không nghĩ tới nữa, vì rằng tất cả điều đó lão đã  từng nghĩ tới, thậm chí đã suy đi tính lại. Trong suốt những đêm không ngủ, những lúc bên bệ lò rèn, đã bao nhiêu năm ròng, cái ý nghĩ ấy đã cùng cây búa nện xuống mặt đe, đã nung trong lò lửa và tôi trong nước lạnh. Và cũng từ lâu lão đinh ninh rằng  chỉ có một mình chúa trời mới phán xét được là lão đúng hay sai. Nếu như lão còn được gặp lại con trai ở thế giới bên kia thì lão cũng sẽ kể hết mọi điều như nó đã xảy ra… Nhưng lão không hề cầu xin tha thứ, không hề. Ngay như sau dạo ấy, khi những cô gái lão ở ngoài thành phố ném vào mặt lão những lời gớm ghiếc, lão Trôrđôn cũng không hề hối hận,  lão cũng chỉ  im lặng… 

Và cho đến hôm nay họ vẫn không thể tha thứ cho lão cái điều khủng khiếp ấy. Điều đó xảy ra  vào những ngày Xultan chuẩn bị lên  đường ra mặt trận ở nhà ga thành phố. 

Vào một ngày tháng Mười năm 1941, có người chay đến lò rèn tìm lão Trôrđôn: về nhà nhanh lên, con trai ông về chia tay đấy. Lão Trôrđôn vội vội vàng vàng khoác nguyên cả cái tạp dề  thợ rèn ám khói, thủng lỗ chỗ. Bên tai lão vẫn còn vang lên tiếng đe tiếng búa.  Lão bước nhanh trên đường mà trong bụng vẫn hoàn toàn chưa tin; con trai lão còn ít tuổi, đâu đã đến lúc gọi lính. Nhưng té ra lại là chuyện thật. Xultan cưỡi một con ngựa mượn của ai đó vừa từ huyện lị về để gặp bà mẹ đang đau ốm. Đã nửa năm nay bà lão vẫn không sao bình phục. Cậu ta đề nghị bố ra thành phố để tiễn cậu tại nhà ga. Họ trò chuyện câu được câu chăng và cũng chẳng kịp xem nên chia tay ra sao. Thời gian lúc này chỉ có vậy. Biết  bao lời chưa nói hết, biết bao điều còn trong ý nghĩ của mỗi người chưa kịp nói ra. Không dễ có ai diễn đạt nổi tất cả những gì trong tâm hồn của nhân dân vào những ngày hôm ấy… 

Lão Trôrđôn cho ngựa phi nước kiệu vào thành phố. Con ngựa băng qua suốt quãng đường ba mươi cây số suýt kiệt sức dọc đường. Điều đầu tiên đập vào mắt lão và khiến lão đinh tai nhức óc là một biển người náo động trước sân ga. Chẳng biết ai vào ai nữa! Những chiếc xe tải căng băng đỏ trên thành xe, những chiếc xe ngựa lớn, nhỏ chất đầy cỏ khô và rơm, những con ngựa thả rông hoặc đóng yên cương, còn trên các ngả đường tiếng còi tàu thét vang, tiếng toa tàu loảng xoảng. Và ở giữa tất cả cái mớ hỗn độn đó là nhân dân, từ các làng, các bản, từ thành phố kéo về, cụ già có, thanh niên có, trẻ em có… 

Lão Trôrđôn rất vội. Lão buộc ngựa vào chiếc xe bắt gặp đầu tiên, giữa bao nhiêu con ngựa khác và chạy đi tìm con. Lão vừa chen giữa đám đông, vừa hỏi dò. Người ta cho biết là số lính mới đang ở chỗ  công viên gần ga, họ phải tập hợp riêng ở đấy không được đi đâu, vì sắp đến giờ lên đường. Lão liền tìm tới công viên. Ở đấy, sau hàng rào, tất cả đã tập họp thành hàng lối, đang điểm danh, những khẩu lệnh vang lên. Lão Trôrđôn đứng lúng túng: Làm sao tìm nổi ai  ở đây được? Lại thêm đám cây cối che khuất cả hàng quân. Bỗng nhiên lão nghe có tiếng gọi gần đây: "Bố ơi, bố, lại đây!". Cả hai cô con gái giơ tay vẫy lão. Chúng  đang  đứng ở lối vào công viên. Lão lách lại phía chúng và nhìn thấy thằng con đứng trong hàng quân ở bên kia bờ rào. Và thằng bé cũng  nhận ra bố, liền vẫy lão và cười. Nụ cười của nó có vẻ gì dè dặt và lúng túng.  Lão bỗng thấy thương con. Nó còn quá trẻ, chưa có ria mép, chỉ có vóc dáng là bằng người khác, còn đôi vai và gương mặt thì vẫn là trẻ con. Nó còn lớn và phải lớn nữa. Lão cứ ngẫm mình mà đoán là nó sẽ phải lớn lên thành một trang nam nhi chắc chắc, vạm vỡ. Chỉ hai năm nữa thôi, nó sẽ là một chàng trai lực lưỡng. 

Sau này những lúc nghĩ về con, lão Trôrđôn nhiều lần cứ muốn tìm hiểu cho ra nhẽ xem ở nó có cái gì khiến cho lão phải kính nể, lão không chỉ yêu con như người ta vẫn yêu, mà là kính nể nó ngay từ tuổi thơ như một người thông minh, biết xử sự chững chạc, mặc dù nó vẫn chỉ là một đứa bé nghịch ngợm, ngoan ngoãn mà thôi. Rốt cuộc lão vẫn không giải thích được tại sao lại như vậy. Đặc biệt là từ khi nó trở thành giáo viên. Lão Trôrđôn  hết sức kính trọng nó, xử sự với nó một cách nghiêm túc,  mặc dù nhiều lúc nó về nhà quên cả cởi khăn quàng đỏ, - chả là ở trường nó làm  phụ trách đội mà. Cậu bé vẫn còn bồng bột, xốc nổi, nhưng lão tin với thời gian, nó sẽ trở thành người vững vàng. Nó cũng chẳng cần  ai giao huấn, dạy dỗ gì nhiều. Tự nó sẽ tìm được đường đi của mình, lão Trôrđôn  nghĩ thế. Cũng có thể chính vì điều ấy mà khiến lão đã xảy ra va chạm với mấy cô con gái. Cả hai, cô chị Dâynhes và cô em Xalica, đều học ở thành phố. Cả hai đã lấy chồng và trở thành người thành phố. Họ đã mang Xultan ra thành phố, ở đấy cậu đã tốt nghiệp trường trung cấp  sư phạm và đã ra dạy học được một năm. 

Lúc lão Trôrđôn cuối cùng đã lách được tới chỗ các cô con gái của lão, thì không hiểu tại sao họ lại kéo lão ra khỏi đám đông. Rồi bằng một giọng thầm thì độc địa, họ bắt đầu rỉa rói, mắng chửi cậu em nào là thằng chơi trội, đồ oắt con, thằng ngốc. Xung quanh chật ních những người, vì vậy các cô con gái vội vã và nóng nảy kéo thẳng ra quảng trường, và trong cảnh chen chúc, xô đẩy, giữa họ  và ông bố đã xảy ra  cuộc trò chuyện như sau: 

- Bố có biết là thằng con bố tình nguyện nhập ngũ không? 

- Không, - Lão Trôrđôn ngạc nhiên. Nhưng sao kia? 

- Chúng con đã gọi điện về huyện,  chỗ ban tuyển quân, người ta cho biết là chính nó đã đề nghị, đã tuyên bố tình nguyện ra trận. Bố hiểu chứ? 

- Nó chưa đến tuổi, bố hiểu điều ấy rồi chứ? 

-  Thế nghĩa là nó thấy cần thiết đấy. 

- Cần thiết ư? - cả  hai cô con gái tranh nhau tấn công bố - Bố không hiểu gì hết, bố ạ. Chồng chúng con có mặt ở đấy là đủ rồi. Mà cũng chẳng biết các anh ấy có trở về hay không. Mình chúng con chịu cảnh đơn chiếc là đủ rồi, bây giờ  còn nó là người cuối cùng trong gia đình lại tự mang thân ra trận. 

- Nó sẽ mất tăm ở ngoài ấy như con chim non. Nó ra đấy không phải để làm  anh phụ trách đội đâu. 

- Bố, tại sao bố lại im lặng? 

- Nhưng bố biết nói gì, làm gì bây giờ? 

-  Đi ngay tới chỗ nó, chúng con sẽ xin  cho bố được vào. Bố hãy đi vào bảo nó không được làm thế. Bây giờ cũng chưa muộn, bố phải thuyết phục nó. 

- Dù nó từ chối, bố vẫn còn  kịp bố ạ. Chỉ có bố, mới khuyên nhủ được nó. 

- Không, hãy gượm đã, - Lão Trôrđôn  lẩm bẩm. Trong tình trạng láo nháo ấy, lão rất khó giải thích cho các cô con gái rằng việc đòi hỏi ở một con người điều ấy là không thể được. Nó làm sao từ bỏ được lời hứa của mình? Nó làm sao còn nhìn được vào mắt của những người đã đứng cùng nó trong một đội ngũ? Họ sẽ nghĩ về nó ra sao và tự nó  rồi sẽ nghĩ về chính mình thế nào? 

- Không tiện đâu các con ạ, nó sẽ xấu hổ đấy - Lão Trôrđôn nói. 

- Xấu hổ cái gì? 

- Ai có việc thì biết, chứ lạy chúa, có ma nào thèm để ý đến nó! 

- Nhưng chính nó sẽ tự biết về  mình, - Lão Trôrđôn cáu kỉnh, - Điều đó mới quan trọng. 

- Thôi, bố ạ, nó còn là đứa bé mà. Đi đi, bố đi đi, còn chưa muộn đâu. 

Vừa lúc ấy cả đám đông chuyển động, ồn ào dạt về một phía. Dàn nhạc hơi tấu vang bài hành  khúc, lá cờ đỏ vút lên và từ cổng công viên những hàng quân bước ra. Đã đến giờ lên tàu. Các cô gái càng níu chặt lấy tay áo lão Trôrđôn , giục giã lão, họ kêu thét lên giữa hàng trăm ngàn tiếng người, tiếng nhạc: 

- Phải đến gặp ngay ông Uỷ viên phụ trách, bố ơi! Ông ta đang ở trong ga kia kìa. Bố phải cứu lấy nó. 

- Bố ơi, phải làm tất cả vì mẹ chúng con đang đau yếu. Bố hãy nói với ông uỷ viên về mẹ, bố phải nói là bà cụ sắp chết rồi. 

Những lời này đã làm lão Trôrđôn giao động. Và họ kéo  ông lão băng qua đám người đưa tiễn đến thẳng nhà ga nơi có người uỷ viên phụ trách hành quân làm việc. 

Những bậc tam cấp cao bằng đá dẫn từ quảng trường lên toà nhà ga. Và suốt từ dưới lên trên, các bậc thềm chật ních những người. Các cô con gái cứ kéo lão đi ngược lên giữa biển người hầm hập, nhễ nhại mồ hôi, ngang qua trăm ngàn cặp mắt đã mờ đi vì đau  khổ của chiến tranh, qua những giọt lệ, nét can đảm cũng như tuyệt vọng của những con người đang ly biệt nhau, qua tiếng trống và tiếng nhạc chiến chinh, qua tiếng kêu từ biệt của những người lính trên quảng trường, qua cả nỗi đau trần trụi và tiếng thét lặng im trong lòng người. 

Và mặc dù lão Trôrđôn hoàn toàn hiểu rằng các cô con gái của lão chỉ muốn làm điều tốt lành cho đứa em và cho gia đình, rằng chúng yêu em và bảo vệ em theo cách riêng của mình, nhưng trong lòng lão vẫn dâng lên một nỗi tức giận không nén nổi đối với những đứa con gái  đang lôi lão tới sau lưng con trai,  để mong khuất phục ý chí của nó, hành vi tự lập của nó, để giết chết ở trong nó cái phẩm giá của con người. Chúng vẫn kéo lão lên mãi, leo lên từng bậc thềm, chen lách giữa đám đông sôi sục.  Và cuối cùng, khi đã lên tới những bậc thềm cao nhất, lão Trôrđôn đã nhìn thấy hàng quân, trong đó có con trai lão đang bước đi. Dàn nhạc hơi đi cuối hàng quân, đó là bộ phận sau cùng lên tàu. Xultan đi ở hàng ngoài. Lão Trôrđôn nhận ngay ra con và lão thấy nó đang quay nhìn tứ phía. Nó đang tìm bố và các chị giữa đám đông. Nếu nó biết được rằng chính lúc ấy, người ta đang kéo bố nó đến chỗ người uỷ viên phụ trách để van xin tha cho nó, để hạ nhục nó trước con mắt mọi người, để chà đạp lên phẩm giá làm người của nó! 

Liền đó, lão Trôrđôn nhìn thấy từ đám đông một cô gái quàng khăn đỏ  chạy ra. Cô lao về phía Xultan, nhưng lập tức bị đám đông chen bật ra, nên cô chỉ kịp nắm lấy tay nó mà thôi. 

Lúc họ đã tới trước căn phòng trưởng ga, nơi người  uỷ viên đang làm việc, mấy cô con gái đẩy lão về phía cửa: 

- Đi đi, bố đi nhanh lên, bố hãy nói rằng bố là bố nó, bố hãy nói về mẹ. Bố hãy nói rằng nó là đứa trẻ, nó chưa biết nghĩ. Bố hãy xin họ thả nó xuống tàu. Bố hãy trình bày tất cả sự thật. 

- Đi đi, bố còn xem xét gì nữa! Thời gian lúc này quý từng phút đấy! 
Lão Trôrđôn bỗng thấy ngượng với mọi người, mặc dù chẳng ai chú ý đến họ. Cả những cán bộ quân sự lẫn dân sự đều đang tất bật chạy lo công việc của mình.

- Bố không quen làm thế, bố không đi đâu, - Lão Trôrđôn từ chối thẳng thừng. 

- Không, bố phải đi! 

- Mà nếu bố không đi, tự chúng con sẽ đi! Chúng con phải làm bằng được! - Và không kiên nhẫn được nữa, các cô gái sấn sổ vào cửa phòng người uỷ viên. 

- Không được, các con không được đi! - Lão Trôrđôn  nắm lấy tay chúng và kéo ra phía cửa. 
Bằng một sức mạnh đáng sợ, lão lại lôi chúng băng qua đám đông trở xuống các bậc thềm. Và lúc ấy lão chợt nghe chúng kêu lên những tiếng mà rất ít khi người ta nghe thấy ở con cái của mình:

- Bố xua con bố vào chỗ chết! 

- Bố sẽ bị nguyền rủa, bố không phải là bố chúng tôi! 

- Đúng, bố không phải là bố của chúng tôi! - Cô kia cũng kêu lên. 
Lão Trôrđôn tái nhợt, chậm rãi nới những ngón tay, buông mấy đứa con gái ra, im lặng quay lại, rồi rẽ đám đông, lão chạy về phía quảng trường. Vội vã  đến chia tay với con, lão băng qua biển người trên quảng trường, qua những tiếng ầm ào, kêu thét, lão tiến vào sân ga, chỗ có đoàn tàu đang đỗ. Nhưng đường đến đó đã bị cấm. Trên sân ga cả một biển người đen đặc đang xao động, dàn nhạc hơi vang lừng, ở đấy không làm sao chen chân được nữa.

Ghì chặt vào hàng song sắt nhà ga, lão Trôrđôn  cố nhìn qua lớp lớp đầu người đến chỗ những toa tàu đỗ dài dằng dặc như bất tận 

- Xultan, Xultan, con ơi, bố đây! Con có nghe thấy bố nói không? - Lão giơ tay qua hàng rào vẫy gọi. 

Nhưng gọi ở đây thì chỉ uổng công! Một người công nhân đường sắt đứng cạnh hàng rào hỏi lão: 

- Ông lão có ngựa chứ? 

- Có, - Lão Trôrđôn đáp. 

- Thế thì lão biết chỗ nhà ga phân nhánh không? 

- Tôi biết, ở phía kia kìa. 

- Thế thì thế này,  bố ạ, bố lên ngựa và phóng tới đấy. Bố vẫn còn kịp, chỉ có năm cây số,  không hơn. Đoàn tàu sẽ dừng ở đấy vài phút, bố tới đó mà chia tay với con, chỉ có điều nhanh lên, đừng chần chừ nữa!. 

Lão Trôrđôn lồng lộn khắp quảng trường chạy tìm con ngựa của mình, đoạn lão chỉ kịp nhớ giật phắt cái nút buộc dây cương, xỏ chân vào bàn đạp, quật cái túi nóng bỏng lên lưng ngựa và gò lưng, lão phóng thẳng theo dãy phố dọc đường xe lửa. Như một anh chàng du mục dữ tợn, dọc con phố chỗ vắng vẻ, chỗ ồn ào, lão làm cho mấy khách qua đường phát hoảng lên: "Chỉ mong cho kịp thôi, mong cho kịp thôi, còn bao điều phải nói với nó nữa!". - Vừa nghĩ vậy, lão vừa lẩm bẩm mấy lời cầu nguyện và câu phù chú của những tay kỵ sĩ, qua hàm răng vẫn dính chặt: "Hãy phù hộ cho con, hỡi các đấng thần linh tiền bối. Hãy phù hộ cho con, hỡi thần Kamba-r'ata chuyên trông coi các loại ngựa, xin dừng làm ngựa con gấp ngã! Xin hãy ban cho nó đôi cánh của chim ưng, cho nó trái tim bằng sắt, cho nó đôi chân của loài hươu và buồng phổi  của loài cá!" 

Băng qua đường phố, lão Trôrđôn cho ngựa phi vào con đường nhỏ men bờ đất bên cạnh đường sắt và lại ra roi. Lão đến cách chỗ nhà ga phân nhánh không xa  thì sau lưng cũng ầm ầm tiếng đoàn tàu vừa đến. Tiếng gầm rít nặng nề, hầm hập của cặp đầu máy xe lửa nối nhau như trời long đất lở đè ập xuống đôi vai lão to lớn đang cúi rạp xuống. 

Đoàn tàu vẫn đuổi theo lão. Con ngựa đã thấm mệt. Nhưng lão tính là vẫn kịp nếu đoàn tàu dừng lại, vì đến nhà ga cũng không xa nữa. Và nỗi sợ hãi, lo lắng nhỡ đoàn tàu không dừng lại khiến lão nhớ đến chúa: "Lạy chúa cao cả, nếu như người có ở trên đời này, xin người hãy dừng đoàn tàu kia lại! Lạy chúa, xin người hãy dừng, hãy dừng  đoàn tàu kia lại!" 

Đoàn tàu đã dừng lại trên sân ga đúng lúc lão Trôrđôn  đến ngang toa cuối cùng. Và anh con trai lão đang chạy dọc đoàn tàu ngược lại phía người cha. Thoạt nhìn thấy anh,  lão nhảy phốc xuống ngựa. Họ lặng lẽ lao tới ôm chầm lấy nhau và như cùng ngất đi, quên hết mọi sự trên đời. 

- Bố, bố tha lỗi cho con, con tình nguyện nhập ngũ đấy - Xultan nói rời rạc. 

- Bố biết, con ạ. 

- Con đã làm cho các chị tức giận, bố ạ. Nếu được, con mong các chị nuốt giận làm lành. 

- Các chị con tha thứ cho con rồi. Con đừng giận, đừng quên các chị và hãy viết thư  cho các chị nghe con. Và con cũng đừng quên mẹ nhé. 

- Tất nhiên bố ạ. 

Trên sân ga, một tiếng chuông vang lên, báo hiệu đã đến giờ chia tay. Lần cuối cùng người cha nhìn vào gương mặt con và thoáng gặp nơi con những đường nét của chính mình thời trẻ, cái thuở  bình minh của tuổi thanh xuân, lão ghì chặt con vào ngực. Vào phút ấy, bằng sức lực một đời lão muốn trao cho con tất cả tấm lòng của người cha. Vừa ôm hôn con, lão Trôrđôn  vừa nhắc đi nhắc lại chỉ một điều: 

- Hãy xứng đáng là một con người, con trai của bố nhé! Dù bất cứ ở đâu, cũng phải là một con người! Luôn luôn xứng đáng là một con người ! 

Đoàn tàu đã chuyển động. 

- Đồng chí Trôrđôn (1), lên đường. Người chỉ huy kêu lên. 

Và khi Xultan chạy theo đoàn tàu và đã được mọi người kéo vào trong toa, lão Trôrđôn đứng buông thõng  hai tay, đoạn lão quay lại ôm lấy cái bờm hầm  hập, đầm đìa mồ hôi của con ngựa mà nức nở. Lão vừa khóc, vừa ôm ghì lấy cổ ngựa và lão lắc lư mạnh đến nỗi dưới gánhh nặng của nỗi đau buồn của lão, những vó ngựa cũng lảo đảo, loạng choạng chẳng thể đứng yên. 

Những người công nhân đường sắt lặng lẽ đi qua. Họ đã hiểu trong những ngày này người ta khóc vì sao.  Chỉ có những đứa trẻ ở nhà ga là đột nhiên im bặt, với vẻ tò mò và sự đồng cảm trẻ thơ, chúng đứng nhìn cái người cao lớn, già nua kia đang khóc. 

*
Mặt trời đã lên quá đỉnh núi chùng hai ngọn cây liễu khi lão Trôrđôn  vừa đi qua khe Nhỏ, ra đến khoảng đất bao la của miền thung lũng nhấp nhô gò đống dẫn đến tận vùng núi tuyết. Lão Trôrđôn  đã trở lại bình tâm. Phải, chính trên mảnh đất này, con trai lão đã sống…
                                                             
(CHINGHIX  AITMATỐP - Nga;  ANH NGỌC dịch)
  
(1) Người Nga và các dân tộc ở Liên Xô (cũng như nhiều dân tộc ở Âu, Mỹ…) gọi tên theo họ, nên hai bố con cùng gọi như nhau (ND).

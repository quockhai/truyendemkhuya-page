Bức thư tình đầu tiên của tôi là viết trên lưng của Swapna. Năm ấy tôi 16 tuổi, còn em vừa tròn 14 tuổi. Hôm ấy ba mẹ tôi và chị gái ra ngoài mua sắm, Swapna giống như một con bướm nhẹ nhàng lượn bay vào nhà tôi. Em mặc chiếc váy rực rỡ nhiều màu có đính kim sa lấp lánh. Không hiểu vì sao bỗng dưng tôi lại có cảm giác bị kích động như vậy, tôi ôm ghì lấy em, hôn em tới tấp, rồi tôi đưa em qua giường của tôi.

 “Bây giờ, để anh dành cho em một điều bất ngờ.” Tôi nói.

“Bất ngờ gì ạ.” đôi mắt to tròn như một con nai, em nhìn tôi.

Tôi kêu em lên giường, kéo lớp áo ngoài của em, để lộ cả một khoảng lưng trắng ngần.

“Anh định làm gì thế.” Em hỏi.

“Rồi em sẽ biết.” Tôi đáp. Và tôi bắt đầu viết trên lưng em, thực ra là vẽ loằng ngoằng thì đúng hơn. Từ bờ vai đến thắt lưng, tấm lưng em như một tờ giấy mà tôi thì dùng bút chì được tùy thích vẽ lên đó.

 “Em đoán được anh viết gì không?” Tôi hỏi em.

“Tôi rất yêu em. Đợi hai đứa mình lớn, em có đồng ý cưới anh không?”

Nghe tôi nói, Swapna đang nằm xoài trên giường, vội bật dậy nhanh như con chó phốc nằm trên tay ông chủ, nhảy xuống.

“Giờ đến lượt em viết.” Em bắt tôi cũng bò ra giường.

Tôi cảm giác thấy những ngón tay em đang vạch trên lưng tôi ba chữ: “Em đồng ý. ”

Bức thư tình đầu tiên của tôi như thế đó.

Nửa năm sau, gia đình Swapna chuyển đi Calcutta, hai đứa chúng tôi mất liên lạc với nhau. Tôi kể mối tình đầu này cho anh bạn thân Mitra nghe. Anh ta nghe xong bảo câu chuyện thiên tình sử của chúng tôi chỉ là mối tình trẻ con mà thôi.

Không bao lâu,  tôi ý thức được bức thư tình viết trên lưng Swapna rất an toàn, bởi nó được viết bằng một loại mực tàng hình, chỉ một loáng là bay mất mà không để lại dấu vết gì.

Nhưng sau đó, có lẽ nói là những ngón tay của tôi lại ngứa ngáy, chúng muốn viết thư tình cho các cô gái xinh đẹp. Những lúc không sao cưỡng lại được, tôi càng thấy bứt rứt. Người yêu thứ hai của tôi tên là Zubaydah, một cô gái của tín đồ Muslim. Ngày nào tôi cũng gặp cô ở bến xe bus. Ban đầu chúng tôi chỉ nhìn cười chào nhau, dần dần bắt chuyện, rồi phát triển đến mức tán tỉnh. Cũng giống như Swapna, Zubaydah rất xinh đẹp, thực ra nàng hấp dẫn hơn nhiều so với Swapna. Trong mắt tôi, Zubaydah như nàng công chúa Ả rập trong câu chuyện “Ngàn lẻ một đêm”, nàng là sự kết hợp diệu kỳ của dòng máu Ấn độ và Persis.

Năm ấy tôi 22 tuổi, tôi tưởng tượng chính mình có thể viết những bức thư tình nổi tiếng giống như của Kafka viết cho Milana, thực hiện giấc mộng trở thành nhà văn. Tôi đã viết bằng ngón cái, ngón trỏ và ngón giữa như rồng bay phượng múa. Cho nên dù Zubaydah ở cách mấy con phố, tôi vẫn đều đặn cách một ngày lại viết thư cho em. Mỗi lần gặp em ở bến xe bus công cộng, tôi đều hỏi: “Nhận được thư của anh không?” nhưng em chỉ gật đầu cười. Tôi hỏi vì sao em không viết thư trả lời, đáp lại vẫn là những nụ cười tươi tắn. Tôi coi phản ứng này của em như sự thể hiện nhút nhát e thẹn của những cô gái. Còn anh bạn Mitra của tôi bảo, các cô gái hồi giáo thường sống nội tâm hơn đại đa số các cô gái Ấn Độ thẳng tính phóng khoáng. Thế nên tôi không thúc giục em trả lời thư cho tôi nữa.

Nhưng tình cảm của em và tôi rất sâu đậm. Hai đứa chúng tôi đi xem phim, đi ăn, dạo công viên... đi đâu cũng có đôi, tay trong tay. Nhưng không bao giờ em cho phép tôi được hôn em. Càng cấm tôi càng khát khao, đúng như Mitra nói, trái cấm lúc nào cũng rất ngọt và thơm.
   
Tốt đẹp không ở lại mãi, bỗng đâu giữa trời nắng lại có sấm sét bổ vào đầu tôi. Khi đó tôi đang là nhân viên kinh doanh công ty trà Brooke Bond, chỉ đến tối mới có mặt ở nhà. Tối hôm đó vừa về đến nhà, một bức thư để sẵn trên bàn được dán niêm phong màu đỏ. Mở thư ra, tôi như bị chết đứng. Thư mời ra tòa án của một luật sư tư vấn riêng cho Zubaydah. Tôi run rẩy sợ hãi đọc đi đọc lại hai lần, tôi bị cáo buộc tội quyến rũ cô gái vô tội, yêu cầu hai tuần sau lên tòa án chất vấn.

Tâm trạng tôi rơi vào trạng thái hoang mang, hỗn loạn. Tôi quyết định nhờ bố đi cùng đến tòa, ít ra cũng vững tin hơn. Cho dù chúng tôi sẽ ra tòa nhưng tôi cũng không để vụ này đổ trách nhiệm hoàn toàn cho tôi. Ban đầu tôi phải đối mặt với sự thịnh nộ của ba mẹ, nhưng sau đó ba mẹ tôi cũng mời luật sư bào chữa cho tôi.

Tôi cũng không hiểu nổi vì sao bố của Zubaydah đã buộc tội tôi quyến rũ con gái ông. Con gái ông hoàn toàn tự nguyện quan hệ với tôi, hơn nữa đi đâu Zubaydah cũng rất sẵn sàng. Điều mà tôi thấy kỳ lạ nhất là từ đó về sau trở đi, tôi không bao giờ gặp Zubaydah ở bến xe bus nữa, có lẽ nào em bị ông bố cấm cung ở nhà.

 “Pháp quan đại nhân tôn kính, bức thư tôi viết như thế này: “Zubaydah em yêu, linh hồn của anh, anh yêu từng bộ phận trên cơ thể em, yêu nhất là những ngón tay đẹp nõn nà, nó giống như những ngọn nến lung linh ngũ sắc, không lúc nào ngừng cháy trong trái tim anh. Nó như làm gảy lên cung đàn trái tim anh, tấu lên những bản nhạc say đắm lòng người, bản nhạc này tuyệt diệu hơn bất kỳ bản nhạc nào trên thế gian, giống như là một lời an ủi duy nhất với công việc tiếp thị trà tầm thường của tôi...

“Tôi lúc nào cũng muốn được ở bên cạnh em. Vì em tôi thậm chí có thể bỏ mặc tín ngưỡng, quên cả người thân, để được đi với em đến cùng trời cuối đất, chỉ cần em gật đầu đồng ý...”

Khi đó tôi ngồi bên cạnh ba mẹ và chị gái Sushila, đối diện bên kia là Zubaydah, gương mặt em che đi một nửa bởi tấm mạng đen. Em bây giờ trở thành một kẻ thù đáng chết, kẻ đã hủy hoại danh dự gia đình tôi. Đôi mắt cha tôi hừng hực như phun ra những tia lửa vì tức giận. Chị gái Sushila đứng bên cạnh nói thầm vào tai tôi: “Thư của em viết thật cảm xúc, em sắp thành một nhà văn rồi.”

Chị Sushila đã nói đúng phần nào, có lẽ đúng là tôi luyện viết thư tình. Mỗi một bức thư tình tôi đều giữ lại, để sau này viết tiểu thuyết hoặc sẽ có lúc dùng đến, mặc dù cho đến giờ tôi chưa bao giờ viết ra những thứ chết tiệt này. Chị Sushila sẽ nói, tôi đang trở thành Kafka thứ hai.

Ba tôi, một đại tá quân đội về hưu, không mong muốn màn kịch dở tiếp diễn nên quyết định tung tiền để dập tắt. Cuối cùng cha tôi cũng mất 5 vạn rupi để hai nhà sẽ tự hòa giải.

Nhưng bài học xương máu này cũng không khống chế được tật ngứa ngáy ngón tay đòi viết thư tình của tôi. Song chỉ có điều đối tượng để tôi viết thư biến thành nhân vật hư cấu, nhưng tôi chợt nhận ra rằng, làm như vậy chẳng giải quyết được gì bởi những nhân vật hư cấu đó đâu kích thích được cảm xúc thật của tôi.

Rồi tôi lại rơi vào lưới tình, tôi yêu một nữ điêu khắc trẻ đẹp, tôi quyết định chỉ dùng lời nói để thổ lộ tình cảm. Tôi tự nhủ mình, nếu những ngón tay ngứa ngáy kia không kiềm chế nổi, đòi viết thư tình thì tuyệt đối không được gửi đi.

Người yêu mới của tôi tên là Shabu Hasini. Không giống như những người con gái đã đi qua đời tôi, em không trẻ con như Swapna, cũng không bí ẩn như Zubaydah. Em là một cô gái trẻ chín chắn, có thể tự tin, thoải mái giao tiếp với tôi. Nhưng cái khiến tôi cảm động vô cùng là những ngón tay của em, nó giống như nghệ sỹ thổi sáo đang diễn tấu, lướt qua lướt lại trong tiếng sáo dìu dặt, những ngón tay  tự do, phóng khoáng mềm mại vô cùng. Trên từng ngón tay, em tô một loại sơn móng đặc biệt. Khi màn trời tối lại, những ngón tay lấp lánh như những con đom đóm phát sáng trong đêm. Có lúc những ngón tay ấy trông giống như những cây kẹo mút của trẻ con, dài mà tròn bóng khiến tôi phát thèm. Một lần tôi đưa em đi ăn ở nhà hàng Nhật Bản, chúng tôi không thể không dùng đũa, tôi vụng về lóng ngóng còn em thuần thục dùng đũa gắp thức ăn cho tôi.

Chúng tôi yêu nhau chỉ có mấy tháng mà tôi cảm giác như mình và em là một. Tôi nghĩ thầm có lẽ em chính là người sẽ chung sống với tôi đến đầu bạc răng long. Cho dù li hôn bây giờ là chuyện rất bình thường, nhưng tôi biết rất rõ là kết hôn chỉ có một lần trong đời người.

Shabu Hasini và tôi quyết định đi du lịch Shimla. Em như một con chim tự do, muốn đi đâu là đi, còn tôi thì ngược lại phải tìm lý do mới xin công ty cho nghỉ.
          
Tuần này, tôi được sống những ngày thật hạnh phúc. Chúng tôi được đi bộ, cưỡi ngựa, được ăn ở nhà hàng mái vòm có tên là Ashiana. Nhà hàng vừa vặn nằm trên lưng chừng núi. Cả hai chúng tôi lựa chọn chiếc ghế mà chúng tôi thích nhất ở lưng núi, ngồi bên nhau mặc cho thời gian trôi cho đến khi nhìn thấy những làn khói cuộn lên từ thung lũng, những đốm đèn bắt đầu nháy sáng phía trong núi, chúng tôi mới trở về khách sạn.

Cái buổi chiều hai đứa chúng tôi ngồi trên chiếc ghế ngắm mặt trời dần lặn, tôi để ý thấy móng tay của Shabu Hasini lấp lánh trong bóng tối. Thấy tôi đang mải ngắm những ngón tay, em liền xòe bàn tay như chiếc quạt giấy, em tựa như con công đang múa. 
          
Tôi hỏi em: “Có khi nào em nghĩ tới những ngón tay của thợ điêu khắc, nhất là ngón tay của người con gái?”

Ngừng giây lát, tôi hỏi tiếp: “Ví như những ngón tay của em?”

“Chưa bao giờ em nghĩ tới bàn tay điêu khắc của em.” Em ngạc nhiên nhìn tôi như muốn dò hỏi về câu hỏi thú vị.

“Vâng, mấy ngày tới em thử điêu khắc xem.”

“Không phải tay của em, em yêu ạ.” Tôi cải chính, kéo tay em đặt vào lòng bàn tay mình tôi ngắm nhìn, vuốt nhẹ nhàng từng ngón tay em. “Không phải bàn tay của em mà là ngón tay của em.... Shabu Hasini, em xem này, những ngón tay của em đều có ba đường chỉ rất rõ nét giống như những đường chỉ trong lòng bàn tay vậy. Nếu như đường chỉ ở lòng bàn tay có thể nói lên mệnh vận của mỗi người, tại sao không để những ngón tay cũng dự đoán cho số phận của hai chúng ta?”

“Em hoàn toàn đồng ý.” Em nói.

Sau câu nói của em, tôi xích lại gần em, mặc kệ từng đầu ngón tay tôi vuốt nhẹ lên mặt em, má em... Tôi thấy em khép chặt đôi mắt, say đắm bởi những ngón tay mơn man.

“Còn điều này nữa chưa nói với em.” Tôi nói “ Đó là những đầu ngón tay. Đó là bộ phận nhạy cảm nhất trên mỗi ngón tay. Anh tin đó là cảm xúc tính túy nhất của con người. giống như kim thu phát sóng của tháp truyền hình, mỗi đầu ngón tay có thể thu phát mọi cảm giác trong không gian.”

“Em yêu anh” em hôn lên những ngón tay và các đầu ngón tay của tôi. “Tại sao anh không viết ra những lời anh vừa nói? Những lời anh nói với em đêm nay, anh viết thành một bức thư gửi cho em, em sẽ trân trọng nó. Mà anh chưa bao giờ viết thư cho em.”
         
“Ồ, đáng lẽ anh sớm phải nói với em, anh chỉ biết bày tỏ tình yêu của mình bằng lời nói, giống như tổ tiên xưa của chúng ta vậy, chữ viết mãi sau này mới xuất hiện. Nếu dùng chữ viết để biểu đạt chẳng khác gì hạn chế những người thích dùng ngôn ngữ tự do để tỏ tình hay sao?"

Nhưng từ trong sâu thẳm con tim, tôi lại nghĩ đến Zubaydah, nhớ đến cái năm không hiểu vì sao mà thư tình của tôi bị người ta đọc trước tòa. Tôi nhất quyết không bao giờ để xảy ra chuyện này một lần nữa.

“Sau này anh sẽ viết thư cho em,” tôi nói “nhưng bây giờ em đang ở bên cạnh anh thì hãy cho anh được thổ lộ tình cảm chân tình với em bằng lời nói. Bỗng dưng toàn thân Shabu Hasini run lên. Tôi phát hiện ra những ngón tay của em trắng bệch, tê dại. Tôi vội cởi áo khoác ngoài choàng lên người em, dìu em về khách sạn.

Suốt một đêm, em cứ kêu có một luồng khí lạnh chạy dọc lưng em, tôi lo sợ điều gì đó bất an đến với em. Sáng sớm hôm sau, tôi mời bác sỹ đến khám, nhưng vị bác sỹ này không kết luận ra bệnh gì. “Có lẽ là nhiễm virus.” Bác sỹ chỉ có thể cho em uống kháng sinh liều cao. Chiều hôm đó, tôi và em ra sân bay, đưa em về nhà ba mẹ em ở Delhi. Tôi giới thiệu mình là một hoạ sỹ, đồng nghiệp của em, còn lại dành cho em tự nói với họ.

Hôm sau, tôi gọi điện đến nhà em, nhận được tin em đã rơi vào trạng thái hôn mê, miệng luôn kêu: “ngón tay, đầu ngón tay...”

Qua hai ngày sau, tôi nhận được tin buồn, em đã bỏ tôi mà đi xa.

Đêm hôm đó, tôi mơ thấy mình ngồi trên chiếc ghế ở núi Shimla, tựa như được ngồi trên tấm thảm bay lên trời cao. Tôi bay lạc giữa các vì sao, bỗng một giọng nói vang lên trong không trung: “Đúng vậy, em đã hoàn thành cho anh, em chỉ đắp nặn những ngón tay của em... nhưng anh có viết thư cho em không? Em vẫn đang chờ đợi...”

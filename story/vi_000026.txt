Nhà cô giáo Liên ai cũng ăn ớt giỏi.

Năm bố cô đi bộ đội về phục viên. Nhà bảy người, lo ăn bạc mặt, cơm không đã là ngon quá, là đặc sản gọi theo ngôn ngữ bây giờ. Thức ăn Có cái gì thì ăn cái gì! Cá mắm bà mẹ thương, bớt bơ gạo lấy năm lạng, tháng hơn tháng mua một lần. Muối giềng, bố đi rừng xách về, cạo rửa, thái giã, cho tí mì chính rang lên, thơm nức.

Bảy người ăn chỉ hai người làm công điểm. Năm bảy lạng thóc/công, tới vụ đến sân kho hợp tác xã lấy về ba tạ, chia cho bảy người, sáu tháng. Cơm không một tuần ngày mùa, bữa nào bọn trẻ cũng căng rốn, chống tay đầu gối, ì ạch đứng dậy. Thùng gạo như có ai ăn cắp, mấy ngày lại ù éc, thì thụp xay giã. Mưa liền liền, quanh ra quẩn vào lại nấu cơm. Mẹ sốt ruột, ngồi thừ; bố con vẫn chơi cờ tướng cãi nhau inh om.

Đã ba lần mẹ chui vào hòm thóc, gọi đứa út mang cái chổi, cái hót rác. Tiếng đáy hòm như tiếng đóng quan tài. Lũ trẻ vẫn bịt mắt bắt dê, xì kíu vòng quanh.

Mẹ cầm rá gạo, bố nắm hai nắm lại, bảo: Mùa còn lâu. Đói không chết. Nước mắt mẹ trào ra. Mẹ nghiêng ngó không thấy ai, lại bốc lại một nắm gạo vào rá.

Cơm chín chưa? Cứ chơi đi, sốt cả ruột. Đây hai củ khoai này chia nhau. Anh cả nhận nhiệm vụ, giơ hai củ khoai lên cao như người ta giơ cúp bóng đá bây giờ. Lũ em nhao nhao, rướn người, vòng quanh, trước sau. Đám người và khoai rầm rập từ bếp, lên nhà, ra vườn thì hạ cỗ. Anh cả xoè tay, phồng rộp, lựt lạt. Khoai chỉ còn ấm ấm. Trật tự! Anh quát vào những bàn tay lũ em đang như mũi lê xông lên. Trật tự rồi, tất cả tản ra, ăn cả vỏ.

Về ăn cơm! Tiếng mẹ gọi. Chạy, mấy cây cà bị oan, những đôi chân trần như gió. Bố chúng mày đâu?... Đây đây, có món này tuyệt vời. Bố cầm cái bát con đỏ nhức mắt vào. Ớt.  Đàn con đã sang bát thứ hai. Mẹ ngồi đầu nồi, đôi đũa cái đảo đi đảo lại. Mẹ nhìn bố như cầu cứu. Bát ớt dưới mâm vẫn chỉ mình bố. Bố cầm cái thìa xúc, hắt nhẹ vào từng bát đàn con, động tác rất nhanh. Thằng cả lè lưỡi, vươn cổ nuốt. Thằng hai mặt nhăn nhó. Đứa ba, tư, năm, sáu kêu lên: Ớt! Ớt! Đứa út khóc như bị mẹ đánh. Mẹ nhìn bố: Anh ác lắm! Bố chạy lên đầu nồi, tự xới rồi đổ cả bát ớt vào. Thẳng lưng thẳng cổ, mặt không biến sắc. Bố đang làm tấm gương.

Nồi cơm tưởng thiếu hoá ra thừa. Bữa tối, lũ trẻ nem nép, lặng lẽ ngồi. Bát ớt cũng im lặng đỏ, vàng ra xung quanh.

Như thế... như thế... thì đến mùa.

Ơn giời không đứa nào ốm. Có đầu có đuôi cứ nuôi khắc lớn, hết tháng rồi sẽ thành năm. Bảy đứa giờ đều làm cán bộ. Bữa cơm sum họp ớt vẫn nhiều hơn thịt cá. Bát đỏ, bát vàng bố phải ba lần lấy. Mẹ vui trào nước mắt, kể chuyện ngày xưa có anh kia nhà đói, sang hàng xóm đúng bữa cơm được mời xã giao. Anh ngồi xuống liền, nhận bát cơm, tự so đũa. Để chống thẹn anh bảo vừa ăn cơm ở nhà thấy có ớt ăn vui một bát. Tất cả cười. Cô út sụt sịt, anh cả bĩu môi: Cả nhà nhìn cô giáo Liên kìa, hơi tí là xúc động. Liên nói trong giọng khóc: Không phải. Các anh ăn hết ớt của em rồi. Em được có ba quả. Bố cầm bát đứng lên, mang vẻ mặt buồn buồn quay lại: Hết mất rồi. Để chiều bảo mẹ ra chợ mua năm cân về ngâm.

Như thế, như thế rồi nhà lại vắng tanh. Mẹ buồn, bố bảo: Có phúc mới được hai ông bà ở nhà đấy.

*
*     *
Liên về từ hôm trước. Ba đứa bé người Mông ngơ ngác cùng về. Liên bảo: Nó là học sinh lớp con, cho xuống thị trấn chơi. Mẹ hỏi tên từng đứa. Dế à! Sùng à! Mua à! Liên bảo: Mẹ đưa chúng nó tắm rửa hộ con với, còn bố sắm cho mỗi đứa một cái đèn ông sao. Bố cười: Thôi bố mẹ chuyển ngành sang làm giáo viên vậy.

Ba thằng Dế, Sùng, Mua tắm rửa xong nom nhanh nhẹn hoạt bát hẳn lên. Đèn ông sao bố cũng vừa mang về. Liên mở điện thoại bảo: Nhà ta hôm nay ăn cơm sớm cho bọn trẻ chơi trăng.

Ngon quá! Thịt nướng ngào ngạt. Canh cá thơm lựng. Món nộm ba màu tươi rói. Ai là tác giả đấy? Bố thiết kế, mẹ thi công, cô giáo chấm mấy điểm? Tám điểm. Sao cho đắt thế? Em có nhiều tiến bộ nhưng phải cố gắng nhiều hơn nữa. Liên nghiêm mặt giọng cô giáo. Cả nhà cùng cười. Ô! Các cháu ăn đi! Mẹ giật mình. Ba đứa ăn nhỏn nhẻn, và một miếng lại nhìn. Bố gắp thịt nướng, mẹ gắp món nộm cho ba thằng. Liên bảo: Mỗi đứa ăn hết ba bát thì cho chơi trăng không thì bắt đi ngủ.

Câu chuyện của ba người lớn lại tiếp tục. Mẹ kể chuyện ngày xưa, bố kể chuyện hồi ấy. Liên cười: Bố mẹ mắc bệnh già rồi đấy. Liên bảo con ở trên Hua Nậm hoá ra sướng, gió núi lồng lộng, rau cải nương ngọt lừ, thỉnh thoảng lại được miếng thịt nai thịt hoẵng phụ huynh cho. Bố bảo: Ừ, ở trên đấy cũng tốt. Mấy người đã được sống sạch, ăn sạch. Mẹ lắc đầu: Cái ông này định cho nó ế chồng trên đấy à. Liên cười sặc sụa: Bố mẹ muốn con tảo hôn như bố mẹ hay sao!

Chết rồi, sao mấy đứa cứ bưng bát thế này? Mẹ vột vạt. Bố nhìn, Liên nhìn. Thằng Dế mắt đỏ hoe, thằng Sùng mặt như khóc, thằng Mua đang sụt sịt.

- Sao vừa mới đi sáng nay mà đã nhớ nhà à?

- Khổ quá! Trẻ con nói nhẹ thôi. Nhớ chứ sao lại không nhớ.

- Toàn món ngon thế này mà...

Ba người lớn nói. Ba đứa trẻ ngồi im, nước mắt cứ thế trào ra.

- Nào để cô bón nào. Chiều mai cô trò mình lại về bản.

Liên nựng, lau nước mắt, bón cho từng đứa. Thằng Dế cố nuốt, thở hổn hển. Thằng Sùng lắc đầu quầy quậy. Không. Không ăn đâu. Thằng Mua, thìa cô vừa đưa đến là nức nở.

- Chết thật về nhà cô giáo lại không ăn thế này mang tiếng chết. Thế không ăn thịt, ăn nộm để bà chan canh nhé.

Mẹ cầm muôi chan vào bát từng đứa. Xong, mẹ đưa lên nếm thử.

- Chết thật rồi. Mình hại ba đứa trẻ rồi. Mẹ kêu toáng lên.

Bố ngơ ngác. Liên mặt tái đi.

- Cay quá. Tao còn thấy cay. Bọn trẻ ăn làm sao nổi. Thôi thôi làm món khác.

Ba người lớn đứng dậy. Liên mở tủ lạnh lấy miếng thịt ra thái. Mẹ cầm đèn pin ra vườn hái cà. Bố chạy sang quán mua ba hộp cocacola. Loáng cái, tất cả đã về mâm. Ba đứa cắm đầu ăn. Mẹ nhìn những cái má phồng phồng nhịp nhàng. Liên hồi hộp đến tận khi ba đứa buông đũa. Bố ngồi im như ân hận.

Liên đang châm nến vào đèn cho ba đứa. Mẹ bưng mâm ra vòi nước, bố đi theo. Mẹ gắt bố:

- May hôm nay tôi phát hiện ra. Không thì mang tiếng cả đời.

Bố thở dài. Có lẽ tôi mắc bệnh già rồi.
 

Điện Biên 09-10-2009
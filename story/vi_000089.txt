Néc-lương ngày hạn, nắng như bỏ xuống mặt đất những trái bom lửa lớn thiệt lớn. Không tiếng nổ nhưng có tiếng cháy và lửa lòn theo từng thớ đất, bốc ngược lên mặt nắng. Từng gương mặt người đi đường tưởng như đang bị nướng chín. Cái nóng chảy trong lòng tôi cũng không kém phần bạo liệt. Tôi về nơi chôn nhau cắt rốn của Quyền, nơi Quyền có thời con nít với món me nước chấm mắm đường, hay những con bửa củi lớn bằng ngón tay bổ bụp bụp trên vạt tre. Cha nàng đang kiệt quệ sức lực ở nơi đó.

Tôi đã phụ lòng bác. Ngày trở về Néc-lương bác gởi Quyền cho ba má tôi nhưng nói với tôi. “Đem về bên đó rủi có bề gì (bác muốn nói tới giặc giã và cả những rối ren chỗ bác ở) tao lấy ai thường cho bây.”

Bác Đen cũng không muốn đi. Nhưng “xứ đó còn mồ mả má thằng Thích với lại ông bà nội nó” mười mấy năm nay bỏ xứ đi, đêm nào nhắm mắt bác cũng thấy nắng Néc- lương dội phực lửa mấy gò mả lạng.

Bác Đen là người Khơ Me gốc Việt, sinh ra và lớn lên tại đất Néc- lương chói nắng. Vậy mà tụi Pôn Pốt lùa hốt sạch. Má của Quyền chết đợt này. Bác Đen dẫn anh em Quyền tản cư tới xóm tôi, cất một cái chòi nhỏ cặp sông. Quyền thiệt bụng, mắt đẹp, mũi thẳng, hàng răng trắng như ngọc. Hai đứa tôi từ nhỏ đã chơi trò chú rể cô dâu, lớn một chút thì đi đâu cũng dính nhau.

Tôi tới tuổi nghĩa vụ quân sự, mong mình vượt qua được mọi nguy hiểm, hẹn ngày giải ngũ về cưới nàng. Không dè, vĩnh viễn ngày đó không tới. Quyền đột ngột lên cơn sốt cao, không lâu sau thì qua đời.

Bác Đen xót trong bụng lắm. Thích nói bác hay ngồi chù ụ một mình. Một ngày vừa tắm xong, bần thần một hồi rồi té quỵ. Anh kêu tôi khi nào tiện lên chơi cho bác khuây khoả phần nào, nhắm mắt cũng vui.

Tại chợ Néc-lương tôi gặp một cô gái khi đang mua xoài. Hệ thần kinh tôi chết đơ ở phút này. Mớ tiền định bụng trả cho chị bán trái cây nằm bung trên đất. Em cúi xuống cầm nắm tiền quấn gọn nhét vô tay tôi:

- Chê tiền hả?

Em tiếp tục lựa xoài. Chị bán trái cây kín đáo quay mặt đi cười mỉm. Tôi bẽn lẽn đưa tiền cho chị, xách bọc trái cây lên dùng dằng nửa đi nửa ở.

Khi em đi, tôi rất muốn chạy theo nói với em một câu gì đó, chẳng hạn nhà em ở đâu, em tên gì, em có biết… Chị bán trái cây kéo tôi lại bằng tiếng kêu nhỏ rồi lắc đầu:

- Chú… đừng dính vô cổ. Coi bộ chú hiền nên tôi khuyên vậy.

Chị không giải thích gì thêm. Tôi xớ rớ một hồi lại đảo mắt kiếm. Chợ chỉ có mấy ngõ đường nhưng bóng em mờ mịt. Đường tới nhà bác Đen bỗng dưng lan man trong trí nhớ. Anh Thích đã chỉ đường kỹ lưỡng. Vậy mà lúc này tôi không hỏi han được gì, lựng chựng như là không biết mình đang định đi đâu về đâu.

Em không đẹp nghiêng nước đổ thành nhưng từng nét nhỏ trên gương mặt em giống Quyền một cách lạ lùng. Không gặp lại em, tôi có cảm giác y như mình lại mất Quyền thêm lần nữa, dù tôi biết chắc đó không phải là Quyền. 

 Tôi bước ra đường, càng thấy cái nóng Cao Miên dữ dội hơn. Nó bủa xuống đầu, chụp đầy mũi mùi không khí khét nghẹt. Sao cái xứ cháy da phỏng trán này lại có người đẹp vậy không biết.

 Anh Thích thấy tôi lên mừng muốn khóc, hỏi đi nắng mệt dữ sao mà dòm buồn hiu vậy. Tôi kể về cuộc gặp gỡ kỳ lạ của mình rồi phân bua.

- Thôi bỏ đi, có lẽ tại em quá nhớ Quyền nên trông gà hoá cuốc. Chắc không ai giống cổ vậy đâu.

Anh Thích cười cười, chỉ tôi coi khu đất sát nách nhà anh. Ở đó có nhiều dãy, mỗi dãy được chia ra những ô nhỏ y như chuồng heo. Anh nói “Nếu cậu để ý, một hồi thôi thế nào cũng thấy từng cặp từng cặp quấn nhau xà nẹo đi vô đó. Con mẻ đang ở trỏng”.

Anh muốn nói tới một người phụ nữ có nét giống Quyền. Chị ta lớn hơn Quyền sáu bảy tuổi nhưng biết ăn dọn với lại đầu óc đơn giản nên nhìn trẻ hơn tuổi rất nhiều. Tôi lại bần thần thêm lần nữa.

…Bác Đen không nói được, nghe tôi nói tên bác chảy nước mắt.  Con người dày dạn nắng gió mà giờ phải nằm một chỗ, chỉ có thể giao tiếp bằng cái nắm tay yếu ớt, bằng những giọt nước trong khe. Có phải chính tôi là một phần nguyên do làm ra nỗi đau này. Anh Thích rầy tôi, đừng có nghĩ khơi khơi, bệnh tật nó có từ ai đâu. 

 
 Minh họa: Lê Trí Dũng


Ngồi với bác một hồi tôi ra bước xuống sàn nhà đi vòng vòng cho đầu óc giãn ra. Trước mắt tôi lại hiện ra hình bóng Quyền. Vẫn đường nét đó, nụ cười nhẹ hều, thanh như một nét gió. Em ẵm trên tay một đứa nhỏ độ hơn một tuổi.

- Ủa, anh làm gì ở đây? 

Tôi  chưa kịp trả lời, hình như lúc đó tôi đang mắc cắn môi hay lưỡi mình gì đó..

- Sao mà nhăn nhó vậy, anh đau ở đâu à?

Tôi  lơ mơ nhìn em, không còn cảm giác gì nữa.

- Cho em gởi một chút nhe.

Em ém vào tay tôi thằng con rồi lật đật bước đi, chui nhanh vào một căn trong dãy chuồng trước mặt. Tôi cứng mình. Lúc này mọi thứ cứ quay xà quần trước mắt tôi. Nụ cười em hay nụ cười Quyền trong trí nhớ tôi nhảy dựng dưới nắng như một bóng ma. Tôi giận mình đã nhập nhằng hai hình ảnh này. Em là ai kệ em. Quyền  phải khác. Tới khi đứa bé trân mình ư ư tôi mới hay hình như mình đã ôm nó quá siết.

Nó ngước hai mắt ngơ ngác nhìn tôi. Tôi có cảm giác nó sẽ ồ khóc nên chành miệng cười với nó một cái sượng trân. Tôi ôm nó đi qua đi lại, có cảm giác mình vừa đi hết một vòng trái đất, mắt không nhìn nhưng đầu không dứt khỏi khu nhà trước mặt. Trong đó ồn ào chộn rộn, ầm ù rối rắm. Có cả tiếng ong ong và những cơn co giật bưng bưng…

Rồi em cũng ra, vừa gài nút áo vừa bước, tóc chảy xệ một bên vai. Sau em một người thả phanh áo, hở cái bụng bự đen như bồ hóng, lắc lư mình xuống thang.

Em định ẵm con đi nhưng sực nhớ quay lại cám ơn tôi. Tôi không trả lời. Em bước tới rờ trán tôi một cái:

- Anh mệt sao mà mặt xanh dờn. Thôi, ngồi xuống đây nghỉ một chút đi. Anh không quen giữ con nít hả. Nó quậy anh dữ hôn? Tại ba nó xỉn rồi nên em mới mượn anh.

Tôi định hít một hơi để lấy lại bình tĩnh.

- Ba nó hả? Chồng chị hả?

- Có gì không anh?

- Giữ con cho chị tiếp khách?

Em xịt qua mũi một tiếng cười nghe lạt như nước miếng.

- Chuyện bình thường.

Tới đó, chừng như em thấy mặt tôi khó coi lắm nên cười.

- Ờ quên, chắc anh chưa từng thấy... Trên đời này cái gì mà không xảy ra được hả anh?

- Bộ anh ta không ghen à?

- Ghen có mà đói nhăn răng. Còn gì nữa mà ghen. Hai đứa như hai giề lục bình trôi xơ cờ, sống ngày nào tính ngày đó. Khi không bữa đó em bịnh, chiêm bao thấy già lụm cụm đi không nổi bò xuống sông uống nước. Mà anh thấy cái bến ở đây hông, nước nó hụt tuốt mị dưới đáy. Chồng em lúc này đang làm cho bà chủ, nạt nộ mấy đứa nhỏ mới vô để tụi nó không trốn. Bà chủ cho miếng ăn, cho tiền nhậu, hút sách, chơi gái. Vậy mà thấy em bệnh nó lo, đi mua cháo, mua thuốc, rồi cạo gió cho em. Thấy dẫu gì nó cũng người Việt mình, lấy nó không ai ăn hiếp, vậy là nhập cục, rồi sanh con. Lúc đầu cũng thương nhau bộn bộn, nhưng cái nghề này nó làm tình nghĩa vợ chồng chết lần hồi. Nhưng biết làm gì sống bây giờ, được cái em đẹp nên có nhiều khách, cũng sống được.       

 Tôi thấy chua cho cảm xúc mình từng có với em (với những đường nét giống Quyền). Tôi bắt đầu biết giận con mắt. Những khi nhìn thấy chồng em ẵm thằng con, tập cho thằng nhỏ kêu ba ba, hay bồng nó để lên cao cho nó hái mấy bông me nước, tôi bức rức. Ngay lúc này đây, cái gương mặt giống gương mặt Quyền đang ở một căn chòi  nào đó  trong cái dãy chuồng chật chội. Và… chồng lên gương mặt đó là một bộ mặt đàn ông lạ hoắc... Giá như em mang một hình hài của ai khác, hay ít ra tôi có thể thoát khỏi nơi này ngay tức khắc để không nhìn thấy bất cứ cái gì gợi đầy hình dung. Nhưng tôi thương bác Đen. Về rồi biết có gặp lại nữa không.

Mỗi khi có dịp gặp tôi lãng vãng dưới sân nhà, em không e dè ém đứa con vô tay tôi để đi khách. Có khi tôi đang ở trên nhà bác Đen em cũng xộc lên để lại đứa nhỏ rồi bỏ đi. Những ngày này tôi cực muốn nổi khùng. Có không ít lần tôi đã sắp sẵn những câu kịch độc để đánh thức liêm sỉ trong em. Nhưng khi gặp em tôi lại thấy đôi mắt quen thuộc quá, nhất là lúc nào trên tay em cũng có thằng con. Thấy mắt em nhìn con thì tôi không nỡ nói gì.

Em vẫn vô tư, dữ dằn, trâng tráo trước mắt tôi. Có hôm tôi nghe em gào như sư tử giành mồi:

- Đồ khốn nạn, đồ chó đẻ. Mày sống được thì sống, sống không được cắt cổ mổ họng chết cho rồi. Có đâu mày hành tao tới nông nổi này nè.

Kế đó, tiếng rượt đuổi nhau, tiếng đấm đá bịch đụi. Tôi thò đầu ra nhìn thấy em vừa chạy vừa lấy tay bụm một bên mặt, máu từ đuôi mày ròng xuống. Thì ra chồng em lên cơn ghiền lại kẹt tiền. Anh ta lùng đâu được ông khách nghe nói sộp lắm. Em từng gặp ông này rồi. Ngoài những thứ có được trên cơ thể, ông còn đem theo mấy món đồ nghề dành chơi gái. Em than mình đang “mắc kẹt”. Ông khách đưa tay định rờ, “mắc kẹt cũng được”. Quạu quá em lên gối ngay mặt ông… Anh chồng vác một khúc tre đập em từ đầu tới chân. Hôm đó em ghé qua chỗ tôi, một tay bồng con, tay sỗ sàng vạch vai, vén quần tới háng... Giọng  gay gắt như muốn uống máu tươi của chồng.

Tôi khó chịu lắm, cả em và chồng em. Không thể nào tin có kiểu vợ chồng như vậy. Tôi uể oải buông một câu lấy lệ.

- Rồi mai mốt chị tính sao?

Em đang vỗ vỗ hai tay vào má con trai quay ngang trợn mắt nhìn tôi:

- Tính sao là sao?

- Chẳng hạn làm gì đó cảnh cáo anh ta, hay là bỏ anh ta.

- Ôi trời. Em mà bỏ nó em bỏ từ lâu. Không được đâu.

- Sao vậy? Hay là chị sợ không ai dám lấy chị.

Em vừa mỉm vừa trề, tay vuốt nhè nhẹ bàn tay thằng con.

- Không phải sợ vậy. Em có đủ nghề để khách mê dù đó là loại nào. Có người mặc kệ nghề em, sẵn sàng sống lâu dài với em, em muốn gì có nấy. Em đối với họ ăn bánh trả tiền thì được. Nhưng biểu bỏ chồng theo họ em không làm đâu. Nó đánh em cỡ này mà nhằm nhò gì, hồi lúc em có bầu lần đầu, nó còn đánh em đẻ non luôn.

-  Sao chị chịu nổi?

- Không biết nữa, hình như kiếp trước em nợ nần gì nó hay sao ấy?

 Tôi nghĩ em không tới nỗi nào, định tìm cơ hội gọi thức lòng tự trọng trong em. 

- Chị…  rất giống Quyền, người yêu của tôi...

- Em biết. Chú Ba hồi trước ghét em lắm. Nhưng bữa Quyền chết chú có kêu em rảnh rảnh qua nhà chú chơi, nhìn mặt em đỡ nhớ Quyền. Em còn biết bữa mua xoài anh chết đứng khi thấy em, nhưng em không tưởng tới đâu.

Tôi nóng rần mặt:

- Bộ tôi không xứng đáng sao?

- Không phải. Nhìn anh em cũng thấy anh đàng hoàng, cũng đẹp trai. Nhưng anh có thương em lắm cũng chỉ thương hại, em để ý làm chi cho khổ thân. Có người thương em thiệt bụng, mê em chết lên chết xuống em còn bỏ được. Anh ở đây đi, thế nào cũng gặp ảnh. Ảnh đi một chuyến hàng hai ba ngày, về, sớm tối gì cũng chạy tới em.

Em nói tới đó chợt nhìn ra hướng cổng. Tôi nhìn kỹ mắt em. Lúc này  trong đó chứa một góc rừng nhiệt đới sau trận mưa tầm tã, ăm ắp nắng gió, có từng giọt nước đọng trên lá và cả những chồi xanh. Nó long lanh hơn cả lần gặp đầu. Mặc dù bên cạnh đó là một vết thương đang sưng đỏ.

- Chị thương người đó rồi?

Em giật mình, cười giả lả:

- Úi trời, loại gái ghiếc như em có nước thương tiền.

Em nói tới đó, bồng đứa nhỏ lên hun cái chụt rồi vừa đi vừa hí hắc chọt ét nó. Tiếng cười của hai mẹ con trong leo lẻo lọt tót giữa cái nắng  không có bóng cây.      

 Hôm tôi gần về, ông khách quen em hay nhắc có tới. Em dắt ông  ghé chỗ tôi. Dáng ông bề thế bụi bậm nhưng còn giữ được nét trai tráng. Em cười luôn miệng, nụ cười thiệt đẹp. Có khi em ghẹo cho ông cười. Ông là người dễ thân thiện. Ông nói với tôi chồng em đòi trăm triệu anh ta thả cho em tự do. Khi giao tiền anh ta đòi nhiều hơn. “Tiền thì kiếm không khó nhưng thằng đó không dễ buông tha cho cổ”. Trong khi ông đã vì em bỏ vợ. Tôi vỡ lẽ, chồng em dù không ra gì nhưng chung thuỷ với em. Ưng ông, biết đâu mai mốt chán, ông bỏ em như chơi.

- Em không sợ ai bỏ mình hết. Còn thằng chồng em nó mà chung thuỷ cái cù lôi. Nó lấy tiền em đi ôm con khác.

Tới chỗ này người khách không chịu nổi, đứng bựt dậy, mặt phừng đỏ:

- Vậy em cần gì ở anh ta mà chịu đấm ăn xôi?

Em cũng lớn tiếng hét trả lại:

- Em không cần gì hết. Nhưng con em cần một người cha… Hồi đó cha bị Pôn Pốt giết khi em mới biết đi. Mồ côi… bị ăn hiếp đủ phương. Em không muốn con mình gặp cảnh vậy. Kệ em đi…

Em nói nhẹ hều như giỡn chơi. Người khách thì đuối muốn rụng từng sợi thần kinh. Ông thở như sắp chết, dáng đọng vũng trên ghế. Tôi  nghĩ nếu đứng dậy được ông sẽ đi và không bao giờ trở lại nơi này. Nhưng hôm sau ông lại tới. Hình như ông vẫn còn nuôi hi vọng ...

Tôi kêu em để thằng nhóc tôi giữ cho. Vô đó với ông đi. Lâu lâu cũng được…
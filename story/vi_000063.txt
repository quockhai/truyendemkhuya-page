Buổi sáng thức dậy, bước ra cửa, chợt thấy trời trong veo, những ánh nắng ngọt mát, dịu dàng bắt đầu lan tỏa. Gió cũng ngọt ngào và mềm mại hơn. Tiếng hót líu lo, sảng khoái của bầy sẻ non ríu ran khắp hiên nhà. Sấu đã xanh thẫm lá, phượng đã thả đầy những thanh bảo kiếm xanh non, sản phẩm của cả một mùa rút ruột mình làm lửa. Từng giọt thời gian đã chảy dài dưới mỗi cành dương liễu…

Vậy là trời đã sang thu!

  Vườn hoa trước nhà đã nhú đầy những nụ cúc xanh non. Những nụ cúc nhỏ xíu luôn khiến tâm hồn ta xốn xang kỳ lạ. Người ta bảo hoa hồng đẹp, quyến rũ nhưng ít thủy chung. Ngày hoa nở đẹp rực rỡ, đài hoa cũng vươn mình tự hào khoe sắc. Khi cánh hoa lũa tàn cũng là lúc đài hoa rũ bỏ, từng cánh nhỏ tả tơi, tan tác, mặc cho gió trời thả sức cuốn trôi. 
Hoa cúc không quá đẹp, không quá đam mê, không quá rợn ngợp nhưng hoa cúc làm người ta nghĩ nhiều về sự bền bỉ, son sắt, thủy chung. Những cánh hoa nhỏ xíu, trắng đến tinh khôi, không bao giờ xòe nở hết mình, hoa nở càng đến độ, từng đầu cánh nhỏ càng co tròn lại, hướng về tâm hoa, nhỏ nhắn, căng đầy, tròn trịa. Ngày hoa tàn, đài hoa rắn rỏi hơn, ra sức nâng đỡ những sợi cánh mỏng manh không còn sức sống. Cánh hoa càng khô héo, tàn lũa, đài hoa càng gồng mình lên để nâng niu, gìn giữ, chỉ đến khi cả hai không còn sức sống, chúng cùng tàn lũa và cùng nhau tìm về đất mẹ.

Hoa cúc - mùa thu - những tinh khôi của đất trời, của đời người như hòa làm một. Mùa thu là mùa đẹp nhất trong năm, cũng là mùa thăng hoa của hồn người. Khi đã trải qua những non nớt, mỏng manh; những sôi nổi rực cháy đam mê và khát vọng; mùa thu vừa cho người ta chiêm nghiệm, vừa cho người ta vững bước đi lên, đủ sức chống chọi với những rét mướt của mùa đông quạnh quẽ đã đang lẩn quất đâu đây…

Mùa thu!

Bọn trẻ tíu tít đến trường, xốn xang áo quần, khăn quàng, sách bút… Không giống bọn trẻ chúng tôi thuở trước, nhón chân sáo đến trường, đầu đội trời, chân đạp đất, tay xách làn cói với vài cuốn vở mỏng teng, rồi quản bút, lọ mực. Tan trường về, áo quần, mặt mũi lem nhem mực, nhảy tùm xuống ao ngụp lặn chán chê rồi ngoi lên. Da đứa nào đứa nấy bóng nhẫy, đen giòn. Bọn trẻ bây giờ quần áo tinh khôi, khăn quàng đỏ chói, sách vở trĩu nặng hai vai, trĩu nặng cả tâm hồn. Thả cánh diều bay cao sợ vướng đường dây điện; nhảy xuống sông trầm mình tắm mát sợ nước đục ngầu làm mẩn ngứa da non; chạy dọc bờ đê hái hoa bắt bướm, chỉ thấy chang chang một màu trắng lóa của bê tông, cốt thép, không biết bên trời sông lở về đâu, sông bồi về đâu.

Mùa thu!

Lá bàng không xanh thêm nữa, quả bắt đầu chín rộ, bọn học trò tíu tít dưới gốc cây đập hạt bàng, nhặt phần nhân thơm bùi, béo ngậy chia cho nhau từng mẩu. Những mối tình đầu nảy nở từ những sẻ chia nhỏ xíu nhỏ xiu ấy, để rồi mỗi lần cầm quả bàng chín mọng trên tay, đưa lên môi nhấm nháp, lại thấy đâu đây vị ngọt nụ hôn đầu. Hoa sữa cũng bắt đầu tách mình khỏi kẽ lá chuẩn bị cuốn người ta vào những đam mê, nhung nhớ, khát khao…

Bất chợt một sớm nào thức dậy, thấy trời se se, đất se se… hình như thu đã đi rồi…
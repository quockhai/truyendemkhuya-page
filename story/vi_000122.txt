1. 
Chàng xách ba lô nhảy lên đúng toa tàu theo vé đã ghi. Tưởng đi trễ thì một mình một đường. Đường quang tạnh người. Nhưng nhầm. Người người lên lên xuống xuống ở cửa toa đông như kiến chạy lụt. Cửa toa tàu trở thành bé tin hít so với những hành lí của khách đưa theo. Bỗng chốc chàng thấy mình như thành con kiến ngập lụt trong mớ hàng hóa ấy. 

Bất ngờ nhất là đến cả chó, gà cũng xách được lên tàu. Không hiểu sao họ lách qua được cổng kiểm soát. Kể ra cũng thánh thật. Tất cả cứ vậy mà nhung nhúc lên xuống. Thế mới biết không chỉ mình mình trễ. Chàng nghĩ. Mình trễ là do kẹt xe. Không biết những vị thượng đế này do gì? Hay là quen tính giờ cao su, một trong những “bản sắc” của người nhà mình!

Loay hoay một chặp chàng cũng tới được chỗ của mình. Khi chàng tới các hành khách xung quanh đã yên vị đâu vào đấy. Còn đúng chỗ chàng. Hành lí những người đi trước quá nhiều, chàng hết chỗ bỏ, đành quăng ba lô dưới gầm ghế.
- Anh ngồi đây à?
- Vâng, chào anh!
- Vậy là đủ. Chạy ấu đi cho rồi. Lóng quá!

Chàng nở nụ cười thân thiện nhất có thể với ba người ngồi cùng bàn. Nói thân thiện nhất có thể vì không khí trên tàu khá ngột ngạt. Tết đến đít rồi mà Sài Gòn vẫn nắng. Nắng vàng mắt. 

Anh chàng ngồi đối diện xem ra khá xởi lởi. Anh lôi chai trà xanh không độ, loại truyền hình vẫn quảng cáo ra rả hàng ngày với câu slogan Tinh thần Việt Nam hòa cùng thế giới, vặn nắp làm một hớp và đẩy về phía chàng.
- Lóng quá! Làm một hớp cho mát.
- Dạ, em cảm ơn.
- Khách khí gì! Trước lạ sau quen. Đi với nhau còn dài mà. Mà anh về đâu? 
Anh ta vừa nói vừa ấn chai trà xanh không độ 100% trà xanh thiên nhiên vào tay chàng.
- Em về Thanh Hóa. Còn anh?
- Em về Linh Bình.
- Anh ở Ninh Bình à? Huyện nào vậy? Gần Hoa Lư không?
- Linh Bình là quê vợ. Vợ em đây. Nhà mình gần Hoa Lư em nhỉ? -Vừa nói anh ta vừa quay lại hỏi cô gái ngồi kế bên. Chị vợ cười, khoe chiếc răng khểnh như một di tích của tuổi thơ không chịu nhổ răng đúng thời kì.
- Dạ, gần Hoa Nư.
- Còn em ở Hà Lội. Hai vợ chồng về ngoại đón thằng cu con gửi ngoại rồi mới về nhà.
- Hà Nội sao anh cũng nói lộn n thành l?
- Không. Hà Lội cũng ba bảy loại. Em ở Gia Lâm, uống lước sông Đuống, giáp Bắc Linh lên ló thế.
*
*     *
2. 
Tít tít tít tít. Chiếc điện thoại rung lên hai nhịp. Tê đùi. Tin nhắn.
- Lần này ngồi cạnh một nàng xinh chứ?

Tin nhắn của thằng bạn thân. Như thành cái lệ. Hễ biết chàng đi đâu, bắt đầu lên xe lên tàu là nó lại nhắn tin hoặc gọi điện xem “có được ngồi cùng em nào không?”. Rất tiếc là không. Số chàng thế. Cạnh chàng là một gã trai trên dưới ba mươi. Mặt mày bóng nhẫy. Tóc vuốt gel bóng lộn, đảm bảo muỗi hay ruồi bay mỏi cánh vô tình đậu xuống kiểu gì cũng trượt chân, không gãy chân cũng gãy cánh. Lúc sau bắt chuyện mới biết gã là một tiểu đại gia mới nổi ở Sài Gòn (chữ của gã dùng). Nay gã về quê ăn tết. Trông gã khá kiểu cách và luôn muốn tỏ ra là kẻ tiền nhét đầy mông, kiểu trưởng giả học làm sang.

Nhưng lúc ấy chàng không muốn cái “vía đen” ám vào mình và bị thằng bạn cười khẩy nên chàng nhắn lại:
- Lạy trời. Lần này đúng là một em. Nhưng xấu. Chắc tao đã hóa giải được cái dớp. Lần sau nhất định khá hơn.
- Xấu cũng được. Bắt quàng lấy cho vui.
- Phải nói sao nhỉ? Mặt toàn mụn. Trông cứ như mặt đất Afghanistan vừa qua một trận oanh tạc của Mỹ. Toàn ổ gà ổ voi, xe tăng chạy vào chắc cũng xa lầy.
- Hehe. Nó mà biết nó vả mày rụng răng. Vậy nha. Đi an toàn.

Vài tin nhắn của thằng bạn khiến chàng sực nhớ một kết luận chung, một tóm lại là, chàng thua hẳn nó cái khoản đi đường. Bốn năm đại học xa nhà. Hai thằng ở cùng phòng. Đi đi về về cỡ hai bàn tay tính chưa hẳn hết lượt. Xe có. Tàu có. Nhưng chưa một lần chàng may mắn được ngồi cạnh một em nào đó. Hoặc cụ ông. Hoặc nam nhi trai tráng. Nếu là nữ cũng cỡ tuổi mẹ hoặc… bà ngoại. Chẳng bù cho thằng bạn lần nào đi đâu nó cũng vớ được em ngồi cạnh để tỉ tê, tán gẫu cho đỡ buồn, đỡ ngáp vặt. 

Có lần trên chuyến xe tour, hồi năm thứ tư nghỉ hè về, nó ngồi cạnh một em sinh viên năm hai trường nghệ thuật. Xe xuất phát lúc 6g tối mà 11g khuya, khi xe tắt điện, hành khách đa phần ngủ, riêng nó và con em ngồi cạnh hôn nhau xoắn xuýt. Nó và con em ngồi ngay sau ghế anh tài. Tới khi con em xuống xe anh tài xế mới quay lại nói, trên xe này sướng nhất mày? Yêu lâu chưa mà dữ dằn vậy? Nó đáp tỉnh rụi: Vừa quen lúc lên xe. Anh tài còn thiếu nước vái sống nó làm sư phụ.
*
*     *
3. 
Rồi tàu cũng rú lên một hồi như lợn bị chọc tiết, và chạy. Chàng lôi trong ba lô ra tờ báo, và đọc. Ở hai ghế đối diện qua lối đi giữa tàu là hai ông bà già, nếu nhìn mặt đoán tuổi theo kiểu game show Nhìn hình đoán chữ đang rất hot trên truyền hình thì tuổi hai cụ cỡ tròm trèm 70. Ông già lôi thỏi kẹo cao su, rút một cái nhai chóp chép, và không quên mời bà già bên cạnh. Bà già xua tay:
- Ấy, tôi cảm ơn. Tôi là cứ phải cái nhà anh này. 

Nói rồi bà lôi trong túi ra miếng trầu như đã chuẩn bị sẵn từ trước. Cũng nhai chóp chép và cười độ lượng. Nụ cười như vớt lên từ thế kỉ trước.
- Tôi hỏi khí không phải! Chứ nhà ông về mãi đâu? 

Bà già xòe hàm răng đen nhức pha chút đỏ hều của nước cốt trầu hướng về phía ông cụ.
- Tôi về Nam Định. Còn bà, bà xuống đâu? Sao đi một mình? Con cháu đâu cả?
- Ối dào, con cháu còn lu bu. Bọn chúng quay như chong chóng. Dễ phải sát tết mới hết việc, chúng bay ra sau. Tôi vào chăm bẵm cháu. Chẳng giấu gì ông. Có thằng cháu đích tôn mà ở xa cả nghìn cây số. Ông nhà biệt phái tôi vào Nam cho yên tâm đấy. Nhưng đi một thời gian lại thấy nhớ nhà, nói ông lại cười, nhớ hơi ông ấy, cái tuổi già, nghĩ mình ông ấy thui thủi ở nhà cũng nhớ, cũng tồi tội, thế là tôi lại về.

- Còn tôi thì đi đám cưới thằng út. Chúng làm công nhân trong này. Làm ăn kiểu gì không biết, mới vào đầu năm cuối năm đã gọi điện kêu bố mẹ vào cưới gấp. Không biết chúng yêu ọt ra làm sao.
- Chắc lại theo phim ảnh. Tôi ở trong này suốt ngày ru rú với cháu, chẳng dám bước đi đâu. Thành ra ngồi ôm thằng đích tôn và ti vi. Mà bật lên, loạn xì ngầu, kênh nào cũng thấy yêu. Lũ trẻ giờ yêu kinh khủng quá. Cái nết nó chạy đi đâu hết. Phải ngày xưa thì ăn đòn gánh cả lượt. Lại còn trai yêu trai, gái yêu gái nữa. Thấy mà chết khiếp. Dưng mà đám cưới vui chứ ông? - Bà cụ như bắt đúng mạch, được thể trút một hơi dài, sau như sực nhớ là hơi quá đà, đành hỏi một câu vớt vát.

- Vui gì bà. Nhạc nhẽo đập muốn inh tai. Cả sáu đám cưới tổ chức cùng một lúc. Công nhân mà. Hoàn cảnh cả. Thành ra nhà hàng bình dân ngăn ra làm sáu. Sáu sân khấu chỉ cách nhau bằng cái dây thừng căng ngang. Bên này đang nói thì bên kia mở nhạc. Sáu loa thi nhau phát. Chẳng phân biệt được đám nào với đám nào. 
Tôi lên phát biểu. Mà nào phải phát biểu. Bị “biểu” thì phải “phát” thôi. “Phát” ra cũng không nghe được là mình nói gì vì hai bên nó hát át đi cả. Mệt quá đi cày. Nhưng mà cũng xong. Mong sao chúng bảo nhau tu chí làm ăn. Không thì tếch về quê mà sống. Chứ cứ sôi sùng sục như thế, tôi ở ba ngày mà ngứa hết cả người.
Nói rồi hai ông bà già cười tuế tóa. Nhìn điệu cười ông già bà lão dám chắc các cụ cũng từng “vang bóng một thời” lắm, nghiêng đình đổ bến sông lắm. Cái thời gian nó kinh thật!

 
 Minh họa: Lê Trí Dũng

*
*     *
4. 
– Ôi trời, sốt ruột quá. Tàu gì chạy cứ như gà rù thế này. Đụng tí nà dừng. Đến cái ga nẻ giữa rừng cũng dừng. Đi ngủ. Trải chiếu ngủ thôi.
- Gì mà em cứ lộn ruột lên thế. Ngồi ngắm cảnh đi. Đẹp thế lày mà.
- Đẹp gì! Chỗ nào chẳng thế. Đất nước mình nàng quê nào mà chẳng giống nhau. Cây cối, ruộng nương và đất đá.
- Bậy lào! Mỗi lơi đẹp mỗi kiểu chứ. Em chán quá!

Tiếng nói qua lại của đôi vợ chồng quá dễ thương. Anh chồng thì cứ n thành l, còn chị vợ lại l thành n. Không biết con cái sau này sẽ nói theo trường phái nào, hay là “song tiếng hợp bích” của cả bố và mẹ. Chàng thoáng nghĩ và tỉnh khỏi tờ báo. Từ lúc tàu chạy chàng đã ngốn hết ba tờ báo. Có thể trước khi về tới nhà còn phải đọc hết hai cuốn sách nữa. Nhưng đọc mãi cũng nhức mắt. Tàu chạy ì à ì ạch, giật lên giật xuống nên câu chữ cũng thành như có chân, nhảy loạn cả lên.

- Mà sao tàu mình cứ phải tránh niên tục thế nhỉ. Tức anh ách - Chị vợ lại tiếp tục.
- Em đừng kêu lữa được không. Như thế lày là sướng lắm rồi. Đi xe khách có được ngồi tử tế không? Chúng lại không nhét cho sặc mật ra thì chớ. Giá cả thì chém cho lên tới trời. Về đến nhà chỉ còn thấy ông bà ông vải.
- Anh thì núc nào cũng vô tư được.
- Vô tư cho ló khỏe. May mà mua được vé tàu. Cái gì của nhà lước cũng tốt, cũng yên tâm nhỉ?
- Anh cổ nỗ vừa thôi. Giờ còn ôm nấy nhà nước có mà chết. Tàu xe nhà nước gì mà ngày nễ, tết nhất cũng tăng giá vùn vụt. Đáng nẽ đã nà nhà nước phải giữ giá ổn định chứ. Rồi thành phường buôn cả nượt.
- Không thế có mà nhà nước chết héo? Kinh tế giờ nó thế cả chị ơi. Nhà nước cũng phải làm kinh tế mới sống được. Anh chàng tiểu đại gia Sài Gòn ngủ gà ngủ gật giờ cũng lên tiếng góp vui, giọng rất đời.

Chị vợ vẫn đầu gác lên đùi anh chồng Hà Nội uống nước sông Đuống, chân gác lên thành ghế và nói:
- Cái quái gì rồi cũng đổ nên đầu dân. Rút cuộc chỉ dân nà khổ. Dân càng nghèo càng khổ. Bọn giàu thì cứ nứt đố đổ vách. Bắn cũng không hết. Còn dân nghèo có ngóc đầu nên được đâu. Chị vợ nói ban đầu to rồi nhỏ dần nhỏ dần như chỉ cho mình chị nghe.
- Giàu nghèo thì cũng giống nhau thôi, hơn gì đâu. Cũng “hơi” và “ôm”. Giàu thì đi xe hơi, uống bia ôm. Nghèo thì đi xe ôm, uống bia hơi. Anh tiểu đại gia Sài Gòn ngáp ngang và phè ra một câu như nhặt được từ quán nhậu. 

Chàng và vợ chồng anh Hà Nội uống nước sông Đuống cùng cười hỉ hả, và nói như anh chồng là câu nói “hay một cách quá đáng, hay không chịu được, mãi sau mới… chịu được”.
*
*      *
5. 
Tít tít tít tít. Lại tin nhắn. Có vẻ như đi tàu xe ngoài nghe phone thì nhắn tin là một việc hay được tranh thủ nhất. Biết vậy nên lũ bạn hay săn sóc. Lần này là tin nhắn bằng thơ hẳn hoi: “Tình hình liệu có OK/Sao lâu chẳng thấy ủ ê nhắn gì?”

Từ khi có anh di động kể ra cũng tiện. Gấp thì a lô hay từ từ thì nhắn tin cái là xong. Và nhiều tên thành dở người, phát tiết thăng hoa dở giọng Bút Tre, tin nhắn cứ câu sáu câu tám, làm như khua phím là ra thơ kiểu gà công nghiệp đẻ trứng. Thấy thơ thì chàng cũng nhăn trán nặn vài câu đáp trả, con gà tức nhau tiếng gáy, chàng chơi hẳn bốn câu: “Mày à, tàu vẫn chạy thông/Riêng tao thì đã ê mông lắm rồi/Hết đi đến đứng lại ngồi/Lần sau quyết sẽ lên trời với máy bay”. Nhấn OK. Tin nhắn gởi đi. Nhưng rồi lại quay lại. Hóa ra không có sóng. Nhìn ra cửa sổ. Tàu đang leo đèo. Dưới kia là biển, sóng vỗ ì ùng mà máy trên này không một cột sóng. Hay thật!

Mười phút sau tàu lao xuống, qua đèo mới có sóng trở lại và gởi tin đi. 

Tất nhiên tin nhắn gửi đi là những dòng chữ không dấu, cho nhanh, cho tiện. Người viết sợ độc giả không dịch được nên phải viết đầy đủ dấu. Tiếng Việt nó giàu và đẹp mà (lời các cụ dạy thế). Nếu không dấu nhiều khi từ chữ này nó nhảy tọt sang chữ khác, có trời mới hiểu là gì. Chả vậy mà hồi năm tư chàng nhận được một tin nhắn của cô em trong xóm trọ mà tá hỏa. Em nhắn: “Em dang o truong. Anh den nhanh di. Em cho. Muon lam roi!”. Ai nhìn vào những dòng ấy mà không giật mình, căng cứng người, căng cứng bụng trên bụng dưới mới là không bình thường. Nhưng ý em nó là “Em đang ở trường. Anh đến nhanh đi. Em chờ. Muộn lắm rồi!”, chứ đâu phải...
*
*       *
6. 
- Trời ơi, quá trời sao luôn! - Giọng anh Hà Nội uống nước sông Đuống pha thêm tí Sài Gòn, kêu lên.
- Mệt anh quá. Thôi ngủ đi. Trải chiếu xuống sàn ngủ đi. Chín giờ tối rồi còn gì.
Chị vợ không đoái hoài tới trời trăng mây gió, tới khái niệm đẹp mà anh chồng thao thao bất tuyệt từ khi lên tàu. Và anh chồng thì xem như đấy là chuyện thường ngày, khẽ cười khì, đứng lên ghế với tay lấy chiếc chiếu và hai con thú bông từ sàn gác đồ xuống.

Hai con thú bông giống nhau. Hình thù một con sâu xanh. Chàng cười:
- Anh chị có hai cái gối ôm đẹp quá!
- Đẹp gì! Quà tết của công ty đấy. Tết không cho gì, tặng công nhân toàn hàng thanh ní. Ôm hai đêm trật bụng ra đây. 

Vừa nói chị vợ vừa xọc tay vào bụng con sâu lôi ra vài sợi bông hóa học, cười cười. Nụ cười hồn nhiên. Rồi anh Hà Nội uống nước sông Đuống trải chiếu và hai vợ chồng xuống nằm dưới sàn tàu trong gầm ghế. Anh bảo chàng qua ghế anh chị nằm mà ngủ, còn ghế kia để anh tiểu đại gia Sài Gòn ngủ, cho thoải mái. Chàng lấy làm ngại khi anh chị nằm chui đầu vào gầm ghế mình. Chàng nói, hay anh chị quay đầu ngược lại, ngại quá. Anh chồng cười. Nụ cười “vô tư đi”: “Làm gì đâu mà ngại. Đừng đánh rắm, thả bom xuống dưới lày là được”. 

Giờ này điện trên các toa tàu đã tắt, chỉ còn để bóng ngủ mịt mù như chút ánh sáng của đom đóm đực. Nhưng sao hai anh chị nằm dưới sàn vẫn chưa ngủ. Cứ nghe rục rà rục rịch. Chàng không rõ họ đang làm gì dưới đó. Chị vợ kêu buồn ngủ từ trưa mà giờ có vẻ tỉnh như sáo tắm. Chàng chợt nghĩ không biết có cặp nào đi tàu thế này mà làm được cái việc duy trì nòi giống không? Nghĩ rồi tự rủa mình vớ vẩn quá. Họ vô tư quá. Chồng công nhân. Vợ công nhân. Ở trọ. Con nhỏ gởi ông bà ngoại cả năm trời mới về nhà một lần. Thế mà vẫn vui. Bất chợt chàng nhớ tới nàng.

Giờ này liệu nàng đã ngủ chưa? Ở bên ấy nàng có vui được như vợ chồng anh chàng Hà Nội uống nước sông Đuống không? Chỉ tại chàng mà nàng ra đi. Hai lăm tuổi rồi. Tết năm ngoái về quê, chàng cùng bố qua nhà nàng chơi. Chàng giật mình. Sao nàng đẹp, hiền và dễ thương vậy. Thời buổi này con gái không tìm đâu thấy sự thuần khiết như ở nàng. Như có luồng điện chạy dọc sống lưng, rẽ ngang vào ngực trái. Chàng biết mình đã rung. 

Nàng mới đi xuất khẩu lao động về. Thời điểm ấy bao quanh chàng có khối em. Sinh viên ngân hàng có, kinh tế có, báo chí có. Tất cả các đối tượng ấy hứa hẹn một tương lai dễ thấy hơn. Nhưng không hiểu sao chàng cứ bị hình ảnh nàng làm đắm đuối. Nửa tháng ở nhà chàng không tài nào thoát khỏi thứ từ trường ấy từ phía nàng. Và nàng cũng cảm nhận được. “Em nên ôn lại và thi một trường nào đấy, cao đẳng hay trung cấp cũng được. Giờ các trường không ít, toàn thiếu học viên, sinh viên để đào tạo thôi”. Chàng nói. “Em hai lăm tuổi rồi. Liệu học có kịp không?”. “Có gì mà không kịp. Hai tám tuổi học xong cũng vừa mà”. “Nhưng em sợ. Nếu có người làm điểm tựa, em mới dám ở lại nhà, đi học”. Nàng nói đến thế rồi mà chàng còn không dám chìa mình ra nhận làm điểm tựa. 

Nói như Xuân Diệu là chàng chỉ biết yêu thôi chẳng biết gì. Một chút mơ hồ sợ nàng không làm được le lói. Chàng chần chừ. Trong lúc ấy, nàng nhận thấy sự thiếu quyết đoán. Thế là nàng lại đi. Máy bay cất cánh về phía Mã Lai chàng mới nhận được tin từ em gái nàng. Thế là hết. Dù cố gắng mọi cách chàng không tài nào liên lạc được. Có phải khi mất đi một cái gì đó con người ta mới thấy hết được sự cần thiết về sự có mặt, sự hiện hữu của nó bên mình. Thành ra con người đâu đã là loài “tiến hóa”, hoàn thiện tới mức hoàn hảo như ta tưởng. Còn cần phải thêm nhiều thời gian để “tiến hóa” đến tận cùng, để tránh những bi kịch yếu đuối nữa.

*
*      *
7. 
Bỗng tiếng gà gáy te te kéo giật chàng khỏi ý nghĩ. Trên tàu cũng có tiếng gà. Thú thật! Chàng ngước đầu lên. Gần như hành khách đã ngủ hết. Anh chàng tiểu đại gia Sài Gòn ở ghế bên mở miệng ngáy khò khè như bò kéo xe ngược dốc, nước dãi tràn cả ra một bên mép, hiện nguyên hình một anh nông dân không thể lột xác thành giới thượng lưu thị thành. Dưới gầm ghế vợ chồng anh Hà Nội uống nước sông Đuống ngủ say bí tỉ. Ông già bà cụ ở ghế đối diện cũng không tìm đâu thấy dấu hiệu của… thức. 

Bất giác chàng nhận ra ánh trăng đan loang loáng trên ô cửa toa tàu. Thứ ánh sáng vàng vàng bàng bạc, lấp lóa. Ngày mai. Ngày mai chàng sẽ về tới nhà. Có thể tết nàng về lắm chứ. Đúng. Chỉ ngày mai thôi. Ngày mai ông già bà cụ sẽ về với con cháu, mang theo những ám ảnh về một Sài Gòn “kinh khủng” hơn một làng quê Bắc Bộ. Ngày mai vợ chồng anh Hà Nội uống nước sông Đuống sẽ gặp cu con, liệu thằng cu con có chấp nhận hai người là bố mẹ nó sau một năm đằng đẵng không thấy mặt? 

Ngày mai tiểu đại gia Sài Gòn có quyền kênh kiệu ở làng. Rồi sau đó là gì nữa. Không biết. Chàng chỉ nghĩ nhất định đợt này về chàng sẽ gặp nàng. Và thật dứt khoát. Phải cho nàng biết chàng đã nghĩ gì suốt một năm qua. Chàng thấy nóng lòng kinh khủng. Chàng cứ trở mình, trằn trọc với ý nghĩ con tàu chạy chậm quá, thời gian trôi chậm quá. Chẳng biết bao giờ mới tới được ngày mai! 
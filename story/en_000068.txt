Anil Chandra là một cây bút truyện ngắn sống tại New Delhi, Ấn Độ. Cuốn sách đầu tiên của ông mang tựa đề “Cam kết của Hakim và những câu chuyện khác”, bao gồm hàng loạt các truyện ngắn, ngay sau khi xuất bản đã lập tức đã thành công. Ông đã viết hơn 125 truyện ngắn. Hầu hết những truyện ngắn này đã được xuất bản, được dịch sang tiếng Hindi và được độc giả đón nhận nhiệt thành.

 
 Tác giả Anil Chandra

Anil Chandra là một sinh viên theo học ngành hóa học nhưng lại rất đam mê lịch sử. Ông là tác giả của ba cuốn sách về lịch sử: “Cái nhìn thoáng qua về Ấn Độ cổ đại”, và “Cái nhìn thoáng qua về Trung Quốc cổ đại” và “Đại cương Lịch sử và Văn hóa Trung Quốc” (từ năm 1200 đến 1949).
 
Sukhbir Chauhan ngồi tựa lưng vào một tảng đá, chiếc khăn xếp trên đầu rủ qua mắt, cậu có thể cảm nhận được những tia nắng ấm áp đang nhảy nhót trên đôi chân trần của mình. Đôi chân luôn chỉ đường cho cậu. Đôi chân có thể cảm nhận được cát và đá, cảm nhận được mọi loài cỏ cây, hoa lá.
           
Một con chim không biết từ đâu bay tới cất tiếng hót líu lo. Dường như Sukhbir hiểu được những điều nó muốn nói. Có lẽ bởi cậu bị mù, nên cậu có thể dễ dàng cảm nhận và hòa mình vào những thanh âm trong thế giới của các loài chim và thú. Cậu không bao giờ nói với ai về điều này ngoại trừ anh trai Rajbir và ông nội Shivpriya Chauhan của mình. Họ không bao giờ cười nhạo những gì cậu vẫn tưởng tượng. 
           
Sukhbir mới mười bốn tuổi và cậu biết rằng mọi người đều thương hại cậu. Nhưng cậu nghĩ rằng mọi người sẽ ghen tị với cậu nếu họ biết được thế giới của cậu đẹp như thế nào. Chẳng ai có thể nghe được tiếng xào xạc của cây cỏ, cũng khó có thể thể cảm nhận được sự vật một cách rõ ràng như cậu, những vật tròn như quả trứng, như con sâu nước, sự thô cứng của đá, sự mềm mượt của những tấm da thuộc hay sự xù xì của vỏ cây. Họ không thể ngửi được tất cả các mùi vị của cuộc sống. Họ đã bỏ qua rất nhiều điều thú vị trong cuộc sống. 
          
 Hàng ngày, Sukhbir lên núi chăn đàn dê hàng trăm con của cha mình. Cậu nhận biết chúng qua những âm thanh của móng guốc in trên những hòn đá và qua mùi của chúng. Việc chăn dê đối với Sukhbir thật đơn giản. Chỉ cần Sukhbir cất tiếng gọi là chúng chạy đến. Khi Sukhbir thổi sáo, chúng theo cậu vượt qua những cánh đồng và tha thẩn trên những sườn đồi. 
          
 Lúc này trời có vẻ lạnh hơn. Đã đến lúc khởi hành. Sukhbir rút cây sáo đặt lên môi, thổi một điệu nhạc. Đàn dê ngước đầu lên và hướng mắt về phía cậu. Cậu có thể cảm nhận được một sự di chuyển nhẹ nhàng về phía mình. Cậu lại thổi thêm một điệu nữa. Cả đàn dê bước theo chân cậu. Cậu sẽ dẫn chúng xuống núi, nhốt chúng vào chuồng, và ngày mai cậu sẽ lại dẫn chúng trở lại nơi này. Ngày qua ngày đều như vậy, một cuộc sống thật yên bình và hạnh phúc. 
           
Nhưng niềm hạnh phúc ấy chẳng hiện hữu được bao lâu khi Rani Lakshmi Bai, nữ hoàng của Jhansi, tuyên chiến với người Anh và đội quân bất khuất Tantia Tope đã ra đời. Sukhbir thấy cha và anh trai Rajbir thắng yên ngựa. Họ ôm lấy cậu, hôn tạm biệt cậu, súng trường của họ áp vào ngực cậu.
           
Âm thanh của tiếng vó ngựa xa dần, cậu cảm nhận được mẹ đang đứng bên cạnh mình. Bà choàng tay ôm lấy cậu. "Chúng ta sẽ làm gì bây giờ hả mẹ?" Cậu hỏi mẹ. 
           
"Con trai, chúng ta sẽ đi khỏi nơi này. Chúng ta sẽ chuyển đến nhà ông nội của con. " 
          
 "Nhưng ở đó tối lắm. Con không thuộc những cánh đồng ở đó, làm sao con có thể chăn đàn dê của cha ở một nơi xa lạ và tối tăm như vậy?” Lần đầu tiên Sukhir ý thức được bóng tối đáng sợ như thế nào. 

           Sáng hôm sau, bầy ngựa đã được cột vào xe bò, cả đàn gia súc, gia cầm và đàn dê cũng đã sẵn sàng cho chuyến đi mười lăm cây số. "Lên xe đi Sukhbir," mẹ cậu giục. "Chúng ta đi thôi." 
           
Sukhir ôm một vật gì đó bọc bằng vải lanh. Cậu nói: "Con sẽ mang theo khấu súng trường mà anh để lại. Mọi người vẫn nói rằng đôi mắt của đen láy và rất sáng. Nếu có điều gì bất trắc xảy ra, con sẽ bảo vệ mẹ với khẩu súng này. " 
          
 Sukhbir leo lên xe ngồi bên cạnh mẹ. Đột nhiên cậu nói, "Con sẽ chẳng làm được gì nữa, mẹ à. Chúng ta sẽ đến một nơi xa lạ mà đôi chân của con không quen biết, và con không thể làm gì được". Cậu đã cảm nhận được bánh xe bắt đầu chuyển động.
           
Lát sau cậu lại cất tiếng hỏi: "Hãy nói cho con biết mẹ nhìn thấy những gì? Mẹ có còn thấy ngọn núi nơi con thường vẫn chăn đàn dê của cha không?" 
          
 Khi họ đến nơi, ông nội của cậu đã lên đường, tất cả mọi người, cả những người đàn ông lớn tuổi và các cậu bé đều đã lên đường gia nhập đội quân Tantia Tope. Sukhbir bị bỏ lại một mình. Không có việc gì khác ngoài việc phải chờ đợi. 
           
Ngày qua ngày, mọi chuyện vẫn cứ như vậy, không có một tin tức gì mới. Sukhbir cố tìm cách thích nghi tốt hơn với trang trại của ông nội. Nhưng điều đó không hề dễ dàng, nó không giống với ở nhà. Cậu không thuộc về nơi này.
         
 Bỗng một ngày, có một vài binh lính thất thểu trên những con ngựa mệt mỏi lững thững đi ngang qua. Sukhbir lắng nghe. Những bước chân ngựa mệt mỏi, mùi mồ hôi của những người đàn ông đã nói cho cậu biết những gì cần biết. 
           
"Các chú có gặp cha và anh trai của cháu không?” cậu bé cất tiếng hỏi.
          
 "Cháu là ai?" 
          
 "Cháu là Sukhbir Chauhan," cậu trả lời. "Cháu bị mù. Cha cháu đã gia nhập đội quân Tope Tantia. " 
           "Chúng ta cũng đang trên đường gia nhập đội quân Tantia Tope. Chúng ta sẽ nói cho ông ấy biết là chúng ta đã gặp cháu. " 
          
 Một lúc sau, cậu lại nghe thấy rất nhiều tiếng vó ngựa tiến đến. Nhưng đó không phải là những con ngựa của quân đội Tantia Tope. Chúng chạy nhanh hơn và người cưỡi ngựa ghì cương không chặt. 
          
 Cậu có thể nghe thấy tiếng nhạc ngựa leng keng. Đó là người Anh. Họ đang đuổi theo những người đàn ông mệt mỏi lúc nãy. 
          
 "Mày có thấy một nhóm binh sĩ đi ngang qua đây không?" Đó là người đàn ông trên con ngựa gần nhất, cậu tin chắc rằng đó là tên chỉ huy. 
          
 "Cháu ở đây cả ngày," Sukhbir trả lời. "Nhưng cháu chẳng gặp ai cả."
         
"Chúng chỉ có thể chạy theo hướng này về phía những ngọn đồi. Chúng không thể đi xa được. Ngựa của chúng đã rất mệt rồi " một người đàn ông nói.
          
 Đúng như vậy, ngựa của họ đã mệt mỏi, Sukhbir nghĩ, nhưng thật lạ lùng là những người này không thể tìm thấy hướng đi của các binh sĩ Tantia Tope, trong khi mà cậu vẫn còn cảm nhận được âm thanh tiếng vó ngựa của họ và mùi phân ngựa vẫn còn ấm và chưa bị bụi phủ. 
          
 "Đi nào," tên chỉ huy ra lệnh “Chúng không thể đi xa được.”           
            
Ngay sau đó, lại một nhóm binh sĩ cưỡi ngựa lao vụt qua Sukhbir. Ngay từ xa, cậu có thể nhận thấy có khoảng ba mươi người trong số binh sĩ đó. Nếu là một đội kỵ binh Anh thì phải nhiều hơn. Cậu căng tai chờ đợi. Họ sẽ sớm đến thôi. 
          
 Cậu nghe thấy một phát súng. Thêm ba phát nữa - đó là quân mình. Người Anh không bắn như thế. Chúng không đánh hăng như đội quân Tantia Tope. Những phát súng dầy hơn. Họ đang dồn nhau về phía ngọn núi. Cậu bắt đầu thấy lo sợ cho Rajbir và cha của mình. 
        
  Cậu thất thểu quay về.
          
 "Con có gặp những người Anh không?" Mẹ cậu hỏi. 
          
 "Có, con đã gặp họ thưa mẹ. Họ đã nói chuyện với con. Họ đang đuổi theo những binh sĩ đang trên đường gia nhập đội quân Tantia Tope. Cha và anh Rajbir cũng đang ở đó. " 
       
"Mẹ ước gì có thể biết được tin tức gì đó về cha và anh con” mẹ cậu nói. 
        
"Con cũng thế mẹ à. Không biết cha và anh Rajbir bây giờ thế nào. Con thấy buồn vì mình cũng là một người đàn ông mà phải ở lại nơi này. Ngày hôm nay, con thấy thật xấu hổ. Con đã phải đối mặt với các binh sĩ Tantia Tope và sau đó là với lính Anh. Con đã nói chuyện với các binh sĩ quân mình nhưng con đã không nói tiếng Anh. Họ không biết rằng con không thể nhìn thấy được. Mẹ à, con chẳng thể làm được gì hết có phải không?"
          
Cả đêm hôm đó Sukhbir không ngủ. Cậu dằn vặt, suy nghĩ xem mình có thể làm gì để giúp đỡ những người khác, đó là cuộc đấu tranh với chính bản thân mình. Cậu nhớ lại những cảm nhận về những giọt mồ hôi còn đọng trên cổ, tưởng tượng ra hình ảnh của những con ngựa, mùi từ những người đàn ông giống như những người cậu đã gặp ngày hôm đó, những âm thanh của bu lông súng trường, mùi nồng nặc của thuốc nổ, tiếng đạn nổ tanh tách, tiếng vó ngựa, và tiếng nói chuyện lầm rầm. Đột nhiên, cậu bật dậy và hét lên: "Mẹ ơi! Mẹ ơi! Con vừa có một giấc mơ. Con mơ thấy anh Rajbir cưỡi ngựa quay trở về. Anh ấy bị thương." Người mẹ cũng bật dậy. Ngay sau đó, họ nghe thấy tiếng vó ngựa bên ngoài: "Khẩu súng", cậu hét lên, "Hãy đưa cho con khẩu súng!" 
         
 Bà vội đặt khẩu súng trường vào tay Sukhbir. Cậu mở các bulông rồi đóng lại. Cậu tra đạn vào nòng. Cậu lần về phía cửa và từ từ mở nó ra. Con ngựa cứ tiến lại gần không hề chạy chậm lại.
          
 "Mẹ hãy bật đèn lên” Sukhbir nói. "Hãy đứng bên cạnh con." 
           
Sukhbir đứng giữa cửa. Chân trần bám chặt vào sàn đá. Cậu giơ súng lên, sẵn sàng bóp cò.
           
"Dừng lại không tôi bắn", cậu hét lên. "Đó là con ngựa của Rajbir, nhưng cũng có thể là không phải “, cậu nghĩ.
        
 "Sukhbir … Sukhbir, em đang làm gì với khẩu súng vậy?" Đó là anh Rajbir.
        
 "Chuyện gì thế này, Rajbir?" người mẹ la lên.
        
 "Mẹ, con không có thời gian để giải thích. Con đến để đưa Sukhbir đi.Tướng Tantia Tope cần nó. " 
          
 "Tantia Tope? Con định đưa Sukhbir đến vùng chiến đấu ư? Sukhbir có thể làm gì ở đó chứ?" 
          
 "Em sẽ đi. Em có thể làm gì? "Sukhbir hỏi.
          
 "Anh sẽ nói cho em biết trên đường đi. Đến đây nào. " 
          
 Sukhbir tiến về phía con ngựa và cảm nhận được chân của anh trai. Cậu nắm lấy bàn đạp ngựa và đặt chân của mình vào đó. Rajbir nắm lấy tay trái của cậu và kéo lên. Cậu đã lên được ngựa.
          
 "Giữ chặt vào, Sukhbir. Chúng ta xuất phát." 
           
Cậu đã hầu như không thể nắm được thắt lưng của anh trai khi con ngựa phóng đi. Họ phi nước đại. Trời bắt đầu mưa. Họ sẽ đi đâu? Họ cần cậu để làm gì? Tại sao tướng Tantia Tope lại cần cậu?
          
 Mặt đất gồ ghề hơn. Có tiếng đá. Đột nhiên, Sukhbir ngồi thẳng lên. Cậu ngửi thấy mùi của núi, ngọn núi của cậu ...
           
"Chúng ta đến nơi rồi," Rajbir nói và thắng ngựa lại. "Tôi đã trở lại," Rajbir hét lên. Sukhbir cảm nhận thấy cánh tay của anh trai vòng qua người mình. Anh đưa cậu xuống đất. 
          
 "Sukhbir có đó không?" Đó là tiếng nói của cha. 
          
  "Con ở đây. Anh Rajbir đã đưa con đến đây.”
          
 "Cậu bé đâu?" giọng của tướng Tantia Tope. 
           
"Chúng tôi đang ở đây," Rajbir nói. "Ngài hãy giải thích cho Sukhbir hiểu. Tôi vẫn chưa nói gì cả. " 
           
"Nghe này, Sukhbir," Tantia Tope nói."Chúng ta có một trăm người. Người Anh đang ở phía Bắc và lính biệt kích đang dẫn chúng trở lại. Bọn người Anh không biết chúng ta đang ở đây và chúng sẽ rút lui về phía bắc qua con sông mà chúng ta đang chiếm giữ. " 
          
 "Cháu hiểu rồi," Sukhbir nói. "Chúng sẽ bị đánh úp."
          
 "Đúng rồi," Tantia Tope nói. "Nhưng mọi chuyện đã không như vậy. Bọn người Anh đã lên đỉnh núi bằng lối khác. Chúng ta ngăn chúng vượt qua nhưng chúng lại bao vây chúng ta. Chúng ta sẽ tấn công chúng đêm nay nhưng chỉ có một con đường lên núi từ phía bên này. Một con đường mòn rất nhỏ. Chúng ta không thể làm được gì cả khi mà trời tối đen như mực thế này. Anh trai của cháu nói rằng cháu có thể dẫn chúng ta lên núi. "
          
 "Cháu ư. Cháu dẫn đường cho đội quân Tantia Tope ư?" 
           
"Đúng, là em, Sukhbir," người anh nói. "Em rất thuộc con đường này mà."
           
"Đúng, em biết rất rõ con đường này." Tất nhiên. Trước đây hầu như ngày nào cậu cũng đi qua đoạn đường này. 
           
Họ bắt đầu đặt những bước chân đầu tiên lên con đường. Cha theo sau cậu, sau đó đến Rajbir, và tiếp đó là những người khác, tất cả theo sau cậu lên ngọn núi của chính cậu. Cậu đã dẫn đường cho các binh lính Tanti Tope.
           
Chân của cậu đã quen với mỗi hòn đá, mỗi rễ cây, mỗi ngã rẽ. Cậu nhận ra mùi hương của núi, của cây cỏ, những cơn gió nhẹ, bầu không khí ở nơi này và cậu biết chắc rằng trên đỉnh núi sẽ lạnh hơn nơi này rất nhiều. 
          
 "Đây là đường mòn chăn thả dê," người cha thì thầm. "Ta không biết con đã đưa chúng đến đây. Ta sẽ không bao giờ cho phép con đến nơi này. Nếu lỡ bị trượt chân... " 
           
"Con sẽ không bị trượt. Đó là con đường đến ngọn núi của con." 
          
 Cậu tự mỉm cười. Nơi này tối đen như mực. Có đốt thêm đuốc thì những người đàn ông này cũng không thể leo lên. Họ không thể trông thấy đường. Chỉ Sukhbir Chauhan mới có thể cảm nhận thấy, bởi vì cậu bị mù.
          
 "Chúng ta sắp đến nơi rồi thưa cha," cậu nói khi chạm vào mặt một vách đá. Cậu sờ vào tảng đá ẩm ướt và ra hiệu cho mọi người leo lên. Chẳng bao lâu sau, cậu và cha đã lên đến đỉnh. Từng người một cũng leo lên, thở dốc. Không có bất kì một bất trắc nào cả. Tantai Tope thì thầm chỉ thị. Binh sĩ tản ra. 
          
 Cha của Sukhbir đẩy cậu ra đằng sau một tảng đá lớn. "Con hãy ở đây, Sukhbir. Chúng ta sẽ quay lại."
           
Bây giờ cậu phải ngồi chờ đợi. Cậu cảm thấy họ đi xa dần hướng tới doanh trại của những người đàn ông đang say ngủ. 
           
Một tiếng hét vang lên và sau đó lại một tiếng khác. Sau đó là hàng loạt những tiếng la hét và tiếng đạn bắn loạn xạ. Những tiếng kêu của những người bị thương. Tiếng đạn bắn liên hồi, cậu nghe thấy giọng khàn khàn và những tiếng hét của những binh sĩ Tantia Tope: "Chúng đang bỏ chạy." 
           
Một tiếng nổ khủng khiếp. Sukhbir có thể ngửi thấy mùi khét lẹt của thuốc thuốc nổ. Lại một tiếng súng và sau đó im bặt cho đến khi cậu nghe tiếng cha gọi. "Sukhbir, con còn ở đó không?" 
          
"Con ở đây." 
          
 Ai đó nắm lấy tay cậu. Đó là tướng Tantai Tope. "Ta muốn cảm ơn cháu," ông nói. "Nếu không có cháu thì đêm nay chúng ta đã không thể thắng, ta không nghĩ rằng chúng ta có thể làm được điều này. Ta không tin rằng chúng ta lại có thể leo lên được ngọn núi này. " 
           
Tất cả mọi người vây xung quanh cậu, siết chặt tay cậu. Những đôi mắt rưng rưng, xúc động. "Tất cả là nhờ có cháu ..." 
          
 "Họ sẽ sáng tác một bài hát về sự kiện này", một người đàn ông lớn tuổi nói. "Khúc balat ca ngợi Sukhbir Chauhan bằng tiếng địa phương. Đó là mong muốn của Thiên Chúa. Người đã phái cậu bé đến dẫn đường cho chúng ta."
           
Đúng, đó là ý muốn của Thiên Chúa, người đã giúp đôi chân của cậu quen với những miền đất xa lạ, vì trước đó cậu đã chưa từng dám trèo lên đến đỉnh, Người đã để cho cậu có thể cảm thấy dấu chân của mình in trên đá.                                   
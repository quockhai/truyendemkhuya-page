- Mời vào, mời vào - người vợ vừa mở cửa vừa nói - Tôi rất muốn cậu xem giúp cái họng cho nhà tôi.

- Xin lỗi cô - anh thanh niên xách chiếc vali xinh xinh ngắt lời - cháu chỉ là thợ sửa chữa tivi, được gọi đến đây để...

- Biết rồi, biết rồi - người vợ thì thào  - Vấn đề là nhà tôi đang ốm, nhưng anh ấy sợ bác sĩ kinh khủng. Tôi gọi cậu đến, nhờ làm công tác tư tưởng để nhà tôi chấp nhận cho bác sĩ đến khám. Cậu hiểu chưa? Vào đi, vào đi!

Rồi dắt tay cậu thợ sửa chữa tivi đến bên giường bệnh nhân.

- Thợ sửa tivi đã đến rồi đấy, anh thân yêu - người vợ thì thầm.

- Tivi nhà mình làm sao vậy? - bệnh nhân hỏi, giọng run run.

- Cũng may, không có gì quan trọng lắm đâu - người vợ đáp - hóa ra, nó bị nhiễu, chỉ cần chỉnh lại ăng ten là xem được ngon lành. Nhưng cậu thợ đáng yêu này còn nhận lời tiện thể xem qua cho anh cái họng nữa đấy.

 
Minh họa của K.Long

Vẻ tin cậy của vợ chồng chủ nhà đã chinh phục cậu thợ sửa chữa tivi khiêm tốn, và cậu ta đưa tuavít đụng đụng vào chiếc răng vàng của bệnh nhân.

- Cứ nằm yên, chỉ cần nuốt một viên thuốc, uống với rượu uýtsky pha sô đa là khỏi thôi mà. Cứ tin vào cháu - Rồi cậu thợ sửa chữa tivi tận tụy cũng xin phép rút lui, tay nắm chặt hai peso thù lao do người vợ giúi vụng.

Sau đó người vợ yêu chồng còn gọi cả đàn cả lũ người nữa đến, thăm bệnh nhân và chuẩn bị tinh thần để chồng mình nhận làm cái việc khủng khiếp nhất: cho bác sĩ khám. Mỗi người một nghề khác nhau, thôi thì đủ cả: thợ đốt lò, phu khuân vác, người bán máy giặt, lại có cả hai anh thợ chụp ảnh rong, người nào cũng bắt bệnh nhân há mồm cho xem và đưa ra những lời khuyên cực kỳ bổ ích.

Mẹo của người vợ, cuối cùng cũng đạt được kết quả mong muốn. Bây giờ, người chồng phải chủ động đề xuất nguyện vọng đón bác sĩ đến.

- Em thử tính xem sao, em yêu dấu, ta có cần gọi bác sĩ không nhỉ? - người chồng nói bằng giọng quyết đoán sau chuyến thăm khám của anh chàng cảnh sát vẫn hay đứng trực ở góc bên đường.

- Không cần, anh yêu dấu - nghĩ rằng đầu óc đức ông chồng vẫn còn chưa thật sự thông, người vợ giả vờ phản đối - còn, còn một vị nữa muốn thăm khám cho anh mà. Em nghĩ, qua vị này nữa là đủ. Làm phiền bác sĩ vì chuyện vặt vãnh thế này, sao tiện? Vị sắp đến đây là một chuyên gia thu thập… thu thập gì gì thì em quên mất rồi.

- Thu-u thu-uê-uế. Thu thuế - vị khách vừa đến bèn rành rọt tiếp lời.

Bệnh nhân trố mắt nhìn vị khách vừa vào nhà, lầm bầm không rõ tiếng, đang muốn kể lể gì đó về tình trạng thu nhập và chi tiêu rất đáng buồn của gia đình mình, thì đột nhiên ngất xỉu…

Khi bác sĩ chính cống được triệu đến, thì trình độ y học của ông đã hoàn toàn bất lực, phải lập tức gọi xe cấp cứu đưa ngay bệnh nhân lên bệnh viện tuyến trên.
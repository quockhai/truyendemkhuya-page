Y cứ nghĩ mình giỏi nếu không nhận được mẩu tin vào đêm ấy…

Dĩ nhiên y không nói ra nhưng mọi động thái của y khiến ai cũng có cảm giác y luôn ngồi trên cao, cảm giác  bị y coi là tầm thường, bé nhỏ. Ngay lần đầu quen biết, y cứ một chú hai chú với tôi – y chỉ thua tôi dăm tuổi – khiến tôi cứ nghĩ y chơi xỏ. Cũng đã nghe nhiều người xì xầm nên tôi cảnh giác, chỉnh ngay: Kiểu xưng hô thế còn gì anh em. “Thế là anh cho phép em đấy nhé”. Lời tưng tửng phát ra từ một người cao, gầy, vẻ mặt khắc khổ pha chút phớt đời, lãng tử, bố ai biết được có thực lòng không.

Nói chuyện riết với y rồi đâm thích, không như những cuộc chuyện trò xã giao nhạt nhẽo của tôi với mấy tay cán bộ cùng cơ quan. Tôi làm ở Phòng Văn hóa - Thông tin huyện, trực tiếp biên tập rồi phê: hằn học, cầu kỳ, rối rắm và loại bài thơ của y một cách không thương tiếc. Thế mà y cứ cảm ơn mãi. Chả là, chẳng thấy bài thơ tâm huyết của mình được đăng, y gửi báo trung ương. Nửa tháng sau y nhận được báo biếu và nhuận bút. Lần đầu y được đăng thơ cũng là lần đầu y đến cơ quan tôi, lần đầu chúng tôi gặp nhau. Xem bài thơ trên báo tôi thấy nguyên xi như bài mà tôi bỏ vào sọt rác. Tôi chúc mừng rồi bảo: “Báo trung ương đề tài rộng hơn… Mà sao cậu không ca ngợi cuộc sống, con người ở địa phương nhỉ? Cậu có bài nào gởi không? À mà cậu viết cứ như ám chỉ ai ấy… Nhẹ nhàng chút đi!”. “Chú cứ nói thế…” – Y ỡm ờ. Y ra về sau khi chấp nhận cách xưng hô anh-em.

Từ khi y chuyển về ở kế bên nhà tôi, tôi hiểu y hơn. Y dị ứng với cán bộ nhưng rất nhũn nhặn nhiệt tình với những người chân lấm tay bùn, thấp cổ bé họng. Đặc biệt y rất yêu trẻ con. Hai đứa con tôi có được chú-bạn-thân-to-xác, tôi cũng mừng. Nhưng chúng cứ luôn miệng Chú Nam bảo… nhiều lúc làm tôi phát cáu.

Về thơ, nói thật tôi không có khiếu sáng tác. Tôi rành tiếng Tàu, thơ Đường, điển tích… đến y cũng phải nể, lắm khi phải cậy nhờ. Giảng cho y xong tôi thường bảo: Núi này cao còn có núi khác cao hơn. Huyện ta nhân tài đâu chỉ một. Y cười: “Anh đúng là người tốt – chả đụng chạm đến ai, thấy cái gì an toàn mới làm, tranh thủ dạy đạo đức bất cứ lúc nào. Nhờ những người như anh mà cuộc sống luôn ổn định và… tẻ ngắt. Em không muốn phải tồn tại kiểu ấy”.

- Sao cứ thích rắc rối thế?

- Không muốn phỉnh phờ nhau mãi thôi…

- Cứ lập gia đình là ổn cả, ai cũng thế… - Tôi khuyên.

Cuối cùng đã xảy ra chuyện. Tôi chả biết kể thế nào cho lô-gic. Thôi thì cứ ghi lại từ đầu vậy.

*

*      *

… Y vào ngành giáo dục khá muộn, lúc 29 tuổi, vừa giải ngũ. Những năm tháng mặc đồ lính, y là tay ghi-ta cự phách của đại đội và là một đảng viên trẻ, năng nổ. Tấm bằng tốt nghiệp trung học phổ thông, sự hiểu biết về nhạc lý và khả năng sử dụng các loại nhạc cụ đã giúp y có được chân hợp đồng trong một trường trung học cơ sở: giáo viên nhạc. Công bằng mà nói, nhờ Hoan – người bạn học cũ, tổ trưởng bộ môn toán nhà trường – xin ban giám hiệu, y mới có nghề nghiệp ổn định.

Sự khốc liệt của cuộc chiến mà y đã trải qua trên chiến trường K. khiến y thêm trân trọng cuộc sống, yêu thương học sinh và cả… dè sẻn niềm vui, hạnh phúc riêng tư. Y dồn hết tâm trí vào chuyên môn, học hỏi về phương pháp sư phạm. Mỗi tiết dạy, y cố giúp học sinh cảm nhận vẻ đẹp của nghệ thuật, của cuộc sống và giá trị của hòa bình qua từng giai điệu lời ca và những mẩu chuyện cảm động đời lính. Rồi y được đề bạt chức tổ trưởng chuyên môn. Sau đó y đăng ký học đại học từ xa ngành ngữ văn. Năm 40 tuổi, y tốt nghiệp đại học, lập gia đình, và…. có thơ, nhạc đăng báo Trung ương. Thật là một năm đầy ý nghĩa với y, người-giáo-viên-cựu-chiến-binh.

Vậy là y- nhà giáo, nghệ sĩ tương lai - với tờ báo biếu vừa nhận nâng niu trên tay, bước vào phòng làm việc của ông trưởng phòng giáo dục theo lời “mời miệng” trong tâm trạng vô cùng thoải mái và một thái độ kiêu hãnh quá mức cần thiết. Y gõ cửa. Trời mưa như trút nước. Năm nay lũ lụt dữ đây. Y gõ mạnh tay hơn.

- Mời vào! - Giọng nữ chứ không phải nam.

Chẳng lẽ mình nhầm phòng? Y phân vân, song vẫn đẩy cửa bước vào. Ở bàn làm việc là một phụ nữ đeo kính. Cặp kính trắng gọng đen làm lúc đầu y không nhận ra người đồng nghiệp trẻ vừa được thăng chức phó trưởng phòng năm ngoái.

- Chào… c…ô! - Suýt nữa y lỡ lời nên chữ “cô” nghe không được rõ lắm. - Ông trưởng phòng…

- Đồng chí trưởng phòng xuống các trường giải quyết một số công chuyện - Bà phó phòng nói, mắt vẫn không rời tập giấy trên bàn. - Tôi sẽ làm việc với anh.

Tuần trước, y tặng cho ông trưởng phòng cuốn tạp chí đăng các sáng tác mới của y. Thế là không phải chuyện nhạc và thơ rồi. Cảm giác lâng lâng trong y bay đâu mất.

Y kéo ghế ngồi. Bà phó phòng ngước nhìn y và… không nói gì. Có lẽ tại trời mưa to. Y gài khuy áo khoác vì thấy hơi lạnh và để lấp thời gian trống.

Mãi rồi buổi làm việc cũng được mở đầu:

- Nói chung thì… - Bà phó phòng nói – Vấn đề chính là… Anh phải biết nhìn trước nhìn sau… bên cạnh đó…

Lời nói chậm nhưng quá nhỏ. Y căng tai mà câu được câu mất, không đoán được nội dung.

Bà phó phòng tiếp tục:

- Phải biết nhận thức rõ hiện tượng và bản chất, cái riêng và cái chung, cái “con” và cái “người” trong mỗi một con người. Ai cũng đã, đang và sẽ mang ơn một ai đó. Và bất cứ ai rồi cũng phạm phải một sai lầm nào đó…

Mưa bớt nặng hạt. Những câu bóng gió khó hiểu của một người áp dụng triết học, vừa được học ở các lớp quản lý, chính trị, không đúng chỗ làm câu chuyện trở nên nặng nề.

Y im lặng, đếm từng hạt mưa nhỏ xuống cửa sổ sau lưng bà phó phòng, cảm thấy buồn. Chiến tranh, sự sống và cái chết, anh hùng và tiểu nhân, vẻ hào nhoáng và sự trống rỗng, cái tốt – xấu, được – mất… tất cả nhòe trong màn mưa thời bình, chỉ còn lại nỗi đau của vết thương trên người y mỗi khi trở trời là hiện hữu.

Ờ thì một kẻ vô ơn. Y tự nguyền rủa mình. Không có bằng sư phạm mà được làm giáo viên dĩ nhiên y phải mang ơn nhiều người: Hoan - người giới thiệu; hiệu trưởng - người đồng ý nhận; phòng giáo dục tiếp nhận hồ sơ, gởi cấp trên xem xét; chủ tịch hội cựu chiến binh ủng hộ nói vào cho y; bí thư đảng ủy xã nhận xét tốt về y…

Người đầu tiên y làm mất lòng là Hoan. Sai lầm thứ nhất của y: quên bẵng sự hiện hữu của nền kinh tế thị trường.

 Được phân công dạy nhạc, làm tổng phụ trách đội, ngoài giờ học chính y cho các em tham gia nhiều loại hình sinh hoạt khác. Điều này ảnh hưởng đến lịch dạy kèm của Hoan. Hoan nhiều lần nhắc riêng y:  Nhiệm vụ chính của học sinh là học tập.

 Y không quan tâm. Năm ấy, y quyết đưa một luồng gió mới vào trường học. Tuần nào cũng có vài buổi “vui như hội”. Hoan buộc lòng phải nói thẳng:

- Ông làm ảnh hưởng đến số lượng học sinh học kèm môn toán của tôi. Ông gián tiếp “giúp” tôi đưa kinh tế gia đình đi xuống. Thật sai lầm cho tôi …

Lúc ấy y mới vỡ lẽ. Thay vì sắp xếp lại hoặc giảm bớt thời gian ngoài giờ, y đưa vấn đề ra trước hội đồng nhà trường bàn bạc: Sinh hoạt hay học kèm? Sự việc được y vô tư  trình bày làm lòi cái đuôi của Hoan ra rõ đến nỗi cả hội đồng ồ lên. Tuy vậy, Thủy - người đang giữ chức hiệu phó chuyên môn, sau này là phó trưởng phòng giáo dục - lên tiếng giảng hòa:

- Không cần phải cứng nhắc, nghiêm trọng thế. Đồng chí Nam cân đối lại thời gian, tránh ảnh hưởng chất lượng học tập của học sinh. Đồng chí Hoan làm đơn, kèm danh sách học sinh và lịch học cụ thể cho nhà trường xét duyệt mới được mở lớp.

Hiệu trưởng phát biểu tán thành ý kiến của hiệu phó và giải tán cuộc họp. Thế là số lượng bạn bè của y giảm hơn một người. Riêng Hoan, mỗi lần tình cờ gặp y là nhìn trật sang bên.

Thủy là người thứ hai, người đưa y về đúng vị trí của mình. Sai lầm thứ hai của y: không hiểu rằng bệnh-thành-tích cũng có “mặt tích cực”.

 Bảy năm từ ngày y từ chiến trường K. trở về, ngành giáo dục đã có những chuyển biến lớn. Người ta đua nhau báo cáo thành tích lên cấp trên. Thành tích học tập của học sinh cũng như thành tích của nhà trường tăng đột biến. Mới năm nào tỷ lệ đỗ tốt nghiệp cấp II của nhà trường là 49%, nay đổi thành 94%. Những thành phần sau hầu hiếm trong trường, đó là: học sinh bị đuổi học, học sinh ở lại lớp, giáo viên xếp loại tay nghề khá. Điểm của học sinh không tồn tại số 0,1,2. Từ điển xếp loại tay nghề giáo viên không tồn tại chữ trung bình, yếu, kém. Y thường khoe với tôi ngôi trường y đang dạy là ngôi trường trong mơ, ở đó toàn trường cùng dìu nhau vào thế kỷ 21. Hội đồng giáo viên “đoàn kết – vị tha”, cùng phấn đấu thực hiện chỉ tiêu trên giao, cùng thong thả dạy, cùng cật lực làm thêm mọi nghề và cái chính là… cùng hưởng lương Nhà nước.

 Y đọc nhiều, chịu khó tìm hiểu nền giáo dục của các nước tiên tiến. Trong hội nghị công nhân viên chức, khi Thủy giao khoán cho từng giáo viên bộ môn và giáo viên chủ nhiệm để thực hiện kế hoạch năm học mới, y nói: “Chúng ta khó mà hoàn thành chỉ tiêu khoán chất lượng cao một cách vô lý thế”.

Thủy phát biểu:

- Chỉ tiêu, kế hoạch là cái giúp chúng ta không ngừng nỗ lực vì sự nghiệp giáo dục, ngày càng hoàn thiện bản thân mình. Điều đơn giản như vậy mà đồng chí Nam cố tình không hiểu, lại còn lôi kéo các đồng chí khác hạ thấp chất lượng học tập của học sinh. Tôi mạnh dạn kết luận: đồng chí Nam thiếu ý thức phấn đấu, tinh thần xây dựng. Tôi đề nghị cách chức tổ trưởng chuyên môn của đồng chí Nam với hai lý do. Một là: đồng chí Nam chưa đạt chuẩn sư phạm. Hai là: chất lượng giảng dạy thua xa các đồng chí khác trong tổ. Xin đồng chí hiệu trưởng cho tôi ít phút để lấy biểu quyết của hội đồng. Ai đồng ý bãi nhiệm đồng chí Nam thì giơ tay.

Một, hai, rồi ba phút trôi qua. Vẫn chỉ có hai cánh tay giơ lên, một của Thủy và một của y.

Y thấy tội cho Thủy, đứng lên gỡ bí:

- Hai vấn đề đồng chí Thủy nêu ra là hiển nhiên, là chuyện giấy trắng mực đen không ai có thể chối cãi được. Vì thế tôi xin thôi chức tổ trưởng để về đúng vị trí của mình. Cảm ơn đồng chí Thủy vì lời góp ý thẳng thắn.

Cả hội đồng vỗ tay. Đồng nghiệp thích y nhưng ít người dám lên tiếng ủng hộ.

Hai năm sau, y tham gia hội thi giáo viên dạy giỏi cấp huyện. Cuối năm cả trường có hai người đạt danh hiệu “giáo viên dạy giỏi cấp cơ sở” là y và Hoan. Đây cũng là thành tích của trường nên ban giám hiệu giao lại cho y chức tổ trưởng. Nghĩ mình còn làm được hơn thế nhiều, y theo lớp đại học sư phạm Văn hệ từ xa.

*

*      *

Không chịu nổi sự im lặng của Nam, bà phó trưởng phòng giáo dục - đào tạo để tập tài liệu xuống bàn, đẩy về phía y:

 - Anh xem đi.

 Y nhìn “cô” phó phòng. Bốn mắt gặp nhau. Cặp kính đã xong việc nằm buồn thiu trên bàn, nhìn lên trần nhà.

- Tôi là người hiểu anh rõ mà. – Thủy nói, cầm tập giấy khổ A4 để lên tay y.

Y nhìn thoáng qua. Thì ra y lầm. “Cộng hòa xã hội chủ nghĩa Việt Nam / Độc lập – Tự Do – Hạnh Phúc / Đơn Tố Cáo”. Y nhẩm từng tờ :1,2,3,4,5,6 . Y lật lại trang đầu.

 - Tố cáo thầy Hoan… – Y nói sau khi đọc gần hết trang.

Y nhìn lên đôi mắt vô cảm của Thủy.

 - Anh cứ tiếp tục! Chừng ấy năm công tác với nhau … – Thủy nói lấp lửng.

Đọc hết trang hai, y lại nói:

 - Không tố cáo tôi.

 Y nhìn kỹ ánh mắt dò xét của Thủy lúc này. Y phần nào hiểu được. Thủy xem đây là cuộc đấu trí. Từ khi bỏ mắt kính ra, Thủy giữ nguyên một tư thế, ngồi khoanh tay phía trước, và… nhìn y.

Y đọc lại những câu bịa đặt, vô lý, đôi chỗ đến ngớ ngẩn, vu oan giá họa Hoan – Hiệu trưởng ngôi trường mà mới năm trước thôi ba người còn chung đơn vị công tác.      
                                                   
*

*     *

Y đấu tranh không khoan nhượng với những hiện tượng tiêu cực trong nhà trường. Lần ấy, y đã chống lại cả ban giám hiệu: Ân – hiệu trưởng, Thủy – phó hiệu trưởng chuyên môn. Hoan – phó hiệu trưởng chuyên trách hoạt động ngoài giờ. Sai lầm thứ ba của y: không xác định được trọng tâm vấn đề.

Trong cuộc họp chi bộ y đã đưa ra tất cả vấn đề nổi cộm trong trường học. Trước đấy, y thấy có nhiều sai phạm của ban giám hiệu nhà trường, song chưa đến mức báo động. Thời điểm y nêu vấn đề là khi sự cấu kết của các thành viên ban giám hiệu, sự thỏa hiệp của một số đảng viên đã rõ nét. Y biết mình sẽ gặp trở ngại lớn trong cuộc họp nhưng y vẫn quyết tâm làm đến cùng trước khi những kẻ cơ hội đó tạo ra những tiền lệ nguy hiểm, làm hoen ố sự vô tư, trong sáng của ngành giáo dục.

Cuộc họp chi bộ sáng chủ nhật hôm ấy gồm tám người: ban giám hiệu ba đảng viên, tổ chuyên môn bốn đảng viên và một đồng chí đảng viên là chủ tịch công đoàn.

Hôm ấy, y nói như chưa bao giờ được nói, như sẽ chẳng bao giờ được nói nữa. Không ai cản được y, kể cả chị chủ tịch công đoàn – người, sau này y kể lại, duy nhất đứng về phía y.

Trước hết y nói về sự mất dân chủ trong trường học, hiện tượng trù dập giáo viên của ban giám hiệu. Với Ân, y kết tội chia rẽ giáo viên để dễ điều khiển. Nói chuyện với người này Ân phàn nàn về người kia và ngược lại. Mục đích Ân muốn đạt được là sự mất đoàn kết, thiếu tin tưởng lẫn nhau trong đội ngũ giáo viên, qua đó Ân dễ dàng nhận ra người “cứng đầu” hay đang có “âm mưu” hại mình và mọi chi tiết “xấu”dù là nhỏ nhất của từng người. Nhưng quan trọng hơn cả là ai cũng tưởng mình được hiệu trưởng tin cậy, cảm thông, chia sẻ…

 Tiền thừa giờ được Ân đặc biệt quan tâm. Trên dưới 100 triệu mỗi năm mà chỉ có 15 người trong trường được hưởng. Dĩ nhiên Ân được hưởng nhiều nhất. Là sếp, Ân tự phân cho mình nhiều tiết dạy rồi cứ việc nhờ những giáo-viên-tập-sự-dễ-bảo dạy thay. Vừa là cướp công, vừa che mắt thiên hạ một cách tinh vi. Những người được Ân nhờ thì ngậm bồ hòn làm ngọt. Một phần số tiền tăng tiết lại chảy vào túi của Ân bằng hình thức hối lộ.

Hoan đứng phắt dậy, ngoáy mũi, cười khẩy:

- Có làm có hưởng, làm nhiều hưởng nhiều, làm ít hưởng ít, không làm không hưởng.

 Y nói :

- Hãy để mọi người đều “có làm” thế mới công bằng. Anh không biết tiết-tăng-giờ giá trị hơn tiết-chuẩn sao?

 Rồi y phê phán lối cư xử trịch thượng, kẻ cả, thiếu tính sư phạm của ban giám hiệu. Không khí trong trường học thật nặng nề. Giáo viên có cảm giác của người làm thuê cho ban giám hiệu. Y lấy Thủy ra làm dẫn chứng. Thủy thường chắp tay sau lưng đi từng phòng học không phải để xem học sinh học tập thế nào mà để xem giáo viên có… ngồi không. Lời phát biểu của Thủy lúc nào cũng chung chung, mang tính đe dọa hoặc tối nghĩa: Một số đồng chí thiếu tinh thần trách nhiệm, Ai làm sai sẽ chịu hoàn toàn trách nhiệm, Qua sự việc trên cho chúng tôi thấy…

- Cô Thủy quan tâm đến thành tích hơn là quan tâm đến học sinh. - Y nói.

Kỳ nghỉ hè năm ấy, có hai học sinh được ba mẹ dẫn tới xin ở lại lớp mà không được; có bảy trường hợp phụ huynh đến trả lại giấy khen vì nhà trường công nhận “học sinh giỏi lớp 9” nhưng qua kỳ thi tốt nghiệp điểm thi của các em quá kém, chỉ đủ để học cấp III bán công.

 - Vì sự nghiệp chung mà… – Thủy nói.

Y thẳng thừng:

- Ngụy biện.

- Thế còn những thành tích của tôi?

- Đúng thành tích là của cô và của cả ban giám hiệu nữa. Học sinh là người bị hại vì những cái phù phiếm mà các đồng chí gọi “thành tích” đó – Giọng y khá gay gắt - Cô giải thích thế nào về việc tự tiện đuổi khéo học sinh mà cô cho là tiếp thu chậm – cá biệt ra ngoài chơi mỗi khi cấp trên về dự giờ, kiểm tra? Cô sẵn sàng vi phạm quyền được học hành của trẻ em vì uy tín trường-lớp. Thật đáng nể!

Hoan bị phê phán nặng nhất.

Y nói:

- Với anh, tôi lấy làm tiếc tại sao anh không chọn nghề buôn.

Hoan nghe xong đập bàn, khạc nhổ cho trơn họng, rồi gân cổ:

- Tôi sẽ kiện… cái tội xúc phạm nhân phẩm!

Y nói:

- Ngành giáo dục mà có những giáo viên dạy giỏi như anh thì lợi ích cũng chẳng là bao. Đơn cử một điều: Đất nước ta hiện nay có được mấy người vừa là đảng viên, vừa là giáo viên giỏi, vừa là cán bộ quản lý chỉ đặc biệt dạy kèm cho con nhà giàu? Kiến thức thì không truyền thụ hết trong giờ học chính mà chỉ để dành như bửu-bối-của-lớp-học-kèm?!...Theo các đồng chí, đây là trường học hay siêu thị dành cho tầng lớp thượng lưu? Phần đông học sinh nghèo khó, vậy mà nhà trường ép nộp tiền học tăng tiết nhằm hợp thức hóa việc dạy thêm – học thêm tại trường. Phần trường, trường ép, phần thầy, mạnh ai nấy ép, các em chịu sao thấu ?!...

Y quay sang các tổ trưởng chuyên môn, nói tiếp:

- Chính sự im lặng, sợ sệt của các đồng chí đã tạo cảm giác quyền lực, gián tiếp làm hư cán bộ quản lý. Nếu không có nịnh bợ, sợ hãi thì không có khái niệm quyền lực. Tôi phê phán tư tưởng ôn hòa, ba phải của các đồng chí.

Ân là người phát biểu sau cùng:

- Tôi đề nghị thư ký ghi ý kiến của tôi: Đồng chí Nam có lòng đố kị, tự ty thái quá, ganh ghét với thành công của người khác, thiển cận khi nhìn nhận vấn đề, phát biểu thiếu căn cứ, xúc phạm danh dự người đảng viên, giáo viên. Tôi đề nghị gởi biên bản cuộc họp lên cấp trên.

Y bảo:

- Vậy thì tôi sẽ gởi đơn tố cáo đi kèm…

- …

- … Mà đã hết đâu. – Y tiếp – Khoản  thu chi trong hai chính sách thể hiện tính nhân đạo là học phí đối với học sinh nghèo và kinh phí cấp cho công tác xóa mù chữ, phổ cập giáo dục bị mấy người tính toán, tận dụng tối đa…

Hoan lại đập bàn. Lần này thì chén nước trà không chịu nổi lực tác dụng quá lớn từ bàn tay hộ pháp của Hoan tác động đột ngột lên cái mặt bàn vô tội nẩy lên, văng nước ra tứ phía, đáp xuống, trút những giọt còn lại lên cuốn sổ công tác của y. Nhưng hình như y không dừng lại được nữa:

- … Sau mỗi đợt quyết toán xong học phí, cuối năm học, ban giám hiệu lại vận động các giáo viên chủ nhiệm mở chiến dịch thu vét. Tiền mà những gia đình học sinh nghèo thắt lưng buộc bụng nộp sau, do bị thúc ép, suốt mấy năm nay nằm ở đâu khi mà nhà trường đã đưa vào danh sách thất thu? Và nữa: Tiền dạy phổ cập mà cũng được cân nhắc kỹ lưỡng xem người nào được hưởng, người nào không, báo cáo láo cách nào để trên duyệt chi nhiều nhất thì trong trắng đến thế là cùng. Những vị lãnh đạo tâm huyết, chánh trực nếu nghe được chuyện này thì á khẩu ngay…

Mọi người bỏ về, chỉ còn y và chị chủ tịch công đoàn ngồi ôm đầu. Y dừng lời, đứng chôn chân tại cái bàn có cuốn sổ dính đầy nước trà.

Suốt một tuần, y viết đơn tố cáo. Y trực tiếp gởi tận tay ông trưởng phòng giáo dục huyện. Y đã phạm sai lầm lớn: có quá nhiều vấn đề và quá nhiều người liên quan.

 Thời gian sau đó, đồng nghiệp sợ liên lụy, ngày càng ít người dám tiếp xúc, nói chuyện với y. Rồi phòng giáo dục mời y lên hòa giải chuyện không lớn lắm đó.

 Hết năm học, y nhận được giấy chuyển công tác về một trường miền núi trong huyện. Hai tháng sau, theo cơ cấu của trên, xét năng lực, thành tích công tác và sự đóng góp cho huyện nhà, Ân nhận chức giám đốc trung tâm hướng nghiệp của huyện; Thủy được điều động về phòng giáo dục và Hoan lên thay Ân.

 Như A.Q, y tự nhủ: còn nhiều chiến trường không tiếng súng, nhiều mặt trận gian khổ cần đến những người lính dũng cảm như mình.                                                               

*

*      *

- Tôi vẫn chưa hiểu… – Y nói với Thủy khi đã đọc xong lá đơn.

- Những việc thế này anh rành lắm mà…

Đoán được Thủy đang ám chỉ điều gì, y lật  trang cuối xem lại.

- Đơn thư nặc danh kiện thầy Hoan. – Y nói giọng phẫn nộ – Tôi mà hèn thế ư?

- Anh về nghiên cứu lại. Tiến tới hay rút lui phụ thuộc ở anh. Nghị quyết của tôi là thế.                 

Theo kinh nghiệm, cứ nghe đến chữ nghị quyết mà Thủy ưa dùng một cách tùy tiện, thì có nói gì thêm cũng vô ích. Y miễn cưỡng xếp tập giấy bỏ vào túi áo và đứng lên.

Thủy tiễn y ra cửa thì gặp Hoan. Hoan tỏ vẻ xởi lởi:

- Chào cô Thủy, thầy Nam… Lâu ngày quá… Đồng chí trưởng phòng hôm nay có đây không, cô Thủy?

- Chào anh. Thầy xuống cơ sở rồi.

Hoan bắt tay y rồi xin phép đi trước.

Thủy bảo y:

- Người ta như thế còn anh …

- … làm tổng phụ trách đội – Y chêm vào rồi cáo lui.

*

*      *

Từ chiều tới tối, vợ chồng y đọc không biết bao nhiêu lần lá đơn ấy.

- Em thấy sao? – Y hỏi.

- Người tố cáo bị điên anh ạ!  Lá đơn toàn chuyện vu vơ.

Vợ đã lên giường còn y vẫn ngồi hút thuốc và suy ngẫm…

Rồi y nhận được tin nhắn. Nội dung:

Nực cười châu chấu đá xe. Tưởng rằng chấu ngã, ai dè xe…  nghiêng xuống đè nát con châu chấu.          

- Tìm ra rồi! – Y nhảy cẫng lên, reo hò như Ác-si-mét thuở nào.

- Ai … kiện? – Vợ y hỏi, giọng ngái ngủ.

- Em còn nhớ phép ngụy biện bỏ thuốc độc vào giếng không ?

- …

Ủa, sao lại mừng? Y giật mình tự hỏi, thừ người ra bó gối chấm com…

Mới năm ngoái gần một nửa số học sinh trong trường đến tặng hoa cho y khi y chuyển đi. Và đã có rất nhiều giọt nước mắt lưu luyến. Chẳng lẽ năm học tới cũng là hoa và lệ sao?

Đêm ấy, Nam ngủ không ngon giấc. Gần sáng, y thiếp đi. Trong mơ, y thấy mình mặc quân phục, nằm giữa đám cỏ hoa…

*

*     *

Tôi viết thư kể chuyện này cho bạn thân của tôi ở Đông Hà, học giả Đinh Văn Nhân, người nổi tiếng là kiệm lời. Quả không sai. Đúng một tuần tôi nhận được lá-thư-hai-chữ: thì thế. Còn Nam đã nghỉ hè theo đúng nghĩa đen lẫn nghĩa bóng. Chiều nào đi làm về tôi cũng thấy y ngồi ngoài hè, nhìn lên ngọn cây bàng trước ngõ và… ngáp. Những lúc như thế tôi thấy anh có chút dịu dàng của phụ nữ, chút hồn nhiên của trẻ thơ. Thằng nhỏ nhà tôi thì thầm, chú Nam nghe chim hót trên lùm cây; bé lớn cãi, chú bảo là tìm xem còn giọt nắng nào ngủ quên trên vòm lá không. Tôi vừa buồn cười vừa nghe xót xót trong lòng.

Cứ ngỡ mọi chuyện sẽ qua. Không ngờ sau đợt công tác miền Đông về, tôi như bị sét đánh, chả thể tin vào tai mình khi nghe vợ bảo: Anh ấy ngáp mãi rồi đi…

Bác sĩ bảo đột quỵ.

Hàng xóm bảo bị trúng gió.

Kẻ xấu bụng bảo quả báo.

Người-ba-phải bảo bớt rắc rối.

Đồ-độc-mồm bảo đáng đời.

Đồng nghiệp của tôi bảo bị thần kinh, do vết thương tái phát.

Tôi muốn hét to: Thằng gàn dở hơi; nhưng cổ họng nghèn nghẹn… Nhớ thời gian nhận giấy chuyển công tác y đâm ra ưa lý sự. Y bảo ngáp và ngủ giúp ta hạnh phúc viên mãn. Tôi bảo ngây ngây mà y không nghe. Tính y vẫn vậy: cực đoan-bảo thủ.

Tôi điện thoại báo cho anh-bạn-học-giả tin buồn. Lần này câu trả lời thêm được một chữ: không thể thế.

Tôi cũng muốn nghĩ như vậy.

Ước gì chỉ là cơn mơ!
1. Có lẽ bạn không tin tôi đâu, trước khi lên đường đi bộ đội, tôi từng là một chàng trai đò dọc, chở thuyền đi trên sông Mã. Nói là chở thuyền đi trên sông Mã nhưng thật ra tôi còn đi khắp cả những con sông quê nhà tỉnh Thanh mình nữa đấy. Bạn sẽ hỏi tôi, làm chân sào, vậy thì có gì thú vị, có nhận ra sự thao thiết của những dòng sông đi qua không” Thì chính điều này mà tôi mới viết ra câu chuyện, in đậm trong tâm khảm một đời người.

 
 Minh họa: Lê Trí Dũng
Tôi không tài nào quên nổi. Mỗi khi có tiếng khuấy nước. Tiếng nước làm tôi lại khấp khoải như chờ có ai đó gọi từ khoảng sông xa vời:

- Đò ơi!... Cho đi... với!

Tiếng gọi cũng có thể của người tàng tàng tuổi “đàn bà”... Có thể là của cô con gái nhà ai đi cho kịp về đón tết, lúc thời gian giao mùa. Ngày ấy chưa có giao thông thuận tiện, đường xá chưa thông thiên như bây giờ.

Tôi đi theo chân sào, nghĩ cho cùng cũng do được nhiều đêm ngủ cùng ông Tơ chủ thuyền ở cái bến Hàm Rồng này đấy. Không hiểu sao, tôi không nhớ được lý do nào, chuyến chở nước mắm của ông đậu lại bến nhà tôi, bên dòng sông Chu. Tôi đoán già đoán non mà không tìm ra lý do chắc chắn. Có thể ông mê mẩn bà Thình ở trạm nước mắm này rồi. Cũng có thể do cuộc hò của người chủ thuyền và bọn chân sào đò dọc một đêm nào buông lời trêu ghẹo bạn tình:

Tiếng chày giã gạo thậm thình
Xuống đò nhóm lửa kẻo mình đang mong

Tôi nghe tiếng cười khoen khoén của bà Thình. Tiếng đấm vào lưng ông Tơ có dễ vang ra tận bến sông. Có lẽ không đau hay sao mà kéo theo cơ man nào là tiếng cười. Nghe tiếng cười tóe ra là người già có thể chép miệng cho là quân dửng mỡ. Nhưng mà tôi đang tuổi lớn, nghe tiếng cười đó như có sự lạ kỳ lay chuyển trong mạch máu, con tim và rung động đến từng hơi thở.

Thế là ông Tơ, do cái vui ở đời mà nhận một tháng đôi lần chở cá mắm đến trạm cho bà Thình. Không hiểu do ông Tơ có sáng kiến hay sao mà từ đó không ai gọi là bà Thình nữa mà chuyển sang là cô Thình, có khách sáo một tí nhưng nhìn chung thì gọi bằng cô nó trẻ. Về tôi, tốt nghiệp cấp hai rồi, thi mãi không vào được trường huyện nên đành ở nhà chờ một công việc gì, người ta gọi mà đi làm. Nhìn thấy da của tôi sạm nắng ông Tơ cười:

- Vợ con gì chưa?

- Bác hỏi cháu ạ!

- Cái thằng, tao thế này mà mày gọi bằng bác chả làm cho cô Thình tưởng già hả? - Ông cười rất duyên. Không có vẻ gì là giận cả. Biết tôi chưa vợ con, ông bảo may đấy. Mặc dù đứng trước mặt cô Thình, ông vẫn buông ra câu hò.

...
Đò dọc... anh trải... mấy... chiếu ngang
Anh thời... nằm... í... i... giữa.... chứ... hai nàng hai bên
Một cô yếm trắng răng đen
Má lúm đồng tiền xinh thật là xinh
...

 
 Ảnh: naturewallpapers
Tôi nhìn thấy nét mặt của cô Thình bợt bạt vì cảm động. Tiếng hò của ông Tơ thực tình không hay lắm đâu, nhưng lại có sức cám dỗ một cách lạ thường. Không buồn đau, rầu rĩ mà như thể mời chào người ta lên trên con đò đang chuẩn bị nhổ sào đến nơi nào đấy. Hai bàn tay to bè bè của ông lúc đặt lên bả vai tôi hỏi có vợ con chưa thì rất thô, nhưng lúc này làm động tác chèo thì thật tuyệt.

Tôi cứ tưởng như đất dưới chân đang chuyển động. Tôi từng được nghe hò của đám đắp đê cái năm khôi phục đập Baza, nhưng so với cách phô diễn động tác lao động của ông thì còn xa mới đạt tới trình độ, vừa có cái khỏe của người lao động mà lại có được thần thái của người nghệ sĩ. Cô Thình lo ra mặt, chỉ sợ ông Tơ nhổ neo thì nồi thịt chó của cô ai ăn. Có bao kẻ nhìn vào bà góa mà phát rỏ dãi nhưng cô Thình đâu có thèm. Lại ưng cái con người ăn sóng nói gió này.

Nước da nhuộm đen không còn một chỗ nào hở. Bộ tóc rễ tre ấy mà cụi vào cổ, thì chửa biết chừng. Cái miệng, không biết nhuộm răng bằng thuốc của nhà ai mà đen nhanh nhánh. Lại được nước cốt trầu thuốc khảm lên, có cần đánh răng gì đâu, thở ra cái là mùi trầu thuốc thơm, có dễ ở xa mươi bước chân cũng thấy thoảng trong gió. Chắc trong đêm người ta vẫn nhận ra. Rồi không hiểu đua đòi với ai mà cấy vào khóe miệng một chiếc răng vàng lấp lánh. Bởi vậy mà có tên là Tơ Chói.

Tơ Chói đi đến đâu là nổi đình nổi đám ra đến đó. Cô Thình như bị trời trồng đứng không rê nổi bàn chân. Mặt mũi đỏ lựng lên vì động tác múa máy của ông Tơ. Hai tay ông khỏa nước. Bàn chân ông làm động tác tóe nước một cách thần kỳ. Khẽ nhón đôi chân ông nhảy lên con thuyền. Tất cả chỉ là động tác như người trong mơ. Nhưng vì thế mà rõ ràng như là có thật. Sau này cô Thình thú nhận, cô cũng hết hồn tưởng là thuyền nhổ neo rời bến. Chiều qua lúc khuân nước mắm lên trạm ông ấy nói thầm vào tai cô: Nhớ món thịt chó lắm!

- Chết đàng nhà anh, ai cho nói đùa cái kiểu mất nết?

- Thì tôi muốn nhờ cô kiếm ở đâu một chân chó để xả hơi thật mà!

- Mấy người miệng vạc ống dầu thế kia một chân thấm tháp vào đâu?

- Thì em kiếm cho anh nửa con cầy tơ... ba bát nhớ!

Cái bữa đánh chén thịt chó ở bờ sông hôm đó tôi cũng được cô Thình mời. Thật bất ngờ, cô bảo với ông Tơ, tôi gửi thằng cháu đi cùng anh đấy.

Ông Tơ mắt lờ đờ, nói qua hơi rượu:

- Được! Nhận. Thàng này.

Ông khoa tay múa chân làm động tác cậy sào. Bàn chân ông dậm xuống nền đất nện mà lại vang lên cái kiểu thậm thịch của dân chèo đò khi hát đồng thanh trên sông.

... O buôn chi mà có bị có cân
O xuống đò dọc một thân một mình
Bàn chân... Ơi bàn chân xinh
Tìm bàn chân tính tình tinh tình tình...

Mấy tay chèo thuyền cho ông, nãy giờ đang tì tì thịt chó bỗng dưng như động rồ xếp hàng hai bên chiếu rượu vỗ tay đôm đốp đệm cho ông Tơ. Rồi bỗng dưng từ khoang ngực trần ấy buông ra những lời làm cho tôi không nén được lòng khát khao xin được đi theo con đò Tơ Chói.

Thương ai... đứng bụi nấp bờ
Sáng trông đò ngược, tối chờ thuyền xuôi
Thuyền ngược anh bỏ đò xuôi
Khúc sông bỏ vắng để người sầu riêng
Sầu riêng cơm chẳng muốn ăn
Đã bưng lấy bát lại dằn xuống mâm
Không ăn thì đói thì gầy
Ăn vô nước mắt chảy đầy bát cơm

Bữa chén thịt chó gói lá đù mại ấy không ngờ cô Thình cao hứng đã gửi tôi cho ông Tơ Chói. Cô nói với giọng tha thiết, làm tôi khoái tỉ mà ra đi cùng con đò dọc, như là số phận của mình.

- Thằng Tỏa, cháu của em, nó cũng có máu giang hồ lắm. Anh cho nó theo biết đâu sẽ học được anh mà làm nên chuyện.

Cô Thình cười gắp vào bát của ông Tơ một cục thịt nạc còn tỏa ra mùi thơm của lá đù mại. Cô cười thật hay hớm, dịu dàng:
- Nhưng chỉ mong đừng bắt chước cái tài “dại gái” của anh.

Nghe đến đó ông Tơ cười lên khằng khặc, hai tay múa may như người cầm sào cạy vào mạn thuyền.
          Chiếu hoa trải xuống mà ngồi
          Em ơi xích lại cùng tôi kẻo buồn.

2.
Tôi rời bến quê ra đi cùng ông Tơ vào chiều mùa hè năm ấy. Khi hai bên bờ bãi của con sông Chu xanh ngút ngát ngàn dâu. Những mái nhà tranh đạm bạc núp vào lùm xanh đang thở ra những vệt khói. Tiếng đập nứa đâu đó để đan thuyền vỗ vào bụng dòng sông với âm hưởng thoang thoảng trong cái nóng nôi như muốn phơi nỏ con người. Tôi không thấy buồn. Thật đấy. Mà lòng ngực tràn ra cùng không gian lồng lộng của khoang trời. Cái cảm tưởng trời đất lồng lộng, rộng thoáng này giúp tôi quen với khó nhọc làm chân đò dọc. Nhưng điều quan trọng là tôi được tâm hồn nghệ sĩ của ông Tơ truyền lan sang tự lúc nào. Nhờ sự truyền cảm ấy mà sự bồng bột không lường trước nỗi vất vả do cuộc sống phóng khoáng của người trai chống đò dọc cũng mau chóng thấm tháp trong tôi theo ngày tháng. Những vết chai sần trên vai gặp lúc thuyền mắc cạn cũng lành trên da thịt một cách mau chóng. Đôi bàn tay chưa quen với việc kéo thuyền, lớp da bị phồng rộp, bóc ra lại bị cái rét buốt thấu xương. Quan trọng hơn cả là xã hội trong con đò dọc.

 
  Ảnh: naturewallpapers
Tôi nói thế không ngoa một tí nào. Một khách nào đó, gánh gồng xuống đò đi Kim Tân. Người ta dễ có lòng tin hơn một cô nào đó lộn chồng, từ thị xã Thanh Hóa ra bến Hàm Rồng chờ đò. Một chiếc nón vành đã rách hoặc cạp lại bằng những đường may vụng về. Cái tay nải bằng vải nhuộm nâu đã ngả màu, cặp mắt nhìn thảng thốt. Tôi không tài nào dám tin vào cái vẻ bề ngoài đó. Mấy chàng chân sào trẻ tuổi nhung nhúc như tôi thì cười ra vẻ là từng trải, ta đây. Vẻ đùa cợt nơi ông Tơ biến đi đâu mất, bộ mặt đen đúa tạo cho người ta một niềm tin. Niềm tin cậy ấy trong ánh mắt của ông lan sang người khách có thân phận tồi tàn kia.

- Nhà cô... Về đâu... có một mình?

- Em về Đa Nê... Đan Nẫm...

- À! Tôi hiểu rồi. Cô sẽ lên ở bến Kiểu.

Nói rồi ông Tơ vào trong khoang ném cho cô gái mấy mớ rau còn nghều ngào chân, rễ dính đất do mấy bà bạn hò của ông Tơ qua bãi làng Giàng quẳng lên: - Cô giúp cho mấy thằng đực rựa kia nhặt ít rau này. Tay Hải, được bọn trên đò đặt cho là “Hải máy ảnh”, dáng thấp lùn, bàn tay nổi bắp cuồn cuộn. Cái miệng dẻo, có lẽ cả con người của Hải đáng giá nhất là cái miệng. Hắn vội tuôn ra những lời, không ra vẻ trêu chọc, mà làm cho cô gái có vẻ buồn rầu không bị lạ lẫm trước sự tiếp thị dân dã.

          Cây cải là cây cải ngồng
          Cô em... anh hỏi có chồng hay chưa?

Một tay cao lớn, nước da trắng, nét mặt mới nhìn thì giỏi trai nhưng nhìn lâu không dấu nổi cái thồn thộn do sự quềnh quàng, thô vụng. Nhưng cất tiếng làm bè với Hải có vẻ rất hợp cạ.

          Chồng thì em có... vừa bưa...
          Em lên đò dọc nên chưa có chồng

Có phải một công việc khác sẽ rơi vào thô thiển, bợm cựa nhưng với dân chèo đò, động tác thị phạm và cái miệng có vẻ đĩ thõa đó lại tạo ra không khí chan hòa một cách dễ chịu.

Một gã cũng choai choai tuổi tôi, không biết vớ đâu được chiếc lược sừng, nhấp nước sông cho ướt đầu rẽ một đường ngôi giữa đầu. Răng lược làm lộ làn da trắng nhỡn. Lôi ra cái gương bé tí, nhe răng ra như trẻ con làm xấu, tay không ngớt cọ miếng cau lia lịa. Chính cái động tác này làm cho cô khách lúc mới xuống đò mặt rầu rĩ, không nhịn được cười. Tôi cam đoan cô cười vì động tác của tay Thơ đó.

Những động tác của mấy chú “gà trống choai” và “con bò lạc” không qua được cặp mắt ông Tơ. Ông ngó lên bờ, hỏi ai không biết:

- Mẹ cái con mụ Chiếng, bảo mau mau cái chân lên bây giờ vẫn mất mặt.

Ông Tơ chưa dứt lời thì một bà béo, nhưng lại quấn chung quanh người hàng hóa cồng kềnh làm cho cái béo như được nhân lên gấp bội. Bà Chiếng cười khe khé:

- Ông anh nói xấu em cái chi đớ? - Rồi với giọng phân bua với đám chân sào - Chị cũng phải tranh thủ đưa thêm một số hàng nữa. Chở trên thuyền chứ có phải xe cộ chi bảo cồng kềnh làm cản trở giao thông.

- Bà không biết đấy thôi. Mấy thùng nước mắm của bà nó nặng chìm. Chứ mấy cái mớ này chỉ tổ cho gió nó cản lại - Ông Tơ nhìn bà Chiếng bồ hôi mồ kê, như cái nấm không nhịn được cười. Quái chết, không biết cái ngữ nào nó ôm cho xuể bà béo này.

- Đừng hòng nhá... cái lão Tơ kia! Mùa đông thì sưởi ấm, mùa hè thì làm lạnh, không cho ai đâm đầu vào đâu.

Từ trong khoang chui ra một cái đầu tóc bù xù. Ngáp một cái, hỏi ông Tơ:

- Cái của nợ... ông mới nhặt được ở đâu thế này?

Anh ta hất đầu về phía cô gái đang ngồi nhặt rau. Bà Chiếng lại tưởng chạm tới mình vội chu chéo lên:

- Đừng làm bộ cho chị nhờ. Dẫu là cán bộ cán bị đi chăng nữa cũng không được nói cái giọng khinh người ấy!

- Ơ cái bà này tôi nói chi bà? - Gã trai mặc bộ đồ công nhân cãi lại.

Ông Tơ không để cho miệng của đám khách tạp pí lù kịp gây chiến, vội vàng dập gót chân vào mạn thuyền như ra hiệu lệnh. Tức thì mấy tay phu đò vội vàng kéo neo đẩy phăng con đò ra ngoài bến sông, làm cho bà Chiếng, cái tay công nhân trạm thủy nông Sông Mã, cô gái ngồi nhặt rau bị cuộn trong sự rùng mình của con thuyền. Sự việc không kịp ới lên một tiếng tức thì tiếng dậm của mái chèo vào trong khoang buông ra.

          Dô khoan... Thuyền tôi...
          Dô khoan!...
          Ván táu!!!
          Dô khoan
          Sạp lim!!!
          Dô khoan!!!
          Đôi mạn Săng lẻ...
          Dô khoan!!!
          Lại chứ... Khắc...
          Dô khoan!!!
          Đôi chim... là chim Phượng Hoàng...

 
 Ảnh: naturewallpapers
Những bộ mặt vừa khó đăm đăm, cái miệng đanh đá muốn tuôn ra những lời khó chịu hay độc địa chửi rủa... chưa kịp buông ra những lời nặc nô của trần gian khốn khó này, nín lặng nghe những lời thô mộc buông ra từ bộ mặt như được đẽo bằng đất đá. Vừa lúc nãy như thể căm thù một ai đó, bây giờ giãn ra bởi sự mát lành của sông nước, của câu hò mệt nhọc nhưng đầy khoáng hoạt, cao ngạo.

Thằng Thơ mới vừa làm dáng chải đường ngôi vào giữa đầu, mong có dịp khoe với mấy cô em ra bến sông gánh nước. Nhưng vào việc quên khuấy đi cái đường ngôi cầu kỳ. Bộ mặt nghiêm trang như vào một cuộc tế lạy thần sông, câu chuyện mà hắn được nghe ông Tơ kể một đêm giông bão con thuyền quay tít trong những con sóng trào.

Tay cao lớn có cái tên là Bìm thì hai chân lùi và tiến dậm vào mạn thuyền như một lực sĩ của câu chuyện thần thoại nào mà người ta sắp kể cho mọi người. Cái miệng có lẽ lúc thường rất vô duyên nhưng lúc này lại rất hợp với cảnh khẩn trương cho con thuyền băng ra ngoài xa, khi ánh hoàng hôn đang nhuốm màu xuống dòng sông thăm thẳm cuộn cùng tiếng hò khoan...

Tay Hải cặp môi chảu ra vậy mà khi nghe hiệu lệnh của ông Tơ đã nhảy phắt vào vị trí. Không còn cái ngơ ngác của mệt mỏi vì đêm qua vào xóm Giàng đi tìm người thân. Đôi mắt không còn nháy như người chớp ảnh.

Hôm rồi, ông Tơ chưa kiếm được người nên kiêm luôn tay chèo cặp đôi với Hải, nay có tôi vừa được bổ sung, ông đứng trên con thuyền chạm đầu rồng gõ vào mạn ra lệnh. Thật sự ông đang cai quản một dàn đồng ca, có lẽ tôi chưa được chứng kiến bao giờ. Sau một phút há hốc mồm ra vì sự lạ, tôi bị bàn tay hộ pháp của Bìm lái theo nhịp hát của những tay chèo. Tôi bị cái cảm xúc mãnh liệt cuốn đi mà nhập vào điệu “dô khoan”, những giọng trầm đục, vang lên. Nhưng lạ thật, tôi nghe rõ có cả giọng con gái trong và cao, vút lên. Có đánh chết tôi cũng không công nhận đó là tiếng của bà Chiếng, chua lòm.

Sau này tôi mới nhận ra cái đau nơi mông đít của mình. Thì ra cái tay Bìm cao to không chịu được cái giọng khê nồng phá đám của tôi nên thích vào mông tôi cho bớt đi sự quá chớn, mà tôi có chịu đâu. Thú thật tôi được cuốn theo dàn đồng ca bất tận tỏa ra hai triền sông.

Mai này, mỗi khi gặp chuyện khổ đau ở đời, tôi mới hiểu. Những người chèo đò dọc trên sông này, họ hò khoan là một nhu cầu, là cách tốt nhất lấy hơi thở đều đặn và tìm thấy trong câu hò ấy là sự thiêng liêng cho những lần vượt thác, xuống ghềnh.

3.
Một năm sau, tôi quen với con sào tì vào xương quai xanh mỗi khi con thuyền mắc cạn. Bàn tay tôi đã thay những lớp da non và mọc lên những lớp da sần sùi. Những đêm trăng không ngủ được, ông Tơ đánh thức cả lũ phu thuyền dậy chèo vượt đêm. Tiếng dậm gót chân vào mạn thuyền đã đánh thức làng mạc ven sông. Chắc có ai đó vì lý gì đó mà trái tim chớm buồn, giật mình thức dậy nghe được tiếng bắt nhịp của ông Tơ gửi vào lòng đêm sâu thăm thẳm:
          Chắp tay vái lại con sào
          Vực sâu đã biết, thác cao đã tường

Hoặc lời gửi theo màn đêm cho ai không biết, có cảm thông với nỗi vất vả khó nhọc của người chở đò dọc:
          Cá lên khỏi nước cá khô
          Trai đò sắng ngược còn mô khổ bằng
          Thuyền ngược anh bỏ sào xuôi
          Em đừng lo lắng cho người kém xinh

Tôi căng lồng ngực lên cùng câu hò trong đêm, lòng khao khát gửi đến tai người yêu dấu mà mình có dịp gặp trên con đò này.

Một buổi chiều. Trên bến Vạn Hà này, mọi lần thuyền chúng tôi không dừng lại bởi đi có được bao lăm kể xuất phát từ Hàm Rồng. Ông Tơ mặt đăm đăm, cầm lấy tay nải của tôi. Trong đó chứa vài bộ đồ nhằm nhập. Ông đưa cho tôi một gói tiền trong chiếc khăn tay mùi xoa. Lạ quá. Chiếc khăn này tôi gửi một bài thơ, viết theo lối của giọng hò Sông Mã, tặng cho con gái của ông cơ mà:
          Bến ơi có nhớ thuyền không?
          Thuyền rời bến cũ mênh mông nỗi buồn

Ông Tơ nói giọng không tỏ ra buồn hay vui:

- Ta trả công cho mi, trừ cơm ăn đi rồi đó! Đi đi... Chàng trai Sông Mã. Đi... đi!

Tôi hiểu rồi. Cái tội của tôi là từ dạo cuối năm ngoái. Cô con gái của ông Tơ vừa tốt nghiệp sư phạm cấp hai, trong lúc chờ phân công đã xuống thuyền lo cơm nước hộ bố. Thấy tôi và Hồng nói chuyện tự nhiên, “Hải nháy” đã đe dọa: - Cái đồ đũa mốc lại đòi chọc mâm son. Hồng nghe được cười, kệ anh ấy. Anh vận câu hò được đấy. Kể ra bỏ công mà đi theo ông già nhà em, biết đâu anh sáng tác được cả bài hát ấy chứ!


  Ảnh: naturewallpapers
- Ông Tơ cũng sáng tác được sao? - Tôi ngạc nhiên hỏi lại Hồng.

- Ông chỉ giỏi vận theo kiểu dân gian thôi. Mà anh cũng chép cả bài hát còn gì?

- Đâu có... Tôi xấu hổ trả lời. Chắc Hồng ngó vào cuốn sổ tay của tôi ghi bài hát. Và cái sự việc xảy ra thật đáng xấu hổ. Cái cuốn sổ tay của tôi không ngờ là hộp thư đối đáp của tôi và Hồng. Không hiểu tay Bìm, hay Thơ đã mách lối mà ông Tơ lại xem được những câu nhăng nhít của tôi và Hồng gửi cho nhau. Cả cái chuyện tôi vay tiền thằng Bìm ra cửa hàng bách hóa Hàm Rồng mua cái khăn mùi xoa tặng Hồng nữa. Chả trách ông Tơ giận không hò có dễ đến một tuần.

- Làm trai... Đừng vì một đứa con gái mà vùi đầu vào váy không ngóc được đầu dậy...

Tôi nghe ông nói câu đó, phải hàng năm trời mà không hiểu ra làm sao.
 

Đoạn kết

1. Tôi chờ để lên đò về thăm bố vợ tương lai vào dịp cuối năm. Hai bên sông Mã không cần trang trí gì mà mùa xuân vẫn phô bày ra hương sắc. Người giục tôi lên đò dọc vẫn là ông Tơ. Năm tháng không làm cho ông già đi một chút nào, da đen óng ánh như củ nâu ngậm bùn. Ông hỏi tôi.

- Tọa, có phải mi không? Đi đâu mà vào lúc người ta đón xuân này!

- Tôi về quê ạ!

- Bao năm ở quân đội có lên được tướng tá chi không?

- Trung sĩ...

Tôi trả lời ông, lời nói không muốn phát ra khỏi miệng. Chắc không ai có thể thông cảm cho người lính khi qua trận mạc hàng chục năm trời như tôi mà chỉ tiến bộ ngần ấy.

- Hả? Về được là tốt rồi... Lên đò đi chứ. Bây giờ không còn xe cộ đâu...

2. Bố vợ tôi có một mong ước. Những ngày tháng ít ỏi con theo thầy không hề uổng. Nhưng thầy cứ tiếc một điều, những điệu hò sông Mã mà người xưa yêu mến không còn ai chăm sóc biết đâu mai này không còn bóng dáng trên đời này nữa. Thày thèm đi trên con đò trên sông này một lần, được nghe câu hò của các cô gái bên sông ra gánh nước....

Tôi vừa được người vợ hiền là cô giáo Hồng. Được người cha vợ là người thầy đầu đời dạy cho mình biết viết những lời ca đầu tiên. Vâng là khúc ca vang lên từ sông Mã quê mình.

3. Tôi hỏi bố vợ về bà Thình, bà Chiếng... và cô gái lỡ lứa, cùng anh chàng cán bộ thủy nông... Ông cười vang cả nhà. Họ nhiều đến mức ông không nhớ hết. Nhưng con đò đã xe duyên cho họ. Có người nên vợ nên chồng. Họ có dịp qua sông và không có dịp phát vãng trong dòng đời. Nhưng chắc là những đêm nằm trên con đò ngắm trăng và nghe giọng hò khoan thì không ai quên nổi.


Những ngày cuối tháng 12-2008
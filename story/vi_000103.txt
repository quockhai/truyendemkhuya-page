I.
Cái xác chết trôi dạt vào ụ đá bên hông quán Chiều Mơ vào một chiều ảm đạm. Bữa đó Út không đi làm, chị Mén cũng ở nhà. Hai chị em theo đám người trong xóm rần rần kéo lên trên đê xem người ta vớt cái xác chết trôi. Bà chủ Chiều Mơ te te lại xem mặt người chết ré lên một tiếng thất thanh: “Cha mẹ ơi… con Bèo!” rồi đâm đầu chạy biến vào trong quán, từ đó đến tối cũng không thấy ló mặt ra. 

Út đứng xem cứ thắc mắc rõ ràng là chị Thúy Hồng mà sao bà chủ gọi tên Bèo. Hỏi chị Mén, không thấy chị trả lời. Út dòm qua thấy mặt chị trắng bệch, mắt đỏ ngầu ầng ậc nước. Cũng phải. Chị thân với chị Hồng thế kia mà. Ngày nào đi làm chị Hồng cũng sang réo chị. Lắm lúc chị Hồng gây lộn với thằng Ki, sang ngủ nhờ, hai người cứ rúc rà rúc rích tâm tình gì đó suốt cả đêm làm Út không ngủ được, cứ lăn tới lăn lui như giường có rệp. 

Út không thương cũng không ghét chị Hồng, giờ thấy chị ra nông nỗi cũng không khỏi chạnh lòng thương xót. Gương mặt chị hồng hào xinh đẹp là thế mà giờ nhợt nhạt nhăn nhúm, thấy cả mấy miếng da bong ra tím lịm bên chóp. Còn cái thân hình mướt rượt mà lắm lần Út nghe thấy đám đàn ông đầu xóm khen “thơm như múi mít” giờ phình lên, trương cứng tưởng chừng chỉ cần chọc một ngón tay vào là bục ra, xì hơi như trái bóng. Một bên khóe miệng chị ri rỉ chảy ra một dòng đỏ lét, nhớp nháp khiến cô Năm bún riêu đứng cạnh Út lấy tay che mắt thằng con trai bảy tuổi, lôi nó đi xềnh xệch: “Về! Ở đây bắt gớm, coi gì!”

Lúc hai người công an khiêng cái cáng phủ tấm ga trắng chạch đi vòng qua quán Chiều Mơ, đám khách khứa tò mò bu đen đỏ bên mấy ô cửa tre vuông vuông không hẹn mà đồng lúc nghe một cảm giác nôn ọe dâng lên ngang cổ họng. 

Người chết được mang đi rồi nhưng cái mùi tanh lợm vẫn bốc lên, như đâu đó phía ngoài kia, từ sát mí con sông nước trở màu đục ngầu đỏ loét trong ánh chiều tà. 

Nghe đâu bữa đó quán Chiều Mơ đổ cả mớ đồ ăn khách bỏ mứa xuống sông. Nghe đâu bà chủ bắt đám tiếp viên xịt từ đầu đến chân hết ráo mấy chai nước hoa bà mua hôm đi biên giới cũng không át nổi thứ mùi quái ác nồng nặc luồn lách lẩn quất trong bầu không khí đến cả mấy ngày.

Mấy ngày đó bà chủ Chiều Mơ nhấp nhổm như ngồi trên đống lửa. Khách đến lèo tèo vài ba mống, đám tiếp viên uể oải ngồi không, xầm xì với nhau chuyện con nhỏ nhảy sông. Mấy ngày đó cũng là mấy ngày thằng Ki gườm gườm ra vô đá ghế đá bàn, mặt lừ lừ như con gấu. Đám tiếp viên vì thế lẩn như chạch. Nó ra nhà trước, cả bọn túm tụm tuốt mé sau. Nó xuống ăn cơm, đứa nào đứa nấy lẳng lặng ôm chén dạt ra sân trước. Một bữa tóm được con nhỏ đang run cầy sấy chỗ cây dừa nước nhìn ra bờ đá, nó xáng một bạt tai ná lửa chỉ vô mấy que nhang bốc khói đỏ lòe: “Gì đây? Con quỷ cái! Tụi mày dám lén ông cúng bái con đĩ chết trôi đó hả? Muốn đi theo nó đúng không!” Con nhỏ xanh lét, ngồi té bẹp trên đất, cặp đùi trắng nõn hơ hớ run rẩy sau lần váy mỏng y một cây chuối bị phạt tè ngang gốc. Thằng Ki liếm mép, lôi tắp lự con nhỏ vô cái chòi mái lá lùm xùm đổ dốc.

II.
Khuya đó Út thấy chị Mén đi làm về không được bình thường. Chị đi chân thấp chân cao, tay cầm cái bóp đầm áp chặt một bên đùi che cái gì đó. Út giả đò thở đều đều lúc chị vạch mùng thò cái đầu hăng hắc mùi nước hoa lẫn mùi bia khẽ khàng: “Ngủ rồi hả Út?”. He hé mắt ngó theo, Út thấy chị từ từ bỏ cái bóp xuống mặt bàn, chầm chậm xoay thứ chị vẫn che che giấu giấu nãy giờ dưới ánh đèn. Suýt chút nữa Út để mình bật lên tiếng kêu thảng thốt. Váy chị rách toạc một mảng dài. Phần da thịt hiện ra chi chít những vết bầm đen, vết xước móng tay kéo dài, cả những vết răng hằn tươm máu. Những vết răng còn theo đường váy rách lên cao, cao nữa…

Út nghe tiếng hai hàm răng mình trèo trẹo nghiến riệt lấy nhau. Quân độc ác. Lũ thú vật. Những đồng pôlime xanh đỏ của chúng... Tiền ư? Tiền là gì chứ?

“Tiền là tiên là phật, mày chưa nghe nói sao cưng! Tiền biến thằng ngu thành một ông hoàng, biến đứa con gái trong trắng thơ ngây thành con đĩ. Tiền giúp thằng nhỏ oe oe khát sữa như mày cùng con chị nó không chết đói ở đầu đường xó chợ, để lớn tướng được như giờ đó cưng.” - Tiếng chị Hồng lè nhè trong một cơn say lâu lắc rồi bỗng đâu trở về ong ong trong đầu Út. Cắn chặt môi, tiếng xối nước ào ào trong nhà tắm của chị Mén lẫn lộn trong tiếng nước mắt Út giọt tiếp giọt lặng lẽ rớt xuống manh chiếu cũ mèm. 

Trong đêm những giọt nước sáng bật lên, trong văn vắt.

III.
Bà chủ quán Chiều Mơ rốt cuộc cũng tìm được căn nguyên của thứ mùi lạ gần cả tuần ám riết lấy quán bà. Xăm xăm đi thẳng ra đám đất rộng rinh mặt sau quán, bà chỉ tay vào một đám cỏ lách um tùm chen chúc cạnh hàng rào: 

“Mày lôi mấy cái bị đen lủ khủ trỏng ra tao coi thử giống gì!”

Thằng Ki một tay bịt mũi, một tay cầm cây sào dài chọc chọc vô đám cỏ, tòng teng kéo ra một cái bịch ni lông bốc mùi tanh ói. Cây sào chọc chọc thêm vài cái nữa, cái bị toạc ra, ri rỉ xuống đất một dòng đỏ loét đỏ lơ như thứ bữa chảy ra từ miệng cái xác chết trôi.

“Gớm quá đi mày!” – Bà chủ Chiều Mơ ré lên trong lúc thằng Ki tiếp tục chọt chọt vài ba cái nữa.
“Lỡ rồi… Để coi cái gì…” – Thằng Ki mặt lạnh tanh.

Một mớ đầu cá bầy nhầy bết máu vung vãi đầy ra đất. Đám ruồi nhặng xô tới, bu đen đặc trên mấy cái miệng nhọn há hốc, mấy con mắt trắng dã lờ đờ.

“Mẹ! Con nào thằng nào chơi cái trò dơ dáy này không biết!” – Định nhổ một bãi nước bọt theo thói quen, bà chủ Chiều Mơ sực nhớ còn kẹt hai lớp khẩu trang trên miệng. Ngoay ngoảy đánh cái mông vĩ đại, bà trở vô nhà bằng mấy bước chân sầm sập nện xuống cái cầu gỗ thiết kế vòng vèo uốn lượn làm lối đi cho khách tới mấy căn nhà chói chênh chếch mép sông. 

Cả khu xóm trên đê lẫn dưới đê thế là được nghe một bản giao hưởng sống động từ bà chủ Chiều Mơ ngay buổi tối ngày hôm đó. Phải công nhận giọng bà khỏe và thánh thót ngân nga được lắm cung bậc lên xuống khác nhau. Lúc rên rỉ than van làm ăn ế ẩm, phận tay yếu chân mềm nên thiên hạ lờn mặt coi thường. Lúc lanh lảnh rủa xả cả dòng cả họ đứa nào ác độc đem đầu cá tanh tưởi bỏ hàng rào độc hại quán bà bằng mùi, triệt hạ chuyện kinh doanh của bà bằng mùi. Đúng là thứ ganh ăn tức ở, thấy người ta có thì xúm vô đạp đổ cho hôi. Bà biết ra đứa nào thì không phải mình nó, mà con cái nó, cha mẹ nó bà nhận đầu xuống sông cho cá nó rỉa, cho Hà Bá kéo chân. Chửi chán, bà sai thằng Ki vác thanh thép sáng loáng dài cả thước đi vòng vòng quanh xóm. Sáng ra cột cửa ngõ nhà ai cũng hằn một vệt dài ngoằng, nhìn thấy ghê ghê. 

IV
“Con mẹ Chiều Mơ dạo này trúng tà hay sao mà khuya nào cũng chửi um hết xóm!”- Ba Thợ Cạo quấy rột rột cây tăm bông trong lỗ tai, lim dim mắt trên cái ghế nệm xơ xác lòi một miếng mút to đùng.

Không ngẩng lên, Út bẻ đôi lưỡi lam bén ngót gắn vào dao cạo. Một mảng râu ria lờm xờm bong ra khỏi khuôn mặt ông khách.

“Ai biết được anh. Nghe đâu đứa nào chơi xấu liệng đầu cá vô rào nhà bả.”

“Xời. Muốn chửi đã miệng thì dựng chuyện vậy thôi. Chứ dân chòm này ai dám dây vô ổ kiến lửa đó. Ngó theo cái thân vẽ rồng vẽ rắn của thằng Ki là ai cũng rợn rồi…”

“Bởi vậy nó mới được thể. Tối qua nó xách mã tấu đánh dấu từng nhà anh thấy không? Dân chòm mình hiền nên lép nó. Em bằng được cỡ anh em chơi nó một cú biết tay.”

“Thôi thôi. Tao lạy mày lo yên ổn mà làm ăn. Ai hùng hổ gì thây kệ người ta. Nói chơi có ngày mang vạ. Lại khổ con chị mày…” – Ba Thợ Cạo chùng giọng, lom lom nhìn mặt thằng nhỏ học nghề. Mắt sáng, mày rậm, trán dồ ra cả thước. Thằng này tánh hiền hiền mà cũng ngang đến trời gầm. Lớn lên ở cái nơi cóc cắn trên đê dưới bãi này, biết nó còn được mấy ngày hiền hậu vô tư.  

V.
Út theo chị Mén ra sông một đêm đứng gió. Chị đi trước, tóc thả dài đung đưa những vệt trăng đầu tháng nhạt màu. Út xách cái giỏ nhựa lẽo đẽo theo sau, nghe mùi hoa nhài thoang thoảng từ mái tóc mới gội ướt nhèm. 

Út nhớ một lần lúc nhỏ nằm bên chị thật thà: “Em hổng nhớ gì về má hết. Chỉ nhớ đâu hồi đó có lần thấy má gội đầu. Tóc má thơm ơi là thơm. Thơm mùi hoa nhài đó Hai. Giờ đi đâu nghe mùi hoa nhài tự nhiên đứng lại, hít cho đã rồi đi...”. Sau bữa đó, Út thấy trong nhà tắm có thêm chai dầu gội trên nhãn để hình chùm bông nhài trắng muốt. Lần nào đi làm về chị Mén cũng gội đầu, mùi son phấn nặc nồng thay thế bằng mùi hương nhẹ nhàng, tinh khiết mà phảng phất theo giấc ngủ êm đềm của Út đến tận sớm mai. Út chỉ thích nhìn chị duy nhất mỗi sớm mai. Khi chị quấn cao mái tóc dài, cầm cây chổi quét nhà loẹt xoẹt, bộ đồ bộ chị mặc in hoa, in cả nắng lung linh. 

 
 Minh họa: Lê Trí Dũng


Trăng lốm đốm mặt sông lúc chị Mén bới ra một chén cơm vung, xấp xấp lên trên ít thức ăn, đặt đôi đũa bắt ngang. Chị hướng ra mặt nước lay động bóng trăng, lầm rầm như đang nói với chính mình: 

“Hồng à!... Mà không… Bèo à! Cho tao gọi mày bằng cái tên đó nghen Bèo. Xong xuôi hết nợ đời rồi, Hồng hiếc gì trả lại hết cho người ta đi mày hả. Rốt cuộc rồi Bèo cũng lại là Bèo. Bèo theo sông theo nước mà đi, để lại mình tao ngồi khóc mày như thế này sao? Mà tao đúng là điên thật. Tội gì khóc chứ hả Bèo! Ở bên kia chắc mày sung sướng hơn ở đây gấp biết nhiêu lần. Vậy nên mày mới đi. Con khờ. Đi cũng không kiếm cái áo nào đèm đẹp mặc mà đi, bữa mặc chi cái áo cũ mèm. Bọn mình có gì tốt đẹp khác ngoài mấy thứ khoác bên ngoài đó hả mậy? Vậy mà cũng không đem. Mà đã đi thì mày đi luôn đi. Theo dòng mà ra thẳng biển. Về quê mày, về với thằng chài nghèo thương mày hồi đó luôn đi. Dạt chi, neo chi lại cái chỗ khốn nạn này…”  

Chị Mén quệt ngang dòng nước mắt, cúi đầu. Gió bỗng nổi. Tàn giấy đỏ âm ỉ bắt gió bùng lên rừng rực đến những mảnh cuối cùng. Rồi tao tác, tả tơi rải xuống lòng sông những mảng vụn đen đúa, còng queo.  

“Về ăn với tao chén cơm đi nhé, Bèo ơi. Đúng một thất rồi. Tao đốt cho mày cả một mớ tiền, về mà lấy đem đi. Không có tiền thì ở đâu cũng bị người ta coi rẻ thôi, con khờ.”

Tiếng chị Mén vẫn rầm rì rầm lan theo gió, theo nước. Từ đám cây bụi lùm xùm phía xa xa, một hồi những tiếng lá lay động rầm rì, xạc xào đáp lại. Bỗng nhiên Út nghe cơn ớn lạnh từ đâu chạy dọc sống lưng khi những que nhang trên cát vụt đỏ bừng. Cái gương mặt phồng căng, ri rỉ máu bên khóe miệng của chị Hồng ngày càng lởn vởn qua lại dày hơn trong tâm trí. 

Leo lên tới lưng chừng mấy bậc cấp dẫn lên bờ đê, một cái gì đó thoáng qua tầm mắt làm Út dừng sững lại. Rõ ràng Út vừa nhìn thấy một cái gì đó thoăn thoắt đi trên vách đê, chỗ ụ đá nhìn lên quán Chiều Mơ. Cảm giác lành lạnh sống lưng trở lại khi Út sực nhớ ra vách đê chỗ đó khá dốc, dê cừu còn leo lên leo xuống tìm lá gai ăn được, chứ con người thì… Mà ai lại mò mẫm đêm hôm làm gì ở cái chỗ vừa hôm rồi đậu một cái xác chết trôi. Cơn tò mò nổi lên át xuống cảm giác rờn rợn da thịt. 

“Phải chị Hồng chắc bả cũng không nỡ nhát mình, mình với chị Hai mới cúng bả đây mà.” Tự trấn an, Út thụt lùi từng bước nhè nhẹ sau lưng chị Mén, rồi co giò chạy biến vào bóng tối dưới bờ đê.

VI.
Bà chủ quán Chiều Mơ tất tả bươn ra bãi đất sát hàng rào đã thấy thằng Ki đang dí một cái đầu đen đen xuống đất bằng đôi cánh tay hộ pháp. Cái thân hình tiếp sau đó vẫn liên tục choài đạp, vẫy vùng mãi đến lúc thằng Ki cho luôn mấy cú đá thúc mạng sườn mạnh thấu trời. Thằng xỏ lá vậy là nằm bẹp, người cong lên như con tôm, ư ử tiếng rên đau đớn. Mấy ánh đèn pin nhá lên. Bà chủ Chiều Mơ thốt một tiếng nghiến răng trèo trẹo: “Cha nó! Thằng Út em con Mén đây mà…” 

Một tay ghì mặt thằng nhỏ oặt èo như cọng bún ra dưới ánh đèn, thằng Ki giơ tay còn lại táng bôm bốp vào khuôn mặt bé choắt đang tái hẳn đi.

“Thằng chó! Thằng chó! To gan cóc hả mậy? Dám đem đầu cá làm thúi quán ông hả mậy? Mày tới số rồi con.” 

Thêm mấy cú thoi bằng cái nắm tay cứng như sắt trút xuống tấm thân hình gầy tong, để rớt ra mấy tiếng thều thào:

“Không… Không phải tui mà…”

“Láo! Không mày chớ ai? Tao bắt tận mặt mày đang rình rập cạnh hàng rào mà còn chối hả, thằng chó!”

Chuỗi đấm đá sắp liên tục tiếp thì một bóng người đầu tóc xấp xõa từ trong nhà lao ra, ôm chầm lấy bắp chân lông lá của thằng Ki:

“Đừng! Đừng đánh nữa. Đừng đánh nữa mà… Em xin anh… Em lạy anh…! Không phải nó đâu mà..”

Bà chủ Chiều Mơ nhảy dựng trên đôi chân ngắn ngủn, xia xỉa những móng tay đỏ chót vào trán đứa con gái đang lăn lê bò ra đất khóc ỉ ôi, bà quẳng về phía thằng Ki cái nhìn gớm chết:

“Xử sao cho được. Tùy mày.”

Buông thằng nhỏ chỉ còn là một cọng bún mềm nhũn ra đất, thằng Ki cúi xuống, nhè hàm răng vàng ệch khói thuốc nhăn nhở cười với miếng thịt mơn mởn đầm đìa nước mắt:

“Bữa một lần chưa đã. Nhớ anh dẫn xác tới nữa đúng không cưng?”. 

Nó chồm xuống giật phắt mẩu áo hai dây mỏng mảnh, xé bung luôn cái váy ngắn cũn cỡn. 

Bà chủ Chiều Mơ cười khẩy ngó con tiếp viên bị đè nghiến ra đất. Đồ thân lừa ưa nặng. 

Ung dung, đắc ý bà quay lại trong bếp, dặn đám nấu nướng coi lại nồi ba ba hầm. Có lão khách Việt Kiều khoán trắng khoản này cả nửa tháng nay để tăng cường sinh lực. 

Ngoài mé vườn vẫn hồng hộc, lồng lộn tiếng thằng Ki xen lẫn giữa những âm thanh rên xiết oằn oại đứt khúc. 

Không ai biết có một đôi mắt đang hằn lên, ngầu đỏ vằn vện những đường gân máu. Và một bàn tay quắp lại thành một nắm đấm siết chặt, run run.

VII.
Chớm trưa.

Ba Thợ Cạo hớt hải dộng rầm rầm lên cánh cửa khép.

“Tiêu… tiêu rồi. Thằng Út giết người rồi cô Mén ơi!”

Một tiếng gì rơi xoảng trong nhà. Rồi cái mặt tái xanh tái xám của Mén thò ra, ngó Ba Thợ Cạo:

“Giết người… Ai?...”

“Thằng Út! Thằng Út nó đâm chết thằng Ki bên Chiều Mơ rồi. Cô lẹ qua mà…”

Không đợi nghe hết câu Mén vụt lao ra đường cái. 

Trời đương nắng. Nắng chòng chành lay lắc trên mấy ngọn cây xanh mướt dưới chân đê. Dòng sông hứng nắng, miên man chở những hạt vàng lấp lánh vòng vèo uốn khúc qua một khoảng đê giờ bu đông nghẹt, lố nhố những đầu người. 

Nắng túa nhòe mặt đường dưới chân Mén, trước mắt Mén cả chục mặt trời chênh chếch, chói lòa. Choáng váng, xây xẩm Mén khuỵu đầu gối xuống đất. Không đau. Chỉ nghe tê dại ong ong nhập nhoạng trong đầu những tiếng người lao xao đã dường như ở rất gần.

“Sao khổ thế này cô Mén ơi!” – Ba Thợ Cạo sụt sịt vừa đỡ vừa lôi Mén lọt qua cánh cửa chăng ngang chăng dọc những hoa hòe, điện đóm của quán Chiều Mơ. 

Ba bốn đứa con gái mặt bự phấn trắng ởn, le te chạy về phía Mén, thầm thì rối rít, không ra vui cũng chẳng ra buồn:

“Công an họ tìm mày ở trỏng. Bả khai hôm qua thằng chả có đánh mày mấy bợp tai.

VIII.
Mén rời đồn công an lúc đã chạng vạng. Cố đi thật nhanh trên đôi chân còn chưa thôi lập cập, Mén băng ngang qua cổng quán Chiều Mơ, vòng xuống chân đê. Cái ghế dựa i-nox sáng loáng không thấy nhắc ra để trước quán như mọi ngày. Cũng phải. Thằng người ngồi trên nó lúc trưa đã được đắp chiếu khiêng đi rồi. Còn lâu mới có đứa thế chỗ cái thằng vừa mới chết. 

Nghe một đứa trong quán kể nó chết mà mắt mở trợn trừng, cái miệng hô há hốc như con cá đang quẫy nước thình lình bị một nhát bẹp đầu. Chết mà không tin nổi mà mình lại chết. Chỉ tiếc Mén đến trễ, không được coi trên cái mặt kinh tởm của nó trong lúc vật vã đau đớn, ngớp ngớp từng làn hơi ngắn ngủn ngắm sự sống trôi tuột khỏi tầm tay, nó có nghĩ đến Bèo. Đến con nhỏ đem hết vốn liếng của một đứa gái bia ôm dâng hiến cung phụng cho thằng chồng hờ bất lực, chỉ nhăm nhăm chực đè nó ra vần vò, cấu xé rồi thượng cẳng tay hạ cẳng chân cho thỏa cơn tự ái đàn ông. Đến con nhỏ thà nhảy sông chết cũng không chịu bị chồng bán cho lão Việt Kiều già làm vật thử nghiệm thứ món ăn cường dương đại bổ gì gì đó. Con nhỏ đó chắc giờ đang đứng chờ thằng người làm nó đớn đau khốn khổ đâu đó trên con đường thế giới bên kia. Mày sẽ cười vào mặt nó hả hê vui sướng hay lại ngu ngốc mà ủ dột khóc lóc vì nó hả Bèo? Mày có vì nó mà oán giận em tao… Thằng Út tội nghiệp của tao, tao sẽ làm gì với nó bây giờ. Làm sao với cuộc đời phía trước của nó bây giờ?

Đêm sụp xuống nhanh hơn nuốt một tiếng than Mén để bật ra đầu lưỡi, nuốt luôn cái dáng hình nhỏ bé chập choạng vào con đường nhỏ vòng vèo, om om tối cạnh dốc cầu.

Cả buổi trời quay vòng với mớ câu hỏi của người cán bộ điều tra có đôi mắt như thấu suốt ruột gan kẻ đối diện, Mén cố đóng kín mọi ý nghĩ về nơi ấy - cái chuồng dê cũ ẩm thấp, mục miễu của Ba Thợ Cạo. 
Nửa sau chuồng đã lẩn khuất chìm lút trong đám cây cỏ, lùm bụi lan ra rậm rịt um tùm. Nhìn từ xa chỉ còn thấy mấy cái rìa mái tôn lởm chởm chìa ra phía mặt sông. Đoạn này sông vắng vì bờ dốc, mặt nước phình to ăm ắp, mùa cạn cũng sâu lút đầu người. Mấy năm trước có đứa nhỏ học sinh theo bạn tắm sông hụt chân chết đuối đoạn này. Rồi năm sau nữa, mấy người đi kéo cá, kéo luôn một bàn chân người nhão nhớt. Khúc sông từ đó vắng tanh. 

Chỉ có tiếng nước lọc xọc chảy qua những thanh sắt đóng kè ngăn lở đất vọng theo tiếng gọi Mén thầm thì trên mấy bậc thang chuồng ọp ẹp:

“Út… Út ơi… Chị nè!”

Một ánh đèn pin chợt nháng lên, rồi cái đầu bù xù của đứa em trai Mén thò ra, lớ ngớ sau cánh cửa tôn hoen rỉ:

“Hai! Sao mà Hai biết em ở đây?”

“Anh Ba nói…”

Nắm bàn tay run rẩy chìa ra của đứa em trai, Mén đu người vào hẳn trong chuồng. Leo lét dưới ánh đèn pin vẽ một vòng cung sáng tối, hai ba vỏ chai nước khoáng rỗng nằm lăn lóc, nửa ổ bánh mì ăn dở gói trong bịch ni lông, một cái áo jean bạc màu cuộn lại như cái gối. Và… Út cảm giác lùng bùng trong đầu. Một con dao Thái Lan lưỡi thép ánh lên sáng loáng. 

Không kiềm được, Mén giáng thẳng tay. Một bên má đứa em trai hằn dấu năm ngón tay thẳng đuột.

“Chị…” – Tiếng gọi sửng sốt nghẹn ngào tắt ngang cổ họng. Út đứng đó. Cứng đờ lạnh toát nhìn chị mình đổ gục xuống lớp cỏ khô bục nát, nồng mùi.

“Tại sao? Tại sao em lại đến thế này hả Út? Chị nuôi em đâu phải để em giết… người!…”

Thằng Út quỳ xuống đỡ Mén. Mén ngẩng lên và đọc được những ý nghĩ trong đầu nó. Chị! Em có thể ngoảnh mặt khi nhìn người ta dày vò chị được sao? Chị có biết vì sao em bỏ học, vì sao em theo Ba Thợ Cạo học nghề? Đó là vì em muốn kiếm tiền thật nhanh, thật nhiều tiền để chị không còn tối nào cũng phấn phấn son son đến cái động quỷ của mụ Chiều Mơ đó nữa. Không một lão già nào được sờ mó, quăng tiền vào mặt chị nữa. Không một thằng ma cô nào đánh đập hành hạ chị nữa. Chị chịu đủ rồi! Mà em cũng nhịn đủ rồi. Quá đủ rồi…

“Tại sao em lại giết… người!”, vẫn câu nói lập bập trong sợ hãi đến mê sảng, Mén dồn vào đứa em.

“Em… không giết nó. Lúc em tới nó… đã chết rồi. Em không giết người… Cũng không phải em bỏ đầu cá vào hàng ranh quán mụ Chiều Mơ… Có một người đến từ cái thuyền thúng giữa sông. Em đã theo anh ta vào tận sát rào quán... Chị có tin em không?”

Mén nhìn vào đôi mắt khẩn khoản chờ đợi của em trai. Một đôi mắt còn trong veo thế này sao lại có chi những gợn sóng tủi phận, hận thù. Không. Không thể được… 

“Đi thôi!”

Đứa em trai ngơ ngác nhìn người chị vụt đứng dậy trong góc chuồng ẩm tối.

“Đi đâu chị?”

Quyết liệt, bất ngờ, người chị lôi phắt em mình ra khỏi chỗ nó chui rúc ẩn náu suốt một ngày trời. 

Băng băng đi ngược dòng sông đang lặng lờ trôi về phía phố, phía những ánh đèn vàng lấp lánh, hai cái bóng – một mảnh dẻ nhỏ bé, một gầy guộc lêu nghêu dắt tay nhau, cắm cúi về chân trời trước mặt. 

IX.
Người ta nói có con nhỏ tiếp viên dẫn em trai bỏ đi khỏi xóm nhà lá ven sông. Ai cũng đồn chị em nó trốn đi để chạy tội giết người. Mãi tới lúc công an còng tay một người dẫn vào quán Chiều Mơ dựng lại hiện trường, cả xóm mới hay thủ phạm đâu phải là thằng Út. Người này thấp đậm, da đen bóng nom chắc nụi. Nghe đâu cũng dân chài miệt biển. Nghe đâu người cùng làng với con nhỏ tiếp viên chết trôi dạo nọ của Chiều Mơ. 

Lúc bị giải ra xe người đó cố ngoái đầu, nhìn miết về phía cái ụ đá chênh chếch trông ra mặt sông cồn cào nhịp sóng. Chắc vì vậy mà quanh ụ đá bỗng đâu trào bọt trắng đục ngầu.

Chiều Mơ quán đóng cửa luôn kể từ hôm ấy. Bà chủ nhỏng nhảnh phốp pháp còn bận ngồi gỡ lịch lâu lâu.

Và cánh cửa một ngôi nhà trong xóm bờ sông cũng khép cửa đã lâu. Ba Thợ Cạo cách đôi ba bữa lại thẫn thờ dạo qua, bâng quơ câu nói một mình:

“Chị em thằng Út bộ không về đây nữa sao ta?” 
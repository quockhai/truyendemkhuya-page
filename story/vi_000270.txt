Làng Nhan nằm ở lưng chừng hõm núi Sót, giữa những mảng đá lô nhô chạy cuối dãy Trường Sơn.
Làng chỉ có vài chục hộ, sống chủ yếu bằng việc cấy những mảnh ruộng sát chân mấy quả núi nhỏ tạo thành thế đứng của núi Sót. Lúc rảnh rỗi thì họ rủ nhau đi rừng lấy măng, mật ong, và những sản vật khác, phục vụ cho cuộc sống thường ngày.
Giữa làng Nhan có một giếng hồ cổ. Gọi là giếng hồ vì vừa là giếng nông, vừa là hồ nhỏ. Trẻ con trong làng không dám nhảy xuống tắm trộm dù nước chỉ ngang cổ đứa trẻ lên mười. Cá trong giếng hồ chỉ khi mặt trời chưa lên, người ra lấy nước sớm nhất mới nhìn thấy. Cả đàn cá lấp lánh sáng lượn lập lờ trên mặt nước giếng hồ, ngửa cổ đớp sương. Khi nhác thấy bóng người, lũ cá quẫy đuôi rồi lặn suốt cả ngày đến đêm, không ai còn nhìn thấy chúng nữa. Người tinh mắt nhất cũng không nhìn thấy lũ cá nằm ở đâu trong đáy hồ, mặc dù nước trong suốt có thể nhìn thấy đáy với những cọng rong rêu đung đưa. Hình như có một khe nhỏ nào đó mà mắt thường không thể nhìn thấy.
Vào mỗi sáng sớm, khi mặt trời chưa lên, người trong làng tự giác mỗi nhà chỉ dám vục một gánh nước nguồn sớm gánh về chứa trong chum vại. Nước này chỉ dùng để ăn. Các cô gái làng cũng thường dùng để tráng người sau khi tắm xong. Chỉ thế thôi thì da dẻ trắng trẻo. Nếu ngâm người vào trong nước này lâu, sự việc sẽ ngược lại.
Đã có những trường hợp xảy ra thật kỳ lạ.
Ấy là trường hợp bà Thảo Lăm.
 

Minh họa: Xuân Hải
Bà tên Thảo, chồng tên Lâm. Nhưng cả làng cả huyện gọi là bà Thảo Lăm. Không hiểu người ta cố ý nói trại cái tên Lâm thành Lăm, là do bà Thảo hồi mới đẻ thằng con đầu bỗng nhiên nói nhịu, phải đi quét chùa một thời gian, hay do ông Lâm hồi mới ở chiến trường ra bị sức ép bom nên nói mãi mới tròn câu. Ông Lâm may chỉ là thương binh hạng nhẹ, cũng được suất thương binh, nhưng không ảnh hưởng lục phủ ngũ tạng, không ảnh hưởng chuyện sinh con cái. Ông Lâm ở chiến trường ra, nom đẹp trai rắn rỏi hơn hẳn thời còn là cậu học trò nhút nhát ở nhà, nên tuy giọng nói hơi bị méo vẫn khối cô gái làng mê. Người ông dong dỏng, dẻo dai như rái cá.
Thời trẻ bà Thảo mê ông Trạo, nhà ở sát chân núi Sót. Khi ông đi bộ đội, ngày tiễn ông, bà chạy theo xe đưa tặng ông cuốn sổ có vẽ đôi chim bay bằng bút mực đỏ. Nhưng suốt mấy năm chiến tranh, bà không nhận được lá thư nào của ông gửi về. Ấm ức mãi thì rồi cũng quên. Sau này, bà gật đầu nhận lời lấy ông Lâm, cùng học cấp ba, lại cùng đơn vị chiến đấu với ông Trạo, nhưng được về làng trước nhất, ngay sau khi hết chiến tranh.
Đêm động phòng, ông Lâm không nhịn được, buột miệng: Thế này mà ngày ở trong đó, thằng Trạo cứ nhất quyết không chịu biên cho cô một chữ. Đang đêm tân hôn, bà chột dạ hỏi nhỏ nhẹ: Thế anh biết gì về thằng cha bạc bẽo ấy? Chàng thương binh Lâm thật thà: Ai khiến cô vẽ đôi chim màu đỏ. Nó bảo cô trù ẻo nó chứ mê gì nó. Nó chê cô da ngăm đen, nom không sánh được con bé Huyền xóm dưới. Con bé Huyền thì tặng cho thằng Trạo cái khăn mùi xoa. Bà Thảo Lăm tức khí bật dậy khỏi giường tân hôn, kiếm cái ảnh của Trạo đốt chết tươi dưới ngọn đèn dầu.
Ông Lâm khi ấy mới ở chiến trường về, quý vợ mới cưới hơn củ tam thất núi, bên ngoài tỏ ra không ghen mà lại còn phụ họa với vợ (nhưng thực chất bên trong ai bảo ông không ghen sôi sục?): Ngu gì ngu hơn con dúi trên núi, vợ ta như củ tam thất chín năm ra mầm, mười ba năm ngậm nước núi thế này mà nó dám bỏ mặc, dám chê đen. Đen thế này chứ đen nữa con Huyền làm sao sánh nổi. Ông Lâm nâng bổng bà Thảo lên, người bà Thảo cũng mảnh mai, dáng nét nhân ngư, nên cung cách của ông Lâm tự nhiên cứ như lũ trai phố.
Chuyện này cũng có căn nguyên của nó. Vào dịp giải phóng miền Nam, khi tràn vào Sài Gòn, cánh lính đơn vị ông bắt được những tấm ảnh “đen”, trước khi nộp cấp trên, cả đống anh chàng trai tân lộc ngộc quê Bắc Kỳ mới trên núi xuống trố mắt ngắm nghía đến đỏ mặt hình ảnh những cuộc mây mưa kỳ quái của đám gái Việt với tụi lính Mỹ.
Bây giờ ông Lâm chỉ mới bắt chước chút đỉnh, đã khiến bà Thảo đỏ hết cả người, cứ nhắm chặt mắt lại không dám mở ra nhìn chồng mới cưới. Kỳ thực đấy là lần đầu tiên bà thấy ông Lâm thật sự đẹp trai như lời khen của mọi người. Mọi khi bà mải nhớ người cũ, không thiết nhìn chàng trai nào. Không ngờ chồng bà là người đàn ông rất ra dáng. Bà không bao giờ quên được cái đêm tân hôn đó, như lần đầu tiên được đi máy bay cùng chồng vào thăm Sài Gòn (cái gì lần đầu mà chả nhớ dai?). Sau này đầy lần ông Lâm cho vợ niềm vui thích, nhưng bà thì cứ nhớ mãi đêm tân hôn ấy.
Nhưng bà cũng không quên mối thâm thù kia. À, ra lão chết tiệt chê mình đen, không bằng con mụ Huyền cong cớn xóm dưới. Thảo nào mà lúc ở trại thương binh về làng là lóc cóc đi hỏi con mụ ấy làm vợ. Bà quên mất mình đã là vợ ông Lâm ba năm rồi thì ông Trạo mới dám cả gan vác mặt về làng, chân đi thiếu một ống. Thời gian quên ấy là bà đang sung sướng với người chồng bộ đội vinh quy về làng, lành lặn, đẹp trai, phong độ, đi đâu cũng được nở mày nở mặt.
Bà bèn làm chuyện tày đình là phá bỏ lời dặn truyền đời của các cụ: Chỉ được phép tráng nước giếng hồ khi tắm táp xong xuôi, không được ngâm người trong nước ấy. Các cụ cứ cấm thế nhưng biết đâu chả có những cụ đã phá lệ mà ngâm trộm mình trong nước giếng hồ từ lâu rồi? Vì cứ như bà suy ra, tắm xong tráng nước giếng hồ lần một người thơm thảo, lần hai, trắng tinh trắng nõn. Lần ba…  Huống chi lại còn ngâm mình trong nước ấy. Có khi do bà tổ Dong ghét thói đời đen bạc, nên mới cấm tiệt không cho con gái trong vùng được phép xinh đẹp quá chăng? Nhưng con gái làng Nhan vẫn nổi tiếng cả vùng vì đẹp. Đã nhiều đời trôi qua, lính lệ vẫn thường về làng bắt đi bao nhiêu cô gái đẹp mang về triều thi thố chọn cung nữ. Những mỹ nữ cứ bước từ làng Nhan ra, dáng đi tha thướt, cái dáng đi truyền hết đời này sang đời khác khiến bao nhiêu quan lại và vua chúa phải mê đắm.
Bà Thảo suy đi tính lại nát óc. Thôi thì cứ thử phá lệ một lần xem nó ra làm sao. Biết có phải lệ làng thật không hay chỉ là huyền thoại?
Huyền thoại ấy như sau:
Thời Đức Thái Tổ chưa lập chiếu dời đô, vua vô cùng yêu một nàng phi. Nàng tên Dong, được một người làng Nhan nuôi từ nhỏ. Nàng từ đâu tới, cha nuôi cũng không biết. Chính ông nhặt được nàng từ khi mới được mấy ngày tuổi, nằm ngọ nguậy trong đống lá dong khô bên cạnh giếng hồ. Hôm ấy ông ra giếng hồ sớm nhất làng. Ông người xấu xí, lưng cong như con khỉ già nên gái làng chê. Hồi trẻ, ông bỏ làng đi đâu đó rất lâu rồi trở về, râu tóc tua tủa, mắt nhìn ngơ ngác. Ông chỉnh trang lại căn nhà nát rồi lặng lẽ sống, không động chạm đến ai. Chỉ có một câu chuyện truyền miệng, người kể không mỏi miệng, người nghe cứ há hốc mồm, đó là thi thoảng vào buổi chiều tà, có những người nhìn thấy một con khỉ ôm xác một con thỏ hay xác con nai con còn tươi đến hiên nhà ông gù thả xuống rồi chạy mất dạng. Người kỹ càng hơn thì bảo đó là một con khỉ cái đuôi đỏ, gương mặt nom như một thiếu phụ, đôi mắt long lanh nước. Con khỉ cái thả thức ăn của rừng xuống hiên nhà ông gù rồi, vẫn còn lưu luyến tần ngần mãi mới bỏ đi. Nghe người già kể lại, ngày nhặt được nàng Dong, người nàng đầy lông lá. Nhưng đôi mắt thì mở to nhìn mọi người như là đứa trẻ đã hiểu mọi sự.
Nàng Dong càng lớn càng đẹp u uẩn, khiến trai trong vùng bỏ hết việc tìm đến, họ tụ tập tranh giành trước cửa nhà. Một người đi qua, chỉ mặt nàng Dong, bảo: Tai họa. Cha nuôi nàng lạy lục mãi, thầy mới phán cho câu tiếp: Cải họa thành phúc, một đóa dâng lên ngôi cao. Cha nuôi hiểu ra quyết dâng tặng Đức Thái Tổ đứa con gái nuôi.
Nàng Dong vào cung, hàng ngày đi tha thẩn trong vườn uyển, chỉ mỉm cười mà không mấy khi mở miệng nói. Đức Thái Tổ yêu mê mẩn khiến hoàng hậu và nhiều phi tần ghen ghét nàng. Họ cho người tìm đến nhà cha nuôi, tìm hiểu cặn kẽ về nàng. Tin tức cho hoàng hậu và các phi tần được biết rõ, vùng đó có một giếng hồ, con gái vùng này ai ai cũng có nước da mịn màng trắng ngần nhờ ăn nước giếng hồ và tráng thứ nước ấy sau khi tắm xong. Hàng ngày nàng Dong vẫn thường ăn và tắm nước ấy mà lớn lên. Hoàng hậu liền cho người đến múc nước giếng hồ đem về cung để uống và tắm táp. Các phi tần được lòng hoàng hậu cũng được bà ban phát cho thứ nước thiêng này. Dần dà cả những người vợ của đám lính cũng bí mật lấy cắp nước giếng hồ. Nguồn nước vì thế mà cạn dần, không đủ cho dân làng dùng. Cả làng chỉ trông chờ vào nguồn nước ăn duy nhất này.


Ảnh: Phạm Duy Tuấn
 
Đàn cá cũng đã lâu không ai còn nhìn thấy nữa. Điều này khiến dân làng lo lắng. Lũ trẻ theo lời người già chỉ bảo, thì thào rủ nhau rình suốt đêm không dám trái lời đứng lên để chạy về nhà ngủ. Khi người ra lấy nước giếng hồ đầu tiên thấy lũ trẻ ngủ quên bên thành giếng, đập cho vài phát mới tỉnh. Tỉnh dậy kể rằng đêm qua chúng mở to mắt ra hết cỡ, thở cũng không dám, vẫn không thấy chút động tĩnh gì. Chúng nằm úp sấp, đầu gối lên thành giếng mà căng mắt ra để nhìn xuống mặt nước, căng mắt quá nên nước mắt tiết ra, rồi nước mắt ấy rơi xuống giếng hồ, tan ra cùng nước.
Sương mù phả xuống làng Nhan những hạt li ti mang vị muối. Sương ấy cũng nhỏ xuống lòng hồ, tan loãng.
Đàn cá vẫn biệt tăm.
Như kẻ tha hương, nàng Dong ngồi trong vườn uyển, gảy khúc ca của dân làng Nhan:
Gió xuân ơi hãy thổi xanh đồng cỏ.
Ánh nắng ơi hãy chảy chan hòa trên những ngọn núi…
Mùa khô năm ấy kéo dài. Đám lính quây kín giếng hồ không cho dân làng ra lấy nước. Người cha nuôi bèn khăn gói đi lên kinh thành tìm nàng Dong. Nhưng ông đi chưa được nửa đoạn đường thì bị hoàng hậu cho người giết chết.
Nàng Dong về làng chịu tang cha. Trên đường về, nàng thoát chết mấy lần nhờ có đám cận thần vốn xưa kia cũng có những người từng si mê nàng. Cũng vì si mê mà họ đã theo chân nàng sung lính thị vệ, rồi cũng nhiều người dần có được những chức sắc quan trọng.
Về làng, nàng mới hiểu hết mọi chuyện. Khi ấy đám lính do hoàng hậu sai quây kín giếng hồ cũng được lệnh tạm rời khỏi nơi ấy, tránh bị nàng Dong tâu lên vua sự việc này.
Mộ cha nuôi đắp chủ yếu bằng đá tảng. Nàng đặt mộ cha quay hướng về rừng. Đêm đắp xong phần mộ cha, nàng nghe có tiếng rên rỉ phía rừng. Rừng bạt ngàn bí ẩn. Tiếng nỉ non như gió như bụi, vương thoang thoảng, tan loãng vào sương khói. Nàng đứng lên, đi về phía có tiếng khóc. Nhưng rừng vẫn lặng lẽ và rì rào, không có muông thú rình rập quanh mộ, không có ngay cả những chiếc lá vàng rơi rụng. Vì đã qua mùa rụng lá. Lá rừng đang lên xanh dưới tấm thảm mưa nhẹ nhàng phủ xuống vạn vật.
Sau khi mộ cha nuôi đã đắp xong, nàng Dong đến bên giếng hồ nguyền một câu: Nước giếng hồ này chỉ dành cho những ai biết đủ dùng.
Đêm đêm nàng ngồi bên giếng hồ nhìn chong chong xuống đáy hồ như gọi như chờ đợi ai. Rồi như có tiếng réo sôi, như những khúc nhạc thiên hồ rớt xuống đáy những vì sao, cả đàn cá ngoi lên tung tăng vẫy gọi nàng Dong.
Lúc đó, một thầy địa lý tâu lên nhà vua: Giếng hồ làng Nhan là cái rốn rồng. Nếu cái rốn rồng này mà suy thì vận nước sẽ suy. Vua bèn ra lệnh cho một nhóm lính thị vệ tức tốc về làng Nhan để tăng cường bảo vệ nàng Dong, rồi cắt cử trông nom giếng hồ.
Nàng Dong tự mình ngâm trong bồn gỗ ngọc am chứa đầy nước giếng hồ ba ngày ba đêm. Khi nàng bước ra khỏi bể ngâm, người làng không còn nhận ra nàng Dong mảnh mai dịu dàng, đẹp u uẩn trước đây nữa. Nàng trở nên lạnh lùng và sắc nhọn. Đám lính thị vệ rong ruổi đưa nàng về cung, không hiểu đi đường ra sao mà mãi ba tháng sau, nàng mới về tới. Gương mặt đẹp u uẩn giờ trở nên lạnh băng. Vua càng say mê nhưng chỉ ngồi trên cao mà ngắm vẻ lạnh băng đó, không dám vui vầy cùng thân xác nàng nữa.
Khi Đức Thái Tổ dời đô, nàng Dong xin được trở lại làng. Vua phần thấy nàng Dong nay đã dường như kẻ lạ, dường như người của cõi mộng; phần nể tình nàng bấy lâu, bèn đồng ý. Hơn nữa thực sự là cái rốn rồng không thể thiếu nhan sắc trụ trì trông nom. Tuy nhiên cũng vì sự xếp đặt của tự nhiên để nơi đó là rốn rồng nên cũng không được phép thay đổi xây cất nhiều, sẽ động chạm đến phong thủy.
Nàng Dong về làng lập lại lệ nông, cắt đặt mọi việc như cấy lúa, chăn nuôi gia súc, dạy dệt thổ cẩm cho những nhóm tộc khác biệt gồm toàn người tứ xứ tụ về. Nàng cắt đặt mọi việc, gương mặt lạnh lùng bí ẩn mà bước đi vẫn nhẹ như tơ, thân hình mong manh như giọt sương mai.
Chiều chiều, nàng ngồi bên giếng hồ, nâng cây đàn một dây ca bài ca tứ xứ:
Không thơm như hoa, không cao như cây.
Tôi chỉ là nhánh cỏ. Cỏ mướt xanh. Gió xuân làm cho tôi mướt xanh.
Ánh dương chiếu rọi vào tôi…
Khi ấy trong rừng thốt nhiên có những tiếng lao xao. Một vài cái đuôi khỉ đập đập như đang múa cùng lá rừng. Rồi hàng đàn khỉ múa lượn theo khúc nhạc. Một con khỉ cái ngồi im dỏng tai nghe khúc ca, mắt âng ẩng nước.
Sau này người làng bảo: Nàng Dong chính là con gái của một con khỉ cái đuôi đỏ. Lại cũng có người nhất quyết rằng Mẫu Dong là con gái của một nàng tiên cá.
Không biết nàng là nhân ngư hay hậu duệ của Tề Thiên Đại Thánh, khi Mẫu Dong mất, hàng đàn khỉ chạy về ngồi trước mộ, khóc nỉ non hàng tuần. Sau đó chúng kéo nhau đi đâu mất tích. Rừng vắng như rừng hoang. Còn đàn cá cũng bỗng nhiên biến mất.
Giếng hồ ngày nay cũng chỉ còn lại mấy con cá đuôi cờ lượn qua lượn lại lười biếng. Dù nước giếng vẫn như ngàn năm trước, trong veo, là thứ nước thơm cho làn da của những mỹ nhân làng Nhan.
*
*    *
Trở lại câu chuyện về bà Thảo Lăm. Bà lấy nước giếng hồ đổ vào thùng tắm đóng bằng gỗ ngọc am. Khi đổ nước vào thùng, gỗ thùng cứ thế mà đỏ dần lên như một khối son. Bà Thảo Lăm cởi hết áo quần bước vào. Người bà nóng giãy đam mê trả thù nỗi đau thời con gái. Bà quên hết lời nguyền xưa kia, quyết ngâm cho cái đen như củ tam thất trên da dẻ của mình tan biến.
Khi bà bước ra khỏi thùng nước, cả thân hình như một khối đỏ. Những khối đỏ xoay tròn quanh da thịt bà Thảo Lăm như những bông hoa điên điển tới mùa rụng cánh.


Ảnh: Phạm Duy Tuấn
 
Sau vụ việc trên, bà Thảo Lăm ngồi thiền trước ngôi đền Mẫu Dong ba tháng trời. Bà ngồi quên ăn quên ngủ, khiến ông Lâm suýt chút nữa bị bà Huyền bỏ bùa mê thuốc lú. Sau ba tháng ngồi thiền và cầu xin thành tâm, bà Thảo Lăm được Mẫu chứng, cho quẻ săm khá đẹp nước.
Bà Thảo Lăm trở thành người thắp đèn, châm hương, dọn lễ và quét đền thờ Mẫu Dong từ bận ấy.
Bà Huyền nước da trắng như trứng bóc.
Thời còn đi học, Huyền được cánh trai làng chấm điểm 8, thắng cô bé Thảo một điểm vì có nước da trắng hơn Thảo, nom cũng đa tình hơn. Huyền học không giỏi lắm, thường hay phải chạy sang nhà Trạo nhờ giảng toán. Nhưng Trạo chỉ coi Huyền như em gái.
Nhà gần nhà Huyền, nhưng khi đi học Trạo thường thích đi con đường ngang qua nhà cô bé Thảo, học lớp dưới. Khi đi ngang qua, Trạo nhìn vào thấy Thảo đang thắt khăn quàng đỏ thì huýt sáo gọi.
Khi ấy Lâm thường hay láng cháng quanh hai người. Nhưng Lâm hiền khô, cứ đứng xa xa mà nhìn Thảo. Vào lớp rồi, Lâm vẫn nhấp nhổm đợi xem Trạo có ra đầu hồi lớp huýt sáo gọi Thảo hay không. Lâm giấu nỗi buồn của mình trong lòng, cứ thế mà ra chiến trường.
Thảo đã quen với tiếng huýt sáo hàng ngày của Trạo, nên mới vẽ đôi chim bay ngang trời màu đỏ tặng chàng trai Trạo khi anh lên đường ra tiền tuyến.
Nhưng Thảo không biết, khi ấy Trạo đã làm hỏng chuyện của hai người rồi.
Mỗi khi đi học Thảo không biết có người luôn đi phía sau hai người, vô cùng ấm ức và khổ não. Huyền cứ đi vòng vo phía dưới một đoạn, có khi đi cùng bạn nhưng tai không nghe bạn nói gì, mắt chỉ nhìn thấy hình ảnh hai người phía trước. Trạo đi sau Thảo một đoạn, đủ để không bị bạn bè chế. Huyền được trai làng chấm điểm cao, nhưng trong lòng chỉ có Trạo. Cô học kém dần, chỉ nghĩ mưu để chia rẽ Trạo và Thảo. Huyền tích cực sang nhà Trạo nhờ giảng bài hơn. Cậu học trò to nghệch, tỏ rõ chí khí người anh, lại không tinh chuyện người lớn bằng cô bé ít tuổi hơn mình.
Đêm hôm đó, nghe chỉ bài xong, Huyền nhờ Trạo đưa về. Đấy là một đêm mưa gió. Gió từ trên núi Sót tràn xuống như đổ ụp cái lạnh vào thung sâu. Huyền bước thấp bước cao với cây đèn sắp hết dầu.
Gần đến giếng hồ thì Trạo bảo:
- Để anh chạy ù về châm cái đèn ở nhà.
Huyền níu tay Trạo:
- Không cần đâu, để em vặn cái bấc lên.
Huyền đặt cây đèn xuống đường, vờ vặn bấc lên, nhưng mồm thổi nhẹ một cái khiến đèn tắt ngấm. Đường làng vắng tênh. Giếng hồ reo long tong như khúc nhạc từ miền u uẩn vọng về. Núi rừng đồng lòng thổi gió xuống thung u u.
Huyền hốt hoảng ôm chầm lấy Trạo, miệng ú ớ:
- Ôi, anh Trạo ơi…
Trạo không ngờ đến tình huống này. Huyền không mặc nịt áo con, cái áo mỏng bị gió làm tuột hết khuy tung ra để lộ bộ ngực thanh tân dưới ánh đêm nhờ nhờ. Hình như khi ấy trăng sắp hé. Trăng cuối tháng nằm vắt ngang núi, bị mây che lấp. Huyền ép chặt bộ ngực trần vào ngực Trạo.
Rồi bằng động tác bất ngờ, Huyền kéo bàn tay Trạo đặt lên cái vú chũm cau đang căng cứng trong cơn gió lạnh tràn về từ núi cao…
Những ngày sau đó, Huyền không muốn tắm. Cô muốn giữ nguyên hơi ấm bàn tay Trạo trên ngực mình.
Sau một tháng trời không tắm, không động một chút nào nước giếng hồ, bên vú có bàn tay Trạo nắn vào bị sạm đen. Cái vết nám cứ ngày một rõ khiến Huyền rất lo lắng, nhưng không dám cho ai biết. Điều này mãi sau khi trở về làng với một bên chân, ông Trạo mới biết. Ông không còn nỗi đau tự mình dằn vặt mình vì Thảo nữa. Vả lại, ông đã chối bỏ Thảo ngay khi còn ở chiến trường, cả khi cái chết kề bên. Ông chối bỏ vì ông không còn lối thoát nào hơn là đành nhường cô cho người bạn, vì ông biết ông Lâm vẫn thường đi sau hai người một cách lặng lẽ…
Sau này, đôi khi bà Huyền cũng cởi khuy áo giả vờ bị gió làm tuột như xưa kia, với những người đàn ông khác, trong đó có lần với ông Lâm. Thực ra từ khi một bên vú bị sạm đen, bà Huyền càng ham muốn được bàn tay ông Trạo ve vuốt. Nhưng ngày ông Trạo ra đi đã không thèm chia tay như cách bà muốn. Đêm đêm Huyền đi lang thang trên con đường ngày nào đã ôm cứng Trạo, cảm nhận lại cảm giác ngày ấy. Rồi bà gặp một, hai, ba… người đàn ông còn sót lại trong làng hoặc đi qua làng, những người đàn ông vì nguyên cớ nào đó không phải ra mặt trận. Họ đưa tay nắn vết sạm trên bầu vú con gái quá thì. Bà Huyền thả mình cho những cơn mê tưởng. Bà như tìm lại được cảm giác với ông Trạo.
Khi đã lấy nhau, bà Huyền sinh ra Mãnh, nhưng dường như bị ngấm thuốc si mê, đêm đêm bà Huyền vẫn thường trốn ông Trạo, vật vờ trên con đường làng đi qua giếng hồ. Bà đã ngấm cảm giác được trốn chạy khỏi cuộc sống tẻ nhạt thường ngày, bằng việc mở khuy áo ngực cho những bàn tay đàn ông vục vào. Tuy vậy bà quyết không đẻ con lạc loài. Vì bà không thể tìm được cảm giác nào đắm đuối hơn bàn tay ông Trạo khi run rẩy chạm vào ngực bà năm xưa…
*
*    *
Con gái thứ hai của ông bà Thảo Lăm tên là Nhan Bình. Cô học giỏi nhất trường làng, rồi học lên nữa. Nhan Bình có cặp mắt hồ đào, nước da mịn màng trắng kỳ ảo, người mềm mại uyển chuyển, mảnh như sợi tơ. Hồi nhỏ mấy lần tưởng con bé Nhan Bình không qua khỏi sài đẹn, có bận sốt co giật suýt chết. Ông bà Thảo Lăm phải nhờ nhà lang Mường cứu. Thầy cúng súc miệng bằng nước giếng hồ, rồi phì ra khắp người con bé sài đẹn, mặc kệ cho nó khóc ngằn ngặt. Nhưng thật kỳ lạ, qua ba ngày, con bé Nhan Bình tỉnh lại, cười toét miệng, lại ăn bột thun thút. Bà Thảo Lăm quấy bột bằng nước giếng hồ, lại lấy nước ấy xông hơ cho con bé mỗi tuần một lần, cho tới khi Nhan Bình lớn cao bằng cây mai già trước cửa nhà. Nhan Bình sau trận ốm đó, cứ thế mà lớn không đau ốm gì nữa, nhưng người mảnh tơ, đôi khi đi qua ai đó mà như làn gió lướt qua.


Ảnh: Phạm Duy Tuấn
 
Người già trong làng bảo Nhan Bình có dáng nét của bà Mẫu Dong xưa.
Nhan Bình không hay biết chuyện xưa của người lớn, ngay từ nhỏ đã thích đi theo thằng Mãnh, con trai duy nhất của ông Trạo và bà Huyền, leo lên núi cao để tìm phong lan.
Mãnh hơn Nhan Bình một tuổi nên ra dáng đàn anh. Nhưng Mãnh không đi học, không thể học được chữ nào. Mãnh nói ít, lầm lì như kẻ tự kỷ. Mãnh khi mới sinh ra cái đầu đã bị dẹt, đôi mắt ngơ ngác, thân thể dài ngoẵng. Mặc dù vậy, trí não của cậu chứa đầy sức mạnh của một chàng trai xứ núi.
Bác sĩ bảo trong máu của ông Trạo vẫn còn ngấm chất độc ngày ông ở trong chiến trường. Vì thế nên hai ông bà chỉ dám sinh con một lần.
Nhan Bình sinh ra lớn lên ở làng Nhan, nhưng chí khí của cô không chỉ dừng lại bên chân núi Sót. Cô được trời phú cho nhan sắc, lại được cả trí tuệ. Đám trai làng không anh chàng nào học vượt được Nhan Bình. Thậm chí trai huyện cũng thua Nhan Bình cả bậc dài. Cô không chơi quá thân với ai. Gặp ai cũng lễ độ và hồ hởi. Sẵn sàng giúp bạn bè những gì có thể. Nhưng không ai đọc được ý nghĩ của Nhan Bình.
Vậy mà Mãnh lại có thể đi cùng cô, lúc lên rừng, lúc xuống núi. Buổi sớm, Mãnh thường gánh hộ Nhan Bình gánh nước giếng hồ về đổ vào bể. Biết Mãnh bị thiểu năng, bà Thảo Lăm không cấm cậu ta lui tới với con gái mình. Ngay cả anh trai Nhan Bình, một chàng trai khôi ngô nhất xứ núi bấy giờ cũng rất thích cặp kè với Mãnh, dù hơn Mãnh tới ba tuổi.
Nhan Bình mê phong lan liễu. Loài phong lan này thường mọc trên những mỏm đá tai mèo. Cánh lá nhỏ như ngón tay con gái. Hoa màu hồng nhạt, điểm xuyết cánh ren tím. Mãnh thường leo núi tìm bằng được loài phong lan này mang về cho Nhan Bình.
Nhưng rồi hai anh em Nhan Bình vẫn ra đi. Họ đi lên thành phố, nơi mịt mù không có gió núi thổi tới những hạt sương tinh khiết mỗi khi trăng lên. Nơi nhộn nhịp đời sống công nghệ hiện đại. Nơi họ dần quen với những bụi xăng và nhà cao tầng, những mái đầu xù, kem dưỡng da, những bar đêm, sàn nhảy, những lớp học ban đêm học, những địa điểm tu tập chơi bời... và còn nhiều thứ khác mà kẻ ở núi không thể không tò mò.
Nhanh chóng, Nhan Bình được thế giới của đô thị đón nhận vào lòng, như thể cô chính là thành viên của nó. Cô mang dáng nét uyển chuyển của bà tổ Dong, nét lạnh lùng và vững chãi của dãy núi Sót. Cô khiến cho nhiều chàng trai không thể không đi theo cô, khi họ nhìn thấy vẻ đẹp như một nàng hồ ly còn sót lại trên thế gian.
Mãnh trở nên lì lợm hơn. Thường xuyên lên núi một mình tìm hoa phong lan. Anh mang về treo trên những cành cây trong vườn. Những giò phong lan cũng kéo ong về đầy vườn mỗi khi có mùa hoa rộ.
*
*    *
Cuối năm, những vạt rừng đổ lá vàng. Đây đó lác đác những cánh lá đỏ, như bài hát của một nhạc sĩ đã tả cái màu đỏ hiếm hoi của lá rừng thời chiến tranh.
Mùa đông bắt đầu chuẩn bị trút hết những cơn gió lạnh trả về núi. Trên những sườn núi cao thấp thoáng những nụ hoa đào hoa mận khiến người lên núi trong lòng bâng khuâng. Có vẻ năm nay hoa sẽ rất đẹp. Rất nhiều nụ chi chít trên những cành cây. Đứng từ dưới thung nhìn lên, lòng thấy phấn chấn.
Mãnh đi sang nhà Liêu dặn:
“Tôi đi lên núi đây. Tôi sẽ mang về những cành hoa thật đẹp để cho mấy nhà chúng ta, nếu có hơn sẽ đưa ra thị trấn bán. Tôi nhờ Liêu trông mẹ giúp vài ngày”.
Bà Huyền nằm ho trên giường. Cả tháng nay ông Trạo đã được đón đi chữa trị những căn bệnh lạ do chất độc ngấm trong cơ thể. Mãnh đi ra đi vào tần ngần. Nhưng bà Huyền xua tay bảo Mãnh cứ lên núi kiếm cho bà cành đào thật đẹp. Tết sắp đến. Bà muốn đón ông Trạo về với cành đào rực rỡ trong nhà.
Liêu chạy theo dúi cho Mãnh gói xôi cô đồ trên chõ lũa, chuẩn bị sáng nay lên nương:
“Anh yên tâm. Liêu sẽ nấu cháo cho mẹ, xong rồi mới đi nương”
Liêu là chị em hàng xóm với Nhan Bình. Liêu âm thầm lớn lên trong làng Nhan. Hàng ngày cô lên nương trồng sắn, âm thầm thương Mãnh. Chỉ có đất làng Nhan biết cô vẫn chờ Mãnh, chờ anh qua cơn bạo bệnh nhớ Nhan Bình.
Nước mắt Liêu thấm xuống đất làng Nhan.
Mãnh leo lên núi.
Những bậc đá hoang không chân người giẫm lên bao nhiêu thời gian giờ được bước chân chàng trai làng Nhan đạp lên. Cỏ như được hồi sinh. Dường như nếu không có người dẫm lên, cỏ sẽ tàn úa âm thầm. Bước chân Mãnh đi thật khéo léo mạnh mẽ, như con nai con hoẵng. Phía trên vạt núi cao, trên những mỏm đá tai mèo, có một vạt rừng xanh thấp thoáng trong mây. Những nụ đào đang tỏa sắc hồng dưới những tia nắng cuối đông lạnh lẽo.
Mãnh đeo nải chứa đồ ăn và bình nước. Trong nải còn có lá thư Nhan Bình gửi về, tối qua lão Khang người buôn dưới xuôi lên mang đến tận nhà. Lão Khang đưa thư cho Mãnh, mắt nhìn ngang ngửa xem trên giàn bếp có xâu thịt trâu khô nào để tiện tay bốc về nhắm rượu lá lam chiều.
Mãnh tay run run cầm lá thư.
 

Ảnh: Phạm Duy Tuấn
 
Thư viết:
“Chào Mãnh, tôi phải đi lấy chồng rồi. Tôi dính hắn như cách Mãnh ngấm rượu lá lam chiều…”
Bỗng nhiên Mãnh nhớ đến hình ảnh cô bé Nhan Bình năm nào trong làng. Có thể giờ đây Nhan Bình không còn nhớ Mãnh của ngày xưa nữa. Cô đã là người thành phố, uống nước nơi xa lạ, nơi mà Mãnh chưa một lần biết đến.
Hồi Nhan Bình về làng chơi rồi lại đi, khi cô đi khuất, Mãnh mở chum lôi ra chai rượu lá lam chiều. Ngửa cổ tu ực mấy ngụm. Một lúc sau thấy trong người rạo rực. Mãnh nhìn thấy Liêu đi từ ngoài cổng vào, nom giống Nhan Bình. Mà đúng là Nhan Bình đang trở lại với Mãnh. Cô ngồi thụp xuống bên cạnh Mãnh, lấy tay đỡ Mãnh đứng lên. Rồi không hiểu bằng cách nào ngực Nhan Bình chạm vào ngực Mãnh tê dại. Mãnh ngả người ôm ngang lưng Nhan Bình, rồi đặt lên môi cô nụ hôn đầu đời…
Tỉnh dậy, Mãnh nhìn thấy Liêu.
Mãnh hỏi:
Liêu sang khi nào?
Liêu mặt đỏ ửng:
Khi anh mới uống say, nằm dưới đất.
Nhan Bình đâu? Tôi thấy Nhan Bình về mà.
Liêu lắc đầu:
Chị ấy có về đâu. Là em.
Mãnh tỉnh hẳn:
Khi nãy là anh nhìn thấy Nhan Bình. Liêu… đừng giận nhé.
Liêu gật. Mặt Liêu từ đỏ lựng chuyển sang tái nhợt. Đôi môi vừa mới đây như nụ hoa đào, giờ héo rũ như tàu khoai.
Cô chạy về nhà, dội nước giếng hồ lên tóc lên vai, rồi nước ngấm khắp thân thể con gái của Liêu. Cô òa khóc. Nước mát làm mịn màng những nơi thô ráp.
Từ hôm đó, Liêu âm thầm đi bên cạnh Mãnh, nhưng cô không hé răng nói điều gì về cái nụ hôn hôm Mãnh say rượu nhớ Nhan Bình.
Mãnh nhìn thấy một chùm phong lan liễu bên mép đá tai mèo. Anh đặt mấy cành đào đã chặt xuống. Những mỏm đá tai mèo trơn láng dưới những hạt bụi sương phủ mờ trên núi. Mãnh cố với chùm phong lan.
Nhưng đá trơn khiến anh trượt chân lăn xuống vực.
Lá thư Nhan Bình gửi Mãnh rơi ra, bay bay như cánh chim cô độc…
Mãnh tỉnh dậy trong một hang đá.
Anh cố gắng định thần nhớ lại. Không hiểu đang ở đâu. Cái ngách hang tối mờ mờ. Cả người đau vì bị bầm dập. Nhìn ra cửa hang, Mãnh thấy mấy cành đào anh đã chặt được xếp gọn gàng một bên cùng con dao đi núi. Nhìn sâu vào trong hang, là một khoanh rộng xếp đá tảng rất đẹp làm thành một cái giường lớn, trên cái giường đó là những lớp lá khô được rải như lớp chiếu. Một bức đá mỏng đặt ngay ngắn cuối chân giường. Đá như bức phù điêu che cho chiếc giường ấm cúng và kín đáo.
Lúc đó Mãnh mới nhận ra mình đang nằm giữa một đống lá tươi, gần một cái bếp bắc bằng đá, xung quanh anh là đệm lá ấm áp, loại lá mà loài thú rừng thường nhai để đắp vết thương. Những vết bầm dập trên đùi và ngực, dường như có ai đó đã nhai và đắp lá lên.
Nằm trong trạng thái vô thức và đau nhức khắp người, Mãnh thiếp đi trong giấc ngủ sâu.
Trong cơn mơ ngủ, Mãnh nhìn thấy một con khỉ cái đuôi đỏ có đôi mắt khá đẹp đang nướng một củ mài trên bếp lửa.
Dường như lại có khúc nhạc đá đang tràn ngập lòng hang sâu.
Không thơm như hoa, không cao như cây.
Tôi chỉ là nhánh cỏ…
*
*    *
Khi Mãnh về đến làng thì mùa xuân cũng về tới. Anh không ngọng nghịu như mọi khi nữa mà đã trở nên một chàng trai mạnh mẽ, phong trần. Anh không kể những ngày qua mình đi đâu, làm gì. Bà Huyền ông Trạo mổ con lợn lửng mời cả xóm đến ăn mừng sự kiện con trai mất tích đã trở về, lại còn đẹp đẽ sáng láng hơn xưa.
Mãnh lấy chiếc nhẫn bằng đá mà anh tự mài từ một mẩu đá thạch anh đỏ ra đeo vào tay Liêu.
Liêu là con gái làng Nhan. Cô không đẹp như Nhan Bình. Không sánh được với bà Thảo bà Huyền thuở còn trẻ. Cô là hậu duệ kém may mắn của bà tổ Dong. Nhưng nếu có một ngày, có một đứa trẻ được đặt ở hiên nhà, Liêu sẽ ôm nó vào lòng và ủ ấm cho nó.
Rừng bí ẩn và xanh bạt ngàn.
Những tiếng lao xao như tiếng của ngàn xanh đẩy gió trải khắp thung dài…
 
2013
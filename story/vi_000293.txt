Thuyền ai thấp thỏm muốn ôm cầm
(Hàn Mặc Tử)
 
.KIỀU BÍCH HẬU
 
Làng có tên là Nguyệt. Nếu đánh đu lên một đám mây bay qua, bạn sẽ phát hiện làng là một vành trăng khuyết. Chỗ hõm vào ấy là đầm Dạ. Đầm Dạ rộng hơn mười lăm sào, thuộc một gia đình giàu bậc nhất làng – gia đình ông Du.
Nhà ông Du giàu không phải vì ba mùa cắm mặt xuống ruộng.
Ông buôn long nhãn và tim sen. Ông là người có quyền lực nhất làng. Một thứ quyền lực ẩn sau mặt tiền. Dĩ nhiên ông cưới cô Diệp, một đám kháu nhất làng. Cô Diệp sòn sòn sinh hạ cho ông ba con: hai trai, một gái. Duy có đứa gái út, tên Dạ Liên, khốn thay lại không được bằng người.
Người làng bảo: Ông trời không bao giờ cho ai tất cả. Ông ấy đặt Dạ Liên vào nhà ông Du, để san bớt những thứ ông Du được quá nhiều.
Dạ Liên vẫn lớn lên, vào tuổi mười lăm thân thể cân đối, cao quý nõn nà.
Ông Du rất hay đi khỏi làng. Nhưng không hẳn lúc nào cũng là đi buôn.
Người làng bảo: Ông Du tham đáo để. Cưới cô vợ đẹp thế rồi mà vẫn mê gái bên ngoài. Mà đó là gái phong sương. Ông phải lòng một nữ sĩ đàn nguyệt, cô này đẹp, lẳng, chuyên dùng ngón đàn cùng hương sắc phóng túng để chài đàn ông.
Ông Du là con buôn, mánh khóe xảo quyệt trên đời không thiếu, nhưng ông mất khối tiền và lao tâm khổ tứ vì cô gái chơi đàn nguyệt kia. Chỉ vì ông mê cô.
Cô theo dàn nhạc dân tộc của cô vung vẩy khắp nơi. Hễ có dịp là ông Du bám theo cô. Van vỉ, lạy lục, say mê và bao bọc.
Cô cho ông những lời cay nghiệt, nước mắt, những cuốn sách viết về tình yêu cay đắng mà cao cả. Đôi khi cô cũng cho ông cả thân thể cô.
Có đêm, nằm xoay lưng lại vợ mình, ông mơ thấy miệng mình đầy những cô. Ông co rúm người, giật lên. Tỉnh dậy ông hụt hơi, trống hoang.
Làng Nguyệt như con vịt gày xụi lơ xấu xí. Vì làng nghèo nhấp nhô. Dân làng ai cũng muốn ra ngoài làm ăn như ông Du, nhưng lạ thay họ cứ thất bại. Đa phần lũ thanh niên nhấp nhổm bỏ làng.
Làng nghèo đến cây cối cũng rũ buồn. Hầu như quanh năm suốt tháng không khí trong làng loãng tuệch chán nản. Và người làng chẳng còn điều gì vui thú hơn là thêu dệt những câu chuyện về gia đình ông Du. Mỗi người muốn mở tới bốn con mắt ngày đêm theo dõi động tĩnh của gia đình lạ lùng nhất làng này.
Người ta cũng khoái nói chuyện Dạ Liên. Không phải vì nàng xinh đẹp, mà vì nàng là sự thất bại của ông Du.
Đám trai làng thì khấp khởi. Họ hy vọng. Người nào được ông Du chấm gả cho Dạ Liên, hẳn sẽ được cả đống vàng. Chắc chắn ông sẽ phải dùng vàng để bù cho khiếm khuyết của Dạ Liên. 
*
*    *

Minh họa: Thành Chương
Cô gái chơi đàn nguyệt kia chết trẻ. Kỷ vật mà cô để lại cho ông Du là cây nguyệt cầm khảm trai duyên dáng và một hòm tôn những sách diễm tình. Hình như những trang sách viết về những cuộc tình thất bại đã thấm nước mắt cô.
Ông Du ôm nguyệt cầm về quê. Từ đó ông ở lì trong làng, không đi đâu nữa. Đi buôn cũng không. Hết đam mê u muội rồi. Ông đã yêu tiền như yêu người tình vậy. Say mê, thấp thỏm đợi chờ rồi thất vọng.
Nhưng của nả của ông thì còn đầy. Ông đem giao hết cho bà cai quản. Đó là một sai lầm của đàn ông!
Bà Du trở nên quyền thế, nghiễm nhiên cắt đặt mọi việc trong nhà, trong làng. Ông mặc kệ.
Ông chỉ quan tâm đến Dạ Liên và cái đầm sen bìa làng.
Dù không buôn tim sen nữa, nhưng ông Du vẫn nuôi đầm sen. Mỗi mùa sen, đầm Dạ trở thành đôi cánh tiên xanh xinh đẹp của làng. Đầm bát ngát sen xanh, làm cả không gian trở nên dịu dàng, thanh thoát. Hàng vạn lá sen xanh ngắt ngửa mặt hứng nắng. Lớp nhung trắng mịn phủ trên mặt lá lọc nắng cứ óng ánh li ti hư ảo tuyệt đẹp. Hương sen thơm mát bao phủ làm người ta yên lòng hơn.
Ông Du chẳng còn việc gì ý nghĩa hơn là nguyện làm người đưa đường cho con gái ông.
Hay ông cũng đang muốn tìm đường cho tâm can ông vậy.
Ông có thể ngỡ rằng mình đang sống.
Mà sống rất khác.
Ông đi trước, miệng rầm rì kể chuyện. Dạ Liên tay trái bám vai cha, lần theo. Tháng năm âm, mùa sen, hai cha con cứ ngày ngày từ nhà ra đầm sen, từ đầm sen trở về nhà.
Có vẻ như mùa sen nào cũng đến như nhau. Chỉ có Dạ Liên đổi khác sau mỗi mùa sen. Tóc dài đen mướt mải, càng nảy nở gợi tình thì càng dằn vặt xốn xang.
*
*    *
Để bớt xốn xang, nàng được cha dạy chơi nguyệt cầm.
Đó quả là thứ đàn chúa. Cho âm thanh tròn như trăng rằm.
Nàng lập tức mê nguyệt cầm. Khi chơi nguyệt cầm nàng mở được lối cho mình. Cha bảo nàng nuôi móng tay dài để khảy dây tơ. Dây đại được se bằng mười hai sợi tơ nõn. Âm thanh biến ảo phi thường. Với cây đàn này, Dạ Liên đã yêu thực sự vì đàn thiêng như thấm tâm hồn riêng của nó. Nàng có thể lên dây tơ tùy theo tâm trạng của mình để có tiếng đàn phiêu linh với những âm ngang tàng, kiêu hãnh, hoặc trong vang rộn ràng, hay nỉ non sâu nặng, trầm đục u buồn…
Nhưng bà Du phản đối. Thậm chí căm ghét. Thứ quân tử cầm này đâu phải dành cho đàn bà.
 Âm thanh của nó khiến bà nhớ lại những dày vò bần tiện mỗi lần thấy ông Du khăn gói ra đi. Bà ghen tức lồng lộn. Ghen đến tận cùng. Cơn ghen bay thẳng đến cõi chết.
Nhưng ông đã canh chừng rất kỹ, không để bà hại cây đàn. Ông cũng không để Dạ Liên chơi đàn khi ở nhà.
Nơi nào không khí không trong sạch, không đàn. 
Ông tức tốc gọi Oa Ngưu đến cùng với bốn trai làng khỏe trẻ, chọn tre già ngâm kỹ dựng một cái chòi ven đầm Dạ.
Vào những đêm trăng thật sáng, thật trong, hai cha con mang nguyệt cầm ra chòi chơi cho nhau nghe. Vào tháng năm âm, khi không khí trong làng Nguyệt thơm ngát hương sen thì họ chơi đàn cả đêm. Người cha ngồi trong chòi, ngất ngư với hũ rượu mà Oa Ngưu mang đến. Đó là thứ rượu ngon và thơm nhất làng. Oa Ngưu sinh ra để nấu rượu ngon. Nàng con gái tắm dưới đầm sen xong, quấn lụa bạch ngồi đàn. Những ngón búp sen vẩy trên dây tơ, làm trào lên dòng suối âm thanh giòn giã hoan hỉ. Và rằng thường hoa sen “ẩn hiện tùy nghi diệu”, thế mà khi tiếng đàn trong vắt vang lên thì muôn ngàn cánh hoa bỗng nở ra rộn ràng, và hương sen dịu dàng lan tỏa khắp không gian, ướp ánh trăng trên mặt đầm trong mùi thơm thanh tịnh. Chỉ duy nhất ở đây, trên mặt đầm sen, trong tiếng đàn nguyệt kiêu hãnh, ánh trăng có hương thơm thanh khiết nhường ấy.
*
*    *
Nhưng bà Du không muốn để cha con họ hưởng an lạc.
Đá thúng đụng nia, nồi đổ niêu vỡ, rỉa thịt róc xương.
Cũng vẫn là tầm thường. 
Bà có thể hành ông đến thế nào cũng được nhưng không được động đến Dạ Liên và nguyệt cầm. Giờ thì ít khi nàng rời cây đàn. Đàn bà nanh ác thì làm được cái gì? Bà còn muốn gì nữa, khi đã thâu tóm được tiền và quyền hành trong tay?
Người như ông Du khinh miệt thứ ấy. Một ngày kia ông đi lặng lẽ. Ông trăng trối để lại cho Dạ Liên cây đàn nguyệt, hòm tôn đầy sách và đầm sen. 
Một mình nó cả cái đầm sen ư? Hai người anh của nàng nhảy dựng lên. Cái đầm đẻ ra tiền đấy. 
Tất cả đất đai, của chìm của nổi còn lại do bà Du định đoạt.
Ông dọa quay về bóp cổ đứa nào tranh giành đầm sen của con gái.
Thôi cũng xong!
Người làng bảo: Ông chết là vì đêm nào cô gái phong sương kia cũng hiện về gọi ông đi. Ông khôn ngoan dường ấy mà sống trên đời không biết tránh xa người đàn bà có mùi quyến rũ. Đó là tướng đàn bà làm tiêu tán đại gia.
Oa Ngưu trở thành bờ vai cho tay nàng bám lấy.
Hôm nào Oa Ngưu bận hoặc say khướt, thì nàng có thể tự mình ra đầm sen. Dù không có cha nữa, thì nàng cũng không vấp ngã trên đường. Lối đi đã nằm trong lòng nàng. Nhưng dù sao có Oa Ngưu đi cùng thì vẫn hơn. 
Cái gã đến lạ!
Nàng chưa biết mặt mũi gã nom ra sao. Nàng chỉ biết gã có bờ vai rắn như đá, nhưng lại nóng ấm. Bờ vai bên phải cho tay trái nàng bám vào. Và mỗi khi, gã nắm tay nàng để kéo qua một đoạn bờ ruộng gãy, thì trong lòng bàn tay gã, tỏa ra một ngọn lửa mơ hồ. Nàng khá tò mò, muốn miết những ngón tay nàng lên chỗ ấy, để cảm thấu hơi lửa mê hoặc. Nó cứ chập chờn. Như có như ẩn. Dụ dỗ ngón tay nàng miết tới. Mê hoặc.
Bà Du thấy Oa Ngưu là muốn đào đất đổ đi. Có thời, Oa Ngưu về ở hẳn nhà bà gần hai tháng để trông nom thợ làm long nhãn và sao tim sen. Oa Ngưu có thể làm long nhãn tròn xoay và trong màu hổ phách, còn tim sen sao xong mùi hương như ru ngủ. Nhưng những chiếc quần tí xíu may bằng ren đỏ rực của Dạ Liên cứ biến mất dần. Thế là bà đuổi Oa Ngưu về nhà. Nhưng cứ có việc, thì ông Du lại gọi Oa Ngưu đến. 
Vì Oa Ngưu là cháu họ.
Chị con bác ruột ông Du sống độc thân. Một đêm bà đương ngủ thì một con ốc sên bò lên người. Sợ quá, bà bất tỉnh. Ốc sên thừa cơ chui vào chỗ ấy. Các đêm sau người đàn bà hay mơ thấy ốc sên, rồi có thai, đẻ ra thằng con trai rớt bèo nhèo bèn đặt tên là Oa Ngưu. Oa Ngưu hay sang nhà cậu Du chơi.
*
*     *
Bà Du đã cắt đặt xong đâu vào đấy cả. Bà sẽ gả Dạ Liên cho đám con trai ông Tam Đẫn. Các thêm cái đầm sen. Vì Dạ Liên khiếm khuyết, chẳng được bằng người. Cho hẳn một cái đầm sen chứ không cho vàng. Tuyệt nhiên không một mảy vàng. Đám nhà Tam Đẫn cũng đồng ý. Thông gia với nhà giàu dựa hơi thôi cũng vững.
Nhớ rằng, một lần, sau mùa sen, người cha đặt dưới bàn tay Dạ Liên hai tấm lụa quý. Nàng nắm nhẹ lớp lụa mềm như nước dưới ngón tay:
- Màu trắng hả cha?
- Ừ!
- Màu đỏ nữa, cha?
- Ừ. Sao con bé nhạy màu đỏ thế nhỉ? Người cha tự hỏi.
Nhạy màu cho lắm thì cũng chỉ thêm dằn vặt mà thôi.
- Con thích đứa nào ở làng này không? - Cha hỏi.
- Con không thích! - Nàng đáp gọn lỏn.
- Ừ, chưa vội con ạ. Chỉ một khắc đam mê thôi cũng giá trị hơn tất cả những gì còn lại của ta - Người cha tiếc nuối.
Dạ Liên bảo không lấy đám con trai nhà Tam Đẫn. Bà Du liền ép. Bà không nói ra, nhưng nàng phải hiểu rằng có người lành lặn chịu lấy nàng là may cho nàng lắm lắm. Nhưng bà không thể hiểu, rằng nàng con gái của bà, tuy khiếm khuyết mà lại kiêu hãnh, kiêu hãnh quá đỗi.
Đó là nỗi khổ của nàng!
Mặc cho nàng phản đối kịch liệt, đám cưới ập đến. Người ta bắt nàng mặc áo lạ. Nàng chạy ra đường cái, xé toang, vứt đi. Người làng tò mò, hoảng hốt trước sự thể chưa bao giờ có. Phi lý thật. Oa Ngưu cũng ở đó, gã chói mắt trước màu đỏ rực tí xíu.
Tương lai chấm dứt!
Chó nó lấy thứ đàn bà con gái xé toạc quần áo, toang hoang phơi ra trước mắt bàn dân thiên hạ. Bà Du bầm gan tím ruột!
*
*    *
Điên rồ hơn, cứ mùa sen là Dạ Liên bỏ nhà ra đầm sen ở.
“Nó đi trông sen đấy”. Bà Du tự dối mình như thế. Bà cũng đành để Oa Ngưu canh chừng nàng. Thôi đành trông vào nó vậy, chứ còn biết làm sao. 
Nhưng khắp vùng đã lan đi những lời đồn đại về những đêm trăng mùa sen, với thiếu nữ một mình trông sen, vận lụa bạch chơi nguyệt cầm làm cá vờn hoa múa trong đêm. Thậm chí cả những thây ma quanh đó cũng đội mồ thức dậy, rạo rực nhảy múa cùng thần tiên bay xuống từ trời.
Làng Nguyệt, mùa sen, đêm trăng, Dạ Liên với đàn nguyệt phiêu linh, ngang tàng mà dây tơ thật mong manh. 
Người làng bảo: Đúng đêm rằm tháng năm âm, mùa sen đương độ chín, Dạ Liên hiến thân cho thần linh.
Trước đó, nàng câm nín cả tuần. Miệng ngậm cơm sen. Hơi thở nàng nhẹ, thơm ngát chẳng hương hoa nào sánh kịp.
Trăng lên cao dần, trong đến độ tinh khiết, nhìn thấu cả những vân lá sen. Nàng lội xuống đầm tắm rồi bước lên chòi, khoác tấm lụa bạch, mang cây đàn nguyệt lên thuyền. Sen nhẹ nhàng uốn mình, nép vào nhau rẽ lối cho thuyền trôi ra giữa đầm. Bàn tay nàng lần lên đầu đàn hình lá đề, có khảm trai những bông sen và một góc tay áo lụa phất qua. Nàng tì miết ngón tay trên hai sợi tơ mong manh trên khung đàn dài. Đê mê trào dâng. Tiếng đàn vút lên, lan tỏa lung linh mặt nước. Mỗi bông sen thức tỉnh, bỗng hóa thành một vũ nữ, váy trắng váy hồng lớp lớp rộn ràng trong một vũ điệu hoa sen kì bí, lộng lẫy và thơm ngát cả vùng.
Nàng ghì miết ghì miết dây đàn. Tiếng đàn kiêu hãnh, trong đến độ có thể vỡ ra. Cây đàn uốn mình. Một bờ vai đàn ông cứng rắn. Cần đàn nóng rực, khát khao bốc cháy. Tấm lụa bạch bay lên, thoát ra chớp màu đỏ rực nhỏ xíu. Thuyền nan dập dềnh, dập dềnh trên khúc hoan ca của sen. Sen nín thở đợi chờ khắc thăng hoa cực lạc. Nàng lặng đi, rồi thiêm thiếp bên cây đàn.
Sáng ra, trên lá sen xanh, là óng ánh một lớp mỏng tang thủy tinh pha ngọc trai. Như có ngàn con ốc sên vừa trườn qua.
Nhớ rằng, làng Nguyệt vào mùa sen bỗng đẹp lung linh. Là vì trước đó, khi mùa đông vừa kịp hết, thì Oa Ngưu theo yêu cầu điên rồ của nàng đã mang hạt sen, củ sen ném và dâm thẳng xuống bùn ở tất cả các ao làng Nguyệt, bất kể ao đó của ai.
Làng Nguyệt lúc ấy ao nào cũng đầy sen.
Nhớ rằng, một lần người cha đặt một nắm hạt sen già dưới bàn tay con gái. Tay nàng chạm khẽ lên hạt sen cứng. Những ngón dài trắng muốt như ngó sen. Có cảm giác tim sen đang cựa nhẹ trong vỏ cứng.
Người cha bảo:
- Đây là những hạt sen giống, cha dành cho con. Tim sen là bất diệt! Gặp nhân duyên thì dù đã lăn lóc qua cả trăm năm sau, những hạt sen dưới bàn tay con ấy vẫn có thể nảy mầm. Nếu gặp ác nghiệp, mầm sẽ mãi ngủ im trong vỏ cứng, không thể phá vỡ vỏ mà thành cây hoa.
Thế rồi sen trở thành vị cứu tinh của làng Nguyệt. Ao nào cũng đầy sen. Dù không làm cho dân làng giàu, nhưng qua mỗi mùa sen, chòm xóm có đồng ra đồng vào. Mùi cơm mùi mắm ấm khí quanh làng. Đó quả là những ao sen báu.
*
*    *
Dự án đường cao tốc chạy sát bìa làng. Dân làng khấp khởi. Đổi đời! Một số nhà phải dọn đi, một số ruộng không cần phải cấy hái nữa. Nhường đất cho đường. Đất sẽ biến thành tiền. Một đống tiền. Dành dụm cả đời cũng không có một lúc nhiều tiền như thế!
Tiền đấy. Ôm tiền và dọn đi. 
Nhưng Dạ Liên phản đối. Nàng không chịu nhường đầm sen cho đường cao tốc. Dân làng sẽ đổi đời cơ mà. Đồ điên.
Cưỡng đoạt!
Nàng giật tung tấm lụa bạch. Đốm đỏ rực nhức nhối. Nhưng đám người đứng trơ ra, cười cợt, bình phẩm.
Oa Ngưu cay đắng quấn nàng vào tấm lụa đỏ, giấu đi, mặc nàng vùng vẫy bất lực. Đám người lạ mới đến làng cũng cười cợt. Ừ. Họ đâu có phải mẹ nàng mà thua cái võ lột truồng phơi ra của nàng. 
Đầm sen tắt thở. Cát đổ ào xuống lấp kín huyền thoại làng Nguyệt. Xe cộ ầm ào gầm rú vút qua. Nhà cao tầng vọt lên với những cửa miệng há ngoác mời mọc đầy lè hàng hóa.
Một cao ốc sang trọng sẽ mọc lên ngay chỗ xưa là đầm Dạ.
Hàng ngàn tấn sắt, hàng ngàn tấn xi măng lao thẳng xuống lòng đầm, vùi lấp. Chưa bao giờ tim sen bị đè nặng và vùi sâu đến thế.
Dạ Liên biết. Đêm trước khi những chiếc xe với máy móc khổng lồ tiến đến, những hạt sen già đã kịp rời gương sen, rớt xuống lòng đầm.
Hình ảnh đó ám ảnh nàng. Đêm về, nàng thường thấy những củ sen, hạt sen bị tòa cao ốc nhấn chìm trong lòng đất, đã đi xuyên qua cả trái đất, gặp tiết trời ấm nắng đã mọc lên tốt tươi thơm ngát trong những đầm bao la ở bên kia trái đất.
Chỉ cần gặp nhân duyên, dù rất hiếm hoi là sen bừng nở.
*
*    *
Chưa phải là mùa sen, cũng không có trăng, đêm lạnh giá đầu xuân, vậy mà lại có tiếng đàn nguyệt. Tiếng đàn trầm đục, dày vò, nhức buốt.
Sáng ra khi tổ bảo vệ tòa cao ốc đang xây dựng đi kiểm tra thì thấy xác Dạ Liên quấn trong hai tấm lụa treo lơ lửng trên cọc giàn giáo. Màu trắng lẫn đỏ pha thật đẹp, nhìn xa như máu đỏ chảy trên tuyết.
Oa Ngưu bỏ làng đi, sau khi hóa cây nguyệt cầm và hòm sách của nàng. Gã mang theo những mảnh nhỏ ren đỏ rực, giấu kín trong ba lô.
Dân làng sống vội vã và lo lắng. Xe máy Tàu rồ lên trên đường làng đổ xi măng. Làng có những triệu phú tiền đồng và những tỷ phú thời gian. Cờ bạc. Bia cỏ ngập ngụa. Có cả đâm chém. Và cả chất gây nghiện.
Nhưng người ta vẫn nhớ đã cất nàng ở phía Tây của làng, cách xa đường cao tốc, cùng với những hạt sen đen nhức.
Tim sen ở đó.
Lại một mùa sen nữa. Những đám mây bụi từ công trường ngổn ngang bên kia nhuộm không khí làng Nguyệt thành một màu đùng đục buồn bã.
Làng nhớ Dạ Liên, nhớ tiếng Nguyệt cầm kiêu hãnh từng làm nảy lên vạn mầm sen trong làng. Bụi cứ lầm lũi phủ trùm lên cả làng, làm mờ dần hồi ức.
“Những gì đẹp đẽ thường qua nhanh”.
Làng Nguyệt. Mùi sen thơm ngát. Ánh trăng non run rẩy trong tiếng nguyệt cầm, dập dềnh vạn lá sen vẫy. Và thân thể nõn nường của Dạ Liên.
Nhớ tiếc thít chặt tim người.
Nàng đã một mình đi về.
Phía tim sen.
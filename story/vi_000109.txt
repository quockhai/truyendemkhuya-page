Có một điều ước nguyện chị vẫn giấu kín trong lòng là được một lần đến Cổ thành và sông Thạch Hãn ở Quảng Trị.

Năm 1972 chị đang là học sinh phổ thông, cuộc chiến vào giai đoạn khốc liệt nhất. Trong làng, con trai, con gái đều đã ra mặt trận. Những đứa trẻ như chị hồi ấy ngồi trên ghế nhà trường nhưng cũng chẳng thể vô tư. Máy bay giặc quần đảo trên bầu trời. Trong bản tin thời sự của Đài Tiếng nói Việt Nam cùng với những tin thắng trận ở ngoài mặt trận là tin bom nổ, người chết. Người chết đa phần là đàn bà, người già và trẻ con. Hầu như đứa trẻ nào trong làng cũng có cha, anh hoặc chị đang ở ngoài mặt trận. Ngôi làng của chị thường là nơi tập kết của các đơn vị bộ đội trên đường vào chiến trường. Buổi chiều và buổi tối, những người còn lại trong làng có ối việc để làm. Họ làm bất cứ việc gì miễn là có ích cho người ra trận.

Một lần, trong lúc cài lá nguỵ trang lên ba lô giúp anh bộ đội trẻ, chị nhìn thấy con búp bê bằng len nhỏ xíu, màu vàng tươi như hoa cúc. Bắt gặp ánh mắt hết sức tò mò của chị, anh ấy mỉm cười: “Đấy là lời chúc may mắn thôi. Bé ạ!”. “Anh tin thế chứ?”. Anh cười thật tươi: “Tất nhiên rồi!”. “Này, tên em là gì?”. “Là Nhiên! Bọn bạn gọi em là Nhiên làng Bẹo”. “Làng Bẹo. Hay đấy chứ!”. Chị hít một hơi thật sâu, rồi nói khẽ: “Em cũng muốn chúc các anh như thế!”. “Chúc gì cơ?”. “Chúc may mắn! Nhưng em không có búp bê... Anh cho em mượn để làm mẫu có được không?”. Anh bộ đội sốt sắng: “Được chứ!”. Chị cầm vội lấy con búp bê: “Chờ em nha!”.

Ngay lúc ấy, chị đã chạy vội về nhà. Chị có chiếc áo len màu vàng là quà tặng của bố gửi trước lúc từ cơ quan ra chiến trường; thứ mà chị coi như báu vật. Chị cắt rời một ống tay áo, tháo những sợi len và tết một con búp bê. Con búp bê không đẹp nhưng chị nghĩ, sẽ không sao. Con búp bê chị làm bằng rất nhiều sợi len để cầu mong cho các anh bộ đội có thật nhiều may mắn. Lúc ấy mới khoảng chín hay mười giờ tối. Chị nghĩ, nếu làm từ giờ đến gần sáng sẽ được ít nhất là chục con búp bê như thế này. Như vậy sẽ có cơ hội cầu chúc may mắn cho mấy người nữa. Nghĩ thế, chị mải miết buộc và cắt tỉa những sợi len. Đêm đã ngả về sáng. Chị chạy vội đến nơi đơn vị bộ đội tập kết, nhưng chung quanh vắng lặng. Sân đình chỉ còn lác đác vài cành lá nguỵ trang rơi. Dưới bóng tre không thấy chiếc ô tô nào nữa. Chị chạy theo ra đầu làng. Con đường lặng lẽ đắm mình dưới ánh trăng bàng bạc. Không thể kìm nổi nỗi thất vọng, chị ngồi thụp xuống vệ đường ôm mặt khóc tức tưởi như bị người thân bỏ quên.

Trời sáng rõ, chị ôm những con búp bê thất thểu quay về bằng lối tắt qua những khu vườn, cố tránh không để ai nhìn thấy. Rồi không thể chậm trễ, chị tháo hết sợi len của chiếc áo, hối hả làm những con búp bê nho nhỏ. Chờ khi có đơn vị bộ đội qua làng, chị đem tặng các anh bộ đội để cầu chúc những điều may mắn. Chị giữ lại con búp bê mượn của anh bộ đội hôm nào với bao điều ân hận. Chừng hơn hai tháng sau, đi học về, mẹ đưa cho chị một chiếc phong bì thư kèm theo cái nhìn đầy sự nghi ngờ. Chị ngồi phịch xuống bậc cửa. Con tim nhảy loạn lên trong lồng ngực mới chớm tuổi thiếu nữ làm chị muốn nghẹn thở. Chị chưa từng nhận được thư của ai gửi cho. Trên bì thư không có tên người gửi. Tên và địa chỉ người nhận cũng chẳng giống ai: Gửi bé Nhiên làng Bẹo. Xã... Huyện... Tỉnh... Chị run lập cập, mãi mới bóc được phong bì.

“Quảng Trị, ngày... tháng... năm 1972.

Đêm nay bọn tôi sẽ vượt sông sang chi viện cho đơn vị đang chiến đấu ở  Thành cổ. Cuộc chiến rất ác liệt. Bất chợt tôi nhớ đến em. Hôm đó tôi đã chờ em, nhưng lệnh xuất phát sớm. Làm được nhiều búp bê không? Đừng buồn nhé, bé con! Hãy tặng những điều may mắn cho những người vào sau chúng tôi. Còn tôi tặng em con búp bê may mắn đó!

Chúc học giỏi!

Thắng - Hải Phòng”.

Trang giấy chắc xé vội trong quyển sổ nào đấy chỉ từng ấy dòng chữ rất ít thông tin về người viết càng làm cho chị, cô bé mười bốn tuổi, day dứt không nguôi.

Qua đài, báo, chị hiểu cuộc chiến đấu 81 ngày đêm ở Cổ thành Quảng Trị vô cùng khốc liệt. Thế mà chị đã lấy mất con búp bê - lời chúc may mắn của anh bộ đội tên Thắng - Hải Phòng. 

Thời gian trôi. Phép nhiệm màu của thời gian chỉ làm dịu nỗi đau chứ không bao giờ xoá được. Cuộc sống vẫn là cuộc sống, cũng như bao nhiêu người khác, cuộc mưu sinh lầm lụi cuốn chị vào vòng quay của nó. Không hiểu sao chị không thể tạo được cuộc sống riêng cho mình.

Hơn ba mươi năm sau. Chị quyết định gác lại mọi công việc, lên tàu. Chị vào Thành cổ thắp hương rồi lang thang suốt buổi ở đó. Chị thầm thì những câu nói lộn xộn theo ý nghĩ nảy ra trong đầu. Buổi tối, chị tìm về xóm nhỏ trên bến Nhan Biều, theo lời kể của ông bố, nơi đây khi xưa đoàn quân nối nhau vượt sông sang chi viện cho đồng đội chiến đấu bên Thành cổ. Chị ra bờ sông, lặng lẽ đứng nhìn theo dòng nước cuồn cuộn trôi xuôi. Hình như đang là mùa lũ. Trong bóng nhập nhoạng từ ánh đèn điện thị xã hắt xuống, chị thấy dưới sông dập dềnh những người đang bơi từ phía bên kia về. Chị rùng mình, lạnh toát sống lưng. Nhìn kỹ thì đó chỉ là những đám bèo nhỏ bị nước cuốn. Chợt có tiếng đàn ông nói sát ngay bên cạnh:

 - “O à!”.

Chị khuỵu xuống. Một bàn tay nâng chị dậy. Đấy là một ông già.

- “O có người thân năm bảy hai chiến đấu ở đây ?”

- ...

- Dạo ấy tôi chở đò cho bộ đội qua sông nầy rồi lại chở thương binh ra. Đã có nhiều người về đây như o vậy.”.

Chị không nói gì, chỉ tay xuống dòng nước đang lấp loá trôi, nhấp nhô những người đang bơi cạnh những con thuyền. Chị nhìn rõ khẩu súng đặt ngang cái bọc ni lon trước mặt mỗi người. Chỉ nghe tiếng sóng vỗ ì oạp vào bờ cát. Ông già lắc đầu:

- Không có chi đâu o à? Chắc o mệt rồi! Tôi làm nghề đánh cá ở khúc sông nầy. Nhà tôi ở gần bến kia thôi. Ở nhà tôi còn có mụ già nữa. O đã có chỗ nghỉ chưa? Hay là về nghỉ với mụ nhà tôi. Mụ ấy xưa cũng chở bộ đội qua sông nên biết nhiều chuyện lắm! Nhưng tôi nói trước nha. Chuyện của mấy mụ thì tin vừa vừa thôi!”

Chị theo ông già về căn nhà bên bến sông. Bà già rất khó đoán tuổi, khuôn mặt đầy nếp nhăn nhưng giọng nói còn trong và dáng đi nhanh nhẹn. Một mâm cơm đạm bạc được dọn ra. Bà già xới một lưng cơm đưa ra thắp hương ở cái am nhỏ ngay trước sân nhà. Chị đứng sau lưng bà già lắng nghe bà lầm rầm khấn khứa điều gì đó mà không nghe rõ. Chị thắp một nén hương, chắp tay vái ba vái theo bà già. Ban đêm, ông già lại ra sông kiếm cá trên con thuyền nhỏ. Vắt chiếc áo lên vai, ông già còn nhắc chị:

- O tin lời mụ vừa vừa thôi nha!

Bà già như không nghe thấy lời ông già vừa nói. Bà thì thầm:

- Ông già không bao giờ tin cả dù ông cũng nhìn thấy mấy chú về bên ni. Nhất là trong những đêm rằm tháng bảy ta, đêm hai bảy tháng bảy tây và đêm ba mươi tháng tư. Tôi nhìn thấy mấy chú rõ lắm. Họ mới chỉ mười bảy, mười tám đôi mươi cả. Nhiều chú ngó trắng trẻo, thư sinh. Tội lắm! Có những đêm, tôi còn thấy mấy chú ngồi bên bờ ni vừa vỗ tay vừa hát. Nhiều chú băng trắng còn loang máu trên đầu, trên tay chân. Họ hát nhiều bài mình nghe cũng thấy náo nức như ngày xưa. Nhưng thi thoảng lại nghe tiếng đàn của ai đó phập phừng. Lúc ấy chú nào cũng ngồi cúi đầu lặng lẽ. Có hôm tôi còn nghe rõ cả tiếng rên đau đớn ở ngoài đó vọng về. Nghe não cả ruột. Mà không hiểu sao mấy chú chỉ về đến bờ bên ni thôi...

Tiếng bà già đều đều. Chị mệt mỏi gục đầu xuống mép chiếc bàn gỗ. Lúc sau ngẩng lên đã không thấy bà già đâu cả. Chắc bà đang dọn dẹp hay đã đi ngủ rồi. Chị nghe thấy tiếng bước chân nhè nhẹ ngay ngoài cửa, rõ ràng không phải bước chân của bà già. Chị khe khẽ nói với ra:

- Ai đấy?

Tiếng con trai giọng Bắc rất trẻ, rụt rè:

- Nhà có khách phải không ạ?

- Mời vào!

Không nghe tiếng động của bức mành mành đã thấy một anh bộ đội trẻ măng vẻ ngượng ngùng như con gái đứng ở chỗ tranh tối tranh sáng của ánh đèn. Chị kéo ghế:

- Mời anh ngồi!

Anh lính trẻ lúng túng:

- Dạ thôi ạ! Cháu đứng đây được rồi... Cô mới ở Bắc vào ạ?... Cô đi tìm người nhà phải không?...

Chị xoay hẳn người về phía anh lính. Anh lính lùi một chút để vẫn khuất nơi ánh đèn không chiếu tới. Chị không thể nhìn rõ mặt anh lính. Chị dụi mắt. Vẫn thế. Chắc chị đang còn ngái ngủ. Anh lính trẻ lại hỏi:

- Cô tìm con hay em mình ạ?

- À... Ừ!

Chị nghĩ, cậu lính này trẻ quá. Chắc là mới rời ghế nhà trường. Trông dáng vụng về thế kia thì biết. Vẫn giọng rụt rè, anh lính hỏi khẽ:

- Cô có sợ... ma không?...

- Tôi... chưa gặp ma bao giờ...

- Thì họ cũng như chúng cháu cả thôi... Nếu cô thật sự muốn tìm người thân hãy đi theo cháu!

- Mà sao cậu biết tôi đi tìm người thân?

Anh lính trẻ cúi đầu như cậu học trò không thuộc bài đứng trước cô giáo. Trông tồi tội:

- Khi chiều cháu và các anh em thấy cô ở Thành cổ. Đại đội trưởng Dũng cử cháu về đây. Họ muốn... gặp cô... Nếu cô vui lòng xin đi theo cháu...

Chị chợt thấy lành lạnh, với tay lấy cái áo, khoác thêm lên người và đi theo anh lính. Thoắt cái, chị đã thấy mình đứng trong Thành cổ. Sao mà hôm nay trời nhiều sương đến vậy! Sương trắng đục như sữa bảng lảng bay. Xung quanh chị có rất nhiều bộ đội. Ai cũng trẻ trung. Có nhiều tiếng gọi lao xao, tha thiết: “Mẹ ơi! Con đây! Con đây cơ mà!”. “Mẹ ơi! Người yêu của con ở nhà có còn chờ con không?”. “Con không thể về được đâu! Mẹ bảo cô ấy đi lấy chồng đi mẹ ơi!”. “Mẹ ơi! Con nhớ nhà quá!”. Tim chị thắt lại. Tất cả đều đứng lẫn trong sương, lãng đãng, lúc gần, lúc xa nhưng rất thân thiện. Một người có vẻ nhiều tuổi hơn trong số anh em, chị đoán là người mà cậu lính trẻ ban nãy gọi là đại đội trưởng Dũng, tiến đến cách chị một tầm nhìn. Vẫn không thể thấy rõ mặt anh lính. Anh đứng nghiêm, giơ tay chào:

- Tôi là thiếu uý Trần Dũng. Anh em gọi là Dũng đại trưởng. Xin cho biết, người cô đang tìm là ai? Quê quán ở đâu và phiên hiệu đơn vị của đồng chí đó?

Câu hỏi rất lính nhưng giọng nói nghe ấm áp làm chị thấy những người có mặt quanh đây thật gần gũi. Chị tự tin hơn nhưng lại lúng túng đáp:

- Giá như tôi biết hết được những điều anh vừa hỏi...

- Sao vậy? - Giọng người lính chùng xuống đầy vẻ cảm thông.

- Tôi chỉ biết tên anh ấy là Thắng. Thắng - Hải Phòng...

- Có thể có nhiều anh em quê Hải Phòng tên là Thắng...?

Chị cúi đầu:

- Trên đường hành quân ra mặt trận, đơn vị anh dừng chân nghỉ ở làng tôi. Tôi đã mượn lời chúc may mắn của anh mà không kịp trả...

Anh lính có tên Dũng đại trưởng khẽ reo:

- Thế là có cơ may rồi!

Anh quay lại hỏi đồng đội:

- Ở đây có ai đã từng cho mượn lời chúc may mắn trên đường ra trận không?

Có nhiều tiếng cười khúc khích. Dũng đại trưởng khoát tay ra hiệu cho đồng đội im lặng:

- Anh em, chúng ta hãy đi tìm xem... Cô yên tâm, ở đây anh em chúng tôi tìm nhau nhanh lắm! Nếu có người như thế, chắc chắn anh em sẽ tìm được!

Những người lính trẻ tản đi nhanh chóng. Dũng đại trưởng hỏi:

- Miền Bắc mình dạo này thế nào, cô?

- Thay đổi nhiều lắm rồi so với ngày các anh ra đi! Có rất nhiều khu công nghiệp mọc lên không chỉ ở miền Bắc mà trong cả nước. Nhiều vùng nông thôn giàu lên trông thấy. Không còn đói nghèo như xưa. Ở thành phố, thị xã có nhiều gia đình giầu có, sống sung sướng như Tây...

- Như Tây?!... - Dường như Dũng đại trưởng nhăn mặt.

Chị thấy mình vừa lỡ lời.

- Thế còn người đói, người nghèo nữa không? – Anh lính hỏi.

- Cũng còn... nhưng ít... - Chị như người có lỗi. Dũng đại trưởng trầm ngâm:

- Bố mẹ tôi chắc vẫn nghèo... Có mấy thằng con lớn đến đâu ra trận đến đấy... Chẳng biết còn có đứa nào trở về?...

Trước mắt chị chợt hiện lên đôi mắt của đứa trẻ nép sau chân mẹ giữa căn nhà lá lụp xụp tranh tối tranh sáng, hôm đoàn cán bộ của tỉnh đi khảo sát, thăm và tặng quà cho hộ nghèo trong tỉnh. Chị và anh lính trước mặt cùng im lặng rất lâu. Mãi sau mới nghe anh nói, nhẹ như tiếng gió thoảng:

 - Có lẽ bọn cháu chẳng bao giờ về được! Đến anh em ở đây cũng chưa biết hết tên của nhau. Đông lắm, cô ạ! Cô đừng khóc! Bọn cháu thấy ai vào đây cũng khóc làm chúng cháu buồn quá!

Chị để mặc cho những giọt nước từ khoé mắt lặng lẽ nối nhau lăn trên gò má, chầm chậm rơi xuống ngực áo.

Một khoảng lặng rất lâu giữa mênh mang lãng đãng sương khói nhạt nhoà. Gương mặt anh lính như ở ngay bên cạnh, gần gũi, thân thương.

- Cô đừng buồn! Cháu và đồng đội đã không bị mọi người quên lãng. Điều ấy thật quan trọng đối với những người lính như chúng cháu...

Chị nghẹn ngào, nước mắt vẫn không thể ngừng rơi. Anh lính thở dài:

- Đã có nhiều người vào đây với ý muốn đưa người thân về quê nhà. Đấy là điều mơ ước của anh em, đồng đội phải nằm lại ở bất cứ chiến trường nào. Nhưng, cháu đã nói với cô, anh em, đồng đội ở mảnh đất này đông lắm. Có người chết mất xác. Nhiều người có thể về quê được nhưng không nỡ chia xa đồng đội của mình. Cháu nói thật đấy, cô ạ! Hãy để chúng cháu được cùng nhau ở lại mãi với đất này!...

Chị muốn nắm chặt đôi bàn tay anh lính, nhưng không sao đến gần được. Anh lính chợt reo khe khẽ:

 - Kìa cô! Có lẽ người cô cần tìm đã đến!

Từ đằng xa có bóng một người lính chạy như bay trong sương về phía chị. Người lính ấy đứng ở chỗ Dũng đại trưởng khi nãy. Chị nghe rõ tiếng anh mừng rỡ:

- Có phải em đấy không, cô bé hợp tác xã làng Bẹo?

Cũng cách chị một tầm nhìn, anh lính dừng lại. Anh còn trẻ lắm và đúng là anh ấy rồi! Chị không thể quên được giọng nói ấy. Nhưng anh lính sững lại, bối rối:

- Cháu... xin lỗi ... Cô cũng ở làng Bẹo à? Cô bé ấy tên là Nhiên, có phải là con gái cô?...

Chị thấy nghẹn đắng trong lòng. Mình đã không còn là cô bé làng Bẹo hơn ba mươi năm về trước. Chị từ từ xoè bàn tay. Con búp bê len vẫn vàng tươi như hoa cúc.

- Nó thuộc về anh.

Anh lính mỉm cười:

- Nhờ cô mang về cho cô bé làng Bẹo. Cô nói với cô bé rằng: Cháu đã nhìn thấy những con búp bê cô ấy làm tặng cho những người lính ở mặt trận. Cháu chưa bao giờ quên cô ấy và luôn cầu chúc cho cô bé học giỏi, có được nhiều điều may mắn!...

Nước mắt vẫn chảy dài trên má, chị hỏi:

- Không gặp được cô bé, anh buồn lắm phải không?

- Nếu gặp được cô bé, không phải ở đây mà ở làng Bẹo của cô ấy thì tốt biết bao!... Nhưng không sao. Cháu tin cô bé sẽ làm những điều mà bọn cháu chưa làm được!

Làn sương chợt như mỏng đi. Hơi thở của mặt trời làm ấm dần mặt đất. Đằng xa, những người lính đã tập hợp thành đội ngũ. Anh lính cười thật tươi và giơ tay vẫy chị và đi về đứng vào hàng ngũ. Những đám sương đục nuốt dần hình bóng họ.

Mặt trời le lói phía bờ bên kia của dòng sông. Con búp bê len trên tay chị nhuốm màu nắng vàng rực lên, hơn cả màu vàng của hoa cúc.
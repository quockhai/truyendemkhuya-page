Chiến tranh đã lùi xa lắm rồi. Nhiều cái đã quên. Con đường hành quân xưa. Một trận bom B52 rải thảm. Một trận bom bi. Con suối trong veo. Những cung đường toạ độ… Tất cả đang chìm dần vào dĩ vãng. Nhưng có những cái đáng quên thì không sao quên được. Mà thực ra thì cũng chẳng nên quên, khi trước nhà còn trồng một khóm hoa nhài và lúc nào nó cũng xanh tốt với những bông hoa trắng ngần, thơm ngào ngạt…
Người đàn ông nghĩ vậy!

*

*     *

Chỉ còn một mình, Thu ngồi ở trong phòng. Trong lúc chờ tới giờ làm việc, mọi người kéo ra ban công, nói chuyện, cười đùa rôm rả. Họ đang nói chuyện về người trưởng ban của mình. Từ ngày về ban kỹ thuật sửa chữa điện này làm việc, không hôm nào Thu không nghe thấy họ nói về anh. Thu chưa gặp anh. Anh đi công tác Hà Nội chưa về. Nhưng dù chưa gặp anh, Thu cũng biết được ít nhiều về anh. Về tính nết, về con người, về căn phòng riêng của anh ở một gác xép trên khu tập thể, về cả bàn tay trái làm đủ mọi việc của anh, bàn tay duy nhất còn lại...

Cứ mỗi ngày nghe được một chuyện về anh trưởng ban, Thu lại thấy thêm một nét giống một người mà cách đây mấy năm Thu đã từng yêu. Và cứ đến nơi làm việc. Thu lại nhủ thầm: Không lẽ lại là anh ta! Cả cái tên nữa, chẳng lẽ lại có sự trùng hợp đến tuyệt đối như thế: Lê Hoàng Phương! Cái tên mà đã bao lâu nay Thu cố đem nó vào dĩ vãng, và mỗi lần chợt nghĩ đến là Thu có ngay một cảm giác khó chịu. Nhưng ở đây, mọi người luôn nhắc tới anh ấy. Hôm nay, Phương sẽ về. Mọi người cùng ra ban công chờ anh.

Phương về thật. Ngoài ban công có tiếng reo:

- A, anh Phương đến kìa, các cậu ơi!

Mọi người ồn ào.

Riêng Thu, mở căng mắt chờ đợi.

Họ ùa vào phòng.

Trời ơi, anh ta thật! Con người ấy thật!

Anh đứng sững lại. Khuôn mặt anh thoáng đỏ lên rồi tái đi. Miệng anh thốt lên:

- Thu!

Thu không nghe thấy gì cả, cô hấp tấp bước vội ra cửa. Cô không nhìn thấy mọi người ngạc nhiên nhìn theo cô và có những cặp mắt nhìn xoáy vào Phương.

Ra tới phòng thường trực, Thu nép vào một chiếc ghế con góc phòng. Cô thở hổn hển như vừa có ai đuổi. Cuộc gặp gỡ với Phương làm Thu thảng thốt...

Ngày ấy, Phương và Thu ở cùng một đơn vị thông tin đường dây. Thu còn là một cô gái mười tám, tuổi quân chưa đầy năm. Đi bộ đội, Thu cắt mái tóc rất dài của mình đi, chỉ để đến ngang vai và lúc nào cũng bồng bềnh. Thu hồn nhiên như bất cứ một cô gái nào ở tuổi mười tám, hát, cười luôn miệng. Cặp môi đỏ mọng lúc nào cũng mấp máy không yên, khuôn mặt trắng hồng, má bầu bầu làm tăng thêm vẻ hồn nhiên của Thu. Thu được bổ sung vào tiểu đội của Phương, là người con gái duy nhất của tiểu đội. Thu được mọi người chiều, nhất là Phương. Phương bỏ công làm cho Thu một cái giát hầm, một chiếc bàn tre thật đẹp, để gương lược. Ba buổi trưa kỳ cạch, hý hoáy, Phương đưa cho Thu một cái lọ hoa bé tý xíu rất xinh và một cái hộp thuốc đánh răng vuông vắn... Thu nhận tất cả, và tự coi mình là một đứa em gái bé bỏng đáng được mọi người chiều chuộng.

Thu biết Phương vừa học xong Đại học Bách khoa, anh chỉ nhập ngũ trước Thu có một khóa tuyển quân và vào mặt trận Trị Thiên này trước Thu một tháng. Mọi cái còn bỡ ngỡ chẳng khác gì Thu. Thu thích gần Phương, thích ngồi nghe Phương kể về những điều Phương đã học. Đó là những chiếc mô-tơ từ lớn nhất đến bé nhất, là những chiếc máy biến thế, những mạng, những lưới điện mà ở lớp 10 Thu mới chỉ biết sơ sơ qua bài giảng. Mỗi lần đi đâu về, ngay từ xa, Phương đã gọi:

- Thu ơi, cho em một bông chuối con này...

- Thu ơi, em có lấy hoa dẻ không này...

Có hôm, Phương còn mang về cho Thu một con chim bé bằng ngón tay cái, cặp mắt có viền vàng óng ánh.

Hôm làm cho Thu cái lọ hoa bé tý tẹo xong, Phương hỏi Thu:

- Em thích cắm hoa gì vào đây?

- Hoa nhài... Em thích nhất hoa nhài, anh Phương ạ! - Thu trả lời.

Mắt Phương sáng lên!

Rồi một hôm, Phương tìm đâu được một khóm hoa nhài về trồng ngay ở cửa hầm của Thu.

Một buổi trưa hè tháng sáu, Thu để nguyên bàn tay bé nhỏ của mình trong bàn tay trắng trẻo của Phương. Thu nhắm mắt lại. Thu thấy mặt mình nóng ran, cô đổ tại nắng hè và gió Tây Trị - Thiên .

Chuyện sẽ êm đẹp tất cả nếu như không có đợt đi chiến đấu lần ấy...

Tổ thông tin gồm ba người: Quang, Phương và Thu làm việc ở sở chỉ huy trung đoàn. Tiếng súng mở màn trận đánh đã điểm từ lâu mà cao điểm 30 vẫn chưa bị tiêu diệt. Hai hướng đánh chính bị địch chặn mất cửa mở không lên được nữa. Tiểu đoàn dự bị của trung đoàn đã xuất trận. Từ phía trước mặt, tiếng súng dồn dập vọng về. Trung đoàn trưởng Lê Mai đã vọt lên khỏi hầm, anh dọi ống nhòm về phía cao điểm 30. Giọng anh lạc đi:

- Thông tin đâu, gọi bốn mốt!

Quang siết chặt tổ hợp trong tay:

- Bốn mốt, bốn mốt đâu? Không một gọi bốn mốt! Bốn mốt đâu? Báo cáo thủ trưởng có bốn mốt trả lời!

Lê Mai giật lấy ống nghe:

- Bốn mốt! Bốn mốt nghe đây, không một ra lệnh: Phát huy toàn bộ hỏa lực chiếm cho được ba mươi trong mười lăm phút... Bốn mốt, bốn mốt đâu? - Lê Mai quẳng mạnh ống nghe - Đứt dây rồi... Liên lạc, liên lạc đâu?

Quang đứng vụt dậy. Bóng dáng cao lớn của Quang che khuất cả một khoảng ánh sáng cửa hầm. Anh nói to, át cả tiếng bom đạn:

- Thủ trưởng để tôi đi.

- Cậu hãy lo cho tốt đường dây của cậu đã.

- Báo cáo thủ trưởng, một công đôi việc, tôi đi nối dây và lên tuyến của tiểu đoàn năm nhân thể...

Lê Mai nhìn Quang. Đôi mắt của người chỉ huy tính nóng như lửa ấy chợt dịu xuống. Anh rất tiết kiệm lời khen, chiến sĩ quen hiểu anh qua mắt. Lê Mai dằn giọng nói với Quang:

- Lệnh cho D năm vùng hỏa lực của tiểu đoàn xung phong ngay.

Quang ưỡn ngực:

- Rõ!

Rồi anh quay sang Thu và Phương:

- Đồng chí Phương thay tôi bám máy. Dây đứt tiếp tục đi nối. Kiên quyết không để mất liên lạc. Đồng chí Thu sẵn sàng thay thế đồng chí Phương. Các đồng chí rõ chưa?

- Rõ - Chỉ có một mình Thu đáp rắn rỏi. Cô nhìn vào khuôn mặt rám nắng của Quang. Một ý nghĩ thoáng qua: “Lát nữa thôi liệu Quang có về không?”. Cô siết lấy bàn tay Quang, đặt khẩu AK vào tay anh và nhìn theo hút bóng Quang đang khuất dần xuống phía khe núi, nơi tiếng súng vẫn nổ rền.

Thu quay lại Phương.

Ôi, trước mắt Thu không còn là Phương nữa. Một khuôn mặt tái mét. Hố mắt trũng sâu, thâm quầng, đôi mắt đã trắng ra. Phương ngồi vào một ngách hầm. Thu vội hỏi Phương:

- Anh sốt hả, anh Phương?

Phương lắc đầu thiểu não. Thu nhìn Phương đăm đăm như nhìn một người xa lạ. Một cảm giác rất lạ mà Thu không kịp nhận biết, chạy dọc sống lưng. Cô không nói gì nữa, quay lại chiếc máy điện thoại tác chiến, bàn tay nhỏ nhắn của cô nắm lấy ống nghe. Cô gọi :

- Bốn mốt đâu? Bốn mốt đâu? Alô bốn mốt!… Bốn mốt …

Tiếng súng vẫn nổ. Cao điểm 30 vẫn sôi lên. Bốn mốt không trả lời. Cũng chẳng có tiếng của Quang gọi về. Mồ hôi túa ra ướt lòng bàn tay Thu. Mồ hôi nhỏ giọt trên má Thu. Cô đổi lại ghế ngồi và lại gọi tha thiết:

- Bốn mốt! Bốn mốt... Alô bốn mốt!… Bốn mốt đâu.

- Bốn mốt đây! Báo cáo cho tôi, tôi đã lên tới mõm E bốn, đang phát triển tiếp… Quang thông tin đã hy sinh …

Một tiếng động chói tay, máy lại câm bặt. Mấy giây im lặng nặng nề trôi qua.

Cô quay sang Phương:

- Dây lại đứt, anh Quang hy sinh rồi! Anh ở lại trực máy, tôi đi nối dây!

- Thu! Đừng! Thu!…

Thu mím môi nhìn Phương. Phương đang giơ hai bàn tay chới với:

 - Tôi… tôi…

Giọng anh ta ấp úng và đôi mắt dại đi, run rẩy.

Bây giờ thì Thu đã hiểu rõ cảm giác của mình, đó là sự khinh miệt. Cô quát lên. Lần đầu tiên trong đời cô quát lên như thế:

- Đồ hèn!

Và cô lao vụt đi, để mặc Phương đang run rẩy sững sờ. 

 
 Minh họa: Nguyễn Đăng Phú

Đoạn đường dây ra trận địa dài không đầy một cây số. Nó nằm ngang trên trục của tầm pháo kích. Đạn pháo đã cầy nát con đường mà hôm qua, Thu và đồng đội đã phải vạch từng khóm cây mà đi, tay lần dây, chân thoăn thoắt bước. Những quả đạn pháo vẫn chát chúa nổ xung quanh nhưng hình như Thu không hề biết đến nó. Trước mắt Thu chỉ còn là hướng đánh và nơi Quang đã nằm xuống.
Đoạn dây đứt ngay cạnh lòng con khe cạn. Một đầu dây bị văng mạnh lên cao, cuốn vào một chạc cây, Thu ngẩng nhìn ngọn cây như để tự lượng sức mình. Là con gái cô chưa bao giờ trèo cây. Những lần đi dạo trong rừng. Thu vẫn đứng ở dưới gốc, chỉ cho Phương lấy cho mình những chùm hoa dẻ mềm mại và vàng óng ở vắt vẻo trên cao. Còn bây giờ… Thu hất mạnh người leo lên. Bàn chân sờ soạng, bàn tay lấn cấn. Tim Thu thót lại mỗi khi có một viên đạn pháo nổ đâu đấy rất gần. Những lúc ấy, đôi mắt nghiêm nghị của Quang lại hiện ra và cặp môi Thu mím lại, đôi tay co người Thu lên cao hơn.

Thu đã xuống đến đất, cô quay máy gọi:

- Bốn mốt, bốn mốt đâu?

- A lô. Tôi bốn mốt đây, đồng chí ở đâu?

- Bốn mốt! Đồng chí báo cáo về không một đi, dây thông rồi.

- A lô. Không một, không một đâu? Anh báo cáo cho tôi: Chúng tôi lấn thêm một mỏm E, yêu cầu chi viện thêm đạn DKZ và B40. Rất gấp… A lô. Không một…

Thu ôm máy, xách súng lao vút về phía tiếng súng nổ dồn…

 *

*     *

Bảy ngày sau, tại hậu cứ.

Thu biết Phương đã đứng ở cửa hầm mình rất lâu, nhưng Thu im lặng. Thu không còn muốn nhìn thấy Phương nữa. Đã mấy lần Thu nhỏm dậy rồi lại nằm vật xuống sàn hầm. Cô ngước nhìn căn hầm của mình, chỗ nào cũng có bàn tay của Phương: cái sàn, cái bàn ngoài cửa, bậc lên xuống… cách đây chỉ vài ngày với Thu nó thân thuộc quá. Còn bây giờ cô cảm thấy nó vô nghĩa đến thế. Trước mắt cô lúc này chỉ là một con người hèn nhát, một khuôn mặt trắng bệch, một đôi bàn tay run rẩy. Bất cứ một lúc nào Thu cũng muốn xua đuổi hình ảnh ấy đi.

Có tiếng lạo xạo ở cửa hầm. Một tiếng gọi khe khẽ:

- Thu!

Thu vuốt mấy sợi tóc xòa xuống trán, khom lưng lách ra khỏi cửa hầm. Cô nhìn thẳng vào mặt Phương. Đôi mắt ấy lảng tránh. Thu nói bằng một giọng thảng thốt:

- Anh còn đến đây làm gì nữa!

Rồi giọng cô rắn rỏi hẳn lên, nghiêm túc như một cô giáo:

- Thế là ta hiểu nhau quá kỹ rồi anh Phương ạ. Chẳng còn gì nữa để ta nói với nhau…

- Tôi hiểu… Em không muốn gặp lại tôi nữa… Tôi đến để từ biệt. Tôi đi… Ở một nơi nào đó tôi sẽ làm tất cả để chuộc lại điều đã làm tôi ân hận. Trong cuộc chiến đấu mới, không biết được tôi còn hay mất, chỉ biết rằng tôi đã ân hận và tôi sẽ gắng hết sức mình. Thu hiểu điều ấy cho tôi…

Trời chiều yên ả, tiếng của Phương loãng vào không gian, Thu đứng im, không nói. Rồi im lặng làm cho cô bối rối, cô đưa tay sờ lên má mình, nơi đã có cái hôn của Phương, bàn tay cô có cảm giác gợn gợn và gai ốc sần lên khắp người. Phương đứng dậy, chìa tay:

- Tôi đi …

Thu vẫn đứng yên không nói.

- Tôi đi… - Phương nhắc lại, giọng nhỏ như thoảng qua hơi thở. Bóng anh loạng xoạng xa dần sau một dãy cây rừng thưa thớt. 

*

*    *

Cho đến hôm nay…

Thu đang phải sống một chuỗi ngày nặng nề. Chưa lần nào Phương tìm cách gần Thu và Thu cũng chưa lần nào chuyện trò với Phương. Thỉnh thoảng bất đắc dĩ hai người mới trao đổi với nhau vài lời thuần túy chuyện công tác. Thu không hỏi Phương về đây công tác từ bao giờ. Chuyện Thu về học ở đại học Bách khoa thế nào Phương cũng chưa biết.

Thu thấy ở Phương có rất nhiều đổi khác. Anh trầm tính và chín chắn hơn xưa rất nhiều. Anh thường nói bằng một giọng chắc nịch, không cầu kỳ và ít lời thừa:

- Hôm nay, Thu đi kiểm tra thiết bị. Chú ý theo dõi bộ phận sửa chữa đầu máy. Ở đấy, họ đang kêu thiếu máy hàn điện. Thu thử bàn với họ xem có cách nào khắc phục được không?

- Mười giờ anh Sơn xuống điều độ xem tình hình cái “CE” ra sao. Sửa xong chưa, bao giờ xong...

Mọi người răm rắp theo anh. Công việc diễn ra trôi chảy.

Sáng nay, Thu ra thăm thực địa. Chiếc máy biến thế đặt ở một vị trí thật oái ăm: Ba phía là đồi, phía còn lại là một bức tường chắn ngang chỉ hở ra một khoảng trống không lấy gì làm rộng lắm. Chiếc máy biến thế này đã từng bị hỏng sau một trận bom của giặc Mỹ ngày xưa. Một mình Thu vòng qua vòng lại bên chiếc máy biến thế. Thu đã thử đặt ra một loạt phương án mà vẫn băn khoăn chưa quyết bề nào. Chợt Thu dừng lại: “Phải đưa nó ra vị trí rộng rãi mới xử lý được…”.

Đúng lúc ấy thì Phương đến.

Từ xa Thu đã nhận ra anh. Đôi chân bước vội vàng. Cánh tay còn lại vung vẩy. Ống tay áo rộng không được giắt lại bay lất phất. Đôi vai anh lệch hẳn đi. Dáng đi hấp tấp càng làm anh thêm tội nghiệp.

Tới nơi, anh gật đầu:

- Thu đấy à?

Thế rồi không nói gì nữa, anh mở chiếc thước rút bắt đầu đo đo, tính tính. Mãi một lúc sau, anh mới lại hỏi Thu:

- Thu tính sao với cái máy này?

Từ ngày về ban kỹ thuật này chưa lúc nào Thu lúng túng bằng lúc này. Bấy lâu nay, Thu đã phải nén mình để ít cười đùa hơn. Ở nơi đông đúc, Thu còn có thể giữ nghiêm nét mặt trao đổi công việc với Phương, hoặc dễ dàng lảng tránh Phương. Còn lúc này, chỉ hai người, Thu lúng túng dù chỉ là trao đổi công việc. Ngập ngừng, Thu nói:

- Tôi... chưa có phương án.

- Phải đưa nó ra ngoài khỏi vị trí này...

- Tôi cũng nghĩ thế!

- Có thể cả dây lõi cũng đứt.

- Có lẽ, tôi chưa xem kỹ được.

Cứ nhát gừng thế, hai người trao đổi với nhau những lời khô khốc:

- Thu tính đưa ra bằng cách nào?

- Tôi sẽ suy nghĩ thêm.

- Gấp lắm rồi đấy Thu ạ...

- Vâng... tôi hiểu.

Phương bước ra ngoài phạm vi đặt máy. Anh chợt đứng lại và xoay người nhìn vào bức tường che chắn. Anh gọi Thu:

- Thu này, Thu có nhìn thấy vệt đen trên bức tường này không? Đây này! Trong những ngày chống chiến tranh phá hoại của giặc Mỹ, đây là bức tường bảo vệ. Vào một ngày chiến đấu ác liệt, kẻ thù đã ném bom xuống đây, lửa bom bắt vào đầu biến thế. Chiếc máy biến thế này bốc cháy. Biết có sự cố, người công nhân vận hành bảng điện chỉ kịp cắt mạch điện rồi nhắm hướng bom nổ và nơi có chiếc máy này mà lao tới. Lúc đó, ngọn lửa đã bốc lên rất cao, anh ấy đã lăn xả vào cứu chữa. Ngọn lửa vừa tắt thì cũng vừa một đợt bom nữa vội xuống. Hơi ép của bom hất anh văng mạnh vào tường khi anh chưa kịp nhảy xuống giao thông hào này. Anh ấy đã hy sinh và để lại vệt máu trên bức tường này đây...

Lắng đi một lát, Phương tiếp:

- Tôi về đây, bài học người ta giảng cho tôi đầu tiên là về cái chết của người công nhân dũng cảm này.

Anh chợt nghiêm trang nói với Thu :

- Hãy chọn một phương án tốt nhất, nhưng đừng phá bức tường này. Bây giờ nó không còn là bức tường che chắn nữa mà nó là một tấm bia ghi công người anh hùng, một tấm bia kỷ niệm.

Giọng anh lắng hẳn xuống:

- Mà... mỗi kỷ niệm là một bài học sâu sắc. Dù đó là kỷ niệm thành công, mất mát, hay chua xót ...

Khuôn mặt anh già đi. Mấy nét nhăn như vết rạn chân chim hằn xuống, sâu hơn. Anh hối hả bước đi, không kịp chào Thu...

 *

*    *

Lời khuyên của Phương và hình ảnh Phương làm Thu đêm nay trằn trọc. Cô ngồi dậy, bật đèn, bật tung cánh cửa sổ cho mát rồi ngồi vào bàn làm việc. Những con số nhảy múa trước mắt cô: “Cao 3,5 mét, chiều dài... mét, chỗ rộng nhất... mét, cửa ra vào... mét, hào rộng... mét đưa ra, không phá tường...”. Trong óc cô, một loạt phương án đặt ra. Sửa tại chỗ, phải làm nhà che tạm khi phá mái nhà cũ, phải mất hai tháng mới xong. Không được. Phá mái nhà, dùng cần cẩu kéo ra. Không được. Cần cẩu vào được tới nơi còn khó thì lúc đưa ra còn khó tới đâu. Dùng pa-lăng xích rút lõi tại chỗ. Không được. Chẳng lẽ dứt khoát phải phá bức tường, lấp hào rồi dùng con lăn đưa ra. Hết bao nhiêu công, bao nhiêu ngày vào đây. Mà bức tường ấy còn là kỷ niệm của người đã hy sinh để bảo vệ chiếc máy. Phá đi, anh ấy không muốn. Và bản thân Thu cũng không muốn. Ngày xưa, ở chiến trường Thu đã từng nâng niu, trân trọng dù chỉ là những vật kỷ niệm cỏn con của đồng đội đã vĩnh viễn đi xa. Ngay cả anh Quang, trong trận đầu đi chiến đấu, anh chẳng để lại gì lúc hy sinh, có chăng chỉ là bóng dáng to lớn che khuất cửa hầm hôm ấy. Vậy mà hôm nay Thu vẫn nhớ...

Có tiếng gõ cửa.

- Ai đấy? - Thu hỏi.

- Thu còn thức đấy chứ? Xin lỗi, tôi vào được không?

“Anh ấy!”. Thu do dự, rồi bước ra mở cửa. Vừa bước vào, Phương vội nói:

- Xin lỗi! Tôi thấy cần bàn ngay kẻo quên. Tôi định thế này: Ngày mai cô cho triển khai đưa máy biến thế ra đi. Dùng máy kéo C một trăm kéo ra, lợi dụng cần trục 10 tấn rút lõi. Tất cả công việc ấy chỉ mất một ngày. Năm ngày tập trung anh em công nhân làm cả ba ca là ta đã sửa xong rồi. Thu tính sao?

- Anh thức đến tận bây giờ?

- Quen rồi Thu ạ. Vả lại rất cần, không có điện thì không thể ra gang được.

Một phút im lặng. Sau công việc, hình như hai người không còn chuyện gì để nói với nhau nữa. Thu bối rối. Phương lặng lẽ nhìn những chồng sách của Thu ở trên bàn. Một lát sau Phương mới nói tiếp :

- Phòng tổ chức cán bộ vừa gặp tôi về việc Thu xin chuyển sang bộ phận khác - Anh ngừng một lát - Thu nghĩ lại xem, ở đây còn bao nhiêu công việc. Thu có thể ghét tôi, nhưng ở ban này còn có bao nhiêu người để Thu quý mến, gần gũi, còn có bao nhiêu công việc để Thu bỏ nhiều công sức và hiểu biết vào đấy. Là kỹ sư điện mà Thu muốn tách ra khỏi trung tâm này... Thu nghĩ lại xem.

Thu im lặng nghe. Quả thật những ngày sống, làm việc ở ban này. Thu luôn luôn có một cảm giác bứt rứt khó chịu về Phương. Thà rằng hai người ở hai nơi, khuất đi lại dễ. Đằng này, hàng ngày gặp nhau. Mà Thu thì vẫn không sao xóa được mặc cảm. Ngay lúc Phương đặt chân vào đây, Thu vẫn có cảm giác khó chịu. Chỉ đến lúc anh nói xong mấy lời về công việc và biết anh thức khuya vì nó, lòng Thu mới dịu đi chút ít. Thu biết, việc xin đi nơi khác của mình là chẳng có ý nghĩa gì hết. Sẽ có cả những người cho rằng Thu chạy trốn công việc đang bề bộn ở đây. Nhưng...

Phương dừng mắt trước cái lọ hoa bé nhỏ.

- Thu vẫn giữ được cái này à?

- ...

- Thu cho tôi xin lại...

- Không!

- Nó không là cái gai chọc vào mắt Thu sao?

- Mỗi một kỷ niệm có bài học của nó - Vô tình Thu nhắc lại lời nói khi chiều của Phương.

Phương nhìn vào mắt Thu, anh muốn nói một điều gì đấy. Nhưng rồi anh chỉ thở dài.

*

*    *

Trời mưa suốt đêm. Sáng ra, cơn mưa vẫn ùn ùn kéo tới, mờ mịt trời đất. Hiện trường sửa chữa máy biến thế ồn ào, xáo động trong mưa. Những người công nhân mặc những chiếc áo mưa ngắn ngang đầu gối, chiếc mũ cát úp sụp xuống. Chiếc máy kéo gừ gừ át cả tiếng mưa và tiếng nói của mọi người. Thu đang điều khiển cho chiếc xe gạt ấy lùi dần vào, tiếng của cô lanh lảnh:

- Nữa đi, nữa... nữa... Bên phải một chút, ối, khéo không sụt xuống hào bây giờ...

Phương phất cờ hiệu, chiếc xe rồ máy nhích tới. Đoạn cáp căng ra dần. Phía trước xe, Thu xòe hai bàn tay đã bợt nước vì lạnh lên trời vẫy cho xe nhích lên từng tí một. Vết xích của nó bám sâu xuống đất, cày lên những đường nham nhở. Cỗ máy từ từ trườn ra. Nó bám được vào mép tôn. Đột nhiên, nó lắc mạnh rồi trượt đi. Miếng tôn theo đà trơn cũng lết mạnh về phía trước. Phương thét lên:

- Dừng lại... dừng lại...

Không kịp nữa rồi, cả cỗ máy lớn theo đà trơn cứ thế lướt đi. Chiếc dây cáp chùng lại. Lá tôn bắn vọt ra, miệng hào há toác. Từ phía trước xe, Thu lao nhanh về phía trước máy không kịp tính toán gì. Vừa thấy bóng Thu, Phương choài người xô vào Thu. Cô bật ngã về phía ta-luy bên cạnh. Chiếc máy lướt tới, lao tuột qua người Phương, khi anh vừa bật được ra khỏi đường đi của nó. Những người công nhân bám riết lấy nó.

Phương nhảy xuống giao thông hào, giơ vai đón một chiếc xà beng. Anh xoạc chân, nghiến răng chịu đựng.

Mỗi lần Thu lao tới là một lần Thu nhìn thấy khuôn mặt Phương. Khuôn mặt ấy đã xám lại vì sức nặng quá lớn đè vào. Mái tóc ướt nước mưa bết dính vào mặt. Cánh tay còn lại ghì chặt vào chiếc xà beng. Thu quên tất cả. Cô ôm những khúc gỗ nhảy xuống hào đặt chống vào thanh cỗ máy. Thu nghe Phương nói đứt đoạn:

- Cho xe kéo... kéo... đi... cố... cho nó... bám thành... hào...

Thu vội chạy đi. Chiếc xe rồ máy kéo đoạn cáp căng dần, căng dần. Cỗ máy xô mấy chiếc xà gỗ lệch đi. Nó lao bịch đầu vào bờ hào bên này, khựng lại một chút rồi ủi cả lớp đất bờ hào trườn lên. Chiếc xe đỗ lại. Cỗ máy kềnh càng nằm vắt qua hào, lầm lì, bướng bỉnh. Những chiếc xà beng rút ra. Phương nắm lấy đầu xà beng toan lôi ra. Nhưng anh không đủ sức nữa, mắt anh hoa lên và từ từ quỵ xuống. Anh chỉ kịp nghe tiếng Thu gào lên:

- Anh Phương - Một bàn tay ướt lạnh mềm mại đón lấy Phương… 

*

*     *

Thu đến bệnh viện thăm Phương. Chiếc máy biến thế đã sửa chữa xong, Thu mới có được phút rảnh rang này.

Mình sẽ nói gì với anh ấy? Xin lỗi anh về những ngày sống vừa qua? Mình có lỗi gì mà xin! Mình sẽ bảo anh: “Bây giờ em lại hiểu thêm anh”. Anh ấy biết điều ấy quá đi chứ. Lúc nào mình chẳng hiểu anh ấy. Người nữ y sĩ đón Thu ở cửa phòng Phương nằm. Chị dặn Thu:

- Anh ấy đang ngủ. Ngủ được là tốt. Cố gắng chờ anh ấy dậy đã nhé!

Chị y sĩ đi rồi, Thu ngồi lặng lẽ nhìn Phương. Khuôn mặt ấy đã hồng hào trở lại. Cặp môi hơi hé như cười làm cho cái núm tròn trên cái cằm đầy đặn lúc ẩn, lúc hiện. Cái chăn tụt xuống. Cô đỡ lấy, tém xuống lưng anh. Nhưng sự động chạm nhẹ nhàng ấy đã làm cho anh tỉnh giấc. Sau cái nhìn ngỡ ngàng, anh bỗng reo khe khẽ:

- Thu!

Một nụ cười mãn nguyện nở trên môi anh, nụ cười đậm đà hơn cả khi mơ. Thu chẳng biết nói năng sao. Rồi như chợt tỉnh, giọng anh bỗng khô lại:

- Sửa xong máy chưa Thu?

- Xong rồi anh ạ!

- Có kịp theo tiến độ ra gang không?

- Kịp anh ạ...

Thu nhớ lại buổi tối hôm trước, lúc Phương đến, hôm đó, anh ấy tái mặt đi, toàn thân rung lên và bàn tay còn lại ôm lấy một bên vai!

Thu cắt chanh, pha nước. Cô đặt cốc nước vào tay Phương:

- Anh uống nước!

- Cảm ơn Thu - Anh uống ngon lành. Anh trao cái cốc lại cho Thu, nhìn sâu vào mắt Thu - Cảm ơn Thu đã đến.

Phương thở thật mạnh và câu nói bật ra sau cái thở ấy:

- Sau ngày ấy, tôi đi...

Thu vội vàng giơ tay cản lại. Đôi mắt cô tha thiết nhìn Phương, đôi mắt ấy như nói với Phương: “Đừng, anh đừng kể nữa, em hiểu…”

Trời tối dần. Từ phía nhà máy một vùng trời bỗng hồng rực rỡ. Mắt Phương sáng lên:

- Lò thép hoạt động rồi, Thu ơi.

Gió ùa vào phòng. Hương hoa nhài ở đâu cũng ùa vào theo thơm dịu. Thu muốn chạy ra tìm hái cho Phương chùm hoa mà hai người cùng yêu, cùng thích. Phương cản lại, anh nắm lấy tay Thu:

- Đừng, Thu. Tự hương hoa sẽ đến với chúng mình mà.

*

*     *

- Chuyện ấy xảy ra từ lâu lắm rồi, ngày tôi còn làm việc. Bây giờ đã hưu trí rồi. Kể lại chuyện này mà tôi vẫn bị làm sao ấy.

Có tiếng từ phía nhà trong vọng ra:

- Làm sao hả anh? Chúng mình hiểu nhau quá rồi.

- Thu đấy! Chúng tôi đã làm đám cưới sau đó ít lâu… Khi làm căn nhà này, tôi ráng làm một mảnh vườn nhỏ trồng hoa. Cây hoa đầu tiên tôi trồng xuống ấy là hoa nhài… Có cái muốn quên cũng không thể quên được. Giá mà không có một chút của hôm qua…

Người đàn ông kể vậy!
Bà xã thường dạy tôi, đời sống của gia đình, bớt tiêu tiền phải tính toán chi li, tiền không tiêu đến chính là tiền kiếm được.

Tôi không hiểu ý vợ, cứ khiêm tốn xin chỉ bảo. Vừa vặn hôm ấy thấy tôi hút thuốc, bà xã liền tiến hành thuyết giáo hiện thân:       
- Ví dụ hôm nay anh hút một bao thuốc, phải tiêu mất mười đồng, nhưng nếu không hút bao thuốc này, chẳng phải là anh đã kiếm cho nhà mình mười đồng? Tôi chợt hiểu, nghĩ ra cũng đúng, vội vàng dập thuốc. Trong lòng phục vợ sát đất.

Một hôm đi dạo phố với vợ, sau khi dạo hết những cửa hàng vợ chỉ định, tôi nài vợ:
- Em thương anh, thương anh, quả thật anh mỏi chân lắm rồi, mình vẫn nên vẫy chiếc ta xi đi về em ạ!    

Cặp mắt hạnh nhân của bà xã liếc chéo tôi một cái, dạy bảo:
- Lẽ nào anh quên những lời em thường nói với anh?

Trong lòng tôi một ngàn lần không vui vẻ, hai chân mỏi nhừ, nhưng vẫn vác trên vai những “chiến lợi phẩm” vợ tôi mua, thất tha thất thểu lê từng bước về nhà.

Bởi hôm ấy không vẫy xe đi về, chân tôi sưng vù lên mấy hôm liền, trong thâm tâm cứ trách vợ. Ngấm ngầm suy nghĩ mấy hôm, cuối cùng đã hình thành trong đầu tôi một kế hoạch trả thù hoàn hảo rất kín kẽ.

Một hôm từ bên ngoài về nhà, tôi hớn hở báo hỉ với vợ:      
- Em thân yêu, báo cho em một tin vui lớn, nhưng em phải có sự chuẩn bị về tâm lý. Hôm nay anh kiếm cho nhà mình được bảy trăm năm mươi ngàn đồng. Số tiền không nhỏ đâu, em thử tính hộ anh xem, rút cuộc ta nên gửi vào ngân hàng, hay là làm cái gì khác?

Bà xã không xúc động như tôi tưởng tượng, chỉ nhàn nhạt hỏi.
- Xét cho cùng thì anh làm gì mà kiếm được những bảy trăm năm mươi mươi lăm ngàn đồng? Nói thử em nghe!

Tôi cao giọng báo cáo tỉ mỉ quá trình kiếm tiền của mình:    
- Hôm nay đi một chuyến xe lên thành phố, nhằm trúng một chiếc xe “con ngựa quí” còn mới 80%, giá hai trăm ngàn đồng nhân dân tệ. Anh định mua chiếc xe ấy. Đột nhiên nhớ đến lời dạy của em, số tiền không tiêu chính là tiền kiếm được. Thế là anh bỏ ý định mua xe. Lần này kiếm về cho nhà mình hai mươi vạn đồng. Nhưng anh vẫn không nản lòng, lại nhằm trúng chiếc xe rẻ hơn chiếc xe con ngựa quí, vừa nhìn vào bảng giá, ồ, cũng đòi mười lăm vạn. Trong lòng anh lại nhớ đến lời em dạy, liền bỏ ý định này. Vậy là lại kiếm cho nhà mình mười lăm vạn đồng. Ngay sau đó, anh lại đến chỗ bán nhà gác, lại kiếm về cho nhà mình bốn mươi vạn. Theo em, công của anh hôm nay lớn như thế, em nên khao anh như thế nào?        

Nào ngờ chỉ một câu của vợ, suýt nữa khiến tôi ngất xỉu. Vợ tôi cười lạnh lùng nói:
- Tiền anh kiếm được lần này, em không lấy một xu, anh hãy để giành mà tiêu. Mọi khoản chi của anh từ nay về sau, xin anh hãy trích từ số tiền mình kiếm được!
Đến Việt Nam lần này Park Jung Chang có hai nhiệm vụ, về mặt hình thức, cô, trên danh nghĩa nữ ca sĩ hàng đầu Hàn Quốc có mặt tại thành phố miền Trung nước Việt tham dự Festival âm nhạc tại Cổ Đô; nhưng còn sứ mệnh thứ hai mới là thứ có sức nặng để kéo cô khỏi giảng đường của Học viện Âm nhạc, đó là nhiệm vụ giải mã bí mật mang tên “Mắt Rồng” được cha cô, thượng sĩ Park Ji Oan khoanh đỏ trong tấm bản đồ. Park Jung Chang đã nhiều lần nhìn thấy bữa tiệc âm thanh trên gương mặt dăn dúm của cha, sự rạng rỡ hiện ra trong một vùng ánh sáng tuôn chảy lai láng của những hồi tưởng… Những năm tháng tuổi già, cha cô thường đuổi theo những âm thanh mê hoặc mà ông mang về từ những năm tham chiến trên đất Việt. Biết cô sẽ đi Việt Nam, ông đã dúi vào tay con gái tấm bản đồ cùng ánh nhìn cầu khẩn.
 
Nhiệm vụ thứ nhất diễn ra suôn sẻ. Trong khi Park Jung Chang còn đang đắm đuối với trích đoạn vở nhạc kịch Cosi Fan Tutte của Mozart có tên Thiếu nữ tuổi 15 thì chồng cô đã lấy xong cặp vé đi Phú Hòa. Việc điện đàm với trợ thủ dẫn đường Lee San Ju để hẹn nhau tại Cổ Đô cũng đã được Kim Jang Sun lo liệu đâu ra đấy. Lee San Ju đã có công tìm kiếm và sẽ dẫn con ma Núi Rồng, một cựu lính trinh sát tên Quang, từ Hà Nội vào nhập đoàn. Khác với tâm nguyện của Park Jung Chang, Kim Jang Sun, chồng cô có một động cơ khác. Trong mắt gã, một ý tưởng dù phù phiếm đến mấy nhưng nếu định giá được sẽ đem lại những lợi ích cụ thể. Một triệu đô la là một con số thật hấp dẫn. Hóa ra lão bố vợ Park Ji Oan của gã không phải là một tay khù khờ. Nếu thành công đây sẽ là một phi vụ béo bở, một cơ may của gã.


Minh họa: Phạm Minh Hải
 
Có một sự hối thúc nào đó từ bên trong khiến Park Jung Chang cảm giác chồng cô sốt sắng hơn thường lệ. Một chút băn khoăn thoảng qua nhưng cảm giác hạnh phúc nhanh chóng lấn lướt trong cô. Park Jung Chang chưa bao giờ nghi hoặc những gì chồng cô làm. Hơn lúc nào hết, sau gần mười năm chung sống, cô cảm thấy hài lòng và biết ơn chồng. Có lẽ cha cô, thượng sĩ Park Ji Oan khi xưa, đã quá khắt khe khi nhìn nhận chàng rể. Bởi một sự thực hiển nhiên, nếu không vì tâm nguyện trên cả phù phiếm của ông thì sẽ chẳng bao giờ Kim Jang Sun chịu đến Việt Nam cùng cô lần này.
 
“Chỉ vì mấy viên đá mốc rêu ngâm nước suốt mấy chục năm ấy thôi sao?”, Kim Jang Sun đã tỏ ra hết sức ngạc nhiên khi cô nói ra mục đích thực sự của chuyến đi. Thậm chí chồng cô đã cười khìn khịt gật gù vỗ vai vợ “nếu như thần âm nhạc đã biến vợ anh thành một kẻ phù phiếm thì  bố vợ anh quả là một kẻ phù phiếm gấp mười lần, anh không thể lý giải điều gì đã khiến bố vợ anh bị ám ảnh đến như vậy…”.
 
Cùng hoạt động âm nhạc nhưng quan niệm của Kim Jang Sun khác vợ hoàn toàn. “Quan trọng là nó giúp chúng ta có một cuộc sống tốt chứ không phải là một thứ tôn giáo”, quan điểm của chồng cô là vậy. Vì thế Park Jung Chang đã rất ngạc nhiên khi chỉ độ tuần sau Kim Jang Sun bỗng đổi ý. Gã tỏ ra quan tâm đến kế hoạch một cách chi tiết và cùng lên kế hoạch với vợ.
 
Sự nhiệt tình và năng động của chồng đã giúp Park Jung Chang rất nhiều khi thực hiện sứ mệnh, quan trọng nhất là gã đã móc nối được với Lee San Ju. Gã đồng hương đang có mặt tại Việt Nam đã móc ra từ hàng vạn chiến binh già một nhân vật quan trọng: Thiếu úy Quang, vốn thuộc đại đội đóng chốt tại Núi Rồng khi xưa.
 
Trước khi vợ chồng Park Jung Chang đến Việt Nam, Lee San Ju đã thuyết phục được vị cựu binh làm chiếc ra đa sống giải mã bản sơ đồ nhằng nhịt như một ma trận mà Kim Jang Sun đã mail sang trước đó. Một khoản tiền công hậu hĩnh cùng lời hứa bao trọn gói một chuyến thăm lại chiến trường đã kéo được ông cựu binh già bật khỏi lô cốt gia đình.
 
Theo lý lẽ của Kim Jang Sun thì chỉ có những con ma mới khám phá được ma trận một cách nhanh nhất: “Khi tấm bản đồ do một con ma vốn là lính trinh sát Park Ji Oan vẽ ra, thì người đọc và giải mã nó, tốt nhất cũng nên là một con ma tương ứng ở hàng ngũ đối phương”. Thiếu úy Nguyễn Ngọc Quang đúng tuyệt đối theo công thức đó. Ông vốn là lính trinh sát của đại đội án ngữ phía bắc Tuy An chốt tại Núi Rồng ngay sau khi đại đội lính Đại Hàn của thượng sĩ Park Ji Oan bị đánh bật.
 
Tất cả cùng lên tàu vào Phú Hòa. Chỉ đến khi đoàn tàu lướt qua dãy núi thoai thoải mang hình con vật thiêng chầu về hướng Đông cùng với tiếng ồ lên khi gặp cảnh cũ người xưa của con ma Núi Rồng thì Park Jung Chang mới yên tâm phần nào. Quan sát sức vóc và trí tuệ minh mẫn của ông già tên Quang, Park Jung Chang thầm so sánh với cha mình và nén tiếng thở dài. Căn bệnh đái đường đã rút kiệt sức lực của thượng sĩ Park Ji Oan những năm cuối cuộc đời. Và việc thực hiện nguyện vọng của cha với cô lúc này là một nhiệm vụ quan trọng. Điều khiến Park Jung Chang lo lắng là sự thay đổi của thực địa. Ba mươi năm với một đất nước đang chuyển mình, các khu công nghiệp được xây dựng, nhà máy xí nghiệp mọc lên, rồi những vùng nguyên liệu, cả nạn khai thác đá… Sẽ có rất nhiều nguy cơ dẫn đến việc Núi Rồng bị xóa sổ trên bản đồ chứ đừng nói đến cái điểm tin hin mà thượng sĩ Park Ji Oan khoanh tròn đỏ chót.
 
Với lý do tham quan, tìm hiểu một số nét văn hóa của đồng bào dân tộc thiểu số trong tỉnh, địa phương giới thiệu họ làm việc với Sở Văn hóa. Sau buổi làm việc, đoàn có thêm một thành viên mới – Trần Lợi, chuyên gia về văn hóa dân gian, một người sống tự do.
 
Đoàn người tiếp cận Núi Rồng từ hướng Đông. Phải mất đến hơn một tiếng để ông Quang xác định được hướng đi đến ngôi làng Eaka năm xưa. Họ lần theo hạ lưu dòng suối Eabia dưới sự chỉ dẫn của vị chuyên gia văn hóa. Trần Lợi cho biết, dăm năm trước, một cơn lũ ống tràn qua gần như đã cuốn phăng tất cả. Lũ về giữa đêm khuya khiến dân bản không kịp trở tay, hai phần ba dân số trong bản Eaka đã bị vùi trong đất đá. Cơn lũ đi qua, cả thung lũng trắng một màu đá nham nhở như một thạch địa. Còn sót lại dăm nóc nhà với vài chục nhân khẩu, chính quyền đành tìm một địa điểm tái định cư an toàn hơn để những người còn lại di trú với kỳ vọng tránh cho họ phải đối diện với những ký ức kinh hoàng, vì thế nơi họ đang tìm đến sẽ chỉ là một phế tích.
 
Cả nhóm len lách theo đường mòn ven suối Eabia đầy những dây leo chằng chịt. Lee San Ju liên tục trao đổi tiếng Việt với vị chuyên gia văn hóa. Câu chuyện không ra bí mật nhưng cũng không ra chiều chuyện phiếm. Kim Jang Sun chửi thầm trong bụng về cái thứ ngôn ngữ khỉ gió ở xứ này mà khi còn ở Seoul, cất công tìm hiểu gã đã biết rằng dù có rành rẽ như một ngoại ngữ phổ thông thì khả năng ấy cũng trở nên vô dụng với cách phát âm nặng thổ ngữ của vùng Phú Hòa chết tiệt. Chính bởi thế gã mới phải vời đến tay bạn học năm xưa theo học ngành Việt Nam học, hiện đang làm dịch vụ môi giới hôn nhân tại Việt Nam.
 
Đến cuối ngày, cả đoàn gặp vùng đá trắng đổ tràn một góc thung lũng. Trần Lợi nói rằng đây chính là địa điểm buôn Eaka trong khi cựu thiếu úy Quang ngắm nghía gật gù. Tất cả dừng lại dựng lều nghỉ ngơi. Khi ấy mặt trời xuống bên kia núi. Trong khi thiếu úy trinh sát Quang đang loay hoay buộc lại một góc lều bạt thì Lee San Ju và Trần Lợi vẫn tiếp tục câu chuyện. Vị trí buôn Eaka hầu như không còn dấu vết gì, chỉ còn lại vài khoảng đất khá bằng phẳng. Trong khi đó Lee San Ju và Trần Lợi đang nhỏ to trao đổi:
 
- Ông Lợi này! Ông có nhớ chính xác khoảng sân trước căn nhà rông lớn của Eaka khi xưa không?
 
- Khu vực này nghe nói trong chiến tranh bị oanh kích nên Eaka gần như bị nát vụn… Tìm lại đúng vị trí xưa sẽ rất khó khăn.
 
- Ông có nghe nói đến một cái giếng, xung quanh miệng xây bằng đá…
 
- Một cái giếng… Một cái giếng… - Trần Lợi lẩm bẩm - Anh có thể nói rõ hơn không?
 
- Thì đại để là thế. Nó như một dấu vết để nhận ra ấy mà.
 
Trần Lợi bỗng đổi nét mặt:
 
- Này anh bạn! Hãy nói thật đi, quý ông kia tìm về nơi này không phải chỉ với mục đích nhìn lại nơi nhạc phụ đã từng gây tội ác đúng không?
 
- Tôi không biết… Tôi chỉ là người phiên dịch.
 
- Oh… Vậy thì hãy phiên dịch lại với quý ông bạn của anh là chừng nào tôi biết điều gì đằng sau cái giếng ấy thì chúng ta hãy tiếp tục thăm lại chiến trường xưa của nhạc phụ ông ta. Anh hiểu chứ!


Ảnh: Phạm Duy Tuấn
Trần Lợi nói như mặc cả trong khi mắt gã đánh về phía Park Jung Chang và ông Quang. Khả năng giao tiếp tiếng Anh là thứ xa xỉ đối với Trần Lợi, vì thế, hắn chỉ biết nắm chắc lấy cái đuôi là gã phiên dịch này. Lee San Ju quay sang trao đổi với Kim Jang Sun bằng tiếng Hàn. Qua vài lời to nhỏ Kim Jang Sun nhận ra ngay đẳng cấp của đối thủ. Gã đã quá coi thường vị chuyên gia văn hóa. Trong khi đó, Park Jung Chang và thiếu úy Quang đang làm quen với nhau bằng tiếng Anh.
Trần Lợi tiếp tục đánh mắt về phía ông Quang đầy cảnh giác, dù sao trong đoàn ông ta cũng là người hiểu rõ nhất những gì hắn nói. Linh cảm của Trần Lợi đã không sai. Vì thế, hắn đâu có ngu đưa hổ đến tận hang. Hắn cần có thêm thời gian để làm rõ mọi chuyện. Đám người này đi tìm gì?! Câu chuyện về những kho báu trỗi dậy trong gã. Có thể lắm chứ. Chỉ có những kho báu mới có một sức hút mạnh mẽ đến thế, nhất là khi mà hai vợ chồng nhà kia phải bỏ lại cả ông bố gần đất xa trời để rủ nhau mò mẫm đến tận hang cùng ngõ hẻm này. Nếu thế thì hắn cũng đang là một thành viên trong đoàn tìm kiếm. Và quan trọng hơn, Trần Lợi ý thức rõ về chiếc chìa khóa hắn cầm trong tay.
 
- Thôi được! Đồng ý là có một bí mật. Anh đúng là một con ma anh bạn ạ – Lee San Ju chuyển ngữ lời của Kim Jang Sun nói với Trần Lợi.
 
- Tôi quan tâm đến cách giải quyết bí mật ấy kia, anh bạn.
 
- Anh hãy dẫn chúng tôi đến miệng giếng Mắt Rồng. Chúng tôi muốn tìm một thứ ở đó.
 
- Nó là cái gì vậy?
 
- Anh làm ơn không hỏi được không. Chúng tôi chỉ có thể nói rằng không có vàng bạc châu báu gì ở đó cả, và hứa là sẽ trả công cho anh xứng đáng.
 
- Ồ… Vậy sao…
 
Rồi như ngộ ra điều gì Trần Lợi khẽ reo lên. Hắn cúi xuống nhặt một viên đá dẹt, tay kia gõ gõ nhè nhẹ lên viên đá nheo mắt nhìn Kim Jang Sun giễu cợt:
 
- Nó là thứ này đúng không?
 
- Tại sao ông biết?
 
- Chúng tôi cũng đang tìm nó…
 
Kim Jang Sun hốt hoảng nhìn về phía vợ. Chó chết! Vậy là mày cũng biết. Gã tự nhủ rồi bất ngờ đổi cách xưng hô, gằn từng tiếng:
 
- Thôi được! Không phải vòng vo. Ông muốn gì?
 
- Tôi muốn biết khoản mà tôi sẽ được hưởng – Trần Lợi tỏ ra rất biết chiều ý đối phương.
 
Cuộc đối thoại có vẻ âm thầm nhưng rốt ráo. Lee San Ju lại quay sang trao đổi với Kim Jang Sun, đoạn quay lại nói với Trần Lợi:
 
- Ông sẽ có câu trả lời khi chúng tôi có mặt tại miệng giếng Mắt Rồng. Được chứ?
 
- Tôi không khờ đến thế. Tôi muốn có một giao ước cụ thể trước khi đến miệng giếng.
 
Lại một cuộc hội ý nhỏ giữa cặp tiếng Hàn.
 
- Thôi được. Ông sẽ có ít nhất năm ngàn đô. Năm ngàn đô. Ông hài lòng chứ? Bởi chính chúng tôi cũng chưa biết sẽ được lợi lộc gì từ chiếc giếng chết tiệt đã bị vùi lấp đến mấy chục năm. Chúng ta đang đi câu, ông hiểu chứ…
 
- Nhưng nếu câu được cá?
 
- Mười nghìn.
 
- Ok. Tôi hiểu - Trần Lợi nói như một sự chốt hạ cho cuộc thỏa thuận.
 
- Vậy thì nên tận dụng thời gian – Lee San Ju đề nghị.
 
- Park Jung Chang! Em và ông Quang cứ nghỉ lại lều, bọn anh đi tắm suối một lát sẽ trở lại – Kim Jang Sun ngoái lại dặn vợ, đoạn quay sang Lee San Ju - Nói với ông già cứ nghỉ ngơi, ngày mai mới bắt đầu công việc.
 
Khi bộ ba tiếng Hàn rút đi thì cặp tiếng Anh - Park Jung Chang và ông Quang cũng đang diễn ra những trao đổi nhưng theo một chiều hướng hoàn toàn khác. Rất may là vốn tiếng Anh của thiếu úy trinh sát năm xưa chưa đến nỗi mai một. Họ cũng có những bí mật để chia sẻ. Park Jung Chang thuật lại vắn tắt câu chuyện của cha cho người cựu binh. Câu chuyện về ngôi làng, những thổ dân, một nghi lễ, những âm thanh ma mị… Tất cả như hiện về trước mắt cô tại thung lũng đá này… Những lời cha kể văng vẳng bên tai…
 
*
*    *
 
Ngày ấy, khi đơn vị quyết định chiếm lại cứ điểm yết hầu án ngữ vùng Tuy An, Park Ji Oan cùng một nhóm trinh sát Đại Hàn được giao nhiệm vụ tiền trạm mục tiêu Eaka. Đây là điểm yết hầu khống chế toàn bộ khu đồng bằng rộng lớn vốn được mệnh danh là vựa lúa của cả khu vực, cái dạ dày của quân giải phóng Bắc Việt.
 
Tháng trước, đại đội của Park Ji Oan đã bị đánh bật khỏi điểm cao. Thay vào đó là một đại đội pháo của quân giải phóng chiếm đóng ở ngọn đồi đối diện, và điều khiến cứ địa này trở nên khó nhằn chính là nó được sự hỗ trợ quyết liệt của những thổ dân buôn làng Eaka. Đã ba lần quân Đại Hàn đổ bộ đều thất bại thảm hại. Chẳng những không nhổ được cái gai bướng bỉnh mà còn nướng cả trăm quân lính một cách vô lý đến không thể hiểu.
 
Điều bất ngờ là những cái chết không phải đến từ súng pháo quân giải phóng mà đổ ùng ùng từ trời cao xuống. Không tiếng nổ, không đụng độ, chỉ có những tiếng ú ở hoảng loạn qua điện đài cùng những âm thanh rầm rập như cả đàn voi rừng nổi giận, rồi tất cả tắt lịm, mất hút. Ma quái, bí ẩn. Sau này, trong cuộc tấn công thứ ba, một tên lính may mắn thoát nạn trở về thì bí mật mới được hé lộ.
 
Hàng trăm quân lính Đại Hàn đã rơi vào những bẫy đá khổng lồ. Những phiến đá nặng hàng tấn rầm rầm lăn từ trên núi xuống càn quét tận diệt khiến đám lính trong tích tắc đã trở nên nát bấy thịt xương mà không kịp hiểu chuyện gì xảy ra. Thứ duy nhất còn lại là những tiếng la hét hoảng loạn. Sau khi rút kinh nghiệm, một phương án khác được triển khai. Áp dụng đúng bài của quân giải phóng, kết hợp du kích và hiện đại. Cấp trên đã đồng ý dùng không quân chi viện. Nhóm của thượng sĩ Park Ji Oan được giao trinh sát trước khi máy bay đến làm nhiệm vụ san phẳng cứ điểm, xay nát những hòn đá tảng cùng đám thổ dân, những kẻ sáng tạo ra những thạch địa lợi hại.


Ảnh: Phạm Duy Tuấn
 
Khi tiếp cận được ngôi làng, có một thứ đã làm thay đổi quyết định của Park Ji Oan. Hôm ấy là một đêm trăng sáng đến nao lòng. Dòng suối Eabia căng mình đón gió, những cánh hoa rừng uống no ánh vàng của trời cao phập phồng tỏa hương tinh khiết. Khung cảnh thanh bình và ảo mị như một miền cổ tích.
 
Lắng nghe trong gió, Park Ji Oan bỗng thấy những âm thanh thật lạ. Khi khoan, lúc nhặt, khi bổng lúc trầm. Những âm thanh có một sức hút lạ lùng làm trỗi dậy trong chàng thượng sĩ giấc mơ âm nhạc trước khi vào lính. Trong phút chốc, như bị ma ám, anh ngược hướng gió đi dưới ánh trăng, bất chấp những nguy hiểm rình rập. Lặng lẽ ngược dòng âm thanh bí ẩn và mê dụ, Park Ji Oan có cảm giác mình đang tìm đến một bữa tiệc âm nhạc giữa đại ngàn.
 
Và cảnh tượng đặc biệt nhất trong đời đã diễn ra trước mắt anh. Dưới ánh trăng, một người con trai từng lọn tóc đổ dài nhảy nhót trên bờ vai trần rắn chắc và vuông vức như tạc bằng đá núi, hai tay cầm hai dụng cụ như hai dùi trống nhỏ, trước mặt anh ta là một giàn nhạc cụ lạ. Bàn tay người trai bản chạm vào đâu những âm thanh bay lên đến đấy. Một hệ thống khuếch đại với những ống nứa to nhỏ khác nhau được bài trí xung quanh.
 
Vây quanh người con trai là những bóng đàn ông dũng mãnh với giàn nhạc cụ kim loại hình tròn phụ họa, một tai xách, một tay gõ. Bên đống lửa những cô gái với một những bước chân nhịp nhàng uyển chuyển, họ vừa nhảy múa theo điệu nhạc vừa di chuyển theo hình tròn. Xen kẽ giữa các cô là những chiến sĩ quân giải phóng. Tâm điểm của đám đông chính là chàng trai đang gõ xuống những phiến hình trụ tuồng như là đá đánh thức những âm thanh réo rắt vang vọng núi rừng. Từ bàn tay người con trai có tiếng gầm gào đồng vọng của thác lũ, có sự dịu dàng mơn man căng nở của cánh lan rừng, có sự mời gọi tha thiết của tiếng chim gọi bạn…
 
Những gì diễn ra trước mắt đã khiến Park Ji Oan như mê mụ, trong phút chốc anh quên cả nhiệm vụ tối quan trọng, và quên cả nỗi hiểm nguy với tính mạng của mình. Đứng sau một phiến đá lớn, đôi chân Park Ji Oan vô thức nhún nhảy theo nhịp điệu âm thanh, lòng rộn ràng khôn xiết.
 
Trăng đã lên giữa trời, soi ánh sáng vằng vặc khắp núi rừng, từng tàng cây ngọn cỏ đều thấm đẫm âm thanh và ánh sáng, đều như hòa nhịp vào đại tiệc giữa thiên nhiên. Bản hòa tấu của đất trời như bất tận. Khi những thanh củi trong đống lửa sụm xuống, những chàng trai khiêng ra một khúc cây có treo một con thú rừng đặt lên hai giá đỡ. Những vò rượu cũng được bê ra đặt quây quanh bếp than hồng. Con thú được quay tròn, những giọt mỡ tứa ra rơi xuống bùng cháy và tỏa mùi thơm đến nôn nao.
 
Park Ji Oan nuốt nước bọt nghĩ đến cái dạ dày lép kẹp. Cũng giây phút ấy, anh bỗng nhìn bóng mình đổ dài chờm lên một khoảng sáng rung rinh bên cạnh. Ánh trăng lên cao làm lộ ra một miệng giếng khuất dưới bóng cây. Miệng giếng được kè bằng những viên đá vuông vức bằng bịa. Mặt nước tròn vành vạnh như một vầng trăng căng mọng như hứng lấy những âm thanh.
 
Đêm đã khuya. Chiếc máy thông tin bên hông phát tín hiệu từ sở chỉ huy. Hai mũi còn lại đang réo gọi để hợp tin. Như tỉnh ngộ, thượng sĩ Park Ji Oan lật đật làm thao tác thông liên lạc. Một dòng tin truyền đi, Park Ji Oan hiểu tất cả những gì vừa diễn ra trước mắt anh một chút nữa thôi sẽ trở thành tro bụi. Không hiểu sao, thay vì rút lui nhanh chóng anh lại ngửa mặt nhìn lên trời cao, đối diện với mặt trăng vời vợi sầu thảm rắc vàng hoang hoải và thở dài trước những năm tháng quân trường đằng đẵng xứ người, Park Ji Oan bỗng giật mình như tỉnh ra sau cơn ác mộng.
 
Anh vội chạy một đường vòng cung về phía sau nhà rông, móc tấm khăn tay trong túi áo, rút cục bông ngậm xăng trong chiếc bật lửa mang theo, nhặt một viên đá nhỏ gói vào chiếc khăn và bứt dây rừng cột lại, anh bật quẹt châm lửa. Cục lửa vút cao lượn một đường cầu vồng trước khi đậu xuống mái nhà rông.
 
Không hiểu sao Park Ji Oan cứ đứng chôn chân nhìn mái nhà bén lửa trong khi lẽ ra phải rút chạy từ lâu. Ba tiếng tù và rúc lên. Từ trên cao nhìn xuống Park Ji Oan thấy mái nhà rông bốc cháy đùng đùng. Rất nhanh, bóng các chiến sĩ giải phóng lẩn vào từng khe đá, những người đàn ông nhanh chóng rời những chiếc cần hút rượu từ những chiếc vò không phải để lao vào dập lửa mà nhanh chóng biến vào rừng, có lẽ họ chạy đến nơi có những chiếc bẫy đá.
 
Còn những người đàn bà lao vào cứu ngôi nhà đang bốc cháy. Họ chạy về phía miệng chiếc giếng, nơi Park Ji Oan vừa đứng, xếp thành dây chuyền múc từng gầu nước truyền tay nhau hất lên mái nhà. Nguy hiểm đang đến gần, Park Ji Oan cắm mặt chạy ngược lên đỉnh núi, nơi có một bãi trống trực thăng có thể đỗ. Ngọn lửa càng ngày càng lan to, những người đàn bà tỏ ra bất lực, đúng lúc đó, tiếng máy bay ù ù dội xuống.
 
Một tiếng hô rất to như là hiệu lệnh khiến Park Ji Oan phải ngoái cổ, tất cả đàn bà bỏ không dập lửa nữa, thay vào đó họ chạy lại dàn nhạc ôm vào ngực những phiến đá hình trụ đã phát ra những âm thanh mê hoặc. Mỗi người phụ nữ ôm chặt một phiến đá nhỏ ápvào ngực và chạy thẳng về phía giếng nước. Họ vứt những vật mang theo xuống giếng. Máy bay oanh kích đã đến rất gần. Park Ji Oan quay đầu tiếp tục chạy mải miết về phía đỉnh núi.
 
Theo hiệp đồng, trực thăng sẽ đón tổ trinh sát tại điểm cao này, nơi không có những trận thạch địa nhất loạt như bão lũ đổ xuống. Trong khoảnh khắc chờ trực thăng đến đón, Park Ji Oan nhìn thấy cảnh những người đàn bà vây quanh tảng đá nơi anh vừa đứng, thả dây tời, và tảng đá lớn bỗng chuyển động ngoan ngoãn nằm choán lên miệng giếng.
 
Anh đoán chắc chắn những người đàn bà thổ dân đã thả những phiến đá nhỏ dùng làm nhạc cụ xuống giếng như một cách cất giữ bảo vệ và đậy miệng giếng bằng phiến đá khổng lồ.
 
Mười lăm phút sau, khi chiếc trực thăng đã rút, cả vạt núi bị oanh tạc tơi bời. Lửa cháy mù mịt. Tiếng thú rừng bị thương rống thảm thiết. Những phiến đá lớn trên sườn núi nổ tung bụi mù mịt, trăm nghìn mảnh đá nhỏ lăn trùm lấp kín một đoạn suối Eabia. Làng Eaka gần như bị xóa sổ.
 
Thiếu úy Quang là người chứng kiến đoạn kết câu chuyện bi hùng ấy, bởi ông chính là một trong những chiến sĩ giải phóng tham dự cuộc vui của dân làng. Không thể khác, bởi những gì diễn ra đúng như lời kể của con gái người lính phía đối phương đang ngồi trước mặt ông. Trận oanh kích khiến buôn làng Eaka gần như bị xóa sổ, nhưng hỏa lực đơn vị ông bố trí trong một hang đá rộng vòm kiên cố vẫn được bảo toàn, nhờ vậy mà sau đó khi máy bay không kích rút đi, quân đổ bộ tấn công đã bị pháo kích quân giải phóng chặn đứng, cứ điểm vẫn được chốt giữ.
 
Riêng thung lũng Eaka bị đất đá trùm lấp khiến dòng suối Eabia phải đổi dòng. Hơn hai mươi em nhỏ con em đồng bào dân tộc buôn Eaka sống sót sau đó đã được bộ đội dựng nhà bên thung lũng phía hạ lưu dòng suối, đối diện sườn núi có mũi đá chìa ra như chiếc vây rồng để ở, sau này thành bản Eaka mới. Chỉ có điều, ông Quang không hề biết dàn nhạc cụ bằng đá đã được buôn làng bảo vệ chứ không hề bị phá hủy. Sau sự kiện oanh kích Eaka ông Quang cũng bị thương trong một trận chiến đấu và được trên rút về căn cứ.


Ảnh: Phạm Duy Tuấn
 
Những ngày gắn bó với dân làng, ông Quang cũng đã được biết về những thanh đá ngậm trong mình những thang âm bí ẩn. Tuy nhiên, không phải ai cũng có tài gọi được những thang âm ấy cất lời. Bộ nhạc cụ đá được buôn làng cất giữ như một báu vật, và người có quyền năng gọi đá hát sẽ được tôn làm thần đá. Một điều kỳ lạ là trong buôn thời nào cũng chỉ có một người duy nhất nắm giữ quyền năng này. Thông thường thì khi nào người nắm quyền năng muốn truyền cho người khác sẽ tập hợp tất cả thanh niên trên mười sáu tuổi và chưa có vợ đến để thử được khả năng và chọn ra một người xuất sắc nhất để kế tục. 
 
Người con trai có tên A Đe ngày ấy mới được chọn kế tục được ba năm nhưng tài năng từ đôi tay đã được cả buôn công nhận. Sự kết hợp tình cờ từ bàn tay, ánh mắt, những rung lắc cơ thể của anh đã tạo nên một sự cộng hưởng làm nên một dàn hợp âm lạ lùng khiến con nai đang gặm cỏ cũng phải nghiêng tai nghe, con hươu đang uống nước dưới suối Eabia cũng phải ngỏng chiếc cổ dài ngơ ngác.
 
Con trai con gái trong buôn đang làm gì nghe thấy tiếng đàn của thần đá A Đe cũng bỏ đấy đứng ngây ngất như bị bắt mất hồn. Ông Quang cũng không biết bộ nhạc cụ đá có từ bao giờ và ai là người khai sinh ra nó, chỉ biết rằng nó chỉ được dùng trong các lễ hội của buôn bên cạnh dàn chiêng đồng cổ xưa và tiếng tù và hiệu triệu. Nhân vật chính trong cuộc vui mà Park Ji Oan đã nhìn thấy ở đêm trăng năm xưa chính là thần đá A Đe của buôn làng Eabia…
 
*
*     *
 
- Nguyện vọng của bố tôi là như thế. Bố tôi muốn tìm lại những phiến đá đang ngủ yên dưới giếng sâu trả lại cho người dân địa phương vì bố tôi nghĩ đó sẽ là một di sản quý… Đấy là lý do chúng tôi tìm đến đây. Mong ông hợp tác và giúp đỡ.
 
- Qua những gì chị nói tôi có thể tin chị. Nhưng…
 
- Ông cứ nói. Tôi sẵn sàng nghe.
 
Ông Quang dường như hơi lưỡng lự:
 
- Xin lỗi chị… Tôi biết tiếng Hàn.
 
- Nghĩa là sao? Ông có thể nói rõ hơn?
 
- Ý của chồng chị không như chị. Trong suốt hành trình tôi đã nghe được câu chuyện giữa Kim Jang Sun chồng chị với Lee San Ju cũng như những gì hai người trao đổi với Trần Lợi.
 
- Ông nói sao?
 
- Theo họ, dưới giếng Mắt Rồng là bộ đàn đá cổ nhất Đông Nam Á đang được tìm kiếm. Đã có người đồng ý giá một triệu đô la nếu như tìm thấy, đó là lý do chồng chị sang Việt Nam cùng chị…
 
- Ông vừa nói gì? Không thể như thế được…
 
- Tôi thề với danh dự của một người trong cuộc rất hiểu tâm nguyện của cha chị dù chưa từng gặp mặt. Nguy hiểm hơn, Trần Lợi đã đồng ý giúp chồng chị và Lee San Ju đạt mục đích mang những phiến đá đi khỏi nơi này với cái giá mười nghìn đô. Theo nhận định chủ quan của tôi, giếng Mắt Rồng vẫn còn, và những phiến đá cũng vẫn nằm dưới đó. Chỉ có điều việc tìm kiếm bằng cách nào và diễn ra trong bao lâu mà thôi. Vì thế tôi và chị phải hành động.
 
- Bây giờ… Bây giờ tôi phải làm sao? – Park Jung Chang lập bập.
 
- Hãy theo tôi. – Ông Quang nói gọn và rõ như một mệnh lệnh.
 
Park Jung Chang luống cuống đi theo người lính già. Trăng sáng soi đường nên họ đi dọc bờ suối không mấy khó khăn. Ngay từ khi đặt chân đến bãi đá trắng, ông Quang đã nhận ra đó không phải là địa điểm buôn làng Eaka trước đây mà chỉ là làng mới, bởi chính ông là người tham gia tìm địa điểm, vào rừng chặt cây dựng những ngôi nhà cho bọn trẻ còn lại, tuy nhiên, ông cũng hiểu để tìm được miệng giếng khi xưa không hề dễ. Địa điểm mà Trần Lợi cho đoàn dừng chân dựng trại và tuyên bố đây là làng Eabia thực ra chỉ là làng mới lập sau này. Ông Quang biết điều đó nhưng im lặng. Vì thế, Trần Lợi đồ rằng, nếu ông Quang bám theo, dù là con ma Núi Rồng hay dù có khả năng thấu thị những ngôi mộ đá nơi này thì cũng đừng hòng tìm ra miệng giếng Mắt Rồng, bởi đơn giản, ông đang chiến đấu trên một trận địa giả.
 
Tuy nhiên, Trần Lợi đã nhầm. Đang đắc chí với suy nghĩ đã đánh lừa được con sói già thì hắn đã thấy hai bóng người xuất hiện ngay sau lưng. Hắn cùng Kim Jang Sun và Lee San Ju đang đứng trước một miệng giếng nham nhở đen hoẳm. Kim Jang Sun thảng thốt khi nhận ra sự có mặt của vợ:
 
- Em yêu! Em đang làm gì vậy? Em vẫn nhớ những điều anh dặn đấy chứ?
 
- Anh yên tâm. Nhưng em không nghĩ anh sẽ bắt em phải nằm lại cả đêm nay trong căn lều ấy!
 
- Tốt nhất nên như thế, em yêu…
 
- Anh có bị làm sao không đấy? Em không vào rừng để hát opera!
 
- Nhưng như thế có lẽ sẽ hay hơn… – Kim Jang Sun dằn giọng đầy ngụ ý.
 
- Kim Jang Sun, anh vẫn nhớ những gì đã hứa với em đấy chứ… - Bắc Park Jung Chang hổn hển.
 
Lúc này ông Quang đã tiến lại gần nói bằng tiếng Hàn dõng dạc:
 
- Này anh bạn. Tôi đã biết hết rồi. Anh hãy tôn trọng nguyện vọng của vợ anh cũng là của bố cô ấy.
 
Kim Jang Sun cười gằn:
 
- Á à! Được lắm lão già! Thì ra lão còn nói được cả tiếng Hàn cơ đấy. Tôi cảnh báo, lão đang thò mũi vào chuyện của người khác. Lão nên nhớ lão được thuê đến đây vì điều gì.
 
- Là để dẫn các anh đến miệng giếng Mắt Rồng đúng không? Và bây giờ anh đang đứng trước nó. Hợp đồng đã chấm dứt.
 
- Lão nói tiếng Hàn khá lắm. Vậy thì khỏi phiên dịch, Lee San Ju, mày hỏi thằng chuyên gia văn hóa xem đã muốn nhận tiền bây giờ chưa rồi hãy giúp tao bảo vệ Park Jung Chang. Để con sói già này cho tao.
 
- Kim Jang Sun… Em không cần tiền… Chúng ta đâu cần nhiều tiền đến thế…
 
- Cô im ngay…
 
Kim Jang Sun lồng lên như một con thú. Park Jung Chang hét lên tuyệt vọng trong khi ông Quang chỉ mặt Trần Lợi:
 
- Ngươi là kẻ đốn mạt đội lốt chuyên gia văn hóa. Hối mau còn kịp…
 
Nhưng chưa nói hết câu, Kim Jang Sun đã lao vào như một con gấu túm lấy cổ áo ông già. Gã rít lên:
 
- Hãy biết điều, nếu ông còn muốn quay về…


Ảnh: Phạm Duy Tuấn
 
Ngay lập tức người lính già hất cánh tay gã bẻ quặt ra phía sau, Kim Jang Sun mất đà tụt xuống giếng, rất nhanh, ngay khi bị rơi, một tay gã bám vào miệng giếng, một tay túm được cổ chân ông Quang. Park Jung Chang hét lên định lao tới nhưng Lee San Ju đã kịp giữ cô lại.
 
Ông Quang đứng tấn, cố gắng giữ thăng bằng, giọng điềm tĩnh:
 
- Tôi sẽ kéo anh lên, với điều kiện anh phải thực hiện theo nguyện vọng của Park Ji Oan, bố vợ anh?
 
- Ông già, làm ơn đi... Phần của ông sẽ là mười nghìn đô...
 
- Đừng nói với ta những lời như thế. Hãy nghĩ đến vợ anh…
 
- Lão già… Đồ ngu…
 
Sự bực tức làm cho Kim Jang Sun tuột bên tay đang bám vào thành giếng, gã hoảng hốt chuyển ngay sang túm chặt chân ông Quang bằng hai tay. Ông Quang gồng căng cơ thể, hai chân rung bần bật, hàm răng nghiến chặt không hiểu vì sức nặng của gã nhạc công người Hàn đang đeo trên cơ thể ông hay vì tức giận. Ông quay sang Trần Lợi:
 
- Sao anh còn đứng đấy…  Anh không biết phải làm gì sao?
 
Trần Lợi như tỉnh ra, hắn chồm tới miệng giếng, tia mắt vằn lên độc ác. Trần Lợi bất ngờ xô người lính già thêm một nhịp. Một tiếng “ùm” vọng lên từ khoảng sâu tối tăm. Trần Lợi điên cuồng bê những phiến đá ném xuống giếng như lấp một ngôi mộ. Park Jung Chang và Lee San Ju lao vào ngăn cản. Cuộc giằng co giữa hai và một khiến hắn loạng choạng bước hụt xuống miệng giếng cùng với phiến đá đang ôm trên tay. Park Jung Chang ngửa mặt nhìn trăng gào thảm thiết. Trăng ở trên cao. Trăng không với tới độ sâu hun hút của giếng Mắt Rồng.
 
Nước mắt rồng ở quá sâu nên không chảy được, chỉ có nước mắt Park Jung Chang chảy tràn từ ngày ấy…
 
*
*    *
 
Mười năm sau.
 
Park Jung Chang đến Việt Nam lần hai tham sự Festival tại Cổ Đô nhân kỷ niệm hai mươi năm thiết lập quan hệ Việt – Hàn. Lần đầu tiên, bộ đàn đá được mang từ Phú Hòa đến Festival trình diễn với khách quốc tế. Theo lời giới thiệu, nó đã được thủy táng gần nửa thế kỷ dưới một chiếc giếng cổ có tên gọi Mắt Rồng trên một sườn núi thuộc địa phận Tuy An. Bộ đàn đá được công nhận là có thang âm chuẩn nhất trong những bộ tìm thấy ở Việt Nam. Khi phần biểu diễn đàn đá của nghệ sĩ Việt Nam kết thúc, bất giác Park Jung Chang bước lên sân khấu xin phép được cầm hai chiếc dùi nhỏ dạo trên những phiến đá. Bàn tay cô như được điều khiển bởi một thế lực siêu nhiên, từ những ngón tay vút lên những thang bậc ngân nga réo rắt như nước chảy, như chim hót… Qua những phiến đá thẫm màu phập phồng run rẩy trước mắt, cô nhìn thấy ánh mắt thanh thản của cha trước khi từ giã cõi đời.
 
Thời gian còn lại ở Việt Nam, theo kế hoạch, Park Jung Chang đến viếng mộ cựu chiến binh Nguyễn Ngọc Quang để nói với ông lời trăng trối của cha cô, thượng sĩ Park Ji Oan. Tại một làng cổ ngoại thành Hà Nội, mộ của ông Quang nằm bình dị bên những ngôi mộ khác trong nghĩa trang làng. Bên cạnh là nghĩa trang liệt sĩ của xã nổi bật dòng chữ “Tổ quốc ghi công”, nơi an nghỉ của những người lính ngã xuống vì sự bình yên của dải đất hình chữ S này, trong đó có những đồng đội của ông Quang.
 
Trại viết Phú Yên tháng 4 năm 2012
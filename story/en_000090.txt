Nó vừa giải quyết nỗi buồn từ bụi cây ven đường, kéo khoá quần rồi quay ra, bỗng nhiên nó thấy dưới gốc cây cách chỗ nó đứng không xa có một chiếc xắc tay. Nó ngó nghiêng nhìn bốn xung quanh, thấy không có ai nó mới len lén chạy qua nhặt lên. Một chiếc xắc tay làm bằng da cá sấu màu hạt dẻ thẫm, rất xịn, mặt da mềm mại, bóng loáng. Nó chưa bao giờ nhìn thấy xắc tay nào đẹp như thế. Thực ra nó chưa từng có một cơ hội được tiếp xúc với con gái.

Nó mân mê sờ chiếc xắc mềm bóng, trong lòng nghĩ thầm, chiếc xắc này nhất định là của một cô gái xinh đẹp, ăn mặc thật mốt. Sao chiếc xắc tại lại nằm ở bụi cây? Nó không hiểu. Chẳng ai lại vứt chiếc xắc mới thế, không đời nào. Hay là có kẻ cướp chiếc xắc của người ta, lấy hết tiền rồi vứt ra đây? Nó định ném trả lại chỗ cũ cho xong, nhưng nó tiếc vì chiếc xắc đã cho nó một cảm xúc về cuộc sống. Nó cẩn thận nhìn khắp xung quanh một lần nữa. Chỉ thấy có hai chiếc xe hơi lướt qua, phía trong cái mặt lạ hoắc dửng dưng nhìn qua cửa xe. Nó đi nhanh về chỗ dựng xe máy của mình, cất chiếc xắc tay vào trong cốp.

Nó sướng đến nỗi quên mất nó đang thất nghiệp nửa tháng nay, cả thời hạn thuê nhà nó không nhớ đến. Nó đứng quan sát một lúc, chắc chắn xung quanh không có ai mới lên xe phi một mạch về hướng nhà trọ. Nó thuê phòng sống cùng với chủ nhà, sống ở cái môi trường như thế này càng làm cho nó không có dịp được tiếp xúc với con gái. Suốt 5 năm qua, nó làm việc ở phòng tài vụ của một xưởng sản xuất bột mỳ. Ngày nào cũng đi từ sáng sớm đến tối mịt mới về, nó ngại giao lưu bạn bè, vả lại cũng tại cái tật nói lắp nên khiến nó ngại tiếp xúc với mọi người.

Nếu như có ai đó nhìn thấy nó cầm chiếc xắc đắt tiền chạy về nhà vào lúc trời nhá nhem, nhất định người ta sẽ nghĩ có điều gì đó không bình thường. Về đến phòng trọ, nó đóng sập cửa lại, chốt cái then cửa, để cho trái tim đang thình thịch giảm tốc trở lại.

Căn phòng của nó ngoài chiếc giường cũ kỹ, đồ đạc chẳng có gì. Nó trèo lên giường, hồi hộp mở chiếc xắc ra, cảm giác như tim nó muốn chui ra khỏi lồng ngực. Bên trong có vài tờ biên lai thanh toán, một thỏi son và một chiếc cắt móng tay. Nó kéo khoá ngăn bên xem có còn gì không, thấy có mấy đồng xu lẻ, một tờ khăn giấy gấp vuông vức, nó lại hồi hộp một trận. Nó mở chiếc khăn giấy đưa sát lên mặt, mùi thơm sực lên mũi nó. Mùi thơm của chiếc khăn giấy kích thích nó, nó cảm giác như đang được ở bên cạnh con gái, được ôm ấp thưởng thức mùi hương toát da từ da thịt con gái. Nó cẩn thận mở nắp thỏi son. Một thỏi son màu đỏ nóng đã dùng hết một nửa. Nó nhè nhẹ giơ thỏi son lên miệng, một mùi thơm như kẹo ngọt xộc lên. Nó cảm thấy nó như đang bước vào một thế giới cảm giác khác.

Nó tận hưởng nốt buổi chiều còn lại, vắt hết sức tưởng tưởng ra một cô gái có hương thơm và tô son màu đỏ thẫm. Chắc hẳn đó phải là một cô gái trẻ tầm hai mươi tuổi, mảnh mai, xinh xắn, tóc đen cắt ngắn. Không biết như thế nào, nó lại nghĩ cô gái ấy có kiểu tóc ngắn. Đêm xuống, nó tưởng tượng cô gái đó nằm trên giường mặc bộ váy ngủ trắng mỏng tang mà nó nhìn thấy qua cửa kính cửa hàng quần áo trên phó. Nó hưng phấn, giở mình liên tục không sao ngủ được. Rồi nó ngủ thiếp đi lúc nào không biết.

Tỉnh dậy, thứ đầu tiên nó nhìn thấy chính là chiếc xắc nó để bên cạnh. Nó âu yếm vỗ về chiếc xắc một lúc rồi nhảy xuống giường. Trưa nay, nó phải đi phỏng vấn ở một cửa hàng thuốc, ngày mai một trung tâm thương mại cao cấp mới khai trương cũng hẹn nó. Nó hy vọng ít nhất có một nơi nhận nó làm việc. Giá như nó không có tật nói lắp, tìm được một công việc ở thành phố này với nó không khó, thậm chí còn tìm được việc lương cao, có cơ hội được tiếp xúc với nhiều người. Nó nghĩ tiếc hùi hụi.

Sau bữa sáng, nó quyết định mang chiếc xắc nhặt được nộp cho cảnh sát. Nó không muốn chủ nhân của chiếc xắc phải chờ đợi. Thế là nó cầm chiếc sắc đi, lấy hết sức can đảm bước vào sở cảnh sát.

“Tôi có thể gặp gặp cảnh sát trưởng được không?” Nó nói với anh cảnh sát trực ban. Nó rất ngạc nhiên vì vừa rồi nó không hề bị nói lắp. Anh cảnh sát trực ban chỉ về phía phòng làm việc của cảnh sát trưởng. Nó đi vào trong, cảnh sát trưởng dừng việc, ngẩng đầu lên nghiêm nghị nhìn xuyên nó bằng đôi mắt to, lông mày rậm.

“Tôi... tôi... tôi... nhặt được chiếc ...chiếc... chiếc xắc...” Nó lắp bắp, mặt đỏ rần.

“Anh nhặt được ở đâu?” Cảnh sát trưởng chậm rãi hỏi nó.

Nó phải mất mấy phút mới trình bày rõ việc nhặt được chiếc xắc, nhưng nó không nói nó nhặt từ hôm trước. Cảnh sát trưởng chìa tay cầm lấy cái xắc. Lúc đó nó mới ý thức được nó không muốn giao cái sắc nó ôm khư khư trước ngực cho một người vô tâm ấy. Nhưng nó không còn lựa chọn nào khác. Cảnh sát trưởng mở chiếc xắc, kiểm tra bên trong. Nó đứng cạnh đờ đẫn như một con gà tồ.

“Anh chắc chắn bên trong không có tiền?” Giọng ồm ồm của cảnh sát trưởng hỏi nó.

“Tiền... tiền...? Không... không có...” Nó lắp bắp trả lời, nghĩ đến chiếc khăn giấy trong chiếc sắc đặt dưới gối của mình, nó cảm thấy có một chút tội lỗi.

Cảnh sát trưởng nhìn nó vẻ nghi ngờ, “ ầm...ờ...” lẩm bẩm không rõ điều gì trông mồm

“Cứ tạm thời như thế đã. Thời gian gần đây, chúng tôi liên tục nhận được nhiều tin báo bị cướp túi xách. Chúng tôi sẽ thông báo cho các cô gái đã khai báo đến nhận lại.” Cảnh sát trưởng nói với nó.

Anh cảnh sát trực ban ghi lại tên và địa chỉ của nó.

Nó rời khỏi sở cảnh sát, vội vàng đến chỗ phỏng vấn. Chủ hiệu thuốc tây tiếp nó hai phút rồi để nó về vì chê nó nói lắp.

Nó lại quay trở về căn phòng trọ, vui với nó chỉ có chiếc giường cũ kỹ và chiếc khăn giấy thoang thoảng mùi thơm. Tối hôm đó, nó đặt chiếc khăn giấy lên ngực ngủ ngon lành cho đến sáng. Chiều nay là lịch phỏng vấn của nó ở Trung tâm thương mại. Suốt cả buổi sáng, nó cứ loanh quanh trước cổng sở cảnh sát, mắt không rời từng cô gái đi vào.

Đã qua hai giờ chiều, cuối cùng sự theo dõi của nó đã có kết quả. Một cô gái dáng cao, mảnh mai bước từ trong xe hơi ra. Nó chỉ thấy được cô gái ấy để tóc ngắn màu hạt dẻ, chiếc kính râm to che hết nửa khuôn mặt. Nó cảm thấy dòng máu trong huyết quản đang chảy dần dật. Đúng là cô ấy rồi. Nó cảm thấy hình như nó quen cô gái đó. Nó sốt ruột đi lại dưới gốc cây. Khoảng gần nửa tiếng sau, cô gái mới từ trong sở cảnh sát đi ra, trên tay vẫn xách chiếc túi ni lông to mà lúc trước xách vào. Có lẽ bên trong túi ni lông đó có đựng chiếc xắc, nó nghĩ thế. Nó vội nhấn đề nổ máy, bám theo chiếc ô tô. May mà đường vắng. Nó để ý cô gái đi vào trong một toà nhà cao tầng. Nó dựng chiếc xe máy, chạy theo vào. May mắn nó được đi chung cầu thang máy với cô gái. Cô gái bỏ chiếc kính râm xuống, nó liếc vụng, dễ đến 27, 28 tuổi, nhưng không xinh như nó tưởng tượng. Cô gái lườm chiếc găng tay mòn nát và đôi giày rách của nó, rồi khinh khỉnh quay đi. Cô gái lên tầng 7, nhưng mùi nước hoa vẫn còn nông nặc cả buồng thang máy. Nó quay trở lại tầng 1, với tâm trạng thẫn thờ như bị mất đồ.

Nó chợt nghĩ ra buổi phỏng vấn quan trọng. Nó chẳng để ý đến mọi người chửi mắng nó, nó tăng ga vụt qua tất cả các xe nhưng vẫn bị muộn 15 phút. Đầu tóc nó rối tung, ngơ ngác bước vào phòng phỏng vấn.

Người phụ trách phỏng vấn là một phụ nữ có tuổi. “Bị tắc đường à?” Bà ấy nhìn nó vẻ thân thiện hỏi nó.

Nó gật đầu, vì nó sợ mở miệng lộ tật nói lắp. Bà ấy cầm hồ sơ lý lịch của nó đọc chừng mấy phút.

Vì sao cháu rời bỏ công ty cũ? “Bà ấy hỏi. “Tôi có đọc cháu làm ở đó đã 5 năm.”

“Hai năm nay, công ty cháu kinh doanh không tốt, cắt giảm nhân sự nhiều.” Nó giải thích rất trôi chảy.

Bà ấy đồng tình gật đầu.

“Công việc trước đây cháu làm rất tốt.” Bà ấy đọc được từ trong bộ hồ sơ của nó. Cuối cùng nó được nghe câu nói mà sau mỗi lần phỏng vấn nó khao khát được nghe. “Tôi tin rằng chúng tôi sẽ tuyển dụng cậu.”

“Cảm ...cảm...ơn... bác...bác...” Nó lắp bắp nói.

Bà ấy ngạc nhiên ngẩng lên nhìn nó, sau đó động viên nó bằng một nụ cười.

Nó nhìn bà ấy như nói cảm ơn rồi rời khỏi phòng. Nó nghĩ thầm, cho dù nó không biết chủ nhân của chiếc xắc là ai, nhưng nó chắc chắn chiếc xắc đó đã mang lại cho nó may mắn, nó đã tìm được việc. Nó lên xe máy, nhấn đề sung sướng lướt đi.
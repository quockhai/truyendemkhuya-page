Vậy là theo lệnh của Tổng biên tập tôi đã lên đến nhà máy Z. Tiền thân của nhà máy Z chính là cái xưởng sửa chữa nhỏ thời chiến đóng ở cua chữ Z. Nhưng như ông Lộc- Giám đốc- nói: “ Nếu chúng nó về đủ cả vợ cả chồng thì nhà máy này bằng ba nhà máy, bốn quả đồi này đã thành những làng quân nhân...”. 

Nhưng hết chiến tranh những người yêu chưa về, không về với người yêu, thế là họ lại theo ông lên miền trung du này, về với nhà máy Z để làm việc và để đợi chờ. Dù gì cái nhà máy Z cũng là địa chỉ dễ tìm về hơn một miền quê nào đó đã vội vã cho nhau. Dù gì ở đây còn có chị có em cùng cảnh. Về quê ngày ngày nhìn lũ em, lũ cháu, đánh con chửi chồng rồi lại hủ hỉ nịnh chồng ru con thì chịu sau cho thấu.
Những năm chiến tranh đã có lần tôi đến xưởng Z viết bài và đã được làm việc với ông Lộc. Giờ gặp lại người cũ với những công việc xưa, nên việc thu thập tài liệu khá nhanh, đang tính về thì ông Lộc nói.

 - Ấy chết, tôi đã lỡ nói với các o dưới phân xưởng có nhà văn, người quen cũ về thăm nhà máy, các o muốn anh ở lại trò chuyện giao lưu.

- Nhưng tôi phải về để viết bài phục vụ hội nghị. Và thú thực tôi mới cưới vợ...

- Hôm nay với mai là mấy. Anh mới cưới vợ. Đàn ông chúng mình muốn là cưới. Anh xem ngày ấy đông vui là thế, ai đến cũng muốn nấn ná ở lại cua chữ Z. Còn giờ đây... tội lắm. Nhà máy hoàn thành tốt nhiệm vụ cũng là vì chị em biết làm gì ngoài công việc chuyên môn nữa đâu. Họ tìm vui trong công việc. Họ làm việc để đỡ nhớ, làm để mà quên...

- Thôi được rồi, ông đã nói vậy thì tôi ở lại, nhưng biết nói gì bây giờ, với các o, dễ đụng chạm lắm.

- Tuỳ anh, nói gì cũng quý... ở đây họ thèm nghe tiếng... đàn ông.

Thế là đêm ấy lỗi hẹn với người vợ mới cưới, lỗi hẹn với tuần trăng mật, tôi ở lại với các o nhà máy Z... Khi câu chuyện trong buổi giao lưu đã mặn, không khí chân tình, cởi mở và dần ấm lên. Tôi đã phạm phải một sai lầm chết người, tôi đã buột miệng kể câu chuyện của tôi và người lính lái xe năm xưa, nói lên những phán đoán của mình và còn ngu đến mức là hỏi: Không biết ngồi đây có ai là người yêu của anh ấy?! 

Khi nghe tôi hỏi, ban đầu cả hội trường lặng phắc, lặng để nghe cái lạnh chính Đông miền trung du ngấm sâu, day dứt, nhay nghiến từng thớ thịt, sau đó những mái đầu dần ngoái sang nhau, rồi những cặp mắt thảng thốt nhìn nhau, rồi tay tìm đến tay, những giọt nước mắt bắt đầu lăn tròn rồi nhoè ra răn reo lan trên má.  Và bỗng nhiên vỡ oà ra khi hơn chục o vụt đứng dậy, ôm mặt lao ra cửa. Tôi ngồi như bị đóng đinh trên ghế, tràn ngập trong hồn là sự trống vắng, ăn năn. 

Vô tình tôi trở thành kẻ độc ác, vô tình tôi cày cuốc lên vết thương sâu mà biết rằng không bao giờ lành lại trong mỗi o. Tôi cứ ngồi bất động thế mắt dõi mông lung về một chốn vô định phía ngoài ô cửa tối om om của đêm cuối tháng. Tôi tê liệt hết mọi cảm xúc, không còn lạnh, không còn đau, không còn cả ăn năn hối lỗi. Tôi cứ ngồi vậy cho đến khi ông Lộc đến bên vỗ nhẹ vào vai tôi nói:

- Thôi về đi chú, không sao đâu, rồi ngày mai mọi việc lại về với nếp cũ, biết không thể quen nhưng tôi tin các o chịu được, thôi về đi, đường thì dài mà ai cũng phải sống...về đi.

Tôi bước như vô thức theo ông Lộc về phòng, ở đó trên chiếc bàn mộc đơn sơ của Giám đốc, ông đã cho dọn sẵn một đĩa lòng lợn còn vương mấy sợi khói cạnh đĩa rau thơm xanh rợp mắt bên chai rượu nút lá chuối hình như cũng đang còn ấm nóng. Tôi và ông ngồi vào bàn, không ai nói với ai một lời, mỗi người đều đang đuổi theo từng suy nghĩ của mình. Không nói, nhưng chắc ông cũng đang nghĩ về những năm tháng ở cua chữ Z, những năm tháng ở chiến trường như tôi. 

Trời giữa Đông vẫn rét ngọt, càng rượu vào càng thấy rét, ngoài kia hình như đêm còn gió, mấy cành cọ già đang gãy vào đêm những tiếng sột soạt như váy áo người đi. Phòng có hai người mà sao trống vắng lạ, cửa đóng chặt mà hình như gió vẫn đang còn lùa tứ phía, gió như những đêm ở rừng. Nếu giờ đây có một vài tiếng mang tác, có mấy tiếng chim bắt cô trói cột, có vài doi sáng đèn dù thì có khác gì cái thời ở cua chữ Z. Không biết dưới kia, trong cái màn đêm tịch mịch đen đặc như có thể xắt ra được từng miếng ròng ròng lạnh này, các o ai thức, ai ngủ, mười o vừa chạy về kia đã thay mấy lần áo gối trong đêm? Vẳng đâu đó hình như có tiếng ngâm một bài thơ của Hoàng Cầm.

Đêm xuống

Làm lầu hoang

Trò chuyện gì ai đâu

Mồ tháng giêng mưa song

Đằm ca dao sáo diều chiều tím lịm lưng trâu

Bưởi Nga My sao mẹ bắt đèo bòng 



Lạy trời chương trình tiếng thơ thôi đừng phát nữa, những bài thơ yêu đương với nhung nhớ theo sóng phát thanh bay đến nơi đây trong cái đêm Đông này chỉ như là những mũi khoan sâu vào cái đau, cái rét, cái tủi, cái hờn, cái cô đơn... Và phía đồi xa kia nữa, hai cây cọ già cứ sóng đôi mãi mà chi tình. Rồi trong thung xa hình như có tiếng trẻ con gắt sữa khóc đòi mẹ... Chúng tôi cứ ngồi im như hai quả đạn pháo tịt cho đến khi chai rượu vơi đi còn non nửa thì ông Lộc lên tiếng:

- Ngày đó anh đã viết bài bút ký rất hay về xưởng của chúng tôi, và sau anh còn có nhiều nhà văn, nhà  báo về viết nữa. Các anh đều nói nguyên nhân để xưởng chúng tôi không ai vi phạm kỷ luật về chuyện nam nữ, xưởng chúng tôi hoàn thành tốt nhiệm vụ vì tôi là người giáo dục chị em tốt, chị em thấm nhuần tư tưởng, chị em gác chuyện riêng tư để một lòng cho tiền tuyến... đúng cả nhưng chỉ một phần.

- Chẳng lẽ ông đã... đã cho... nhưng sao không có ai...

- Đúng, ngày đó để hoàn thành nhiệm vụ, tôi đã phải làm điều đó, tất nhiên không phải với tất cả, chỉ với ai tôi biết nếu kiềm chế quá họ sẽ phát bệnh... nên đến bây giờ, cũng chính vì điều đó làm tôi day dứt. Không có người lính nào đi qua chiến tranh trở về mà không có nỗi ám ảnh, anh có, tôi có và bao bạn bè đều có. Chỉ có điều cái sự ám ảnh đó nó không hề giống nhau. Với tôi ám ảnh lớn nhất và nhiều khi tôi mơ ước, giá mà ngày đó quân của tôi bị vi phạm kỷ luật về chuyện trai gái nhiều hơn...

- Nếu vậy thì lấy ai mà phục vụ chiến đấu...

- Tôi cũng đã nghĩ như anh và nhiều người đều nghĩ vậy. Nhưng chiến tranh đáng lẽ không nên có bóng dáng của đàn bà. Nhìn họ sống, họ chiến đấu, họ hy sinh... tôi không thể chịu được. Đối với đàn bà, thiên chức làm mẹ là cao cả nhất, đáng tính nhất. Nếu mất đi thiên chức đó là nỗi mất mát lớn nhất đối với họ, không có chiến công nào bù đắp nổi...

- Nhưng ngày đó họ vẫn được yêu và có những cuộc hẹn hò, cậu lính lái xe và một cô gái trong xưởng của ông đã...

- Vâng, và còn nhiều đôi nữa được như vậy, tôi đã nói mà, tôi không dám cấm tiệt các o... nhưng đơn vị tôi không có cô gái nào có được hạnh phúc làm mẹ. Đó chính là nỗi ám ảnh thường trực trong tôi kể từ ngày hết chiến tranh. Người yêu không về nhưng nếu họ có được những đứa con...

Nói đến đó ông tu nguyên một tợp rượu và hai hàng nước mắt cứ tự nhiên răn reo trên má. Ông đang tự ăn năn mặc dù chả ai trách ông điều đó. Cái quan niệm chiến tranh không cần có bóng dáng của phụ nữ cũng chính vì ông quá ám ảnh với những mất mát của những người lính nữ dưới quyền quản lý của ông. Ông cũng như tôi, như bạn bè tôi cái quãng đời còn lại là quãng đời tua lại những gì đã trải qua trong chiến tranh. Nhưng tôi tin, dù có những nỗi ám ảnh nhưng mỗi người lính đi qua chiến tranh đều thấy tự hào là chúng tôi đã chiến thắng, dân tộc chúng ta đã chiến thắng. Còn cái nỗi ám ảnh là không tránh khỏi, nhất là ngày ngày chứng kiến những bước đi vùn vụt của thời gian.

- Ngày hôm nay anh đã chứng kiến tất cả, nếu là tôi thì anh sẽ nghĩ gì - không cả lau nước mắt, vừa rót rượu vào ly cho tôi ông Lộc vừa nói- Tôi đã tạo điều kiện cho các o ra ở gần đường... và ở đó đã có tiếng trẻ bi bô... nhưng nhiều o nhất quyết không chịu đi... anh... anh có thể... chỉ một lần...

- Ý ông nói - tôi trợn tròn mắt nhìn ông, nhưng ông không có biểu hiện gì, chắc ông đã đoán biết được phản ứng này - Thôi tôi xin ông, tôi là người chứ nào phải con ong...

- Tôi biết anh sẽ từ chối. Từ chối đó là điều dễ làm nhất khi gặp phải chuyện khó làm. Mời anh ở lại là tôi có ý này, là nhà văn, đã từng gặp họ trong chiến tranh, giờ gặp lại tôi tin... sở dĩ tôi nhờ anh chứ không nhờ ai khác, bởi tôi biết các o có thể chết già chứ người không xứng đáng thì đừng hòng, chập tối nay, trước khi ngồi với anh tôi đã nói chuyện với o ấy. Khóc nhiều nhưng đã bằng lòng, chỉ bắt tôi phải hứa không được cho anh biết o ấy là ai... o ấy sợ, nếu biết, rồi anh lại tìm lên... gia đình anh sẽ tan nát... anh hứa với tôi đi, chuyện này: “Sống để trong dạ, chết ngạ bên mồ.”

Nghe ông Lộc nói, tôi không còn thấy biểu hiện gì, cứ ngồi uống hết ly rượu này đến ly rượu khác. Và rồi tôi không còn thấy lạnh nữa, tôi nhìn ông Lộc mờ ảo sau đĩa lòng lợn còn đầy tú hụ nay đã lạnh ngắt như sắp đóng thành băng. Và rồi hình như ông Lộc đã dìu tôi đi, trời vẫn tối, vẫn có thể xắt ra được từng miếng ròng ròng lạnh, phía đồi xa hai cây cọ già vẫn tạc vào đêm ước mong đôi lứa... Bước chân xiêu vẹo và ông Lộc đã đưa tôi đến dãy nhà nào, vào phòng nào tôi cũng không biết. Và tôi đã làm những gì trong đêm... duy nhất tôi chỉ nhớ nước mắt của cô ấy và tôi đã thấm ướt mặt nhau, chảy tràn trên gối. Nếu chúng tôi có con chung thì chắc đứa con đó được sinh ra từ nước mắt... mọi vật đều được sinh ra từ nước, nhưng chắc chắn không phải là nước mắt... Sáng ra tôi thấy mình đã nằm êm ấm ở phòng khách nhà máy. Rồi ông Lộc đến cùng đi ăn sáng. Trong bữa ăn ông không hề hỏi, không hề nhắc đến chuyện tối qua, cứ như là không có thật, không hề có một đêm như thế...

                                                                                    *****
Tôi viết xong bài bút ký kịp cho hội nghị điển hình tiên tiến. Hôm ở hội nghị, tôi và ông Lộc có gặp nhau, ông cũng không hề nói gì. Tôi những mong ông sẽ thông báo cho tôi biết là cô ấy như thế nào? Sau đêm ấy rồi cuộc sống cô ấy ra sao? Liệu có ra hoa kết trái không? Chắc biết tôi nóng lòng muốn biết, suốt hội nghị ông luôn lánh mặt tôi, chỉ đến lúc trước khi lên xe trở về nhà máy, ông mới bắt tay tôi thật chặt và nói:

- Tuyệt lắm.

Tôi chưa kịp hỏi bài bút ký tuyệt hay là cái đêm hôm ấy đã để lại một kết quả tuyệt vời thì ông đã đóng sầm cửa và xe đã lao đi. Sau đó có một tin động trời. Ông Lộc bị cách chức giám đốc và cho về nghỉ hưu trước tuổi vì vi phạm vấn đề đạo đức. Thế là tôi lại vội vã lên với ông. Ông không giải thích gì nhiều, chỉ nói:

- Là thằng lính với nhau, anh có tin tôi không?

- Phải đâu một mình tôi với ông là lính, họ cũng là lính nhưng họ có tin ông đâu.

- Tôi chỉ hỏi anh kìa.

- Không tin ông thì tôi chả tìm lên đây là gì. Nhưng như thế là thế nào mới được chứ...

- Chả có chuyện gì cả, trước đây tôi được khen nhiều vì biết canh không cho chị em đẻ, giờ thì... Nếu chỉ vậy thì cũng không đến nỗi phải về vườn đâu. Việc nó thành lớn khi tay phó của tôi nó kiện cáo lung tung. Cũng tại tay phó nó muốn làm cái... việc nhân đạo như ông nhưng bị các o tránh như tránh hủi. Nó đâm ra tức... Thế là kiện, thế là tôi xin về hưu. Thôi cũng đỡ áy náy được phần nào. Được cái vợ con nói hiểu mình, giờ có anh hiểu nữa, thế là tốt...

- Ông nói thật... đêm đó - tôi ghé miệng hỏi nhỏ vào tai ông - Ông chỉ cần nói tôi có để lại giọt máu của mình trong đêm đó không. Tôi không bắt ông phải cho tôi xem mặt người đàn bà đêm đó và con của cô ấy đâu...

- Anh là thằng lính, tôi thằng lính và cô ấy cũng là thằng... à quên, là o lính. Chúng ta đều đã hứa. Anh hãy quên đi điều ấy như ông đã quên những việc làm ân nghĩa trong đời. Chỉ được quên đi những việc làm ân nghĩa, còn nữa phải nhớ hết, nhớ về những ngày tháng gian nan xưa để mà làm nhiều việc tốt.

Biết chả khai thác thêm được gì ở ông, nhưng cứ thành lệ, mỗi năm đến mùa Đông chín là tôi lại bươn bả trong cái rét cắt da để lên miền trung du với ông, nhiều khi một mình tôi đi thơ thẩn quanh nhà máy, quanh những ngôi nhà mới khang trang vừa mọc lên trên nền mấy dãy nhà cấp bốn khi xưa. Tôi đi quanh những ngõ làng của công nhân nhà máy để như mong gặp một khuôn mặt mà tôi chưa từng... biết mặt, mong gặp một giọng nói mà tôi chưa từng nghe, mong gặp những dòng nước mắt nhạt nhoà đôi má. Nhưng nước mắt thì của ai mà chả giống nhau, nó cứ chảy xuôi, chảy như mong vợi bớt nỗi buồn, nhưng hình như nước mắt càng chảy thì nỗi buồn càng dâng cao.
(Nhớ Thảo, viết tặng Châu Giang)

Tôi có người bạn thân từ lớp một tới lớp chín tên là Minh. Minh Tầu Hỏa. Chẳng là cha Minh làm nghề lái tầu hoả nên tụi học trò cùng học lớp Năm trường tư thục Vạn Hạnh lấy cái nghề của cha mẹ mà réo, thay cho sự trùng tên với ba thằng Minh khác. Tên Minh quả lắm, cho tới khi bọn tôi học lớp 9 trường Trưng Vương, chỉ riêng khối Chín đã có tới bốn thằng Minh, nhưng chỉ một bạn tôi có biệt danh Minh Tầu Hoả.

 
 
Minh đậm, thấp và chắc chắn. Nó thuộc con nhà lao động. Cha là công nhân, mẹ ở Hà Nội mà vẫn làm vườn với khoảng đất nhoi lên giữa cái hồ sau nhà. Minh không nghịch, hiền, nên chả thích học võ vẽ gì. Tụi tôi quen nhau từ trường tư thục Vạn Hạnh, nhưng tình bạn chỉ thực gắn kết, khó dỡ bỏ, có lẽ suốt đời, sau thử thách năm ấy… 

Năm ấy cả lớp 9A Trưng Vương của tôi tới hồ Hale có bài ngoại khoá buổi sáng. Bấy giờ đã gần trưa, chỉ còn hai tổ học trò quanh hồ. Cuộc va chạm diễn ra khi một thằng nhóc ra dáng anh chị túm tóc một bạn gái ở tổ của Cường Bạch Mai. Cường la lớn, thằng kia bỏ tóc nó ra. Song Cường không dám xông vào cứu bạn. Tôi chạy tới. Tôi đẩy thằng mất dạy ra. Nó gườm gườm. Tóc nó vàng cháy, mặt đầy tàn hương, nhỉnh hơn tôi nửa đầu. Tôi gằn giọng: “Sao mày túm tóc nó?”

Chát, thằng kia không thèm trả lời, thoi cho tôi một cú đánh ngắn, rất nhanh vào mồm, trong tiếng cười hô hố của một lũ nhóc tuổi sàn sàn như nó đang đứng lố nhố phía sau. Cú đánh nhanh, như tia chớp, bất ngờ quá, làm tôi không tránh kịp, cũng không thể ngờ, nó lại dám chọi mình trước.

Tôi quệt máu. Tại trường có thằng Chân là em võ sư Dương Như Hải, hay tổ chức tập bốc-xơ từ lớp Tám. Tuần nào bọn tôi chả đánh cặp đôi vài lần ở nhà Chân hay Thảo Bạch Mai. Tôi nổi tiếng với các cú thọc thẳng. Bỏ cái cặp xuống bên trái. Bất ngờ, tung cú tay trái đua-rech và trúng. Nghiêng người sang trái, lại lắc phải, bổ sung liên tục hai nhát xuynh. Những cú đánh cầu vồng từ cao dội xuống với tốc độ cao rất hiệu nghiệm. Không ngờ một đứa nhỏ con hơn nó, tưởng đã toé máu mồm thì sợ hãi bỏ chạy, lại ra đòn như bão vậy. Nó bị bất ngờ, không biết lối nào mà lần, lùi sát hồ. Tôi chỉ chờ thế, xoay người lao thẳng vào, tung cú đạp chéo với cườm chân rất mạnh, trúng giữa ngực buộc nó ngã bổ chửng, gục xuống. Thấy tướng rơi vào thế thua, tôi lại đang hăng, liên tục đánh móc, tất cả tụi bụi đời xô vào bao lấy tôi. Không ngờ bị tứ phía tấn công, tôi đành hai tay ôm đầu che gáy, nhảy sang bên. Không một ai trong hai tổ học sinh dám xông vào. Thằng bạn thân hơn cả Minh Tầu Hoả, tên Cường, chỉ đứng nhìn la lên: “Ơ kìa, sao lại chơi hội đồng”. Cường là đứa cùng học bốc-xơ với tôi ở nhà Thảo và Minh Xồ Bạch Mai. Chỉ mình Minh Tầu hoả, vâng mình nó, chả võ vẽ gì, thường ngày hiền lành rủ rỉ như cục đất, chỉ với chiếc cặp da có nẹp những miếng sắt ở góc cặp đã xông vào giải cứu. Những cú quăng, lăng, đập với cả hai tay, cũng kiên quyết và quá mạnh làm bốn năm thằng bụi đời kinh sợ chạy mất, để tôi rảnh tay tiu nốt đối thủ của tôi, quẳng nó xuống hồ. Trẻ ranh, học trò hồi ấy đánh nhau với bụi đời đường phố là thường.

Giải tán. Ai về nhà nấy. Bạn gái cùng học lí nhí cám ơn. Minh hỏi “Thọ đau không” và đưa tay quệt vệt máu còn trên miệng tôi. “Đứt lợi tí thôi mà!” - Tôi nói. “Thôi về nhà tớ, hôm nay mẹ tớ có lòng lợn ông bô lấy từ căng tin đoạn đầu máy Hà Nội. Minh Tầu Hoả cười rất tươi, vỗ vào vai tôi.

Tôi theo Minh về phố Khâm Thiên ngay gần đó. Nhà Minh ở sâu trong ngõ. Đây là lần đầu tiên, tôi tới nhà Minh từ trưa và ở lại tới chiều. Nắng tháng bảy dữ dội. Trong căn nhà phía sau, kế vạt đất là cả cái hồ lớn. Mẹ Minh dọn cơm ra và cô em gái chạy từ đâu về, hớn hở giúp hai ông anh dọn bát đĩa. Em gái tên Thảo, kém chúng tôi hai tuổi. Thảo thường buộc tóc đuôi gà, vểnh lên sau gáy. Tôi thường trêu, cái đuôi gà của em thật giống y như đuôi con gà trống nhà anh. Thảo cũng hiền như anh nó, chả giận tôi bao giờ. Thảo ngước mắt nhìn tôi khâm phục, y như trước một anh hùng, khi Minh kể cho nó nghe trận chiến vừa rồi và không quên nhắc, cấm không được mách mẹ đấy, rồi tí nữa anh cho ra đảo.

Đảo ư? Thảo nhảy lên, nó chạy đi một lát và quay lại với cái lẵng nan mây tre nhỏ.

 
 Ảnh: st

Chúng tôi ra hồ. Minh kéo cái bè chuối và đặt Thảo ngồi lên. Hai đứa tụt quần dài, bơi chầm chậm. Bấy giờ, sau phố Khâm Thiên còn cơ man đất ruộng và hồ. Cái cồn giữa hồ là đất của nhà Minh. Trên đó, mẹ Minh trồng bốn mùa đủ loại rau, đỗ, lạc, rau cải, cả khoai nước, khoai lang v.v... Tôi đã ra đấy vào mùa đông năm trước, hái ớt và cắt ít lá sả cùng Minh. Nhưng cũng chưa có dịp tham quan hết mảnh đất tới hơn ngàn vuông mét ấy. Chúng tôi hái ớt, rau muống và cả những quả cà nho nhỏ màu trắng, với đám rau ngót xanh rượi, lá óng lên như phết mỡ, ánh lên trong ráng chiều. Chợt Thảo quay lại kéo tôi đi, để mặc Minh hái đỗ. Hoá ra bên vạt đất sát hồ có một thứ cây rất xanh, rất lạ bò lan ra ven hồ, leo lên cả đám rào tre, ngọn vươn lên chua chúa.

- Anh nhìn này - Bàn tay nhỏ bé với các búp tay thon thon của em Thảo bới các khóm lá và bứt ra những quả nho nhỏ to hơn hạt lạc tròn xoe bọc lớp vỏ bốn cánh mỏng như giấy. Thứ quả tôi chưa nhìn thấy bao giờ. Những quả ấy mọc ở kẽ lá, bị che trong đám lá xanh rợm, rậm rì thì khó ai có thể nhận ra.

- Ăn thế nào em? - Tôi hỏi.

Thảo tách lớp vỏ giấy ra, trong tay hiện một quả tròn xoe như viên bi màu vàng chanh bóng.

- Ăn đi, em hái nữa cho anh.

Nó đưa vào miệng tôi. Tôi cắn. Trời! Chua! Tôi kêu.

“Anh ngậm miệng lại nuốt nước bọt đi!”. Thảo ngước đôi mắt đen láy, đưa hai tay bưng lên trước mắt tôi một bụm quả vàng ươm như nắng đầy. Đôi đuôi sam của em vung vẩy. Tôi làm theo Thảo nói như cái máy. Ồ, ngọt nhỉ. Ngọt hậu! - Tôi nghĩ.

Tôi ngồi ở mô đất bên cạnh ăn chùm quả Thảo hái, khi em thoăn thoắt hái các đọt ngọn và lá lạc tiên cho vào giỏ nhỏ. Hái làm gì nhiều thế? Tôi hỏi. Thảo cười, cho mẹ nấu canh để khỏi mất ngủ anh ạ. Mẹ em hay mất ngủ lắm. Hóa ra mẹ Thảo sau khi sinh Thảo thường xuyên mất ngủ, tới tận hai ba giờ sáng mới đặt mình. Bà đã châm cứu, uống thuốc và một thầy lang nào đó đã mách bà dùng thứ lá này sau thời gian chữa trị với châm cứu. Thảo tới bên tôi với cái giỏ đầy lá và ngọn lạc tiên non. “Canh ngọn rau này ngon đấy. Mẹ em ăn quen rồi. Nó làm mẹ em ngủ ngon và sâu hơn...”. Một giấc ngủ ngon và sâu, sâu nhẹ nhàng, sau này nghe cha tôi giảng giải, tôi mới hiểu giấc ngủ sâu và nhẹ, nó cần thiết ra sao cho những ngày người ta lao động miệt mài để tái hồi sinh lực.

Ngày 2 tháng 8, năm 1964, sự kiện Vịnh Bắc Bộ xảy ra, đến 5-8 Mỹ ném bom miền Bắc. Triển lãm máy bay siêu thanh của Mỹ bị bắn rơi trưng bầy đủ loại ở Vân Hồ. Những tin đồn về chiến tranh loang ra và người ta bắt đầu đào hầm trú ẩn. Hà Nội được đặt những ống thống làm cái hầm trú máy bay dọc ven Hồ Gươm và các tuyến phố. Không khí lớp học có vẻ rã đám. Một ngày Minh bảo, mai tao thôi học. Tôi giật mình? Sao lại thôi? À, ở đoạn đầu máy Hà Nội lấy thợ lái tầu. Tao bỏ học. Quyết định rồi. Quyết định rồi? Tôi hỏi lại. Thứ Bẩy  tôi tới nhà Minh. Hoá ra cha cậu quyết định điều ấy. Cha truyền con nối. Chiến tranh tới rồi và nghề lái tầu hoả cũng đâu dễ mà xin vào, phải là con em của ngành.

Minh đi học phụ lái, đốt lò và học lái tầu hơi nước. Tôi nghe nó kể, tưởng  tượng ra cảnh nó đội mũ xanh, mặt lấm lem đầy than và cái tầu thở phì phì tung khói trắng. Tao đã đi khắp đất nước! - Minh tự hào bảo. Thi thoảng nó tạt qua nhà tôi cho cái phiếu phở. Những học viên thực tập trên các chuyến tầu được ăn hơn mọi người, trước khi đi vào đạn bom, vào cõi chết, được ăn sáng với hai bát phở thịt lợn ở căng tin dọc đường Nam Bộ, dãy nhà từ ga Hàng Cỏ xuống sát đầu Khâm Thiên.

Rồi chiến tranh. Tháng Bẩy năm 1965, lớp tôi hơn mười thằng lên đường, trong đó có tôi. Trước khi đi hai ngày, tôi tới nhà Minh. Minh không có nhà. Anh Minh đang phụ chạy tầu ở khu Bốn! - Thảo nói với tôi. Em sẽ sơ tán chứ? Không, bố em lái tầu, anh Minh lái tầu, em và mẹ phải ở đây phục vụ. Tôi nhìn cái giỏ xanh ăm ắp lá và dây lạc tiên… Dạo này mẹ Thảo lại mất ngủ. Thảo tiễn tôi ra ngoài ngõ. Tôi quay lại vẫy tay, thôi em vào nhà đi, anh đi đây. Ngoan nhé! Nó như muốn khóc. Đôi mắt to, đen nhay nhánh, nhìn tôi không chớp. Tóc đuôi sam tong teo. Nó đứng chỉ tới ngực tôi. Năm ấy Thảo 13 tuổi.

Tôi lên đường.

Chiến tranh như cơn lốc cuốn ào ào chúng tôi đi trên khắp các mặt trận quanh Hà Nội, Hải Dương, Thái Nguyên, Phú Thọ… Rồi khu Bốn ác liệt ngập trong khói bom và cái chết.

 	
 Ảnh: st 	
Năm 1967, tôi chẳng thể ngờ lại gặp Minh ở Nghệ An. Chuyến tàu của nó bị đánh tan. Phải cắt đầu máy khỏi toa cứu đầu tàu. Địch đuổi đầu tàu bắn riết. Thợ chính tử thương chết gục bên tay lái. Thợ phụ thứ hai cũng chết. Minh cầm tay lái thục mạng kéo nước hết cỡ. Nó may mắn thoát chết, chỉ bị thương nhẹ ở cánh tay và tụi tôi gặp nhau trong bệnh viện dã chiến Hưng Nguyên, khi tôi cũng bị hai phát bi vào bọng chân. Minh chạy tầu cũng như đi chiến đấu, song may mắn hơn tôi, nó vẫn được về nhà sau những chuyến đi dài, có khi hai ba tuần ở tuyến trong. Thảo và mẹ thế nào? - Tôi hỏi. Minh lấy trong túi ra cái ảnh đen trắng. Trong ảnh, cô gái 16 xuân xanh cười. Mắt to và đẹp. Môi nó bắt đầu ra dáng môi con gái. Nét nào ra nét đấy. Cái mũi hơi hếch tinh nghịch, nhưng sống mũi thẳng, dài. Vẫn hai cái búp tóc đuôi sam. Có vẻ nó lớn nhỉ! - Tôi bảo. Ừ, không có nó có lẽ gay Thọ ạ. Bây giờ nó ở nhà thay mẹ làm vườn, nuôi lợn. Mẹ Minh yếu lắm. Mất ngủ lại hen nữa. Bỏ học rồi. Chiến tranh mà. Tôi im lặng.

Chúng tôi được ở một tuần với nhau ở bệnh viện Hưng Nguyên. Máy bay định vẫn bay đen trên bầu trời và, thành phố Vinh chìm trong bom đạn.

Chiều chiều, qua những đụn khói đen và trắng cuộn lên, hai đứa yên lặng nhìn về phương Bắc.

Hà Nội giờ đây ra sao? Tôi nhớ cái cầu thang nhỏ mà mợ tôi vẫn chui vào, mỗi khi còi thành phố tại Nhà hát Lớn rú lên.

Minh bảo, lần trước tao về, Thảo nó bắt tao đào cho nó cái hầm lớn ở ngay nền nhà, ken tre dầy, chả hiểu để làm gì. Mày biết không, dạo này nó đi học đàn buổi tối đấy.

- Nó muốn làm nghệ sĩ hả? - Tôi hỏi.

- Không, nó sinh hoạt chi đoàn, thi thoảng đi uý lạo với khu đoàn. Nó hát hay lắm mày ạ, đôi khi tao lại thấy tụi nó, mấy đứa ở khu đoàn, ngồi hát ở ngay bên cái hầm tao mới đào - Minh bảo.

Rồi Minh lại khoác ba lô về với đoàn tầu của nó. Công việc lái tầu thực nguy hiểm. Tôi còn bắn lại được kẻ muốn giết mình, chứ công nhân lái tầu hơi nước ì à ì ạch... chỉ có nước là chạy thục mạng trên đôi ray mong manh. Minh từng kể, có hôm định đánh tan đường ray, tầu kẹt ở giữa, tiến lui đều không được, khát và đói vài bữa là thường.

Tự nhiên thấy luyến tiếc thời học trò thế, bọn tôi có suy nghĩ gì đâu, hạnh phúc ở vô tư!

Hôm Minh sắp đi, tôi lục ba lô lấy ra cái lược Duay-ra, hì hụi chuốt lại răng lược. Tôi lấy mũi dao găm khắc ba chữ: Tặng em Thảo. Ngần ngừ tí tôi khắc thêm: Chiến thắng. Đúng là, khi tôi khắc, tôi có nhớ tới cái đuôi sam vắt va vắt vẻo như cái đuôi con gà trống của Thảo. Ừ, mỗi bận con gà ra sân vươn cổ: Ò í o… o... o. Cái đuôi lắc la lắc lư...

- Chả có gì, mày về đưa cho nó - Tôi đưa cho Minh hai mảnh dù pháo sáng. Hai phong lượng khô PA 702 - Cái này, tôi chỉ hai miếng dù, mày đưa cho mẹ tao một cái, mẹ mày một. Lụa thật đấy. Đây nữa! Tôi đưa Minh cái lược chuốt rất kĩ từng răng: “Mày bảo với nó, là anh Thọ làm từ mảnh chiếc F4 trung đội anh ấy xử lũ giặc trời rơi tại chỗ, tháng trước ở Đò Lèn…”

Minh không nói gì, nhận quà, nhìn tôi bảo, mắt mày  thâm quầng. Không hiểu sao, sau khi bị thương, mất nhiều máu, tôi mất ngủ. Cứ ba giờ sáng là tỉnh như sáo. Tôi mới 21 tuổi. Tuổi ăn tuổi ngủ.

Minh đi ra Bắc. Tôi lại vào Nam.

Địch phong tỏa gắt gao đầu đường 20. Máy bay OV10 bay quần đảo trên đầu 24 tiếng, thay nhau xăm xoi. Còn lũ L19 lượn thấp kêu à à suốt, rồi đêm là tụi C130 bay dọc tuyến, thì thũm bắn đạn cối, hay đạn súng 20 li sáu nòng kì kì kêu, dai như chão. Máy bay B.52 cầy đi cầy lại, đất đá, rừng nguyên sinh biến thành cả một miền bụi khô rang, dầy tới mắt cá chân, triền miên dài rộng hàng mấy trăm héc-ta, chạy suốt từ ngay cửa khẩu. Trên núi đá, những bầy vượn đen, hon hót suốt sớm sớm cũng biến mất. Buổi chiều hôm trước, núi còn xanh, qua một đêm, ba đợt bom B52 dội, sáng ra nhìn trắng xóa. Đến đá cũng thành vôi. Tụi Hà Nội cùng lính nông dân các miền khác nhau cứ sống để đánh nhau, chờ đợi!

Thật bất ngờ, tháng 6 tôi có thư. Thư Minh.

Thế là nó đã ra Hà Nội rồi! Thư Minh kể, nó tới chợ Dậu thăm mợ tôi. Cậu tôi vẫn dạy ở kiến trúc, sơ tán tận Lâm Thao. Anh tôi thì ở Khu Tư mà sao chả gặp! Trời, còn thư nữa. Lá thư dán kín đặt giữa thư Minh.

Hoá là thư của Thảo.

 
  Ảnh: st
Chữ con gái mềm. Thư dài đến bốn trang giấy pơ-luya trắng lốp. Nó cảm ơn về cái lược, khen lược anh Thọ làm rất bắt tóc, hơn hẳn cái lược sừng của anh Minh mua. Hừ, nó nịnh đây! Tôi đọc đoạn cuối. Thư chép tất cả tác dụng về lạc tiên. Thu hái ra sao. Rễ, cành, thân và tác dụng như thế nào với giấc ngủ. Hóa ra Minh đã kể là tôi mất ngủ. Nó làm như lính tráng có thể tự do đi hái thuốc cho mình. Vả lại, tôi còn mất ngủ nữa dâu. Mệt nhoài suốt đêm ngày, quần nhau ở cửa khẩu này, có ngày đêm táng nhau tới 21 trận, thì chả thuốc men gì, cũng ngủ quay ra trên đất. Song thế là nó quan tâm tới tôi, những dòng chữ thật làm tôi cảm động. Tôi không có em gái. Kể ra, tôi cũng có tình cảm như anh em với nó.

Đêm trong gian hầm ở hang núi lạnh, tôi đọc đi đọc lại thư Minh, rồi thư Thảo và, đêm ấy tôi mơ trở về Hà Nội. Không hiểu sao trong giấc mơ tôi toàn thấy Thảo và Minh cho tôi ăn lạc tiên. Tỉnh dậy tôi nhớ lại, thấy cả cảm giác rất rõ vị chua chua và ngọt hậu ở đầu lưỡi, ở môi. Suốt 11 năm chiến trường, trong cả cuộc chiến, đấy là giấc mơ đẹp duy nhất. Giấc mơ thần tiên của chiến tranh. Lạc tiên!

Rồi mùa mưa tới, tụi tôi thọc sâu vào Lào. Đánh chiếm Boloven. Tràn xuống tận Campuchia rồi lại quay lại Lào. Thấm thoát thời gian veo vèo trôi, đã là 1972, tuổi thanh xuân rũ đi, bạc phếch trong những trận chiến, trong rừng. Tụi tôi quây thành phố khungsedon, ròng rã từ đầu năm tới tận cuối năm 1972. Tôi chỉ huy một trung đội phục bên kia bến sông, nơi địch tiếp tế với máy bay C123. Chọi nhau với T28, L19, với cả pháo kích và có lần chúng bắn thẳng ba bốn phát DKZ từ bên kia bờ sang, lửa cháy khét lẹt.

Đêm 26 tháng 12, quá nửa đêm, đường dây điện thoại hữu tuyến bất ngờ reo. Tôi chửi tục, vồ lấy máy. Mỹ đánh Hà Nội bằng B52. Giọng thằng Tâm phố Huế trực ở tiểu đoàn hét kên. Chúng nó đánh sập toàn bộ Khâm Thiên rồi! Nhà thằng Minh bạn mày chắc chả còn.

Tôi tỉnh phắt. B52! Trời ạ. Chúng tôi đụng độ bao lần với tụi này. Một máy bay là hơn trăm quả bom. Ba chiếc đều thả cùng một lúc thì còn gì nữa. Tới núi cũng sạt, rừng cũng tan. Tôi thức tới sáng, bật radio nghe BBC, nghe đài Việt Nam Cộng hòa và Đài tiếng nói Việt Nam. Không biết mợ tôi ra sao? Không thấy tin tụi Mỹ đánh vào chợ Giời, nơi nhà tôi. Chúng nó đánh vào Khâm Thiên, gần nhà ga mà. Thảo sơ tán hay ở nhà? Thằng Minh thì ở đâu? Không có giấy mà viết một cái thư. Có giấy cũng chẳng biết gửi ai mà chuyển ra Bắc. Tôi trằn trọc tới sáng. Mai lại hành quân. Mai lại hành quân...

Chiến tranh chả khi nào hứa hẹn. Mà có lời hứa cũng mấy khi thực hiện được. Thư gửi Minh về lần trước cho Thảo, tôi nói, sẽ về thăm nó và kiếm cho nó hẳn một cái dù pháo sáng. Dù thì bây giờ đây có đủ cả ba bốn cái, mấy mảnh dù hàng loang lổ rất đẹp Thảo ạ. Nhưng anh chưa về được. Cuộc chiến kéo tôi từ đứa trẻ 16 tuổi tới khi 27 tuổi tiến vào Sài Gòn. Ngày nào ria mép chưa có, bây giờ râu tôi đã đen cứng và lởm chởm, ra dáng một thằng đàn ông phong trần.

Ngay sau ngày hoà bình tôi được về phép. Đó là xuất ưu tiên, bởi tôi nhận được điện Trung đoàn cho những sĩ quan có cha mẹ mất mà chưa được về. Mợ tôi mất đúng vào giai đoạn gay cấn nhất của Hà Nội. Tất cả cho tiền tuyến, mợ tôi nhịn ăn tiếp tế cho chồng con, gầy gò như xác ve, và hạ đường huyết chết trong buồng tắm.

Hà Nội đầy sắc lính từ các mặt trận tràn về. Không một ai để ý tới một tay lính già lụi cụi một mình đi tìm bạn.

Tôi tới Khâm Thiên, không tìm ra lối cũ vào nhà Minh. Ngõ cũ, đường cũ trôi đâu, mất đâu? Không ai biết Minh Tầu Hoả của tôi ở đâu. Đường không còn ngõ để vào. Chắc vạt ruộng vườn của nhà nó chả còn.

Rồi qua bạn bè tôi cũng tìm được nó. “Giờ Minh đã có vợ. Nó ở ngay ngõ sát khu chuyên gia Kim Liên!”. Tôi cầm mảnh giấy của thằng Cường vẽ đường, lếch thếch đi bộ, tìm tới Minh Tầu Hoả.

 
 
Cái cửa gỗ sơn xanh đóng từ nắp hòn đạn im ỉm. Gõ mấy lần, mới thấy cái đầu đứa trẻ ngơ ngác nhìn. “Bác hỏi ai ạ?”. Bác hỏi chú Minh. “Bố cháu vừa về. Đang ở trên gác”. “Gọi xuống đây cho chú. Bảo bố là có chú Thọ tới”.

Tiếng chân bước rất thong thả trên cầu thang. Minh hiện ra. Nó già và đẫy ra nhiều so với trước. Đen nhẻm. Thấp và đậm.

- Trời ơi. Mày sống hả? - Minh nhao tới. Cả hai ôm nhau, ứa nước mắt.

Trước mặt tôi tấm ảnh 9X12 ép ni-lon, rõ hình Thảo.

Một cô gái Hà Nội đẹp, tươi mơn mởn. Hai cái đuôi sam đen to, nhanh nhánh hai bên vai và miệng cười rõ tươi. Đôi mắt to đẹp nhìn thẳng vào tôi và cái mũi hếch tinh nghịch. Cây đàn ghi ta Thảo cầm trên tay. Thấp thoáng sau Thảo là một thanh niên rất trẻ và vài người lính đội mũ sắt, có cả một cái đầu nòng 37 nom như cái phễu đen.

Giọng Minh trầm và đục:

- Tao từ Lạng Sơn về, mày biết không, giữa nhà là ba, bốn cái hố bom. Không có một dấu vết nào ngôi nhà của tao nữa. Cả gian hầm con Thảo nằm. Nước ao cũng cạn nửa, vì hơn chục quả bom tương xuống đó. Tao không thể ở mảnh đất ấy. Tao về ngõ này ở cho giấc ngủ bình thường.

Tôi chùi nước mắt.

- Đây là tấm ảnh, vật còn lại duy nhất của nó đưa tao, trước khi nó mất hai tháng. Ảnh phóng viên báo nào chụp, khi đoàn viên thành phố và khu thăm trận địa trên Trúc Bạch. Minh nói và đưa tay quyệt ngang mắt. Vậy là cả nhà Minh đã mất. Cha nó cùng cả đoàn tầu chạy rơi vào mưa bom ở sát Lạng Sơn, khi giải tỏa hàng viện trợ. Mẹ và Thảo mất ở Khâm Thiên... Tôi ôm lấy Minh. Thằng bạn thân yêu từ lớp Một và suốt cuộc đời...

Tôi đưa tấm ảnh lên nhìn cho kĩ. Mắt tôi nhoà.

Thảo như còn sống. Đôi mắt em đen nhánh, thật sống động vẫn như nhìn tôi. “Anh nhớ, cho tất cả vào đun kĩ, ăn như canh, sau vài bữa sẽ ngủ sâu và nhẹ. Mất ngủ thì sao có sức khoẻ mà chiến đấu với giặc Mỹ anh nhỉ…”. Như có ai đang đọc lên ở trong hang đá ấy và vẫn thi thoảng nói bên tai cho tôi. Tôi nhớ lá thư hôm nào.

Thảo ơi, em gái của anh, bây giờ hoà bình rồi, em đang phiêu du ở miền nảo miền nào. Tôi ứa nước mắt!

Chiến tranh thì làm gì có giấc ngủ sâu và nhẹ. Tự nhiên tôi nhớ tới những quả lạc tiên trong bàn tay khum khum nhỏ bé của em, những búp tay thon dài đã hái rau cho mẹ, dọn cơm cho tôi và Minh, những búp tay ấy đã chơi đàn cho em hát trong cuộc chiến… Những quả lạc tiên nho nhỏ, giản dị, vàng như nắng thu, cả cái vị chua chua ngọt hậu. Cuộc sống có ngọt hậu bao giờ? Khi nào? Tôi tự hỏi!

Hôm sau tôi tra tài liệu thấy ghi:
Hồi 22h 45 ngày 26-12-1972... Cả một dãy phố Khâm Thiên phía chợ, hơn ba trăm người đã hy sinh.


Nước Đức, tháng 5.2012
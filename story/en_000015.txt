Hai mươi ba năm trước, có một người con gái lang thang qua làng tôi, đầu bù tóc rối, gặp ai cũng cười cười, cũng chả ngại ngần ngồi tè trước mặt mọi người. Vì vậy, đàn bà trong làng đi qua cô gái thường nhổ nước bọt, có bà còn chạy lên trước dậm chân, đuổi “Cút cho xa!” Thế nhưng cô gái không bỏ đi, vẫn cứ cười ngây dại quanh quẩn trong làng.

Minh họa của Thanh Huyền

Hồi đó, cha tôi đã ba lăm tuổi. Cha làm việc ở bãi khai thác đá bị máy chém cụt tay trái, nhà lại quá nghèo, mãi không cưới được vợ. Bà nội thấy con điên có sắc vóc, thì động lòng, quyết định mang cô ta về nhà cho cha tôi, làm vợ, chờ bao giờ cô ta đẻ cho nhà tôi “đứa nối dõi” sẽ đuổi đi liền. Cha tôi dù trong lòng bất nhẫn, nhưng nhìn cảnh nhà, cắn răng đành chấp nhận. Thế là kết quả, cha tôi không phải mất đồng xu nào, nghiễm nhiên thành chú rể.

Khi mẹ sinh tôi, bà nội ẵm cháu, hóp cái miệng chẳng còn mấy cái răng vui sướng nói: “Cái con mẹ điên này, mà lại sinh cho bà cái đứa chống gậy rồi!” Có điều sinh tôi ra, bà nội ẵm mất tôi, không bao giờ cho mẹ đến gần con.

Mẹ chỉ muốn ôm tôi, bao nhiêu lần đứng trước mặt bà nội dùng hết sức gào lên: “Đưa, đưa tôi…” bà nội mặc kệ. Tôi còn trứng nước như thế, như khối thịt non, biết đâu mẹ lỡ tay vứt tôi đi đâu thì sao? Dù sao, mẹ cũng chỉ là con điên. Cứ mỗi khi mẹ khẩn cầu được bế tôi, bà nội lại trợn mắt lên chửi: “Mày đừng có hòng bế con, tao còn lâu mới đưa cho mày. Tao mà phát hiện mày bế nó, tao đánh mày chết. Có đánh chưa chết thì tao cũng sẽ đuổi mày cút”. Bà nội nói với vẻ kiên quyết và chắc chắn. Mẹ hiểu ra, mặt mẹ sợ hãi khủng khiếp, mỗi lần chỉ dám đứng ở xa xa ngó tôi. Cho dù vú mẹ sữa căng đầy cứng, nhưng tôi không được một ngụm sữa mẹ nào, bà nội đút từng thìa từng thìa nuôi cho tôi lớn. Bà nói, trong sữa mẹ có “bệnh thần kinh”, nếu lây sang tôi thì phiền lắm.

Hồi đó nhà tôi vẫn đang giãy giụa giữa vũng bùn lầy của nghèo đói. Đặc biệt là sau khi có thêm mẹ và tôi, nhà vẫn thường phải treo niêu. Bà nội quyết định đuổi mẹ, vì mẹ không những chỉ ngồi nhà ăn hại cơm nhà, còn thỉnh thoảng làm thành tiếng thị phi.

Một ngày, bà nội nấu một nồi cơm to, tự tay xúc đầy một bát cơm đưa cho mẹ, bảo: “Con dâu, nhà ta bây giờ nghèo lắm rồi, mẹ có lỗi với cô. Cô ăn hết bát cơm này đi, rồi đi tìm nhà nào giàu có hơn một tí mà ở, sau này cấm không được quay lại đây nữa, nghe chửa?”

Mẹ tôi vừa và một miếng cơm to vào mồm, nghe bà nội tôi hạ “lệnh tiễn khách” liền tỏ ra kinh ngạc, ngụm cơm đờ ra lã tã trong miệng. Mẹ nhìn tôi đang nằm trong lòng bà, lắp bắp kêu ai oán: “Đừng… đừng…” Bà nội sắt mặt lại, lấy tác phong uy nghiêm của bậc gia trưởng nghiêm giọng hét: “Con dâu điên mày ngang bướng cái gì, bướng thì chả có quả tốt lành gì đâu. Mày vốn lang thang khắp nơi, tao bao dung mày hai năm rồi, mày còn đòi cái gì nữa? Ăn hết bát ấy rồi đi đi, nghe thấy chưa hả?”

Nói đoạn bà nội lôi sau cửa ra cái xẻng, đập thật mạnh xuống nền đất như Dư Thái Quân nắm gậy đầu rồng, “phầm!” một tiếng. Mẹ sợ chết giấc, khiếp nhược lén nhìn bà nội, lại chậm rãi cúi đầu nhìn xuống bát cơm trước mặt, có nước mắt rưới trên những hạt cơm trắng nhệch. Dưới cái nhìn giám sát, mẹ chợt có một cử động kỳ quặc, mẹ chia cơm trong bát một phần lớn sang cái bát không khác, rồi nhìn bà một cách đáng thương hại.

Bà nội ngồi thẫn thờ, hóa ra, mẹ muốn nói với bà rằng, mỗi bữa mẹ sẽ chỉ ăn nửa bát, chỉ mong bà đừng đuổi mẹ đi. Bà nội trong lòng như bị ai vò cho mấy nắm, bà nội cũng là đàn bà, sự cứng rắn của bà cũng chỉ là vỏ ngoài. Bà nội quay đầu đi, nuốt những giọt nước mắt nóng, rồi quay lại sắt mặt nói: “Ăn mau ăn mau, ăn xong còn đi. Ở nhà này cô cũng chết đói thôi”.

Mẹ tôi dường như tuyệt vọng, đến ngay cả nửa bát cơm con cũng không ăn, thập thễnh bước ra khỏi cửa, nhưng mẹ đứng ở bậc cửa rất lâu không bước ra. Bà nội dằn lòng đuổi: “Cô đi, cô đi, đừng có quay đầu lại. Dưới gầm trời này còn nhiều nhà người ta giàu”. Mẹ tôi quay lại, đưa một tay ra phía lòng bà, thì ra, mẹ muốn được ôm tôi một tí.

Bà nội lưỡng lự một lúc, rồi đưa tôi trong bọc tã lót cho mẹ. Lần đầu tiên mẹ được ẵm tôi vào lòng, môi nhắp nhắp cười, cười hạnh phúc rạng rỡ. Còn bà nội như gặp quân thù, hai tay đỡ sẵn dưới thân tôi, chỉ sợ mẹ lên cơn điên, quăng tôi đi như quăng rác. Mẹ ôm tôi chưa được ba phút, bà nội không đợi được giằng tôi trở lại, rồi vào nhà cài chặt then cửa.

Khi tôi bắt đầu lờ mờ hiểu biết một chút, tôi mới phát hiện, ngoài tôi ra, bọn trẻ chơi cùng tôi đều có mẹ. Tôi tìm cha đòi, tìm bà đòi, họ đều nói, mẹ tôi chết rồi.

Nhưng bọn bạn cùng làng đều bảo tôi: “Mẹ mày là một con điên, bị bà mày đuổi đi rồi.” Tôi tìm bà nội vòi vĩnh, đòi bà phải trả mẹ lại, còn chửi bà là đồ “bà lang sói”, thậm chí hất tung mọi cơm rau bà bưng cho tôi.

Ngày đó, tôi làm gì biết “điên” nghĩa là cái gì đâu, tôi chỉ cảm thấy nhớ mẹ tôi vô cùng, mẹ trông như thế nào nhỉ? Mẹ còn sống không?

Không ngờ, năm tôi sáu tuổi, mẹ tôi trở về sau năm năm lang thang.

Hôm đó, mấy đứa nhóc bạn tôi chạy như bay tới báo: “Thụ, mau đi xem, mẹ mày về rồi kìa, mẹ bị điên của mày về rồi!” Tôi mừng quá đít nhổng nhổng, co giò chạy vội ra ngoài, bà nội và cha cũng chạy theo tôi. Đây là lần đầu tiên tôi nhìn thấy mẹ, kể từ khi biết nhớ. Người đàn bà đó vẫn áo quần rách nát, tóc tai còn những vụn cỏ khô vàng khè, có trời mới biết là do ngủ đêm trong đống cỏ nào.

Mẹ không dám bước vào cửa, nhưng mặt hướng về phía nhà tôi, ngồi trên một hòn đá cạnh ruộng lúa trước làng, trong tay còn cầm một quả bóng bay bẩn thỉu. Khi tôi và lũ trẻ đứng trước mặt mẹ, mẹ cuống cuồng nhìn trong đám tôi tìm con trai mẹ. Cuối cùng mẹ dán chặt mắt vào tôi, nhìn tôi chòng chọc, nhếch mép bảo: “Thụ… bóng… bóng…” Mẹ đứng lên, liên tục giơ lên quả bóng bay trong tay, dúi vào tôi với vẻ lấy lòng.

Tôi thì liên tục lùi lại. Tôi thất vọng ghê gớm, không ngờ người mẹ ngày đêm tôi nhớ thương lại là cái hình người này. Một thằng cu đứng cạnh tôi kêu to: “Thụ, bây giờ mày biết con điên là thế nào chưa? Là mẹ mày như thế này đấy!”

Tôi tức tối đáp lại nó: “Nó là mẹ mày ấy! Mẹ mày mới là con điên ấy, mẹ mày mới là thế này!” Tôi quay đầu chạy trốn. Người mẹ bị điên này tôi không thèm. Bà nội và bố thì lại đưa mẹ về nhà. Năm đó, bà nội đuổi mẹ đi rồi, lương tâm bà bị day dứt giày vò, bà càng ngày càng già, trái tim bà cũng không còn sắt thép được nữa, nên bà chủ động đưa mẹ về, còn tôi lại bực bội, bởi mẹ đã làm tôi mất thể diện.

Tôi không bao giờ tươi tỉnh với mẹ, chưa bao giờ chủ động nói với mẹ, càng không bao giờ gọi “Mẹ!”, khi phải trao đổi với mẹ, tôi gào là chủ yếu, mẹ không bao giờ dám hé miệng.

Nhà không thể nuôi không mẹ mãi, bà nội quyết định huấn luyện cho mẹ làm việc vặt. Khi đi làm đồng, bà nội dắt mẹ đi “quan sát học hỏi”, bà bảo mẹ không nghe lời sẽ bị đánh đòn.

Sau một thời gian, bà nội nghĩ mẹ đã được dạy dỗ tương đối rồi, liền để mẹ tự đi cắt cỏ lợn. Ai ngờ mẹ chỉ cắt nửa tiếng đã xong cả hai bồ “cỏ lợn”. Bà nội vừa nhìn đã hoảng hốt sợ hãi, cỏ mẹ cắt là lúa giống vừa làm đòng trỗ bông trong ruộng nhà người ta. Bà nội vừa sợ vừa giận phát cuồng chửi rủa: “Con mẹ điên, lúa và cỏ mà không phân biệt được…”

Bà nội còn đang chưa biết nên xoay xở ra sao, thì nhà có ruộng bị cắt lúa tìm tới, mắng bà cố ý dạy con dâu làm càn. Bà nội tôi lửa giận bốc phừng phừng, trước mặt người ta lấy gậy đánh vào eo lưng con dâu, chửi: “Đánh chết con điên này, mày cút ngay đi cho bà…”
Mẹ tuy điên, nhưng vẫn biết đau, mẹ nhảy nhỏm lên chạy trốn đầu gậy, miệng phát ra những tiếng lắp bắp sợ hãi: “Đừng… đừng…” Sau rồi, nhà người ta cũng cảm thấy chướng mắt, chủ động bảo: “Thôi, chúng tôi cũng chẳng bắt đền nữa. Sau này giữ cô ta chặt một tí là được…”

Qua cơn sóng gió, mẹ oại người dưới đất thút thít khóc. Tôi khinh bỉ bảo: “Cỏ với lúa mà cũng chả phân biệt được, đúng là lợn!” Lời vừa dứt, gáy tôi bị một cái tát lật, là bà. Bà trừng mắt bảo tôi: “Thằng ngu kia, mày nói cái gì đấy? Mày lại còn thế nữa? Đấy là mẹ mày đấy!” Tôi vùng vằng bĩu môi: “Cháu không có loại mẹ điên khùng thế này!”

“A, mày càng ngày càng láo. Xem bà có đánh mày không!” Bà nội lại giơ tay lên, lúc này chỉ thấy mẹ như cái lò xo bật từ dưới đất lên, che giữa bà nội và tôi, mẹ chỉ tay vào đầu mẹ, kêu thảng thốt: “Đánh tôi, đánh tôi!”

Tôi hiểu rồi, mẹ bảo bà nội đánh mẹ, đừng đánh tôi. Cánh tay bà đang giơ cao thõng xuống, miệng lẩm bẩm: “Con mẹ điên này, trong lòng nó cũng biết thương con đây”. Tôi vào lớp một, cha được một hộ chuyên nuôi cá làng bên mời đi canh hồ cá, mỗi tháng lương 50 tệ. Mẹ vẫn đi làm ruộng dưới sự chỉ bảo của bà, chủ yếu là đi cắt cỏ lợn, mẹ cũng không còn gây ra vụ rầy rà nào lớn nữa.

Nhớ một ngày mùa đông đói rét năm tôi học lớp ba, trời đột ngột đổ mưa, bà nội sai mẹ mang ô cho tôi. Có lẽ trên đường đến trường tôi, mẹ đã ngã ì oạch mấy lần, toàn thân trông như khỉ lấm bùn, mẹ đứng ở ngoài cửa sổ lớp học nhìn tôi cười ngớ ngẩn, miệng còn gọi tôi: “Thụ… ô…” Có mấy đứa bạn tôi cười khúc khích, tôi như ngồi trên bàn chông, oán hận mẹ khủng khiếp, hận mẹ không biết điều, hận mẹ làm tôi xấu hổ, càng hận thằng Phạm Gia Hỷ cầm đầu trêu chọc.

Trong lúc nó còn đang khoa trương bắt chước mẹ, tôi chộp cái hộp bút trước mặt, đập thật mạnh cho nó một phát, nhưng Phạm Gia Hỷ tránh được. Nó xông tới bóp cổ tôi, chúng tôi giằng co đánh nhau. Tôi nhỏ con, vốn không phải là đối thủ của nó, bị nó dễ dàng đè xuống đất. Lúc này, chỉ nghe một tiếng “vút” kéo dài từ bên ngoài lớp học, mẹ giống như một đại hiệp “bay” ào vào, một tay tóm cổ Phạm Gia Hỷ, đẩy ra tận ngoài cửa lớp.

Ai cũng bảo người điên rất khỏe, thật sự đúng là như vậy. Mẹ dùng hai tay nhấc bổng thằng bắt nạt tôi lên cao, nó kinh sợ kêu khóc gọi bố mẹ, một chân béo ị khua khoắng đạp loạn xạ trên không trung. Mẹ không thèm để ý, vứt nó vào ao nước cạnh cổng trường, rồi mặt thản nhiên, mẹ đi ra.

Mẹ vì tôi gây ra đại họa, mẹ lại làm như không có việc gì xảy ra. Trước mặt tôi, mẹ lại có vẻ khiếp nhược, nhìn tôi vẻ muốn lấy lòng. Tôi hiểu ra đây là tình yêu của mẹ, dù đầu óc mẹ không tỉnh táo, thì tình yêu của mẹ vẫn tỉnh táo, vì con trai của mẹ bị người ta bắt nạt. Lúc đó tôi không kìm được kêu lên: “Mẹ!” đây là tiếng gọi đầu tiên kể từ khi tôi biết nói.

Mẹ sững sờ cả người, nhìn tôi rất lâu, rồi y hệt như trẻ con, mặt mẹ đỏ hồng lên, cười ngớ ngẩn. Hôm đó, lần đầu tiên hai mẹ con tôi cùng che một cái ô về nhà. Tôi kể sự tình cho bà nội nghe, bà nội sợ rụng rời ngã ngồi lên ghế, vội vã nhờ người đi gọi cha về.

Cha vừa bước vào nhà, một đám người tráng niên vạm vỡ tay dao tay thước xông vào nhà tôi, không cần hỏi han trắng đen gì, trước tiên đập phá mọi bát đĩa vò hũ trong nhà nát như tương, trong nhà như vừa có động đất cấp chín. Đây là những người do nhà Phạm Gia Hỷ nhờ tới. Bố Phạm hung hãn chỉ vào cha tôi nói: “Con trai tao sợ quá đã phát điên rồi, hiện đang nằm nhà thương. Nhà mày mà không mang một nghìn tệ trả tiền thuốc thang, mẹ mày tao cho một mồi lửa đốt tan cái nhà mày ra.”

Một nghìn tệ? Cha đi làm một tháng chỉ năm mươi tệ! Nhìn những người sát khí đằng đằng nhà họ Phạm, cha tôi mắt đỏ lên dần, cha nhìn mẹ, ánh mắt cực kỳ khủng khiếp, một tay nhanh như cắt dỡ thắt lưng da, đánh tới tấp khắp đầu khắp mặt mẹ. Một trận lại một trận, mẹ chỉ còn như một con chuột khiếp hãi run rẩy, lại như một con thú săn đã bị dồn vào đường chết, nhảy lên hãi hùng, chạy trốn, cả đời tôi không thể quên tiếng thắt lưng da vụt lạnh lùng lên thân mẹ và những tiếng thê thiết mẹ kêu. Sau đó phải trưởng đồn cảnh sát đến ngăn bàn tay bạo lực của cha.

Kết quả hòa giải của đồn cảnh sát là: cả hai bên đều có tổn thất, cả hai không nợ nần gì nhau cả. Ai còn gây sự sẽ bắt luôn người đó. Đám người đi rồi, cha tôi nhìn khắp nhà mảnh vỡ nồi niêu bát đũa tan tành, lại nhìn mẹ tôi vết roi đầy mình, cha tôi bất ngờ ôm mẹ tôi vào lòng khóc thảm thiết: “Mẹ điên ơi, không phải là tôi muốn đánh mẹ, mà nếu như tôi không đánh thì việc này không thể dàn xếp nổi, nhà mình làm gì có tiền mà đền cho người. Bởi nghèo khổ quá mà thành họa đấy thôi!”. Cha lại nhìn tôi nói: “Thụ, con phải cố mà học lên đại học. Không thì, nhà ta cứ bị người khác bắt nạt suốt đời, nhé!” Tôi gật đầu, tôi hiểu.

Mùa hè năm 2000, tôi thi đỗ vào trung học với kết quả xuất sắc. Bà nội tôi vì làm việc cực nhọc cả đời mà mất trước đó, gia cảnh ngày càng khó khăn hơn. Cục Dân Chính khu tự trị  n Thi (Hồ Bắc) xếp nhà tôi thuộc diện đặc biệt nghèo đói, mỗi tháng trợ cấp bốn mươi tệ. Trường tôi học cũng giảm bớt học phí cho tôi, nhờ thế tôi mới có thể học tiếp.

Vì học nội trú, bài vở nhiều, tôi rất ít khi về nhà. Cha tôi vẫn đi làm thuê năm mươi tệ một tháng, gánh tiếp tế cho tôi đặt lên vai mẹ, không ai thay thế được. Mỗi lần bà thím nhà bên giúp nấu xong thức ăn, đưa cho mẹ mang đi. Hai mươi cây số đường núi ngoằn ngoèo ruột dê làm khổ mẹ phải tốn sức ghi nhớ đường đi, gió tuyết cũng vẫn đi. Và thật là kỳ tích, hễ bất cứ việc gì làm vì con trai, mẹ đều không điên tí nào. Ngoài tình yêu mẫu tử ra, tôi không còn cách giải thích nào khác. Y học cũng nên giải thích khám phá hiện tượng này.

Ngày 27.4.2003, lại là một chủ nhật, mẹ lại đến, không chỉ mang đồ ăn cho tôi, mẹ còn mang đến hơn chục quả đào dại. Tôi cầm một quả, cắn một miếng, cười hỏi mẹ: “Ngọt quá, ở đâu ra?” Mẹ nói: “Tôi… tôi hái…” không ngờ mẹ tôi cũng biết hái cả đào dại, tôi thành thật khen mẹ: “Mẹ, mẹ càng ngày càng tài giỏi”. Mẹ cười hì hì.

Trước lúc mẹ về, tôi theo thói quen dặn dò mẹ phải cẩn thận an toàn, mẹ ờ ờ trả lời. Tiễn mẹ xong, tôi lại bận rộn ôn tập trước kỳ thi cuối cùng của thời phổ thông. Ngày hôm sau, khi tôi đang ở trên lớp, bà thím vội vã chạy đến trường, nhờ thầy giáo gọi tôi ra ngoài cửa. Thím hỏi tôi, mẹ tôi có đến đưa tiếp tế đồ ăn không? Tôi nói đưa rồi, hôm qua mẹ về rồi. Thím nói: “Không, mẹ mày đến giờ vẫn chưa về nhà!” Tim tôi thót lên một cái, mẹ tôi chắc không đi lạc đường? Chặng đường này mẹ đã đi ba năm rồi, có lẽ không thể lạc được. Thím hỏi: “Mẹ mày có nói gì không?” Tôi bảo không, mẹ chỉ cho cháu chục quả đào tươi. Thím đập hai tay:” Thôi chết rồi, hỏng rồi, có lẽ vì mấy quả đào dại rồi!”

Thím bảo tôi xin nghỉ học, chúng tôi đi men theo đường núi về tìm. Đường về quả thực có mấy cây đào dại, trên cây chỉ lơ thơ vài quả cọc, bởi nếu mọc ở vách đá mới còn giữ được quả. Chúng tôi cùng lúc nhìn thấy trên thân cây đào có một vết gãy cành, dưới cây là vực sâu trăm thước. Thím nhìn tôi rồi nói: “Chúng ta đi xuống khe vách đá tìm!” Tôi nói: “Thím, thím đừng dọa cháu…”. Thím không nói năng kéo tôi đi xuống vách núi…

Mẹ nằm yên tĩnh dưới khe núi, những trái đào dại vương vãi xung quanh, trong tay mẹ còn nắm chặt một quả, máu trên người mẹ đã cứng lại thành đám màu đen nặng nề. Tôi đau đớn tới mức ngũ tạng như vỡ ra, ôm chặt cứng lấy mẹ, gọi: “Mẹ ơi, Mẹ đau khổ của con ơi! Con hối hận đã nói rằng đào này ngọt! Chính là con đã lấy mạng của mẹ… Mẹ ơi, mẹ sống chẳng được hưởng sung sướng ngày nào…” Tôi ghé sát đầu vào khuôn mặt lạnh cứng của mẹ, khóc tới mức những hòn đá dại trên đỉnh núi cũng rớt nước mắt theo.

Ngày 7.8.2003, một trăm ngày sau khi chôn cất mẹ, thư gọi nhập học dát vàng dát bạc của Đại học Hồ Bắc đi xuyên qua những ngả đường mẹ tôi đã đi, chạy qua những cây đào dại, xuyên qua ruộng lúa đầu làng, “bay” thẳng vào cửa nhà tôi. Tôi gài lá thư đến muộn ấy vào đầu ngôi mộ cô tịch của mẹ: “Mẹ, con đã có ngày mở mặt mở mày rồi, MẸ có nghe thấy không? MẸ có thể ngậm cười nơi chín suối rồi!”
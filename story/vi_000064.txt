Chạy chữa đã hai năm, nhà lâm cảnh nghèo túng cùng cực, nhưng phổi bà Lâm vợ lão Túc, vẫn ung thư tan tành. Tử thần lừng lững trước mặt, không gì ngăn được.
Lúc này, lão Túc tựa cửa nhìn ra xa. Kia, nhà ông Bảy Nhu, ông Sáu Giáp. Kìa, nhà bà Rớt, nhà ông Thính…Láng giềng cách vài đám ruộng mà lão thấy xa lắc. Phải. Họ cách xa lắm, bởi lão đã đo khoảng cách bằng đơn vị “tiền”, đã đo bằng đơn vị “hạnh phúc”. Người ta rầm rầm tiến lên, nụ cười trên môi. Lão sụp xuống vực với nước mắt.

Lão Túc quay vào nhìn vợ. Nom bà ấy như cái que khô ai đó vất lên mặt chiếu tồi tàn. Một que khô dáng hình nhân. Lão chép miệng. Cái chép miệng tự phát không kìm được. Tiếng chép miệng khô khốc, đục, ngắn ngủn và dứt khoát buồn. Tiếng chép miệng không lẫn với bất kỳ âm thanh nào, ngoại trừ tiếng chép miệng loài Lươn.

Ừ, loài Lươn trong bùn vẫn chép miệng như thế vào lúc hoàng hôn.

Lão Túc lại quay nhìn bên ngoài. Lão không dám nhìn vợ lâu hơn. Nhìn lâu, lão nhất định sẽ thấy cái que khô ấy phực cháy, bùng lên ngọn lửa xanh tím. Đã nhiều lần nhìn vợ, lão thấy ngọn lửa ấy rồi. “Lửa xanh tím – lão vẫn nghĩ – đó là lửa thần thức đang lìa khỏi xác, lửa bàn giao Thần Chết. Lửa ấy, như sư Bản nói, sẽ tiến vào không trung vô tận, đi tìm nơi ở mới, bỏ lại cái xác hư nát nằm lại”. Đột ngột, lão rít lên nho nhỏ trong cổ họng: “Đồ phản chủ. Hèn mạt. Nhà nát, mày cuốn gói đi à?”. Lão mím chặt môi, ánh mắt bắt gặp khóm hoa Mẫu Đơn. Lão chong mắt vào đoá hoa to nhất. Ngay lúc này, tiếng chuông của Hương Trì tự vang lên, ngân chật không gian. Tiếng chuông lan toả vô số vòng sóng, vút cao cả ngàn cánh bay. Ừ, đã năm giờ chiều. Sư Bản luyện kung phu xong. Trong một thoáng, lão thấy đoá Mẫu Đơn chao qua chao lại, mặc dù trời không tí gió. Hoa rung động theo tiếng chuông ư? Một sinh linh ư? Có thể lắm. Nếu không, hoa làm sao lớn lên, khoe sắc, úa tàn. Lão vẫn chong mắt vào đoá Mẫu Đơn. Lão biết sư Bản phải đánh đủ 108 tiếng chuông, bởi xâu chuỗi bồ đề của sư Bản 108 hột. Còn lâu mới hết tiếng chuông. Một tiếng chuông nữa lại vang lên, tiếp sức cho tiếng chuông vừa đuối. Trong không gian không ngớt tiếng chuông. Lạ thật, trong một thoáng lão thấy sư Bản chờn vờn trên đoá hoa. Đoá hoa lập tức nhoè đi…Lão thấy mình trò chuyện cùng sư Bản trong một màn sương. Cả hai nhấp nhô trên sóng chuông chùa. Lão nghe tiếng nói mình phát ra im lặng:

“Tại sao sư không nhổ chiếc răng xiêu vẹo ấy đi?”

Lão nghe tiếng sư Bản trả lời:

“Nhổ mà làm gì? Một sáng đẹp trời nào đấy, tôi súc miệng, nó văng ra thôi. Không có bàn tay con người là không chảy máu, không đau”.

Nói xong, sư Bản cười. Hơi gió tuôn ra làm rung rinh chiếc răng độc nhất. Một chiếc răng không bạn bè, trông quá cô đơn.

Lão Túc lại nghe tiếng mình phát ra:

“Sư sợ chết không?”

Sư Bản lại cười. Sư cười hoài. Nụ cười thoáng hư vô, ảo ảnh:

“Nhờ chết, ta mới biết sống. Sao lại sợ?”

Lão Túc nghe mình nín thinh. Lão thấy mình bé lại. Sư Bản hỏi:

“Chị nhà, dạo này ra sao?”

“Mong manh lắm.”

“Mệnh mỏng, đừng buồn.”

Lão Túc nghe mình nói thêm:

“Mỏng hơn dao lam”

Sư Bản cũng lại nói thêm:

“Ừ, nhưng ông đừng lấy lam ấy để cứa mình.”

Lão lại nghe mình nín thinh. Một tiếng chuông chùa nữa lại vang âm u trong không gian. Lão lại thấy mình dùng tay sờ nắn vết chai nơi đầu gối sư Bản. Hai vết chai dày cộm, đen đủi. Đó là dấu ấn chứng minh đức kiên trì quì gối lâu năm của sư. Lão lại nghe tiếng hỏi của mình, vang lên im lặng như ruồi bay:

“Liệu tâm sư có chai như đầu gối không?”

Tiếng trả lời của sư bản lẫn vào tiếng chuông:

“Đầu gối quì. Tâm không quì.”

Lão lại nghe mình nín thinh. Lão thấy bàn tay mình sững lại, không sờ nắn vết chai ấy nữa.

Lão dụi mắt, đóa Mẫu Đơn lại hiện ra. Một tiếng chuông nữa lại rền vang trong không gian. Lão Túc quay vào nhìn vợ nằm thiêm thiếp trên giường. Lão thấy đôi môi nhợt nhạt, khô héo của vợ vừa mỉm cười. Ồ, có lẽ bà ấy đang bồng bềnh theo tiếng chuông. Đúng là bà ấy thoi thóp sống thêm là nhờ tiếng chuông. 

Tiếng chuông đã dìu bà ấy trên cánh bay mỗi ngày hai lần, đầu ngày rồi cuối ngày. Tiếng chuông đã xoa dịu bà ấy vơi đau đớn lầm than. Lại một tiếng chuông nữa vang lên, đuổi theo tiếng chuông vừa đuối. Lão thấy đôi môi vợ mình lại mỉm cười. Lão ngẩng đầu lên, không dám nhìn nữa, và mắt lão gặp tấm lưới chài treo ở góc nhà. Tấm lưới chài từ lâu vẫn treo đấy không dùng. Thình lình, từ tấm lưới chài, một ảo ảnh hiện ra. Tấm lưới chài vụt bung rộng, xoè trên không, rồi chụp xuống một giòng sông. 

Lão thu chài, bắt được một con cá Sảnh to bằng bàn tay xoè. Một loại cá vợ lão rất thích. Thích đến nỗi, lúc còn khoẻ mạnh, bà ấy ăn luôn cả vảy, nhai rụm cả xương. Lão dụi mắt. Ảo ảnh biến tan. Lão cau mày. Phải có con cá ấy nấu cháo loãng, cho bà ấy húp một thìa trước khi chết chứ? Một thìa thôi, cũng mãn nguyện lắm rồi. Một tiếng chuông nữa lại rền trong không gian. Tiếng chuông nghe mạnh mẽ hẳn, tiếng chuông bay vun vút. Tiếng chuông thúc giục. Lập tức, lão đến góc nhà, gỡ tấm lưới chài, vắt lên vai, bước nhanh ra cửa, xăm xăm hướng sông tre làng, quyết tìm một con cá Sảnh, cho dù nhỏ bằng ngón tay cũng được.

Lão băng đồng, không theo con lộ ngoằn ngoèo mất thời giờ. Một tiếng chuông nữa lại vang lên, đuổi theo lão làm bạn đồng hành. Ảo ảnh lại vùng dậy. Trước mắt lão, tấm lưới chài lại bung rộng lên không trung, mặc dù nó đang nằm yên trên vai lão. Tấm lưới chài bung trắng trời, ụp xuống một giòng sông trắng sữa. Một con cá Sảnh dính vào mắt lưới giãy giụa. Từ nơi giãy giụa, nhô lên một bát cháo bốc khói. Vợ lão húp được vài thìa. Đôi môi khô héo liền dãn ra, hồng lại. Lão dụi mắt. Loạt ảo ảnh lại biến tan.

Trời đã nhá nhem tối, cảnh vật biến dạng sang những hình thù kỳ dị. Đất cày vỡ trông như bầy chó mực sắp hàng. Lũy tre, cây đa như thành, như núi. Bụi đế trong như ông “ba bị” ngồi rình. Vừa đi, lão vừa thì thầm với tấm lưới chài vắt vai: “Chú trăm mắt, hãy tìm cho lão một con Sảnh nhé?”. Ruộng đã cày khô, bầy chó mực lởm chởm, lão vấp ngã, tấm lưới văng ra, lão mò nhặt lại. Đột ngột, lão nghe re re một giọng kỳ lạ, mảnh mai mà quyền lực: “Ông là ai? Sao dám đi con đường trái tim?”. Lão nhíu mày: “Ừ, mình không cần trả lời, vì đấy là tiếng nói của im lặng. Tiếng nói của im lặng, có đủ lời đáp lại, nếu ai đó sợ nó. Ừ, nó vừa doạ mình.”

Trời đã tối hẳn. Một màn đen đã hoàn tất công việc xóa nhòa. Công việc của nó chỉ thế thôi. Tiếng chuông của Hương Trì tự cũng bỏ lão rồi. Lão cố gắng bước nhanh, cố gắng thì thầm cùng tấm lưới chài: “Chú trăm mắt, chú cố giúp lão một lần nhé?” Chân lão vẫn bước khập khiễng qua ruộng, qua bờ…Thình lình, trước mặt lão hiện ra sừng sững một bức tường đen khổng lồ. Lão đã đến sát luỹ tre mà không hay. Lão suýt nữa đi xuyên qua nó. 

Mũi lão nhăn nhíu đánh hơi, lão cố hít thở thật sâu, lão đi thật chậm dọc bờ tre, căng mắt nhìn xuống bờ sông tre làng. Lão cố tìm một vùng lõm. Vùng lõm bờ sông lúc nào cũng nhiều cá. Trời quá tối. Cái tối từ trời, từ đất, từ tre, từ lão, hoà vào với cái tối từ tối, làm lão ngạt thở. Không thể tìm được một vùng lõm bờ sông. Lão quyết định quăng chài ngay chỗ đang đứng. Lão dạng chân, trùn người đứng thế trung bình tấn, hai tay nắm chặt tấm lưới chài, lưng thẳng đuỗng. Lão mím môi, nói với tấm chài, nói với sông đen: “Chỗ này được đấy. Chài ơi! Sông ơi! Cố gắng nhé.” 

Lập tức, lão rùn người, xoạt chân ra sau, rồi vươn lên, quay tít tấm chài một vòng, tung xuống sông. Tấm chài đã bung ra như ý. Lão nghe ba tiếng “Bùm”, “Xào” rồi “Im lặng”. Lão xoa tay, thở phào. Có lẽ, chân chài đã đến đáy sông, lão kéo sợi dây chóp để thu chài. Bỗng lão sững người, lão lặng người. Tấm lưới chài như có Hà bá níu lại. Không kéo lên được. Lão cau mày, mím môi: “Quái, thế này là thế nào?”. Lão cố kéo dây chóp, thu chài lần nữa. Vẫn không nhúc nhích. Hà Bá chôn tấm chài xuống sông rồi. Vô phúc. Thật vô phúc.

Lúc này, lão quyết định cởi quần áo, xuống nước xem sao. Lão lặn xuống, quơ tay tìm kiếm. Lão gặp ngay tấm lưới chài của mình, đã gói trọn một bụi tre sập chìm dưới nước. Lão lặn men theo tấm lưới. Mắt lưới dính vào gốc tre, nước chảy nhét rối vào gai, vào cành, vô phương tháo gỡ. Có lẽ, chỗ này đất bờ sông bị sập, đã vùi bụi tre xuống nước. Lão nghe tiếng cá quẫy rất nhiều nơi đây. Nơi bụi tre sập này, quả là một sào huyệt cá.

Ngạt thở, lão ngoi lên, vuốt nước mặt, thở hắt ra, thở hắt vào. Lão bơi lần vào bờ, cố gắng trèo lên. Lúc này, gió rông khá mạnh. Cái lạnh luôn lạnh hơn trong đêm tối, lạnh hơn trong tuyệt vọng, như được tra thêm cả trăm mũi khoan xoáy vào da thịt. Lão run bắn như  động kinh. Có tiếng chó tru văng vẳng đâu đây, dưới đất, trên cao, trong gió, uất nghẹn như một nỗi bất công thật tệ, cứ trôi nổi vô bờ…

Lão lật bật mặc quần áo, run rẩy không cài nút được. Lão trừng mắt nhìn vào đêm đen, xuyên qua ruộng đồng, xuyên qua xóm Ao, đột ngột lão thấy “cái que khô ai đó vất lên mặt chiếu tồi tàn” nơi nhà lão đã phực cháy, bùng lên ngọn lửa xanh tím. Ngọn lửa vút cao, sáng loà. Một linh tính tín hiệu làm lão giật thót. Vợ lão “ra đi” trong cô đơn, không  ai bên cạnh.

Lão hốt hoảng. Lão chạy, lão té, lão vùng dậy, lão chạy, lão té…Cứ thế trong một đêm đen không biết có con người.

Đột ngột, lão Túc dừng hẳn lại. Lão không chạy nữa. Lão không chạy nữa để bị té. Lúc này, lão cẩn thận bước đi trong đêm tối,cố gắng không cho mình ngã. Lão bình tĩnh, không còn hốt hoảng. Lão không lạnh, không cô đơn, không tuyệt vọng. Tất cả những món tầm thường ấy đã rút ra khỏi người lão. Cả cái ngọn lửa xanh tím kia cũng không còn, bởi nó chưa hề có. Cả tấm lưới chài cùng bụi tre sập cũng chưa từng có. Giờ đây, chỉ một việc duy nhất có. Đó là “hậu sự”. Phải. Lão cần cái nhà hòm tươm tất, và cả y phục cho da xương bà ấy. Nhà hòm màu đỏ. Y phục màu trắng, trắng và trắng…
Đúng chín giờ sáng nay, chị Bé mê man…
 
Hình ảnh cuối cùng mà chị còn thấy được, và ôm theo, đó là Ki tô tóc nâu ánh vàng đẹp đẽ, má bụ sữa, một nốt muỗi đốt phồng mọng đỏ như son bên sống mũi trái, làm biến dạng cả khuôn mặt ngăn ngắn nâu đậm. Nó chu môi, uốn cong lên như một nụ hoa hé cánh, kêu “tô tồ” vào mặt chị, rồi nhoẻn cười, ôm con chút chít chạy đi. “Ôi, Ki tô của tôi”.
 
Bà Tảo ngồi quẹt mắt, chốc chốc dử lại đùn ra nặng khóe mắt đã rão trên gương mặt già nua, đen đúa nhăm nhúm không còn nặn ra được một giọt trong, bàn tay run rẩy quờ quạng mặt bàn. Từ hôm chị Bé bị bệnh viện trả về, bà không rỉa rói chị nữa. Thương sao nếu không có ai cho bà ấy rỉa. Nhưng chị không còn thời gian để xót thương những con người như bà ấy. Chị phải chắt chiu chút ít thời gian còn lại, trước khi đất ném rào rào lên mặt chị, để nghĩ và nhớ về Ki tô.
 
Ki tô ơi giờ con ở phương trời nao!
 
*
*     *
 

Minh họa: Thành ChươngLóp ngóp, còng mọp, đau nhức, khấp khởi bước lên được ba bậc tam cấp nhà hàng xóm, chị Bé gỡ nón, hồi hộp hỏi:
 
- Ki tô dậy chưa cô?
 
- Cháu đang ngủ chị ạ. Chị cứ vào nhà uống nước chơi, cũng chưa biết khi nào cháu dậy. Nó ngủ nghê bất thường lắm. Thay đổi múi giờ mà.
 
Quả là số ruồi bâu nhặng bậu. Lần này là lần thứ năm chị sang nhà hàng xóm, cốt để nhìn thấy thằng bé, vậy mà lần nào cũng không gặp, khi nó ngủ, khi nó đi thăm cụ ngoại, khi mẹ nó ẵm đi chợ chơi, thế là chị lại lủi thủi về. Lần này chị gắng ngồi đợi, ước sao mẹ thằng bé thấu lòng chị mà cho chị vào buồng trong ngắm nó tý. Nhưng mẹ nó chỉ ngồi buôn chuyện bô lô ba la, mà không hề có ý cho chị vào buồng.
 
- Bác gọi cháu là Ki tô không đúng đâu, tên cháu là Ki-an-tô cơ. -  Mẹ thằng bé nhắc khéo.
 
Chị Bé cười, tay vuốt lại món tóc bạc thưa xõa xuống bên má, ém nó vào sau lá tai héo khô:
 
- Khổ, bác cố gọi Ki tô… hai chữ đã méo mồm rồi.
 
Chưa kịp nghe mẹ nó trả lời thì chị Bé đã loáng thoáng thấy tiếng bà Tảo “Ơ, cái Bé nó đi đâu không nhổ cỏ vườn?”
 
Chị Bé lập cập đứng lên, cái ụ gù sau lưng chị nhức nhối trong lớp áo công nhân cũ xanh bợt.
 
- Lúc khác bác sang chơi với cháu nhé. – Mẹ Ki tô tỏ ra ái ngại khi chị đã sang chơi năm lần mà chưa gặp thằng bé.
 
Ừ. Chị Bé ấp sụp cái mê nón rách vá miếng vải nhựa trên chóp xuống mái đầu bạc, như sợ mẹ thằng bé đọc được nỗi khát thèm của mình. Tội nợ chưa, năm mươi lăm tuổi đời mà còn khát một đứa bé. Nhưng chị khó có thể kiềm chế. Chị sợ rằng nếu lần này mà không gặp được thằng bé, chị sẽ đánh mất một thứ quý giá nhất đời chị, và mãi mãi không thể có được nữa.
 
Chị ngồi xuống bên một luống rau muống cạn, cỏ đã lên tốt quá, mùi nhựa cỏ tươi xông lên mũi ngạt ngào. Chị mới vặt sạch cỏ cách đây ba hôm mà nay nó đã lên xanh ngùn ngụt. Thứ cỏ lá lúa, ngọn bò tới đâu cắm rễ tới đó, kinh hoàng. Luống rau bên cạnh, bà Tảo cũng đang rứt cỏ soàn soạt, miệng bà quát tháo sàn sạt:
 
- Ngày nào mày cũng lê… ồn sang nhà nó ăn cứt à? Nhà nó giàu, nó khinh mày như chó ghẻ mà mày còn cố đấm ăn xôi. Mày chờ nó đổ cơm thừa canh cặn lên đầu hay sao mà mày ngồi thối đít ra ở đấy, để mấy con mẹ mày nhảy vào phá nát vườn rau…
 
- Cháu có xin xỏ gì đâu thím. - Chị Bé thở dài. – Chỉ là cô Phương năm năm nay mới ở nước ngoài về, cháu muốn hỏi thăm cô ấy một tiếng.
 
- Cái đồ giẻ rách nhà mày thấy người sang bắt quàng làm họ. Tại sao lại có cái giống này lọt vào nhà tao cơ chứ. Mày có nhanh tay nhặt cỏ đi không? Sao cứ thần lần như ngỗng ỉa thế!
 
Bà Tảo vung tay ném bụp túm cỏ cả rễ lẫn đất vào đầu chị Bé, miệng tiếp tục rỉa rói chị bằng những từ ngữ độc địa nhất mà bà sưu tập hoặc tự nghĩ ra. Rỉa rói cũng như thở vậy, phải đều đều và liên tục, không thì sống sao được với chuỗi ngày buồn tẻ xó nhà quê này. Chị Bé đã quen với việc nghe thím dâu rỉa rói, và chị tập mãi rèn mãi, sao cho khỏi tủi cực, khỏi hờn giận. Nhưng chị tập suốt năm chục năm nay mà vẫn chửa quen. Có đêm thím dâu chửi dữ quá, chị Bé phải né sang góc cổng nhà hàng xóm ngồi nhờ, rồi ngủ gục ở đấy, sớm hôm sau lập cập trở về, cõng thêm tội ngủ lang, lại ăn thêm trận chửi mới!
 
Trong làng Thanh ai cũng thống nhất với nhau rằng chị Bé là người khổ nhất. Người ta thường chỉ phải làm dâu một thời, nay chị Bé coi như phải làm dâu cả đời. Ừ thì ít ra cũng có một cái danh Nhất, dù là người khổ nhất.
 
Bố chị mất khi chị còn trong trứng nước. Mẹ chị cũng mất nốt khi chị mới lên ba. Chú Tảo khi ấy còn chưa vợ đón chị về nuôi. Đàn ông đàn ang trẻ tuổi nuôi con mọn vật vờ, nhưng dù sao thì Bé chỉ đói chứ không bị rỉa rói. Bé được năm tuổi thì chú Tảo lấy vợ, Bé cực nhục từ đấy.
 
Thím Tảo không hay đánh Bé, nhưng rỉa rói thành thần. Những lời đay chì, như những mũi kim độc, cắm sâu vào từng lỗ chân lông Bé, đau buốt triền miên. Thím muốn thể hiện một thứ quyền lực tội nghiệp với ai đó, bất cứ ai trên đời này, nhưng ai cho thím thể hiện? Dễ nhất là chị Bé. Chị ở ngay bên, không dám cãi một nhời, nhu mì cam chịu hơn con giun, quá dễ để thím Tảo vươn móng vuốt quyền lực ra tóm lấy chị.
 
Hồi lên mười một tuổi, Bé ước được đi lấy chồng. Một ước mơ tội lỗi, thầm kín, đáng xấu hổ của một đứa bé gái mồ côi mười một tuổi. Bé không biết lấy chồng thì sung sướng gì, nhưng ít ra cũng thoát khỏi thím Tảo. Thím ấy nghiện rỉa rói Bé rồi, nghiện ngày càng nặng. Bé chỉ có cách trốn khỏi tầm mắt thím.
 
Ở tuổi mười hai, đang lúc Bé bắt đầu trơn lông đỏ da chuẩn bị dậy thì, cao lên từng ngày, gần được thước rưỡi, thì bỗng đâu Bé bị sốt, li bì ba ngày không ăn gì được. Chú Tảo rồi thím Tảo thay nhau đổ nước cháo, giã cây sài đất sống vắt lấy nước cho Bé uống hạ nhiệt. Sốt nóng quá, Bé cứ nằm lỳ trên cái võng cho thoáng. Thím Tảo đã lắc đầu nghĩ đến chi phí cho một đám ma thì Bé tỉnh lại. Chả biết nên vui hay nên buồn nữa.
 
Bé tỉnh lại với một cái bướu trên lưng như lạc đà. Bé không đứng thẳng lên được, cố ngóc cổ lên chỉ cao chưa tới một mét ba. Bé có tên mới - Bé gù. Từ đó, giấc mơ giải thoát bằng một đám cưới cũng lụi tàn trong Bé. Chẳng có ai điên đi cưới một đứa con gái đã mồ côi, không có một cắc hồi môn, lại gù!
 
Những lúc đau đớn vì đã lưng gù lại phải gánh lúa, Bé đổ tại cái võng. Lúc ốm xương cốt nhũn nhão, cái võng cong cong uốn lưng Bé cong theo, khi tỉnh dậy thì gù lưng mất rồi. Tức mình Bé gù chửi rủa cái võng. Cái võng vô tri chắc nó không đau khi nghe chửi độc.
 
*
*     *
 
Ấy thế mà chị Bé không bỏ được cái võng oan nghiệt. Mùa hè nóng bốn mươi độ, chị bật quạt điện cho mát, tức thì chú Tảo lại rỉa:
 
- Mày làm gì ra tiền mà mướn quạt!
 
Rỉa xong chú giập cầu giao, sợ nhỡ đâu lúc chú ngủ say, chị Bé lại bật quạt thì chết tiền điện. Mỗi tháng phải trả hơn chục ngàn tiền điện mà bà Tảo đã đay nghiến rồi. Nhà Tảo chỉ thắp mỗi bóng đèn bằng hạt lạc lúc chập tối, bật ti vi xem thời tiết xong rồi tắt phụt đi ngay cho đỡ hao điện.
 
Chị đành quay lại với cái võng, muỗi vo ve ru chị ngủ, võng đu đưa dỗ dành cái bướu ở lưng chị. Cái võng đẩy chị vào kiếp con gù, nhưng rồi chị cũng có bỏ nó được đâu. Nó cứ ung dung đu đưa đu đưa. Mùi rau cần tươi thoang thoảng thơm lặng lẽ theo trăng bay vào cửa sổ ngay đầu võng. Cái mùi này nó thành mùi người chị rồi cũng nên. Mùa hè thì người chị tỏa ra mùi nhựa rau muống, mùa đông tỏa mùi rau cần. Chị thích mùi rau cần hơn. Nó thấm vào óc, xông nhẹ đi những u uất.
 

Ảnh: Phạm Duy Tuấn
Thôi thì không lấy được chồng, phải ở với chú thím, chị Bé mong ngày cậu Tần, con chú thím lấy vợ, cô dâu mới biết đâu gánh đỡ rỉa rói của thím Tảo cho chị. Ngày ấy cũng đến, Tần cưới một cô dâu dáng gày mảnh mành như đọt trúc. Sau ngày cưới một tuần, thịt thà thừa ra từ bữa cỗ đã hết, nhà Tảo quay lại thực đơn bình thường với tương, đậu kho tương, sung muối, và lọ muối vừng mặn chát. Cô dâu không ăn nổi, cũng không thay thực đơn nổi trước sự tằn tiện sắt đá của nhà chồng, bèn xúi chồng đòi ra ở riêng. Thế là chị Bé lại một mình chịu đựng.
 
Khổ quá, chị Bé lần mò đến nhà chú họ xa, kêu khóc với chú cho hả. Vợ chú bèn bảo, thôi, nhà nó ác miệng như thế thành quen rồi, mày cứ coi như không nghe thấy gì. Lấp lỗ tai đi là xong. Chắc kiếp trước mày là thằng đàn ông động gộ đi hiếp đàn bà, mày hiếp phải nó rồi, giờ kiếp này mày nên sống nhục mà trả nợ nó, mà mày cũng còn sống được mấy ngày nữa đâu…
 
Đúng thế, chị Bé mới ngoại ngũ tuần mà trông như tám mươi. Ra đường người lạ gọi chị là cụ. Chị gù, lọm khọm đã đành, tóc lại bạc trắng và da tái nhợt nhăm nhúm. Mắt bạc phếch mà bộ răng cửa lại đã rơi hết nên trông chị càng lão tợn. Già rồi vẫn bị mắng như đứa con nít. Có hôm, chán bị rỉa rói quá rồi, chị chạy sang nhà Bí.
 
- Bí, chị cho tôi ngủ nhờ một đêm cho đỡ nóng, ở nhà chú thím tôi không cho chạy quạt điện.
 
- Rõ chán chửa, không bao giờ bật quạt điện thì mua quạt làm gì?
 
- Chú tôi có mua đâu, quạt điện là con giai chú mua biếu đấy, nhưng chú thím không bao giờ dùng! - Chị Bé nói.
 
Sớm hôm sau, chị Bé còn đang ở trên giường nhà hàng xóm, đã nghe thấy bà Tảo chửi róc rách:
 
- Tổ sư con gù, mày đi ngủ mướn ngủ lang được bạc được vàng mày bỏ nhà bà nuôi mày từ thời không lá tóc đến khi đầu bạc. Mày về đây bà róc xương mày.
 
Bà Tảo chửi như hát hay. Không chửi thì nhạt miệng. Giống như sáng ra người ta phải làm ấm chè thì mới yên tâm đi hết ngày. Với lại, người lớn có quyền chửi mắng răn dạy người bé. Chửi như là sứ mệnh rồi. Tưởng rằng chị Bé nghe chửi thành quen, cứ lờ tịt đi thôi mà không nghe không thấu nữa. Cũng chẳng ai quan tâm nữa. Bởi lẽ, chị Bé đã mắc bệnh rồi. Những mũi kim độc róc ráy len vào trong tế bào não, từng thớ thần kinh. Không ai nhìn thấy vì nó âm thầm, ẩn náu, rí rách cắn rứt, dai dẳng không ngừng. Nó chỉ biểu lộ ra bằng sự xấu xí hốc hác già nua bên ngoài của chị Bé. Như bà lão tám mươi. Người vô tâm thì bảo, ai mà chẳng phải già, chẳng qua tạng chị Bé già nhanh.
 
*
*     *
 
Lần này thì chị túm được thằng bé Ki tô rồi. Đặt ba mớ rau cần ở bậc thềm nhà nó, như quà biếu, rau cần non ngó cần xanh nõn, chạm vào là thơm hắc nhè nhẹ, lan tỏa khắp hiên nhà. Chị Bé nhìn thấy Ki tô đang chạy vòng quanh bộ ghế tựa cổ. Chao ơi, thằng bé đẹp thiên thần! Quả đúng như linh tính chị mách bảo. Chị như được chạm khẽ vào hạnh phúc, nước mắt chị trào ra. Chị khựng lại trước ba bậc tam cấp, trân trối nhìn thằng bé, sợ chị chớp nhẹ mắt là thằng bé sẽ tan vào không khí. Nó chạy đến cửa, bỗng dừng lại nhìn chị, mái tóc nâu ánh vàng tuyệt đẹp, rực lên trước ánh sáng tràn vào từ cửa chính, má hồng rực như quả táo đỏ, đôi mắt nâu trong veo nhìn thẳng vào chị, đôi môi tươi hồng của nó chu lại kêu “tô tồ”.  Rồi nó lại quày quả chạy vòng quanh bộ bàn ghế. Khoảnh khắc ấy đã đọng lại trong đáy mắt chị Bé, như một bức ảnh rõ nét nhất. Bức ảnh thiên thần hạnh phúc lần đầu tiên bay xuống cuộc đời đau khổ của chị!
 
- Mời bác vào nhà chơi với cháu! – Mẹ Ki tô niềm nở mời chào. – May quá hôm nay bác sang chơi mà cháu nó thức.
 
- Rau cần chị trồng ruộng nhà, rau sạch, chỉ tưới nước tiểu trộn gio, chị vẫn nhổ bán ngoài chợ, nay mang ba mớ biếu em.- Chị Bé thật thà lập cập nói, mắt vẫn không rời Ki tô. Chị ước sao được chạm nhẹ vào nó, nhưng bàn tay chị đen đúa xương xẩu, móng tay xiêu vẹo cái còn cái mất, lại két đất và cáu màu vàng xỉn thế này, chị muốn mà không dám.
 
Giá như chị biết được kết cục của mình như thế, chị đã liều một lần lén ôm lấy thằng bé từ phía sau, để mà cảm nhận da sữa thơm tho ngầy ngậy, ấm áp mềm mại của nó. Nó cứ chạy quanh bộ bàn ghế, làm mùi thơm ngòn ngọt đáng yêu từ da thịt trẻ con lan tỏa khắp gian phòng rộng. Mũi chị Bé phập phồng.
 
- Kianto, lại đây với bác Bé nào! – Mẹ Ki tô gọi.
 
- Thôi thôi, bác bẩn lắm! - Chị Bé chối đây đẩy. Rồi vội rút trong túi ra một tờ tiền.
 
- Hồi mẹ mày đẻ, ở tận đẩu đâu nước ngoài, bác không mừng được, bây giờ bác đưa, chả đáng là bao, nhưng mẹ mày cầm lấy mua gì cho Ki tô ăn nhé.
 
- Bác khổ thế, làm gì có tiền. Nhà cháu không lấy đâu. - Mẹ thằng bé giãy nảy.
 
- Mẹ nó cứ cầm cho bác yên lòng. – Chị Bé nghẹn ngào. Khi người ta yêu, khi người ta thèm muốn, người ta chẳng tiếc thứ gì, cho đi được là hạnh phúc nhất.
 
*
*     *
 
Mặc cho bà Tảo chửi bới, rủa sả, cứ đi nhổ rau cần về, bán nhoáng nhoàng gánh rau xong là chị Bé lẩn sang nhà hàng xóm chơi với Ki tô. Chị mê mẩn nó đến mất ăn mất ngủ. Chị phải tranh thủ từng phút một, bởi chỉ mấy ngày nữa, mẹ thằng bé sẽ lại mang nó về phương trời nào đó, mà cả đời chị không tưởng tượng ra. Ki tô vẫn nhìn chị với ánh mắt trong veo, không hiểu được vẻ xấu xí u ám ruộng bùn của chị, bởi chú bé lai này chưa được hai tuổi, và cũng bởi chú ngây thơ quá, không biết đến nỗi yêu của chị dành cho chú. Đó là tình yêu tội lỗi của một người đàn bà xấu xí, sẽ không bao giờ có được một đứa con.
 
Vẻ đẹp hoàn hảo của chú bé Ki tô an ủi nỗi đau, xoa dịu tật bệnh của chị. Những ngày Ki tô ở đó, chị phơi phới vui, và quả thật quên được những rỉa rói chửi bới thường ngày đâm thấu tim chị. Vẻ đẹp mà chị ước mơ, theo đuổi và trở thành hiện thực, một vẻ đẹp trẻ con ước ao hiện hữu. Chị lén lút cho phép mình sở hữu vẻ đẹp thơ trẻ ấy. Chị chỉ nghĩ về Ki tô, và muốn mang tất cả mọi thứ cho nó. Chị được gặp Ki tô thêm hai lần nữa, nhưng vẫn cương quyết không chạm vào thằng bé. Khi yêu và tôn thờ quá mức, tiếc thay, ta chẳng thể chạm vào tình yêu ấy.
 
Ngày ấy cũng đến, mẹ con Ki tô bay về Hà Lan với bố nó, ấy là chị nghe nói thế. Chị mất ngủ bởi luôn nghĩ đến Ki tô. Nỗi nhớ bào mòn chị, nhưng chị hài lòng, bởi chị đã có thể gắn mình vào một sinh linh nhỏ bé, xinh đẹp và tràn đầy hạnh phúc. Thứ mà chị không bao giờ có. Cuộc đời đau khổ héo mòn của chị, cuối cùng, cũng có một chút ánh sáng, dù là vay mượn. Nhưng quan trọng gì, với chị, nó đáng giá, thế là đủ. Nó là hạnh phúc bí mật của chị.
 
Chị lại lầm lũi những tháng ngày nghe chửi, cố chịu đựng để chờ ngày Ki tô về nước. Hai năm thằng bé sẽ về nước một lần, mẹ nó bảo thế. Năm nào cũng đi đi về về thì có mà sạt nghiệp, tiền vé máy bay đắt như xắt vào thịt!
 
Chị nhớ mong từng ngày, thằng con trong tưởng tượng. Dù gì bây giờ đã khác xưa, chị đã có một niềm hy vọng mà bấu víu.
 
Đêm qua, bà Tảo thấy khó ở, không ngủ được. Mà không ngủ được thì bà lại tức, vì chuyện con cái Bé nó mang cho nhà Nếp ba mớ rau cần ế. Cái đồ ngu dại ấy, cứt đã chẳng dính đít lại còn có đồ đem cho. Rau cần ế mang về phơi khô rồi nấu độn cơm lại chẳng tốt à. Ế cũng đáng giá vài ba ngàn đồng chứ ít ỏi gì đâu! Càng nghĩ càng tức. Sáng nay, bà sẽ dựng nó dậy thật sớm để chửi cho hả hơi.
 
Đứng xế đầu giường chị Bé, bà Tảo bắt đầu réo rắt:
 
- Tổ sư con gù, giờ này còn ngủ mòng mắt thì lấy cứt đổ miệng à?
 
Chị Bé cố giơ cánh tay lên, chống xuống giường nhỏm dậy, nhưng rồi chị lại buông tay.
 
- À, con này hôm nay giỏi nhỉ, bà thì róc xương mày cho chó gặm giờ! Có dậy ngay ra đồng hái rau bán hay không thì bảo?
 
- Thím… cho con đi viện… - Chị Bé thì thào.
 
- Đi viện? – Bà Tảo thở dốc – Sang gớm nhỉ? Mày giả cách chứ gì? Dậy ra đồng hái rau ngay.
 
Nhưng chị Bé vẫn gan lỳ nằm đó.
 
Quái lạ, trước nay nó có bao giờ dám thế đâu? Bà nói một câu, mà thường thì chưa phải nói, nó đã tự động bò dậy, rửa mặt rồi cắp nón lù gù ra đồng hái rau khi còn nhọ mặt người. Hái xong gánh rau ra chợ bán trước người ta, chín giờ sáng bán xong lại tất tả ra đồng làm cỏ lúa… đến trưa trật mới dám lò dò về nhà dọn cơm hầu ông bà. Gần năm chục năm nay nó đều thế cả, kể cả lúc cảm cúm hắt hơi sổ mũi nó cũng khặc khừ đi làm, có bận còn ngất ngoài đồng anh Cu Bền phải cõng về. Mà nay nằm rệp không chịu dậy thì hẳn không phải trò đùa.
 
Bà Tảo lại gần, rờ lên trán chị Bé, làn da khô rang, nóng rãy. Bà rụt vội tay về:
 
- Ối ông Tảo, lại đây xem con cái Bé làm sao mà nóng sột lên này!
 
Ông Tảo bỏ dở nhang đèn lau dở trên bàn thờ, lật đật chạy lại, cũng rờ tay lên trán đứa cháu:
 
- Nóng quá! – Ông giật mình – Cứ y như hồi nó bệnh mười hai tuổi.
 
Ông biết, con cái Bé này dặt dẹo thế mà ít khi ốm vặt. Nhưng nó đã ốm là ốm to. Ốm đình ốm đám, không phải chuyện chơi.
 
- Tốt nhất là gọi xe cho nó lên bệnh viện. – Ông bàn.
 
Bà Tảo nghĩ đến tiền, thuê xe cũng mất dăm chục ngàn. Rồi viện phí nữa. Nó mồ côi, may ra bệnh viện không thu viện phí, nhưng còn tiền thuốc… Bà bóp bóp vạt áo nhũn nhẽo mấy đồng lẻ.
 
- Có nhanh gọi thằng cu Bền không! – Ông Tảo gắt.
 
Lần đầu tiên trong đời, chị Bé đi bệnh viện. Lần đầu tiên trong đời, chị không phải tất tả cắm mặt xuống ruộng rau, chị đơn giản nằm đó, có người ngồi cạnh chăm hẳn hoi, có người hỏi muốn ăn gì. Ôi chao, lạ quá, nhưng có điều chị mệt đến nỗi thở ra hít vào cũng khó khăn.
 
Được đâu chục ngày như thế, thì cu Bền lại được lệnh ông Tảo, lên bệnh viện đón chị Bé về. Bệnh viện đã khám xét hết cách, mà không thể biết bệnh của chị Bé. Họ trả về. Ông bà Tảo nghĩ thế là hết nhẽ rồi. Bệnh viện mà họ trả về thì ông, bà làm gì hơn được!
 
Chị Bé về rồi, nhưng bà Tảo thôi không rí rách chửi nữa. Chửi cái xác ốm nằm đấy thì bõ bèn gì! Nhìn chị Bé nằm cong queo trên giường, da mặt trắng bệch không còn dấu hiệu của sự sống, bà Tảo bỗng gai người. Nhớ lại hồi nó mười hai tuổi, nằm ốm trên cái võng rách, ba sợi thừng đứt thõng xuống đu đưa, bà đã có chút hả hê, khi cơn bạo bệnh làm nó xấu xí, dị dạng, nó không xứng ngồi tót trong lòng chú nó, như một lần bà bắt gặp, dậy thì nổ đốt mơn mởn như bông huệ non nhí nháu vòi vĩnh.
 
Bà lại lo, kiểu này nó không dậy nữa đâu, lấy đứa nào đi hái rau bán rau, lấy đứa nào nghe bà chửi mỗi khi bà khó ở, mà bà thì thường xuyên khó ở. Xương cốt đau lẩn nhẩn kinh niên làm bà không lúc nào nhẹ nhõm vui lên được. Ông ý thì bà chả dám nói nặng một câu, ăn bánh đấm, chả đá ngay lập tức.
 
Chị Bé cứ trôi từ cơn ác mộng này sang cơn mê đẹp khác. Lúc chị bị thím Tảo lấy dao róc xương, xương trắng hếu lộ ra mà không thấy máu chảy, hay là chị nóng sốt dữ quá, máu cũng khô ráo cả rồi. Lúc mê đẹp thì chị thấy Ki tô về nước, nó lại ôm con chút chít chạy vòng quanh chị, miệng không kêu tô tồ mà kêu Bé, Bé, mùi thơm từ da sữa con trẻ ngòn ngọt mê hồn lan tỏa cả một vùng không gian. Chị mỉm cười hạnh phúc, mũi phập phồng, rồi thoáng cái, lại ngửi thấy mùi rau cần non thơm hăng hắc quanh chị và thằng bé. Ki tô, nó đứng trước mặt chị, làm nên một thiên đường rực rỡ, một vẻ đẹp lần đầu tiên chị nhìn thấy và ngưỡng mộ. Chị tôn thờ nó. Nó soi sáng cuộc đời tối tăm nhục cực muôn phần của chị. Nhưng chị không thể yên lòng nhắm mắt, chị muốn nhìn thấy Ki tô nhiều hơn, gần hơn, chị sẽ liều mình chạm vào má thằng bé, bằng ngón tay khô cong queo và cáu màu bùn ruộng của chị. Tha lỗi cho bác Bé, Ki tô, bác hôi hám quá, nhưng bác thèm được chạm vào con!
 
Bác biết là bác không có quyền mơ ước con, nhưng bác đã làm thế, và bác sẽ tiếp tục làm thế, trong âm thầm.
 
Sốt Sốt Sốt. Máu khô đi cho tới khi cạn kiệt. Chị Bé cũng không nuốt nổi giọt nước nào nữa. Chị cố chống cự lại cái chết, chị muốn nhìn thấy Ki tô thêm nữa. Nước mắt chị ứa ra. Chao ơi là khổ, trước đây mỗi lần bị rỉa rói khiếp đảm, chị muốn chết, mấy lần muốn lao đầu xuống giếng làng, bởi cái chết còn nhẹ hơn việc sống, dễ dàng hơn việc sống, và biết đâu, vui hơn kiếp sống này. Nhưng mà còn Ki tô?
 
Ki tô ơi giờ này con ở đâu? Bác sẽ không được chạm vào con nữa ư?...
 
Mùi rau cần non thơm hăng hắc nhẹ nhàng bay lên từ vạt ruộng ven đường cái, xen lẫn mùi nhang phảng phất... 
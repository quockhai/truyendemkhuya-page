Tàu lại vào ga, đã có mấy lần tôi tỉnh dậy sau cái đỗ khựng người lại. Vẫn còn chưa tới ga Tokyo, nơi tôi cần đến để tiếp tục cuộc hành trình dài lê thê của mình. Dạo này tôi thèm ngủ ghê gớm, hễ đặt lưng xuống giường là tôi ngủ ngon lành, chẳng mộng mị như cái thủa tôi còn là một cậu bé con suốt ngày đắm chìm trong những chuyện thần tiên hay trinh thám ly kỳ.

Điện thoại rung lên bần bật, tôi nhìn vào màn hình không hiển thị số. Điện thoại của mẹ! Tôi đoán vậy vì các cuộc gọi từ nước ngoài chẳng mấy khi báo số trên con dế nhỏ của tôi. Hầu như tôi không thể nghe rõ lời mẹ nói, chỉ có tiếng tàu điện lao vút vào khoảng không với thứ thanh âm kim loại cao tốc rên xiết trên đường ray.

“Con có sao không?...”

“Con không sao mẹ ạ, mẹ yên tâm đi. Con sẽ gọi lại cho mẹ khi trở về nhà.”

Câu trả lời của tôi dành cho sự lo lắng của mẹ bao giờ cũng vậy. “Con không sao, mẹ yên tâm”. Nó giống như một thứ phản xạ in trong não bộ tôi, khi gặp điều kiện lập tức nó sẽ tự bật ra. Thú thực tôi không muốn mẹ phiền lòng vì mình.

Các bà mẹ bao giờ cũng vậy, chẳng bao giờ thấy con cái mình trưởng thành. Chính vì vậy, càng ngày tôi càng ít nói với mẹ về những rắc rối của bản thân. Chỉ khi nào có niềm vui, tôi cóp nhặt lại, nuôi nó khôn lớn và kể với mẹ để dành cho bà những bất ngờ nho nhỏ hay chí ít ra cũng chỉ là một nụ cười.

Hai mí mắt tôi lại rơi xuống nặng nề. Hôm nay tôi phải tỉnh dậy lúc 5 giờ sáng, vượt một chặng đường xa đến cả gần một nghìn cây số cho một cuộc gặp bắt đầu lúc quá trưa. Xong việc tôi lại quay trở về bằng từng ấy cây số mà nhẩm tính cũng phải gần nửa đêm mới có thể chạm được vào cánh cửa của ngôi nhà.

“Lại thêm một vụ giết người nữa!”

Tôi lẩm nhẩm và định bụng vào nhà tắm trước khi nước trong bồn trào ra ngoài, chợt sực nhớ tới cuộc điện thoại của mẹ lúc ban chiều. Tôi với vội lấy ống nghe và bắt đầu bấm một dãy số dài quen thuộc. Không biết chừng mẹ đang chờ đợi cuộc gọi của con trai. Hẳn bà đang đứng ngồi không yên với hàng chuỗi những hình ảnh rùng rợn trên bản tin thời sự quốc tế ở quê nhà?

Giải tỏa sự lo lắng của mẹ xong, tôi tiếp tục dán mắt vào màn hình nơi đang truyền đi những hình ảnh đầm đìa máu. Xác nạn nhân được phủ bằng những chiếc ga trải trắng toát. Không phải là một người mà có đến bảy người bị thiệt mạng, cùng với họ là những người bị thương được các nhân viên y tế chuyển vào bệnh viện trên những chiếc xe cấp cứu rồ ga inh ỏi.

Bình luận viên mang vẻ mặt đớn đau cố hữu và thốt ra những lời bình muôn thủa… Xót xa cho số phận của những người xấu số, trước khi ngã xuống trên vũng máu oan nghiệt, chắc hẳn mỗi người trong số họ đều có cuộc sống riêng của mình với bao ước mơ, hy vọng xen lẫn cả những thất bại hay thử thách.

Có lẽ cuộc đời họ đã rẽ theo hướng khác nếu như họ không phải gánh chịu lưỡi dao oan ức và nghiệt ngã của tên cuồng sát, một kẻ máu lạnh ngụy trang bằng cặp kính cận dày cộp trông lương thiện và lành hiền như một trí thức nửa mùa. Những vẻ mặt như vậy có thể dễ dàng bắt gặp ở bất cứ đâu, trên những con đường tôi đi qua: công viên, siêu thị, nhà máy, sân ga…hay trên những con tàu điện oằn mình sớm tối.

Ngẫu nhiên thực, khi cuộc điện thoại của mẹ rung lên cũng là lúc tôi chỉ còn cách Akihabara một ga nữa. Và nếu như ngày hôm qua, nếu như tôi đến đó, không biết chừng… Ơn trời, tôi vẫn còn ngồi đây!…

Có hẳn một âm mưu giết chóc đã được lên kế hoạch. Sát thủ gửi hàng loạt các tin nhắn lên trên một trang web như một bản tường thuật sống mạch lạc và man rợ.

Ngày, tháng năm…

Thuê được một chiếc xe tải

Chuẩn bị xuất phát thôi!

...

Đã đến nơi rồi…Akihabara

Ta sẽ giết, giết hết các người…

...

Buồn chán.

Sống mà thế này chết còn tốt hơn nhiều.

.....

Xã hội Nhật ngày càng xuất hiện nhiều những vụ kỳ dị. Sau hàng loạt các vụ giết người tàn bạo được liên tiếp đưa lên màn ảnh, đã châm ngòi cho một cuộc khẩu chiến xoay quanh vấn đề xuống cấp đạo đức cũng như mất phương hướng của một bộ phận giới trẻ. Một đất nước vốn vẫn được coi là an toàn như Nhật Bản vậy mà trị an lại đang lâm vào thế bất ổn. Phần lớn tội ác xuất phát từ những căng thẳng, bế tắc cá nhân và lý do của nó đơn giản đến bất ngờ.

Tôi vẫn thường nói với bạn bè rằng Nhật Bản là một xã hội im lặng, giống như một quãng nước sâu lững lờ phía trên nhưng ẩn mình bên trong là những vùng xoáy mãnh liệt. Ở đó con người chỉ biết giam mình vào công việc và tù túng với những phép tắc trên dưới nghẹt thở. Môi trường đó sản sinh ra những con người “im lặng” trong một trạng thái “im lặng”. Sự căng thẳng triền miên đó làm cho con người tự đóng cửa lòng, giam hãm mình vào thế giới riêng.

Hệ lụy của sự bế tắc, không lối thoát đó dẫn đến những vụ tự sát, nổi loạn, giết chóc. Sau mỗi sự kiện, người ta có thói quen lập ra một ủy ban đối sách nhằm ngăn chặn các hành động tương tự, nhưng có khi nào họ nghĩ đến một giải pháp căn bản nhằm phá vỡ tảng băng lạnh lùng kia đang chế ngự hầu hết toàn xã hội, khối băng nặng nề bắt họ phải im lặng đến khát thèm hơi ấm đồng loại nhưng không thể nói ra chỉ vì những nguyên tắc bất thành văn?

Tự nhiên tôi liên tưởng đến hình ảnh về một chiếc mặt nạ, nó bị đá bung ra và sau lớp ngụy trang dày hiển hiện lên một khuôn mặt thật, nhìn rõ hình hài. Và cả câu chuyện tôi đọc tối qua nữa chứ, trong đó có đoạn: “Cánh đồng lúa rực lên màu nắng. Nơi chân trời xa xôi hiện lên từng đám mây trắng xốp trông như những bộ lông cừu”.

Có phải hồn tôi đang ở nơi đấy không?

 

Osaka, Japan tháng 6/2008
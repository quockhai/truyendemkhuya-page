Cậu thiếu niên viết một dòng chữ trên mặt đất:
“Cho cháu một đồng, để gọi điện thoại về nhà.”
Đây là một cậu thiếu niên mới đi ra khỏi nhà, cậu chỉ mới mười ba, mời bốn tuổi. Cậu thiếu niên rất đói, muốn xin tiền để mua cái ăn. Vì thế, cậu thiếu niên viết dòng chữ lên mặt đất. Đương nhiên, dòng chữ này không phải là phát minh của cậu thiếu niên. Trước đây, khi cậu thiếu niên chưa đi ra khỏi nhà, cậu đã thấy người khác xin ăn kiểu như vậy. Hiện tại, đến khi cậu thiếu niên đói đến không chịu nổi, đành phải bắt chước làm theo cách ấy.
Nhưng, cậu thiếu niên rất khó xin được tiền, rất nhiều người đều biết đây là một trò lừa bịp người ta, nên không cho tiền. Cậu thiếu niên đứng ngây tại chỗ rất lâu, cũng không có người nào cho tiền.
Sau đấy, có một cậu bé khoảng bảy, tám tuổi đi tới. Cậu bé được người lớn dẫn đi chơi. Cậu bé trông thấy dòng chữ trước mặt cậu thiếu niên ấy, cậu bé nói với người lớn: “Mẹ ơi! Cho con một đồng nhé?”
Người lớn nói: “Con xin tiền để làm gì?”
Cậu bé nói: “Anh kia muốn gọi điện thoại về nhà, chúng mình cho anh ấy một đồng  được không?”
Người lớn nói: “Trò lừa tiền đấy!”
Cậu bé nói: “Mẹ ơi! Mẹ cho con một đồng nhé?”
Người lớn bèn cho cậu bé một đồng.
Cậu bé đem một đồng tiền đưa cho cậu thiếu niên.
Nhưng, cậu thiếu niên đâu có đem đồng tiền ấy đi gọi điện thoại, cậu thiếu niên vẫn đứng tại đấy, không động đậy. Nhìn thấy thế, cậu bé bèn hỏi: “Anh ơi! Anh đi gọi điện thoại đi chứ?”
Cậu thiếu niên đứng đấy không nổi nữa, cậu thiếu niên bước đi, nói với cậu bé mình đi gọi điện thoại. Song, cậu thiếu niên đâu có đi gọi điện thoại, cậu thiếu niên cần tiền, có tiền rồi là có thể mua cái ăn.
Sau khi không nhìn thấy cậu bé nữa, cậu thiếu niên bèn dừng lại, đồng thời viết lại dòng chữ mới: “Cho cháu xin một đồng, để gọi điện thoại về nhà.”
Điều cậu thiếu niên không ngờ được rằng, một lát sau, cậu thiếu niên lại trông thấy cậu bé. Cậu thiếu niên hơi cảm thấy ngượng ngùng, không dám ngẩng đầu nhìn cậu bé.
Đương nhiên, cậu bé cũng nhìn thấy cậu thiếu niên.
Sau khi trông thấy cậu thiếu niên, cậu bé bỗng nhiên lại nói với người lớn: “Mẹ ơi! Mẹ cho con một đồng tiền nữa nhé?”
Người lớn nói: “Con lại xin tiền để làm gì?”
Cậu bé nói: “Cho anh kia ạ!”
Người lớn nói: “Cậu ta đã đánh lừa con, không đi gọi điện thoại, con còn cho cậu ta tiền để làm gì nữa?”
Cậu bé nói: “Có lẽ anh ấy đã đi gọi, nhưng chưa gọi được! Mẹ ơi! Mẹ lại cho con một đồng nữa đi?”
Người lớn lại cho cậu bé một đồng tiền.
Cậu bé lại đưa tiền cho cậu thiếu niên.
Cậu thiếu niên thật sự xấu hổ, lần này, không để cậu bé giục đi gọi điện thoại, cậu thiếu niên bèn chạy đi ngay.
Sau khi chạy đi, cậu thiếu niên trở lại mua một bát miến, bát miến không có thịt, hai đồng một bát. Ăn xong, cậu thiếu niên tìm một nơi khác, lại viết xuống đất một dòng chữ như thế.
Điều khiến cho đến bản thân cậu thiếu niên không dám tin nữa: Một lát sau, cậu thiếu niên lại nhìn thấy cậu bé nọ. Cậu thiếu niên muốn chạy trốn, song cậu bé đã nhìn thấy cậu rồi.
Cậu bé đi  đến gần cậu thiếu niên, hỏi cậu: “Anh ơi! Vì sao anh lại ở đây chứ, anh chưa gọi được điện thoại à?”
Cậu thiếu niên mặt đỏ lựng, không hé răng.
Cậu bé không tiếp tục hỏi cậu thiếu niên nữa, mà nhìn người lớn nói: “Mẹ ơi! Mẹ đưa điện thoại di động cho con?”
Người lớn nói: “Con lấy điện thoại di động để làm gì?”
Cậu bé nói: “Cho anh kia gọi điện thoại mà!”
Người lớn nói: “Cậu ta không biết gọi đâu!”
Cậu bé nói: “Anh ấy biết gọi đấy! Con tin anh ấy không lừa con đâu!”
Người lớn bèn đưa điện thoại di động cho con, cậu bé lập tức cầm điện thoại di động đưa cho cậu thiếu niên. Cậu thiếu niên cầm lấy, nhưng không gọi điện thoại, chỉ đứng ngây ra.
Cậu bé nói: “Anh gọi đi! Gọi điện thoại cho người nhà anh đi!”
Cậu thiếu niên bấm số máy, lát sau, cậu thiếu niên nức nở, nghẹn ngào nói: “ Mẹ ơi! Con muốn về nhà!...”
“Trên đất Giàng Ô, đừng có ai dại dột mà thách đố Sùng Kathy bất kể một điều gì!” Đó là lời khuyên cho tất cả các tay buôn trong vùng nếu chưa một lần được bước chân vào ngôi nhà sàn lớn của ông trùm lâm sản khét tiếng mới gần 30 tuổi. Người ta vẫn kể đi kể lại về chuyện hàng chục tay buôn trong vùng cùng mang lễ đến dâng cho người thắng cuộc, họ đã thua trùm rắn vì dám thách thức ông cưới được thiếu nữ đẹp nhất Giàng Ô, khi ấy nàng 18 tuổi.
 
Đám rước dâu lớn, người ngựa đứng chật kín từ đầu con dốc đến sân túp lều cỏ chênh vênh phía trên sườn thung lũng. Người người kéo đến xem đám rước dâu lớn nhất từ trước tới nay không ai được chiêm ngưỡng vẻ đẹp của nàng ấy khi mặc áo cưới truyền thống, bởi người Nùng Giàng Ô có tục trùm tấm vải đen lên cô dâu để che vía nàng không còn nhớ đường về nhà Pa mẹ. Chú rể, người giàu nhất Giàng Ô không có mặt trong đám rước mà đang ngồi bên bàn rượu trên gian nhà lớn.
 
 
 Minh họa: Thành Chương
 
Mấy năm gần đây, các tay buôn hàng sống đều đã quy về một mối của Sùng Kathy, chỉ còn ông Ngò vẫn móc nối được mối làm ăn bên cửa khẩu, thế nên nhân dịp này Sùng Kathy mời ông đến dự đám cưới nhân tiện thương lượng chuyện làm ăn.
 
- Tôi sẽ mua với giá gấp đôi những người bán lẻ khác nếu bác chịu đổ hàng về đây.
 
Tiếp tiếng của trùm rắn là tiếng đập tay xuống bàn chát chúa.
 
- Sùng Kathy, anh tưởng anh là ai mà dám mặc cả với Ngò rắn hả?!
 
- Bác nhìn lại xem, tất cả rắn trên vùng này đều là của Sùng Kathy. Nể bác nên tôi mời đến thương lượng, nếu không ưng thì nhất định bác phải đấu thắng thua với tôi!
 
- Tao chưa bao giờ biết sợ ai trên cái đất này.
 
- Nếu bác chứng minh được mình sành về rắn hơn thì cháu chịu thua, còn nếu bác thua thì từ nay về sau phải giải nghệ. Ngược lại cháu cũng sẽ làm thế nếu bác thắng.
 
- Đấu thế nào?
 
- Trong ba món rắn trên bàn sẽ có một món độc. Bác phải dùng tất cả ba món đó theo thứ tự bác chọn, nếu ăn món có thuốc giải trước thì không sao, nhưng phải món có độc thì hậu quả bác biết rồi đấy!
 
- Sùng Kathy? Mày muốn giết người à?
 
- Nói thế là bác nhận thua?
 
- Tao sẽ không thua!
 
Tiệc cưới của Sùng Kathy thật đặc biệt. Ba món cỗ chính được chế biến hoàn toàn từ các loại rắn: Thịt rắn thập cẩm viên chả lá lốt nướng, khúc trăn hầm dương xỷ nếp, cốt hổ chúa chiên giòn. Trên mỗi bàn đều có một con cạp nia hấp trong tư thế ngóc đầu khỏi nồi rất sống động khiến ai cũng vô cùng kinh ngạc, thán phục.
 
Rượu rót tràn bát sành, cỗ bàn không ngừng tiếp ra từ bếp. Trên những khuôn mặt khách hoan hỷ, hơi rượu đã phừng phừng. Ai cũng trầm trồ khen ngợi Sùng Kathy, mồ côi từ nhỏ nhưng cất được ngôi nhà to nhất vùng, cưới được cô gái đẹp nhất xứ…
 
Nhưng chú rể Sùng Kathy dường như không để tâm đến những lời chúc phúc. Ngồi trước mâm cỗ, mắt Cathy giương chòng chọc vào ông Ngò như thôi miên.
 
Cặp đũa tre nhuộm đỏ trong tay ông Ngò run lật bật. Phải chọn món nào đây? Chọn sai món là toi mạng với cái thằng ôn Nùng này! Tiếng tăm về sự chơi độc của Sùng Kathy ông đã biết đến nhưng giờ đây mới thực sự đối mặt. Cả đời ông đã sống cùng rắn, ăn ngủ cùng rắn, các loại rắn từ thường đến cực độc trước nay ông đều coi thường. Vậy mà giờ đây, chỉ với mấy con rắn đã chết biến thành món ăn trên bàn khiến ông run sợ.
 
Hít vào thật sâu lấy lại khí thế, ông Ngò quyết định gắp món cốt chúa chiên trước.
 
Ông chầm chậm nhai. Chầm chậm nghe ngóng… Khách khứa kéo đến vây xúm xít quanh mâm Kathy và Ngò rắn. Hàng trăm cặp mắt đổ dồn vào miệng ông Ngò. Hàng trăm quả tim hồi hộp đập. Không khí đám cưới từ rôm rả tươi vui đã chuyển dần sang ắng lặng, căng thẳng đến nghẹt thở.
 
Ông Ngò đã nếm xong ba món và ngồi im lặng chờ đợi. Bỗng người bên cạnh hoảng hốt rú lên khi nhìn vào khuôn mặt đang dần biến dạng của ông, những tia máu tím nổi lên từ bên hai mang tai lan dần khắp má, mắt và mũi, trước khi đổ ào xuống, ông Ngò chỉ kịp thét lên:
 
- Giàng ôi...! Sùng Kathy… mày... thật là... độc ác!...
 
*
*     *
 
Katay thở dốc vì căng thẳng, mồ hôi lấm tấm trán. Cậu trỏ điều khiển vào đầu video, màn hình vụt tắt. Đĩa VCD này Katay đã xem đi xem lại nhiều lần nhưng mỗi khi đến câu nói đó thì cậu bị kích thích mạnh nên không đủ can đảm xem tiếp. Chiếc đĩa còn nóng hổi trên tay, là mẹ đã đem sao từ cuộn băng hình cũ, đưa cho Katay trước khi bà qua đời và dặn tuyệt đối không được để cha hay bất kể ai nhìn thấy.

 
 Ảnh: Phạm Duy Tuấn
Trên ghế salon gỗ, tiếng ngáp nhỏ dần thế vào đó là hơi thở nhè nhẹ của một giấc mơ trẻ thơ chập chờn...
 
…“Vào mùa sinh sản, khi ấy ổ trứng của chúa mẹ chỉ nở được bảy chú chúa con. Nó lặng lẽ cuốn những quả trứng tưởng không bao giờ nở ném ra ngoài cửa hang. Bỗng nhiên có vật gì đó nặng trịch đè lên đầu, cổ bị sợi dây thắt nút siết chặt lại, lưỡi chúa mẹ cố thè ra nhưng không thể phun nọc độc như những lúc tấn công con mồi. Chúa mẹ quẫy mạnh đuôi, tuyệt vọng nhìn về phía cửa hang, một quả trứng trong đám mà nó tưởng đã bị ung nứt làm đôi, chúa con tách lớp vỏ đục vàng, ướt nhèo trườn ra ngoài.”…
 
Con Win-110 phi thẳng vào dưới gần sàn nhà lớn, tiếng vít ga thử máy khiến Katay choàng tỉnh giấc. Pa đi săn rắn về. Gọi là săn nhưng chính xác là thu mua từ các bản lân cận.
 
- Katay! Xuống đây!
 
Katay không đi cầu thang mà tụt luôn từ trên sàn nhà theo cây cột lớn xuống.
 
- Xem này Katay, thích không?
 
Katay trống đầu gối, hỏi trống không:
 
- Mấy con?
 
- Tám. Chưa đủ một đàn!
 
Katay ló mắt vào chiếc lồng sắt để bên cạnh. Đám chúa con bằng cổ tay bị đổ ào xuống lồng nhớn nhác trườn khắp tìm nơi ẩn nấp.
 
Pa đẩy một chiếc lồng khác đến. Cậu giúp hé miệng túi cước, từ bên trong ló ra một cái đuôi đen không còn nhọn lắm, rồi từng khúc, từng khúc cuồn cuộn dần dà trườn ngược lên. Hổ chúa lớn tưởng sắp thoát khỏi túi cước ngột ngạt, nhưng khi cái cổ gần được giải phóng thì đột ngột bị chặn lại, Sùng Kathy đưa tay nắm chặt cổ chúa lớn lôi ra.
 
- Nhìn xem! Có hơn hai mét không? Chúa cái ta tóm cách đây mấy tháng, giờ mới đến hắn và đàn con.
 
Pa đổi giọng ngay ở câu sau, tiếng ông âm âm, ngạo nghễ. Ông cẩn thận dùng lưỡi dao sắc lẹm cắt từng mối chỉ khâu vụng về trên mép hổ chúa, chiếc nanh trắng trồi ra ngoài, lóe sáng.
 
Sùng Kathy là tay chơi rắn khét tiếng, nhưng không phải loại nào cũng mê. Ông thu mua tất cả các loại rắn và xuất ngay khi đủ chuyến. Riêng hổ chúa, mỗi khi săn hay mua được, ông thường giữ lại rất lâu, nuôi đến chán ngán mới cho đem đi. Thuở còn thanh niên Sùng Kathy sang Macao chỉ để xăm hình một con rắn lớn cuộn vòng quanh người, mắt thường thì cho rằng đó là một con mãng xà, ai thật sành về loài rắn mới nhận ra được là hổ chúa ở ánh mực tàu đổ tinh xảo thỉnh thoảng lại lóe xanh.
 
Dưới nhà sàn lớn có một tầng âm, không khí ẩm mốc, máy phát điện ngoài suối chạy ngày đêm để ánh sáng lúc nào cũng được thắp trong này. Kathy kê một chiếc giường sát bên cạnh lồng hổ chúa. Vào những ngày không đi săn ông ở luôn dưới này, khi ấy Katay không được phép bén mảng xuống, cậu cũng chẳng bao giờ muốn biết lý do về điều đó.
 
Những ngày ấy Katay thường ra thăm mẹ, thường thì cỏ dại đã mọc kín xung quanh mộ, cậu không dám nhổ vì sợ mẹ nằm bên dưới sẽ bị đau nên dùng cây kéo cẩn trọng cắt từng ngọn cỏ trên mộ xếp thành một ngôi nhà cỏ nhỏ, nằm chênh vênh trên sườn của thung lũng Giàng Ô. Cậu không có ký ức nhiều về ông bà ngoại mà chỉ mang máng một điều họ sống trong ngôi nhà cỏ đó, giờ thì cả mẹ đã lần lượt rủ nhau về bên Giàng, chỉ còn hai cha con sống trong ngôi nhà sàn lớn trống vắng với rắn, càng nhiều rắn càng vui.
 
*
*     *
 
Sau khi đã chốt chặt then bên trong, Kathy cởi bỏ áo quần, người đàn ông đã hơn bốn mươi cuồn cuộn các múi cơ chạy dọc thân thể, con hổ chúa lớn trườn khúc khúc quanh người, chiếc đầu hình thoi của mãnh xà vươn ra phía sau gáy chực mổ xuống bất cứ nơi đâu, cái lưỡi đen láy chẻ đôi cựa cuội đến rùng rợn, đuôi thả chớm xuống mắt cá chân trái tinh xảo đến mức nếu không nhìn rõ sẽ tưởng nó đang vùng quẫy hòng thoát khỏi lớp da đỏ cháy nắng.

 
  Ảnh: Phạm Duy Tuấn
Kathy đẩy chiếc giường sát vào lồng hổ chúa, khẽ nằm nghiêng xuống. Con rắn giả trên người Kathy bắt đầu dỡ từng khúc cuồn cuộn trượt quanh theo những vệt gân gồng dọc trên thân thể. Con rắn thật trong lồng không cưỡng lại nổi sự khiêu khích mê hoặc bên ngoài, nó bắt đầu chuyển động, dần dỡ từng khúc đen mềm mại trườn tới, từ cái cổ nhăn nheo vảy thổi ra những tiếng phù phù rùng rợn, cái đầu in những đường vân ánh màu xanh lục nối lại thành hình thoi méo mó khi chúi xuống, lúc lại vươn lên hòa theo vũ khúc giả tạo bên ngoài lồng sắt. Con rắn trong lồng từ từ thè chiếc lưỡi đổi màu khi xanh khi đen qua mắt cáo nhỏ xíu trượt theo lớp vảy nhẵn bóng bên ngoài, cặp thân rắn oằn lên, dập xuống khúc khúc trập trùng vô tận…
 
Đó là trước kia. Còn bây giờ Kathy đã cho phép con trai xuống hầm âm.
 
Katay được Pa giao việc chăm đàn hổ chúa mới nhập, tám chúa con chung một chiếc lồng, chúa lớn ở lồng riêng, mồi của chúng là bất kể một loại rắn nào có nọc độc. Đối với những con chúa nhỏ, Katay đếm tám con rắn độc, mở cửa lồng dốc oạp vào rồi quay lưng bỏ đi sau khi đã vặn chốt cửa lại, trong đầu văng vẳng lời kể của bà ngoại.
 
Rằng thuở xa xưa Giàng là đấng tối cao, vị thánh thần cai trị muôn loài. Một hôm Giàng cho gọi tất cả các muông thú đến và lắng nghe nguyện vọng của từng loài. Muông thú đều đã tương đối hoàn hảo, riêng chỉ có rắn than thở rằng loài mình không có chân, tuy các đốt xương được tạo cho việc di chuyển nhưng cũng khó có thể tự vệ và săn được mồi. Giàng thấy vậy đã ban cho loài rắn thứ vũ khí đặc biệt để bảo vệ sự sinh tồn, là nọc độc. Rắn chúa đem nọc độc Giàng ban về chia cho các loại trong họ và dặn phải phơi thật kỹ. Riêng cạp nia đến muộn nên nọc độc còn lại đã bị rắn chúa nuốt chửng. Cạp nia căm phẫn trườn khắp các nơi, đến bờ suối gặp rắn ráo đang phơi nọc trên một phiến đá, cạp nia liền nuốt chửng. Thế nên loài ráo cho đến nay vẫn không có nọc, còn cạp nia và xà chúa vì không phơi kỹ nên nọc của chúng trở thành kịch độc…
 
Mỗi lần thả một con cạp nia vào lồng làm mồi cho rắn chúa, Katay thường hé mắt quan sát. Cạp nia vừa chạm xuống đáy lồng, tưởng được phóng sinh, nó thu mình tròn vo lại rồi khoan khoái dỡ từng khúc, từng khúc ra. Tiếng “phù... ù... ù” khiến nó giương cặp mắt bé xíu như hạt nho đảo láo liên kiếm tìm. Vừa nhìn thấy một khối đen mốc cuộn bên trong góc lồng đang dần chuyển động, nó vội vã phóng đi. Nhưng những mắt lưới xung quanh lồng đã ngăn nó lại. Và… hấp! Một tiếng đớp nhanh chớp giật và cú ngoằng thân của rắn chúa đã thít chặt lấy nó…
 
*
*     *
 
Pa lấy vợ mới, cô gái tên Loan mà trước kia mẹ thường hay nhắc đến giờ trở thành mẹ kế của Katay. Khách đến dự đám cưới vừa đủ một mâm, bạn làm ăn của Pa ngày một ít đi.
 
Katay không gọi Loan bằng mẹ hay dì mà là chị, hai người rất ít khi trò chuyện, ngay cả lúc cùng nhau lên núi hái chuối rừng về cho nhím ăn. Chị Loan là người Tày, tiếng Tày và Nùng trong vùng này phát âm giống nhau chỉ khác về giọng điệu nặng hay nhẹ, hai người đều biết thế, nhưng không một ai hé răng nói nửa tiếng với nhau.
 
Chiều có sáu đứa trẻ con trong bản ăn mặc rách rưới, kéo nhau đến dưới gầm sàn nhà lớn, chúng mang theo một con cầy hôi bị sập bẫy và gọi Katay xuống, thằng lớn nhất trong đám giương mắt hỏi cậu:
 
- Có mua con này không?
 
- Pa tau không mua cầy bạc má đâu!
 
- Bọn tau bán ít tiền mà! - Thằng bé năn nỉ.
 
- Ít tiền cũng không mua, chúng mày không mang đi thể nào cũng bị Pa tau chửi.
 
Thấy Katay với đám trẻ con xôn xao dưới gầm sàn, Sùng Kathy ló đầu qua song cửa sổ trên nhà lớn nhìn xuống.
 
- Có mua cầy bạc má không? - Vẫn thằng lớn nhất trong đám trẻ ngước lên nhìn Sùng Kathy.
 
- Năm nghìn, không bán thì đi chỗ khác!
 
- Mười nghìn mới bán! - Thằng bé nheo mắt mặc cả.

 
 Ảnh: Phạm Duy Tuấn
Sùng Kathy mặc áo và đi xuống theo lối cầu thang.
 
- Mày bán năm nghìn cho tao, tao bán lại cho người ta có mười nghìn.
 
- Ăn lãi nhiều thế?
 
- Có bán không?
 
- Mười nghìn!
 
- Ừ thì mười nghìn, tao đi bán hộ chúng mày vậy, đưa đây.
 
Thằng bé giúp Sùng Kathy đổ con cầy hôi vào một chiếc lồng gỗ, con cầy bị gãy một chân do trúng bẫy, nó nhảy lòng vòng rồi nằm nép vào góc lồng, mùi tanh tưởi phát ra nồng nặc. Sùng Kathy rút trong túi tờ mười nghìn đưa cho thằng bé. Nhưng nó vẫn đứng chờ hồi lâu. Tới khi Sùng Kathy đã quay đi nó mới ngỡ ngàng hỏi trống không:
 
- Có một tờ à?
 
- Thế mày đòi mấy tờ?
 
- Con cầy này của sáu anh em tau bắt được, sao mày chỉ trả tiền cho tau?
 
- Cái gì nữa?
 
- Tau nói là con cầy này mày phải trả mỗi đứa chúng tau mười nghìn.
 
- Mày bị điên à? Một con cầy hôi, bắt tao trả mỗi đứa chúng mày mười nghìn?
 
- Ờ, mày đồng ý rồi chúng tau mới bán mà.
 
- Mang của chúng mày đi, trả tiền lại cho tao! - Sùng Kathy tức giận mở nắp chiếc lồng và dốc ngược xuống.
 
Con cầy hôi được đà phóng ra ngoài, mặc dù một chân trước của nó còn đang rướm máu. Sùng Kathy nhổ toẹt một bãi nước bọt và nện bước chân nặng trịch lên cầu thang.
 
Đám trẻ con lao nhao đuổi theo con cầy hôi, thấy vậy Loan và Katay không ai bảo ai cũng xúm xuýt vào đuổi bắt giúp lũ trẻ, khoảng cách giữa hai người được hóa giải từ đó.
 
Đám trẻ con đã bắt được con cầy hôi và dùng dây trói chân nó lại. Thằng lớn tay vẫn cầm tờ mười nghìn, nó mếu máo nhìn lên sàn nhà:
 
- Mày trả tau có một tờ này sao đủ tiền cho Pa tau uống thuốc?
 
- Thằng kia sao mày lắm điều thế? - Sùng Kathy nãy nện bước chân thình thình xuống cầu thang, rút thêm tờ năm mươi nghìn đưa cho Katay:
 
- Đưa tiền cho nó, bảo chúng nó đem ngay con Bạc má về làm thịt, không tao nện cho bây giờ!
 
Đám trẻ con sung sướng, nháo nhác khiêng con vật bị trói ra khỏi cổng nhà Sùng Kathy.
 
*
*     *
 
Sùng Kathy không cho phép Loan bước chân xuống tầng âm, ông thường căn dặn Katay điều đó trước những lúc đi vắng. Nhưng Katay phá lệ một lần vì chị Loan nói rất thích xem nhím phóng tên. Katay tháo chốt cửa xuống tầng hầm, mùi mốc và hôi thối bốc lên nồng nặc, ánh điện sáng lờ mờ nhưng vẫn nhìn rõ các lồng sắt và lồng gỗ to nhỏ xếp xen kẽ hoặc chồng lên nhau. Chưa bao giờ Loan thấy nhiều thú rừng và rắn đến chừng ấy. Cô bàng hoàng, bải hoải, bước chân xiêu vẹo bám sát phía sau Katay.

 
Ảnh: Phạm Duy Tuấn 
Loan vốn là một cô gái Tày, sớm xa rời bản xuống thị trấn kiếm sống. Sùng Kathy thường hay lui tới quán cafe cô bán thuê. Cô biết ông qua lời của những người xung quanh rằng tiền buôn thú sống mà Sùng Kathy lời mỗi tháng bằng những người bình thường tích cóp cả một đời. Loan mơ hồ tin điều đó, nhưng chỉ vài tháng sau cô có câu trả lời ngay, Sùng Kathy bỏ tiền mua lại cả quán cafe giữa thị trấn tặng riêng cho cô khi tình yêu giữa hai người đã bén hơn lưỡi dao mới mài vào đá. 
 
Một thời gian sau đó Loan được tin vợ của Sùng Kathy đột ngột qua đời, ông ta nói phải để tang vợ ba năm mới rước cô về chung sống. Loan như con chim bị mắc lưới, sa chân vào thì dễ nhưng thoát ra lại khó, bởi không ai trên vùng đất này không biết việc dây vào Sùng Kathy như chui vào cũi hổ.
 
Katay đưa Loan đến thăm lồng chúa lớn. Thấy động và hơi người lạ, chúa lớn lao ra mổ tới tấp vào thành lồng sắt, Loan hoảng hốt lùi lại, quay đầu chạy như bay ra khỏi cửa tầng âm.
 
Tỉnh dậy lúc mặt trời đã xuống đầu đỉnh núi, cổ họng khát cháy, Loan đi vào gian nhà lớn tìm nước uống. Katay đang chăm chú theo dõi đoạn băng nghi hình đám cưới của cha mẹ mười bốn năm trước, căng thẳng hồi hộp đến mức không biết Loan đã đứng ở phía sau. Lần này cũng vậy, cậu bé chỉ xem được hết câu nói của người đàn ông bị trúng độc rắn. “Giàng ôi...! Sùng Kathy mày... thật là... độc ác!”.
 
Katay lại bị kích thích quá mức. Màn hình tối lại.
 
Loan bịt chặt miệng cố để không thốt lên vì kinh hãi. Chỉ một lát sau tiếng khò khè, ú ớ của mộng trẻ thơ đã bao trùm cả ngôi nhà lớn. Loan rón rén tới nhấc điều khiển video ra khỏi bàn tay nhỏ, vội vã lấy chiếc đĩa ra khỏi đầu máy, khuôn mặt đầy hoan hỷ.
 
Hạt kiểm lâm, công an tỉnh cử cán bộ về điều tra đường dây buôn thú sống vượt biên. Việc giao hàng bị trệ lại, những ngày đó Sùng Kathy rỗi rãi cùng con trai ngồi xem tivi, hai cha con không ai nhắc đến việc Loan bỏ đi khỏi nhà đã gần một tuần. Có một chuyện Katay ngập ngừng hàng giờ mới dám mở miệng hỏi Kathy:
 
- Pa lấy đĩa của con à?
 
- Cái đĩa ấy à, mẹ đã bảo con đừng xem sao còn xem?
 
- Pa biết à?
 
- Có ngày nào là con không xem đâu?
 
- Nhưng sao Pa lấy của con?
 
- Không phải Pa lấy.
 
- Vậy thì là chị Loan rồi.
 
Sùng Kathy lấy trong túi ra một chiếc đĩa khác và cho vào đầu video, vẫn những hình ảnh quen như xảy ra hàng ngày ngay trước mặt Katay. Cậu ôm chặt lấy cánh tay của Pa khi người đàn ông trúng độc kêu lên “Sùng Kathy mày... thật là... độc ác!”. Katay nhắm mắt lại chờ đợi sự im lặng của màn hình video. Nhưng bên tai cậu lại văng vẳng tiếng của trùm rắn: “Bác thua rồi!”
 
Katay ngẩng đầu lên, lấy hết can đảm mở mắt nhìn vào màn hình. Sùng Kathy đỡ ông Ngò nằm xuống giường, đích thân bưng bát nước có màu xanh ngắt bón từng thìa vào miệng cho người thua cuộc.
 
Màn hình chuyển một cảnh khác, Sùng Kathy, ông Ngò và một số dân buôn khác ngồi trò chuyện vui vẻ bên ba món nhắm được chế biến từ rắn. Sùng Kathy giở một gói bột màu đen ra đổ vào món xương rắn chiên giòn. “Đây là bồ hóng, đặc biệt kỵ với các loại rắn độc, nhất là thuộc loại hổ như chúa, phì, hay bành. Chỉ một ít bồ hóng bay vào thịt các loại rắn này sẽ thành độc, thế nên khi chế biến rắn người ta không chế biến trong nhà bếp có bồ hóng mà thường mang ra ngoài trời. Còn đây là món canh rắn nấu lẫn với ngọn dương xỉ nếp, một loại thuốc giải độc rắn. Nếu bác Ngò uống trước canh này thì sẽ không bị phát độc. Bác phải thua Sùng Kathy lần này rồi!”. “Tao thật khâm phục mày!”.
 
*
*     *
 
Katay cũng khâm phục Pa lắm, cuối cùng điều cậu lo lắng bấy nay không phải là sự thật, Pa không phải là người ác độc.
 
Cán bộ điều tra vẫn âm thầm đóng ở trong bản. Tiền của ngày một hao hụt đi. Kathy xót xa nhìn đám hổ chúa nhỏ, có thể nhìn được từng mắt xương của chúng trồi lên qua lớp da, vì tất cả số mồi ít ỏi đều giành cho chúa lớn, dần dần mồi của chúa lớn cũng chẳng kiếm nổi, thế nên Kathy đành để chúa lớn xơi dần đàn con của nó. 

 
 Ảnh: Phạm Duy Tuấn
Quay mặt, tay túm cổ con rắn xấu số cuối cùng, Sùng Kathy chua xót gỡ miệng lồng, tuồn vào con chúa nhỏ là một con chúa cái sắp đến ngày lột xác, không biết là lần lột xác thứ mấy của nó? Đến khi chiếc đuôi nhọn hoắt tuột khỏi tay xuống hẳn phía dưới, Kathy nhắm mắt nghĩ: Loài rắn chuyên tích nọc bằng nọc độc của đồng loại! Kathy sập cửa lồng, một hạt nước mắt rớt xuống.
 
Kathy không muốn ra ngoài mà nằm lì trên chiếc giường bên cạnh lồng rắn, đặt tay lên trán. Độ này ông hay nghĩ đến những chuyện đã xảy ra trong quá khứ. Sùng Kathy mồ côi Pa từ khi còn nhỏ, ngày ngày đeo gùi theo mẹ lên rẫy. “Mẹ ơi! Pa là ai?” “Mày hỏi nhiều thế?” “Con muốn biết mà, Pa là ai? Con muốn biết mặt Pa!”  “Là... là con rắn Hổ chúa, mày đi mà tìm.”
 
Chiều ấy, Kathy lôi về một con hổ chúa toàn thân dài ngoẵng, vảy đen trũi lại ánh màu xanh mốc. “Mẹ, Pa đây phải không?” Mẹ hoảng hốt lao tới: “Tránh ra Kathy! Thả nó ra đi Kathy!”
 
Mẹ theo phản xạ lao vào toan giật con rắn hất ra xa. Nhưng, đầu con rắn đã vồng lên. Bàn tay mẹ thoáng chốc đã đầy máu.
 
- Xà chúa! Cứ coi như mày là Pa của tao đi! Pa đã cướp mẹ đi rồi, chỉ có gần Pa tao mới được nhìn thấy bà ấy.
 
Kathy thủ thỉ với chúa lớn, dụi mắt nhìn vào chiếc lồng, bỗng rùng mình sởn gai ốc, cảnh không có trong tưởng tượng của ông đang diễn ra trước mắt. Hai con hổ chúa một lớn, một nhỏ bắt chéo nhau nghềnh ngàng giữa chiếc lồng sắt, chúng trườn qua nhau, quấn lấy, chà xát nhau, dỡ ra, cuộn vào khúc khúc dập dìu như điệu múa của đôi nam nữ Nùng đêm hội thung lũng Giàng Ô. Sùng Kathy lầm bầm:
 
- Khốn nạn!
 
Tâm trí ông rối lên cảnh loạn luân của loài mang trong mình kịch độc, một con rắn cái dâm đãng ngu ngốc, một cha rắn đực phong tình độc ác, chiếm đoạt xong là nuốt chửng bạn tình vào bụng.
 
Chúa lớn há miệng kẹp sát miếng vẩy khô trên mép chúa nhỏ, lớp giáp xác trên thân chúa nhỏ phồng rộp lên dần chuyển sang màu trắng.
 
- À... không! Không phải rắn lẹo, mà là lột xác, là lột xác! - Sùng Kathy rên rỉ.
 
- Pa ơi! Pa đâu rồi? Người ta đã biết chỗ này rồi. - Katay mặt cắt không ra máu chạy vào.
 
Kathy đưa tay khẽ ra hiệu cho con im lặng.
 
- Các chú ấy đang phá cửa tầng âm.
 
- Mang lồng hổ chúa này vào rừng theo đường sau tầng âm cho Pa.
 
- Còn Pa?
 
- Sẽ đi theo ngay thôi! Nhanh lên!
 
Mưa như trút nước, những tia sét ngoằn ngoèo xé ngang dọc trên trời, dưới đất. Katay ì ạch bò ra kéo theo lồng rắn khỏi cửa sau tầng âm, tháo chốt, dốc nghiêng chiếc lồng. Một vệt đen từ từ trườn ra ngoài nhưng vẫn còn mắc lại nửa khúc bên trong, Katay thò tay vào nắm lấy chặt thân rắn kéo ra giúp, bỗng như một nhát dao thái phạt ngang, cậu bé rụt tay lại thì đã không kịp nữa.
 
Kathy lao đến bên con trong ánh chớp trời, chộp bàn tay đầm đìa máu lẫn nước mưa của Katay lên miệng mút nọc độc lẫn máu nhả ra ngoài. Chớp loang loáng, đầu óc quay cuồng, hai cha con Sùng Kathy hoan hỷ nhìn đôi xà chúa thân bện vào nhau trườn đi trong màn mưa, bỏ lại lớp ráp xác trắng lóa vắt ngang cửa lồng sắt. 
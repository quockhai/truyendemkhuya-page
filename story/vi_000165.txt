4 ngày đêm lăn lóc ở miền cao nguyên đá Hà Giang, câu cửa miệng của TS Khảo cổ học Nguyễn Thị Hậu (bay từ Sài Gòn ra, tham gia chuyến đi) lúc nào cũng là “Ối Giời ơi! Đẹp quá!”, khiến tụi mình cũng phì cười liên tục: “Không còn câu gì khác hay sao mà cứ gọi Giời suốt thế?”.

Nói thế thôi, chứ Hà Giang mùa này tuyệt đẹp. Cái đẹp mùa Xuân muộn, giống như cô gái quê đã chín tuổi, quá lứa nhỡ thì, nay có tý tình yêu hơi hướng đàn ông, nở phây phây ảnh đẹp ở từng chân thon, lưng mềm, má đỏ, môi chín, mắt lóng lánh và hơi thở nóng hôi hổi, rừng rực.

 
   Ảnh: Thanh Hà

Thật thế đấy! Cái vùng biên cương xa tít tắp này, khổ đến cùng cực từ bao nhiêu năm: Nhìn lên nhìn xuống 4 xung quanh toàn là đá xám xịt kê răng nhọn hoắt, khô không khốc chẳng có lấy một giọt nước; cây cối loe nhoe khô xác tranh giành với người từng nắm đất trong gốc đá, để gồng lên những rễ để sống; trâu bò vơ váo nhẫn nại gõ mõ lốc cốc, hì hụi len vào từng hõm núi, lên vách đá cao tìm từng mẩu lá xanh, cỏ tươi; con người sống cuộc sống gầy mòn bên những nương ngô khô xác, nồi mèn mén “thâm canh cố đế” truyền từ đời này qua đời khác, mái nhà mỏng mảnh úp ngói bé tẹo bằng bàn tay xơ xác, dựng trên vách nhà trình tường dày khự như bức tường thành, để nương náu tấm thân rách áo, tướp quần, không giầy không tất vào trong tránh hơi lạnh từ núi đá, gió lùa buốt giá từ bên kia cột mốc biên cương...

Thật thế đấy! Xác xơ, khổ ải thế, vậy mà chỉ tý gió Xuân muộn về, cả vùng đất bảo nhau bừng dậy, trong búp non bặm môi kề trên vách đá, trong lá xanh nheo mắt trên cành rễ khô hanh, trong chúm chím hoa đào tràn màu đỏ hồng từ trên đỉnh núi, mái nhà xuống mơn man triền dốc, trong trắng xóa hoa mận khơi gợi ước mơ trong trẻo, ngọt lành và hoa cải...

Hoa cải đồng loạt nhắc nhau, rối rít nở hoa vàng từ những thân khẳng khiu đâm ra từ nách lá gầy hao.

Hoa cải ươm vàng những mảnh vườn nhỏ hẹp trước mái nhà xiêu vẹo, làm sáng bừng đôi má trẻ con và vơi bớt những lo âu cơm áo hàng ngày của người lớn, sống cỗi cằn trên núi đá.

Trên mảnh đất biên cương tiền đồn đá ngút ngát này, bao đời nay người Mông chỉ biết  lấy cây ngô và cây cải làm nguồn lương thực chính để tồn tại. Người Kinh lên thăm người Mông, chán với cơm thịt cơm cá nhiều đạm dễ gút, nên rụt rè thử món ăn của người Mông và thấy là lạ, hay hay, những món ăn chế biến từ rau cải, nên hí hửng gọi đó là cải mèo, mua về xuôi làm đặc sản.

 
  Ảnh: Thanh Hà
Cây cải Hà Giang được người Mông cẩn thận trồng ven các nương ngô, lúa. Cải được rắc hạt vào các khoảnh đất hẹp cạnh bờ ruộng, ven nương, và từ đó cây mọc lên một cách tự nhiên không cần chăm sóc.

Lá cải mèo có nếp nhăn quanh rìa cùng với ngồng cải, khi chế biến món ăn cho hương vị ngon, giòn, đậm đà và hơi ngăm ngăm đắng. Đặc biệt, khi chế biến cải mèo, không cần bột ngọt hay xương hầm vì món ăn có vị ngọt tự nhiên.

Lên núi đá Hà Giang, khách du lịch người Kinh thường chen nhau gọi món cải mèo luộc, xào, canh hoặc sang hơn, gọi ngồng cải xào thịt hun khói, xào thịt bò... Mùa này, hoa cải vàng rực khắp cao nguyên đá và cũng được người Mông thu hái, cắt thành từng bó rực vàng, mang về chợ huyện, đưa ra vệ đường bán cho người Kinh mang về xuôi.

Màu vàng là màu của hy vọng. Hoa cải vàng giống như những niềm hy vọng giản đơn, bình dị về bát cơm no, manh áo ấm đơn giản mỗi ngày. Nên với đồng bào Hà Giang bây giờ luôn hy vọng mùa hoa cải mỗi năm kéo dài thêm, để họ được nhìn thấy khách lạ lên núi chụp hình trong vườn của nhà mình, cho đỡ cô đơn hiu quạnh; để họ có dịp cắt hái những chùm hoa cải, mang bán lấy tiền mua gạo ăn thay mèn mén, mua muối thay tro  cỏ gianh...

Mình cũng vậy: Chỉ ước hoa cải vàng lâu những lũng núi, để nhiều người Kinh lên thăm người Mông và người Mông bán được nhiều rau cải cho người Kinh.

Lên với Hà Giang xa xôi, lên với đồng bào gian khó, để thấy đất nước mình hùng vĩ nhưng gian lao, phải giữ gìn bảo vệ; đồng bào mình gian khổ nhưng kiên cường bám trụ tiền đồn quê hương. Càng đi càng thấy mọi sự sẻ chia với những con người chung chữ “đồng bào” thật sự ý nghĩa, khi mà chúng ta được quá nhiều, ở nơi sáng điện ấm chăn...

Mùa này hoa cỏ đang đượm màu, ai đi Hà Giang thì nhanh nhanh lên nhé!
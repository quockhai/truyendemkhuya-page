Trọng cười hì hì bảo:

- Hôm nọ tao suýt đâm xuống sông.

- Làm sao mà xuống sông?

Hắn tưng tửng:

- Lấy nhau ba mấy năm giời, có cháu nội cháu ngoại mà “vị” ấy cứ mình mình tôi tôi. Hôm vừa rồi đang ngồi sau xe tự dưng “vị” ấy gọi “anh ơi”. Mình cảm động suýt lao xuống sông…

Vợ hắn lườm:

- Điêu! Chỉ được cái giỏi nói điêu…

Hắn hì hì:

- Khiếp! Người gì mà đanh đá. Ông thấy chưa. Điêu vậy mà bám như đỉa “giằng không ra, dứt cũng không ra…”

Vợ hắn lại nguýt phát nữa, mủm mỉm cười rồi dẫn đứa cháu nội vào nhà trong. Nhìn hai vợ chồng nhà hắn thấy vui vui…

Là đồng đội cùng đơn vị, đánh nhau trong chiến trường từ năm 1972 tôi chẳng lạ gì tính nết của Trọng. Bây giờ đã về hưu nhưng bản tính hắn vẫn bật bưỡng, luôn mồm tán tếu. Nhìn cái đầu đã bạc nhưng cặp mắt vẫn ánh lên tia nghịch ngợm, cái mồm tươi roi rói. Tháng trước hắn nhận quyết định nghỉ hưu, liền gọi điện bảo sang uống rượu mừng… vì hạ cánh an toàn.

Tôi trêu:

- Đời ông sướng như giời, toàn thấy lên… có phải chở cái gì mà chả hạ cánh an toàn.

Hắn cãi:

- Sao lại không chở gì. Này nhá: Đánh nhau triền miên đến tận năm tám mươi, đời lính ông chẳng lạ! Con trai đại úy, con gái giáo viên cấp ba. Bản thân đi nhiều mà không con rơi con vãi để phải kỷ luật. Phải kìm mình kìm mẩy mấy chục năm… khổ chứ sướng gì.

Tôi đang định phản ứng thì đã thấy Trọng trầm ngâm:

- Nói cho vui vậy chứ để có hôm nay, có gia đình như thế này là nhờ bà xã. Mà như thể có duyên có số thật đấy ông ạ. Tưởng dớ dẩn lại hóa bền. Vậy mà ngày đầu mình vẫn nghi ngờ “vị” ấy.

Chuyện hắn có vợ thì ly kỳ lắm. Giải phóng xong, một hôm hắn thì thầm với mình: tao đã có con. Không có vợ mà lại có con? Rõ dớ dẩn! Chắc là hắn nói cho vui. Nghe xong thì bỏ qua, quên luôn. Nhớ làm gì chuyện cái chuyện tào lao. Chỉ biết ngày ấy trước chiến dịch 75 thấy Trọng bần thần như người mất hồn. Không nói không cười, lầm lì như cái bóng. Mà hắn là thằng cha tếu táo nhất đại đội.

Hắn kể:

Ông còn nhớ hồi đầu bảy nhăm. Lần ấy cánh mình về “đường dây” lấy gạo chuẩn bị cho chiến dịch. Mình gặp thằng cùng xóm đang hành quân vào phía trong. Mừng quá. Hỏi thăm biết bố mẹ vẫn khỏe. Yên tâm. Chưa hết, nó bảo mình: “Thằng cu nhà anh khỏe lắm, giống anh như hệt.”

Mình ngớ người: “Thằng cu nào?” thằng này tròn mắt: “Thằng con anh chứ thằng cu nào”. Rồi thằng ấy phải hành quân tiếp, chỉ nói với lại: “Chị ấy ở cửa hàng thực phẩm huyện”. Chả hỏi được thêm câu nào.

 “Thằng cu nhà anh khỏe… chị ấy ở cửa hàng thực phẩm…” Những cái thông tin ngoài sức tưởng tượng. Sao lại có chuyện ấy được. Chả nhẽ… Thôi chết rồi. Có yêu đương tìm hiểu gì đâu, chỉ mỗi đêm… Chả nhẽ vậy mà thành ra thằng con! Tí tì tị vậy mà có con! Ơ hay nhỉ.

Rồi bị cuốn vào chiến dịch. Thì ông còn lạ gì nữa, đánh nhau tối ngày, căng thẳng đầu óc giữa cái sống cái chết rập rình. Cố quên cái việc có con nhưng không thể được. Những lúc thằng OV10 vè vè trên đầu léo nhéo bài ca chiêu hồi, tiếng ru con, tiếng trẻ con khóc như xé lòng… lại nghĩ: mình có con.

Mà cũng thật dớ dẩn ông ạ… Cái đợt về phép đi chuẩn bị đi B cuối năm bảy hai đấy. Chỉ được nghỉ ba ngày cả đi về nên thằng nào thằng nấy cắm cổ vẫy xe tải về tỉnh Nam. Cắm cổ cuốc bộ vào bến xe sơ tán. Vào đến bến thì trời đã tối. Xe về huyện phải sáng mai mới có. Thì còn biết đi đâu! Mình tìm chỗ khuất gió góc tường trải áo mưa ngồi vạ vật ở cái nhà chờ hôi hám và khai nồng mùi nước đái. Chắc phải qua đêm tình nguyện “hiến máu nhân đạo” cho lũ muỗi.

Đang ngồi ngáp vặt thì có một cô gái xách cái túi du lịch vào nhà chờ. Cũng chả để ý. Mình mới hơn mười tám tuổi một tý, thấy con gái thì cũng thích thích nhưng lại sợ, huống chi đây là người không quen… Ông còn lạ gì cái tuổi ấy. Cô ta đến trước mặt mình nói bâng quơ: “Lại phải sáng mai mới có xe về Hồng Hải”. Kệ, nói với ai chứ chả nói với mình. Rồi lại thấy hỏi: “Anh cũng chờ xe a! Anh về đâu?”. Mình nghĩ: về đâu mặc kệ người ta. Rõ dớ dẩn. Mình làm như không nghe thấy lơ đãng ngó đi chỗ khác. Cô gái cúi sát mặt mình, trong ánh sáng nhập nhoạng của lúc cuối ngày mình nhận ra khuôn mặt quen quen. Và cô gái chợt reo lên: “Trọng! Trọng có phải không? Về phép à!”

Đến lúc ấy thì mình cũng nhận ra. Đó là Thủy, học trước mình một lớp ở cấp Ba trường huyện. Học trước nên hơn tuổi, hơn mình hai tuổi. Mình quen Thủy vì hai lớp chỉ cách nhau tấm vách trát bùn rơm. Vào lớp 8 thì Thủy lên lớp 9. Mình lên lớp 10 thì Thủy đã tốt nghiệp ra trường. Mình vẫn sang bên lớp Thủy để chơi vào những giờ giải lao. Gọi mọi người trong lớp bên ấy là anh chị và xưng em ngọt xớt.

- Chị Thủy! Vâng… em về phép để chuẩn bị đi B. Thế chị đi đâu… mà tối thế này còn ở đây?

Thủy bảo:

- Mình đang học Trung cấp Thương nghiệp ở Hà Nội. Nhà trường cho nghỉ mấy ngày, tranh thủ về giúp bố mẹ. Đi xe khách về đến đây thì tối. Lại phải chờ đến sáng mai. Chán ghê cơ. Cho ngồi với!

Mình dịch chỗ ra cho Thủy ngồi. Tự nhiên có bạn nói chuyện để cho qua đêm. Cũng hay. Dù sao Thủy cũng là đàn chị nên cũng không phải ý tứ gì mấy. Nhưng cẩn thận thì mình cũng ngồi hơi xa, giữa hai đứa để cách cái ba lô, cái túi du lịch đặt lên trên.

Thủy móc trong túi ra cái bánh mỳ bẻ đưa mình một nửa: “A n đi! Cho đỡ đói”. Mình trả miếng bằng cách lấy bi đông nước đưa cho Thủy: “Chị uống đi! Cho đỡ khát” Cũng xong bữa tối. Cả hai đứa ngồi dựa tường nói về những kỷ niệm hồi học cấp Ba. Thủy hỏi mình về đời sống chiến sĩ. Mình mở máy nói về cái vất vả luyện tập cũng có cường điệu thêm tí… hì hì… Thủy luôn mồm “Thế à! Thế à! Khổ nhỉ.” Thủy hỏi: “Thế Trọng về lần này có bảo bố mẹ hỏi vợ cho không?” Mình bảo: “Vợ con gì, em còn trẻ con í mà… chưa biết yêu thì lấy vợ thế nào. Chả nhẽ cha mẹ đặt đâu thì con ngồi đấy.” Thủy cũng đồng tình: “Ừ, đàn ông phải hai nhăm ba mươi mới đủ chín chắn, lúc ấy hãy lấy vợ.”

Đêm lạnh. May cũng ít muỗi. Nhưng tối om vì bến xe sơ tán nên chẳng có đèn điện. Thủy lấy cái nilon ra bảo mình: “Trọng ngồi sát vào đây, che áo mưa cho ấm”. Đầu tiên cũng ngại ngại. Nhưng lại nghĩ rét bỏ mẹ, cũng chả cần kiêng cữ. Dù sao Thủy cũng vẫn là đàn chị. Vậy là mình nhấc cái túi và cái ba lô bỏ sang một bên.

Hai đứa ngồi sát bên nhau.

Nói thật với ông, lần đầu tiên trong đời mình ngồi sát với một người con gái. Chuyện trò với nhau một thôi một hồi, rồi cũng chả còn gì để nói. Thủy thiu thiu ngủ. Cả bến xe chỉ có hai đứa. Gió thổi vù vù, lạnh… vậy là sát hơn tý nữa. Tự nhiên mình thấy người con gái này thật gần gũi. Và lòng bâng khuâng. Mình thấy tay Thủy để thõng ra ngoài cái áo mưa. Chắc là lạnh lắm. Mình vươn người qua người nàng và nhấc tay Thủy dém vào trong áo mưa. Lần đầu tiên cầm tay con gái. Cái cổ tay tròn, mềm mềm…  Thủy chợt thức dậy nhìn quanh. Mình hoảng quá, vội vàng rút tay lại.

Và Thủy lại ngủ tiếp. Tự dưng nàng tựa vào vai mình. Hương bồ kết thơm thơm, những sợi tóc mơn man bên má, mình cứ để như vậy, râm ran khắp người. Tự nhiên cái khoảng cách tuổi tác biến mất. Mình cầm tay nàng đan vào tay mình.

Có lẽ Thủy ngủ say nên không biết. Hít một hơi rõ dài, liều… mình ôm lấy nàng.


 Minh họa: Đào Quốc Huy
Ngay lập tức Thủy vùng dậy đẩy mình ngã bổ chẩng và giang thẳng cánh tặng mình một cái tát nảy đom đóm. Hai mắt nàng long lên trong đêm tối. Miệng rít lên gay gắt: “Khốn nạn! Làm cái trò gì đấy?”

Bao nhiêu nhuệ khí của thằng đàn ông lúc này tan tành. Mình vừa sợ vừa ngượng, tay phải chống đất, tay trái xoa má. Khiếp! Con gái mà tay khỏe thế. Má trái rát ràn rạt. Giá ban ngày thì chắc bộ điệu của mình buồn cười lắm. Vừa nhục vừa xấu hổ nên ngọng mồm không biết thanh minh thế nào, phải đến một lúc lâu mình lúng búng bật ra cái câu này: “Xin lỗi chị! Nhưng… tuần sau tôi lên đường rồi… chả biết sống chết thế nào…”

Trong khoảng tối nhờ nhờ, Thủy ngồi dựa tường thủ thế. Hình như Thủy còn tức lắm vì mình nghe tiếng thở hổn hển. Một lúc lại thấy tiếng thở dài rồi tiếng khóc… Mình càng sợ… tay chân như thừa ra: chết rồi! Mình đã xin lỗi rồi mà người ta vẫn còn giận. Giá ban ngày thì mình đã chạy biến để không bao giờ gặp lại.

Bây giờ thì phải tránh xa. Mình đứng dậy định tìm ra góc khác ngồi. Khi vừa quay người thì Thủy nhào tới. Nàng ôm choàng lấy mình, hôn tới tấp lên mặt lên má mình giọng đứt quãng: “Thương quá… Cường ơi”

Lần đầu tiên mình được biết đến cái hôn và lần đầu tiên cảm nhận được sự mềm mại của thân hình người con gái. Sững sờ… rồi cũng hoàn hồn, vậy là Thủy không còn giận mình nữa. Bản năng đàn ông trỗi dậy, mình cũng hôn lên khuôn mặt đẫm nước mắt của Thủy và khát khao khám phá. Thủy nồng nhiệt đón nhận. Hai đứa như tan chảy vào nhau…. Chúng mình cùng nhau … thức trắng đêm.

Sáng hôm sau hai đứa lên xe. Ngồi cạnh nhau nhưng Thủy không nói lời nào. nàng ngoảnh mặt không nhìn mặt mình. Mình lại thấy sợ sợ. Lúc xuống xe, Thủy đứng cúi mặt, chân di di đến nát đám cỏ. Đột ngột nàng nhìn thẳng vào mắt mình. Lúc ấy mới thấy trên gương mặt của Thủy hai mắt thâm quầng, trong ánh mắt ấy nhìn như chất chứa cả yêu thương lẫn trách móc.

Nàng bảo: “Trọng đi giữ gìn… cố gắng về…” rồi ngoắt người đi thẳng.

Mình đứng như trời trồng, chỉ biết nhìn theo.

Hết ba ngày phép mình về đơn vị. Rồi đi B.

Đánh nhau liên miên, lại thêm mấy trận sốt rét rừng, lính tráng thằng nào thằng ấy gầy như que củi. Thảng hoặc có nhớ đến Thủy nhưng không thể hình dung được khuôn mặt. Thời gian làm mờ đi tất cả. Lắm lúc chậc lưỡi: chuyện tình ngang đường ấy mà. Rồi ai nấy đi, nghĩ làm gì cho mệt, rồi cũng nguôi ngoai.

Đấy sự thể nó như vậy. Nếu có con thì chắc chỉ ở trường hợp ấy thôi. Đến khi giải phóng háo hức muốn viết thư về cho bố mẹ, để báo tin còn sống. Nhưng lại nghĩ: nhỡ ra cô ta biết địa chỉ của mình… lằng nhằng lắm. Vả lại có lúc nghĩ chả biết có phải con mình không. Người ta có thể dễ với mình, người ta cũng có thể dễ với người khác. Cô ấy lại xinh, thiếu gì kẻ dòm ngó… có khi chỉ là chuyện kẻ ăn ốc người đổ vỏ… Cái thằng đàn ông nó buồn cười vậy đấy ông nhỉ. Đắn đo chán chê, quyết định khi nào về phép, dành thời gian xác minh hẳn hoi rồi hẵng hay…

Nghĩ vậy thấy nhẹ lòng một chút nhưng vẫn băn khoăn.

Cuối năm bảy nhăm được về phép. Mình toòng teng ba lô lép, thất thểu phố huyện cả buổi trưa mong tìm được người quen để đi nhờ xe đạp về nhà. Chỉ nghĩ quãng đường hơn chục cây số mà ngại. Đến chiều cũng không gặp ai. Tự dưng nảy ra ý nghĩ hay là ghé vào cửa hàng thực phẩm xem thế nào. Chắc người ta chẳng nhận ra mình đâu.

 Mình vào cửa hàng, thấy vắng ngắt. Ngó sang dãy nhà cấp bốn chắc là khu tập thể thấy trên mảnh sân hẹp một thằng bé đang nhảy nhảy đuổi theo con chuồn chuồn. Cuối sân có người đàn bà đang gội đầu. Mái tóc dài rủ xuống chậu nước che kín khuôn mặt.

Thằng bé chợt dừng lại khi thấy mình. Nó nhìn mình ngỡ ngàng rồi khoanh tay: cháu chào chú bộ đội ạ.

Mình sững người khi nhìn thằng bé! Có cái gì rất thân thiết... đến khó tả. Và còn một đặc điểm nữa… Ông có biết là gì không?

Tôi bảo: Chắc là cái đặc điểm hai lông mày giao nhau…

- Đúng đấy! Để mình kể tiếp

Người đàn bà ngẩng lên rẽ mái tóc còn ròng ròng nước. Lặng đi đến vài giây rồi hét lên: “Trọng!” và lao về phía mình, chân vấp phải cái chậu, nước đổ tung tóe. Khi đến cách mình khoảng vài bước chân thì người đàn bà đột ngột dừng lại… òa khóc!

Thủy đấy!

Chợt Thủy ngẩng đầu lên nhìn thẳng mình một lần nữa. Vẫn đứng nguyên chỗ cũ, nàng gọi:

- Cu Thắng! Bố về đây này.

Thằng bé từ nãy đến giờ đứng yên hết nhìn mình nhìn mẹ nó. Khi nghe mẹ nói vậy thì nó nhào tới: “Bố”. Mình cũng không cầm được nước mắt, ngồi thụp xuống ôm lấy con ghì nó vào lòng, hít hà mái tóc mềm, thơm vào hai má bầu bĩnh của nó. Con của mình đây. Con của mình đây. Thằng bé quàng qua cổ như không muốn buông ra.

Mình nhìn lên: Thuỷ nhìn hai bố con, miệng thì cười mà nước mắt giàn giụa, nước trên tóc ròng ròng đẫm áo.

Có lẽ Trọng đang sống lại cái thời khắc ấy. Thấy mắt hắn rơm rớm. Chợt tiếng chị vợ, thì ra chị dắt cháu nội ra đứng nghe từ nãy.

- Lại còn kể. Người đâu mà vô tâm. Chả có gì làm quà cho con…

Trọng hì hì: ai biết là con ai. Đầy người có lông mày như tớ…

Tay Thủy nắm hai tay đứa cháu, chị “kể tội” ông chồng bật bưỡng của mình:

Lão ấy cứ hì hì vậy. Vô tâm lắm anh ạ! Ai đời “cho” hết lão mà lão ấy chả được câu nào với tôi cho ra hồn. Lúc chia tay cũng không hứa hẹn gì. Bực lắm cơ.

Rồi giọng chị trầm hẳn xuống:

Tôi chờ mãi, đến ngày thứ ba tôi không chịu được nữa, đánh liều sang nhà lão. Gần đến nhà, hỏi một đứa trẻ con, nó bảo anh Trọng khoác ba lô đi sáng nay rồi. Không dám vào nhà, tôi chỉ biết đứng khóc. Nhiều người đi đường thấy vậy dừng lại hỏi vì sao mà khóc. Chả dám nói gì rồi lủi thủi về. Bụng nghĩ mình thương người ta, người ta cho là trò đùa. Đành vậy biết sao…

Nhưng vài tuần sau thấy người khang khác. Lúc ấy vì thương quá nên chẳng để ý gì, xong rồi lại thấy lo lo. Bây giờ nỗi lo mơ hồ ấy đã thành sự thực. Tôi cố giấu mọi người, nhưng nghén ghê quá tôi đành phải về nhà chứ nếu ở trường thì lộ việc. Qua mặt được mọi người ở trường chứ sao qua nổi mắt mẹ tôi, bà truy hỏi tận cùng. Tôi kiên quyết không nói. Mẹ nói lại với bố. Bố tôi giận lắm bảo mày bôi gio trát trấu vào gia đình này. Rồi mai nhà trường người ta đuổi học… vì chả nơi nào người ta chưa chấp loại con gái chửa hoang. Đi mà phá… Ngày ấy đi phá là phải phá chui. Thì còn biết tính làm sao.

Nghĩ đi thì vậy nhưng nghĩ lại. Ngộ nhỡ sau này Trọng về, ngộ nhỡ vài ngày nữa Trọng nhắn tin về… Thực lòng đấy là cái điều tôi hy vọng nhất. Và tôi quyết định! Tôi nói quyết định của mình với bố mẹ. Bố tôi miễn cưỡng đồng ý nhưng vẫn sa sả mắng mày làm nhục mặt tao.

Hai bố con đến nhà Trọng vào một buổi chiều. May cả hai bố mẹ Trọng đều ở nhà. Mọi người không biết bố con tôi đến có việc gì. Bố tôi dè dặt nói câu chuyện giữa hai chúng tôi. Rồi ông kết luận: “Bây giờ sự thể đã như vậy, tôi và cháu đến báo cáo để ông bà bên này xem định liệu thế nào.”

Hai bố mẹ Trọng ngớ người. Thì ra Trọng cũng không hề nói với bố mẹ…

Hai cụ bảo ông với cháu nói vậy thì biết vậy. Để chúng tôi tính! Bố con tôi thất vọng ra về. Chắc người ta nói vậy là người ta không chấp nhận. Dằn vặt  mấy đêm liền: không nhận thì thôi, con tôi tôi nuôi.

Nhưng mấy ngày hôm sau thấy bố mẹ Trọng sang nhà. Hai cụ nói với bố mẹ tôi xin được nhận con dâu. Khi được các cụ gọi ra báo cho cái quyết định này thì tôi òa khóc. Bố mẹ tôi mừng lắm.

Nói có vong linh các cụ, vợ chồng tôi được như hôm nay là nhờ tấm lòng các cụ. Khi làm đăng ký kết hôn cũng gặp khó khăn lắm vì Trọng không ở nhà. Người ta bảo là phải được sự đồng ý của cả hai bên mới được. Bố chồng tôi năn nỉ mãi rồi đến nổi khùng lên bảo: “Con tôi nó đang ở chiến trường, hay là các anh gọi nó ra để đăng ký. Tôi đang mong gặp nó đây”. Ông cán bộ xã bảo: “Tôi nể lắm, nhưng vẫn không thể cho đăng ký vì  phải theo luật, cậu Trọng chưa đầy mười chín tuổi”. Bố chồng tôi phải xuống nước: “Thì chúng nó sắp có con… đánh Mỹ lâu dài, cần người lắm chứ. Với lại nhà tôi với nhà anh còn có họ với nhau cơ đấy”. Rồi cũng xong, nhưng tôi phải viết giấy cam đoan… Sở dĩ cần đăng ký kết hôn để tôi khỏi bị đuổi học. Tôi sinh thằng Thắng, bố mẹ chồng tôi mừng đến phát khóc. Mẹ chồng tôi khi bế cháu thì vừa cười vừa khóc nói với bố chồng tôi: “Ông xem, in hệt thằng bố Trọng nó ngày xưa. Phúc đức quá…”

Đến tận hôm lão ấy về. Tôi vẫn ngài ngại. Sợ lão chỉ nhận con. Rồi lão còn hạch hỏi vậy sau khi sinh con nhỡ có đi lại với ai. Tức phát khóc! Tôi bảo không tin thì thôi. Con anh tôi trả lại anh đem về mà nuôi. Tôi ngu, tôi dại thì tôi chết. Hai cụ nghe thấy, tế cho lão một trận. Thực ra từ khi sinh thằng Thắng, bà nội cháu lên cơ quan ở với tôi để chăm sóc thằng bé. Mẹ chồng tôi còn bênh tôi, cụ bảo: “Đanh đá như con mẹ thằng Thắng thì đứa nào dám gì mà phải lo. Tao lại không biết à”.

Mẹ bảo tôi đanh đá cũng phải. Cũng có đứa thấy cảnh đàn bà vắng chồng định sàm sỡ, tôi cho biết mặt ngay. Vậy nên người ta bảo tôi đanh đá. Đanh đá mà giữ được con được chồng thì tôi cũng làm. Nhưng mà tức với lão này. Lúc ấy nghĩ vậy là tôi không phải ân hận gì nữa, lão ấy cũng chẳng thể trách tôi được điều gì.

-Vậy rồi giải quyết làm sao? Tôi tò mò hỏi.

Trọng lại hì hì cười:

- Thì giải quyết bằng… bằng việc có thêm con bé Lợi. Thả phanh có khi bây giờ “vị” ấy còn cho ra cả tá ấy chứ.

Thủy nắm tay đứa cháu nội dứ dứ vào mặt chồng:

- Chỉ được cái nước cùn. Ghét thế không biết! Ông nội nói hay nhỉ, cháu xem ông nội có hư không?

Thằng bé gật đầu: “Ông… nhội… khư.” 

Trọng hôm lấy cháu, cọ cái cằm đầy râu vào má nó khiến thằng bé cười như nắc nẻ. 

Lúc nâng ly, Trọng ghé sát mặt tôi bảo:

- Tớ biết dạo này ông phá ngang viết lách. Chuyện vợ chồng tớ biết đâu bỏ đó nhớ chưa! Đừng có đưa chuyện. Nếu “bà chị” mà biết là tớ bị thiến đấy nhá. Nhớ chưa?


Hải Hậu - tháng 12/2011
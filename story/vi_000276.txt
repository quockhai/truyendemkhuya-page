Thời chiến, đó là vùng “xôi đậu”. Ban ngày lính Cộng hòa đóng quân, ban đêm Việt cộng đến ở. Là vùng đất mới dân Quảng Ngãi và Bình Định từ năm bốn mươi li hương về lập nghiệp. Sống xen cư và cộng cư cùng người dân tộc Kơho, Cil, Mạ… Là miền đất cà phê từ thời Tây xen canh rẫy cà phê mới trồng tạo hình da beo lạ lẫm. Là cây đa hai trăm năm tuổi xanh om cao lớn các hồn ma lởn vởn ranh giới đêm và ngày, lũ kền kền to xác có đến hai, ba ngàn con về trú ngụ, ba ngày sau mới chịu bay đi hết. Thầy trụ trì chùa kể, giọng đều đều…
 
Nơi đó án ngữ Chùa Bửu Sơn. Thuộc xã Phú Sơn, huyện Lâm Hà. 
- Chùa nằm trên Hòn Ông, cao hơn nữa là Hòn Bà. Trước đó, ma và người chung sống. Sau bốn giờ chiều là thế giới của ma. Ma nữ, xinh đẹp, dạo một vòng quanh chùa, rồi men theo lối mòn đi xuống. Đến khúc quành sau đồi thì biến mất. Ít ai dám lên chùa vãng cảnh… - Giọng thầy đều đều, như đang tụng kinh…
Ngay chiều đoàn chúng tôi đi lên, chùa Bửu Sơn vẫn cứ nghe tịch liêu. Dù từ năm 1995, khi Thích Minh Châu về trụ trì, chùa được xây dựng lại đẹp và thoáng đãng hơn.
 

Ảnh: Viet Bang
Nhu cầu tôn giáo là chuyện muôn đời của nhân loại. Đông hay Tây, kim hay cổ. Con người cần chùa để làm nơi cúng kiếng, cầu siêu cho linh hồn người đã khuất về chốn bồng lai. Chùa cần bàn tay con người để xẻ gỗ, đá làm trang nghiêm cửa Phật. Cần hơn nữa bóng dáng con người để làm cho chùa bớt quạnh hiu, hoang vắng.
 
Thích Minh Châu người vui tính, dễ gần và có làm thơ. Nhưng khá khiêm cung. Chúng tôi bịn rịn từ giã thầy, rời khỏi chùa. Xe xuống núi, tôi như từ thế giới thần thoại về lại cõi thực tại, từ cảnh hoang sơ về chốn cõi trần. Thế nào đi nữa, ở khoảng giữa chênh vênh ấy, con người vẫn sống, làm việc, phụng sự, sáng tạo và tin tưởng. Thích Minh Châu vừa như tác nhân đồng thời như một nhân chứng.
 
Cùng tâm trạng đó, sáng hôm sau chúng tôi đi vào một khoảng chênh vênh khác: khoảng giữa cuộc sống và sự chối bỏ cuộc sống, cuộc đời và cuộc chiến đấu giành lại cuộc đời.
 
- Sara cảnh giác nhé, có đứa nổi hứng lên cào cái là tiêu! - Anh bạn văn nói nửa đùa nửa thật, trước khi xe lăn bánh. Dù tôi nghĩ không đến nỗi nào, nhưng câu nói mãi ám tôi. Xe đưa đoàn nhà văn chúng tôi gập ghềnh đi lên. Mươi người cả thảy. Đường vắng. Nhà lơ thơ. Bánh xe lăn nhanh trôi lại phía sau bao nhiêu là cảnh vật lạ cần chiêm ngưỡng. Tiếng chim, tiếng gà eo óc mất hút vào mênh mông rừng núi. Không gian thoáng đãng, mát dịu. Gần nửa tiếng sau, xe ngừng ngay cổng. Bảng hiệu: Trường Giáo dục Đào tạo và Giải quyết việc làm số 2, UBND Thành phố Hồ Chí Minh - Lực lượng Thanh niên Xung phong đang trước mắt. Chúng tôi đi thực tế trại cai nghiện, như dân địa phương gọi trường này. Tôi vẫn chưa hết bị ám bởi câu nói. Năm phút của thủ tục trôi qua, chúng tôi mới vào được Trường. Một, hai, ba bạn văn đi trước, tôi chậm bước lại sau. Khá khôi hài, thứ mặc cảm kì lạ ấy - tôi nghĩ.
 
Nhưng không. Một khu đất rộng đến chục mẫu nằm giữa khoảng bao la, tạo sự thanh bình. Anh cán bộ phụ trách trường tuổi còn rất trẻ tiếp đoàn trong căn phòng tiếp tân đơn sơ. Sự tự tin và hòa nhã của bạn trẻ đánh tan ngay bao giữ kẽ không cần thiết. Cuộc trà nước trôi nhanh, chúng tôi háo hức đến với trại viên.
 
- Tôi có thể gặp gỡ nói chuyện với một học viên tiên tiến được không, anh? - Thúy Ái hỏi.
- Được, được. Thoải mái mà - Anh cán bộ trả lời.
- Còn mình rất cần phỏng vấn nữ diễn viên quen thân ngày cũ… - Câu đùa lãng rơi vào khoảng rỗng. Bởi trường chỉ toàn nam là nam. Thỉnh thoáng có vài bóng hồng điểm xuyết, nhưng đó lại là nhân viên văn phòng hay cô giáo đứng lớp.
 
Khác với cảm giác khi bước chân vào Trại giam Kênh Năm ở Cần Thơ mùa mưa 2002, nơi chúng tôi chỉ giao lưu với cán bộ. Ở đây, nhà văn trực tiếp với đối tượng. Khoảng cách còn lại bị xóa bỏ ngay bước đầu tiên vào cổng trường, cái nhìn đầu tiên, tiếng chào hỏi đầu tiên. 
 
Có năm lớp cả thảy đang học văn hóa. Đông nhất hai mươi, có lớp chỉ dăm bảy người. Vài bạn văn dạo ngoài sân quan sát và ghi chép. Nhà thơ Trần Hữu Dũng đứng cửa lớp Tám nhìn vào, chăm chú với nỗi gì không biết. Phòng bên, nhà văn Thúy Ái đang tranh thủ “làm việc” với một học viên. Ở đó có tiếng cười nhẹ. Tôi đi thẳng về phía cô giáo vừa bước ra khỏi lớp.
 
- Chào cô!
- Dạ, anh…
 
Giọng Nghệ. Nguyễn Thị Hồng. Nước da ngăm, duyên dáng. Sinh viên khoa văn Đại học Đà Lạt. Tình nguyện vào trường với hợp đồng sáu năm. “Em học trò thầy Phạm Quốc Ca”, Hồng tự giới thiệu, khi biết chúng tôi là đoàn nhà văn thành phố lên thăm trường. “Bạn thơ mình đấy mà”, tôi nói, đưa cho Hồng danh thiếp. Hồng cảm thấy thoải mái hơn. Tôi liếc nhanh vào lớp. Ba người quay sang nhìn tôi. Tôi gật đầu chào, mỉm cười. Họ cùng cười, rồi tiếp tục nhìn lên bảng. Duy một học viên không nhếch mép đáp lại. Người cao và gầy, trạc ba mươi ngồi cạnh cửa sổ. Anh nằm rạp người hẳn lên bàn, tay cầm bút chì cạ cạ vào tập vở, lơ đãng nghe thầy giảng bài. Tôi lảng ra xa hơn.
 
- Anh thấy không khí thân mật, không vẻ gì phân biệt đối xử cả…
- Dạ. 
 
Qua cán bộ trường, tôi biết thêm. Chu trình khép kín: người nghiện được công an đưa về - nhập khu cai nghiện - học tập về đạo đức, lối sống - qua lớp dạy văn hóa - rồi học nghề và được tạo nghề. Khu đối diện cách lớp học văn hóa vài chục bước là xưởng lao động. Nhà thoáng mát, vệ sinh. Hơn ba trăm anh em đang bóc hạt điều tại đó. Đồng phục và miệt mài. Họ đang chuẩn bị “vào đời”, sau vài lỗi lầm dại dột.
Em buồn tình anh à. Nó đi lao động nước ngoài rồi ở luôn bên đó. Vài lần thư từ cho em rồi thôi. Về quê, nó cũng không hỏi han tới em.
 
Cháu dại quá đi thôi, bạn bè rủ chơi vài lần, thế là dính luôn.
 
Đời con không gì cả, chỉ sợ cha mẹ con buồn thôi, chú ạ. Mẹ mới lên thăm sáng hôm qua, con không dám gặp mẹ nữa…
 
“Một cuộc đời không là gì cả, một cuộc đời là tất cả” - Câu nói đầy nhân bản của Albert Camus có thể thâu tóm đầy đủ cuộc sống nơi đây. Con người dù sa lầy tới đâu, dù lội lỗi bao nhiêu vẫn là một đời người. Chúng ta sinh chỉ một lần, sống chỉ một lần. Cần phải cứu vãn cuộc sống đó.
 
Trường Giáo dục Đào tạo và Giải quyết việc làm là một trong những nơi giúp cứu vãn cuộc sống như thế. Có lúc số trại viên của trường lên tới hai ngàn ba trăm, nay còn tám trăm rưỡi. Đã có sáu trăm “anh” tái hòa nhập cộng đồng. Tái hòa nhập và, ít khi trở lại. Dù môi trường sinh hoạt của trường khá tốt, không khí đầm ấm, thâm tình. Ngon gấp mươi lần tụi mình thuở sinh viên, anh Đặng Hồng Quang nhận định thế. Hay dù trường có đủ môn giải trí: bóng bàn, bóng chuyền, phòng karaoke, đàn hát cho nhau nghe, hay mới tuần trước Trung tâm ca nhạc nhẹ Thành phố vừa lên phục vụ anh em nữa. Như là một đại gia đình vậy.
Nhớ lời anh bạn văn “toàn ép không à”, tôi ngắt nhỏ Hồng:
 
- Có ca bệnh không em?
- Có, nhưng ít lắm anh à. Các bạn không biết đâu, chỉ khi có thông tin “em nằm viện” thì lớp mới hay. 
- Em không ngại à?
- Không… Người lạ thì có, chứ ở đây các bạn hiền lắm. Còn ít quậy hơn học sinh ngoài phố nữa đấy. 
- Mong các bạn thành công - Tôi nói lời từ biệt Hồng và quay sang bắt tay Th. thật chặt.
Tôi nhìn lướt qua các phòng học, đưa tay lên vẫy. Không có bàn tay nào cào tôi như anh bạn cảnh giác, nơi ấy có những bàn tay vẫy đáp lại. Cùng nụ cười buồn, thâm tình.
 
Những bàn tay vẫy gọi nhau làm người.